﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "stringLiterals.h"
// System.Runtime.InteropServices.GCHandle
#include "mscorlib_System_Runtime_InteropServices_GCHandle.h"
// Metadata Definition System.Runtime.InteropServices.GCHandle
extern TypeInfo GCHandle_t1941_il2cpp_TypeInfo;
// System.Runtime.InteropServices.GCHandle
#include "mscorlib_System_Runtime_InteropServices_GCHandleMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType GCHandleType_t1942_0_0_0;
extern const Il2CppType GCHandleType_t1942_0_0_0;
static const ParameterInfo GCHandle_t1941_GCHandle__ctor_m10421_ParameterInfos[] = 
{
	{"value", 0, 134222218, 0, &Object_t_0_0_0},
	{"type", 1, 134222219, 0, &GCHandleType_t1942_0_0_0},
};
extern const Il2CppType Void_t272_0_0_0;
extern void* RuntimeInvoker_Void_t272_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.GCHandle::.ctor(System.Object,System.Runtime.InteropServices.GCHandleType)
extern const MethodInfo GCHandle__ctor_m10421_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&GCHandle__ctor_m10421/* method */
	, &GCHandle_t1941_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Int32_t253/* invoker_method */
	, GCHandle_t1941_GCHandle__ctor_m10421_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6273/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3635/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.InteropServices.GCHandle::get_IsAllocated()
extern const MethodInfo GCHandle_get_IsAllocated_m10422_MethodInfo = 
{
	"get_IsAllocated"/* name */
	, (methodPointerType)&GCHandle_get_IsAllocated_m10422/* method */
	, &GCHandle_t1941_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3636/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.InteropServices.GCHandle::get_Target()
extern const MethodInfo GCHandle_get_Target_m10423_MethodInfo = 
{
	"get_Target"/* name */
	, (methodPointerType)&GCHandle_get_Target_m10423/* method */
	, &GCHandle_t1941_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3637/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType GCHandleType_t1942_0_0_0;
static const ParameterInfo GCHandle_t1941_GCHandle_Alloc_m10424_ParameterInfos[] = 
{
	{"value", 0, 134222220, 0, &Object_t_0_0_0},
	{"type", 1, 134222221, 0, &GCHandleType_t1942_0_0_0},
};
extern const Il2CppType GCHandle_t1941_0_0_0;
extern void* RuntimeInvoker_GCHandle_t1941_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Runtime.InteropServices.GCHandle System.Runtime.InteropServices.GCHandle::Alloc(System.Object,System.Runtime.InteropServices.GCHandleType)
extern const MethodInfo GCHandle_Alloc_m10424_MethodInfo = 
{
	"Alloc"/* name */
	, (methodPointerType)&GCHandle_Alloc_m10424/* method */
	, &GCHandle_t1941_il2cpp_TypeInfo/* declaring_type */
	, &GCHandle_t1941_0_0_0/* return_type */
	, RuntimeInvoker_GCHandle_t1941_Object_t_Int32_t253/* invoker_method */
	, GCHandle_t1941_GCHandle_Alloc_m10424_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3638/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.GCHandle::Free()
extern const MethodInfo GCHandle_Free_m10425_MethodInfo = 
{
	"Free"/* name */
	, (methodPointerType)&GCHandle_Free_m10425/* method */
	, &GCHandle_t1941_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3639/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo GCHandle_t1941_GCHandle_GetTarget_m10426_ParameterInfos[] = 
{
	{"handle", 0, 134222222, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.InteropServices.GCHandle::GetTarget(System.Int32)
extern const MethodInfo GCHandle_GetTarget_m10426_MethodInfo = 
{
	"GetTarget"/* name */
	, (methodPointerType)&GCHandle_GetTarget_m10426/* method */
	, &GCHandle_t1941_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t253/* invoker_method */
	, GCHandle_t1941_GCHandle_GetTarget_m10426_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3640/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType GCHandleType_t1942_0_0_0;
static const ParameterInfo GCHandle_t1941_GCHandle_GetTargetHandle_m10427_ParameterInfos[] = 
{
	{"obj", 0, 134222223, 0, &Object_t_0_0_0},
	{"handle", 1, 134222224, 0, &Int32_t253_0_0_0},
	{"type", 2, 134222225, 0, &GCHandleType_t1942_0_0_0},
};
extern void* RuntimeInvoker_Int32_t253_Object_t_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Runtime.InteropServices.GCHandle::GetTargetHandle(System.Object,System.Int32,System.Runtime.InteropServices.GCHandleType)
extern const MethodInfo GCHandle_GetTargetHandle_m10427_MethodInfo = 
{
	"GetTargetHandle"/* name */
	, (methodPointerType)&GCHandle_GetTargetHandle_m10427/* method */
	, &GCHandle_t1941_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253_Object_t_Int32_t253_Int32_t253/* invoker_method */
	, GCHandle_t1941_GCHandle_GetTargetHandle_m10427_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3641/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo GCHandle_t1941_GCHandle_FreeHandle_m10428_ParameterInfos[] = 
{
	{"handle", 0, 134222226, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.GCHandle::FreeHandle(System.Int32)
extern const MethodInfo GCHandle_FreeHandle_m10428_MethodInfo = 
{
	"FreeHandle"/* name */
	, (methodPointerType)&GCHandle_FreeHandle_m10428/* method */
	, &GCHandle_t1941_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, GCHandle_t1941_GCHandle_FreeHandle_m10428_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3642/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo GCHandle_t1941_GCHandle_Equals_m10429_ParameterInfos[] = 
{
	{"o", 0, 134222227, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.InteropServices.GCHandle::Equals(System.Object)
extern const MethodInfo GCHandle_Equals_m10429_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&GCHandle_Equals_m10429/* method */
	, &GCHandle_t1941_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, GCHandle_t1941_GCHandle_Equals_m10429_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3643/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Runtime.InteropServices.GCHandle::GetHashCode()
extern const MethodInfo GCHandle_GetHashCode_m10430_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&GCHandle_GetHashCode_m10430/* method */
	, &GCHandle_t1941_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3644/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* GCHandle_t1941_MethodInfos[] =
{
	&GCHandle__ctor_m10421_MethodInfo,
	&GCHandle_get_IsAllocated_m10422_MethodInfo,
	&GCHandle_get_Target_m10423_MethodInfo,
	&GCHandle_Alloc_m10424_MethodInfo,
	&GCHandle_Free_m10425_MethodInfo,
	&GCHandle_GetTarget_m10426_MethodInfo,
	&GCHandle_GetTargetHandle_m10427_MethodInfo,
	&GCHandle_FreeHandle_m10428_MethodInfo,
	&GCHandle_Equals_m10429_MethodInfo,
	&GCHandle_GetHashCode_m10430_MethodInfo,
	NULL
};
extern const MethodInfo GCHandle_get_IsAllocated_m10422_MethodInfo;
static const PropertyInfo GCHandle_t1941____IsAllocated_PropertyInfo = 
{
	&GCHandle_t1941_il2cpp_TypeInfo/* parent */
	, "IsAllocated"/* name */
	, &GCHandle_get_IsAllocated_m10422_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo GCHandle_get_Target_m10423_MethodInfo;
static const PropertyInfo GCHandle_t1941____Target_PropertyInfo = 
{
	&GCHandle_t1941_il2cpp_TypeInfo/* parent */
	, "Target"/* name */
	, &GCHandle_get_Target_m10423_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* GCHandle_t1941_PropertyInfos[] =
{
	&GCHandle_t1941____IsAllocated_PropertyInfo,
	&GCHandle_t1941____Target_PropertyInfo,
	NULL
};
extern const MethodInfo GCHandle_Equals_m10429_MethodInfo;
extern const MethodInfo Object_Finalize_m1048_MethodInfo;
extern const MethodInfo GCHandle_GetHashCode_m10430_MethodInfo;
extern const MethodInfo ValueType_ToString_m1078_MethodInfo;
static const Il2CppMethodReference GCHandle_t1941_VTable[] =
{
	&GCHandle_Equals_m10429_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&GCHandle_GetHashCode_m10430_MethodInfo,
	&ValueType_ToString_m1078_MethodInfo,
};
static bool GCHandle_t1941_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType GCHandle_t1941_1_0_0;
extern const Il2CppType ValueType_t285_0_0_0;
const Il2CppTypeDefinitionMetadata GCHandle_t1941_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t285_0_0_0/* parent */
	, GCHandle_t1941_VTable/* vtableMethods */
	, GCHandle_t1941_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1850/* fieldStart */

};
TypeInfo GCHandle_t1941_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "GCHandle"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, GCHandle_t1941_MethodInfos/* methods */
	, GCHandle_t1941_PropertyInfos/* properties */
	, NULL/* events */
	, &GCHandle_t1941_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 500/* custom_attributes_cache */
	, &GCHandle_t1941_0_0_0/* byval_arg */
	, &GCHandle_t1941_1_0_0/* this_arg */
	, &GCHandle_t1941_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GCHandle_t1941)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (GCHandle_t1941)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(GCHandle_t1941 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 10/* method_count */
	, 2/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.InteropServices.GCHandleType
#include "mscorlib_System_Runtime_InteropServices_GCHandleType.h"
// Metadata Definition System.Runtime.InteropServices.GCHandleType
extern TypeInfo GCHandleType_t1942_il2cpp_TypeInfo;
// System.Runtime.InteropServices.GCHandleType
#include "mscorlib_System_Runtime_InteropServices_GCHandleTypeMethodDeclarations.h"
static const MethodInfo* GCHandleType_t1942_MethodInfos[] =
{
	NULL
};
extern const MethodInfo Enum_Equals_m1051_MethodInfo;
extern const MethodInfo Enum_GetHashCode_m1052_MethodInfo;
extern const MethodInfo Enum_ToString_m1053_MethodInfo;
extern const MethodInfo Enum_ToString_m1054_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToBoolean_m1055_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToByte_m1056_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToChar_m1057_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDateTime_m1058_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDecimal_m1059_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDouble_m1060_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt16_m1061_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt32_m1062_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt64_m1063_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToSByte_m1064_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToSingle_m1065_MethodInfo;
extern const MethodInfo Enum_ToString_m1066_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToType_m1067_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt16_m1068_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt32_m1069_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt64_m1070_MethodInfo;
extern const MethodInfo Enum_CompareTo_m1071_MethodInfo;
extern const MethodInfo Enum_GetTypeCode_m1072_MethodInfo;
static const Il2CppMethodReference GCHandleType_t1942_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool GCHandleType_t1942_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IFormattable_t275_0_0_0;
extern const Il2CppType IConvertible_t276_0_0_0;
extern const Il2CppType IComparable_t277_0_0_0;
static Il2CppInterfaceOffsetPair GCHandleType_t1942_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType GCHandleType_t1942_1_0_0;
extern const Il2CppType Enum_t278_0_0_0;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t253_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata GCHandleType_t1942_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, GCHandleType_t1942_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, GCHandleType_t1942_VTable/* vtableMethods */
	, GCHandleType_t1942_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1851/* fieldStart */

};
TypeInfo GCHandleType_t1942_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "GCHandleType"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, GCHandleType_t1942_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 501/* custom_attributes_cache */
	, &GCHandleType_t1942_0_0_0/* byval_arg */
	, &GCHandleType_t1942_1_0_0/* this_arg */
	, &GCHandleType_t1942_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GCHandleType_t1942)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (GCHandleType_t1942)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.InteropServices.InterfaceTypeAttribute
#include "mscorlib_System_Runtime_InteropServices_InterfaceTypeAttribu.h"
// Metadata Definition System.Runtime.InteropServices.InterfaceTypeAttribute
extern TypeInfo InterfaceTypeAttribute_t1943_il2cpp_TypeInfo;
// System.Runtime.InteropServices.InterfaceTypeAttribute
#include "mscorlib_System_Runtime_InteropServices_InterfaceTypeAttribuMethodDeclarations.h"
extern const Il2CppType ComInterfaceType_t1939_0_0_0;
extern const Il2CppType ComInterfaceType_t1939_0_0_0;
static const ParameterInfo InterfaceTypeAttribute_t1943_InterfaceTypeAttribute__ctor_m10431_ParameterInfos[] = 
{
	{"interfaceType", 0, 134222228, 0, &ComInterfaceType_t1939_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.InterfaceTypeAttribute::.ctor(System.Runtime.InteropServices.ComInterfaceType)
extern const MethodInfo InterfaceTypeAttribute__ctor_m10431_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InterfaceTypeAttribute__ctor_m10431/* method */
	, &InterfaceTypeAttribute_t1943_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, InterfaceTypeAttribute_t1943_InterfaceTypeAttribute__ctor_m10431_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3645/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* InterfaceTypeAttribute_t1943_MethodInfos[] =
{
	&InterfaceTypeAttribute__ctor_m10431_MethodInfo,
	NULL
};
extern const MethodInfo Attribute_Equals_m5384_MethodInfo;
extern const MethodInfo Attribute_GetHashCode_m5253_MethodInfo;
extern const MethodInfo Object_ToString_m1075_MethodInfo;
static const Il2CppMethodReference InterfaceTypeAttribute_t1943_VTable[] =
{
	&Attribute_Equals_m5384_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Attribute_GetHashCode_m5253_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool InterfaceTypeAttribute_t1943_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern const Il2CppType _Attribute_t1145_0_0_0;
static Il2CppInterfaceOffsetPair InterfaceTypeAttribute_t1943_InterfacesOffsets[] = 
{
	{ &_Attribute_t1145_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType InterfaceTypeAttribute_t1943_0_0_0;
extern const Il2CppType InterfaceTypeAttribute_t1943_1_0_0;
extern const Il2CppType Attribute_t805_0_0_0;
struct InterfaceTypeAttribute_t1943;
const Il2CppTypeDefinitionMetadata InterfaceTypeAttribute_t1943_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, InterfaceTypeAttribute_t1943_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t805_0_0_0/* parent */
	, InterfaceTypeAttribute_t1943_VTable/* vtableMethods */
	, InterfaceTypeAttribute_t1943_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1856/* fieldStart */

};
TypeInfo InterfaceTypeAttribute_t1943_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InterfaceTypeAttribute"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, InterfaceTypeAttribute_t1943_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &InterfaceTypeAttribute_t1943_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 502/* custom_attributes_cache */
	, &InterfaceTypeAttribute_t1943_0_0_0/* byval_arg */
	, &InterfaceTypeAttribute_t1943_1_0_0/* this_arg */
	, &InterfaceTypeAttribute_t1943_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InterfaceTypeAttribute_t1943)/* instance_size */
	, sizeof (InterfaceTypeAttribute_t1943)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.InteropServices.Marshal
#include "mscorlib_System_Runtime_InteropServices_Marshal.h"
// Metadata Definition System.Runtime.InteropServices.Marshal
extern TypeInfo Marshal_t1061_il2cpp_TypeInfo;
// System.Runtime.InteropServices.Marshal
#include "mscorlib_System_Runtime_InteropServices_MarshalMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.Marshal::.cctor()
extern const MethodInfo Marshal__cctor_m10432_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&Marshal__cctor_m10432/* method */
	, &Marshal_t1061_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3646/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Array_t_0_0_0;
extern const Il2CppType Array_t_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo Marshal_t1061_Marshal_copy_from_unmanaged_m10433_ParameterInfos[] = 
{
	{"source", 0, 134222229, 0, &IntPtr_t_0_0_0},
	{"startIndex", 1, 134222230, 0, &Int32_t253_0_0_0},
	{"destination", 2, 134222231, 0, &Array_t_0_0_0},
	{"length", 3, 134222232, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_IntPtr_t_Int32_t253_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.Marshal::copy_from_unmanaged(System.IntPtr,System.Int32,System.Array,System.Int32)
extern const MethodInfo Marshal_copy_from_unmanaged_m10433_MethodInfo = 
{
	"copy_from_unmanaged"/* name */
	, (methodPointerType)&Marshal_copy_from_unmanaged_m10433/* method */
	, &Marshal_t1061_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_IntPtr_t_Int32_t253_Object_t_Int32_t253/* invoker_method */
	, Marshal_t1061_Marshal_copy_from_unmanaged_m10433_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3647/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType ByteU5BU5D_t850_0_0_0;
extern const Il2CppType ByteU5BU5D_t850_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo Marshal_t1061_Marshal_Copy_m10434_ParameterInfos[] = 
{
	{"source", 0, 134222233, 0, &IntPtr_t_0_0_0},
	{"destination", 1, 134222234, 0, &ByteU5BU5D_t850_0_0_0},
	{"startIndex", 2, 134222235, 0, &Int32_t253_0_0_0},
	{"length", 3, 134222236, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_IntPtr_t_Object_t_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.Marshal::Copy(System.IntPtr,System.Byte[],System.Int32,System.Int32)
extern const MethodInfo Marshal_Copy_m10434_MethodInfo = 
{
	"Copy"/* name */
	, (methodPointerType)&Marshal_Copy_m10434/* method */
	, &Marshal_t1061_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_IntPtr_t_Object_t_Int32_t253_Int32_t253/* invoker_method */
	, Marshal_t1061_Marshal_Copy_m10434_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3648/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType CharU5BU5D_t554_0_0_0;
extern const Il2CppType CharU5BU5D_t554_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo Marshal_t1061_Marshal_Copy_m10435_ParameterInfos[] = 
{
	{"source", 0, 134222237, 0, &IntPtr_t_0_0_0},
	{"destination", 1, 134222238, 0, &CharU5BU5D_t554_0_0_0},
	{"startIndex", 2, 134222239, 0, &Int32_t253_0_0_0},
	{"length", 3, 134222240, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_IntPtr_t_Object_t_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.Marshal::Copy(System.IntPtr,System.Char[],System.Int32,System.Int32)
extern const MethodInfo Marshal_Copy_m10435_MethodInfo = 
{
	"Copy"/* name */
	, (methodPointerType)&Marshal_Copy_m10435/* method */
	, &Marshal_t1061_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_IntPtr_t_Object_t_Int32_t253_Int32_t253/* invoker_method */
	, Marshal_t1061_Marshal_Copy_m10435_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3649/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo Marshal_t1061_Marshal_SizeOf_m5137_ParameterInfos[] = 
{
	{"t", 0, 134222241, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Runtime.InteropServices.Marshal::SizeOf(System.Type)
extern const MethodInfo Marshal_SizeOf_m5137_MethodInfo = 
{
	"SizeOf"/* name */
	, (methodPointerType)&Marshal_SizeOf_m5137/* method */
	, &Marshal_t1061_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253_Object_t/* invoker_method */
	, Marshal_t1061_Marshal_SizeOf_m5137_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3650/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Marshal_t1061_MethodInfos[] =
{
	&Marshal__cctor_m10432_MethodInfo,
	&Marshal_copy_from_unmanaged_m10433_MethodInfo,
	&Marshal_Copy_m10434_MethodInfo,
	&Marshal_Copy_m10435_MethodInfo,
	&Marshal_SizeOf_m5137_MethodInfo,
	NULL
};
extern const MethodInfo Object_Equals_m1073_MethodInfo;
extern const MethodInfo Object_GetHashCode_m1074_MethodInfo;
static const Il2CppMethodReference Marshal_t1061_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool Marshal_t1061_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Marshal_t1061_0_0_0;
extern const Il2CppType Marshal_t1061_1_0_0;
struct Marshal_t1061;
const Il2CppTypeDefinitionMetadata Marshal_t1061_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Marshal_t1061_VTable/* vtableMethods */
	, Marshal_t1061_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1857/* fieldStart */

};
TypeInfo Marshal_t1061_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Marshal"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, Marshal_t1061_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Marshal_t1061_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 503/* custom_attributes_cache */
	, &Marshal_t1061_0_0_0/* byval_arg */
	, &Marshal_t1061_1_0_0/* this_arg */
	, &Marshal_t1061_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Marshal_t1061)/* instance_size */
	, sizeof (Marshal_t1061)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Marshal_t1061_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 262529/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.InteropServices.MarshalDirectiveException
#include "mscorlib_System_Runtime_InteropServices_MarshalDirectiveExce.h"
// Metadata Definition System.Runtime.InteropServices.MarshalDirectiveException
extern TypeInfo MarshalDirectiveException_t1944_il2cpp_TypeInfo;
// System.Runtime.InteropServices.MarshalDirectiveException
#include "mscorlib_System_Runtime_InteropServices_MarshalDirectiveExceMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.MarshalDirectiveException::.ctor()
extern const MethodInfo MarshalDirectiveException__ctor_m10436_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MarshalDirectiveException__ctor_m10436/* method */
	, &MarshalDirectiveException_t1944_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3651/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1043_0_0_0;
extern const Il2CppType SerializationInfo_t1043_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
static const ParameterInfo MarshalDirectiveException_t1944_MarshalDirectiveException__ctor_m10437_ParameterInfos[] = 
{
	{"info", 0, 134222242, 0, &SerializationInfo_t1043_0_0_0},
	{"context", 1, 134222243, 0, &StreamingContext_t1044_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.MarshalDirectiveException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo MarshalDirectiveException__ctor_m10437_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MarshalDirectiveException__ctor_m10437/* method */
	, &MarshalDirectiveException_t1944_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044/* invoker_method */
	, MarshalDirectiveException_t1944_MarshalDirectiveException__ctor_m10437_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3652/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MarshalDirectiveException_t1944_MethodInfos[] =
{
	&MarshalDirectiveException__ctor_m10436_MethodInfo,
	&MarshalDirectiveException__ctor_m10437_MethodInfo,
	NULL
};
extern const MethodInfo Exception_ToString_m5385_MethodInfo;
extern const MethodInfo Exception_GetObjectData_m5386_MethodInfo;
extern const MethodInfo Exception_get_InnerException_m5387_MethodInfo;
extern const MethodInfo Exception_get_Message_m5388_MethodInfo;
extern const MethodInfo Exception_get_Source_m5389_MethodInfo;
extern const MethodInfo Exception_get_StackTrace_m5390_MethodInfo;
extern const MethodInfo Exception_GetType_m5391_MethodInfo;
static const Il2CppMethodReference MarshalDirectiveException_t1944_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Exception_ToString_m5385_MethodInfo,
	&Exception_GetObjectData_m5386_MethodInfo,
	&Exception_get_InnerException_m5387_MethodInfo,
	&Exception_get_Message_m5388_MethodInfo,
	&Exception_get_Source_m5389_MethodInfo,
	&Exception_get_StackTrace_m5390_MethodInfo,
	&Exception_GetObjectData_m5386_MethodInfo,
	&Exception_GetType_m5391_MethodInfo,
};
static bool MarshalDirectiveException_t1944_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ISerializable_t439_0_0_0;
extern const Il2CppType _Exception_t1147_0_0_0;
static Il2CppInterfaceOffsetPair MarshalDirectiveException_t1944_InterfacesOffsets[] = 
{
	{ &ISerializable_t439_0_0_0, 4},
	{ &_Exception_t1147_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MarshalDirectiveException_t1944_0_0_0;
extern const Il2CppType MarshalDirectiveException_t1944_1_0_0;
extern const Il2CppType SystemException_t1464_0_0_0;
struct MarshalDirectiveException_t1944;
const Il2CppTypeDefinitionMetadata MarshalDirectiveException_t1944_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MarshalDirectiveException_t1944_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t1464_0_0_0/* parent */
	, MarshalDirectiveException_t1944_VTable/* vtableMethods */
	, MarshalDirectiveException_t1944_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1859/* fieldStart */

};
TypeInfo MarshalDirectiveException_t1944_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MarshalDirectiveException"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, MarshalDirectiveException_t1944_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &MarshalDirectiveException_t1944_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 504/* custom_attributes_cache */
	, &MarshalDirectiveException_t1944_0_0_0/* byval_arg */
	, &MarshalDirectiveException_t1944_1_0_0/* this_arg */
	, &MarshalDirectiveException_t1944_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MarshalDirectiveException_t1944)/* instance_size */
	, sizeof (MarshalDirectiveException_t1944)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Runtime.InteropServices.PreserveSigAttribute
#include "mscorlib_System_Runtime_InteropServices_PreserveSigAttribute.h"
// Metadata Definition System.Runtime.InteropServices.PreserveSigAttribute
extern TypeInfo PreserveSigAttribute_t1945_il2cpp_TypeInfo;
// System.Runtime.InteropServices.PreserveSigAttribute
#include "mscorlib_System_Runtime_InteropServices_PreserveSigAttributeMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.PreserveSigAttribute::.ctor()
extern const MethodInfo PreserveSigAttribute__ctor_m10438_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PreserveSigAttribute__ctor_m10438/* method */
	, &PreserveSigAttribute_t1945_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3653/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* PreserveSigAttribute_t1945_MethodInfos[] =
{
	&PreserveSigAttribute__ctor_m10438_MethodInfo,
	NULL
};
static const Il2CppMethodReference PreserveSigAttribute_t1945_VTable[] =
{
	&Attribute_Equals_m5384_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Attribute_GetHashCode_m5253_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool PreserveSigAttribute_t1945_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair PreserveSigAttribute_t1945_InterfacesOffsets[] = 
{
	{ &_Attribute_t1145_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType PreserveSigAttribute_t1945_0_0_0;
extern const Il2CppType PreserveSigAttribute_t1945_1_0_0;
struct PreserveSigAttribute_t1945;
const Il2CppTypeDefinitionMetadata PreserveSigAttribute_t1945_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, PreserveSigAttribute_t1945_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t805_0_0_0/* parent */
	, PreserveSigAttribute_t1945_VTable/* vtableMethods */
	, PreserveSigAttribute_t1945_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo PreserveSigAttribute_t1945_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "PreserveSigAttribute"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, PreserveSigAttribute_t1945_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &PreserveSigAttribute_t1945_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 505/* custom_attributes_cache */
	, &PreserveSigAttribute_t1945_0_0_0/* byval_arg */
	, &PreserveSigAttribute_t1945_1_0_0/* this_arg */
	, &PreserveSigAttribute_t1945_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PreserveSigAttribute_t1945)/* instance_size */
	, sizeof (PreserveSigAttribute_t1945)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.InteropServices.SafeHandle
#include "mscorlib_System_Runtime_InteropServices_SafeHandle.h"
// Metadata Definition System.Runtime.InteropServices.SafeHandle
extern TypeInfo SafeHandle_t1688_il2cpp_TypeInfo;
// System.Runtime.InteropServices.SafeHandle
#include "mscorlib_System_Runtime_InteropServices_SafeHandleMethodDeclarations.h"
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo SafeHandle_t1688_SafeHandle__ctor_m10439_ParameterInfos[] = 
{
	{"invalidHandleValue", 0, 134222244, 0, &IntPtr_t_0_0_0},
	{"ownsHandle", 1, 134222245, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_IntPtr_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.SafeHandle::.ctor(System.IntPtr,System.Boolean)
extern const MethodInfo SafeHandle__ctor_m10439_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SafeHandle__ctor_m10439/* method */
	, &SafeHandle_t1688_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_IntPtr_t_SByte_t274/* invoker_method */
	, SafeHandle_t1688_SafeHandle__ctor_m10439_ParameterInfos/* parameters */
	, 506/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3654/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.SafeHandle::Close()
extern const MethodInfo SafeHandle_Close_m10440_MethodInfo = 
{
	"Close"/* name */
	, (methodPointerType)&SafeHandle_Close_m10440/* method */
	, &SafeHandle_t1688_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 507/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3655/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_1_0_0;
extern const Il2CppType Boolean_t273_1_0_0;
static const ParameterInfo SafeHandle_t1688_SafeHandle_DangerousAddRef_m10441_ParameterInfos[] = 
{
	{"success", 0, 134222246, 0, &Boolean_t273_1_0_0},
};
extern void* RuntimeInvoker_Void_t272_BooleanU26_t745 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.SafeHandle::DangerousAddRef(System.Boolean&)
extern const MethodInfo SafeHandle_DangerousAddRef_m10441_MethodInfo = 
{
	"DangerousAddRef"/* name */
	, (methodPointerType)&SafeHandle_DangerousAddRef_m10441/* method */
	, &SafeHandle_t1688_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_BooleanU26_t745/* invoker_method */
	, SafeHandle_t1688_SafeHandle_DangerousAddRef_m10441_ParameterInfos/* parameters */
	, 508/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3656/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.IntPtr System.Runtime.InteropServices.SafeHandle::DangerousGetHandle()
extern const MethodInfo SafeHandle_DangerousGetHandle_m10442_MethodInfo = 
{
	"DangerousGetHandle"/* name */
	, (methodPointerType)&SafeHandle_DangerousGetHandle_m10442/* method */
	, &SafeHandle_t1688_il2cpp_TypeInfo/* declaring_type */
	, &IntPtr_t_0_0_0/* return_type */
	, RuntimeInvoker_IntPtr_t/* invoker_method */
	, NULL/* parameters */
	, 509/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3657/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.SafeHandle::DangerousRelease()
extern const MethodInfo SafeHandle_DangerousRelease_m10443_MethodInfo = 
{
	"DangerousRelease"/* name */
	, (methodPointerType)&SafeHandle_DangerousRelease_m10443/* method */
	, &SafeHandle_t1688_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 510/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3658/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.SafeHandle::Dispose()
extern const MethodInfo SafeHandle_Dispose_m10444_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&SafeHandle_Dispose_m10444/* method */
	, &SafeHandle_t1688_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 511/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3659/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo SafeHandle_t1688_SafeHandle_Dispose_m10445_ParameterInfos[] = 
{
	{"disposing", 0, 134222247, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.SafeHandle::Dispose(System.Boolean)
extern const MethodInfo SafeHandle_Dispose_m10445_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&SafeHandle_Dispose_m10445/* method */
	, &SafeHandle_t1688_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274/* invoker_method */
	, SafeHandle_t1688_SafeHandle_Dispose_m10445_ParameterInfos/* parameters */
	, 512/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3660/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.InteropServices.SafeHandle::ReleaseHandle()
extern const MethodInfo SafeHandle_ReleaseHandle_m13183_MethodInfo = 
{
	"ReleaseHandle"/* name */
	, NULL/* method */
	, &SafeHandle_t1688_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 513/* custom_attributes_cache */
	, 1476/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3661/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo SafeHandle_t1688_SafeHandle_SetHandle_m10446_ParameterInfos[] = 
{
	{"handle", 0, 134222248, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.SafeHandle::SetHandle(System.IntPtr)
extern const MethodInfo SafeHandle_SetHandle_m10446_MethodInfo = 
{
	"SetHandle"/* name */
	, (methodPointerType)&SafeHandle_SetHandle_m10446/* method */
	, &SafeHandle_t1688_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_IntPtr_t/* invoker_method */
	, SafeHandle_t1688_SafeHandle_SetHandle_m10446_ParameterInfos/* parameters */
	, 514/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3662/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.InteropServices.SafeHandle::get_IsInvalid()
extern const MethodInfo SafeHandle_get_IsInvalid_m13184_MethodInfo = 
{
	"get_IsInvalid"/* name */
	, NULL/* method */
	, &SafeHandle_t1688_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 515/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3663/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.SafeHandle::Finalize()
extern const MethodInfo SafeHandle_Finalize_m10447_MethodInfo = 
{
	"Finalize"/* name */
	, (methodPointerType)&SafeHandle_Finalize_m10447/* method */
	, &SafeHandle_t1688_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3664/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SafeHandle_t1688_MethodInfos[] =
{
	&SafeHandle__ctor_m10439_MethodInfo,
	&SafeHandle_Close_m10440_MethodInfo,
	&SafeHandle_DangerousAddRef_m10441_MethodInfo,
	&SafeHandle_DangerousGetHandle_m10442_MethodInfo,
	&SafeHandle_DangerousRelease_m10443_MethodInfo,
	&SafeHandle_Dispose_m10444_MethodInfo,
	&SafeHandle_Dispose_m10445_MethodInfo,
	&SafeHandle_ReleaseHandle_m13183_MethodInfo,
	&SafeHandle_SetHandle_m10446_MethodInfo,
	&SafeHandle_get_IsInvalid_m13184_MethodInfo,
	&SafeHandle_Finalize_m10447_MethodInfo,
	NULL
};
extern const MethodInfo SafeHandle_get_IsInvalid_m13184_MethodInfo;
static const PropertyInfo SafeHandle_t1688____IsInvalid_PropertyInfo = 
{
	&SafeHandle_t1688_il2cpp_TypeInfo/* parent */
	, "IsInvalid"/* name */
	, &SafeHandle_get_IsInvalid_m13184_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* SafeHandle_t1688_PropertyInfos[] =
{
	&SafeHandle_t1688____IsInvalid_PropertyInfo,
	NULL
};
extern const MethodInfo SafeHandle_Finalize_m10447_MethodInfo;
extern const MethodInfo SafeHandle_Dispose_m10444_MethodInfo;
extern const MethodInfo SafeHandle_Dispose_m10445_MethodInfo;
static const Il2CppMethodReference SafeHandle_t1688_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&SafeHandle_Finalize_m10447_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&SafeHandle_Dispose_m10444_MethodInfo,
	&SafeHandle_Dispose_m10445_MethodInfo,
	NULL,
	NULL,
};
static bool SafeHandle_t1688_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IDisposable_t233_0_0_0;
static const Il2CppType* SafeHandle_t1688_InterfacesTypeInfos[] = 
{
	&IDisposable_t233_0_0_0,
};
static Il2CppInterfaceOffsetPair SafeHandle_t1688_InterfacesOffsets[] = 
{
	{ &IDisposable_t233_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SafeHandle_t1688_0_0_0;
extern const Il2CppType SafeHandle_t1688_1_0_0;
extern const Il2CppType CriticalFinalizerObject_t1931_0_0_0;
struct SafeHandle_t1688;
const Il2CppTypeDefinitionMetadata SafeHandle_t1688_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, SafeHandle_t1688_InterfacesTypeInfos/* implementedInterfaces */
	, SafeHandle_t1688_InterfacesOffsets/* interfaceOffsets */
	, &CriticalFinalizerObject_t1931_0_0_0/* parent */
	, SafeHandle_t1688_VTable/* vtableMethods */
	, SafeHandle_t1688_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1860/* fieldStart */

};
TypeInfo SafeHandle_t1688_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SafeHandle"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, SafeHandle_t1688_MethodInfos/* methods */
	, SafeHandle_t1688_PropertyInfos/* properties */
	, NULL/* events */
	, &SafeHandle_t1688_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SafeHandle_t1688_0_0_0/* byval_arg */
	, &SafeHandle_t1688_1_0_0/* this_arg */
	, &SafeHandle_t1688_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SafeHandle_t1688)/* instance_size */
	, sizeof (SafeHandle_t1688)/* actualSize */
	, 0/* element_size */
	, sizeof(void*)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 1/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.InteropServices.TypeLibImportClassAttribute
#include "mscorlib_System_Runtime_InteropServices_TypeLibImportClassAt.h"
// Metadata Definition System.Runtime.InteropServices.TypeLibImportClassAttribute
extern TypeInfo TypeLibImportClassAttribute_t1946_il2cpp_TypeInfo;
// System.Runtime.InteropServices.TypeLibImportClassAttribute
#include "mscorlib_System_Runtime_InteropServices_TypeLibImportClassAtMethodDeclarations.h"
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo TypeLibImportClassAttribute_t1946_TypeLibImportClassAttribute__ctor_m10448_ParameterInfos[] = 
{
	{"importClass", 0, 134222249, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.TypeLibImportClassAttribute::.ctor(System.Type)
extern const MethodInfo TypeLibImportClassAttribute__ctor_m10448_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TypeLibImportClassAttribute__ctor_m10448/* method */
	, &TypeLibImportClassAttribute_t1946_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, TypeLibImportClassAttribute_t1946_TypeLibImportClassAttribute__ctor_m10448_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3665/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TypeLibImportClassAttribute_t1946_MethodInfos[] =
{
	&TypeLibImportClassAttribute__ctor_m10448_MethodInfo,
	NULL
};
static const Il2CppMethodReference TypeLibImportClassAttribute_t1946_VTable[] =
{
	&Attribute_Equals_m5384_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Attribute_GetHashCode_m5253_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool TypeLibImportClassAttribute_t1946_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TypeLibImportClassAttribute_t1946_InterfacesOffsets[] = 
{
	{ &_Attribute_t1145_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TypeLibImportClassAttribute_t1946_0_0_0;
extern const Il2CppType TypeLibImportClassAttribute_t1946_1_0_0;
struct TypeLibImportClassAttribute_t1946;
const Il2CppTypeDefinitionMetadata TypeLibImportClassAttribute_t1946_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TypeLibImportClassAttribute_t1946_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t805_0_0_0/* parent */
	, TypeLibImportClassAttribute_t1946_VTable/* vtableMethods */
	, TypeLibImportClassAttribute_t1946_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1864/* fieldStart */

};
TypeInfo TypeLibImportClassAttribute_t1946_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeLibImportClassAttribute"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, TypeLibImportClassAttribute_t1946_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TypeLibImportClassAttribute_t1946_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 516/* custom_attributes_cache */
	, &TypeLibImportClassAttribute_t1946_0_0_0/* byval_arg */
	, &TypeLibImportClassAttribute_t1946_1_0_0/* this_arg */
	, &TypeLibImportClassAttribute_t1946_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeLibImportClassAttribute_t1946)/* instance_size */
	, sizeof (TypeLibImportClassAttribute_t1946)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.InteropServices.TypeLibVersionAttribute
#include "mscorlib_System_Runtime_InteropServices_TypeLibVersionAttrib.h"
// Metadata Definition System.Runtime.InteropServices.TypeLibVersionAttribute
extern TypeInfo TypeLibVersionAttribute_t1947_il2cpp_TypeInfo;
// System.Runtime.InteropServices.TypeLibVersionAttribute
#include "mscorlib_System_Runtime_InteropServices_TypeLibVersionAttribMethodDeclarations.h"
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo TypeLibVersionAttribute_t1947_TypeLibVersionAttribute__ctor_m10449_ParameterInfos[] = 
{
	{"major", 0, 134222250, 0, &Int32_t253_0_0_0},
	{"minor", 1, 134222251, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.TypeLibVersionAttribute::.ctor(System.Int32,System.Int32)
extern const MethodInfo TypeLibVersionAttribute__ctor_m10449_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TypeLibVersionAttribute__ctor_m10449/* method */
	, &TypeLibVersionAttribute_t1947_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253_Int32_t253/* invoker_method */
	, TypeLibVersionAttribute_t1947_TypeLibVersionAttribute__ctor_m10449_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3666/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TypeLibVersionAttribute_t1947_MethodInfos[] =
{
	&TypeLibVersionAttribute__ctor_m10449_MethodInfo,
	NULL
};
static const Il2CppMethodReference TypeLibVersionAttribute_t1947_VTable[] =
{
	&Attribute_Equals_m5384_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Attribute_GetHashCode_m5253_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool TypeLibVersionAttribute_t1947_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TypeLibVersionAttribute_t1947_InterfacesOffsets[] = 
{
	{ &_Attribute_t1145_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TypeLibVersionAttribute_t1947_0_0_0;
extern const Il2CppType TypeLibVersionAttribute_t1947_1_0_0;
struct TypeLibVersionAttribute_t1947;
const Il2CppTypeDefinitionMetadata TypeLibVersionAttribute_t1947_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TypeLibVersionAttribute_t1947_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t805_0_0_0/* parent */
	, TypeLibVersionAttribute_t1947_VTable/* vtableMethods */
	, TypeLibVersionAttribute_t1947_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1865/* fieldStart */

};
TypeInfo TypeLibVersionAttribute_t1947_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeLibVersionAttribute"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, TypeLibVersionAttribute_t1947_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TypeLibVersionAttribute_t1947_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 517/* custom_attributes_cache */
	, &TypeLibVersionAttribute_t1947_0_0_0/* byval_arg */
	, &TypeLibVersionAttribute_t1947_1_0_0/* this_arg */
	, &TypeLibVersionAttribute_t1947_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeLibVersionAttribute_t1947)/* instance_size */
	, sizeof (TypeLibVersionAttribute_t1947)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.InteropServices.UnmanagedType
#include "mscorlib_System_Runtime_InteropServices_UnmanagedType.h"
// Metadata Definition System.Runtime.InteropServices.UnmanagedType
extern TypeInfo UnmanagedType_t1948_il2cpp_TypeInfo;
// System.Runtime.InteropServices.UnmanagedType
#include "mscorlib_System_Runtime_InteropServices_UnmanagedTypeMethodDeclarations.h"
static const MethodInfo* UnmanagedType_t1948_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference UnmanagedType_t1948_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool UnmanagedType_t1948_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UnmanagedType_t1948_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType UnmanagedType_t1948_0_0_0;
extern const Il2CppType UnmanagedType_t1948_1_0_0;
const Il2CppTypeDefinitionMetadata UnmanagedType_t1948_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnmanagedType_t1948_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, UnmanagedType_t1948_VTable/* vtableMethods */
	, UnmanagedType_t1948_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1867/* fieldStart */

};
TypeInfo UnmanagedType_t1948_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnmanagedType"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, UnmanagedType_t1948_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 518/* custom_attributes_cache */
	, &UnmanagedType_t1948_0_0_0/* byval_arg */
	, &UnmanagedType_t1948_1_0_0/* this_arg */
	, &UnmanagedType_t1948_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnmanagedType_t1948)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UnmanagedType_t1948)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 36/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._Activator
extern TypeInfo _Activator_t2378_il2cpp_TypeInfo;
static const MethodInfo* _Activator_t2378_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _Activator_t2378_0_0_0;
extern const Il2CppType _Activator_t2378_1_0_0;
struct _Activator_t2378;
const Il2CppTypeDefinitionMetadata _Activator_t2378_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo _Activator_t2378_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_Activator"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, _Activator_t2378_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &_Activator_t2378_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 519/* custom_attributes_cache */
	, &_Activator_t2378_0_0_0/* byval_arg */
	, &_Activator_t2378_1_0_0/* this_arg */
	, &_Activator_t2378_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._Assembly
extern TypeInfo _Assembly_t2368_il2cpp_TypeInfo;
static const MethodInfo* _Assembly_t2368_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _Assembly_t2368_0_0_0;
extern const Il2CppType _Assembly_t2368_1_0_0;
struct _Assembly_t2368;
const Il2CppTypeDefinitionMetadata _Assembly_t2368_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo _Assembly_t2368_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_Assembly"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, _Assembly_t2368_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &_Assembly_t2368_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 520/* custom_attributes_cache */
	, &_Assembly_t2368_0_0_0/* byval_arg */
	, &_Assembly_t2368_1_0_0/* this_arg */
	, &_Assembly_t2368_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._AssemblyBuilder
extern TypeInfo _AssemblyBuilder_t2357_il2cpp_TypeInfo;
static const MethodInfo* _AssemblyBuilder_t2357_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _AssemblyBuilder_t2357_0_0_0;
extern const Il2CppType _AssemblyBuilder_t2357_1_0_0;
struct _AssemblyBuilder_t2357;
const Il2CppTypeDefinitionMetadata _AssemblyBuilder_t2357_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo _AssemblyBuilder_t2357_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_AssemblyBuilder"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, _AssemblyBuilder_t2357_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &_AssemblyBuilder_t2357_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 521/* custom_attributes_cache */
	, &_AssemblyBuilder_t2357_0_0_0/* byval_arg */
	, &_AssemblyBuilder_t2357_1_0_0/* this_arg */
	, &_AssemblyBuilder_t2357_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._AssemblyName
extern TypeInfo _AssemblyName_t2369_il2cpp_TypeInfo;
static const MethodInfo* _AssemblyName_t2369_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _AssemblyName_t2369_0_0_0;
extern const Il2CppType _AssemblyName_t2369_1_0_0;
struct _AssemblyName_t2369;
const Il2CppTypeDefinitionMetadata _AssemblyName_t2369_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo _AssemblyName_t2369_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_AssemblyName"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, _AssemblyName_t2369_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &_AssemblyName_t2369_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 522/* custom_attributes_cache */
	, &_AssemblyName_t2369_0_0_0/* byval_arg */
	, &_AssemblyName_t2369_1_0_0/* this_arg */
	, &_AssemblyName_t2369_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._ConstructorBuilder
extern TypeInfo _ConstructorBuilder_t2358_il2cpp_TypeInfo;
static const MethodInfo* _ConstructorBuilder_t2358_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _ConstructorBuilder_t2358_0_0_0;
extern const Il2CppType _ConstructorBuilder_t2358_1_0_0;
struct _ConstructorBuilder_t2358;
const Il2CppTypeDefinitionMetadata _ConstructorBuilder_t2358_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo _ConstructorBuilder_t2358_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_ConstructorBuilder"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, _ConstructorBuilder_t2358_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &_ConstructorBuilder_t2358_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 523/* custom_attributes_cache */
	, &_ConstructorBuilder_t2358_0_0_0/* byval_arg */
	, &_ConstructorBuilder_t2358_1_0_0/* this_arg */
	, &_ConstructorBuilder_t2358_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._ConstructorInfo
extern TypeInfo _ConstructorInfo_t2370_il2cpp_TypeInfo;
static const MethodInfo* _ConstructorInfo_t2370_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _ConstructorInfo_t2370_0_0_0;
extern const Il2CppType _ConstructorInfo_t2370_1_0_0;
struct _ConstructorInfo_t2370;
const Il2CppTypeDefinitionMetadata _ConstructorInfo_t2370_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo _ConstructorInfo_t2370_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_ConstructorInfo"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, _ConstructorInfo_t2370_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &_ConstructorInfo_t2370_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 524/* custom_attributes_cache */
	, &_ConstructorInfo_t2370_0_0_0/* byval_arg */
	, &_ConstructorInfo_t2370_1_0_0/* this_arg */
	, &_ConstructorInfo_t2370_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._EnumBuilder
extern TypeInfo _EnumBuilder_t2359_il2cpp_TypeInfo;
static const MethodInfo* _EnumBuilder_t2359_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _EnumBuilder_t2359_0_0_0;
extern const Il2CppType _EnumBuilder_t2359_1_0_0;
struct _EnumBuilder_t2359;
const Il2CppTypeDefinitionMetadata _EnumBuilder_t2359_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo _EnumBuilder_t2359_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_EnumBuilder"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, _EnumBuilder_t2359_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &_EnumBuilder_t2359_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 525/* custom_attributes_cache */
	, &_EnumBuilder_t2359_0_0_0/* byval_arg */
	, &_EnumBuilder_t2359_1_0_0/* this_arg */
	, &_EnumBuilder_t2359_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._EventInfo
extern TypeInfo _EventInfo_t2371_il2cpp_TypeInfo;
static const MethodInfo* _EventInfo_t2371_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _EventInfo_t2371_0_0_0;
extern const Il2CppType _EventInfo_t2371_1_0_0;
struct _EventInfo_t2371;
const Il2CppTypeDefinitionMetadata _EventInfo_t2371_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo _EventInfo_t2371_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_EventInfo"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, _EventInfo_t2371_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &_EventInfo_t2371_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 526/* custom_attributes_cache */
	, &_EventInfo_t2371_0_0_0/* byval_arg */
	, &_EventInfo_t2371_1_0_0/* this_arg */
	, &_EventInfo_t2371_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._FieldBuilder
extern TypeInfo _FieldBuilder_t2360_il2cpp_TypeInfo;
static const MethodInfo* _FieldBuilder_t2360_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _FieldBuilder_t2360_0_0_0;
extern const Il2CppType _FieldBuilder_t2360_1_0_0;
struct _FieldBuilder_t2360;
const Il2CppTypeDefinitionMetadata _FieldBuilder_t2360_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo _FieldBuilder_t2360_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_FieldBuilder"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, _FieldBuilder_t2360_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &_FieldBuilder_t2360_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 527/* custom_attributes_cache */
	, &_FieldBuilder_t2360_0_0_0/* byval_arg */
	, &_FieldBuilder_t2360_1_0_0/* this_arg */
	, &_FieldBuilder_t2360_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._FieldInfo
extern TypeInfo _FieldInfo_t2372_il2cpp_TypeInfo;
static const MethodInfo* _FieldInfo_t2372_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _FieldInfo_t2372_0_0_0;
extern const Il2CppType _FieldInfo_t2372_1_0_0;
struct _FieldInfo_t2372;
const Il2CppTypeDefinitionMetadata _FieldInfo_t2372_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo _FieldInfo_t2372_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_FieldInfo"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, _FieldInfo_t2372_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &_FieldInfo_t2372_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 528/* custom_attributes_cache */
	, &_FieldInfo_t2372_0_0_0/* byval_arg */
	, &_FieldInfo_t2372_1_0_0/* this_arg */
	, &_FieldInfo_t2372_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._ILGenerator
extern TypeInfo _ILGenerator_t2361_il2cpp_TypeInfo;
static const MethodInfo* _ILGenerator_t2361_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _ILGenerator_t2361_0_0_0;
extern const Il2CppType _ILGenerator_t2361_1_0_0;
struct _ILGenerator_t2361;
const Il2CppTypeDefinitionMetadata _ILGenerator_t2361_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo _ILGenerator_t2361_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_ILGenerator"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, _ILGenerator_t2361_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &_ILGenerator_t2361_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 529/* custom_attributes_cache */
	, &_ILGenerator_t2361_0_0_0/* byval_arg */
	, &_ILGenerator_t2361_1_0_0/* this_arg */
	, &_ILGenerator_t2361_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._LocalBuilder
extern TypeInfo _LocalBuilder_t2362_il2cpp_TypeInfo;
static const MethodInfo* _LocalBuilder_t2362_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _LocalBuilder_t2362_0_0_0;
extern const Il2CppType _LocalBuilder_t2362_1_0_0;
struct _LocalBuilder_t2362;
const Il2CppTypeDefinitionMetadata _LocalBuilder_t2362_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo _LocalBuilder_t2362_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_LocalBuilder"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, _LocalBuilder_t2362_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &_LocalBuilder_t2362_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 530/* custom_attributes_cache */
	, &_LocalBuilder_t2362_0_0_0/* byval_arg */
	, &_LocalBuilder_t2362_1_0_0/* this_arg */
	, &_LocalBuilder_t2362_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._MethodBase
extern TypeInfo _MethodBase_t2373_il2cpp_TypeInfo;
static const MethodInfo* _MethodBase_t2373_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _MethodBase_t2373_0_0_0;
extern const Il2CppType _MethodBase_t2373_1_0_0;
struct _MethodBase_t2373;
const Il2CppTypeDefinitionMetadata _MethodBase_t2373_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo _MethodBase_t2373_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_MethodBase"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, _MethodBase_t2373_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &_MethodBase_t2373_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 531/* custom_attributes_cache */
	, &_MethodBase_t2373_0_0_0/* byval_arg */
	, &_MethodBase_t2373_1_0_0/* this_arg */
	, &_MethodBase_t2373_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._MethodBuilder
extern TypeInfo _MethodBuilder_t2363_il2cpp_TypeInfo;
static const MethodInfo* _MethodBuilder_t2363_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _MethodBuilder_t2363_0_0_0;
extern const Il2CppType _MethodBuilder_t2363_1_0_0;
struct _MethodBuilder_t2363;
const Il2CppTypeDefinitionMetadata _MethodBuilder_t2363_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo _MethodBuilder_t2363_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_MethodBuilder"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, _MethodBuilder_t2363_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &_MethodBuilder_t2363_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 532/* custom_attributes_cache */
	, &_MethodBuilder_t2363_0_0_0/* byval_arg */
	, &_MethodBuilder_t2363_1_0_0/* this_arg */
	, &_MethodBuilder_t2363_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._MethodInfo
extern TypeInfo _MethodInfo_t2374_il2cpp_TypeInfo;
static const MethodInfo* _MethodInfo_t2374_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _MethodInfo_t2374_0_0_0;
extern const Il2CppType _MethodInfo_t2374_1_0_0;
struct _MethodInfo_t2374;
const Il2CppTypeDefinitionMetadata _MethodInfo_t2374_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo _MethodInfo_t2374_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_MethodInfo"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, _MethodInfo_t2374_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &_MethodInfo_t2374_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 533/* custom_attributes_cache */
	, &_MethodInfo_t2374_0_0_0/* byval_arg */
	, &_MethodInfo_t2374_1_0_0/* this_arg */
	, &_MethodInfo_t2374_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._Module
extern TypeInfo _Module_t2375_il2cpp_TypeInfo;
static const MethodInfo* _Module_t2375_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _Module_t2375_0_0_0;
extern const Il2CppType _Module_t2375_1_0_0;
struct _Module_t2375;
const Il2CppTypeDefinitionMetadata _Module_t2375_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo _Module_t2375_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_Module"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, _Module_t2375_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &_Module_t2375_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 534/* custom_attributes_cache */
	, &_Module_t2375_0_0_0/* byval_arg */
	, &_Module_t2375_1_0_0/* this_arg */
	, &_Module_t2375_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._ModuleBuilder
extern TypeInfo _ModuleBuilder_t2364_il2cpp_TypeInfo;
static const MethodInfo* _ModuleBuilder_t2364_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _ModuleBuilder_t2364_0_0_0;
extern const Il2CppType _ModuleBuilder_t2364_1_0_0;
struct _ModuleBuilder_t2364;
const Il2CppTypeDefinitionMetadata _ModuleBuilder_t2364_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo _ModuleBuilder_t2364_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_ModuleBuilder"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, _ModuleBuilder_t2364_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &_ModuleBuilder_t2364_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 535/* custom_attributes_cache */
	, &_ModuleBuilder_t2364_0_0_0/* byval_arg */
	, &_ModuleBuilder_t2364_1_0_0/* this_arg */
	, &_ModuleBuilder_t2364_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._ParameterBuilder
extern TypeInfo _ParameterBuilder_t2365_il2cpp_TypeInfo;
static const MethodInfo* _ParameterBuilder_t2365_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _ParameterBuilder_t2365_0_0_0;
extern const Il2CppType _ParameterBuilder_t2365_1_0_0;
struct _ParameterBuilder_t2365;
const Il2CppTypeDefinitionMetadata _ParameterBuilder_t2365_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo _ParameterBuilder_t2365_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_ParameterBuilder"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, _ParameterBuilder_t2365_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &_ParameterBuilder_t2365_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 536/* custom_attributes_cache */
	, &_ParameterBuilder_t2365_0_0_0/* byval_arg */
	, &_ParameterBuilder_t2365_1_0_0/* this_arg */
	, &_ParameterBuilder_t2365_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._ParameterInfo
extern TypeInfo _ParameterInfo_t2376_il2cpp_TypeInfo;
static const MethodInfo* _ParameterInfo_t2376_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _ParameterInfo_t2376_0_0_0;
extern const Il2CppType _ParameterInfo_t2376_1_0_0;
struct _ParameterInfo_t2376;
const Il2CppTypeDefinitionMetadata _ParameterInfo_t2376_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo _ParameterInfo_t2376_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_ParameterInfo"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, _ParameterInfo_t2376_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &_ParameterInfo_t2376_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 537/* custom_attributes_cache */
	, &_ParameterInfo_t2376_0_0_0/* byval_arg */
	, &_ParameterInfo_t2376_1_0_0/* this_arg */
	, &_ParameterInfo_t2376_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._PropertyBuilder
extern TypeInfo _PropertyBuilder_t2366_il2cpp_TypeInfo;
static const MethodInfo* _PropertyBuilder_t2366_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _PropertyBuilder_t2366_0_0_0;
extern const Il2CppType _PropertyBuilder_t2366_1_0_0;
struct _PropertyBuilder_t2366;
const Il2CppTypeDefinitionMetadata _PropertyBuilder_t2366_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo _PropertyBuilder_t2366_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_PropertyBuilder"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, _PropertyBuilder_t2366_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &_PropertyBuilder_t2366_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 538/* custom_attributes_cache */
	, &_PropertyBuilder_t2366_0_0_0/* byval_arg */
	, &_PropertyBuilder_t2366_1_0_0/* this_arg */
	, &_PropertyBuilder_t2366_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._PropertyInfo
extern TypeInfo _PropertyInfo_t2377_il2cpp_TypeInfo;
static const MethodInfo* _PropertyInfo_t2377_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _PropertyInfo_t2377_0_0_0;
extern const Il2CppType _PropertyInfo_t2377_1_0_0;
struct _PropertyInfo_t2377;
const Il2CppTypeDefinitionMetadata _PropertyInfo_t2377_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo _PropertyInfo_t2377_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_PropertyInfo"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, _PropertyInfo_t2377_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &_PropertyInfo_t2377_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 539/* custom_attributes_cache */
	, &_PropertyInfo_t2377_0_0_0/* byval_arg */
	, &_PropertyInfo_t2377_1_0_0/* this_arg */
	, &_PropertyInfo_t2377_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._Thread
extern TypeInfo _Thread_t2379_il2cpp_TypeInfo;
static const MethodInfo* _Thread_t2379_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _Thread_t2379_0_0_0;
extern const Il2CppType _Thread_t2379_1_0_0;
struct _Thread_t2379;
const Il2CppTypeDefinitionMetadata _Thread_t2379_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo _Thread_t2379_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_Thread"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, _Thread_t2379_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &_Thread_t2379_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 540/* custom_attributes_cache */
	, &_Thread_t2379_0_0_0/* byval_arg */
	, &_Thread_t2379_1_0_0/* this_arg */
	, &_Thread_t2379_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._TypeBuilder
extern TypeInfo _TypeBuilder_t2367_il2cpp_TypeInfo;
static const MethodInfo* _TypeBuilder_t2367_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _TypeBuilder_t2367_0_0_0;
extern const Il2CppType _TypeBuilder_t2367_1_0_0;
struct _TypeBuilder_t2367;
const Il2CppTypeDefinitionMetadata _TypeBuilder_t2367_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo _TypeBuilder_t2367_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_TypeBuilder"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, _TypeBuilder_t2367_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &_TypeBuilder_t2367_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 541/* custom_attributes_cache */
	, &_TypeBuilder_t2367_0_0_0/* byval_arg */
	, &_TypeBuilder_t2367_1_0_0/* this_arg */
	, &_TypeBuilder_t2367_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Activation.ActivationServices
#include "mscorlib_System_Runtime_Remoting_Activation_ActivationServic.h"
// Metadata Definition System.Runtime.Remoting.Activation.ActivationServices
extern TypeInfo ActivationServices_t1950_il2cpp_TypeInfo;
// System.Runtime.Remoting.Activation.ActivationServices
#include "mscorlib_System_Runtime_Remoting_Activation_ActivationServicMethodDeclarations.h"
extern const Il2CppType IActivator_t1949_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Activation.IActivator System.Runtime.Remoting.Activation.ActivationServices::get_ConstructionActivator()
extern const MethodInfo ActivationServices_get_ConstructionActivator_m10450_MethodInfo = 
{
	"get_ConstructionActivator"/* name */
	, (methodPointerType)&ActivationServices_get_ConstructionActivator_m10450/* method */
	, &ActivationServices_t1950_il2cpp_TypeInfo/* declaring_type */
	, &IActivator_t1949_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2193/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3667/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType ObjectU5BU5D_t224_0_0_0;
extern const Il2CppType ObjectU5BU5D_t224_0_0_0;
static const ParameterInfo ActivationServices_t1950_ActivationServices_CreateProxyFromAttributes_m10451_ParameterInfos[] = 
{
	{"type", 0, 134222252, 0, &Type_t_0_0_0},
	{"activationAttributes", 1, 134222253, 0, &ObjectU5BU5D_t224_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Activation.ActivationServices::CreateProxyFromAttributes(System.Type,System.Object[])
extern const MethodInfo ActivationServices_CreateProxyFromAttributes_m10451_MethodInfo = 
{
	"CreateProxyFromAttributes"/* name */
	, (methodPointerType)&ActivationServices_CreateProxyFromAttributes_m10451/* method */
	, &ActivationServices_t1950_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, ActivationServices_t1950_ActivationServices_CreateProxyFromAttributes_m10451_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3668/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType ObjectU5BU5D_t224_0_0_0;
static const ParameterInfo ActivationServices_t1950_ActivationServices_CreateConstructionCall_m10452_ParameterInfos[] = 
{
	{"type", 0, 134222254, 0, &Type_t_0_0_0},
	{"activationUrl", 1, 134222255, 0, &String_t_0_0_0},
	{"activationAttributes", 2, 134222256, 0, &ObjectU5BU5D_t224_0_0_0},
};
extern const Il2CppType ConstructionCall_t1980_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Messaging.ConstructionCall System.Runtime.Remoting.Activation.ActivationServices::CreateConstructionCall(System.Type,System.String,System.Object[])
extern const MethodInfo ActivationServices_CreateConstructionCall_m10452_MethodInfo = 
{
	"CreateConstructionCall"/* name */
	, (methodPointerType)&ActivationServices_CreateConstructionCall_m10452/* method */
	, &ActivationServices_t1950_il2cpp_TypeInfo/* declaring_type */
	, &ConstructionCall_t1980_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, ActivationServices_t1950_ActivationServices_CreateConstructionCall_m10452_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3669/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo ActivationServices_t1950_ActivationServices_AllocateUninitializedClassInstance_m10453_ParameterInfos[] = 
{
	{"type", 0, 134222257, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Activation.ActivationServices::AllocateUninitializedClassInstance(System.Type)
extern const MethodInfo ActivationServices_AllocateUninitializedClassInstance_m10453_MethodInfo = 
{
	"AllocateUninitializedClassInstance"/* name */
	, (methodPointerType)&ActivationServices_AllocateUninitializedClassInstance_m10453/* method */
	, &ActivationServices_t1950_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ActivationServices_t1950_ActivationServices_AllocateUninitializedClassInstance_m10453_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3670/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ActivationServices_t1950_MethodInfos[] =
{
	&ActivationServices_get_ConstructionActivator_m10450_MethodInfo,
	&ActivationServices_CreateProxyFromAttributes_m10451_MethodInfo,
	&ActivationServices_CreateConstructionCall_m10452_MethodInfo,
	&ActivationServices_AllocateUninitializedClassInstance_m10453_MethodInfo,
	NULL
};
extern const MethodInfo ActivationServices_get_ConstructionActivator_m10450_MethodInfo;
static const PropertyInfo ActivationServices_t1950____ConstructionActivator_PropertyInfo = 
{
	&ActivationServices_t1950_il2cpp_TypeInfo/* parent */
	, "ConstructionActivator"/* name */
	, &ActivationServices_get_ConstructionActivator_m10450_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ActivationServices_t1950_PropertyInfos[] =
{
	&ActivationServices_t1950____ConstructionActivator_PropertyInfo,
	NULL
};
static const Il2CppMethodReference ActivationServices_t1950_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool ActivationServices_t1950_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ActivationServices_t1950_0_0_0;
extern const Il2CppType ActivationServices_t1950_1_0_0;
struct ActivationServices_t1950;
const Il2CppTypeDefinitionMetadata ActivationServices_t1950_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ActivationServices_t1950_VTable/* vtableMethods */
	, ActivationServices_t1950_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1903/* fieldStart */

};
TypeInfo ActivationServices_t1950_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ActivationServices"/* name */
	, "System.Runtime.Remoting.Activation"/* namespaze */
	, ActivationServices_t1950_MethodInfos/* methods */
	, ActivationServices_t1950_PropertyInfos/* properties */
	, NULL/* events */
	, &ActivationServices_t1950_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ActivationServices_t1950_0_0_0/* byval_arg */
	, &ActivationServices_t1950_1_0_0/* this_arg */
	, &ActivationServices_t1950_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ActivationServices_t1950)/* instance_size */
	, sizeof (ActivationServices_t1950)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ActivationServices_t1950_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Activation.AppDomainLevelActivator
#include "mscorlib_System_Runtime_Remoting_Activation_AppDomainLevelAc.h"
// Metadata Definition System.Runtime.Remoting.Activation.AppDomainLevelActivator
extern TypeInfo AppDomainLevelActivator_t1951_il2cpp_TypeInfo;
// System.Runtime.Remoting.Activation.AppDomainLevelActivator
#include "mscorlib_System_Runtime_Remoting_Activation_AppDomainLevelAcMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType IActivator_t1949_0_0_0;
static const ParameterInfo AppDomainLevelActivator_t1951_AppDomainLevelActivator__ctor_m10454_ParameterInfos[] = 
{
	{"activationUrl", 0, 134222258, 0, &String_t_0_0_0},
	{"next", 1, 134222259, 0, &IActivator_t1949_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Activation.AppDomainLevelActivator::.ctor(System.String,System.Runtime.Remoting.Activation.IActivator)
extern const MethodInfo AppDomainLevelActivator__ctor_m10454_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AppDomainLevelActivator__ctor_m10454/* method */
	, &AppDomainLevelActivator_t1951_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, AppDomainLevelActivator_t1951_AppDomainLevelActivator__ctor_m10454_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3671/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AppDomainLevelActivator_t1951_MethodInfos[] =
{
	&AppDomainLevelActivator__ctor_m10454_MethodInfo,
	NULL
};
static const Il2CppMethodReference AppDomainLevelActivator_t1951_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool AppDomainLevelActivator_t1951_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static const Il2CppType* AppDomainLevelActivator_t1951_InterfacesTypeInfos[] = 
{
	&IActivator_t1949_0_0_0,
};
static Il2CppInterfaceOffsetPair AppDomainLevelActivator_t1951_InterfacesOffsets[] = 
{
	{ &IActivator_t1949_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AppDomainLevelActivator_t1951_0_0_0;
extern const Il2CppType AppDomainLevelActivator_t1951_1_0_0;
struct AppDomainLevelActivator_t1951;
const Il2CppTypeDefinitionMetadata AppDomainLevelActivator_t1951_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, AppDomainLevelActivator_t1951_InterfacesTypeInfos/* implementedInterfaces */
	, AppDomainLevelActivator_t1951_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, AppDomainLevelActivator_t1951_VTable/* vtableMethods */
	, AppDomainLevelActivator_t1951_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1904/* fieldStart */

};
TypeInfo AppDomainLevelActivator_t1951_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AppDomainLevelActivator"/* name */
	, "System.Runtime.Remoting.Activation"/* namespaze */
	, AppDomainLevelActivator_t1951_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &AppDomainLevelActivator_t1951_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AppDomainLevelActivator_t1951_0_0_0/* byval_arg */
	, &AppDomainLevelActivator_t1951_1_0_0/* this_arg */
	, &AppDomainLevelActivator_t1951_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AppDomainLevelActivator_t1951)/* instance_size */
	, sizeof (AppDomainLevelActivator_t1951)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.Activation.ConstructionLevelActivator
#include "mscorlib_System_Runtime_Remoting_Activation_ConstructionLeve.h"
// Metadata Definition System.Runtime.Remoting.Activation.ConstructionLevelActivator
extern TypeInfo ConstructionLevelActivator_t1952_il2cpp_TypeInfo;
// System.Runtime.Remoting.Activation.ConstructionLevelActivator
#include "mscorlib_System_Runtime_Remoting_Activation_ConstructionLeveMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Activation.ConstructionLevelActivator::.ctor()
extern const MethodInfo ConstructionLevelActivator__ctor_m10455_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ConstructionLevelActivator__ctor_m10455/* method */
	, &ConstructionLevelActivator_t1952_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3672/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ConstructionLevelActivator_t1952_MethodInfos[] =
{
	&ConstructionLevelActivator__ctor_m10455_MethodInfo,
	NULL
};
static const Il2CppMethodReference ConstructionLevelActivator_t1952_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool ConstructionLevelActivator_t1952_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static const Il2CppType* ConstructionLevelActivator_t1952_InterfacesTypeInfos[] = 
{
	&IActivator_t1949_0_0_0,
};
static Il2CppInterfaceOffsetPair ConstructionLevelActivator_t1952_InterfacesOffsets[] = 
{
	{ &IActivator_t1949_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ConstructionLevelActivator_t1952_0_0_0;
extern const Il2CppType ConstructionLevelActivator_t1952_1_0_0;
struct ConstructionLevelActivator_t1952;
const Il2CppTypeDefinitionMetadata ConstructionLevelActivator_t1952_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ConstructionLevelActivator_t1952_InterfacesTypeInfos/* implementedInterfaces */
	, ConstructionLevelActivator_t1952_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ConstructionLevelActivator_t1952_VTable/* vtableMethods */
	, ConstructionLevelActivator_t1952_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ConstructionLevelActivator_t1952_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ConstructionLevelActivator"/* name */
	, "System.Runtime.Remoting.Activation"/* namespaze */
	, ConstructionLevelActivator_t1952_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ConstructionLevelActivator_t1952_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ConstructionLevelActivator_t1952_0_0_0/* byval_arg */
	, &ConstructionLevelActivator_t1952_1_0_0/* this_arg */
	, &ConstructionLevelActivator_t1952_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ConstructionLevelActivator_t1952)/* instance_size */
	, sizeof (ConstructionLevelActivator_t1952)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.Activation.ContextLevelActivator
#include "mscorlib_System_Runtime_Remoting_Activation_ContextLevelActi.h"
// Metadata Definition System.Runtime.Remoting.Activation.ContextLevelActivator
extern TypeInfo ContextLevelActivator_t1953_il2cpp_TypeInfo;
// System.Runtime.Remoting.Activation.ContextLevelActivator
#include "mscorlib_System_Runtime_Remoting_Activation_ContextLevelActiMethodDeclarations.h"
extern const Il2CppType IActivator_t1949_0_0_0;
static const ParameterInfo ContextLevelActivator_t1953_ContextLevelActivator__ctor_m10456_ParameterInfos[] = 
{
	{"next", 0, 134222260, 0, &IActivator_t1949_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Activation.ContextLevelActivator::.ctor(System.Runtime.Remoting.Activation.IActivator)
extern const MethodInfo ContextLevelActivator__ctor_m10456_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ContextLevelActivator__ctor_m10456/* method */
	, &ContextLevelActivator_t1953_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, ContextLevelActivator_t1953_ContextLevelActivator__ctor_m10456_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3673/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ContextLevelActivator_t1953_MethodInfos[] =
{
	&ContextLevelActivator__ctor_m10456_MethodInfo,
	NULL
};
static const Il2CppMethodReference ContextLevelActivator_t1953_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool ContextLevelActivator_t1953_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static const Il2CppType* ContextLevelActivator_t1953_InterfacesTypeInfos[] = 
{
	&IActivator_t1949_0_0_0,
};
static Il2CppInterfaceOffsetPair ContextLevelActivator_t1953_InterfacesOffsets[] = 
{
	{ &IActivator_t1949_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ContextLevelActivator_t1953_0_0_0;
extern const Il2CppType ContextLevelActivator_t1953_1_0_0;
struct ContextLevelActivator_t1953;
const Il2CppTypeDefinitionMetadata ContextLevelActivator_t1953_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ContextLevelActivator_t1953_InterfacesTypeInfos/* implementedInterfaces */
	, ContextLevelActivator_t1953_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ContextLevelActivator_t1953_VTable/* vtableMethods */
	, ContextLevelActivator_t1953_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1906/* fieldStart */

};
TypeInfo ContextLevelActivator_t1953_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ContextLevelActivator"/* name */
	, "System.Runtime.Remoting.Activation"/* namespaze */
	, ContextLevelActivator_t1953_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ContextLevelActivator_t1953_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ContextLevelActivator_t1953_0_0_0/* byval_arg */
	, &ContextLevelActivator_t1953_1_0_0/* this_arg */
	, &ContextLevelActivator_t1953_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ContextLevelActivator_t1953)/* instance_size */
	, sizeof (ContextLevelActivator_t1953)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Activation.IActivator
extern TypeInfo IActivator_t1949_il2cpp_TypeInfo;
static const MethodInfo* IActivator_t1949_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IActivator_t1949_1_0_0;
struct IActivator_t1949;
const Il2CppTypeDefinitionMetadata IActivator_t1949_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IActivator_t1949_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IActivator"/* name */
	, "System.Runtime.Remoting.Activation"/* namespaze */
	, IActivator_t1949_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IActivator_t1949_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 542/* custom_attributes_cache */
	, &IActivator_t1949_0_0_0/* byval_arg */
	, &IActivator_t1949_1_0_0/* this_arg */
	, &IActivator_t1949_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Activation.IConstructionCallMessage
extern TypeInfo IConstructionCallMessage_t2284_il2cpp_TypeInfo;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Runtime.Remoting.Activation.IConstructionCallMessage::get_ActivationType()
extern const MethodInfo IConstructionCallMessage_get_ActivationType_m13185_MethodInfo = 
{
	"get_ActivationType"/* name */
	, NULL/* method */
	, &IConstructionCallMessage_t2284_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3674/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Activation.IConstructionCallMessage::get_ActivationTypeName()
extern const MethodInfo IConstructionCallMessage_get_ActivationTypeName_m13186_MethodInfo = 
{
	"get_ActivationTypeName"/* name */
	, NULL/* method */
	, &IConstructionCallMessage_t2284_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3675/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Activation.IActivator System.Runtime.Remoting.Activation.IConstructionCallMessage::get_Activator()
extern const MethodInfo IConstructionCallMessage_get_Activator_m13187_MethodInfo = 
{
	"get_Activator"/* name */
	, NULL/* method */
	, &IConstructionCallMessage_t2284_il2cpp_TypeInfo/* declaring_type */
	, &IActivator_t1949_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3676/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IActivator_t1949_0_0_0;
static const ParameterInfo IConstructionCallMessage_t2284_IConstructionCallMessage_set_Activator_m13188_ParameterInfos[] = 
{
	{"value", 0, 134222261, 0, &IActivator_t1949_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Activation.IConstructionCallMessage::set_Activator(System.Runtime.Remoting.Activation.IActivator)
extern const MethodInfo IConstructionCallMessage_set_Activator_m13188_MethodInfo = 
{
	"set_Activator"/* name */
	, NULL/* method */
	, &IConstructionCallMessage_t2284_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, IConstructionCallMessage_t2284_IConstructionCallMessage_set_Activator_m13188_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3677/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Runtime.Remoting.Activation.IConstructionCallMessage::get_CallSiteActivationAttributes()
extern const MethodInfo IConstructionCallMessage_get_CallSiteActivationAttributes_m13189_MethodInfo = 
{
	"get_CallSiteActivationAttributes"/* name */
	, NULL/* method */
	, &IConstructionCallMessage_t2284_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t224_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3678/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IList_t1195_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.IList System.Runtime.Remoting.Activation.IConstructionCallMessage::get_ContextProperties()
extern const MethodInfo IConstructionCallMessage_get_ContextProperties_m13190_MethodInfo = 
{
	"get_ContextProperties"/* name */
	, NULL/* method */
	, &IConstructionCallMessage_t2284_il2cpp_TypeInfo/* declaring_type */
	, &IList_t1195_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3679/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IConstructionCallMessage_t2284_MethodInfos[] =
{
	&IConstructionCallMessage_get_ActivationType_m13185_MethodInfo,
	&IConstructionCallMessage_get_ActivationTypeName_m13186_MethodInfo,
	&IConstructionCallMessage_get_Activator_m13187_MethodInfo,
	&IConstructionCallMessage_set_Activator_m13188_MethodInfo,
	&IConstructionCallMessage_get_CallSiteActivationAttributes_m13189_MethodInfo,
	&IConstructionCallMessage_get_ContextProperties_m13190_MethodInfo,
	NULL
};
extern const MethodInfo IConstructionCallMessage_get_ActivationType_m13185_MethodInfo;
static const PropertyInfo IConstructionCallMessage_t2284____ActivationType_PropertyInfo = 
{
	&IConstructionCallMessage_t2284_il2cpp_TypeInfo/* parent */
	, "ActivationType"/* name */
	, &IConstructionCallMessage_get_ActivationType_m13185_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo IConstructionCallMessage_get_ActivationTypeName_m13186_MethodInfo;
static const PropertyInfo IConstructionCallMessage_t2284____ActivationTypeName_PropertyInfo = 
{
	&IConstructionCallMessage_t2284_il2cpp_TypeInfo/* parent */
	, "ActivationTypeName"/* name */
	, &IConstructionCallMessage_get_ActivationTypeName_m13186_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo IConstructionCallMessage_get_Activator_m13187_MethodInfo;
extern const MethodInfo IConstructionCallMessage_set_Activator_m13188_MethodInfo;
static const PropertyInfo IConstructionCallMessage_t2284____Activator_PropertyInfo = 
{
	&IConstructionCallMessage_t2284_il2cpp_TypeInfo/* parent */
	, "Activator"/* name */
	, &IConstructionCallMessage_get_Activator_m13187_MethodInfo/* get */
	, &IConstructionCallMessage_set_Activator_m13188_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo IConstructionCallMessage_get_CallSiteActivationAttributes_m13189_MethodInfo;
static const PropertyInfo IConstructionCallMessage_t2284____CallSiteActivationAttributes_PropertyInfo = 
{
	&IConstructionCallMessage_t2284_il2cpp_TypeInfo/* parent */
	, "CallSiteActivationAttributes"/* name */
	, &IConstructionCallMessage_get_CallSiteActivationAttributes_m13189_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo IConstructionCallMessage_get_ContextProperties_m13190_MethodInfo;
static const PropertyInfo IConstructionCallMessage_t2284____ContextProperties_PropertyInfo = 
{
	&IConstructionCallMessage_t2284_il2cpp_TypeInfo/* parent */
	, "ContextProperties"/* name */
	, &IConstructionCallMessage_get_ContextProperties_m13190_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* IConstructionCallMessage_t2284_PropertyInfos[] =
{
	&IConstructionCallMessage_t2284____ActivationType_PropertyInfo,
	&IConstructionCallMessage_t2284____ActivationTypeName_PropertyInfo,
	&IConstructionCallMessage_t2284____Activator_PropertyInfo,
	&IConstructionCallMessage_t2284____CallSiteActivationAttributes_PropertyInfo,
	&IConstructionCallMessage_t2284____ContextProperties_PropertyInfo,
	NULL
};
extern const Il2CppType IMessage_t1978_0_0_0;
extern const Il2CppType IMethodCallMessage_t2289_0_0_0;
extern const Il2CppType IMethodMessage_t1990_0_0_0;
static const Il2CppType* IConstructionCallMessage_t2284_InterfacesTypeInfos[] = 
{
	&IMessage_t1978_0_0_0,
	&IMethodCallMessage_t2289_0_0_0,
	&IMethodMessage_t1990_0_0_0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IConstructionCallMessage_t2284_0_0_0;
extern const Il2CppType IConstructionCallMessage_t2284_1_0_0;
struct IConstructionCallMessage_t2284;
const Il2CppTypeDefinitionMetadata IConstructionCallMessage_t2284_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IConstructionCallMessage_t2284_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IConstructionCallMessage_t2284_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IConstructionCallMessage"/* name */
	, "System.Runtime.Remoting.Activation"/* namespaze */
	, IConstructionCallMessage_t2284_MethodInfos/* methods */
	, IConstructionCallMessage_t2284_PropertyInfos/* properties */
	, NULL/* events */
	, &IConstructionCallMessage_t2284_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 543/* custom_attributes_cache */
	, &IConstructionCallMessage_t2284_0_0_0/* byval_arg */
	, &IConstructionCallMessage_t2284_1_0_0/* this_arg */
	, &IConstructionCallMessage_t2284_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 5/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Activation.RemoteActivator
#include "mscorlib_System_Runtime_Remoting_Activation_RemoteActivator.h"
// Metadata Definition System.Runtime.Remoting.Activation.RemoteActivator
extern TypeInfo RemoteActivator_t1954_il2cpp_TypeInfo;
// System.Runtime.Remoting.Activation.RemoteActivator
#include "mscorlib_System_Runtime_Remoting_Activation_RemoteActivatorMethodDeclarations.h"
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Activation.RemoteActivator::InitializeLifetimeService()
extern const MethodInfo RemoteActivator_InitializeLifetimeService_m10457_MethodInfo = 
{
	"InitializeLifetimeService"/* name */
	, (methodPointerType)&RemoteActivator_InitializeLifetimeService_m10457/* method */
	, &RemoteActivator_t1954_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3680/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* RemoteActivator_t1954_MethodInfos[] =
{
	&RemoteActivator_InitializeLifetimeService_m10457_MethodInfo,
	NULL
};
extern const MethodInfo MarshalByRefObject_CreateObjRef_m6614_MethodInfo;
extern const MethodInfo RemoteActivator_InitializeLifetimeService_m10457_MethodInfo;
static const Il2CppMethodReference RemoteActivator_t1954_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&MarshalByRefObject_CreateObjRef_m6614_MethodInfo,
	&RemoteActivator_InitializeLifetimeService_m10457_MethodInfo,
};
static bool RemoteActivator_t1954_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* RemoteActivator_t1954_InterfacesTypeInfos[] = 
{
	&IActivator_t1949_0_0_0,
};
static Il2CppInterfaceOffsetPair RemoteActivator_t1954_InterfacesOffsets[] = 
{
	{ &IActivator_t1949_0_0_0, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RemoteActivator_t1954_0_0_0;
extern const Il2CppType RemoteActivator_t1954_1_0_0;
extern const Il2CppType MarshalByRefObject_t1308_0_0_0;
struct RemoteActivator_t1954;
const Il2CppTypeDefinitionMetadata RemoteActivator_t1954_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, RemoteActivator_t1954_InterfacesTypeInfos/* implementedInterfaces */
	, RemoteActivator_t1954_InterfacesOffsets/* interfaceOffsets */
	, &MarshalByRefObject_t1308_0_0_0/* parent */
	, RemoteActivator_t1954_VTable/* vtableMethods */
	, RemoteActivator_t1954_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo RemoteActivator_t1954_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RemoteActivator"/* name */
	, "System.Runtime.Remoting.Activation"/* namespaze */
	, RemoteActivator_t1954_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &RemoteActivator_t1954_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RemoteActivator_t1954_0_0_0/* byval_arg */
	, &RemoteActivator_t1954_1_0_0/* this_arg */
	, &RemoteActivator_t1954_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RemoteActivator_t1954)/* instance_size */
	, sizeof (RemoteActivator_t1954)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.Activation.UrlAttribute
#include "mscorlib_System_Runtime_Remoting_Activation_UrlAttribute.h"
// Metadata Definition System.Runtime.Remoting.Activation.UrlAttribute
extern TypeInfo UrlAttribute_t1955_il2cpp_TypeInfo;
// System.Runtime.Remoting.Activation.UrlAttribute
#include "mscorlib_System_Runtime_Remoting_Activation_UrlAttributeMethodDeclarations.h"
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Activation.UrlAttribute::get_UrlValue()
extern const MethodInfo UrlAttribute_get_UrlValue_m10458_MethodInfo = 
{
	"get_UrlValue"/* name */
	, (methodPointerType)&UrlAttribute_get_UrlValue_m10458/* method */
	, &UrlAttribute_t1955_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3681/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo UrlAttribute_t1955_UrlAttribute_Equals_m10459_ParameterInfos[] = 
{
	{"o", 0, 134222262, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.Activation.UrlAttribute::Equals(System.Object)
extern const MethodInfo UrlAttribute_Equals_m10459_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&UrlAttribute_Equals_m10459/* method */
	, &UrlAttribute_t1955_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, UrlAttribute_t1955_UrlAttribute_Equals_m10459_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3682/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Runtime.Remoting.Activation.UrlAttribute::GetHashCode()
extern const MethodInfo UrlAttribute_GetHashCode_m10460_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&UrlAttribute_GetHashCode_m10460/* method */
	, &UrlAttribute_t1955_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3683/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IConstructionCallMessage_t2284_0_0_0;
static const ParameterInfo UrlAttribute_t1955_UrlAttribute_GetPropertiesForNewContext_m10461_ParameterInfos[] = 
{
	{"ctorMsg", 0, 134222263, 0, &IConstructionCallMessage_t2284_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Activation.UrlAttribute::GetPropertiesForNewContext(System.Runtime.Remoting.Activation.IConstructionCallMessage)
extern const MethodInfo UrlAttribute_GetPropertiesForNewContext_m10461_MethodInfo = 
{
	"GetPropertiesForNewContext"/* name */
	, (methodPointerType)&UrlAttribute_GetPropertiesForNewContext_m10461/* method */
	, &UrlAttribute_t1955_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, UrlAttribute_t1955_UrlAttribute_GetPropertiesForNewContext_m10461_ParameterInfos/* parameters */
	, 545/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3684/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Context_t1963_0_0_0;
extern const Il2CppType Context_t1963_0_0_0;
extern const Il2CppType IConstructionCallMessage_t2284_0_0_0;
static const ParameterInfo UrlAttribute_t1955_UrlAttribute_IsContextOK_m10462_ParameterInfos[] = 
{
	{"ctx", 0, 134222264, 0, &Context_t1963_0_0_0},
	{"msg", 1, 134222265, 0, &IConstructionCallMessage_t2284_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.Activation.UrlAttribute::IsContextOK(System.Runtime.Remoting.Contexts.Context,System.Runtime.Remoting.Activation.IConstructionCallMessage)
extern const MethodInfo UrlAttribute_IsContextOK_m10462_MethodInfo = 
{
	"IsContextOK"/* name */
	, (methodPointerType)&UrlAttribute_IsContextOK_m10462/* method */
	, &UrlAttribute_t1955_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t_Object_t/* invoker_method */
	, UrlAttribute_t1955_UrlAttribute_IsContextOK_m10462_ParameterInfos/* parameters */
	, 546/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3685/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UrlAttribute_t1955_MethodInfos[] =
{
	&UrlAttribute_get_UrlValue_m10458_MethodInfo,
	&UrlAttribute_Equals_m10459_MethodInfo,
	&UrlAttribute_GetHashCode_m10460_MethodInfo,
	&UrlAttribute_GetPropertiesForNewContext_m10461_MethodInfo,
	&UrlAttribute_IsContextOK_m10462_MethodInfo,
	NULL
};
extern const MethodInfo UrlAttribute_get_UrlValue_m10458_MethodInfo;
static const PropertyInfo UrlAttribute_t1955____UrlValue_PropertyInfo = 
{
	&UrlAttribute_t1955_il2cpp_TypeInfo/* parent */
	, "UrlValue"/* name */
	, &UrlAttribute_get_UrlValue_m10458_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* UrlAttribute_t1955_PropertyInfos[] =
{
	&UrlAttribute_t1955____UrlValue_PropertyInfo,
	NULL
};
extern const MethodInfo UrlAttribute_Equals_m10459_MethodInfo;
extern const MethodInfo UrlAttribute_GetHashCode_m10460_MethodInfo;
extern const MethodInfo UrlAttribute_GetPropertiesForNewContext_m10461_MethodInfo;
extern const MethodInfo UrlAttribute_IsContextOK_m10462_MethodInfo;
extern const MethodInfo ContextAttribute_get_Name_m10487_MethodInfo;
static const Il2CppMethodReference UrlAttribute_t1955_VTable[] =
{
	&UrlAttribute_Equals_m10459_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&UrlAttribute_GetHashCode_m10460_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&UrlAttribute_GetPropertiesForNewContext_m10461_MethodInfo,
	&UrlAttribute_IsContextOK_m10462_MethodInfo,
	&ContextAttribute_get_Name_m10487_MethodInfo,
	&ContextAttribute_get_Name_m10487_MethodInfo,
	&UrlAttribute_GetPropertiesForNewContext_m10461_MethodInfo,
	&UrlAttribute_IsContextOK_m10462_MethodInfo,
};
static bool UrlAttribute_t1955_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IContextAttribute_t2299_0_0_0;
extern const Il2CppType IContextProperty_t2286_0_0_0;
static Il2CppInterfaceOffsetPair UrlAttribute_t1955_InterfacesOffsets[] = 
{
	{ &IContextAttribute_t2299_0_0_0, 4},
	{ &IContextProperty_t2286_0_0_0, 6},
	{ &_Attribute_t1145_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType UrlAttribute_t1955_0_0_0;
extern const Il2CppType UrlAttribute_t1955_1_0_0;
extern const Il2CppType ContextAttribute_t1956_0_0_0;
struct UrlAttribute_t1955;
const Il2CppTypeDefinitionMetadata UrlAttribute_t1955_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UrlAttribute_t1955_InterfacesOffsets/* interfaceOffsets */
	, &ContextAttribute_t1956_0_0_0/* parent */
	, UrlAttribute_t1955_VTable/* vtableMethods */
	, UrlAttribute_t1955_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1907/* fieldStart */

};
TypeInfo UrlAttribute_t1955_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "UrlAttribute"/* name */
	, "System.Runtime.Remoting.Activation"/* namespaze */
	, UrlAttribute_t1955_MethodInfos/* methods */
	, UrlAttribute_t1955_PropertyInfos/* properties */
	, NULL/* events */
	, &UrlAttribute_t1955_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 544/* custom_attributes_cache */
	, &UrlAttribute_t1955_0_0_0/* byval_arg */
	, &UrlAttribute_t1955_1_0_0/* this_arg */
	, &UrlAttribute_t1955_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UrlAttribute_t1955)/* instance_size */
	, sizeof (UrlAttribute_t1955)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Remoting.ChannelInfo
#include "mscorlib_System_Runtime_Remoting_ChannelInfo.h"
// Metadata Definition System.Runtime.Remoting.ChannelInfo
extern TypeInfo ChannelInfo_t1957_il2cpp_TypeInfo;
// System.Runtime.Remoting.ChannelInfo
#include "mscorlib_System_Runtime_Remoting_ChannelInfoMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.ChannelInfo::.ctor()
extern const MethodInfo ChannelInfo__ctor_m10463_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ChannelInfo__ctor_m10463/* method */
	, &ChannelInfo_t1957_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3686/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Runtime.Remoting.ChannelInfo::get_ChannelData()
extern const MethodInfo ChannelInfo_get_ChannelData_m10464_MethodInfo = 
{
	"get_ChannelData"/* name */
	, (methodPointerType)&ChannelInfo_get_ChannelData_m10464/* method */
	, &ChannelInfo_t1957_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t224_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3687/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ChannelInfo_t1957_MethodInfos[] =
{
	&ChannelInfo__ctor_m10463_MethodInfo,
	&ChannelInfo_get_ChannelData_m10464_MethodInfo,
	NULL
};
extern const MethodInfo ChannelInfo_get_ChannelData_m10464_MethodInfo;
static const PropertyInfo ChannelInfo_t1957____ChannelData_PropertyInfo = 
{
	&ChannelInfo_t1957_il2cpp_TypeInfo/* parent */
	, "ChannelData"/* name */
	, &ChannelInfo_get_ChannelData_m10464_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ChannelInfo_t1957_PropertyInfos[] =
{
	&ChannelInfo_t1957____ChannelData_PropertyInfo,
	NULL
};
static const Il2CppMethodReference ChannelInfo_t1957_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&ChannelInfo_get_ChannelData_m10464_MethodInfo,
};
static bool ChannelInfo_t1957_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IChannelInfo_t2011_0_0_0;
static const Il2CppType* ChannelInfo_t1957_InterfacesTypeInfos[] = 
{
	&IChannelInfo_t2011_0_0_0,
};
static Il2CppInterfaceOffsetPair ChannelInfo_t1957_InterfacesOffsets[] = 
{
	{ &IChannelInfo_t2011_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ChannelInfo_t1957_0_0_0;
extern const Il2CppType ChannelInfo_t1957_1_0_0;
struct ChannelInfo_t1957;
const Il2CppTypeDefinitionMetadata ChannelInfo_t1957_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ChannelInfo_t1957_InterfacesTypeInfos/* implementedInterfaces */
	, ChannelInfo_t1957_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ChannelInfo_t1957_VTable/* vtableMethods */
	, ChannelInfo_t1957_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1908/* fieldStart */

};
TypeInfo ChannelInfo_t1957_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ChannelInfo"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, ChannelInfo_t1957_MethodInfos/* methods */
	, ChannelInfo_t1957_PropertyInfos/* properties */
	, NULL/* events */
	, &ChannelInfo_t1957_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ChannelInfo_t1957_0_0_0/* byval_arg */
	, &ChannelInfo_t1957_1_0_0/* this_arg */
	, &ChannelInfo_t1957_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ChannelInfo_t1957)/* instance_size */
	, sizeof (ChannelInfo_t1957)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.Channels.ChannelServices
#include "mscorlib_System_Runtime_Remoting_Channels_ChannelServices.h"
// Metadata Definition System.Runtime.Remoting.Channels.ChannelServices
extern TypeInfo ChannelServices_t1959_il2cpp_TypeInfo;
// System.Runtime.Remoting.Channels.ChannelServices
#include "mscorlib_System_Runtime_Remoting_Channels_ChannelServicesMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Channels.ChannelServices::.cctor()
extern const MethodInfo ChannelServices__cctor_m10465_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&ChannelServices__cctor_m10465/* method */
	, &ChannelServices_t1959_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3688/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IChannel_t2285_0_0_0;
extern const Il2CppType IChannel_t2285_0_0_0;
static const ParameterInfo ChannelServices_t1959_ChannelServices_RegisterChannel_m10466_ParameterInfos[] = 
{
	{"chnl", 0, 134222266, 0, &IChannel_t2285_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Channels.ChannelServices::RegisterChannel(System.Runtime.Remoting.Channels.IChannel)
extern const MethodInfo ChannelServices_RegisterChannel_m10466_MethodInfo = 
{
	"RegisterChannel"/* name */
	, (methodPointerType)&ChannelServices_RegisterChannel_m10466/* method */
	, &ChannelServices_t1959_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, ChannelServices_t1959_ChannelServices_RegisterChannel_m10466_ParameterInfos/* parameters */
	, 548/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3689/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IChannel_t2285_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo ChannelServices_t1959_ChannelServices_RegisterChannel_m10467_ParameterInfos[] = 
{
	{"chnl", 0, 134222267, 0, &IChannel_t2285_0_0_0},
	{"ensureSecurity", 1, 134222268, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Channels.ChannelServices::RegisterChannel(System.Runtime.Remoting.Channels.IChannel,System.Boolean)
extern const MethodInfo ChannelServices_RegisterChannel_m10467_MethodInfo = 
{
	"RegisterChannel"/* name */
	, (methodPointerType)&ChannelServices_RegisterChannel_m10467/* method */
	, &ChannelServices_t1959_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_SByte_t274/* invoker_method */
	, ChannelServices_t1959_ChannelServices_RegisterChannel_m10467_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3690/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Runtime.Remoting.Channels.ChannelServices::GetCurrentChannelInfo()
extern const MethodInfo ChannelServices_GetCurrentChannelInfo_m10468_MethodInfo = 
{
	"GetCurrentChannelInfo"/* name */
	, (methodPointerType)&ChannelServices_GetCurrentChannelInfo_m10468/* method */
	, &ChannelServices_t1959_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t224_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3691/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ChannelServices_t1959_MethodInfos[] =
{
	&ChannelServices__cctor_m10465_MethodInfo,
	&ChannelServices_RegisterChannel_m10466_MethodInfo,
	&ChannelServices_RegisterChannel_m10467_MethodInfo,
	&ChannelServices_GetCurrentChannelInfo_m10468_MethodInfo,
	NULL
};
static const Il2CppMethodReference ChannelServices_t1959_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool ChannelServices_t1959_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ChannelServices_t1959_0_0_0;
extern const Il2CppType ChannelServices_t1959_1_0_0;
struct ChannelServices_t1959;
const Il2CppTypeDefinitionMetadata ChannelServices_t1959_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ChannelServices_t1959_VTable/* vtableMethods */
	, ChannelServices_t1959_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1909/* fieldStart */

};
TypeInfo ChannelServices_t1959_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ChannelServices"/* name */
	, "System.Runtime.Remoting.Channels"/* namespaze */
	, ChannelServices_t1959_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ChannelServices_t1959_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 547/* custom_attributes_cache */
	, &ChannelServices_t1959_0_0_0/* byval_arg */
	, &ChannelServices_t1959_1_0_0/* this_arg */
	, &ChannelServices_t1959_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ChannelServices_t1959)/* instance_size */
	, sizeof (ChannelServices_t1959)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ChannelServices_t1959_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Channels.CrossAppDomainData
#include "mscorlib_System_Runtime_Remoting_Channels_CrossAppDomainData.h"
// Metadata Definition System.Runtime.Remoting.Channels.CrossAppDomainData
extern TypeInfo CrossAppDomainData_t1960_il2cpp_TypeInfo;
// System.Runtime.Remoting.Channels.CrossAppDomainData
#include "mscorlib_System_Runtime_Remoting_Channels_CrossAppDomainDataMethodDeclarations.h"
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo CrossAppDomainData_t1960_CrossAppDomainData__ctor_m10469_ParameterInfos[] = 
{
	{"domainId", 0, 134222269, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Channels.CrossAppDomainData::.ctor(System.Int32)
extern const MethodInfo CrossAppDomainData__ctor_m10469_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CrossAppDomainData__ctor_m10469/* method */
	, &CrossAppDomainData_t1960_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, CrossAppDomainData_t1960_CrossAppDomainData__ctor_m10469_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3692/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CrossAppDomainData_t1960_MethodInfos[] =
{
	&CrossAppDomainData__ctor_m10469_MethodInfo,
	NULL
};
static const Il2CppMethodReference CrossAppDomainData_t1960_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool CrossAppDomainData_t1960_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CrossAppDomainData_t1960_0_0_0;
extern const Il2CppType CrossAppDomainData_t1960_1_0_0;
struct CrossAppDomainData_t1960;
const Il2CppTypeDefinitionMetadata CrossAppDomainData_t1960_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CrossAppDomainData_t1960_VTable/* vtableMethods */
	, CrossAppDomainData_t1960_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1914/* fieldStart */

};
TypeInfo CrossAppDomainData_t1960_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CrossAppDomainData"/* name */
	, "System.Runtime.Remoting.Channels"/* namespaze */
	, CrossAppDomainData_t1960_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CrossAppDomainData_t1960_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CrossAppDomainData_t1960_0_0_0/* byval_arg */
	, &CrossAppDomainData_t1960_1_0_0/* this_arg */
	, &CrossAppDomainData_t1960_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CrossAppDomainData_t1960)/* instance_size */
	, sizeof (CrossAppDomainData_t1960)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Channels.CrossAppDomainChannel
#include "mscorlib_System_Runtime_Remoting_Channels_CrossAppDomainChan.h"
// Metadata Definition System.Runtime.Remoting.Channels.CrossAppDomainChannel
extern TypeInfo CrossAppDomainChannel_t1961_il2cpp_TypeInfo;
// System.Runtime.Remoting.Channels.CrossAppDomainChannel
#include "mscorlib_System_Runtime_Remoting_Channels_CrossAppDomainChanMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Channels.CrossAppDomainChannel::.ctor()
extern const MethodInfo CrossAppDomainChannel__ctor_m10470_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CrossAppDomainChannel__ctor_m10470/* method */
	, &CrossAppDomainChannel_t1961_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3693/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Channels.CrossAppDomainChannel::.cctor()
extern const MethodInfo CrossAppDomainChannel__cctor_m10471_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&CrossAppDomainChannel__cctor_m10471/* method */
	, &CrossAppDomainChannel_t1961_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3694/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Channels.CrossAppDomainChannel::RegisterCrossAppDomainChannel()
extern const MethodInfo CrossAppDomainChannel_RegisterCrossAppDomainChannel_m10472_MethodInfo = 
{
	"RegisterCrossAppDomainChannel"/* name */
	, (methodPointerType)&CrossAppDomainChannel_RegisterCrossAppDomainChannel_m10472/* method */
	, &CrossAppDomainChannel_t1961_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3695/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Channels.CrossAppDomainChannel::get_ChannelName()
extern const MethodInfo CrossAppDomainChannel_get_ChannelName_m10473_MethodInfo = 
{
	"get_ChannelName"/* name */
	, (methodPointerType)&CrossAppDomainChannel_get_ChannelName_m10473/* method */
	, &CrossAppDomainChannel_t1961_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3696/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Runtime.Remoting.Channels.CrossAppDomainChannel::get_ChannelPriority()
extern const MethodInfo CrossAppDomainChannel_get_ChannelPriority_m10474_MethodInfo = 
{
	"get_ChannelPriority"/* name */
	, (methodPointerType)&CrossAppDomainChannel_get_ChannelPriority_m10474/* method */
	, &CrossAppDomainChannel_t1961_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3697/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Channels.CrossAppDomainChannel::get_ChannelData()
extern const MethodInfo CrossAppDomainChannel_get_ChannelData_m10475_MethodInfo = 
{
	"get_ChannelData"/* name */
	, (methodPointerType)&CrossAppDomainChannel_get_ChannelData_m10475/* method */
	, &CrossAppDomainChannel_t1961_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3698/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo CrossAppDomainChannel_t1961_CrossAppDomainChannel_StartListening_m10476_ParameterInfos[] = 
{
	{"data", 0, 134222270, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Channels.CrossAppDomainChannel::StartListening(System.Object)
extern const MethodInfo CrossAppDomainChannel_StartListening_m10476_MethodInfo = 
{
	"StartListening"/* name */
	, (methodPointerType)&CrossAppDomainChannel_StartListening_m10476/* method */
	, &CrossAppDomainChannel_t1961_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, CrossAppDomainChannel_t1961_CrossAppDomainChannel_StartListening_m10476_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3699/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CrossAppDomainChannel_t1961_MethodInfos[] =
{
	&CrossAppDomainChannel__ctor_m10470_MethodInfo,
	&CrossAppDomainChannel__cctor_m10471_MethodInfo,
	&CrossAppDomainChannel_RegisterCrossAppDomainChannel_m10472_MethodInfo,
	&CrossAppDomainChannel_get_ChannelName_m10473_MethodInfo,
	&CrossAppDomainChannel_get_ChannelPriority_m10474_MethodInfo,
	&CrossAppDomainChannel_get_ChannelData_m10475_MethodInfo,
	&CrossAppDomainChannel_StartListening_m10476_MethodInfo,
	NULL
};
extern const MethodInfo CrossAppDomainChannel_get_ChannelName_m10473_MethodInfo;
static const PropertyInfo CrossAppDomainChannel_t1961____ChannelName_PropertyInfo = 
{
	&CrossAppDomainChannel_t1961_il2cpp_TypeInfo/* parent */
	, "ChannelName"/* name */
	, &CrossAppDomainChannel_get_ChannelName_m10473_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo CrossAppDomainChannel_get_ChannelPriority_m10474_MethodInfo;
static const PropertyInfo CrossAppDomainChannel_t1961____ChannelPriority_PropertyInfo = 
{
	&CrossAppDomainChannel_t1961_il2cpp_TypeInfo/* parent */
	, "ChannelPriority"/* name */
	, &CrossAppDomainChannel_get_ChannelPriority_m10474_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo CrossAppDomainChannel_get_ChannelData_m10475_MethodInfo;
static const PropertyInfo CrossAppDomainChannel_t1961____ChannelData_PropertyInfo = 
{
	&CrossAppDomainChannel_t1961_il2cpp_TypeInfo/* parent */
	, "ChannelData"/* name */
	, &CrossAppDomainChannel_get_ChannelData_m10475_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* CrossAppDomainChannel_t1961_PropertyInfos[] =
{
	&CrossAppDomainChannel_t1961____ChannelName_PropertyInfo,
	&CrossAppDomainChannel_t1961____ChannelPriority_PropertyInfo,
	&CrossAppDomainChannel_t1961____ChannelData_PropertyInfo,
	NULL
};
extern const MethodInfo CrossAppDomainChannel_StartListening_m10476_MethodInfo;
static const Il2CppMethodReference CrossAppDomainChannel_t1961_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&CrossAppDomainChannel_get_ChannelName_m10473_MethodInfo,
	&CrossAppDomainChannel_get_ChannelPriority_m10474_MethodInfo,
	&CrossAppDomainChannel_get_ChannelData_m10475_MethodInfo,
	&CrossAppDomainChannel_StartListening_m10476_MethodInfo,
	&CrossAppDomainChannel_get_ChannelName_m10473_MethodInfo,
	&CrossAppDomainChannel_get_ChannelPriority_m10474_MethodInfo,
	&CrossAppDomainChannel_get_ChannelData_m10475_MethodInfo,
	&CrossAppDomainChannel_StartListening_m10476_MethodInfo,
};
static bool CrossAppDomainChannel_t1961_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IChannelReceiver_t2301_0_0_0;
extern const Il2CppType IChannelSender_t2380_0_0_0;
static const Il2CppType* CrossAppDomainChannel_t1961_InterfacesTypeInfos[] = 
{
	&IChannel_t2285_0_0_0,
	&IChannelReceiver_t2301_0_0_0,
	&IChannelSender_t2380_0_0_0,
};
static Il2CppInterfaceOffsetPair CrossAppDomainChannel_t1961_InterfacesOffsets[] = 
{
	{ &IChannel_t2285_0_0_0, 4},
	{ &IChannelReceiver_t2301_0_0_0, 6},
	{ &IChannelSender_t2380_0_0_0, 8},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CrossAppDomainChannel_t1961_0_0_0;
extern const Il2CppType CrossAppDomainChannel_t1961_1_0_0;
struct CrossAppDomainChannel_t1961;
const Il2CppTypeDefinitionMetadata CrossAppDomainChannel_t1961_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, CrossAppDomainChannel_t1961_InterfacesTypeInfos/* implementedInterfaces */
	, CrossAppDomainChannel_t1961_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CrossAppDomainChannel_t1961_VTable/* vtableMethods */
	, CrossAppDomainChannel_t1961_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1917/* fieldStart */

};
TypeInfo CrossAppDomainChannel_t1961_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CrossAppDomainChannel"/* name */
	, "System.Runtime.Remoting.Channels"/* namespaze */
	, CrossAppDomainChannel_t1961_MethodInfos/* methods */
	, CrossAppDomainChannel_t1961_PropertyInfos/* properties */
	, NULL/* events */
	, &CrossAppDomainChannel_t1961_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CrossAppDomainChannel_t1961_0_0_0/* byval_arg */
	, &CrossAppDomainChannel_t1961_1_0_0/* this_arg */
	, &CrossAppDomainChannel_t1961_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CrossAppDomainChannel_t1961)/* instance_size */
	, sizeof (CrossAppDomainChannel_t1961)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CrossAppDomainChannel_t1961_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 3/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 12/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Remoting.Channels.CrossAppDomainSink
#include "mscorlib_System_Runtime_Remoting_Channels_CrossAppDomainSink.h"
// Metadata Definition System.Runtime.Remoting.Channels.CrossAppDomainSink
extern TypeInfo CrossAppDomainSink_t1962_il2cpp_TypeInfo;
// System.Runtime.Remoting.Channels.CrossAppDomainSink
#include "mscorlib_System_Runtime_Remoting_Channels_CrossAppDomainSinkMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Channels.CrossAppDomainSink::.cctor()
extern const MethodInfo CrossAppDomainSink__cctor_m10477_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&CrossAppDomainSink__cctor_m10477/* method */
	, &CrossAppDomainSink_t1962_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3700/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Runtime.Remoting.Channels.CrossAppDomainSink::get_TargetDomainId()
extern const MethodInfo CrossAppDomainSink_get_TargetDomainId_m10478_MethodInfo = 
{
	"get_TargetDomainId"/* name */
	, (methodPointerType)&CrossAppDomainSink_get_TargetDomainId_m10478/* method */
	, &CrossAppDomainSink_t1962_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2179/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3701/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CrossAppDomainSink_t1962_MethodInfos[] =
{
	&CrossAppDomainSink__cctor_m10477_MethodInfo,
	&CrossAppDomainSink_get_TargetDomainId_m10478_MethodInfo,
	NULL
};
extern const MethodInfo CrossAppDomainSink_get_TargetDomainId_m10478_MethodInfo;
static const PropertyInfo CrossAppDomainSink_t1962____TargetDomainId_PropertyInfo = 
{
	&CrossAppDomainSink_t1962_il2cpp_TypeInfo/* parent */
	, "TargetDomainId"/* name */
	, &CrossAppDomainSink_get_TargetDomainId_m10478_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* CrossAppDomainSink_t1962_PropertyInfos[] =
{
	&CrossAppDomainSink_t1962____TargetDomainId_PropertyInfo,
	NULL
};
static const Il2CppMethodReference CrossAppDomainSink_t1962_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool CrossAppDomainSink_t1962_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern const Il2CppType IMessageSink_t1516_0_0_0;
static const Il2CppType* CrossAppDomainSink_t1962_InterfacesTypeInfos[] = 
{
	&IMessageSink_t1516_0_0_0,
};
static Il2CppInterfaceOffsetPair CrossAppDomainSink_t1962_InterfacesOffsets[] = 
{
	{ &IMessageSink_t1516_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CrossAppDomainSink_t1962_0_0_0;
extern const Il2CppType CrossAppDomainSink_t1962_1_0_0;
struct CrossAppDomainSink_t1962;
const Il2CppTypeDefinitionMetadata CrossAppDomainSink_t1962_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, CrossAppDomainSink_t1962_InterfacesTypeInfos/* implementedInterfaces */
	, CrossAppDomainSink_t1962_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CrossAppDomainSink_t1962_VTable/* vtableMethods */
	, CrossAppDomainSink_t1962_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1918/* fieldStart */

};
TypeInfo CrossAppDomainSink_t1962_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CrossAppDomainSink"/* name */
	, "System.Runtime.Remoting.Channels"/* namespaze */
	, CrossAppDomainSink_t1962_MethodInfos/* methods */
	, CrossAppDomainSink_t1962_PropertyInfos/* properties */
	, NULL/* events */
	, &CrossAppDomainSink_t1962_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 549/* custom_attributes_cache */
	, &CrossAppDomainSink_t1962_0_0_0/* byval_arg */
	, &CrossAppDomainSink_t1962_1_0_0/* this_arg */
	, &CrossAppDomainSink_t1962_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CrossAppDomainSink_t1962)/* instance_size */
	, sizeof (CrossAppDomainSink_t1962)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CrossAppDomainSink_t1962_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Channels.IChannel
extern TypeInfo IChannel_t2285_il2cpp_TypeInfo;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Channels.IChannel::get_ChannelName()
extern const MethodInfo IChannel_get_ChannelName_m13191_MethodInfo = 
{
	"get_ChannelName"/* name */
	, NULL/* method */
	, &IChannel_t2285_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3702/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Runtime.Remoting.Channels.IChannel::get_ChannelPriority()
extern const MethodInfo IChannel_get_ChannelPriority_m13192_MethodInfo = 
{
	"get_ChannelPriority"/* name */
	, NULL/* method */
	, &IChannel_t2285_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3703/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IChannel_t2285_MethodInfos[] =
{
	&IChannel_get_ChannelName_m13191_MethodInfo,
	&IChannel_get_ChannelPriority_m13192_MethodInfo,
	NULL
};
extern const MethodInfo IChannel_get_ChannelName_m13191_MethodInfo;
static const PropertyInfo IChannel_t2285____ChannelName_PropertyInfo = 
{
	&IChannel_t2285_il2cpp_TypeInfo/* parent */
	, "ChannelName"/* name */
	, &IChannel_get_ChannelName_m13191_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo IChannel_get_ChannelPriority_m13192_MethodInfo;
static const PropertyInfo IChannel_t2285____ChannelPriority_PropertyInfo = 
{
	&IChannel_t2285_il2cpp_TypeInfo/* parent */
	, "ChannelPriority"/* name */
	, &IChannel_get_ChannelPriority_m13192_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* IChannel_t2285_PropertyInfos[] =
{
	&IChannel_t2285____ChannelName_PropertyInfo,
	&IChannel_t2285____ChannelPriority_PropertyInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IChannel_t2285_1_0_0;
struct IChannel_t2285;
const Il2CppTypeDefinitionMetadata IChannel_t2285_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IChannel_t2285_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IChannel"/* name */
	, "System.Runtime.Remoting.Channels"/* namespaze */
	, IChannel_t2285_MethodInfos/* methods */
	, IChannel_t2285_PropertyInfos/* properties */
	, NULL/* events */
	, &IChannel_t2285_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 550/* custom_attributes_cache */
	, &IChannel_t2285_0_0_0/* byval_arg */
	, &IChannel_t2285_1_0_0/* this_arg */
	, &IChannel_t2285_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Channels.IChannelReceiver
extern TypeInfo IChannelReceiver_t2301_il2cpp_TypeInfo;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Channels.IChannelReceiver::get_ChannelData()
extern const MethodInfo IChannelReceiver_get_ChannelData_m13193_MethodInfo = 
{
	"get_ChannelData"/* name */
	, NULL/* method */
	, &IChannelReceiver_t2301_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3704/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo IChannelReceiver_t2301_IChannelReceiver_StartListening_m13194_ParameterInfos[] = 
{
	{"data", 0, 134222271, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Channels.IChannelReceiver::StartListening(System.Object)
extern const MethodInfo IChannelReceiver_StartListening_m13194_MethodInfo = 
{
	"StartListening"/* name */
	, NULL/* method */
	, &IChannelReceiver_t2301_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, IChannelReceiver_t2301_IChannelReceiver_StartListening_m13194_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3705/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IChannelReceiver_t2301_MethodInfos[] =
{
	&IChannelReceiver_get_ChannelData_m13193_MethodInfo,
	&IChannelReceiver_StartListening_m13194_MethodInfo,
	NULL
};
extern const MethodInfo IChannelReceiver_get_ChannelData_m13193_MethodInfo;
static const PropertyInfo IChannelReceiver_t2301____ChannelData_PropertyInfo = 
{
	&IChannelReceiver_t2301_il2cpp_TypeInfo/* parent */
	, "ChannelData"/* name */
	, &IChannelReceiver_get_ChannelData_m13193_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* IChannelReceiver_t2301_PropertyInfos[] =
{
	&IChannelReceiver_t2301____ChannelData_PropertyInfo,
	NULL
};
static const Il2CppType* IChannelReceiver_t2301_InterfacesTypeInfos[] = 
{
	&IChannel_t2285_0_0_0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IChannelReceiver_t2301_1_0_0;
struct IChannelReceiver_t2301;
const Il2CppTypeDefinitionMetadata IChannelReceiver_t2301_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IChannelReceiver_t2301_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IChannelReceiver_t2301_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IChannelReceiver"/* name */
	, "System.Runtime.Remoting.Channels"/* namespaze */
	, IChannelReceiver_t2301_MethodInfos/* methods */
	, IChannelReceiver_t2301_PropertyInfos/* properties */
	, NULL/* events */
	, &IChannelReceiver_t2301_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 551/* custom_attributes_cache */
	, &IChannelReceiver_t2301_0_0_0/* byval_arg */
	, &IChannelReceiver_t2301_1_0_0/* this_arg */
	, &IChannelReceiver_t2301_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Channels.IChannelSender
extern TypeInfo IChannelSender_t2380_il2cpp_TypeInfo;
static const MethodInfo* IChannelSender_t2380_MethodInfos[] =
{
	NULL
};
static const Il2CppType* IChannelSender_t2380_InterfacesTypeInfos[] = 
{
	&IChannel_t2285_0_0_0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IChannelSender_t2380_1_0_0;
struct IChannelSender_t2380;
const Il2CppTypeDefinitionMetadata IChannelSender_t2380_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IChannelSender_t2380_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IChannelSender_t2380_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IChannelSender"/* name */
	, "System.Runtime.Remoting.Channels"/* namespaze */
	, IChannelSender_t2380_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IChannelSender_t2380_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 552/* custom_attributes_cache */
	, &IChannelSender_t2380_0_0_0/* byval_arg */
	, &IChannelSender_t2380_1_0_0/* this_arg */
	, &IChannelSender_t2380_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Channels.ISecurableChannel
extern TypeInfo ISecurableChannel_t2300_il2cpp_TypeInfo;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo ISecurableChannel_t2300_ISecurableChannel_set_IsSecured_m13195_ParameterInfos[] = 
{
	{"value", 0, 134222272, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Channels.ISecurableChannel::set_IsSecured(System.Boolean)
extern const MethodInfo ISecurableChannel_set_IsSecured_m13195_MethodInfo = 
{
	"set_IsSecured"/* name */
	, NULL/* method */
	, &ISecurableChannel_t2300_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274/* invoker_method */
	, ISecurableChannel_t2300_ISecurableChannel_set_IsSecured_m13195_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3706/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ISecurableChannel_t2300_MethodInfos[] =
{
	&ISecurableChannel_set_IsSecured_m13195_MethodInfo,
	NULL
};
extern const MethodInfo ISecurableChannel_set_IsSecured_m13195_MethodInfo;
static const PropertyInfo ISecurableChannel_t2300____IsSecured_PropertyInfo = 
{
	&ISecurableChannel_t2300_il2cpp_TypeInfo/* parent */
	, "IsSecured"/* name */
	, NULL/* get */
	, &ISecurableChannel_set_IsSecured_m13195_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ISecurableChannel_t2300_PropertyInfos[] =
{
	&ISecurableChannel_t2300____IsSecured_PropertyInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ISecurableChannel_t2300_0_0_0;
extern const Il2CppType ISecurableChannel_t2300_1_0_0;
struct ISecurableChannel_t2300;
const Il2CppTypeDefinitionMetadata ISecurableChannel_t2300_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ISecurableChannel_t2300_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ISecurableChannel"/* name */
	, "System.Runtime.Remoting.Channels"/* namespaze */
	, ISecurableChannel_t2300_MethodInfos/* methods */
	, ISecurableChannel_t2300_PropertyInfos/* properties */
	, NULL/* events */
	, &ISecurableChannel_t2300_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ISecurableChannel_t2300_0_0_0/* byval_arg */
	, &ISecurableChannel_t2300_1_0_0/* this_arg */
	, &ISecurableChannel_t2300_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Contexts.Context
#include "mscorlib_System_Runtime_Remoting_Contexts_Context.h"
// Metadata Definition System.Runtime.Remoting.Contexts.Context
extern TypeInfo Context_t1963_il2cpp_TypeInfo;
// System.Runtime.Remoting.Contexts.Context
#include "mscorlib_System_Runtime_Remoting_Contexts_ContextMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Contexts.Context::.cctor()
extern const MethodInfo Context__cctor_m10479_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&Context__cctor_m10479/* method */
	, &Context_t1963_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3707/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Contexts.Context::Finalize()
extern const MethodInfo Context_Finalize_m10480_MethodInfo = 
{
	"Finalize"/* name */
	, (methodPointerType)&Context_Finalize_m10480/* method */
	, &Context_t1963_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3708/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Contexts.Context System.Runtime.Remoting.Contexts.Context::get_DefaultContext()
extern const MethodInfo Context_get_DefaultContext_m10481_MethodInfo = 
{
	"get_DefaultContext"/* name */
	, (methodPointerType)&Context_get_DefaultContext_m10481/* method */
	, &Context_t1963_il2cpp_TypeInfo/* declaring_type */
	, &Context_t1963_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3709/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.Contexts.Context::get_IsDefaultContext()
extern const MethodInfo Context_get_IsDefaultContext_m10482_MethodInfo = 
{
	"get_IsDefaultContext"/* name */
	, (methodPointerType)&Context_get_IsDefaultContext_m10482/* method */
	, &Context_t1963_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2179/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3710/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Context_t1963_Context_GetProperty_m10483_ParameterInfos[] = 
{
	{"name", 0, 134222273, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Contexts.IContextProperty System.Runtime.Remoting.Contexts.Context::GetProperty(System.String)
extern const MethodInfo Context_GetProperty_m10483_MethodInfo = 
{
	"GetProperty"/* name */
	, (methodPointerType)&Context_GetProperty_m10483/* method */
	, &Context_t1963_il2cpp_TypeInfo/* declaring_type */
	, &IContextProperty_t2286_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Context_t1963_Context_GetProperty_m10483_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3711/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Contexts.Context::ToString()
extern const MethodInfo Context_ToString_m10484_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&Context_ToString_m10484/* method */
	, &Context_t1963_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3712/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType MarshalByRefObject_t1308_0_0_0;
static const ParameterInfo Context_t1963_Context_CreateEnvoySink_m10485_ParameterInfos[] = 
{
	{"serverObject", 0, 134222274, 0, &MarshalByRefObject_t1308_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Messaging.IMessageSink System.Runtime.Remoting.Contexts.Context::CreateEnvoySink(System.MarshalByRefObject)
extern const MethodInfo Context_CreateEnvoySink_m10485_MethodInfo = 
{
	"CreateEnvoySink"/* name */
	, (methodPointerType)&Context_CreateEnvoySink_m10485/* method */
	, &Context_t1963_il2cpp_TypeInfo/* declaring_type */
	, &IMessageSink_t1516_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Context_t1963_Context_CreateEnvoySink_m10485_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3713/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Context_t1963_MethodInfos[] =
{
	&Context__cctor_m10479_MethodInfo,
	&Context_Finalize_m10480_MethodInfo,
	&Context_get_DefaultContext_m10481_MethodInfo,
	&Context_get_IsDefaultContext_m10482_MethodInfo,
	&Context_GetProperty_m10483_MethodInfo,
	&Context_ToString_m10484_MethodInfo,
	&Context_CreateEnvoySink_m10485_MethodInfo,
	NULL
};
extern const MethodInfo Context_get_DefaultContext_m10481_MethodInfo;
static const PropertyInfo Context_t1963____DefaultContext_PropertyInfo = 
{
	&Context_t1963_il2cpp_TypeInfo/* parent */
	, "DefaultContext"/* name */
	, &Context_get_DefaultContext_m10481_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Context_get_IsDefaultContext_m10482_MethodInfo;
static const PropertyInfo Context_t1963____IsDefaultContext_PropertyInfo = 
{
	&Context_t1963_il2cpp_TypeInfo/* parent */
	, "IsDefaultContext"/* name */
	, &Context_get_IsDefaultContext_m10482_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Context_t1963_PropertyInfos[] =
{
	&Context_t1963____DefaultContext_PropertyInfo,
	&Context_t1963____IsDefaultContext_PropertyInfo,
	NULL
};
extern const MethodInfo Context_Finalize_m10480_MethodInfo;
extern const MethodInfo Context_ToString_m10484_MethodInfo;
extern const MethodInfo Context_GetProperty_m10483_MethodInfo;
static const Il2CppMethodReference Context_t1963_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Context_Finalize_m10480_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Context_ToString_m10484_MethodInfo,
	&Context_GetProperty_m10483_MethodInfo,
};
static bool Context_t1963_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Context_t1963_1_0_0;
struct Context_t1963;
const Il2CppTypeDefinitionMetadata Context_t1963_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Context_t1963_VTable/* vtableMethods */
	, Context_t1963_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1921/* fieldStart */

};
TypeInfo Context_t1963_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Context"/* name */
	, "System.Runtime.Remoting.Contexts"/* namespaze */
	, Context_t1963_MethodInfos/* methods */
	, Context_t1963_PropertyInfos/* properties */
	, NULL/* events */
	, &Context_t1963_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 553/* custom_attributes_cache */
	, &Context_t1963_0_0_0/* byval_arg */
	, &Context_t1963_1_0_0/* this_arg */
	, &Context_t1963_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Context_t1963)/* instance_size */
	, sizeof (Context_t1963)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Context_t1963_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 2/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Contexts.ContextAttribute
#include "mscorlib_System_Runtime_Remoting_Contexts_ContextAttribute.h"
// Metadata Definition System.Runtime.Remoting.Contexts.ContextAttribute
extern TypeInfo ContextAttribute_t1956_il2cpp_TypeInfo;
// System.Runtime.Remoting.Contexts.ContextAttribute
#include "mscorlib_System_Runtime_Remoting_Contexts_ContextAttributeMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo ContextAttribute_t1956_ContextAttribute__ctor_m10486_ParameterInfos[] = 
{
	{"name", 0, 134222275, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Contexts.ContextAttribute::.ctor(System.String)
extern const MethodInfo ContextAttribute__ctor_m10486_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ContextAttribute__ctor_m10486/* method */
	, &ContextAttribute_t1956_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, ContextAttribute_t1956_ContextAttribute__ctor_m10486_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3714/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Contexts.ContextAttribute::get_Name()
extern const MethodInfo ContextAttribute_get_Name_m10487_MethodInfo = 
{
	"get_Name"/* name */
	, (methodPointerType)&ContextAttribute_get_Name_m10487/* method */
	, &ContextAttribute_t1956_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3715/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo ContextAttribute_t1956_ContextAttribute_Equals_m10488_ParameterInfos[] = 
{
	{"o", 0, 134222276, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.Contexts.ContextAttribute::Equals(System.Object)
extern const MethodInfo ContextAttribute_Equals_m10488_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&ContextAttribute_Equals_m10488/* method */
	, &ContextAttribute_t1956_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, ContextAttribute_t1956_ContextAttribute_Equals_m10488_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3716/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Runtime.Remoting.Contexts.ContextAttribute::GetHashCode()
extern const MethodInfo ContextAttribute_GetHashCode_m10489_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&ContextAttribute_GetHashCode_m10489/* method */
	, &ContextAttribute_t1956_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3717/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IConstructionCallMessage_t2284_0_0_0;
static const ParameterInfo ContextAttribute_t1956_ContextAttribute_GetPropertiesForNewContext_m10490_ParameterInfos[] = 
{
	{"ctorMsg", 0, 134222277, 0, &IConstructionCallMessage_t2284_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Contexts.ContextAttribute::GetPropertiesForNewContext(System.Runtime.Remoting.Activation.IConstructionCallMessage)
extern const MethodInfo ContextAttribute_GetPropertiesForNewContext_m10490_MethodInfo = 
{
	"GetPropertiesForNewContext"/* name */
	, (methodPointerType)&ContextAttribute_GetPropertiesForNewContext_m10490/* method */
	, &ContextAttribute_t1956_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, ContextAttribute_t1956_ContextAttribute_GetPropertiesForNewContext_m10490_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3718/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Context_t1963_0_0_0;
extern const Il2CppType IConstructionCallMessage_t2284_0_0_0;
static const ParameterInfo ContextAttribute_t1956_ContextAttribute_IsContextOK_m10491_ParameterInfos[] = 
{
	{"ctx", 0, 134222278, 0, &Context_t1963_0_0_0},
	{"ctorMsg", 1, 134222279, 0, &IConstructionCallMessage_t2284_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.Contexts.ContextAttribute::IsContextOK(System.Runtime.Remoting.Contexts.Context,System.Runtime.Remoting.Activation.IConstructionCallMessage)
extern const MethodInfo ContextAttribute_IsContextOK_m10491_MethodInfo = 
{
	"IsContextOK"/* name */
	, (methodPointerType)&ContextAttribute_IsContextOK_m10491/* method */
	, &ContextAttribute_t1956_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t_Object_t/* invoker_method */
	, ContextAttribute_t1956_ContextAttribute_IsContextOK_m10491_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3719/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ContextAttribute_t1956_MethodInfos[] =
{
	&ContextAttribute__ctor_m10486_MethodInfo,
	&ContextAttribute_get_Name_m10487_MethodInfo,
	&ContextAttribute_Equals_m10488_MethodInfo,
	&ContextAttribute_GetHashCode_m10489_MethodInfo,
	&ContextAttribute_GetPropertiesForNewContext_m10490_MethodInfo,
	&ContextAttribute_IsContextOK_m10491_MethodInfo,
	NULL
};
static const PropertyInfo ContextAttribute_t1956____Name_PropertyInfo = 
{
	&ContextAttribute_t1956_il2cpp_TypeInfo/* parent */
	, "Name"/* name */
	, &ContextAttribute_get_Name_m10487_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ContextAttribute_t1956_PropertyInfos[] =
{
	&ContextAttribute_t1956____Name_PropertyInfo,
	NULL
};
extern const MethodInfo ContextAttribute_Equals_m10488_MethodInfo;
extern const MethodInfo ContextAttribute_GetHashCode_m10489_MethodInfo;
extern const MethodInfo ContextAttribute_GetPropertiesForNewContext_m10490_MethodInfo;
extern const MethodInfo ContextAttribute_IsContextOK_m10491_MethodInfo;
static const Il2CppMethodReference ContextAttribute_t1956_VTable[] =
{
	&ContextAttribute_Equals_m10488_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&ContextAttribute_GetHashCode_m10489_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&ContextAttribute_GetPropertiesForNewContext_m10490_MethodInfo,
	&ContextAttribute_IsContextOK_m10491_MethodInfo,
	&ContextAttribute_get_Name_m10487_MethodInfo,
	&ContextAttribute_get_Name_m10487_MethodInfo,
	&ContextAttribute_GetPropertiesForNewContext_m10490_MethodInfo,
	&ContextAttribute_IsContextOK_m10491_MethodInfo,
};
static bool ContextAttribute_t1956_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* ContextAttribute_t1956_InterfacesTypeInfos[] = 
{
	&IContextAttribute_t2299_0_0_0,
	&IContextProperty_t2286_0_0_0,
};
static Il2CppInterfaceOffsetPair ContextAttribute_t1956_InterfacesOffsets[] = 
{
	{ &_Attribute_t1145_0_0_0, 4},
	{ &IContextAttribute_t2299_0_0_0, 4},
	{ &IContextProperty_t2286_0_0_0, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ContextAttribute_t1956_1_0_0;
struct ContextAttribute_t1956;
const Il2CppTypeDefinitionMetadata ContextAttribute_t1956_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ContextAttribute_t1956_InterfacesTypeInfos/* implementedInterfaces */
	, ContextAttribute_t1956_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t805_0_0_0/* parent */
	, ContextAttribute_t1956_VTable/* vtableMethods */
	, ContextAttribute_t1956_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1924/* fieldStart */

};
TypeInfo ContextAttribute_t1956_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ContextAttribute"/* name */
	, "System.Runtime.Remoting.Contexts"/* namespaze */
	, ContextAttribute_t1956_MethodInfos/* methods */
	, ContextAttribute_t1956_PropertyInfos/* properties */
	, NULL/* events */
	, &ContextAttribute_t1956_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 554/* custom_attributes_cache */
	, &ContextAttribute_t1956_0_0_0/* byval_arg */
	, &ContextAttribute_t1956_1_0_0/* this_arg */
	, &ContextAttribute_t1956_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ContextAttribute_t1956)/* instance_size */
	, sizeof (ContextAttribute_t1956)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 2/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Remoting.Contexts.CrossContextChannel
#include "mscorlib_System_Runtime_Remoting_Contexts_CrossContextChanne.h"
// Metadata Definition System.Runtime.Remoting.Contexts.CrossContextChannel
extern TypeInfo CrossContextChannel_t1958_il2cpp_TypeInfo;
// System.Runtime.Remoting.Contexts.CrossContextChannel
#include "mscorlib_System_Runtime_Remoting_Contexts_CrossContextChanneMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Contexts.CrossContextChannel::.ctor()
extern const MethodInfo CrossContextChannel__ctor_m10492_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CrossContextChannel__ctor_m10492/* method */
	, &CrossContextChannel_t1958_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3720/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CrossContextChannel_t1958_MethodInfos[] =
{
	&CrossContextChannel__ctor_m10492_MethodInfo,
	NULL
};
static const Il2CppMethodReference CrossContextChannel_t1958_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool CrossContextChannel_t1958_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static const Il2CppType* CrossContextChannel_t1958_InterfacesTypeInfos[] = 
{
	&IMessageSink_t1516_0_0_0,
};
static Il2CppInterfaceOffsetPair CrossContextChannel_t1958_InterfacesOffsets[] = 
{
	{ &IMessageSink_t1516_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CrossContextChannel_t1958_0_0_0;
extern const Il2CppType CrossContextChannel_t1958_1_0_0;
struct CrossContextChannel_t1958;
const Il2CppTypeDefinitionMetadata CrossContextChannel_t1958_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, CrossContextChannel_t1958_InterfacesTypeInfos/* implementedInterfaces */
	, CrossContextChannel_t1958_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CrossContextChannel_t1958_VTable/* vtableMethods */
	, CrossContextChannel_t1958_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo CrossContextChannel_t1958_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CrossContextChannel"/* name */
	, "System.Runtime.Remoting.Contexts"/* namespaze */
	, CrossContextChannel_t1958_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CrossContextChannel_t1958_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CrossContextChannel_t1958_0_0_0/* byval_arg */
	, &CrossContextChannel_t1958_1_0_0/* this_arg */
	, &CrossContextChannel_t1958_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CrossContextChannel_t1958)/* instance_size */
	, sizeof (CrossContextChannel_t1958)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Contexts.IContextAttribute
extern TypeInfo IContextAttribute_t2299_il2cpp_TypeInfo;
extern const Il2CppType IConstructionCallMessage_t2284_0_0_0;
static const ParameterInfo IContextAttribute_t2299_IContextAttribute_GetPropertiesForNewContext_m13196_ParameterInfos[] = 
{
	{"msg", 0, 134222280, 0, &IConstructionCallMessage_t2284_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Contexts.IContextAttribute::GetPropertiesForNewContext(System.Runtime.Remoting.Activation.IConstructionCallMessage)
extern const MethodInfo IContextAttribute_GetPropertiesForNewContext_m13196_MethodInfo = 
{
	"GetPropertiesForNewContext"/* name */
	, NULL/* method */
	, &IContextAttribute_t2299_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, IContextAttribute_t2299_IContextAttribute_GetPropertiesForNewContext_m13196_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3721/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Context_t1963_0_0_0;
extern const Il2CppType IConstructionCallMessage_t2284_0_0_0;
static const ParameterInfo IContextAttribute_t2299_IContextAttribute_IsContextOK_m13197_ParameterInfos[] = 
{
	{"ctx", 0, 134222281, 0, &Context_t1963_0_0_0},
	{"msg", 1, 134222282, 0, &IConstructionCallMessage_t2284_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.Contexts.IContextAttribute::IsContextOK(System.Runtime.Remoting.Contexts.Context,System.Runtime.Remoting.Activation.IConstructionCallMessage)
extern const MethodInfo IContextAttribute_IsContextOK_m13197_MethodInfo = 
{
	"IsContextOK"/* name */
	, NULL/* method */
	, &IContextAttribute_t2299_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t_Object_t/* invoker_method */
	, IContextAttribute_t2299_IContextAttribute_IsContextOK_m13197_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3722/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IContextAttribute_t2299_MethodInfos[] =
{
	&IContextAttribute_GetPropertiesForNewContext_m13196_MethodInfo,
	&IContextAttribute_IsContextOK_m13197_MethodInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IContextAttribute_t2299_1_0_0;
struct IContextAttribute_t2299;
const Il2CppTypeDefinitionMetadata IContextAttribute_t2299_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IContextAttribute_t2299_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IContextAttribute"/* name */
	, "System.Runtime.Remoting.Contexts"/* namespaze */
	, IContextAttribute_t2299_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IContextAttribute_t2299_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 555/* custom_attributes_cache */
	, &IContextAttribute_t2299_0_0_0/* byval_arg */
	, &IContextAttribute_t2299_1_0_0/* this_arg */
	, &IContextAttribute_t2299_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Contexts.IContextProperty
extern TypeInfo IContextProperty_t2286_il2cpp_TypeInfo;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Contexts.IContextProperty::get_Name()
extern const MethodInfo IContextProperty_get_Name_m13198_MethodInfo = 
{
	"get_Name"/* name */
	, NULL/* method */
	, &IContextProperty_t2286_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3723/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IContextProperty_t2286_MethodInfos[] =
{
	&IContextProperty_get_Name_m13198_MethodInfo,
	NULL
};
extern const MethodInfo IContextProperty_get_Name_m13198_MethodInfo;
static const PropertyInfo IContextProperty_t2286____Name_PropertyInfo = 
{
	&IContextProperty_t2286_il2cpp_TypeInfo/* parent */
	, "Name"/* name */
	, &IContextProperty_get_Name_m13198_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* IContextProperty_t2286_PropertyInfos[] =
{
	&IContextProperty_t2286____Name_PropertyInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IContextProperty_t2286_1_0_0;
struct IContextProperty_t2286;
const Il2CppTypeDefinitionMetadata IContextProperty_t2286_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IContextProperty_t2286_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IContextProperty"/* name */
	, "System.Runtime.Remoting.Contexts"/* namespaze */
	, IContextProperty_t2286_MethodInfos/* methods */
	, IContextProperty_t2286_PropertyInfos/* properties */
	, NULL/* events */
	, &IContextProperty_t2286_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 556/* custom_attributes_cache */
	, &IContextProperty_t2286_0_0_0/* byval_arg */
	, &IContextProperty_t2286_1_0_0/* this_arg */
	, &IContextProperty_t2286_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Contexts.IContributeClientContextSink
extern TypeInfo IContributeClientContextSink_t2381_il2cpp_TypeInfo;
static const MethodInfo* IContributeClientContextSink_t2381_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IContributeClientContextSink_t2381_0_0_0;
extern const Il2CppType IContributeClientContextSink_t2381_1_0_0;
struct IContributeClientContextSink_t2381;
const Il2CppTypeDefinitionMetadata IContributeClientContextSink_t2381_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IContributeClientContextSink_t2381_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IContributeClientContextSink"/* name */
	, "System.Runtime.Remoting.Contexts"/* namespaze */
	, IContributeClientContextSink_t2381_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IContributeClientContextSink_t2381_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 557/* custom_attributes_cache */
	, &IContributeClientContextSink_t2381_0_0_0/* byval_arg */
	, &IContributeClientContextSink_t2381_1_0_0/* this_arg */
	, &IContributeClientContextSink_t2381_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Contexts.IContributeEnvoySink
extern TypeInfo IContributeEnvoySink_t2302_il2cpp_TypeInfo;
extern const Il2CppType MarshalByRefObject_t1308_0_0_0;
extern const Il2CppType IMessageSink_t1516_0_0_0;
static const ParameterInfo IContributeEnvoySink_t2302_IContributeEnvoySink_GetEnvoySink_m13199_ParameterInfos[] = 
{
	{"obj", 0, 134222283, 0, &MarshalByRefObject_t1308_0_0_0},
	{"nextSink", 1, 134222284, 0, &IMessageSink_t1516_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Messaging.IMessageSink System.Runtime.Remoting.Contexts.IContributeEnvoySink::GetEnvoySink(System.MarshalByRefObject,System.Runtime.Remoting.Messaging.IMessageSink)
extern const MethodInfo IContributeEnvoySink_GetEnvoySink_m13199_MethodInfo = 
{
	"GetEnvoySink"/* name */
	, NULL/* method */
	, &IContributeEnvoySink_t2302_il2cpp_TypeInfo/* declaring_type */
	, &IMessageSink_t1516_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, IContributeEnvoySink_t2302_IContributeEnvoySink_GetEnvoySink_m13199_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3724/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IContributeEnvoySink_t2302_MethodInfos[] =
{
	&IContributeEnvoySink_GetEnvoySink_m13199_MethodInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IContributeEnvoySink_t2302_0_0_0;
extern const Il2CppType IContributeEnvoySink_t2302_1_0_0;
struct IContributeEnvoySink_t2302;
const Il2CppTypeDefinitionMetadata IContributeEnvoySink_t2302_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IContributeEnvoySink_t2302_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IContributeEnvoySink"/* name */
	, "System.Runtime.Remoting.Contexts"/* namespaze */
	, IContributeEnvoySink_t2302_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IContributeEnvoySink_t2302_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 558/* custom_attributes_cache */
	, &IContributeEnvoySink_t2302_0_0_0/* byval_arg */
	, &IContributeEnvoySink_t2302_1_0_0/* this_arg */
	, &IContributeEnvoySink_t2302_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Contexts.IContributeServerContextSink
extern TypeInfo IContributeServerContextSink_t2382_il2cpp_TypeInfo;
static const MethodInfo* IContributeServerContextSink_t2382_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IContributeServerContextSink_t2382_0_0_0;
extern const Il2CppType IContributeServerContextSink_t2382_1_0_0;
struct IContributeServerContextSink_t2382;
const Il2CppTypeDefinitionMetadata IContributeServerContextSink_t2382_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IContributeServerContextSink_t2382_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IContributeServerContextSink"/* name */
	, "System.Runtime.Remoting.Contexts"/* namespaze */
	, IContributeServerContextSink_t2382_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IContributeServerContextSink_t2382_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 559/* custom_attributes_cache */
	, &IContributeServerContextSink_t2382_0_0_0/* byval_arg */
	, &IContributeServerContextSink_t2382_1_0_0/* this_arg */
	, &IContributeServerContextSink_t2382_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Contexts.SynchronizationAttribute
#include "mscorlib_System_Runtime_Remoting_Contexts_SynchronizationAtt.h"
// Metadata Definition System.Runtime.Remoting.Contexts.SynchronizationAttribute
extern TypeInfo SynchronizationAttribute_t1965_il2cpp_TypeInfo;
// System.Runtime.Remoting.Contexts.SynchronizationAttribute
#include "mscorlib_System_Runtime_Remoting_Contexts_SynchronizationAttMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Contexts.SynchronizationAttribute::.ctor()
extern const MethodInfo SynchronizationAttribute__ctor_m10493_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SynchronizationAttribute__ctor_m10493/* method */
	, &SynchronizationAttribute_t1965_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3725/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo SynchronizationAttribute_t1965_SynchronizationAttribute__ctor_m10494_ParameterInfos[] = 
{
	{"flag", 0, 134222285, 0, &Int32_t253_0_0_0},
	{"reEntrant", 1, 134222286, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Contexts.SynchronizationAttribute::.ctor(System.Int32,System.Boolean)
extern const MethodInfo SynchronizationAttribute__ctor_m10494_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SynchronizationAttribute__ctor_m10494/* method */
	, &SynchronizationAttribute_t1965_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253_SByte_t274/* invoker_method */
	, SynchronizationAttribute_t1965_SynchronizationAttribute__ctor_m10494_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3726/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo SynchronizationAttribute_t1965_SynchronizationAttribute_set_Locked_m10495_ParameterInfos[] = 
{
	{"value", 0, 134222287, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Contexts.SynchronizationAttribute::set_Locked(System.Boolean)
extern const MethodInfo SynchronizationAttribute_set_Locked_m10495_MethodInfo = 
{
	"set_Locked"/* name */
	, (methodPointerType)&SynchronizationAttribute_set_Locked_m10495/* method */
	, &SynchronizationAttribute_t1965_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274/* invoker_method */
	, SynchronizationAttribute_t1965_SynchronizationAttribute_set_Locked_m10495_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3727/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Contexts.SynchronizationAttribute::ReleaseLock()
extern const MethodInfo SynchronizationAttribute_ReleaseLock_m10496_MethodInfo = 
{
	"ReleaseLock"/* name */
	, (methodPointerType)&SynchronizationAttribute_ReleaseLock_m10496/* method */
	, &SynchronizationAttribute_t1965_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3728/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IConstructionCallMessage_t2284_0_0_0;
static const ParameterInfo SynchronizationAttribute_t1965_SynchronizationAttribute_GetPropertiesForNewContext_m10497_ParameterInfos[] = 
{
	{"ctorMsg", 0, 134222288, 0, &IConstructionCallMessage_t2284_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Contexts.SynchronizationAttribute::GetPropertiesForNewContext(System.Runtime.Remoting.Activation.IConstructionCallMessage)
extern const MethodInfo SynchronizationAttribute_GetPropertiesForNewContext_m10497_MethodInfo = 
{
	"GetPropertiesForNewContext"/* name */
	, (methodPointerType)&SynchronizationAttribute_GetPropertiesForNewContext_m10497/* method */
	, &SynchronizationAttribute_t1965_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, SynchronizationAttribute_t1965_SynchronizationAttribute_GetPropertiesForNewContext_m10497_ParameterInfos/* parameters */
	, 561/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3729/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Context_t1963_0_0_0;
extern const Il2CppType IConstructionCallMessage_t2284_0_0_0;
static const ParameterInfo SynchronizationAttribute_t1965_SynchronizationAttribute_IsContextOK_m10498_ParameterInfos[] = 
{
	{"ctx", 0, 134222289, 0, &Context_t1963_0_0_0},
	{"msg", 1, 134222290, 0, &IConstructionCallMessage_t2284_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.Contexts.SynchronizationAttribute::IsContextOK(System.Runtime.Remoting.Contexts.Context,System.Runtime.Remoting.Activation.IConstructionCallMessage)
extern const MethodInfo SynchronizationAttribute_IsContextOK_m10498_MethodInfo = 
{
	"IsContextOK"/* name */
	, (methodPointerType)&SynchronizationAttribute_IsContextOK_m10498/* method */
	, &SynchronizationAttribute_t1965_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t_Object_t/* invoker_method */
	, SynchronizationAttribute_t1965_SynchronizationAttribute_IsContextOK_m10498_ParameterInfos/* parameters */
	, 562/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3730/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Contexts.SynchronizationAttribute::ExitContext()
extern const MethodInfo SynchronizationAttribute_ExitContext_m10499_MethodInfo = 
{
	"ExitContext"/* name */
	, (methodPointerType)&SynchronizationAttribute_ExitContext_m10499/* method */
	, &SynchronizationAttribute_t1965_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3731/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Contexts.SynchronizationAttribute::EnterContext()
extern const MethodInfo SynchronizationAttribute_EnterContext_m10500_MethodInfo = 
{
	"EnterContext"/* name */
	, (methodPointerType)&SynchronizationAttribute_EnterContext_m10500/* method */
	, &SynchronizationAttribute_t1965_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3732/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SynchronizationAttribute_t1965_MethodInfos[] =
{
	&SynchronizationAttribute__ctor_m10493_MethodInfo,
	&SynchronizationAttribute__ctor_m10494_MethodInfo,
	&SynchronizationAttribute_set_Locked_m10495_MethodInfo,
	&SynchronizationAttribute_ReleaseLock_m10496_MethodInfo,
	&SynchronizationAttribute_GetPropertiesForNewContext_m10497_MethodInfo,
	&SynchronizationAttribute_IsContextOK_m10498_MethodInfo,
	&SynchronizationAttribute_ExitContext_m10499_MethodInfo,
	&SynchronizationAttribute_EnterContext_m10500_MethodInfo,
	NULL
};
extern const MethodInfo SynchronizationAttribute_set_Locked_m10495_MethodInfo;
static const PropertyInfo SynchronizationAttribute_t1965____Locked_PropertyInfo = 
{
	&SynchronizationAttribute_t1965_il2cpp_TypeInfo/* parent */
	, "Locked"/* name */
	, NULL/* get */
	, &SynchronizationAttribute_set_Locked_m10495_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* SynchronizationAttribute_t1965_PropertyInfos[] =
{
	&SynchronizationAttribute_t1965____Locked_PropertyInfo,
	NULL
};
extern const MethodInfo SynchronizationAttribute_GetPropertiesForNewContext_m10497_MethodInfo;
extern const MethodInfo SynchronizationAttribute_IsContextOK_m10498_MethodInfo;
static const Il2CppMethodReference SynchronizationAttribute_t1965_VTable[] =
{
	&ContextAttribute_Equals_m10488_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&ContextAttribute_GetHashCode_m10489_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&SynchronizationAttribute_GetPropertiesForNewContext_m10497_MethodInfo,
	&SynchronizationAttribute_IsContextOK_m10498_MethodInfo,
	&ContextAttribute_get_Name_m10487_MethodInfo,
	&ContextAttribute_get_Name_m10487_MethodInfo,
	&SynchronizationAttribute_GetPropertiesForNewContext_m10497_MethodInfo,
	&SynchronizationAttribute_IsContextOK_m10498_MethodInfo,
	&SynchronizationAttribute_set_Locked_m10495_MethodInfo,
};
static bool SynchronizationAttribute_t1965_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* SynchronizationAttribute_t1965_InterfacesTypeInfos[] = 
{
	&IContributeClientContextSink_t2381_0_0_0,
	&IContributeServerContextSink_t2382_0_0_0,
};
static Il2CppInterfaceOffsetPair SynchronizationAttribute_t1965_InterfacesOffsets[] = 
{
	{ &IContextAttribute_t2299_0_0_0, 4},
	{ &IContextProperty_t2286_0_0_0, 6},
	{ &_Attribute_t1145_0_0_0, 4},
	{ &IContributeClientContextSink_t2381_0_0_0, 10},
	{ &IContributeServerContextSink_t2382_0_0_0, 10},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SynchronizationAttribute_t1965_0_0_0;
extern const Il2CppType SynchronizationAttribute_t1965_1_0_0;
struct SynchronizationAttribute_t1965;
const Il2CppTypeDefinitionMetadata SynchronizationAttribute_t1965_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, SynchronizationAttribute_t1965_InterfacesTypeInfos/* implementedInterfaces */
	, SynchronizationAttribute_t1965_InterfacesOffsets/* interfaceOffsets */
	, &ContextAttribute_t1956_0_0_0/* parent */
	, SynchronizationAttribute_t1965_VTable/* vtableMethods */
	, SynchronizationAttribute_t1965_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1925/* fieldStart */

};
TypeInfo SynchronizationAttribute_t1965_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SynchronizationAttribute"/* name */
	, "System.Runtime.Remoting.Contexts"/* namespaze */
	, SynchronizationAttribute_t1965_MethodInfos/* methods */
	, SynchronizationAttribute_t1965_PropertyInfos/* properties */
	, NULL/* events */
	, &SynchronizationAttribute_t1965_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 560/* custom_attributes_cache */
	, &SynchronizationAttribute_t1965_0_0_0/* byval_arg */
	, &SynchronizationAttribute_t1965_1_0_0/* this_arg */
	, &SynchronizationAttribute_t1965_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SynchronizationAttribute_t1965)/* instance_size */
	, sizeof (SynchronizationAttribute_t1965)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 1/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 2/* interfaces_count */
	, 5/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Lifetime.ILease
extern TypeInfo ILease_t1966_il2cpp_TypeInfo;
extern const Il2CppType LeaseState_t1971_0_0_0;
extern void* RuntimeInvoker_LeaseState_t1971 (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Lifetime.LeaseState System.Runtime.Remoting.Lifetime.ILease::get_CurrentState()
extern const MethodInfo ILease_get_CurrentState_m13200_MethodInfo = 
{
	"get_CurrentState"/* name */
	, NULL/* method */
	, &ILease_t1966_il2cpp_TypeInfo/* declaring_type */
	, &LeaseState_t1971_0_0_0/* return_type */
	, RuntimeInvoker_LeaseState_t1971/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3733/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeSpan_t1337_0_0_0;
extern const Il2CppType TimeSpan_t1337_0_0_0;
static const ParameterInfo ILease_t1966_ILease_set_InitialLeaseTime_m13201_ParameterInfos[] = 
{
	{"value", 0, 134222291, 0, &TimeSpan_t1337_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_TimeSpan_t1337 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Lifetime.ILease::set_InitialLeaseTime(System.TimeSpan)
extern const MethodInfo ILease_set_InitialLeaseTime_m13201_MethodInfo = 
{
	"set_InitialLeaseTime"/* name */
	, NULL/* method */
	, &ILease_t1966_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_TimeSpan_t1337/* invoker_method */
	, ILease_t1966_ILease_set_InitialLeaseTime_m13201_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3734/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeSpan_t1337_0_0_0;
static const ParameterInfo ILease_t1966_ILease_set_RenewOnCallTime_m13202_ParameterInfos[] = 
{
	{"value", 0, 134222292, 0, &TimeSpan_t1337_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_TimeSpan_t1337 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Lifetime.ILease::set_RenewOnCallTime(System.TimeSpan)
extern const MethodInfo ILease_set_RenewOnCallTime_m13202_MethodInfo = 
{
	"set_RenewOnCallTime"/* name */
	, NULL/* method */
	, &ILease_t1966_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_TimeSpan_t1337/* invoker_method */
	, ILease_t1966_ILease_set_RenewOnCallTime_m13202_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3735/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeSpan_t1337_0_0_0;
static const ParameterInfo ILease_t1966_ILease_set_SponsorshipTimeout_m13203_ParameterInfos[] = 
{
	{"value", 0, 134222293, 0, &TimeSpan_t1337_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_TimeSpan_t1337 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Lifetime.ILease::set_SponsorshipTimeout(System.TimeSpan)
extern const MethodInfo ILease_set_SponsorshipTimeout_m13203_MethodInfo = 
{
	"set_SponsorshipTimeout"/* name */
	, NULL/* method */
	, &ILease_t1966_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_TimeSpan_t1337/* invoker_method */
	, ILease_t1966_ILease_set_SponsorshipTimeout_m13203_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3736/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ILease_t1966_MethodInfos[] =
{
	&ILease_get_CurrentState_m13200_MethodInfo,
	&ILease_set_InitialLeaseTime_m13201_MethodInfo,
	&ILease_set_RenewOnCallTime_m13202_MethodInfo,
	&ILease_set_SponsorshipTimeout_m13203_MethodInfo,
	NULL
};
extern const MethodInfo ILease_get_CurrentState_m13200_MethodInfo;
static const PropertyInfo ILease_t1966____CurrentState_PropertyInfo = 
{
	&ILease_t1966_il2cpp_TypeInfo/* parent */
	, "CurrentState"/* name */
	, &ILease_get_CurrentState_m13200_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ILease_set_InitialLeaseTime_m13201_MethodInfo;
static const PropertyInfo ILease_t1966____InitialLeaseTime_PropertyInfo = 
{
	&ILease_t1966_il2cpp_TypeInfo/* parent */
	, "InitialLeaseTime"/* name */
	, NULL/* get */
	, &ILease_set_InitialLeaseTime_m13201_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ILease_set_RenewOnCallTime_m13202_MethodInfo;
static const PropertyInfo ILease_t1966____RenewOnCallTime_PropertyInfo = 
{
	&ILease_t1966_il2cpp_TypeInfo/* parent */
	, "RenewOnCallTime"/* name */
	, NULL/* get */
	, &ILease_set_RenewOnCallTime_m13202_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ILease_set_SponsorshipTimeout_m13203_MethodInfo;
static const PropertyInfo ILease_t1966____SponsorshipTimeout_PropertyInfo = 
{
	&ILease_t1966_il2cpp_TypeInfo/* parent */
	, "SponsorshipTimeout"/* name */
	, NULL/* get */
	, &ILease_set_SponsorshipTimeout_m13203_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ILease_t1966_PropertyInfos[] =
{
	&ILease_t1966____CurrentState_PropertyInfo,
	&ILease_t1966____InitialLeaseTime_PropertyInfo,
	&ILease_t1966____RenewOnCallTime_PropertyInfo,
	&ILease_t1966____SponsorshipTimeout_PropertyInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ILease_t1966_0_0_0;
extern const Il2CppType ILease_t1966_1_0_0;
struct ILease_t1966;
const Il2CppTypeDefinitionMetadata ILease_t1966_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ILease_t1966_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ILease"/* name */
	, "System.Runtime.Remoting.Lifetime"/* namespaze */
	, ILease_t1966_MethodInfos/* methods */
	, ILease_t1966_PropertyInfos/* properties */
	, NULL/* events */
	, &ILease_t1966_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 563/* custom_attributes_cache */
	, &ILease_t1966_0_0_0/* byval_arg */
	, &ILease_t1966_1_0_0/* this_arg */
	, &ILease_t1966_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 4/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Lifetime.ISponsor
extern TypeInfo ISponsor_t2287_il2cpp_TypeInfo;
extern const Il2CppType ILease_t1966_0_0_0;
static const ParameterInfo ISponsor_t2287_ISponsor_Renewal_m13204_ParameterInfos[] = 
{
	{"lease", 0, 134222294, 0, &ILease_t1966_0_0_0},
};
extern void* RuntimeInvoker_TimeSpan_t1337_Object_t (const MethodInfo* method, void* obj, void** args);
// System.TimeSpan System.Runtime.Remoting.Lifetime.ISponsor::Renewal(System.Runtime.Remoting.Lifetime.ILease)
extern const MethodInfo ISponsor_Renewal_m13204_MethodInfo = 
{
	"Renewal"/* name */
	, NULL/* method */
	, &ISponsor_t2287_il2cpp_TypeInfo/* declaring_type */
	, &TimeSpan_t1337_0_0_0/* return_type */
	, RuntimeInvoker_TimeSpan_t1337_Object_t/* invoker_method */
	, ISponsor_t2287_ISponsor_Renewal_m13204_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3737/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ISponsor_t2287_MethodInfos[] =
{
	&ISponsor_Renewal_m13204_MethodInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ISponsor_t2287_0_0_0;
extern const Il2CppType ISponsor_t2287_1_0_0;
struct ISponsor_t2287;
const Il2CppTypeDefinitionMetadata ISponsor_t2287_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ISponsor_t2287_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ISponsor"/* name */
	, "System.Runtime.Remoting.Lifetime"/* namespaze */
	, ISponsor_t2287_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ISponsor_t2287_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 564/* custom_attributes_cache */
	, &ISponsor_t2287_0_0_0/* byval_arg */
	, &ISponsor_t2287_1_0_0/* this_arg */
	, &ISponsor_t2287_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Lifetime.Lease/RenewalDelegate
#include "mscorlib_System_Runtime_Remoting_Lifetime_Lease_RenewalDeleg.h"
// Metadata Definition System.Runtime.Remoting.Lifetime.Lease/RenewalDelegate
extern TypeInfo RenewalDelegate_t1967_il2cpp_TypeInfo;
// System.Runtime.Remoting.Lifetime.Lease/RenewalDelegate
#include "mscorlib_System_Runtime_Remoting_Lifetime_Lease_RenewalDelegMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo RenewalDelegate_t1967_RenewalDelegate__ctor_m10501_ParameterInfos[] = 
{
	{"object", 0, 134222302, 0, &Object_t_0_0_0},
	{"method", 1, 134222303, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Lifetime.Lease/RenewalDelegate::.ctor(System.Object,System.IntPtr)
extern const MethodInfo RenewalDelegate__ctor_m10501_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RenewalDelegate__ctor_m10501/* method */
	, &RenewalDelegate_t1967_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_IntPtr_t/* invoker_method */
	, RenewalDelegate_t1967_RenewalDelegate__ctor_m10501_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3750/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ILease_t1966_0_0_0;
static const ParameterInfo RenewalDelegate_t1967_RenewalDelegate_Invoke_m10502_ParameterInfos[] = 
{
	{"lease", 0, 134222304, 0, &ILease_t1966_0_0_0},
};
extern void* RuntimeInvoker_TimeSpan_t1337_Object_t (const MethodInfo* method, void* obj, void** args);
// System.TimeSpan System.Runtime.Remoting.Lifetime.Lease/RenewalDelegate::Invoke(System.Runtime.Remoting.Lifetime.ILease)
extern const MethodInfo RenewalDelegate_Invoke_m10502_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&RenewalDelegate_Invoke_m10502/* method */
	, &RenewalDelegate_t1967_il2cpp_TypeInfo/* declaring_type */
	, &TimeSpan_t1337_0_0_0/* return_type */
	, RuntimeInvoker_TimeSpan_t1337_Object_t/* invoker_method */
	, RenewalDelegate_t1967_RenewalDelegate_Invoke_m10502_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3751/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ILease_t1966_0_0_0;
extern const Il2CppType AsyncCallback_t547_0_0_0;
extern const Il2CppType AsyncCallback_t547_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo RenewalDelegate_t1967_RenewalDelegate_BeginInvoke_m10503_ParameterInfos[] = 
{
	{"lease", 0, 134222305, 0, &ILease_t1966_0_0_0},
	{"callback", 1, 134222306, 0, &AsyncCallback_t547_0_0_0},
	{"object", 2, 134222307, 0, &Object_t_0_0_0},
};
extern const Il2CppType IAsyncResult_t546_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult System.Runtime.Remoting.Lifetime.Lease/RenewalDelegate::BeginInvoke(System.Runtime.Remoting.Lifetime.ILease,System.AsyncCallback,System.Object)
extern const MethodInfo RenewalDelegate_BeginInvoke_m10503_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&RenewalDelegate_BeginInvoke_m10503/* method */
	, &RenewalDelegate_t1967_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t546_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, RenewalDelegate_t1967_RenewalDelegate_BeginInvoke_m10503_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3752/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t546_0_0_0;
static const ParameterInfo RenewalDelegate_t1967_RenewalDelegate_EndInvoke_m10504_ParameterInfos[] = 
{
	{"result", 0, 134222308, 0, &IAsyncResult_t546_0_0_0},
};
extern void* RuntimeInvoker_TimeSpan_t1337_Object_t (const MethodInfo* method, void* obj, void** args);
// System.TimeSpan System.Runtime.Remoting.Lifetime.Lease/RenewalDelegate::EndInvoke(System.IAsyncResult)
extern const MethodInfo RenewalDelegate_EndInvoke_m10504_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&RenewalDelegate_EndInvoke_m10504/* method */
	, &RenewalDelegate_t1967_il2cpp_TypeInfo/* declaring_type */
	, &TimeSpan_t1337_0_0_0/* return_type */
	, RuntimeInvoker_TimeSpan_t1337_Object_t/* invoker_method */
	, RenewalDelegate_t1967_RenewalDelegate_EndInvoke_m10504_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3753/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* RenewalDelegate_t1967_MethodInfos[] =
{
	&RenewalDelegate__ctor_m10501_MethodInfo,
	&RenewalDelegate_Invoke_m10502_MethodInfo,
	&RenewalDelegate_BeginInvoke_m10503_MethodInfo,
	&RenewalDelegate_EndInvoke_m10504_MethodInfo,
	NULL
};
extern const MethodInfo MulticastDelegate_Equals_m3586_MethodInfo;
extern const MethodInfo MulticastDelegate_GetHashCode_m3587_MethodInfo;
extern const MethodInfo MulticastDelegate_GetObjectData_m3588_MethodInfo;
extern const MethodInfo Delegate_Clone_m3589_MethodInfo;
extern const MethodInfo MulticastDelegate_GetInvocationList_m3590_MethodInfo;
extern const MethodInfo MulticastDelegate_CombineImpl_m3591_MethodInfo;
extern const MethodInfo MulticastDelegate_RemoveImpl_m3592_MethodInfo;
extern const MethodInfo RenewalDelegate_Invoke_m10502_MethodInfo;
extern const MethodInfo RenewalDelegate_BeginInvoke_m10503_MethodInfo;
extern const MethodInfo RenewalDelegate_EndInvoke_m10504_MethodInfo;
static const Il2CppMethodReference RenewalDelegate_t1967_VTable[] =
{
	&MulticastDelegate_Equals_m3586_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&MulticastDelegate_GetHashCode_m3587_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&Delegate_Clone_m3589_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&MulticastDelegate_GetInvocationList_m3590_MethodInfo,
	&MulticastDelegate_CombineImpl_m3591_MethodInfo,
	&MulticastDelegate_RemoveImpl_m3592_MethodInfo,
	&RenewalDelegate_Invoke_m10502_MethodInfo,
	&RenewalDelegate_BeginInvoke_m10503_MethodInfo,
	&RenewalDelegate_EndInvoke_m10504_MethodInfo,
};
static bool RenewalDelegate_t1967_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ICloneable_t733_0_0_0;
static Il2CppInterfaceOffsetPair RenewalDelegate_t1967_InterfacesOffsets[] = 
{
	{ &ICloneable_t733_0_0_0, 4},
	{ &ISerializable_t439_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RenewalDelegate_t1967_0_0_0;
extern const Il2CppType RenewalDelegate_t1967_1_0_0;
extern const Il2CppType MulticastDelegate_t549_0_0_0;
extern TypeInfo Lease_t1968_il2cpp_TypeInfo;
extern const Il2CppType Lease_t1968_0_0_0;
struct RenewalDelegate_t1967;
const Il2CppTypeDefinitionMetadata RenewalDelegate_t1967_DefinitionMetadata = 
{
	&Lease_t1968_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RenewalDelegate_t1967_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t549_0_0_0/* parent */
	, RenewalDelegate_t1967_VTable/* vtableMethods */
	, RenewalDelegate_t1967_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo RenewalDelegate_t1967_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RenewalDelegate"/* name */
	, ""/* namespaze */
	, RenewalDelegate_t1967_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &RenewalDelegate_t1967_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RenewalDelegate_t1967_0_0_0/* byval_arg */
	, &RenewalDelegate_t1967_1_0_0/* this_arg */
	, &RenewalDelegate_t1967_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_RenewalDelegate_t1967/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RenewalDelegate_t1967)/* instance_size */
	, sizeof (RenewalDelegate_t1967)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Runtime.Remoting.Lifetime.Lease
#include "mscorlib_System_Runtime_Remoting_Lifetime_Lease.h"
// Metadata Definition System.Runtime.Remoting.Lifetime.Lease
// System.Runtime.Remoting.Lifetime.Lease
#include "mscorlib_System_Runtime_Remoting_Lifetime_LeaseMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Lifetime.Lease::.ctor()
extern const MethodInfo Lease__ctor_m10505_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Lease__ctor_m10505/* method */
	, &Lease_t1968_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3738/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_TimeSpan_t1337 (const MethodInfo* method, void* obj, void** args);
// System.TimeSpan System.Runtime.Remoting.Lifetime.Lease::get_CurrentLeaseTime()
extern const MethodInfo Lease_get_CurrentLeaseTime_m10506_MethodInfo = 
{
	"get_CurrentLeaseTime"/* name */
	, (methodPointerType)&Lease_get_CurrentLeaseTime_m10506/* method */
	, &Lease_t1968_il2cpp_TypeInfo/* declaring_type */
	, &TimeSpan_t1337_0_0_0/* return_type */
	, RuntimeInvoker_TimeSpan_t1337/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3739/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_LeaseState_t1971 (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Lifetime.LeaseState System.Runtime.Remoting.Lifetime.Lease::get_CurrentState()
extern const MethodInfo Lease_get_CurrentState_m10507_MethodInfo = 
{
	"get_CurrentState"/* name */
	, (methodPointerType)&Lease_get_CurrentState_m10507/* method */
	, &Lease_t1968_il2cpp_TypeInfo/* declaring_type */
	, &LeaseState_t1971_0_0_0/* return_type */
	, RuntimeInvoker_LeaseState_t1971/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3740/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Lifetime.Lease::Activate()
extern const MethodInfo Lease_Activate_m10508_MethodInfo = 
{
	"Activate"/* name */
	, (methodPointerType)&Lease_Activate_m10508/* method */
	, &Lease_t1968_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3741/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeSpan_t1337_0_0_0;
static const ParameterInfo Lease_t1968_Lease_set_InitialLeaseTime_m10509_ParameterInfos[] = 
{
	{"value", 0, 134222295, 0, &TimeSpan_t1337_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_TimeSpan_t1337 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Lifetime.Lease::set_InitialLeaseTime(System.TimeSpan)
extern const MethodInfo Lease_set_InitialLeaseTime_m10509_MethodInfo = 
{
	"set_InitialLeaseTime"/* name */
	, (methodPointerType)&Lease_set_InitialLeaseTime_m10509/* method */
	, &Lease_t1968_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_TimeSpan_t1337/* invoker_method */
	, Lease_t1968_Lease_set_InitialLeaseTime_m10509_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3742/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeSpan_t1337_0_0_0;
static const ParameterInfo Lease_t1968_Lease_set_RenewOnCallTime_m10510_ParameterInfos[] = 
{
	{"value", 0, 134222296, 0, &TimeSpan_t1337_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_TimeSpan_t1337 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Lifetime.Lease::set_RenewOnCallTime(System.TimeSpan)
extern const MethodInfo Lease_set_RenewOnCallTime_m10510_MethodInfo = 
{
	"set_RenewOnCallTime"/* name */
	, (methodPointerType)&Lease_set_RenewOnCallTime_m10510/* method */
	, &Lease_t1968_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_TimeSpan_t1337/* invoker_method */
	, Lease_t1968_Lease_set_RenewOnCallTime_m10510_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3743/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeSpan_t1337_0_0_0;
static const ParameterInfo Lease_t1968_Lease_set_SponsorshipTimeout_m10511_ParameterInfos[] = 
{
	{"value", 0, 134222297, 0, &TimeSpan_t1337_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_TimeSpan_t1337 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Lifetime.Lease::set_SponsorshipTimeout(System.TimeSpan)
extern const MethodInfo Lease_set_SponsorshipTimeout_m10511_MethodInfo = 
{
	"set_SponsorshipTimeout"/* name */
	, (methodPointerType)&Lease_set_SponsorshipTimeout_m10511/* method */
	, &Lease_t1968_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_TimeSpan_t1337/* invoker_method */
	, Lease_t1968_Lease_set_SponsorshipTimeout_m10511_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3744/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeSpan_t1337_0_0_0;
static const ParameterInfo Lease_t1968_Lease_Renew_m10512_ParameterInfos[] = 
{
	{"renewalTime", 0, 134222298, 0, &TimeSpan_t1337_0_0_0},
};
extern void* RuntimeInvoker_TimeSpan_t1337_TimeSpan_t1337 (const MethodInfo* method, void* obj, void** args);
// System.TimeSpan System.Runtime.Remoting.Lifetime.Lease::Renew(System.TimeSpan)
extern const MethodInfo Lease_Renew_m10512_MethodInfo = 
{
	"Renew"/* name */
	, (methodPointerType)&Lease_Renew_m10512/* method */
	, &Lease_t1968_il2cpp_TypeInfo/* declaring_type */
	, &TimeSpan_t1337_0_0_0/* return_type */
	, RuntimeInvoker_TimeSpan_t1337_TimeSpan_t1337/* invoker_method */
	, Lease_t1968_Lease_Renew_m10512_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3745/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ISponsor_t2287_0_0_0;
static const ParameterInfo Lease_t1968_Lease_Unregister_m10513_ParameterInfos[] = 
{
	{"obj", 0, 134222299, 0, &ISponsor_t2287_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Lifetime.Lease::Unregister(System.Runtime.Remoting.Lifetime.ISponsor)
extern const MethodInfo Lease_Unregister_m10513_MethodInfo = 
{
	"Unregister"/* name */
	, (methodPointerType)&Lease_Unregister_m10513/* method */
	, &Lease_t1968_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, Lease_t1968_Lease_Unregister_m10513_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3746/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Lifetime.Lease::UpdateState()
extern const MethodInfo Lease_UpdateState_m10514_MethodInfo = 
{
	"UpdateState"/* name */
	, (methodPointerType)&Lease_UpdateState_m10514/* method */
	, &Lease_t1968_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3747/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Lifetime.Lease::CheckNextSponsor()
extern const MethodInfo Lease_CheckNextSponsor_m10515_MethodInfo = 
{
	"CheckNextSponsor"/* name */
	, (methodPointerType)&Lease_CheckNextSponsor_m10515/* method */
	, &Lease_t1968_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3748/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo Lease_t1968_Lease_ProcessSponsorResponse_m10516_ParameterInfos[] = 
{
	{"state", 0, 134222300, 0, &Object_t_0_0_0},
	{"timedOut", 1, 134222301, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Lifetime.Lease::ProcessSponsorResponse(System.Object,System.Boolean)
extern const MethodInfo Lease_ProcessSponsorResponse_m10516_MethodInfo = 
{
	"ProcessSponsorResponse"/* name */
	, (methodPointerType)&Lease_ProcessSponsorResponse_m10516/* method */
	, &Lease_t1968_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_SByte_t274/* invoker_method */
	, Lease_t1968_Lease_ProcessSponsorResponse_m10516_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3749/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Lease_t1968_MethodInfos[] =
{
	&Lease__ctor_m10505_MethodInfo,
	&Lease_get_CurrentLeaseTime_m10506_MethodInfo,
	&Lease_get_CurrentState_m10507_MethodInfo,
	&Lease_Activate_m10508_MethodInfo,
	&Lease_set_InitialLeaseTime_m10509_MethodInfo,
	&Lease_set_RenewOnCallTime_m10510_MethodInfo,
	&Lease_set_SponsorshipTimeout_m10511_MethodInfo,
	&Lease_Renew_m10512_MethodInfo,
	&Lease_Unregister_m10513_MethodInfo,
	&Lease_UpdateState_m10514_MethodInfo,
	&Lease_CheckNextSponsor_m10515_MethodInfo,
	&Lease_ProcessSponsorResponse_m10516_MethodInfo,
	NULL
};
extern const MethodInfo Lease_get_CurrentLeaseTime_m10506_MethodInfo;
static const PropertyInfo Lease_t1968____CurrentLeaseTime_PropertyInfo = 
{
	&Lease_t1968_il2cpp_TypeInfo/* parent */
	, "CurrentLeaseTime"/* name */
	, &Lease_get_CurrentLeaseTime_m10506_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Lease_get_CurrentState_m10507_MethodInfo;
static const PropertyInfo Lease_t1968____CurrentState_PropertyInfo = 
{
	&Lease_t1968_il2cpp_TypeInfo/* parent */
	, "CurrentState"/* name */
	, &Lease_get_CurrentState_m10507_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Lease_set_InitialLeaseTime_m10509_MethodInfo;
static const PropertyInfo Lease_t1968____InitialLeaseTime_PropertyInfo = 
{
	&Lease_t1968_il2cpp_TypeInfo/* parent */
	, "InitialLeaseTime"/* name */
	, NULL/* get */
	, &Lease_set_InitialLeaseTime_m10509_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Lease_set_RenewOnCallTime_m10510_MethodInfo;
static const PropertyInfo Lease_t1968____RenewOnCallTime_PropertyInfo = 
{
	&Lease_t1968_il2cpp_TypeInfo/* parent */
	, "RenewOnCallTime"/* name */
	, NULL/* get */
	, &Lease_set_RenewOnCallTime_m10510_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Lease_set_SponsorshipTimeout_m10511_MethodInfo;
static const PropertyInfo Lease_t1968____SponsorshipTimeout_PropertyInfo = 
{
	&Lease_t1968_il2cpp_TypeInfo/* parent */
	, "SponsorshipTimeout"/* name */
	, NULL/* get */
	, &Lease_set_SponsorshipTimeout_m10511_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Lease_t1968_PropertyInfos[] =
{
	&Lease_t1968____CurrentLeaseTime_PropertyInfo,
	&Lease_t1968____CurrentState_PropertyInfo,
	&Lease_t1968____InitialLeaseTime_PropertyInfo,
	&Lease_t1968____RenewOnCallTime_PropertyInfo,
	&Lease_t1968____SponsorshipTimeout_PropertyInfo,
	NULL
};
static const Il2CppType* Lease_t1968_il2cpp_TypeInfo__nestedTypes[1] =
{
	&RenewalDelegate_t1967_0_0_0,
};
extern const MethodInfo MarshalByRefObject_InitializeLifetimeService_m6615_MethodInfo;
extern const MethodInfo Lease_Renew_m10512_MethodInfo;
extern const MethodInfo Lease_Unregister_m10513_MethodInfo;
static const Il2CppMethodReference Lease_t1968_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&MarshalByRefObject_CreateObjRef_m6614_MethodInfo,
	&MarshalByRefObject_InitializeLifetimeService_m6615_MethodInfo,
	&Lease_get_CurrentState_m10507_MethodInfo,
	&Lease_set_InitialLeaseTime_m10509_MethodInfo,
	&Lease_set_RenewOnCallTime_m10510_MethodInfo,
	&Lease_set_SponsorshipTimeout_m10511_MethodInfo,
	&Lease_get_CurrentLeaseTime_m10506_MethodInfo,
	&Lease_Renew_m10512_MethodInfo,
	&Lease_Unregister_m10513_MethodInfo,
};
static bool Lease_t1968_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* Lease_t1968_InterfacesTypeInfos[] = 
{
	&ILease_t1966_0_0_0,
};
static Il2CppInterfaceOffsetPair Lease_t1968_InterfacesOffsets[] = 
{
	{ &ILease_t1966_0_0_0, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Lease_t1968_1_0_0;
struct Lease_t1968;
const Il2CppTypeDefinitionMetadata Lease_t1968_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Lease_t1968_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, Lease_t1968_InterfacesTypeInfos/* implementedInterfaces */
	, Lease_t1968_InterfacesOffsets/* interfaceOffsets */
	, &MarshalByRefObject_t1308_0_0_0/* parent */
	, Lease_t1968_VTable/* vtableMethods */
	, Lease_t1968_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1930/* fieldStart */

};
TypeInfo Lease_t1968_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Lease"/* name */
	, "System.Runtime.Remoting.Lifetime"/* namespaze */
	, Lease_t1968_MethodInfos/* methods */
	, Lease_t1968_PropertyInfos/* properties */
	, NULL/* events */
	, &Lease_t1968_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Lease_t1968_0_0_0/* byval_arg */
	, &Lease_t1968_1_0_0/* this_arg */
	, &Lease_t1968_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Lease_t1968)/* instance_size */
	, sizeof (Lease_t1968)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 12/* method_count */
	, 5/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 13/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.Lifetime.LeaseManager
#include "mscorlib_System_Runtime_Remoting_Lifetime_LeaseManager.h"
// Metadata Definition System.Runtime.Remoting.Lifetime.LeaseManager
extern TypeInfo LeaseManager_t1970_il2cpp_TypeInfo;
// System.Runtime.Remoting.Lifetime.LeaseManager
#include "mscorlib_System_Runtime_Remoting_Lifetime_LeaseManagerMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Lifetime.LeaseManager::.ctor()
extern const MethodInfo LeaseManager__ctor_m10517_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&LeaseManager__ctor_m10517/* method */
	, &LeaseManager_t1970_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3754/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ServerIdentity_t1681_0_0_0;
extern const Il2CppType ServerIdentity_t1681_0_0_0;
static const ParameterInfo LeaseManager_t1970_LeaseManager_TrackLifetime_m10518_ParameterInfos[] = 
{
	{"identity", 0, 134222309, 0, &ServerIdentity_t1681_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Lifetime.LeaseManager::TrackLifetime(System.Runtime.Remoting.ServerIdentity)
extern const MethodInfo LeaseManager_TrackLifetime_m10518_MethodInfo = 
{
	"TrackLifetime"/* name */
	, (methodPointerType)&LeaseManager_TrackLifetime_m10518/* method */
	, &LeaseManager_t1970_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, LeaseManager_t1970_LeaseManager_TrackLifetime_m10518_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3755/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Lifetime.LeaseManager::StartManager()
extern const MethodInfo LeaseManager_StartManager_m10519_MethodInfo = 
{
	"StartManager"/* name */
	, (methodPointerType)&LeaseManager_StartManager_m10519/* method */
	, &LeaseManager_t1970_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3756/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Lifetime.LeaseManager::StopManager()
extern const MethodInfo LeaseManager_StopManager_m10520_MethodInfo = 
{
	"StopManager"/* name */
	, (methodPointerType)&LeaseManager_StopManager_m10520/* method */
	, &LeaseManager_t1970_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3757/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo LeaseManager_t1970_LeaseManager_ManageLeases_m10521_ParameterInfos[] = 
{
	{"state", 0, 134222310, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Lifetime.LeaseManager::ManageLeases(System.Object)
extern const MethodInfo LeaseManager_ManageLeases_m10521_MethodInfo = 
{
	"ManageLeases"/* name */
	, (methodPointerType)&LeaseManager_ManageLeases_m10521/* method */
	, &LeaseManager_t1970_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, LeaseManager_t1970_LeaseManager_ManageLeases_m10521_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3758/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* LeaseManager_t1970_MethodInfos[] =
{
	&LeaseManager__ctor_m10517_MethodInfo,
	&LeaseManager_TrackLifetime_m10518_MethodInfo,
	&LeaseManager_StartManager_m10519_MethodInfo,
	&LeaseManager_StopManager_m10520_MethodInfo,
	&LeaseManager_ManageLeases_m10521_MethodInfo,
	NULL
};
static const Il2CppMethodReference LeaseManager_t1970_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool LeaseManager_t1970_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType LeaseManager_t1970_0_0_0;
extern const Il2CppType LeaseManager_t1970_1_0_0;
struct LeaseManager_t1970;
const Il2CppTypeDefinitionMetadata LeaseManager_t1970_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, LeaseManager_t1970_VTable/* vtableMethods */
	, LeaseManager_t1970_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1938/* fieldStart */

};
TypeInfo LeaseManager_t1970_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "LeaseManager"/* name */
	, "System.Runtime.Remoting.Lifetime"/* namespaze */
	, LeaseManager_t1970_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &LeaseManager_t1970_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &LeaseManager_t1970_0_0_0/* byval_arg */
	, &LeaseManager_t1970_1_0_0/* this_arg */
	, &LeaseManager_t1970_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LeaseManager_t1970)/* instance_size */
	, sizeof (LeaseManager_t1970)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Lifetime.LeaseState
#include "mscorlib_System_Runtime_Remoting_Lifetime_LeaseState.h"
// Metadata Definition System.Runtime.Remoting.Lifetime.LeaseState
extern TypeInfo LeaseState_t1971_il2cpp_TypeInfo;
// System.Runtime.Remoting.Lifetime.LeaseState
#include "mscorlib_System_Runtime_Remoting_Lifetime_LeaseStateMethodDeclarations.h"
static const MethodInfo* LeaseState_t1971_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference LeaseState_t1971_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool LeaseState_t1971_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair LeaseState_t1971_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType LeaseState_t1971_1_0_0;
const Il2CppTypeDefinitionMetadata LeaseState_t1971_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, LeaseState_t1971_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, LeaseState_t1971_VTable/* vtableMethods */
	, LeaseState_t1971_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1940/* fieldStart */

};
TypeInfo LeaseState_t1971_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "LeaseState"/* name */
	, "System.Runtime.Remoting.Lifetime"/* namespaze */
	, LeaseState_t1971_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 565/* custom_attributes_cache */
	, &LeaseState_t1971_0_0_0/* byval_arg */
	, &LeaseState_t1971_1_0_0/* this_arg */
	, &LeaseState_t1971_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LeaseState_t1971)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (LeaseState_t1971)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Remoting.Lifetime.LifetimeServices
#include "mscorlib_System_Runtime_Remoting_Lifetime_LifetimeServices.h"
// Metadata Definition System.Runtime.Remoting.Lifetime.LifetimeServices
extern TypeInfo LifetimeServices_t1972_il2cpp_TypeInfo;
// System.Runtime.Remoting.Lifetime.LifetimeServices
#include "mscorlib_System_Runtime_Remoting_Lifetime_LifetimeServicesMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Lifetime.LifetimeServices::.cctor()
extern const MethodInfo LifetimeServices__cctor_m10522_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&LifetimeServices__cctor_m10522/* method */
	, &LifetimeServices_t1972_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3759/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_TimeSpan_t1337 (const MethodInfo* method, void* obj, void** args);
// System.TimeSpan System.Runtime.Remoting.Lifetime.LifetimeServices::get_LeaseManagerPollTime()
extern const MethodInfo LifetimeServices_get_LeaseManagerPollTime_m10523_MethodInfo = 
{
	"get_LeaseManagerPollTime"/* name */
	, (methodPointerType)&LifetimeServices_get_LeaseManagerPollTime_m10523/* method */
	, &LifetimeServices_t1972_il2cpp_TypeInfo/* declaring_type */
	, &TimeSpan_t1337_0_0_0/* return_type */
	, RuntimeInvoker_TimeSpan_t1337/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3760/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_TimeSpan_t1337 (const MethodInfo* method, void* obj, void** args);
// System.TimeSpan System.Runtime.Remoting.Lifetime.LifetimeServices::get_LeaseTime()
extern const MethodInfo LifetimeServices_get_LeaseTime_m10524_MethodInfo = 
{
	"get_LeaseTime"/* name */
	, (methodPointerType)&LifetimeServices_get_LeaseTime_m10524/* method */
	, &LifetimeServices_t1972_il2cpp_TypeInfo/* declaring_type */
	, &TimeSpan_t1337_0_0_0/* return_type */
	, RuntimeInvoker_TimeSpan_t1337/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3761/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_TimeSpan_t1337 (const MethodInfo* method, void* obj, void** args);
// System.TimeSpan System.Runtime.Remoting.Lifetime.LifetimeServices::get_RenewOnCallTime()
extern const MethodInfo LifetimeServices_get_RenewOnCallTime_m10525_MethodInfo = 
{
	"get_RenewOnCallTime"/* name */
	, (methodPointerType)&LifetimeServices_get_RenewOnCallTime_m10525/* method */
	, &LifetimeServices_t1972_il2cpp_TypeInfo/* declaring_type */
	, &TimeSpan_t1337_0_0_0/* return_type */
	, RuntimeInvoker_TimeSpan_t1337/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3762/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_TimeSpan_t1337 (const MethodInfo* method, void* obj, void** args);
// System.TimeSpan System.Runtime.Remoting.Lifetime.LifetimeServices::get_SponsorshipTimeout()
extern const MethodInfo LifetimeServices_get_SponsorshipTimeout_m10526_MethodInfo = 
{
	"get_SponsorshipTimeout"/* name */
	, (methodPointerType)&LifetimeServices_get_SponsorshipTimeout_m10526/* method */
	, &LifetimeServices_t1972_il2cpp_TypeInfo/* declaring_type */
	, &TimeSpan_t1337_0_0_0/* return_type */
	, RuntimeInvoker_TimeSpan_t1337/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3763/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ServerIdentity_t1681_0_0_0;
static const ParameterInfo LifetimeServices_t1972_LifetimeServices_TrackLifetime_m10527_ParameterInfos[] = 
{
	{"identity", 0, 134222311, 0, &ServerIdentity_t1681_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Lifetime.LifetimeServices::TrackLifetime(System.Runtime.Remoting.ServerIdentity)
extern const MethodInfo LifetimeServices_TrackLifetime_m10527_MethodInfo = 
{
	"TrackLifetime"/* name */
	, (methodPointerType)&LifetimeServices_TrackLifetime_m10527/* method */
	, &LifetimeServices_t1972_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, LifetimeServices_t1972_LifetimeServices_TrackLifetime_m10527_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3764/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* LifetimeServices_t1972_MethodInfos[] =
{
	&LifetimeServices__cctor_m10522_MethodInfo,
	&LifetimeServices_get_LeaseManagerPollTime_m10523_MethodInfo,
	&LifetimeServices_get_LeaseTime_m10524_MethodInfo,
	&LifetimeServices_get_RenewOnCallTime_m10525_MethodInfo,
	&LifetimeServices_get_SponsorshipTimeout_m10526_MethodInfo,
	&LifetimeServices_TrackLifetime_m10527_MethodInfo,
	NULL
};
extern const MethodInfo LifetimeServices_get_LeaseManagerPollTime_m10523_MethodInfo;
static const PropertyInfo LifetimeServices_t1972____LeaseManagerPollTime_PropertyInfo = 
{
	&LifetimeServices_t1972_il2cpp_TypeInfo/* parent */
	, "LeaseManagerPollTime"/* name */
	, &LifetimeServices_get_LeaseManagerPollTime_m10523_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo LifetimeServices_get_LeaseTime_m10524_MethodInfo;
static const PropertyInfo LifetimeServices_t1972____LeaseTime_PropertyInfo = 
{
	&LifetimeServices_t1972_il2cpp_TypeInfo/* parent */
	, "LeaseTime"/* name */
	, &LifetimeServices_get_LeaseTime_m10524_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo LifetimeServices_get_RenewOnCallTime_m10525_MethodInfo;
static const PropertyInfo LifetimeServices_t1972____RenewOnCallTime_PropertyInfo = 
{
	&LifetimeServices_t1972_il2cpp_TypeInfo/* parent */
	, "RenewOnCallTime"/* name */
	, &LifetimeServices_get_RenewOnCallTime_m10525_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo LifetimeServices_get_SponsorshipTimeout_m10526_MethodInfo;
static const PropertyInfo LifetimeServices_t1972____SponsorshipTimeout_PropertyInfo = 
{
	&LifetimeServices_t1972_il2cpp_TypeInfo/* parent */
	, "SponsorshipTimeout"/* name */
	, &LifetimeServices_get_SponsorshipTimeout_m10526_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* LifetimeServices_t1972_PropertyInfos[] =
{
	&LifetimeServices_t1972____LeaseManagerPollTime_PropertyInfo,
	&LifetimeServices_t1972____LeaseTime_PropertyInfo,
	&LifetimeServices_t1972____RenewOnCallTime_PropertyInfo,
	&LifetimeServices_t1972____SponsorshipTimeout_PropertyInfo,
	NULL
};
static const Il2CppMethodReference LifetimeServices_t1972_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool LifetimeServices_t1972_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType LifetimeServices_t1972_0_0_0;
extern const Il2CppType LifetimeServices_t1972_1_0_0;
struct LifetimeServices_t1972;
const Il2CppTypeDefinitionMetadata LifetimeServices_t1972_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, LifetimeServices_t1972_VTable/* vtableMethods */
	, LifetimeServices_t1972_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1946/* fieldStart */

};
TypeInfo LifetimeServices_t1972_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "LifetimeServices"/* name */
	, "System.Runtime.Remoting.Lifetime"/* namespaze */
	, LifetimeServices_t1972_MethodInfos/* methods */
	, LifetimeServices_t1972_PropertyInfos/* properties */
	, NULL/* events */
	, &LifetimeServices_t1972_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 566/* custom_attributes_cache */
	, &LifetimeServices_t1972_0_0_0/* byval_arg */
	, &LifetimeServices_t1972_1_0_0/* this_arg */
	, &LifetimeServices_t1972_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LifetimeServices_t1972)/* instance_size */
	, sizeof (LifetimeServices_t1972)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(LifetimeServices_t1972_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 4/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.ArgInfoType
#include "mscorlib_System_Runtime_Remoting_Messaging_ArgInfoType.h"
// Metadata Definition System.Runtime.Remoting.Messaging.ArgInfoType
extern TypeInfo ArgInfoType_t1973_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.ArgInfoType
#include "mscorlib_System_Runtime_Remoting_Messaging_ArgInfoTypeMethodDeclarations.h"
static const MethodInfo* ArgInfoType_t1973_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference ArgInfoType_t1973_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool ArgInfoType_t1973_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ArgInfoType_t1973_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ArgInfoType_t1973_0_0_0;
extern const Il2CppType ArgInfoType_t1973_1_0_0;
// System.Byte
#include "mscorlib_System_Byte.h"
extern TypeInfo Byte_t680_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata ArgInfoType_t1973_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ArgInfoType_t1973_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, ArgInfoType_t1973_VTable/* vtableMethods */
	, ArgInfoType_t1973_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1951/* fieldStart */

};
TypeInfo ArgInfoType_t1973_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ArgInfoType"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, ArgInfoType_t1973_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Byte_t680_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ArgInfoType_t1973_0_0_0/* byval_arg */
	, &ArgInfoType_t1973_1_0_0/* this_arg */
	, &ArgInfoType_t1973_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ArgInfoType_t1973)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ArgInfoType_t1973)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint8_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.ArgInfo
#include "mscorlib_System_Runtime_Remoting_Messaging_ArgInfo.h"
// Metadata Definition System.Runtime.Remoting.Messaging.ArgInfo
extern TypeInfo ArgInfo_t1974_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.ArgInfo
#include "mscorlib_System_Runtime_Remoting_Messaging_ArgInfoMethodDeclarations.h"
extern const Il2CppType MethodBase_t1102_0_0_0;
extern const Il2CppType MethodBase_t1102_0_0_0;
extern const Il2CppType ArgInfoType_t1973_0_0_0;
static const ParameterInfo ArgInfo_t1974_ArgInfo__ctor_m10528_ParameterInfos[] = 
{
	{"method", 0, 134222312, 0, &MethodBase_t1102_0_0_0},
	{"type", 1, 134222313, 0, &ArgInfoType_t1973_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Byte_t680 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.ArgInfo::.ctor(System.Reflection.MethodBase,System.Runtime.Remoting.Messaging.ArgInfoType)
extern const MethodInfo ArgInfo__ctor_m10528_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ArgInfo__ctor_m10528/* method */
	, &ArgInfo_t1974_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Byte_t680/* invoker_method */
	, ArgInfo_t1974_ArgInfo__ctor_m10528_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3765/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Runtime.Remoting.Messaging.ArgInfo::GetInOutArgCount()
extern const MethodInfo ArgInfo_GetInOutArgCount_m10529_MethodInfo = 
{
	"GetInOutArgCount"/* name */
	, (methodPointerType)&ArgInfo_GetInOutArgCount_m10529/* method */
	, &ArgInfo_t1974_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3766/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectU5BU5D_t224_0_0_0;
static const ParameterInfo ArgInfo_t1974_ArgInfo_GetInOutArgs_m10530_ParameterInfos[] = 
{
	{"args", 0, 134222314, 0, &ObjectU5BU5D_t224_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Runtime.Remoting.Messaging.ArgInfo::GetInOutArgs(System.Object[])
extern const MethodInfo ArgInfo_GetInOutArgs_m10530_MethodInfo = 
{
	"GetInOutArgs"/* name */
	, (methodPointerType)&ArgInfo_GetInOutArgs_m10530/* method */
	, &ArgInfo_t1974_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t224_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ArgInfo_t1974_ArgInfo_GetInOutArgs_m10530_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3767/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ArgInfo_t1974_MethodInfos[] =
{
	&ArgInfo__ctor_m10528_MethodInfo,
	&ArgInfo_GetInOutArgCount_m10529_MethodInfo,
	&ArgInfo_GetInOutArgs_m10530_MethodInfo,
	NULL
};
static const Il2CppMethodReference ArgInfo_t1974_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool ArgInfo_t1974_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ArgInfo_t1974_0_0_0;
extern const Il2CppType ArgInfo_t1974_1_0_0;
struct ArgInfo_t1974;
const Il2CppTypeDefinitionMetadata ArgInfo_t1974_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ArgInfo_t1974_VTable/* vtableMethods */
	, ArgInfo_t1974_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1954/* fieldStart */

};
TypeInfo ArgInfo_t1974_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ArgInfo"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, ArgInfo_t1974_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ArgInfo_t1974_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ArgInfo_t1974_0_0_0/* byval_arg */
	, &ArgInfo_t1974_1_0_0/* this_arg */
	, &ArgInfo_t1974_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ArgInfo_t1974)/* instance_size */
	, sizeof (ArgInfo_t1974)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.AsyncResult
#include "mscorlib_System_Runtime_Remoting_Messaging_AsyncResult.h"
// Metadata Definition System.Runtime.Remoting.Messaging.AsyncResult
extern TypeInfo AsyncResult_t1979_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.AsyncResult
#include "mscorlib_System_Runtime_Remoting_Messaging_AsyncResultMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.AsyncResult::.ctor()
extern const MethodInfo AsyncResult__ctor_m10531_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AsyncResult__ctor_m10531/* method */
	, &AsyncResult_t1979_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3768/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.AsyncResult::get_AsyncState()
extern const MethodInfo AsyncResult_get_AsyncState_m10532_MethodInfo = 
{
	"get_AsyncState"/* name */
	, (methodPointerType)&AsyncResult_get_AsyncState_m10532/* method */
	, &AsyncResult_t1979_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3769/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType WaitHandle_t1637_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Threading.WaitHandle System.Runtime.Remoting.Messaging.AsyncResult::get_AsyncWaitHandle()
extern const MethodInfo AsyncResult_get_AsyncWaitHandle_m10533_MethodInfo = 
{
	"get_AsyncWaitHandle"/* name */
	, (methodPointerType)&AsyncResult_get_AsyncWaitHandle_m10533/* method */
	, &AsyncResult_t1979_il2cpp_TypeInfo/* declaring_type */
	, &WaitHandle_t1637_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3770/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.Messaging.AsyncResult::get_CompletedSynchronously()
extern const MethodInfo AsyncResult_get_CompletedSynchronously_m10534_MethodInfo = 
{
	"get_CompletedSynchronously"/* name */
	, (methodPointerType)&AsyncResult_get_CompletedSynchronously_m10534/* method */
	, &AsyncResult_t1979_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3771/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.Messaging.AsyncResult::get_IsCompleted()
extern const MethodInfo AsyncResult_get_IsCompleted_m10535_MethodInfo = 
{
	"get_IsCompleted"/* name */
	, (methodPointerType)&AsyncResult_get_IsCompleted_m10535/* method */
	, &AsyncResult_t1979_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3772/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.Messaging.AsyncResult::get_EndInvokeCalled()
extern const MethodInfo AsyncResult_get_EndInvokeCalled_m10536_MethodInfo = 
{
	"get_EndInvokeCalled"/* name */
	, (methodPointerType)&AsyncResult_get_EndInvokeCalled_m10536/* method */
	, &AsyncResult_t1979_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3773/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo AsyncResult_t1979_AsyncResult_set_EndInvokeCalled_m10537_ParameterInfos[] = 
{
	{"value", 0, 134222315, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.AsyncResult::set_EndInvokeCalled(System.Boolean)
extern const MethodInfo AsyncResult_set_EndInvokeCalled_m10537_MethodInfo = 
{
	"set_EndInvokeCalled"/* name */
	, (methodPointerType)&AsyncResult_set_EndInvokeCalled_m10537/* method */
	, &AsyncResult_t1979_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274/* invoker_method */
	, AsyncResult_t1979_AsyncResult_set_EndInvokeCalled_m10537_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3774/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.AsyncResult::get_AsyncDelegate()
extern const MethodInfo AsyncResult_get_AsyncDelegate_m10538_MethodInfo = 
{
	"get_AsyncDelegate"/* name */
	, (methodPointerType)&AsyncResult_get_AsyncDelegate_m10538/* method */
	, &AsyncResult_t1979_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3775/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Messaging.IMessageSink System.Runtime.Remoting.Messaging.AsyncResult::get_NextSink()
extern const MethodInfo AsyncResult_get_NextSink_m10539_MethodInfo = 
{
	"get_NextSink"/* name */
	, (methodPointerType)&AsyncResult_get_NextSink_m10539/* method */
	, &AsyncResult_t1979_il2cpp_TypeInfo/* declaring_type */
	, &IMessageSink_t1516_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3776/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IMessage_t1978_0_0_0;
extern const Il2CppType IMessageSink_t1516_0_0_0;
static const ParameterInfo AsyncResult_t1979_AsyncResult_AsyncProcessMessage_m10540_ParameterInfos[] = 
{
	{"msg", 0, 134222316, 0, &IMessage_t1978_0_0_0},
	{"replySink", 1, 134222317, 0, &IMessageSink_t1516_0_0_0},
};
extern const Il2CppType IMessageCtrl_t1977_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Messaging.IMessageCtrl System.Runtime.Remoting.Messaging.AsyncResult::AsyncProcessMessage(System.Runtime.Remoting.Messaging.IMessage,System.Runtime.Remoting.Messaging.IMessageSink)
extern const MethodInfo AsyncResult_AsyncProcessMessage_m10540_MethodInfo = 
{
	"AsyncProcessMessage"/* name */
	, (methodPointerType)&AsyncResult_AsyncProcessMessage_m10540/* method */
	, &AsyncResult_t1979_il2cpp_TypeInfo/* declaring_type */
	, &IMessageCtrl_t1977_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, AsyncResult_t1979_AsyncResult_AsyncProcessMessage_m10540_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3777/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Messaging.IMessage System.Runtime.Remoting.Messaging.AsyncResult::GetReplyMessage()
extern const MethodInfo AsyncResult_GetReplyMessage_m10541_MethodInfo = 
{
	"GetReplyMessage"/* name */
	, (methodPointerType)&AsyncResult_GetReplyMessage_m10541/* method */
	, &AsyncResult_t1979_il2cpp_TypeInfo/* declaring_type */
	, &IMessage_t1978_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3778/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IMessageCtrl_t1977_0_0_0;
static const ParameterInfo AsyncResult_t1979_AsyncResult_SetMessageCtrl_m10542_ParameterInfos[] = 
{
	{"mc", 0, 134222318, 0, &IMessageCtrl_t1977_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.AsyncResult::SetMessageCtrl(System.Runtime.Remoting.Messaging.IMessageCtrl)
extern const MethodInfo AsyncResult_SetMessageCtrl_m10542_MethodInfo = 
{
	"SetMessageCtrl"/* name */
	, (methodPointerType)&AsyncResult_SetMessageCtrl_m10542/* method */
	, &AsyncResult_t1979_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, AsyncResult_t1979_AsyncResult_SetMessageCtrl_m10542_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3779/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo AsyncResult_t1979_AsyncResult_SetCompletedSynchronously_m10543_ParameterInfos[] = 
{
	{"completed", 0, 134222319, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.AsyncResult::SetCompletedSynchronously(System.Boolean)
extern const MethodInfo AsyncResult_SetCompletedSynchronously_m10543_MethodInfo = 
{
	"SetCompletedSynchronously"/* name */
	, (methodPointerType)&AsyncResult_SetCompletedSynchronously_m10543/* method */
	, &AsyncResult_t1979_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274/* invoker_method */
	, AsyncResult_t1979_AsyncResult_SetCompletedSynchronously_m10543_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3780/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Messaging.IMessage System.Runtime.Remoting.Messaging.AsyncResult::EndInvoke()
extern const MethodInfo AsyncResult_EndInvoke_m10544_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&AsyncResult_EndInvoke_m10544/* method */
	, &AsyncResult_t1979_il2cpp_TypeInfo/* declaring_type */
	, &IMessage_t1978_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3781/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IMessage_t1978_0_0_0;
static const ParameterInfo AsyncResult_t1979_AsyncResult_SyncProcessMessage_m10545_ParameterInfos[] = 
{
	{"msg", 0, 134222320, 0, &IMessage_t1978_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Messaging.IMessage System.Runtime.Remoting.Messaging.AsyncResult::SyncProcessMessage(System.Runtime.Remoting.Messaging.IMessage)
extern const MethodInfo AsyncResult_SyncProcessMessage_m10545_MethodInfo = 
{
	"SyncProcessMessage"/* name */
	, (methodPointerType)&AsyncResult_SyncProcessMessage_m10545/* method */
	, &AsyncResult_t1979_il2cpp_TypeInfo/* declaring_type */
	, &IMessage_t1978_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, AsyncResult_t1979_AsyncResult_SyncProcessMessage_m10545_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3782/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType MonoMethodMessage_t1976_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Messaging.MonoMethodMessage System.Runtime.Remoting.Messaging.AsyncResult::get_CallMessage()
extern const MethodInfo AsyncResult_get_CallMessage_m10546_MethodInfo = 
{
	"get_CallMessage"/* name */
	, (methodPointerType)&AsyncResult_get_CallMessage_m10546/* method */
	, &AsyncResult_t1979_il2cpp_TypeInfo/* declaring_type */
	, &MonoMethodMessage_t1976_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2179/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3783/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType MonoMethodMessage_t1976_0_0_0;
static const ParameterInfo AsyncResult_t1979_AsyncResult_set_CallMessage_m10547_ParameterInfos[] = 
{
	{"value", 0, 134222321, 0, &MonoMethodMessage_t1976_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.AsyncResult::set_CallMessage(System.Runtime.Remoting.Messaging.MonoMethodMessage)
extern const MethodInfo AsyncResult_set_CallMessage_m10547_MethodInfo = 
{
	"set_CallMessage"/* name */
	, (methodPointerType)&AsyncResult_set_CallMessage_m10547/* method */
	, &AsyncResult_t1979_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, AsyncResult_t1979_AsyncResult_set_CallMessage_m10547_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2179/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3784/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AsyncResult_t1979_MethodInfos[] =
{
	&AsyncResult__ctor_m10531_MethodInfo,
	&AsyncResult_get_AsyncState_m10532_MethodInfo,
	&AsyncResult_get_AsyncWaitHandle_m10533_MethodInfo,
	&AsyncResult_get_CompletedSynchronously_m10534_MethodInfo,
	&AsyncResult_get_IsCompleted_m10535_MethodInfo,
	&AsyncResult_get_EndInvokeCalled_m10536_MethodInfo,
	&AsyncResult_set_EndInvokeCalled_m10537_MethodInfo,
	&AsyncResult_get_AsyncDelegate_m10538_MethodInfo,
	&AsyncResult_get_NextSink_m10539_MethodInfo,
	&AsyncResult_AsyncProcessMessage_m10540_MethodInfo,
	&AsyncResult_GetReplyMessage_m10541_MethodInfo,
	&AsyncResult_SetMessageCtrl_m10542_MethodInfo,
	&AsyncResult_SetCompletedSynchronously_m10543_MethodInfo,
	&AsyncResult_EndInvoke_m10544_MethodInfo,
	&AsyncResult_SyncProcessMessage_m10545_MethodInfo,
	&AsyncResult_get_CallMessage_m10546_MethodInfo,
	&AsyncResult_set_CallMessage_m10547_MethodInfo,
	NULL
};
extern const MethodInfo AsyncResult_get_AsyncState_m10532_MethodInfo;
static const PropertyInfo AsyncResult_t1979____AsyncState_PropertyInfo = 
{
	&AsyncResult_t1979_il2cpp_TypeInfo/* parent */
	, "AsyncState"/* name */
	, &AsyncResult_get_AsyncState_m10532_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AsyncResult_get_AsyncWaitHandle_m10533_MethodInfo;
static const PropertyInfo AsyncResult_t1979____AsyncWaitHandle_PropertyInfo = 
{
	&AsyncResult_t1979_il2cpp_TypeInfo/* parent */
	, "AsyncWaitHandle"/* name */
	, &AsyncResult_get_AsyncWaitHandle_m10533_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AsyncResult_get_CompletedSynchronously_m10534_MethodInfo;
static const PropertyInfo AsyncResult_t1979____CompletedSynchronously_PropertyInfo = 
{
	&AsyncResult_t1979_il2cpp_TypeInfo/* parent */
	, "CompletedSynchronously"/* name */
	, &AsyncResult_get_CompletedSynchronously_m10534_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AsyncResult_get_IsCompleted_m10535_MethodInfo;
static const PropertyInfo AsyncResult_t1979____IsCompleted_PropertyInfo = 
{
	&AsyncResult_t1979_il2cpp_TypeInfo/* parent */
	, "IsCompleted"/* name */
	, &AsyncResult_get_IsCompleted_m10535_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AsyncResult_get_EndInvokeCalled_m10536_MethodInfo;
extern const MethodInfo AsyncResult_set_EndInvokeCalled_m10537_MethodInfo;
static const PropertyInfo AsyncResult_t1979____EndInvokeCalled_PropertyInfo = 
{
	&AsyncResult_t1979_il2cpp_TypeInfo/* parent */
	, "EndInvokeCalled"/* name */
	, &AsyncResult_get_EndInvokeCalled_m10536_MethodInfo/* get */
	, &AsyncResult_set_EndInvokeCalled_m10537_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AsyncResult_get_AsyncDelegate_m10538_MethodInfo;
static const PropertyInfo AsyncResult_t1979____AsyncDelegate_PropertyInfo = 
{
	&AsyncResult_t1979_il2cpp_TypeInfo/* parent */
	, "AsyncDelegate"/* name */
	, &AsyncResult_get_AsyncDelegate_m10538_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AsyncResult_get_NextSink_m10539_MethodInfo;
static const PropertyInfo AsyncResult_t1979____NextSink_PropertyInfo = 
{
	&AsyncResult_t1979_il2cpp_TypeInfo/* parent */
	, "NextSink"/* name */
	, &AsyncResult_get_NextSink_m10539_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AsyncResult_get_CallMessage_m10546_MethodInfo;
extern const MethodInfo AsyncResult_set_CallMessage_m10547_MethodInfo;
static const PropertyInfo AsyncResult_t1979____CallMessage_PropertyInfo = 
{
	&AsyncResult_t1979_il2cpp_TypeInfo/* parent */
	, "CallMessage"/* name */
	, &AsyncResult_get_CallMessage_m10546_MethodInfo/* get */
	, &AsyncResult_set_CallMessage_m10547_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* AsyncResult_t1979_PropertyInfos[] =
{
	&AsyncResult_t1979____AsyncState_PropertyInfo,
	&AsyncResult_t1979____AsyncWaitHandle_PropertyInfo,
	&AsyncResult_t1979____CompletedSynchronously_PropertyInfo,
	&AsyncResult_t1979____IsCompleted_PropertyInfo,
	&AsyncResult_t1979____EndInvokeCalled_PropertyInfo,
	&AsyncResult_t1979____AsyncDelegate_PropertyInfo,
	&AsyncResult_t1979____NextSink_PropertyInfo,
	&AsyncResult_t1979____CallMessage_PropertyInfo,
	NULL
};
extern const MethodInfo AsyncResult_AsyncProcessMessage_m10540_MethodInfo;
extern const MethodInfo AsyncResult_GetReplyMessage_m10541_MethodInfo;
extern const MethodInfo AsyncResult_SetMessageCtrl_m10542_MethodInfo;
extern const MethodInfo AsyncResult_SyncProcessMessage_m10545_MethodInfo;
static const Il2CppMethodReference AsyncResult_t1979_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&AsyncResult_get_AsyncState_m10532_MethodInfo,
	&AsyncResult_get_AsyncWaitHandle_m10533_MethodInfo,
	&AsyncResult_get_IsCompleted_m10535_MethodInfo,
	&AsyncResult_get_AsyncState_m10532_MethodInfo,
	&AsyncResult_get_AsyncWaitHandle_m10533_MethodInfo,
	&AsyncResult_get_CompletedSynchronously_m10534_MethodInfo,
	&AsyncResult_get_IsCompleted_m10535_MethodInfo,
	&AsyncResult_get_AsyncDelegate_m10538_MethodInfo,
	&AsyncResult_get_NextSink_m10539_MethodInfo,
	&AsyncResult_AsyncProcessMessage_m10540_MethodInfo,
	&AsyncResult_GetReplyMessage_m10541_MethodInfo,
	&AsyncResult_SetMessageCtrl_m10542_MethodInfo,
	&AsyncResult_SyncProcessMessage_m10545_MethodInfo,
};
static bool AsyncResult_t1979_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* AsyncResult_t1979_InterfacesTypeInfos[] = 
{
	&IAsyncResult_t546_0_0_0,
	&IMessageSink_t1516_0_0_0,
};
static Il2CppInterfaceOffsetPair AsyncResult_t1979_InterfacesOffsets[] = 
{
	{ &IAsyncResult_t546_0_0_0, 4},
	{ &IMessageSink_t1516_0_0_0, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AsyncResult_t1979_0_0_0;
extern const Il2CppType AsyncResult_t1979_1_0_0;
struct AsyncResult_t1979;
const Il2CppTypeDefinitionMetadata AsyncResult_t1979_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, AsyncResult_t1979_InterfacesTypeInfos/* implementedInterfaces */
	, AsyncResult_t1979_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, AsyncResult_t1979_VTable/* vtableMethods */
	, AsyncResult_t1979_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1957/* fieldStart */

};
TypeInfo AsyncResult_t1979_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AsyncResult"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, AsyncResult_t1979_MethodInfos/* methods */
	, AsyncResult_t1979_PropertyInfos/* properties */
	, NULL/* events */
	, &AsyncResult_t1979_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 567/* custom_attributes_cache */
	, &AsyncResult_t1979_0_0_0/* byval_arg */
	, &AsyncResult_t1979_1_0_0/* this_arg */
	, &AsyncResult_t1979_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AsyncResult_t1979)/* instance_size */
	, sizeof (AsyncResult_t1979)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 17/* method_count */
	, 8/* property_count */
	, 15/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 17/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.ConstructionCall
#include "mscorlib_System_Runtime_Remoting_Messaging_ConstructionCall.h"
// Metadata Definition System.Runtime.Remoting.Messaging.ConstructionCall
extern TypeInfo ConstructionCall_t1980_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.ConstructionCall
#include "mscorlib_System_Runtime_Remoting_Messaging_ConstructionCallMethodDeclarations.h"
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo ConstructionCall_t1980_ConstructionCall__ctor_m10548_ParameterInfos[] = 
{
	{"type", 0, 134222322, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.ConstructionCall::.ctor(System.Type)
extern const MethodInfo ConstructionCall__ctor_m10548_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ConstructionCall__ctor_m10548/* method */
	, &ConstructionCall_t1980_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, ConstructionCall_t1980_ConstructionCall__ctor_m10548_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3785/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1043_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
static const ParameterInfo ConstructionCall_t1980_ConstructionCall__ctor_m10549_ParameterInfos[] = 
{
	{"info", 0, 134222323, 0, &SerializationInfo_t1043_0_0_0},
	{"context", 1, 134222324, 0, &StreamingContext_t1044_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.ConstructionCall::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo ConstructionCall__ctor_m10549_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ConstructionCall__ctor_m10549/* method */
	, &ConstructionCall_t1980_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044/* invoker_method */
	, ConstructionCall_t1980_ConstructionCall__ctor_m10549_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3786/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.ConstructionCall::InitDictionary()
extern const MethodInfo ConstructionCall_InitDictionary_m10550_MethodInfo = 
{
	"InitDictionary"/* name */
	, (methodPointerType)&ConstructionCall_InitDictionary_m10550/* method */
	, &ConstructionCall_t1980_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 195/* flags */
	, 0/* iflags */
	, 19/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3787/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo ConstructionCall_t1980_ConstructionCall_set_IsContextOk_m10551_ParameterInfos[] = 
{
	{"value", 0, 134222325, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.ConstructionCall::set_IsContextOk(System.Boolean)
extern const MethodInfo ConstructionCall_set_IsContextOk_m10551_MethodInfo = 
{
	"set_IsContextOk"/* name */
	, (methodPointerType)&ConstructionCall_set_IsContextOk_m10551/* method */
	, &ConstructionCall_t1980_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274/* invoker_method */
	, ConstructionCall_t1980_ConstructionCall_set_IsContextOk_m10551_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2179/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3788/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Runtime.Remoting.Messaging.ConstructionCall::get_ActivationType()
extern const MethodInfo ConstructionCall_get_ActivationType_m10552_MethodInfo = 
{
	"get_ActivationType"/* name */
	, (methodPointerType)&ConstructionCall_get_ActivationType_m10552/* method */
	, &ConstructionCall_t1980_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 22/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3789/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Messaging.ConstructionCall::get_ActivationTypeName()
extern const MethodInfo ConstructionCall_get_ActivationTypeName_m10553_MethodInfo = 
{
	"get_ActivationTypeName"/* name */
	, (methodPointerType)&ConstructionCall_get_ActivationTypeName_m10553/* method */
	, &ConstructionCall_t1980_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 23/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3790/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Activation.IActivator System.Runtime.Remoting.Messaging.ConstructionCall::get_Activator()
extern const MethodInfo ConstructionCall_get_Activator_m10554_MethodInfo = 
{
	"get_Activator"/* name */
	, (methodPointerType)&ConstructionCall_get_Activator_m10554/* method */
	, &ConstructionCall_t1980_il2cpp_TypeInfo/* declaring_type */
	, &IActivator_t1949_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 24/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3791/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IActivator_t1949_0_0_0;
static const ParameterInfo ConstructionCall_t1980_ConstructionCall_set_Activator_m10555_ParameterInfos[] = 
{
	{"value", 0, 134222326, 0, &IActivator_t1949_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.ConstructionCall::set_Activator(System.Runtime.Remoting.Activation.IActivator)
extern const MethodInfo ConstructionCall_set_Activator_m10555_MethodInfo = 
{
	"set_Activator"/* name */
	, (methodPointerType)&ConstructionCall_set_Activator_m10555/* method */
	, &ConstructionCall_t1980_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, ConstructionCall_t1980_ConstructionCall_set_Activator_m10555_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 25/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3792/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Runtime.Remoting.Messaging.ConstructionCall::get_CallSiteActivationAttributes()
extern const MethodInfo ConstructionCall_get_CallSiteActivationAttributes_m10556_MethodInfo = 
{
	"get_CallSiteActivationAttributes"/* name */
	, (methodPointerType)&ConstructionCall_get_CallSiteActivationAttributes_m10556/* method */
	, &ConstructionCall_t1980_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t224_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3793/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectU5BU5D_t224_0_0_0;
static const ParameterInfo ConstructionCall_t1980_ConstructionCall_SetActivationAttributes_m10557_ParameterInfos[] = 
{
	{"attributes", 0, 134222327, 0, &ObjectU5BU5D_t224_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.ConstructionCall::SetActivationAttributes(System.Object[])
extern const MethodInfo ConstructionCall_SetActivationAttributes_m10557_MethodInfo = 
{
	"SetActivationAttributes"/* name */
	, (methodPointerType)&ConstructionCall_SetActivationAttributes_m10557/* method */
	, &ConstructionCall_t1980_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, ConstructionCall_t1980_ConstructionCall_SetActivationAttributes_m10557_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3794/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.IList System.Runtime.Remoting.Messaging.ConstructionCall::get_ContextProperties()
extern const MethodInfo ConstructionCall_get_ContextProperties_m10558_MethodInfo = 
{
	"get_ContextProperties"/* name */
	, (methodPointerType)&ConstructionCall_get_ContextProperties_m10558/* method */
	, &ConstructionCall_t1980_il2cpp_TypeInfo/* declaring_type */
	, &IList_t1195_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3795/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo ConstructionCall_t1980_ConstructionCall_InitMethodProperty_m10559_ParameterInfos[] = 
{
	{"key", 0, 134222328, 0, &String_t_0_0_0},
	{"value", 1, 134222329, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.ConstructionCall::InitMethodProperty(System.String,System.Object)
extern const MethodInfo ConstructionCall_InitMethodProperty_m10559_MethodInfo = 
{
	"InitMethodProperty"/* name */
	, (methodPointerType)&ConstructionCall_InitMethodProperty_m10559/* method */
	, &ConstructionCall_t1980_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, ConstructionCall_t1980_ConstructionCall_InitMethodProperty_m10559_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 195/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3796/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1043_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
static const ParameterInfo ConstructionCall_t1980_ConstructionCall_GetObjectData_m10560_ParameterInfos[] = 
{
	{"info", 0, 134222330, 0, &SerializationInfo_t1043_0_0_0},
	{"context", 1, 134222331, 0, &StreamingContext_t1044_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.ConstructionCall::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo ConstructionCall_GetObjectData_m10560_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&ConstructionCall_GetObjectData_m10560/* method */
	, &ConstructionCall_t1980_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044/* invoker_method */
	, ConstructionCall_t1980_ConstructionCall_GetObjectData_m10560_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3797/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IDictionary_t444_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.IDictionary System.Runtime.Remoting.Messaging.ConstructionCall::get_Properties()
extern const MethodInfo ConstructionCall_get_Properties_m10561_MethodInfo = 
{
	"get_Properties"/* name */
	, (methodPointerType)&ConstructionCall_get_Properties_m10561/* method */
	, &ConstructionCall_t1980_il2cpp_TypeInfo/* declaring_type */
	, &IDictionary_t444_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 18/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3798/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ConstructionCall_t1980_MethodInfos[] =
{
	&ConstructionCall__ctor_m10548_MethodInfo,
	&ConstructionCall__ctor_m10549_MethodInfo,
	&ConstructionCall_InitDictionary_m10550_MethodInfo,
	&ConstructionCall_set_IsContextOk_m10551_MethodInfo,
	&ConstructionCall_get_ActivationType_m10552_MethodInfo,
	&ConstructionCall_get_ActivationTypeName_m10553_MethodInfo,
	&ConstructionCall_get_Activator_m10554_MethodInfo,
	&ConstructionCall_set_Activator_m10555_MethodInfo,
	&ConstructionCall_get_CallSiteActivationAttributes_m10556_MethodInfo,
	&ConstructionCall_SetActivationAttributes_m10557_MethodInfo,
	&ConstructionCall_get_ContextProperties_m10558_MethodInfo,
	&ConstructionCall_InitMethodProperty_m10559_MethodInfo,
	&ConstructionCall_GetObjectData_m10560_MethodInfo,
	&ConstructionCall_get_Properties_m10561_MethodInfo,
	NULL
};
extern const MethodInfo ConstructionCall_set_IsContextOk_m10551_MethodInfo;
static const PropertyInfo ConstructionCall_t1980____IsContextOk_PropertyInfo = 
{
	&ConstructionCall_t1980_il2cpp_TypeInfo/* parent */
	, "IsContextOk"/* name */
	, NULL/* get */
	, &ConstructionCall_set_IsContextOk_m10551_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ConstructionCall_get_ActivationType_m10552_MethodInfo;
static const PropertyInfo ConstructionCall_t1980____ActivationType_PropertyInfo = 
{
	&ConstructionCall_t1980_il2cpp_TypeInfo/* parent */
	, "ActivationType"/* name */
	, &ConstructionCall_get_ActivationType_m10552_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ConstructionCall_get_ActivationTypeName_m10553_MethodInfo;
static const PropertyInfo ConstructionCall_t1980____ActivationTypeName_PropertyInfo = 
{
	&ConstructionCall_t1980_il2cpp_TypeInfo/* parent */
	, "ActivationTypeName"/* name */
	, &ConstructionCall_get_ActivationTypeName_m10553_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ConstructionCall_get_Activator_m10554_MethodInfo;
extern const MethodInfo ConstructionCall_set_Activator_m10555_MethodInfo;
static const PropertyInfo ConstructionCall_t1980____Activator_PropertyInfo = 
{
	&ConstructionCall_t1980_il2cpp_TypeInfo/* parent */
	, "Activator"/* name */
	, &ConstructionCall_get_Activator_m10554_MethodInfo/* get */
	, &ConstructionCall_set_Activator_m10555_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ConstructionCall_get_CallSiteActivationAttributes_m10556_MethodInfo;
static const PropertyInfo ConstructionCall_t1980____CallSiteActivationAttributes_PropertyInfo = 
{
	&ConstructionCall_t1980_il2cpp_TypeInfo/* parent */
	, "CallSiteActivationAttributes"/* name */
	, &ConstructionCall_get_CallSiteActivationAttributes_m10556_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ConstructionCall_get_ContextProperties_m10558_MethodInfo;
static const PropertyInfo ConstructionCall_t1980____ContextProperties_PropertyInfo = 
{
	&ConstructionCall_t1980_il2cpp_TypeInfo/* parent */
	, "ContextProperties"/* name */
	, &ConstructionCall_get_ContextProperties_m10558_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ConstructionCall_get_Properties_m10561_MethodInfo;
static const PropertyInfo ConstructionCall_t1980____Properties_PropertyInfo = 
{
	&ConstructionCall_t1980_il2cpp_TypeInfo/* parent */
	, "Properties"/* name */
	, &ConstructionCall_get_Properties_m10561_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ConstructionCall_t1980_PropertyInfos[] =
{
	&ConstructionCall_t1980____IsContextOk_PropertyInfo,
	&ConstructionCall_t1980____ActivationType_PropertyInfo,
	&ConstructionCall_t1980____ActivationTypeName_PropertyInfo,
	&ConstructionCall_t1980____Activator_PropertyInfo,
	&ConstructionCall_t1980____CallSiteActivationAttributes_PropertyInfo,
	&ConstructionCall_t1980____ContextProperties_PropertyInfo,
	&ConstructionCall_t1980____Properties_PropertyInfo,
	NULL
};
extern const MethodInfo ConstructionCall_GetObjectData_m10560_MethodInfo;
extern const MethodInfo MethodCall_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m10580_MethodInfo;
extern const MethodInfo MethodCall_get_ArgCount_m10583_MethodInfo;
extern const MethodInfo MethodCall_get_Args_m10584_MethodInfo;
extern const MethodInfo MethodCall_get_LogicalCallContext_m10585_MethodInfo;
extern const MethodInfo MethodCall_get_MethodBase_m10586_MethodInfo;
extern const MethodInfo MethodCall_get_MethodName_m10587_MethodInfo;
extern const MethodInfo MethodCall_get_MethodSignature_m10588_MethodInfo;
extern const MethodInfo MethodCall_get_TypeName_m10591_MethodInfo;
extern const MethodInfo MethodCall_get_Uri_m10592_MethodInfo;
extern const MethodInfo MethodCall_GetArg_m10594_MethodInfo;
extern const MethodInfo ConstructionCall_InitMethodProperty_m10559_MethodInfo;
extern const MethodInfo ConstructionCall_InitDictionary_m10550_MethodInfo;
extern const MethodInfo MethodCall_set_Uri_m10593_MethodInfo;
extern const MethodInfo MethodCall_Init_m10595_MethodInfo;
static const Il2CppMethodReference ConstructionCall_t1980_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&ConstructionCall_GetObjectData_m10560_MethodInfo,
	&MethodCall_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m10580_MethodInfo,
	&ConstructionCall_get_Properties_m10561_MethodInfo,
	&MethodCall_get_ArgCount_m10583_MethodInfo,
	&MethodCall_get_Args_m10584_MethodInfo,
	&MethodCall_get_LogicalCallContext_m10585_MethodInfo,
	&MethodCall_get_MethodBase_m10586_MethodInfo,
	&MethodCall_get_MethodName_m10587_MethodInfo,
	&MethodCall_get_MethodSignature_m10588_MethodInfo,
	&MethodCall_get_TypeName_m10591_MethodInfo,
	&MethodCall_get_Uri_m10592_MethodInfo,
	&MethodCall_GetArg_m10594_MethodInfo,
	&ConstructionCall_InitMethodProperty_m10559_MethodInfo,
	&ConstructionCall_GetObjectData_m10560_MethodInfo,
	&ConstructionCall_get_Properties_m10561_MethodInfo,
	&ConstructionCall_InitDictionary_m10550_MethodInfo,
	&MethodCall_set_Uri_m10593_MethodInfo,
	&MethodCall_Init_m10595_MethodInfo,
	&ConstructionCall_get_ActivationType_m10552_MethodInfo,
	&ConstructionCall_get_ActivationTypeName_m10553_MethodInfo,
	&ConstructionCall_get_Activator_m10554_MethodInfo,
	&ConstructionCall_set_Activator_m10555_MethodInfo,
	&ConstructionCall_get_CallSiteActivationAttributes_m10556_MethodInfo,
	&ConstructionCall_get_ContextProperties_m10558_MethodInfo,
};
static bool ConstructionCall_t1980_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* ConstructionCall_t1980_InterfacesTypeInfos[] = 
{
	&IConstructionCallMessage_t2284_0_0_0,
	&IMessage_t1978_0_0_0,
	&IMethodCallMessage_t2289_0_0_0,
	&IMethodMessage_t1990_0_0_0,
};
extern const Il2CppType IInternalMessage_t2303_0_0_0;
extern const Il2CppType ISerializationRootObject_t2384_0_0_0;
static Il2CppInterfaceOffsetPair ConstructionCall_t1980_InterfacesOffsets[] = 
{
	{ &ISerializable_t439_0_0_0, 4},
	{ &IInternalMessage_t2303_0_0_0, 5},
	{ &IMessage_t1978_0_0_0, 6},
	{ &IMethodCallMessage_t2289_0_0_0, 7},
	{ &IMethodMessage_t1990_0_0_0, 7},
	{ &ISerializationRootObject_t2384_0_0_0, 16},
	{ &IConstructionCallMessage_t2284_0_0_0, 22},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ConstructionCall_t1980_1_0_0;
extern const Il2CppType MethodCall_t1981_0_0_0;
struct ConstructionCall_t1980;
const Il2CppTypeDefinitionMetadata ConstructionCall_t1980_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ConstructionCall_t1980_InterfacesTypeInfos/* implementedInterfaces */
	, ConstructionCall_t1980_InterfacesOffsets/* interfaceOffsets */
	, &MethodCall_t1981_0_0_0/* parent */
	, ConstructionCall_t1980_VTable/* vtableMethods */
	, ConstructionCall_t1980_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1972/* fieldStart */

};
TypeInfo ConstructionCall_t1980_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ConstructionCall"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, ConstructionCall_t1980_MethodInfos/* methods */
	, ConstructionCall_t1980_PropertyInfos/* properties */
	, NULL/* events */
	, &ConstructionCall_t1980_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 568/* custom_attributes_cache */
	, &ConstructionCall_t1980_0_0_0/* byval_arg */
	, &ConstructionCall_t1980_1_0_0/* this_arg */
	, &ConstructionCall_t1980_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ConstructionCall_t1980)/* instance_size */
	, sizeof (ConstructionCall_t1980)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ConstructionCall_t1980_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 14/* method_count */
	, 7/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 28/* vtable_count */
	, 4/* interfaces_count */
	, 7/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.ConstructionCallDictionary
#include "mscorlib_System_Runtime_Remoting_Messaging_ConstructionCallD.h"
// Metadata Definition System.Runtime.Remoting.Messaging.ConstructionCallDictionary
extern TypeInfo ConstructionCallDictionary_t1982_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.ConstructionCallDictionary
#include "mscorlib_System_Runtime_Remoting_Messaging_ConstructionCallDMethodDeclarations.h"
extern const Il2CppType IConstructionCallMessage_t2284_0_0_0;
static const ParameterInfo ConstructionCallDictionary_t1982_ConstructionCallDictionary__ctor_m10562_ParameterInfos[] = 
{
	{"message", 0, 134222332, 0, &IConstructionCallMessage_t2284_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.ConstructionCallDictionary::.ctor(System.Runtime.Remoting.Activation.IConstructionCallMessage)
extern const MethodInfo ConstructionCallDictionary__ctor_m10562_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ConstructionCallDictionary__ctor_m10562/* method */
	, &ConstructionCallDictionary_t1982_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, ConstructionCallDictionary_t1982_ConstructionCallDictionary__ctor_m10562_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3799/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.ConstructionCallDictionary::.cctor()
extern const MethodInfo ConstructionCallDictionary__cctor_m10563_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&ConstructionCallDictionary__cctor_m10563/* method */
	, &ConstructionCallDictionary_t1982_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3800/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo ConstructionCallDictionary_t1982_ConstructionCallDictionary_GetMethodProperty_m10564_ParameterInfos[] = 
{
	{"key", 0, 134222333, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.ConstructionCallDictionary::GetMethodProperty(System.String)
extern const MethodInfo ConstructionCallDictionary_GetMethodProperty_m10564_MethodInfo = 
{
	"GetMethodProperty"/* name */
	, (methodPointerType)&ConstructionCallDictionary_GetMethodProperty_m10564/* method */
	, &ConstructionCallDictionary_t1982_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ConstructionCallDictionary_t1982_ConstructionCallDictionary_GetMethodProperty_m10564_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3801/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo ConstructionCallDictionary_t1982_ConstructionCallDictionary_SetMethodProperty_m10565_ParameterInfos[] = 
{
	{"key", 0, 134222334, 0, &String_t_0_0_0},
	{"value", 1, 134222335, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.ConstructionCallDictionary::SetMethodProperty(System.String,System.Object)
extern const MethodInfo ConstructionCallDictionary_SetMethodProperty_m10565_MethodInfo = 
{
	"SetMethodProperty"/* name */
	, (methodPointerType)&ConstructionCallDictionary_SetMethodProperty_m10565/* method */
	, &ConstructionCallDictionary_t1982_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, ConstructionCallDictionary_t1982_ConstructionCallDictionary_SetMethodProperty_m10565_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3802/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ConstructionCallDictionary_t1982_MethodInfos[] =
{
	&ConstructionCallDictionary__ctor_m10562_MethodInfo,
	&ConstructionCallDictionary__cctor_m10563_MethodInfo,
	&ConstructionCallDictionary_GetMethodProperty_m10564_MethodInfo,
	&ConstructionCallDictionary_SetMethodProperty_m10565_MethodInfo,
	NULL
};
extern const MethodInfo MethodDictionary_System_Collections_IEnumerable_GetEnumerator_m10609_MethodInfo;
extern const MethodInfo MethodDictionary_get_Count_m10622_MethodInfo;
extern const MethodInfo MethodDictionary_get_IsSynchronized_m10623_MethodInfo;
extern const MethodInfo MethodDictionary_get_SyncRoot_m10624_MethodInfo;
extern const MethodInfo MethodDictionary_CopyTo_m10625_MethodInfo;
extern const MethodInfo MethodDictionary_get_Item_m10614_MethodInfo;
extern const MethodInfo MethodDictionary_set_Item_m10615_MethodInfo;
extern const MethodInfo MethodDictionary_Add_m10619_MethodInfo;
extern const MethodInfo MethodDictionary_Contains_m10620_MethodInfo;
extern const MethodInfo MethodDictionary_GetEnumerator_m10626_MethodInfo;
extern const MethodInfo MethodDictionary_Remove_m10621_MethodInfo;
extern const MethodInfo MethodDictionary_AllocInternalProperties_m10611_MethodInfo;
extern const MethodInfo ConstructionCallDictionary_GetMethodProperty_m10564_MethodInfo;
extern const MethodInfo ConstructionCallDictionary_SetMethodProperty_m10565_MethodInfo;
extern const MethodInfo MethodDictionary_get_Values_m10618_MethodInfo;
static const Il2CppMethodReference ConstructionCallDictionary_t1982_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&MethodDictionary_System_Collections_IEnumerable_GetEnumerator_m10609_MethodInfo,
	&MethodDictionary_get_Count_m10622_MethodInfo,
	&MethodDictionary_get_IsSynchronized_m10623_MethodInfo,
	&MethodDictionary_get_SyncRoot_m10624_MethodInfo,
	&MethodDictionary_CopyTo_m10625_MethodInfo,
	&MethodDictionary_get_Item_m10614_MethodInfo,
	&MethodDictionary_set_Item_m10615_MethodInfo,
	&MethodDictionary_Add_m10619_MethodInfo,
	&MethodDictionary_Contains_m10620_MethodInfo,
	&MethodDictionary_GetEnumerator_m10626_MethodInfo,
	&MethodDictionary_Remove_m10621_MethodInfo,
	&MethodDictionary_AllocInternalProperties_m10611_MethodInfo,
	&ConstructionCallDictionary_GetMethodProperty_m10564_MethodInfo,
	&ConstructionCallDictionary_SetMethodProperty_m10565_MethodInfo,
	&MethodDictionary_get_Values_m10618_MethodInfo,
};
static bool ConstructionCallDictionary_t1982_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IEnumerable_t438_0_0_0;
extern const Il2CppType ICollection_t440_0_0_0;
static Il2CppInterfaceOffsetPair ConstructionCallDictionary_t1982_InterfacesOffsets[] = 
{
	{ &IEnumerable_t438_0_0_0, 4},
	{ &ICollection_t440_0_0_0, 5},
	{ &IDictionary_t444_0_0_0, 9},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ConstructionCallDictionary_t1982_0_0_0;
extern const Il2CppType ConstructionCallDictionary_t1982_1_0_0;
extern const Il2CppType MethodDictionary_t1983_0_0_0;
struct ConstructionCallDictionary_t1982;
const Il2CppTypeDefinitionMetadata ConstructionCallDictionary_t1982_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ConstructionCallDictionary_t1982_InterfacesOffsets/* interfaceOffsets */
	, &MethodDictionary_t1983_0_0_0/* parent */
	, ConstructionCallDictionary_t1982_VTable/* vtableMethods */
	, ConstructionCallDictionary_t1982_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1979/* fieldStart */

};
TypeInfo ConstructionCallDictionary_t1982_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ConstructionCallDictionary"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, ConstructionCallDictionary_t1982_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ConstructionCallDictionary_t1982_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ConstructionCallDictionary_t1982_0_0_0/* byval_arg */
	, &ConstructionCallDictionary_t1982_1_0_0/* this_arg */
	, &ConstructionCallDictionary_t1982_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ConstructionCallDictionary_t1982)/* instance_size */
	, sizeof (ConstructionCallDictionary_t1982)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ConstructionCallDictionary_t1982_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 19/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.EnvoyTerminatorSink
#include "mscorlib_System_Runtime_Remoting_Messaging_EnvoyTerminatorSi.h"
// Metadata Definition System.Runtime.Remoting.Messaging.EnvoyTerminatorSink
extern TypeInfo EnvoyTerminatorSink_t1984_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.EnvoyTerminatorSink
#include "mscorlib_System_Runtime_Remoting_Messaging_EnvoyTerminatorSiMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.EnvoyTerminatorSink::.ctor()
extern const MethodInfo EnvoyTerminatorSink__ctor_m10566_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&EnvoyTerminatorSink__ctor_m10566/* method */
	, &EnvoyTerminatorSink_t1984_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3803/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.EnvoyTerminatorSink::.cctor()
extern const MethodInfo EnvoyTerminatorSink__cctor_m10567_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&EnvoyTerminatorSink__cctor_m10567/* method */
	, &EnvoyTerminatorSink_t1984_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3804/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* EnvoyTerminatorSink_t1984_MethodInfos[] =
{
	&EnvoyTerminatorSink__ctor_m10566_MethodInfo,
	&EnvoyTerminatorSink__cctor_m10567_MethodInfo,
	NULL
};
static const Il2CppMethodReference EnvoyTerminatorSink_t1984_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool EnvoyTerminatorSink_t1984_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static const Il2CppType* EnvoyTerminatorSink_t1984_InterfacesTypeInfos[] = 
{
	&IMessageSink_t1516_0_0_0,
};
static Il2CppInterfaceOffsetPair EnvoyTerminatorSink_t1984_InterfacesOffsets[] = 
{
	{ &IMessageSink_t1516_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType EnvoyTerminatorSink_t1984_0_0_0;
extern const Il2CppType EnvoyTerminatorSink_t1984_1_0_0;
struct EnvoyTerminatorSink_t1984;
const Il2CppTypeDefinitionMetadata EnvoyTerminatorSink_t1984_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, EnvoyTerminatorSink_t1984_InterfacesTypeInfos/* implementedInterfaces */
	, EnvoyTerminatorSink_t1984_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, EnvoyTerminatorSink_t1984_VTable/* vtableMethods */
	, EnvoyTerminatorSink_t1984_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1982/* fieldStart */

};
TypeInfo EnvoyTerminatorSink_t1984_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "EnvoyTerminatorSink"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, EnvoyTerminatorSink_t1984_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &EnvoyTerminatorSink_t1984_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &EnvoyTerminatorSink_t1984_0_0_0/* byval_arg */
	, &EnvoyTerminatorSink_t1984_1_0_0/* this_arg */
	, &EnvoyTerminatorSink_t1984_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EnvoyTerminatorSink_t1984)/* instance_size */
	, sizeof (EnvoyTerminatorSink_t1984)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(EnvoyTerminatorSink_t1984_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.Header
#include "mscorlib_System_Runtime_Remoting_Messaging_Header.h"
// Metadata Definition System.Runtime.Remoting.Messaging.Header
extern TypeInfo Header_t1985_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.Header
#include "mscorlib_System_Runtime_Remoting_Messaging_HeaderMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Header_t1985_Header__ctor_m10568_ParameterInfos[] = 
{
	{"_Name", 0, 134222336, 0, &String_t_0_0_0},
	{"_Value", 1, 134222337, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.Header::.ctor(System.String,System.Object)
extern const MethodInfo Header__ctor_m10568_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Header__ctor_m10568/* method */
	, &Header_t1985_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, Header_t1985_Header__ctor_m10568_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3805/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo Header_t1985_Header__ctor_m10569_ParameterInfos[] = 
{
	{"_Name", 0, 134222338, 0, &String_t_0_0_0},
	{"_Value", 1, 134222339, 0, &Object_t_0_0_0},
	{"_MustUnderstand", 2, 134222340, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.Header::.ctor(System.String,System.Object,System.Boolean)
extern const MethodInfo Header__ctor_m10569_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Header__ctor_m10569/* method */
	, &Header_t1985_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t_SByte_t274/* invoker_method */
	, Header_t1985_Header__ctor_m10569_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3806/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Header_t1985_Header__ctor_m10570_ParameterInfos[] = 
{
	{"_Name", 0, 134222341, 0, &String_t_0_0_0},
	{"_Value", 1, 134222342, 0, &Object_t_0_0_0},
	{"_MustUnderstand", 2, 134222343, 0, &Boolean_t273_0_0_0},
	{"_HeaderNamespace", 3, 134222344, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t_SByte_t274_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.Header::.ctor(System.String,System.Object,System.Boolean,System.String)
extern const MethodInfo Header__ctor_m10570_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Header__ctor_m10570/* method */
	, &Header_t1985_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t_SByte_t274_Object_t/* invoker_method */
	, Header_t1985_Header__ctor_m10570_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3807/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Header_t1985_MethodInfos[] =
{
	&Header__ctor_m10568_MethodInfo,
	&Header__ctor_m10569_MethodInfo,
	&Header__ctor_m10570_MethodInfo,
	NULL
};
static const Il2CppMethodReference Header_t1985_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool Header_t1985_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Header_t1985_0_0_0;
extern const Il2CppType Header_t1985_1_0_0;
struct Header_t1985;
const Il2CppTypeDefinitionMetadata Header_t1985_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Header_t1985_VTable/* vtableMethods */
	, Header_t1985_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1983/* fieldStart */

};
TypeInfo Header_t1985_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Header"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, Header_t1985_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Header_t1985_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 572/* custom_attributes_cache */
	, &Header_t1985_0_0_0/* byval_arg */
	, &Header_t1985_1_0_0/* this_arg */
	, &Header_t1985_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Header_t1985)/* instance_size */
	, sizeof (Header_t1985)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Messaging.IInternalMessage
extern TypeInfo IInternalMessage_t2303_il2cpp_TypeInfo;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo IInternalMessage_t2303_IInternalMessage_set_Uri_m13205_ParameterInfos[] = 
{
	{"value", 0, 134222345, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.IInternalMessage::set_Uri(System.String)
extern const MethodInfo IInternalMessage_set_Uri_m13205_MethodInfo = 
{
	"set_Uri"/* name */
	, NULL/* method */
	, &IInternalMessage_t2303_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, IInternalMessage_t2303_IInternalMessage_set_Uri_m13205_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3808/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IInternalMessage_t2303_MethodInfos[] =
{
	&IInternalMessage_set_Uri_m13205_MethodInfo,
	NULL
};
extern const MethodInfo IInternalMessage_set_Uri_m13205_MethodInfo;
static const PropertyInfo IInternalMessage_t2303____Uri_PropertyInfo = 
{
	&IInternalMessage_t2303_il2cpp_TypeInfo/* parent */
	, "Uri"/* name */
	, NULL/* get */
	, &IInternalMessage_set_Uri_m13205_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* IInternalMessage_t2303_PropertyInfos[] =
{
	&IInternalMessage_t2303____Uri_PropertyInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IInternalMessage_t2303_1_0_0;
struct IInternalMessage_t2303;
const Il2CppTypeDefinitionMetadata IInternalMessage_t2303_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IInternalMessage_t2303_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IInternalMessage"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, IInternalMessage_t2303_MethodInfos/* methods */
	, IInternalMessage_t2303_PropertyInfos/* properties */
	, NULL/* events */
	, &IInternalMessage_t2303_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IInternalMessage_t2303_0_0_0/* byval_arg */
	, &IInternalMessage_t2303_1_0_0/* this_arg */
	, &IInternalMessage_t2303_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 160/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Messaging.IMessage
extern TypeInfo IMessage_t1978_il2cpp_TypeInfo;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.IDictionary System.Runtime.Remoting.Messaging.IMessage::get_Properties()
extern const MethodInfo IMessage_get_Properties_m13206_MethodInfo = 
{
	"get_Properties"/* name */
	, NULL/* method */
	, &IMessage_t1978_il2cpp_TypeInfo/* declaring_type */
	, &IDictionary_t444_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3809/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IMessage_t1978_MethodInfos[] =
{
	&IMessage_get_Properties_m13206_MethodInfo,
	NULL
};
extern const MethodInfo IMessage_get_Properties_m13206_MethodInfo;
static const PropertyInfo IMessage_t1978____Properties_PropertyInfo = 
{
	&IMessage_t1978_il2cpp_TypeInfo/* parent */
	, "Properties"/* name */
	, &IMessage_get_Properties_m13206_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* IMessage_t1978_PropertyInfos[] =
{
	&IMessage_t1978____Properties_PropertyInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IMessage_t1978_1_0_0;
struct IMessage_t1978;
const Il2CppTypeDefinitionMetadata IMessage_t1978_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IMessage_t1978_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IMessage"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, IMessage_t1978_MethodInfos/* methods */
	, IMessage_t1978_PropertyInfos/* properties */
	, NULL/* events */
	, &IMessage_t1978_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 573/* custom_attributes_cache */
	, &IMessage_t1978_0_0_0/* byval_arg */
	, &IMessage_t1978_1_0_0/* this_arg */
	, &IMessage_t1978_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Messaging.IMessageCtrl
extern TypeInfo IMessageCtrl_t1977_il2cpp_TypeInfo;
static const MethodInfo* IMessageCtrl_t1977_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IMessageCtrl_t1977_1_0_0;
struct IMessageCtrl_t1977;
const Il2CppTypeDefinitionMetadata IMessageCtrl_t1977_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IMessageCtrl_t1977_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IMessageCtrl"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, IMessageCtrl_t1977_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IMessageCtrl_t1977_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 574/* custom_attributes_cache */
	, &IMessageCtrl_t1977_0_0_0/* byval_arg */
	, &IMessageCtrl_t1977_1_0_0/* this_arg */
	, &IMessageCtrl_t1977_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Messaging.IMessageSink
extern TypeInfo IMessageSink_t1516_il2cpp_TypeInfo;
static const MethodInfo* IMessageSink_t1516_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IMessageSink_t1516_1_0_0;
struct IMessageSink_t1516;
const Il2CppTypeDefinitionMetadata IMessageSink_t1516_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IMessageSink_t1516_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IMessageSink"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, IMessageSink_t1516_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IMessageSink_t1516_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 575/* custom_attributes_cache */
	, &IMessageSink_t1516_0_0_0/* byval_arg */
	, &IMessageSink_t1516_1_0_0/* this_arg */
	, &IMessageSink_t1516_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Messaging.IMethodCallMessage
extern TypeInfo IMethodCallMessage_t2289_il2cpp_TypeInfo;
static const MethodInfo* IMethodCallMessage_t2289_MethodInfos[] =
{
	NULL
};
static const Il2CppType* IMethodCallMessage_t2289_InterfacesTypeInfos[] = 
{
	&IMessage_t1978_0_0_0,
	&IMethodMessage_t1990_0_0_0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IMethodCallMessage_t2289_1_0_0;
struct IMethodCallMessage_t2289;
const Il2CppTypeDefinitionMetadata IMethodCallMessage_t2289_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IMethodCallMessage_t2289_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IMethodCallMessage_t2289_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IMethodCallMessage"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, IMethodCallMessage_t2289_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IMethodCallMessage_t2289_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 576/* custom_attributes_cache */
	, &IMethodCallMessage_t2289_0_0_0/* byval_arg */
	, &IMethodCallMessage_t2289_1_0_0/* this_arg */
	, &IMethodCallMessage_t2289_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Messaging.IMethodMessage
extern TypeInfo IMethodMessage_t1990_il2cpp_TypeInfo;
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Runtime.Remoting.Messaging.IMethodMessage::get_ArgCount()
extern const MethodInfo IMethodMessage_get_ArgCount_m13207_MethodInfo = 
{
	"get_ArgCount"/* name */
	, NULL/* method */
	, &IMethodMessage_t1990_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3810/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Runtime.Remoting.Messaging.IMethodMessage::get_Args()
extern const MethodInfo IMethodMessage_get_Args_m13208_MethodInfo = 
{
	"get_Args"/* name */
	, NULL/* method */
	, &IMethodMessage_t1990_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t224_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3811/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType LogicalCallContext_t1987_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Messaging.LogicalCallContext System.Runtime.Remoting.Messaging.IMethodMessage::get_LogicalCallContext()
extern const MethodInfo IMethodMessage_get_LogicalCallContext_m13209_MethodInfo = 
{
	"get_LogicalCallContext"/* name */
	, NULL/* method */
	, &IMethodMessage_t1990_il2cpp_TypeInfo/* declaring_type */
	, &LogicalCallContext_t1987_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3812/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodBase System.Runtime.Remoting.Messaging.IMethodMessage::get_MethodBase()
extern const MethodInfo IMethodMessage_get_MethodBase_m13210_MethodInfo = 
{
	"get_MethodBase"/* name */
	, NULL/* method */
	, &IMethodMessage_t1990_il2cpp_TypeInfo/* declaring_type */
	, &MethodBase_t1102_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3813/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Messaging.IMethodMessage::get_MethodName()
extern const MethodInfo IMethodMessage_get_MethodName_m13211_MethodInfo = 
{
	"get_MethodName"/* name */
	, NULL/* method */
	, &IMethodMessage_t1990_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3814/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.IMethodMessage::get_MethodSignature()
extern const MethodInfo IMethodMessage_get_MethodSignature_m13212_MethodInfo = 
{
	"get_MethodSignature"/* name */
	, NULL/* method */
	, &IMethodMessage_t1990_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3815/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Messaging.IMethodMessage::get_TypeName()
extern const MethodInfo IMethodMessage_get_TypeName_m13213_MethodInfo = 
{
	"get_TypeName"/* name */
	, NULL/* method */
	, &IMethodMessage_t1990_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3816/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Messaging.IMethodMessage::get_Uri()
extern const MethodInfo IMethodMessage_get_Uri_m13214_MethodInfo = 
{
	"get_Uri"/* name */
	, NULL/* method */
	, &IMethodMessage_t1990_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3817/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo IMethodMessage_t1990_IMethodMessage_GetArg_m13215_ParameterInfos[] = 
{
	{"argNum", 0, 134222346, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.IMethodMessage::GetArg(System.Int32)
extern const MethodInfo IMethodMessage_GetArg_m13215_MethodInfo = 
{
	"GetArg"/* name */
	, NULL/* method */
	, &IMethodMessage_t1990_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t253/* invoker_method */
	, IMethodMessage_t1990_IMethodMessage_GetArg_m13215_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3818/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IMethodMessage_t1990_MethodInfos[] =
{
	&IMethodMessage_get_ArgCount_m13207_MethodInfo,
	&IMethodMessage_get_Args_m13208_MethodInfo,
	&IMethodMessage_get_LogicalCallContext_m13209_MethodInfo,
	&IMethodMessage_get_MethodBase_m13210_MethodInfo,
	&IMethodMessage_get_MethodName_m13211_MethodInfo,
	&IMethodMessage_get_MethodSignature_m13212_MethodInfo,
	&IMethodMessage_get_TypeName_m13213_MethodInfo,
	&IMethodMessage_get_Uri_m13214_MethodInfo,
	&IMethodMessage_GetArg_m13215_MethodInfo,
	NULL
};
extern const MethodInfo IMethodMessage_get_ArgCount_m13207_MethodInfo;
static const PropertyInfo IMethodMessage_t1990____ArgCount_PropertyInfo = 
{
	&IMethodMessage_t1990_il2cpp_TypeInfo/* parent */
	, "ArgCount"/* name */
	, &IMethodMessage_get_ArgCount_m13207_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo IMethodMessage_get_Args_m13208_MethodInfo;
static const PropertyInfo IMethodMessage_t1990____Args_PropertyInfo = 
{
	&IMethodMessage_t1990_il2cpp_TypeInfo/* parent */
	, "Args"/* name */
	, &IMethodMessage_get_Args_m13208_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo IMethodMessage_get_LogicalCallContext_m13209_MethodInfo;
static const PropertyInfo IMethodMessage_t1990____LogicalCallContext_PropertyInfo = 
{
	&IMethodMessage_t1990_il2cpp_TypeInfo/* parent */
	, "LogicalCallContext"/* name */
	, &IMethodMessage_get_LogicalCallContext_m13209_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo IMethodMessage_get_MethodBase_m13210_MethodInfo;
static const PropertyInfo IMethodMessage_t1990____MethodBase_PropertyInfo = 
{
	&IMethodMessage_t1990_il2cpp_TypeInfo/* parent */
	, "MethodBase"/* name */
	, &IMethodMessage_get_MethodBase_m13210_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo IMethodMessage_get_MethodName_m13211_MethodInfo;
static const PropertyInfo IMethodMessage_t1990____MethodName_PropertyInfo = 
{
	&IMethodMessage_t1990_il2cpp_TypeInfo/* parent */
	, "MethodName"/* name */
	, &IMethodMessage_get_MethodName_m13211_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo IMethodMessage_get_MethodSignature_m13212_MethodInfo;
static const PropertyInfo IMethodMessage_t1990____MethodSignature_PropertyInfo = 
{
	&IMethodMessage_t1990_il2cpp_TypeInfo/* parent */
	, "MethodSignature"/* name */
	, &IMethodMessage_get_MethodSignature_m13212_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo IMethodMessage_get_TypeName_m13213_MethodInfo;
static const PropertyInfo IMethodMessage_t1990____TypeName_PropertyInfo = 
{
	&IMethodMessage_t1990_il2cpp_TypeInfo/* parent */
	, "TypeName"/* name */
	, &IMethodMessage_get_TypeName_m13213_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo IMethodMessage_get_Uri_m13214_MethodInfo;
static const PropertyInfo IMethodMessage_t1990____Uri_PropertyInfo = 
{
	&IMethodMessage_t1990_il2cpp_TypeInfo/* parent */
	, "Uri"/* name */
	, &IMethodMessage_get_Uri_m13214_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* IMethodMessage_t1990_PropertyInfos[] =
{
	&IMethodMessage_t1990____ArgCount_PropertyInfo,
	&IMethodMessage_t1990____Args_PropertyInfo,
	&IMethodMessage_t1990____LogicalCallContext_PropertyInfo,
	&IMethodMessage_t1990____MethodBase_PropertyInfo,
	&IMethodMessage_t1990____MethodName_PropertyInfo,
	&IMethodMessage_t1990____MethodSignature_PropertyInfo,
	&IMethodMessage_t1990____TypeName_PropertyInfo,
	&IMethodMessage_t1990____Uri_PropertyInfo,
	NULL
};
static const Il2CppType* IMethodMessage_t1990_InterfacesTypeInfos[] = 
{
	&IMessage_t1978_0_0_0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IMethodMessage_t1990_1_0_0;
struct IMethodMessage_t1990;
const Il2CppTypeDefinitionMetadata IMethodMessage_t1990_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IMethodMessage_t1990_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IMethodMessage_t1990_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IMethodMessage"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, IMethodMessage_t1990_MethodInfos/* methods */
	, IMethodMessage_t1990_PropertyInfos/* properties */
	, NULL/* events */
	, &IMethodMessage_t1990_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 577/* custom_attributes_cache */
	, &IMethodMessage_t1990_0_0_0/* byval_arg */
	, &IMethodMessage_t1990_1_0_0/* this_arg */
	, &IMethodMessage_t1990_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 8/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Messaging.IMethodReturnMessage
extern TypeInfo IMethodReturnMessage_t2288_il2cpp_TypeInfo;
extern const Il2CppType Exception_t232_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Exception System.Runtime.Remoting.Messaging.IMethodReturnMessage::get_Exception()
extern const MethodInfo IMethodReturnMessage_get_Exception_m13216_MethodInfo = 
{
	"get_Exception"/* name */
	, NULL/* method */
	, &IMethodReturnMessage_t2288_il2cpp_TypeInfo/* declaring_type */
	, &Exception_t232_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3819/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Runtime.Remoting.Messaging.IMethodReturnMessage::get_OutArgCount()
extern const MethodInfo IMethodReturnMessage_get_OutArgCount_m13217_MethodInfo = 
{
	"get_OutArgCount"/* name */
	, NULL/* method */
	, &IMethodReturnMessage_t2288_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3820/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Runtime.Remoting.Messaging.IMethodReturnMessage::get_OutArgs()
extern const MethodInfo IMethodReturnMessage_get_OutArgs_m13218_MethodInfo = 
{
	"get_OutArgs"/* name */
	, NULL/* method */
	, &IMethodReturnMessage_t2288_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t224_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3821/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.IMethodReturnMessage::get_ReturnValue()
extern const MethodInfo IMethodReturnMessage_get_ReturnValue_m13219_MethodInfo = 
{
	"get_ReturnValue"/* name */
	, NULL/* method */
	, &IMethodReturnMessage_t2288_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3822/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IMethodReturnMessage_t2288_MethodInfos[] =
{
	&IMethodReturnMessage_get_Exception_m13216_MethodInfo,
	&IMethodReturnMessage_get_OutArgCount_m13217_MethodInfo,
	&IMethodReturnMessage_get_OutArgs_m13218_MethodInfo,
	&IMethodReturnMessage_get_ReturnValue_m13219_MethodInfo,
	NULL
};
extern const MethodInfo IMethodReturnMessage_get_Exception_m13216_MethodInfo;
static const PropertyInfo IMethodReturnMessage_t2288____Exception_PropertyInfo = 
{
	&IMethodReturnMessage_t2288_il2cpp_TypeInfo/* parent */
	, "Exception"/* name */
	, &IMethodReturnMessage_get_Exception_m13216_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo IMethodReturnMessage_get_OutArgCount_m13217_MethodInfo;
static const PropertyInfo IMethodReturnMessage_t2288____OutArgCount_PropertyInfo = 
{
	&IMethodReturnMessage_t2288_il2cpp_TypeInfo/* parent */
	, "OutArgCount"/* name */
	, &IMethodReturnMessage_get_OutArgCount_m13217_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo IMethodReturnMessage_get_OutArgs_m13218_MethodInfo;
static const PropertyInfo IMethodReturnMessage_t2288____OutArgs_PropertyInfo = 
{
	&IMethodReturnMessage_t2288_il2cpp_TypeInfo/* parent */
	, "OutArgs"/* name */
	, &IMethodReturnMessage_get_OutArgs_m13218_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo IMethodReturnMessage_get_ReturnValue_m13219_MethodInfo;
static const PropertyInfo IMethodReturnMessage_t2288____ReturnValue_PropertyInfo = 
{
	&IMethodReturnMessage_t2288_il2cpp_TypeInfo/* parent */
	, "ReturnValue"/* name */
	, &IMethodReturnMessage_get_ReturnValue_m13219_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* IMethodReturnMessage_t2288_PropertyInfos[] =
{
	&IMethodReturnMessage_t2288____Exception_PropertyInfo,
	&IMethodReturnMessage_t2288____OutArgCount_PropertyInfo,
	&IMethodReturnMessage_t2288____OutArgs_PropertyInfo,
	&IMethodReturnMessage_t2288____ReturnValue_PropertyInfo,
	NULL
};
static const Il2CppType* IMethodReturnMessage_t2288_InterfacesTypeInfos[] = 
{
	&IMessage_t1978_0_0_0,
	&IMethodMessage_t1990_0_0_0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IMethodReturnMessage_t2288_0_0_0;
extern const Il2CppType IMethodReturnMessage_t2288_1_0_0;
struct IMethodReturnMessage_t2288;
const Il2CppTypeDefinitionMetadata IMethodReturnMessage_t2288_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IMethodReturnMessage_t2288_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IMethodReturnMessage_t2288_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IMethodReturnMessage"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, IMethodReturnMessage_t2288_MethodInfos/* methods */
	, IMethodReturnMessage_t2288_PropertyInfos/* properties */
	, NULL/* events */
	, &IMethodReturnMessage_t2288_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 578/* custom_attributes_cache */
	, &IMethodReturnMessage_t2288_0_0_0/* byval_arg */
	, &IMethodReturnMessage_t2288_1_0_0/* this_arg */
	, &IMethodReturnMessage_t2288_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 4/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Messaging.IRemotingFormatter
extern TypeInfo IRemotingFormatter_t2383_il2cpp_TypeInfo;
static const MethodInfo* IRemotingFormatter_t2383_MethodInfos[] =
{
	NULL
};
extern const Il2CppType IFormatter_t2385_0_0_0;
static const Il2CppType* IRemotingFormatter_t2383_InterfacesTypeInfos[] = 
{
	&IFormatter_t2385_0_0_0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IRemotingFormatter_t2383_0_0_0;
extern const Il2CppType IRemotingFormatter_t2383_1_0_0;
struct IRemotingFormatter_t2383;
const Il2CppTypeDefinitionMetadata IRemotingFormatter_t2383_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IRemotingFormatter_t2383_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IRemotingFormatter_t2383_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IRemotingFormatter"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, IRemotingFormatter_t2383_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IRemotingFormatter_t2383_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 579/* custom_attributes_cache */
	, &IRemotingFormatter_t2383_0_0_0/* byval_arg */
	, &IRemotingFormatter_t2383_1_0_0/* this_arg */
	, &IRemotingFormatter_t2383_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Messaging.ISerializationRootObject
extern TypeInfo ISerializationRootObject_t2384_il2cpp_TypeInfo;
static const MethodInfo* ISerializationRootObject_t2384_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ISerializationRootObject_t2384_1_0_0;
struct ISerializationRootObject_t2384;
const Il2CppTypeDefinitionMetadata ISerializationRootObject_t2384_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ISerializationRootObject_t2384_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ISerializationRootObject"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, ISerializationRootObject_t2384_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ISerializationRootObject_t2384_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ISerializationRootObject_t2384_0_0_0/* byval_arg */
	, &ISerializationRootObject_t2384_1_0_0/* this_arg */
	, &ISerializationRootObject_t2384_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 160/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.LogicalCallContext
#include "mscorlib_System_Runtime_Remoting_Messaging_LogicalCallContex.h"
// Metadata Definition System.Runtime.Remoting.Messaging.LogicalCallContext
extern TypeInfo LogicalCallContext_t1987_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.LogicalCallContext
#include "mscorlib_System_Runtime_Remoting_Messaging_LogicalCallContexMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.LogicalCallContext::.ctor()
extern const MethodInfo LogicalCallContext__ctor_m10571_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&LogicalCallContext__ctor_m10571/* method */
	, &LogicalCallContext_t1987_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3823/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1043_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
static const ParameterInfo LogicalCallContext_t1987_LogicalCallContext__ctor_m10572_ParameterInfos[] = 
{
	{"info", 0, 134222347, 0, &SerializationInfo_t1043_0_0_0},
	{"context", 1, 134222348, 0, &StreamingContext_t1044_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.LogicalCallContext::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo LogicalCallContext__ctor_m10572_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&LogicalCallContext__ctor_m10572/* method */
	, &LogicalCallContext_t1987_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044/* invoker_method */
	, LogicalCallContext_t1987_LogicalCallContext__ctor_m10572_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3824/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.Messaging.LogicalCallContext::get_HasInfo()
extern const MethodInfo LogicalCallContext_get_HasInfo_m10573_MethodInfo = 
{
	"get_HasInfo"/* name */
	, (methodPointerType)&LogicalCallContext_get_HasInfo_m10573/* method */
	, &LogicalCallContext_t1987_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3825/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1043_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
static const ParameterInfo LogicalCallContext_t1987_LogicalCallContext_GetObjectData_m10574_ParameterInfos[] = 
{
	{"info", 0, 134222349, 0, &SerializationInfo_t1043_0_0_0},
	{"context", 1, 134222350, 0, &StreamingContext_t1044_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.LogicalCallContext::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo LogicalCallContext_GetObjectData_m10574_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&LogicalCallContext_GetObjectData_m10574/* method */
	, &LogicalCallContext_t1987_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044/* invoker_method */
	, LogicalCallContext_t1987_LogicalCallContext_GetObjectData_m10574_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3826/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo LogicalCallContext_t1987_LogicalCallContext_SetData_m10575_ParameterInfos[] = 
{
	{"name", 0, 134222351, 0, &String_t_0_0_0},
	{"data", 1, 134222352, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.LogicalCallContext::SetData(System.String,System.Object)
extern const MethodInfo LogicalCallContext_SetData_m10575_MethodInfo = 
{
	"SetData"/* name */
	, (methodPointerType)&LogicalCallContext_SetData_m10575/* method */
	, &LogicalCallContext_t1987_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, LogicalCallContext_t1987_LogicalCallContext_SetData_m10575_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3827/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* LogicalCallContext_t1987_MethodInfos[] =
{
	&LogicalCallContext__ctor_m10571_MethodInfo,
	&LogicalCallContext__ctor_m10572_MethodInfo,
	&LogicalCallContext_get_HasInfo_m10573_MethodInfo,
	&LogicalCallContext_GetObjectData_m10574_MethodInfo,
	&LogicalCallContext_SetData_m10575_MethodInfo,
	NULL
};
extern const MethodInfo LogicalCallContext_get_HasInfo_m10573_MethodInfo;
static const PropertyInfo LogicalCallContext_t1987____HasInfo_PropertyInfo = 
{
	&LogicalCallContext_t1987_il2cpp_TypeInfo/* parent */
	, "HasInfo"/* name */
	, &LogicalCallContext_get_HasInfo_m10573_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* LogicalCallContext_t1987_PropertyInfos[] =
{
	&LogicalCallContext_t1987____HasInfo_PropertyInfo,
	NULL
};
extern const MethodInfo LogicalCallContext_GetObjectData_m10574_MethodInfo;
static const Il2CppMethodReference LogicalCallContext_t1987_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&LogicalCallContext_GetObjectData_m10574_MethodInfo,
};
static bool LogicalCallContext_t1987_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* LogicalCallContext_t1987_InterfacesTypeInfos[] = 
{
	&ICloneable_t733_0_0_0,
	&ISerializable_t439_0_0_0,
};
static Il2CppInterfaceOffsetPair LogicalCallContext_t1987_InterfacesOffsets[] = 
{
	{ &ICloneable_t733_0_0_0, 4},
	{ &ISerializable_t439_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType LogicalCallContext_t1987_1_0_0;
struct LogicalCallContext_t1987;
const Il2CppTypeDefinitionMetadata LogicalCallContext_t1987_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, LogicalCallContext_t1987_InterfacesTypeInfos/* implementedInterfaces */
	, LogicalCallContext_t1987_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, LogicalCallContext_t1987_VTable/* vtableMethods */
	, LogicalCallContext_t1987_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1987/* fieldStart */

};
TypeInfo LogicalCallContext_t1987_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "LogicalCallContext"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, LogicalCallContext_t1987_MethodInfos/* methods */
	, LogicalCallContext_t1987_PropertyInfos/* properties */
	, NULL/* events */
	, &LogicalCallContext_t1987_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 580/* custom_attributes_cache */
	, &LogicalCallContext_t1987_0_0_0/* byval_arg */
	, &LogicalCallContext_t1987_1_0_0/* this_arg */
	, &LogicalCallContext_t1987_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LogicalCallContext_t1987)/* instance_size */
	, sizeof (LogicalCallContext_t1987)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.CallContextRemotingData
#include "mscorlib_System_Runtime_Remoting_Messaging_CallContextRemoti.h"
// Metadata Definition System.Runtime.Remoting.Messaging.CallContextRemotingData
extern TypeInfo CallContextRemotingData_t1986_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.CallContextRemotingData
#include "mscorlib_System_Runtime_Remoting_Messaging_CallContextRemotiMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.CallContextRemotingData::.ctor()
extern const MethodInfo CallContextRemotingData__ctor_m10576_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CallContextRemotingData__ctor_m10576/* method */
	, &CallContextRemotingData_t1986_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3828/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CallContextRemotingData_t1986_MethodInfos[] =
{
	&CallContextRemotingData__ctor_m10576_MethodInfo,
	NULL
};
static const Il2CppMethodReference CallContextRemotingData_t1986_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool CallContextRemotingData_t1986_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static const Il2CppType* CallContextRemotingData_t1986_InterfacesTypeInfos[] = 
{
	&ICloneable_t733_0_0_0,
};
static Il2CppInterfaceOffsetPair CallContextRemotingData_t1986_InterfacesOffsets[] = 
{
	{ &ICloneable_t733_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CallContextRemotingData_t1986_0_0_0;
extern const Il2CppType CallContextRemotingData_t1986_1_0_0;
struct CallContextRemotingData_t1986;
const Il2CppTypeDefinitionMetadata CallContextRemotingData_t1986_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, CallContextRemotingData_t1986_InterfacesTypeInfos/* implementedInterfaces */
	, CallContextRemotingData_t1986_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CallContextRemotingData_t1986_VTable/* vtableMethods */
	, CallContextRemotingData_t1986_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo CallContextRemotingData_t1986_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CallContextRemotingData"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, CallContextRemotingData_t1986_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CallContextRemotingData_t1986_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CallContextRemotingData_t1986_0_0_0/* byval_arg */
	, &CallContextRemotingData_t1986_1_0_0/* this_arg */
	, &CallContextRemotingData_t1986_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CallContextRemotingData_t1986)/* instance_size */
	, sizeof (CallContextRemotingData_t1986)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.MethodCall
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodCall.h"
// Metadata Definition System.Runtime.Remoting.Messaging.MethodCall
extern TypeInfo MethodCall_t1981_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.MethodCall
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodCallMethodDeclarations.h"
extern const Il2CppType HeaderU5BU5D_t2251_0_0_0;
extern const Il2CppType HeaderU5BU5D_t2251_0_0_0;
static const ParameterInfo MethodCall_t1981_MethodCall__ctor_m10577_ParameterInfos[] = 
{
	{"h1", 0, 134222353, 0, &HeaderU5BU5D_t2251_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodCall::.ctor(System.Runtime.Remoting.Messaging.Header[])
extern const MethodInfo MethodCall__ctor_m10577_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MethodCall__ctor_m10577/* method */
	, &MethodCall_t1981_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, MethodCall_t1981_MethodCall__ctor_m10577_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3829/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1043_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
static const ParameterInfo MethodCall_t1981_MethodCall__ctor_m10578_ParameterInfos[] = 
{
	{"info", 0, 134222354, 0, &SerializationInfo_t1043_0_0_0},
	{"context", 1, 134222355, 0, &StreamingContext_t1044_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodCall::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo MethodCall__ctor_m10578_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MethodCall__ctor_m10578/* method */
	, &MethodCall_t1981_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044/* invoker_method */
	, MethodCall_t1981_MethodCall__ctor_m10578_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3830/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodCall::.ctor()
extern const MethodInfo MethodCall__ctor_m10579_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MethodCall__ctor_m10579/* method */
	, &MethodCall_t1981_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3831/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo MethodCall_t1981_MethodCall_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m10580_ParameterInfos[] = 
{
	{"value", 0, 134222356, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodCall::System.Runtime.Remoting.Messaging.IInternalMessage.set_Uri(System.String)
extern const MethodInfo MethodCall_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m10580_MethodInfo = 
{
	"System.Runtime.Remoting.Messaging.IInternalMessage.set_Uri"/* name */
	, (methodPointerType)&MethodCall_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m10580/* method */
	, &MethodCall_t1981_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, MethodCall_t1981_MethodCall_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m10580_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3832/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo MethodCall_t1981_MethodCall_InitMethodProperty_m10581_ParameterInfos[] = 
{
	{"key", 0, 134222357, 0, &String_t_0_0_0},
	{"value", 1, 134222358, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodCall::InitMethodProperty(System.String,System.Object)
extern const MethodInfo MethodCall_InitMethodProperty_m10581_MethodInfo = 
{
	"InitMethodProperty"/* name */
	, (methodPointerType)&MethodCall_InitMethodProperty_m10581/* method */
	, &MethodCall_t1981_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, MethodCall_t1981_MethodCall_InitMethodProperty_m10581_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 451/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3833/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1043_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
static const ParameterInfo MethodCall_t1981_MethodCall_GetObjectData_m10582_ParameterInfos[] = 
{
	{"info", 0, 134222359, 0, &SerializationInfo_t1043_0_0_0},
	{"context", 1, 134222360, 0, &StreamingContext_t1044_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodCall::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo MethodCall_GetObjectData_m10582_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&MethodCall_GetObjectData_m10582/* method */
	, &MethodCall_t1981_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044/* invoker_method */
	, MethodCall_t1981_MethodCall_GetObjectData_m10582_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3834/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Runtime.Remoting.Messaging.MethodCall::get_ArgCount()
extern const MethodInfo MethodCall_get_ArgCount_m10583_MethodInfo = 
{
	"get_ArgCount"/* name */
	, (methodPointerType)&MethodCall_get_ArgCount_m10583/* method */
	, &MethodCall_t1981_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3835/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Runtime.Remoting.Messaging.MethodCall::get_Args()
extern const MethodInfo MethodCall_get_Args_m10584_MethodInfo = 
{
	"get_Args"/* name */
	, (methodPointerType)&MethodCall_get_Args_m10584/* method */
	, &MethodCall_t1981_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t224_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3836/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Messaging.LogicalCallContext System.Runtime.Remoting.Messaging.MethodCall::get_LogicalCallContext()
extern const MethodInfo MethodCall_get_LogicalCallContext_m10585_MethodInfo = 
{
	"get_LogicalCallContext"/* name */
	, (methodPointerType)&MethodCall_get_LogicalCallContext_m10585/* method */
	, &MethodCall_t1981_il2cpp_TypeInfo/* declaring_type */
	, &LogicalCallContext_t1987_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3837/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodBase System.Runtime.Remoting.Messaging.MethodCall::get_MethodBase()
extern const MethodInfo MethodCall_get_MethodBase_m10586_MethodInfo = 
{
	"get_MethodBase"/* name */
	, (methodPointerType)&MethodCall_get_MethodBase_m10586/* method */
	, &MethodCall_t1981_il2cpp_TypeInfo/* declaring_type */
	, &MethodBase_t1102_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3838/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Messaging.MethodCall::get_MethodName()
extern const MethodInfo MethodCall_get_MethodName_m10587_MethodInfo = 
{
	"get_MethodName"/* name */
	, (methodPointerType)&MethodCall_get_MethodName_m10587/* method */
	, &MethodCall_t1981_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3839/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.MethodCall::get_MethodSignature()
extern const MethodInfo MethodCall_get_MethodSignature_m10588_MethodInfo = 
{
	"get_MethodSignature"/* name */
	, (methodPointerType)&MethodCall_get_MethodSignature_m10588/* method */
	, &MethodCall_t1981_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3840/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.IDictionary System.Runtime.Remoting.Messaging.MethodCall::get_Properties()
extern const MethodInfo MethodCall_get_Properties_m10589_MethodInfo = 
{
	"get_Properties"/* name */
	, (methodPointerType)&MethodCall_get_Properties_m10589/* method */
	, &MethodCall_t1981_il2cpp_TypeInfo/* declaring_type */
	, &IDictionary_t444_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 18/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3841/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodCall::InitDictionary()
extern const MethodInfo MethodCall_InitDictionary_m10590_MethodInfo = 
{
	"InitDictionary"/* name */
	, (methodPointerType)&MethodCall_InitDictionary_m10590/* method */
	, &MethodCall_t1981_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 451/* flags */
	, 0/* iflags */
	, 19/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3842/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Messaging.MethodCall::get_TypeName()
extern const MethodInfo MethodCall_get_TypeName_m10591_MethodInfo = 
{
	"get_TypeName"/* name */
	, (methodPointerType)&MethodCall_get_TypeName_m10591/* method */
	, &MethodCall_t1981_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3843/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Messaging.MethodCall::get_Uri()
extern const MethodInfo MethodCall_get_Uri_m10592_MethodInfo = 
{
	"get_Uri"/* name */
	, (methodPointerType)&MethodCall_get_Uri_m10592/* method */
	, &MethodCall_t1981_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3844/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo MethodCall_t1981_MethodCall_set_Uri_m10593_ParameterInfos[] = 
{
	{"value", 0, 134222361, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodCall::set_Uri(System.String)
extern const MethodInfo MethodCall_set_Uri_m10593_MethodInfo = 
{
	"set_Uri"/* name */
	, (methodPointerType)&MethodCall_set_Uri_m10593/* method */
	, &MethodCall_t1981_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, MethodCall_t1981_MethodCall_set_Uri_m10593_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 20/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3845/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo MethodCall_t1981_MethodCall_GetArg_m10594_ParameterInfos[] = 
{
	{"argNum", 0, 134222362, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.MethodCall::GetArg(System.Int32)
extern const MethodInfo MethodCall_GetArg_m10594_MethodInfo = 
{
	"GetArg"/* name */
	, (methodPointerType)&MethodCall_GetArg_m10594/* method */
	, &MethodCall_t1981_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t253/* invoker_method */
	, MethodCall_t1981_MethodCall_GetArg_m10594_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3846/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodCall::Init()
extern const MethodInfo MethodCall_Init_m10595_MethodInfo = 
{
	"Init"/* name */
	, (methodPointerType)&MethodCall_Init_m10595/* method */
	, &MethodCall_t1981_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 21/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3847/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodCall::ResolveMethod()
extern const MethodInfo MethodCall_ResolveMethod_m10596_MethodInfo = 
{
	"ResolveMethod"/* name */
	, (methodPointerType)&MethodCall_ResolveMethod_m10596/* method */
	, &MethodCall_t1981_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3848/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo MethodCall_t1981_MethodCall_CastTo_m10597_ParameterInfos[] = 
{
	{"clientType", 0, 134222363, 0, &String_t_0_0_0},
	{"serverType", 1, 134222364, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Runtime.Remoting.Messaging.MethodCall::CastTo(System.String,System.Type)
extern const MethodInfo MethodCall_CastTo_m10597_MethodInfo = 
{
	"CastTo"/* name */
	, (methodPointerType)&MethodCall_CastTo_m10597/* method */
	, &MethodCall_t1981_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, MethodCall_t1981_MethodCall_CastTo_m10597_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3849/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo MethodCall_t1981_MethodCall_GetTypeNameFromAssemblyQualifiedName_m10598_ParameterInfos[] = 
{
	{"aqname", 0, 134222365, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Messaging.MethodCall::GetTypeNameFromAssemblyQualifiedName(System.String)
extern const MethodInfo MethodCall_GetTypeNameFromAssemblyQualifiedName_m10598_MethodInfo = 
{
	"GetTypeNameFromAssemblyQualifiedName"/* name */
	, (methodPointerType)&MethodCall_GetTypeNameFromAssemblyQualifiedName_m10598/* method */
	, &MethodCall_t1981_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, MethodCall_t1981_MethodCall_GetTypeNameFromAssemblyQualifiedName_m10598_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3850/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TypeU5BU5D_t238_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type[] System.Runtime.Remoting.Messaging.MethodCall::get_GenericArguments()
extern const MethodInfo MethodCall_get_GenericArguments_m10599_MethodInfo = 
{
	"get_GenericArguments"/* name */
	, (methodPointerType)&MethodCall_get_GenericArguments_m10599/* method */
	, &MethodCall_t1981_il2cpp_TypeInfo/* declaring_type */
	, &TypeU5BU5D_t238_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3851/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MethodCall_t1981_MethodInfos[] =
{
	&MethodCall__ctor_m10577_MethodInfo,
	&MethodCall__ctor_m10578_MethodInfo,
	&MethodCall__ctor_m10579_MethodInfo,
	&MethodCall_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m10580_MethodInfo,
	&MethodCall_InitMethodProperty_m10581_MethodInfo,
	&MethodCall_GetObjectData_m10582_MethodInfo,
	&MethodCall_get_ArgCount_m10583_MethodInfo,
	&MethodCall_get_Args_m10584_MethodInfo,
	&MethodCall_get_LogicalCallContext_m10585_MethodInfo,
	&MethodCall_get_MethodBase_m10586_MethodInfo,
	&MethodCall_get_MethodName_m10587_MethodInfo,
	&MethodCall_get_MethodSignature_m10588_MethodInfo,
	&MethodCall_get_Properties_m10589_MethodInfo,
	&MethodCall_InitDictionary_m10590_MethodInfo,
	&MethodCall_get_TypeName_m10591_MethodInfo,
	&MethodCall_get_Uri_m10592_MethodInfo,
	&MethodCall_set_Uri_m10593_MethodInfo,
	&MethodCall_GetArg_m10594_MethodInfo,
	&MethodCall_Init_m10595_MethodInfo,
	&MethodCall_ResolveMethod_m10596_MethodInfo,
	&MethodCall_CastTo_m10597_MethodInfo,
	&MethodCall_GetTypeNameFromAssemblyQualifiedName_m10598_MethodInfo,
	&MethodCall_get_GenericArguments_m10599_MethodInfo,
	NULL
};
static const PropertyInfo MethodCall_t1981____System_Runtime_Remoting_Messaging_IInternalMessage_Uri_PropertyInfo = 
{
	&MethodCall_t1981_il2cpp_TypeInfo/* parent */
	, "System.Runtime.Remoting.Messaging.IInternalMessage.Uri"/* name */
	, NULL/* get */
	, &MethodCall_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m10580_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MethodCall_t1981____ArgCount_PropertyInfo = 
{
	&MethodCall_t1981_il2cpp_TypeInfo/* parent */
	, "ArgCount"/* name */
	, &MethodCall_get_ArgCount_m10583_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MethodCall_t1981____Args_PropertyInfo = 
{
	&MethodCall_t1981_il2cpp_TypeInfo/* parent */
	, "Args"/* name */
	, &MethodCall_get_Args_m10584_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MethodCall_t1981____LogicalCallContext_PropertyInfo = 
{
	&MethodCall_t1981_il2cpp_TypeInfo/* parent */
	, "LogicalCallContext"/* name */
	, &MethodCall_get_LogicalCallContext_m10585_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MethodCall_t1981____MethodBase_PropertyInfo = 
{
	&MethodCall_t1981_il2cpp_TypeInfo/* parent */
	, "MethodBase"/* name */
	, &MethodCall_get_MethodBase_m10586_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MethodCall_t1981____MethodName_PropertyInfo = 
{
	&MethodCall_t1981_il2cpp_TypeInfo/* parent */
	, "MethodName"/* name */
	, &MethodCall_get_MethodName_m10587_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MethodCall_t1981____MethodSignature_PropertyInfo = 
{
	&MethodCall_t1981_il2cpp_TypeInfo/* parent */
	, "MethodSignature"/* name */
	, &MethodCall_get_MethodSignature_m10588_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MethodCall_get_Properties_m10589_MethodInfo;
static const PropertyInfo MethodCall_t1981____Properties_PropertyInfo = 
{
	&MethodCall_t1981_il2cpp_TypeInfo/* parent */
	, "Properties"/* name */
	, &MethodCall_get_Properties_m10589_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MethodCall_t1981____TypeName_PropertyInfo = 
{
	&MethodCall_t1981_il2cpp_TypeInfo/* parent */
	, "TypeName"/* name */
	, &MethodCall_get_TypeName_m10591_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MethodCall_t1981____Uri_PropertyInfo = 
{
	&MethodCall_t1981_il2cpp_TypeInfo/* parent */
	, "Uri"/* name */
	, &MethodCall_get_Uri_m10592_MethodInfo/* get */
	, &MethodCall_set_Uri_m10593_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MethodCall_get_GenericArguments_m10599_MethodInfo;
static const PropertyInfo MethodCall_t1981____GenericArguments_PropertyInfo = 
{
	&MethodCall_t1981_il2cpp_TypeInfo/* parent */
	, "GenericArguments"/* name */
	, &MethodCall_get_GenericArguments_m10599_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* MethodCall_t1981_PropertyInfos[] =
{
	&MethodCall_t1981____System_Runtime_Remoting_Messaging_IInternalMessage_Uri_PropertyInfo,
	&MethodCall_t1981____ArgCount_PropertyInfo,
	&MethodCall_t1981____Args_PropertyInfo,
	&MethodCall_t1981____LogicalCallContext_PropertyInfo,
	&MethodCall_t1981____MethodBase_PropertyInfo,
	&MethodCall_t1981____MethodName_PropertyInfo,
	&MethodCall_t1981____MethodSignature_PropertyInfo,
	&MethodCall_t1981____Properties_PropertyInfo,
	&MethodCall_t1981____TypeName_PropertyInfo,
	&MethodCall_t1981____Uri_PropertyInfo,
	&MethodCall_t1981____GenericArguments_PropertyInfo,
	NULL
};
extern const MethodInfo MethodCall_GetObjectData_m10582_MethodInfo;
extern const MethodInfo MethodCall_InitMethodProperty_m10581_MethodInfo;
extern const MethodInfo MethodCall_InitDictionary_m10590_MethodInfo;
static const Il2CppMethodReference MethodCall_t1981_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&MethodCall_GetObjectData_m10582_MethodInfo,
	&MethodCall_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m10580_MethodInfo,
	&MethodCall_get_Properties_m10589_MethodInfo,
	&MethodCall_get_ArgCount_m10583_MethodInfo,
	&MethodCall_get_Args_m10584_MethodInfo,
	&MethodCall_get_LogicalCallContext_m10585_MethodInfo,
	&MethodCall_get_MethodBase_m10586_MethodInfo,
	&MethodCall_get_MethodName_m10587_MethodInfo,
	&MethodCall_get_MethodSignature_m10588_MethodInfo,
	&MethodCall_get_TypeName_m10591_MethodInfo,
	&MethodCall_get_Uri_m10592_MethodInfo,
	&MethodCall_GetArg_m10594_MethodInfo,
	&MethodCall_InitMethodProperty_m10581_MethodInfo,
	&MethodCall_GetObjectData_m10582_MethodInfo,
	&MethodCall_get_Properties_m10589_MethodInfo,
	&MethodCall_InitDictionary_m10590_MethodInfo,
	&MethodCall_set_Uri_m10593_MethodInfo,
	&MethodCall_Init_m10595_MethodInfo,
};
static bool MethodCall_t1981_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* MethodCall_t1981_InterfacesTypeInfos[] = 
{
	&ISerializable_t439_0_0_0,
	&IInternalMessage_t2303_0_0_0,
	&IMessage_t1978_0_0_0,
	&IMethodCallMessage_t2289_0_0_0,
	&IMethodMessage_t1990_0_0_0,
	&ISerializationRootObject_t2384_0_0_0,
};
static Il2CppInterfaceOffsetPair MethodCall_t1981_InterfacesOffsets[] = 
{
	{ &ISerializable_t439_0_0_0, 4},
	{ &IInternalMessage_t2303_0_0_0, 5},
	{ &IMessage_t1978_0_0_0, 6},
	{ &IMethodCallMessage_t2289_0_0_0, 7},
	{ &IMethodMessage_t1990_0_0_0, 7},
	{ &ISerializationRootObject_t2384_0_0_0, 16},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MethodCall_t1981_1_0_0;
struct MethodCall_t1981;
const Il2CppTypeDefinitionMetadata MethodCall_t1981_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, MethodCall_t1981_InterfacesTypeInfos/* implementedInterfaces */
	, MethodCall_t1981_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MethodCall_t1981_VTable/* vtableMethods */
	, MethodCall_t1981_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1989/* fieldStart */

};
TypeInfo MethodCall_t1981_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MethodCall"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, MethodCall_t1981_MethodInfos/* methods */
	, MethodCall_t1981_PropertyInfos/* properties */
	, NULL/* events */
	, &MethodCall_t1981_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 581/* custom_attributes_cache */
	, &MethodCall_t1981_0_0_0/* byval_arg */
	, &MethodCall_t1981_1_0_0/* this_arg */
	, &MethodCall_t1981_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MethodCall_t1981)/* instance_size */
	, sizeof (MethodCall_t1981)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(MethodCall_t1981_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 23/* method_count */
	, 11/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 22/* vtable_count */
	, 6/* interfaces_count */
	, 6/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.MethodCallDictionary
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodCallDiction.h"
// Metadata Definition System.Runtime.Remoting.Messaging.MethodCallDictionary
extern TypeInfo MethodCallDictionary_t1988_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.MethodCallDictionary
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodCallDictionMethodDeclarations.h"
extern const Il2CppType IMethodMessage_t1990_0_0_0;
static const ParameterInfo MethodCallDictionary_t1988_MethodCallDictionary__ctor_m10600_ParameterInfos[] = 
{
	{"message", 0, 134222366, 0, &IMethodMessage_t1990_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodCallDictionary::.ctor(System.Runtime.Remoting.Messaging.IMethodMessage)
extern const MethodInfo MethodCallDictionary__ctor_m10600_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MethodCallDictionary__ctor_m10600/* method */
	, &MethodCallDictionary_t1988_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, MethodCallDictionary_t1988_MethodCallDictionary__ctor_m10600_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3852/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodCallDictionary::.cctor()
extern const MethodInfo MethodCallDictionary__cctor_m10601_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&MethodCallDictionary__cctor_m10601/* method */
	, &MethodCallDictionary_t1988_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3853/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MethodCallDictionary_t1988_MethodInfos[] =
{
	&MethodCallDictionary__ctor_m10600_MethodInfo,
	&MethodCallDictionary__cctor_m10601_MethodInfo,
	NULL
};
extern const MethodInfo MethodDictionary_GetMethodProperty_m10616_MethodInfo;
extern const MethodInfo MethodDictionary_SetMethodProperty_m10617_MethodInfo;
static const Il2CppMethodReference MethodCallDictionary_t1988_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&MethodDictionary_System_Collections_IEnumerable_GetEnumerator_m10609_MethodInfo,
	&MethodDictionary_get_Count_m10622_MethodInfo,
	&MethodDictionary_get_IsSynchronized_m10623_MethodInfo,
	&MethodDictionary_get_SyncRoot_m10624_MethodInfo,
	&MethodDictionary_CopyTo_m10625_MethodInfo,
	&MethodDictionary_get_Item_m10614_MethodInfo,
	&MethodDictionary_set_Item_m10615_MethodInfo,
	&MethodDictionary_Add_m10619_MethodInfo,
	&MethodDictionary_Contains_m10620_MethodInfo,
	&MethodDictionary_GetEnumerator_m10626_MethodInfo,
	&MethodDictionary_Remove_m10621_MethodInfo,
	&MethodDictionary_AllocInternalProperties_m10611_MethodInfo,
	&MethodDictionary_GetMethodProperty_m10616_MethodInfo,
	&MethodDictionary_SetMethodProperty_m10617_MethodInfo,
	&MethodDictionary_get_Values_m10618_MethodInfo,
};
static bool MethodCallDictionary_t1988_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair MethodCallDictionary_t1988_InterfacesOffsets[] = 
{
	{ &IEnumerable_t438_0_0_0, 4},
	{ &ICollection_t440_0_0_0, 5},
	{ &IDictionary_t444_0_0_0, 9},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MethodCallDictionary_t1988_0_0_0;
extern const Il2CppType MethodCallDictionary_t1988_1_0_0;
struct MethodCallDictionary_t1988;
const Il2CppTypeDefinitionMetadata MethodCallDictionary_t1988_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MethodCallDictionary_t1988_InterfacesOffsets/* interfaceOffsets */
	, &MethodDictionary_t1983_0_0_0/* parent */
	, MethodCallDictionary_t1988_VTable/* vtableMethods */
	, MethodCallDictionary_t1988_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2000/* fieldStart */

};
TypeInfo MethodCallDictionary_t1988_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MethodCallDictionary"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, MethodCallDictionary_t1988_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &MethodCallDictionary_t1988_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MethodCallDictionary_t1988_0_0_0/* byval_arg */
	, &MethodCallDictionary_t1988_1_0_0/* this_arg */
	, &MethodCallDictionary_t1988_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MethodCallDictionary_t1988)/* instance_size */
	, sizeof (MethodCallDictionary_t1988)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(MethodCallDictionary_t1988_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 19/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.MethodDictionary/DictionaryEnumerator
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodDictionary_.h"
// Metadata Definition System.Runtime.Remoting.Messaging.MethodDictionary/DictionaryEnumerator
extern TypeInfo DictionaryEnumerator_t1989_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.MethodDictionary/DictionaryEnumerator
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodDictionary_MethodDeclarations.h"
extern const Il2CppType MethodDictionary_t1983_0_0_0;
static const ParameterInfo DictionaryEnumerator_t1989_DictionaryEnumerator__ctor_m10602_ParameterInfos[] = 
{
	{"methodDictionary", 0, 134222382, 0, &MethodDictionary_t1983_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodDictionary/DictionaryEnumerator::.ctor(System.Runtime.Remoting.Messaging.MethodDictionary)
extern const MethodInfo DictionaryEnumerator__ctor_m10602_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DictionaryEnumerator__ctor_m10602/* method */
	, &DictionaryEnumerator_t1989_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, DictionaryEnumerator_t1989_DictionaryEnumerator__ctor_m10602_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3873/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.MethodDictionary/DictionaryEnumerator::get_Current()
extern const MethodInfo DictionaryEnumerator_get_Current_m10603_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&DictionaryEnumerator_get_Current_m10603/* method */
	, &DictionaryEnumerator_t1989_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3874/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.Messaging.MethodDictionary/DictionaryEnumerator::MoveNext()
extern const MethodInfo DictionaryEnumerator_MoveNext_m10604_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&DictionaryEnumerator_MoveNext_m10604/* method */
	, &DictionaryEnumerator_t1989_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3875/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType DictionaryEntry_t1430_0_0_0;
extern void* RuntimeInvoker_DictionaryEntry_t1430 (const MethodInfo* method, void* obj, void** args);
// System.Collections.DictionaryEntry System.Runtime.Remoting.Messaging.MethodDictionary/DictionaryEnumerator::get_Entry()
extern const MethodInfo DictionaryEnumerator_get_Entry_m10605_MethodInfo = 
{
	"get_Entry"/* name */
	, (methodPointerType)&DictionaryEnumerator_get_Entry_m10605/* method */
	, &DictionaryEnumerator_t1989_il2cpp_TypeInfo/* declaring_type */
	, &DictionaryEntry_t1430_0_0_0/* return_type */
	, RuntimeInvoker_DictionaryEntry_t1430/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3876/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.MethodDictionary/DictionaryEnumerator::get_Key()
extern const MethodInfo DictionaryEnumerator_get_Key_m10606_MethodInfo = 
{
	"get_Key"/* name */
	, (methodPointerType)&DictionaryEnumerator_get_Key_m10606/* method */
	, &DictionaryEnumerator_t1989_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3877/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.MethodDictionary/DictionaryEnumerator::get_Value()
extern const MethodInfo DictionaryEnumerator_get_Value_m10607_MethodInfo = 
{
	"get_Value"/* name */
	, (methodPointerType)&DictionaryEnumerator_get_Value_m10607/* method */
	, &DictionaryEnumerator_t1989_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3878/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* DictionaryEnumerator_t1989_MethodInfos[] =
{
	&DictionaryEnumerator__ctor_m10602_MethodInfo,
	&DictionaryEnumerator_get_Current_m10603_MethodInfo,
	&DictionaryEnumerator_MoveNext_m10604_MethodInfo,
	&DictionaryEnumerator_get_Entry_m10605_MethodInfo,
	&DictionaryEnumerator_get_Key_m10606_MethodInfo,
	&DictionaryEnumerator_get_Value_m10607_MethodInfo,
	NULL
};
extern const MethodInfo DictionaryEnumerator_get_Current_m10603_MethodInfo;
static const PropertyInfo DictionaryEnumerator_t1989____Current_PropertyInfo = 
{
	&DictionaryEnumerator_t1989_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &DictionaryEnumerator_get_Current_m10603_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo DictionaryEnumerator_get_Entry_m10605_MethodInfo;
static const PropertyInfo DictionaryEnumerator_t1989____Entry_PropertyInfo = 
{
	&DictionaryEnumerator_t1989_il2cpp_TypeInfo/* parent */
	, "Entry"/* name */
	, &DictionaryEnumerator_get_Entry_m10605_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo DictionaryEnumerator_get_Key_m10606_MethodInfo;
static const PropertyInfo DictionaryEnumerator_t1989____Key_PropertyInfo = 
{
	&DictionaryEnumerator_t1989_il2cpp_TypeInfo/* parent */
	, "Key"/* name */
	, &DictionaryEnumerator_get_Key_m10606_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo DictionaryEnumerator_get_Value_m10607_MethodInfo;
static const PropertyInfo DictionaryEnumerator_t1989____Value_PropertyInfo = 
{
	&DictionaryEnumerator_t1989_il2cpp_TypeInfo/* parent */
	, "Value"/* name */
	, &DictionaryEnumerator_get_Value_m10607_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* DictionaryEnumerator_t1989_PropertyInfos[] =
{
	&DictionaryEnumerator_t1989____Current_PropertyInfo,
	&DictionaryEnumerator_t1989____Entry_PropertyInfo,
	&DictionaryEnumerator_t1989____Key_PropertyInfo,
	&DictionaryEnumerator_t1989____Value_PropertyInfo,
	NULL
};
extern const MethodInfo DictionaryEnumerator_MoveNext_m10604_MethodInfo;
static const Il2CppMethodReference DictionaryEnumerator_t1989_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&DictionaryEnumerator_get_Current_m10603_MethodInfo,
	&DictionaryEnumerator_MoveNext_m10604_MethodInfo,
	&DictionaryEnumerator_get_Entry_m10605_MethodInfo,
	&DictionaryEnumerator_get_Key_m10606_MethodInfo,
	&DictionaryEnumerator_get_Value_m10607_MethodInfo,
};
static bool DictionaryEnumerator_t1989_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IEnumerator_t217_0_0_0;
extern const Il2CppType IDictionaryEnumerator_t1429_0_0_0;
static const Il2CppType* DictionaryEnumerator_t1989_InterfacesTypeInfos[] = 
{
	&IEnumerator_t217_0_0_0,
	&IDictionaryEnumerator_t1429_0_0_0,
};
static Il2CppInterfaceOffsetPair DictionaryEnumerator_t1989_InterfacesOffsets[] = 
{
	{ &IEnumerator_t217_0_0_0, 4},
	{ &IDictionaryEnumerator_t1429_0_0_0, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DictionaryEnumerator_t1989_0_0_0;
extern const Il2CppType DictionaryEnumerator_t1989_1_0_0;
extern TypeInfo MethodDictionary_t1983_il2cpp_TypeInfo;
struct DictionaryEnumerator_t1989;
const Il2CppTypeDefinitionMetadata DictionaryEnumerator_t1989_DefinitionMetadata = 
{
	&MethodDictionary_t1983_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, DictionaryEnumerator_t1989_InterfacesTypeInfos/* implementedInterfaces */
	, DictionaryEnumerator_t1989_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, DictionaryEnumerator_t1989_VTable/* vtableMethods */
	, DictionaryEnumerator_t1989_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2001/* fieldStart */

};
TypeInfo DictionaryEnumerator_t1989_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DictionaryEnumerator"/* name */
	, ""/* namespaze */
	, DictionaryEnumerator_t1989_MethodInfos/* methods */
	, DictionaryEnumerator_t1989_PropertyInfos/* properties */
	, NULL/* events */
	, &DictionaryEnumerator_t1989_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DictionaryEnumerator_t1989_0_0_0/* byval_arg */
	, &DictionaryEnumerator_t1989_1_0_0/* this_arg */
	, &DictionaryEnumerator_t1989_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DictionaryEnumerator_t1989)/* instance_size */
	, sizeof (DictionaryEnumerator_t1989)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 4/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.MethodDictionary
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodDictionary.h"
// Metadata Definition System.Runtime.Remoting.Messaging.MethodDictionary
// System.Runtime.Remoting.Messaging.MethodDictionary
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodDictionaryMethodDeclarations.h"
extern const Il2CppType IMethodMessage_t1990_0_0_0;
static const ParameterInfo MethodDictionary_t1983_MethodDictionary__ctor_m10608_ParameterInfos[] = 
{
	{"message", 0, 134222367, 0, &IMethodMessage_t1990_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodDictionary::.ctor(System.Runtime.Remoting.Messaging.IMethodMessage)
extern const MethodInfo MethodDictionary__ctor_m10608_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MethodDictionary__ctor_m10608/* method */
	, &MethodDictionary_t1983_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, MethodDictionary_t1983_MethodDictionary__ctor_m10608_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3854/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.IEnumerator System.Runtime.Remoting.Messaging.MethodDictionary::System.Collections.IEnumerable.GetEnumerator()
extern const MethodInfo MethodDictionary_System_Collections_IEnumerable_GetEnumerator_m10609_MethodInfo = 
{
	"System.Collections.IEnumerable.GetEnumerator"/* name */
	, (methodPointerType)&MethodDictionary_System_Collections_IEnumerable_GetEnumerator_m10609/* method */
	, &MethodDictionary_t1983_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t217_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3855/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StringU5BU5D_t243_0_0_0;
extern const Il2CppType StringU5BU5D_t243_0_0_0;
static const ParameterInfo MethodDictionary_t1983_MethodDictionary_set_MethodKeys_m10610_ParameterInfos[] = 
{
	{"value", 0, 134222368, 0, &StringU5BU5D_t243_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodDictionary::set_MethodKeys(System.String[])
extern const MethodInfo MethodDictionary_set_MethodKeys_m10610_MethodInfo = 
{
	"set_MethodKeys"/* name */
	, (methodPointerType)&MethodDictionary_set_MethodKeys_m10610/* method */
	, &MethodDictionary_t1983_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, MethodDictionary_t1983_MethodDictionary_set_MethodKeys_m10610_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3856/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.IDictionary System.Runtime.Remoting.Messaging.MethodDictionary::AllocInternalProperties()
extern const MethodInfo MethodDictionary_AllocInternalProperties_m10611_MethodInfo = 
{
	"AllocInternalProperties"/* name */
	, (methodPointerType)&MethodDictionary_AllocInternalProperties_m10611/* method */
	, &MethodDictionary_t1983_il2cpp_TypeInfo/* declaring_type */
	, &IDictionary_t444_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3857/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.IDictionary System.Runtime.Remoting.Messaging.MethodDictionary::GetInternalProperties()
extern const MethodInfo MethodDictionary_GetInternalProperties_m10612_MethodInfo = 
{
	"GetInternalProperties"/* name */
	, (methodPointerType)&MethodDictionary_GetInternalProperties_m10612/* method */
	, &MethodDictionary_t1983_il2cpp_TypeInfo/* declaring_type */
	, &IDictionary_t444_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3858/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo MethodDictionary_t1983_MethodDictionary_IsOverridenKey_m10613_ParameterInfos[] = 
{
	{"key", 0, 134222369, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.Messaging.MethodDictionary::IsOverridenKey(System.String)
extern const MethodInfo MethodDictionary_IsOverridenKey_m10613_MethodInfo = 
{
	"IsOverridenKey"/* name */
	, (methodPointerType)&MethodDictionary_IsOverridenKey_m10613/* method */
	, &MethodDictionary_t1983_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, MethodDictionary_t1983_MethodDictionary_IsOverridenKey_m10613_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3859/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo MethodDictionary_t1983_MethodDictionary_get_Item_m10614_ParameterInfos[] = 
{
	{"key", 0, 134222370, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.MethodDictionary::get_Item(System.Object)
extern const MethodInfo MethodDictionary_get_Item_m10614_MethodInfo = 
{
	"get_Item"/* name */
	, (methodPointerType)&MethodDictionary_get_Item_m10614/* method */
	, &MethodDictionary_t1983_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, MethodDictionary_t1983_MethodDictionary_get_Item_m10614_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3860/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo MethodDictionary_t1983_MethodDictionary_set_Item_m10615_ParameterInfos[] = 
{
	{"key", 0, 134222371, 0, &Object_t_0_0_0},
	{"value", 1, 134222372, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodDictionary::set_Item(System.Object,System.Object)
extern const MethodInfo MethodDictionary_set_Item_m10615_MethodInfo = 
{
	"set_Item"/* name */
	, (methodPointerType)&MethodDictionary_set_Item_m10615/* method */
	, &MethodDictionary_t1983_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, MethodDictionary_t1983_MethodDictionary_set_Item_m10615_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3861/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo MethodDictionary_t1983_MethodDictionary_GetMethodProperty_m10616_ParameterInfos[] = 
{
	{"key", 0, 134222373, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.MethodDictionary::GetMethodProperty(System.String)
extern const MethodInfo MethodDictionary_GetMethodProperty_m10616_MethodInfo = 
{
	"GetMethodProperty"/* name */
	, (methodPointerType)&MethodDictionary_GetMethodProperty_m10616/* method */
	, &MethodDictionary_t1983_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, MethodDictionary_t1983_MethodDictionary_GetMethodProperty_m10616_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3862/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo MethodDictionary_t1983_MethodDictionary_SetMethodProperty_m10617_ParameterInfos[] = 
{
	{"key", 0, 134222374, 0, &String_t_0_0_0},
	{"value", 1, 134222375, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodDictionary::SetMethodProperty(System.String,System.Object)
extern const MethodInfo MethodDictionary_SetMethodProperty_m10617_MethodInfo = 
{
	"SetMethodProperty"/* name */
	, (methodPointerType)&MethodDictionary_SetMethodProperty_m10617/* method */
	, &MethodDictionary_t1983_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, MethodDictionary_t1983_MethodDictionary_SetMethodProperty_m10617_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3863/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.ICollection System.Runtime.Remoting.Messaging.MethodDictionary::get_Values()
extern const MethodInfo MethodDictionary_get_Values_m10618_MethodInfo = 
{
	"get_Values"/* name */
	, (methodPointerType)&MethodDictionary_get_Values_m10618/* method */
	, &MethodDictionary_t1983_il2cpp_TypeInfo/* declaring_type */
	, &ICollection_t440_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 18/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3864/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo MethodDictionary_t1983_MethodDictionary_Add_m10619_ParameterInfos[] = 
{
	{"key", 0, 134222376, 0, &Object_t_0_0_0},
	{"value", 1, 134222377, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodDictionary::Add(System.Object,System.Object)
extern const MethodInfo MethodDictionary_Add_m10619_MethodInfo = 
{
	"Add"/* name */
	, (methodPointerType)&MethodDictionary_Add_m10619/* method */
	, &MethodDictionary_t1983_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, MethodDictionary_t1983_MethodDictionary_Add_m10619_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3865/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo MethodDictionary_t1983_MethodDictionary_Contains_m10620_ParameterInfos[] = 
{
	{"key", 0, 134222378, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.Messaging.MethodDictionary::Contains(System.Object)
extern const MethodInfo MethodDictionary_Contains_m10620_MethodInfo = 
{
	"Contains"/* name */
	, (methodPointerType)&MethodDictionary_Contains_m10620/* method */
	, &MethodDictionary_t1983_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, MethodDictionary_t1983_MethodDictionary_Contains_m10620_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3866/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo MethodDictionary_t1983_MethodDictionary_Remove_m10621_ParameterInfos[] = 
{
	{"key", 0, 134222379, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodDictionary::Remove(System.Object)
extern const MethodInfo MethodDictionary_Remove_m10621_MethodInfo = 
{
	"Remove"/* name */
	, (methodPointerType)&MethodDictionary_Remove_m10621/* method */
	, &MethodDictionary_t1983_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, MethodDictionary_t1983_MethodDictionary_Remove_m10621_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3867/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Runtime.Remoting.Messaging.MethodDictionary::get_Count()
extern const MethodInfo MethodDictionary_get_Count_m10622_MethodInfo = 
{
	"get_Count"/* name */
	, (methodPointerType)&MethodDictionary_get_Count_m10622/* method */
	, &MethodDictionary_t1983_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3868/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.Messaging.MethodDictionary::get_IsSynchronized()
extern const MethodInfo MethodDictionary_get_IsSynchronized_m10623_MethodInfo = 
{
	"get_IsSynchronized"/* name */
	, (methodPointerType)&MethodDictionary_get_IsSynchronized_m10623/* method */
	, &MethodDictionary_t1983_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3869/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.MethodDictionary::get_SyncRoot()
extern const MethodInfo MethodDictionary_get_SyncRoot_m10624_MethodInfo = 
{
	"get_SyncRoot"/* name */
	, (methodPointerType)&MethodDictionary_get_SyncRoot_m10624/* method */
	, &MethodDictionary_t1983_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3870/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Array_t_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo MethodDictionary_t1983_MethodDictionary_CopyTo_m10625_ParameterInfos[] = 
{
	{"array", 0, 134222380, 0, &Array_t_0_0_0},
	{"index", 1, 134222381, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodDictionary::CopyTo(System.Array,System.Int32)
extern const MethodInfo MethodDictionary_CopyTo_m10625_MethodInfo = 
{
	"CopyTo"/* name */
	, (methodPointerType)&MethodDictionary_CopyTo_m10625/* method */
	, &MethodDictionary_t1983_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Int32_t253/* invoker_method */
	, MethodDictionary_t1983_MethodDictionary_CopyTo_m10625_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3871/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.IDictionaryEnumerator System.Runtime.Remoting.Messaging.MethodDictionary::GetEnumerator()
extern const MethodInfo MethodDictionary_GetEnumerator_m10626_MethodInfo = 
{
	"GetEnumerator"/* name */
	, (methodPointerType)&MethodDictionary_GetEnumerator_m10626/* method */
	, &MethodDictionary_t1983_il2cpp_TypeInfo/* declaring_type */
	, &IDictionaryEnumerator_t1429_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3872/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MethodDictionary_t1983_MethodInfos[] =
{
	&MethodDictionary__ctor_m10608_MethodInfo,
	&MethodDictionary_System_Collections_IEnumerable_GetEnumerator_m10609_MethodInfo,
	&MethodDictionary_set_MethodKeys_m10610_MethodInfo,
	&MethodDictionary_AllocInternalProperties_m10611_MethodInfo,
	&MethodDictionary_GetInternalProperties_m10612_MethodInfo,
	&MethodDictionary_IsOverridenKey_m10613_MethodInfo,
	&MethodDictionary_get_Item_m10614_MethodInfo,
	&MethodDictionary_set_Item_m10615_MethodInfo,
	&MethodDictionary_GetMethodProperty_m10616_MethodInfo,
	&MethodDictionary_SetMethodProperty_m10617_MethodInfo,
	&MethodDictionary_get_Values_m10618_MethodInfo,
	&MethodDictionary_Add_m10619_MethodInfo,
	&MethodDictionary_Contains_m10620_MethodInfo,
	&MethodDictionary_Remove_m10621_MethodInfo,
	&MethodDictionary_get_Count_m10622_MethodInfo,
	&MethodDictionary_get_IsSynchronized_m10623_MethodInfo,
	&MethodDictionary_get_SyncRoot_m10624_MethodInfo,
	&MethodDictionary_CopyTo_m10625_MethodInfo,
	&MethodDictionary_GetEnumerator_m10626_MethodInfo,
	NULL
};
extern const MethodInfo MethodDictionary_set_MethodKeys_m10610_MethodInfo;
static const PropertyInfo MethodDictionary_t1983____MethodKeys_PropertyInfo = 
{
	&MethodDictionary_t1983_il2cpp_TypeInfo/* parent */
	, "MethodKeys"/* name */
	, NULL/* get */
	, &MethodDictionary_set_MethodKeys_m10610_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MethodDictionary_t1983____Item_PropertyInfo = 
{
	&MethodDictionary_t1983_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &MethodDictionary_get_Item_m10614_MethodInfo/* get */
	, &MethodDictionary_set_Item_m10615_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MethodDictionary_t1983____Values_PropertyInfo = 
{
	&MethodDictionary_t1983_il2cpp_TypeInfo/* parent */
	, "Values"/* name */
	, &MethodDictionary_get_Values_m10618_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MethodDictionary_t1983____Count_PropertyInfo = 
{
	&MethodDictionary_t1983_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &MethodDictionary_get_Count_m10622_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MethodDictionary_t1983____IsSynchronized_PropertyInfo = 
{
	&MethodDictionary_t1983_il2cpp_TypeInfo/* parent */
	, "IsSynchronized"/* name */
	, &MethodDictionary_get_IsSynchronized_m10623_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MethodDictionary_t1983____SyncRoot_PropertyInfo = 
{
	&MethodDictionary_t1983_il2cpp_TypeInfo/* parent */
	, "SyncRoot"/* name */
	, &MethodDictionary_get_SyncRoot_m10624_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* MethodDictionary_t1983_PropertyInfos[] =
{
	&MethodDictionary_t1983____MethodKeys_PropertyInfo,
	&MethodDictionary_t1983____Item_PropertyInfo,
	&MethodDictionary_t1983____Values_PropertyInfo,
	&MethodDictionary_t1983____Count_PropertyInfo,
	&MethodDictionary_t1983____IsSynchronized_PropertyInfo,
	&MethodDictionary_t1983____SyncRoot_PropertyInfo,
	NULL
};
static const Il2CppType* MethodDictionary_t1983_il2cpp_TypeInfo__nestedTypes[1] =
{
	&DictionaryEnumerator_t1989_0_0_0,
};
static const Il2CppMethodReference MethodDictionary_t1983_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&MethodDictionary_System_Collections_IEnumerable_GetEnumerator_m10609_MethodInfo,
	&MethodDictionary_get_Count_m10622_MethodInfo,
	&MethodDictionary_get_IsSynchronized_m10623_MethodInfo,
	&MethodDictionary_get_SyncRoot_m10624_MethodInfo,
	&MethodDictionary_CopyTo_m10625_MethodInfo,
	&MethodDictionary_get_Item_m10614_MethodInfo,
	&MethodDictionary_set_Item_m10615_MethodInfo,
	&MethodDictionary_Add_m10619_MethodInfo,
	&MethodDictionary_Contains_m10620_MethodInfo,
	&MethodDictionary_GetEnumerator_m10626_MethodInfo,
	&MethodDictionary_Remove_m10621_MethodInfo,
	&MethodDictionary_AllocInternalProperties_m10611_MethodInfo,
	&MethodDictionary_GetMethodProperty_m10616_MethodInfo,
	&MethodDictionary_SetMethodProperty_m10617_MethodInfo,
	&MethodDictionary_get_Values_m10618_MethodInfo,
};
static bool MethodDictionary_t1983_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* MethodDictionary_t1983_InterfacesTypeInfos[] = 
{
	&IEnumerable_t438_0_0_0,
	&ICollection_t440_0_0_0,
	&IDictionary_t444_0_0_0,
};
static Il2CppInterfaceOffsetPair MethodDictionary_t1983_InterfacesOffsets[] = 
{
	{ &IEnumerable_t438_0_0_0, 4},
	{ &ICollection_t440_0_0_0, 5},
	{ &IDictionary_t444_0_0_0, 9},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MethodDictionary_t1983_1_0_0;
struct MethodDictionary_t1983;
const Il2CppTypeDefinitionMetadata MethodDictionary_t1983_DefinitionMetadata = 
{
	NULL/* declaringType */
	, MethodDictionary_t1983_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, MethodDictionary_t1983_InterfacesTypeInfos/* implementedInterfaces */
	, MethodDictionary_t1983_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MethodDictionary_t1983_VTable/* vtableMethods */
	, MethodDictionary_t1983_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2004/* fieldStart */

};
TypeInfo MethodDictionary_t1983_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MethodDictionary"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, MethodDictionary_t1983_MethodInfos/* methods */
	, MethodDictionary_t1983_PropertyInfos/* properties */
	, NULL/* events */
	, &MethodDictionary_t1983_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 583/* custom_attributes_cache */
	, &MethodDictionary_t1983_0_0_0/* byval_arg */
	, &MethodDictionary_t1983_1_0_0/* this_arg */
	, &MethodDictionary_t1983_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MethodDictionary_t1983)/* instance_size */
	, sizeof (MethodDictionary_t1983)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(MethodDictionary_t1983_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 19/* method_count */
	, 6/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 19/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.MethodReturnDictionary
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodReturnDicti.h"
// Metadata Definition System.Runtime.Remoting.Messaging.MethodReturnDictionary
extern TypeInfo MethodReturnDictionary_t1991_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.MethodReturnDictionary
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodReturnDictiMethodDeclarations.h"
extern const Il2CppType IMethodReturnMessage_t2288_0_0_0;
static const ParameterInfo MethodReturnDictionary_t1991_MethodReturnDictionary__ctor_m10627_ParameterInfos[] = 
{
	{"message", 0, 134222383, 0, &IMethodReturnMessage_t2288_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodReturnDictionary::.ctor(System.Runtime.Remoting.Messaging.IMethodReturnMessage)
extern const MethodInfo MethodReturnDictionary__ctor_m10627_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MethodReturnDictionary__ctor_m10627/* method */
	, &MethodReturnDictionary_t1991_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, MethodReturnDictionary_t1991_MethodReturnDictionary__ctor_m10627_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3879/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodReturnDictionary::.cctor()
extern const MethodInfo MethodReturnDictionary__cctor_m10628_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&MethodReturnDictionary__cctor_m10628/* method */
	, &MethodReturnDictionary_t1991_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3880/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MethodReturnDictionary_t1991_MethodInfos[] =
{
	&MethodReturnDictionary__ctor_m10627_MethodInfo,
	&MethodReturnDictionary__cctor_m10628_MethodInfo,
	NULL
};
static const Il2CppMethodReference MethodReturnDictionary_t1991_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&MethodDictionary_System_Collections_IEnumerable_GetEnumerator_m10609_MethodInfo,
	&MethodDictionary_get_Count_m10622_MethodInfo,
	&MethodDictionary_get_IsSynchronized_m10623_MethodInfo,
	&MethodDictionary_get_SyncRoot_m10624_MethodInfo,
	&MethodDictionary_CopyTo_m10625_MethodInfo,
	&MethodDictionary_get_Item_m10614_MethodInfo,
	&MethodDictionary_set_Item_m10615_MethodInfo,
	&MethodDictionary_Add_m10619_MethodInfo,
	&MethodDictionary_Contains_m10620_MethodInfo,
	&MethodDictionary_GetEnumerator_m10626_MethodInfo,
	&MethodDictionary_Remove_m10621_MethodInfo,
	&MethodDictionary_AllocInternalProperties_m10611_MethodInfo,
	&MethodDictionary_GetMethodProperty_m10616_MethodInfo,
	&MethodDictionary_SetMethodProperty_m10617_MethodInfo,
	&MethodDictionary_get_Values_m10618_MethodInfo,
};
static bool MethodReturnDictionary_t1991_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair MethodReturnDictionary_t1991_InterfacesOffsets[] = 
{
	{ &IEnumerable_t438_0_0_0, 4},
	{ &ICollection_t440_0_0_0, 5},
	{ &IDictionary_t444_0_0_0, 9},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MethodReturnDictionary_t1991_0_0_0;
extern const Il2CppType MethodReturnDictionary_t1991_1_0_0;
struct MethodReturnDictionary_t1991;
const Il2CppTypeDefinitionMetadata MethodReturnDictionary_t1991_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MethodReturnDictionary_t1991_InterfacesOffsets/* interfaceOffsets */
	, &MethodDictionary_t1983_0_0_0/* parent */
	, MethodReturnDictionary_t1991_VTable/* vtableMethods */
	, MethodReturnDictionary_t1991_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2010/* fieldStart */

};
TypeInfo MethodReturnDictionary_t1991_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MethodReturnDictionary"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, MethodReturnDictionary_t1991_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &MethodReturnDictionary_t1991_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MethodReturnDictionary_t1991_0_0_0/* byval_arg */
	, &MethodReturnDictionary_t1991_1_0_0/* this_arg */
	, &MethodReturnDictionary_t1991_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MethodReturnDictionary_t1991)/* instance_size */
	, sizeof (MethodReturnDictionary_t1991)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(MethodReturnDictionary_t1991_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 19/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.MonoMethodMessage
#include "mscorlib_System_Runtime_Remoting_Messaging_MonoMethodMessage.h"
// Metadata Definition System.Runtime.Remoting.Messaging.MonoMethodMessage
extern TypeInfo MonoMethodMessage_t1976_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.MonoMethodMessage
#include "mscorlib_System_Runtime_Remoting_Messaging_MonoMethodMessageMethodDeclarations.h"
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.IDictionary System.Runtime.Remoting.Messaging.MonoMethodMessage::get_Properties()
extern const MethodInfo MonoMethodMessage_get_Properties_m10629_MethodInfo = 
{
	"get_Properties"/* name */
	, (methodPointerType)&MonoMethodMessage_get_Properties_m10629/* method */
	, &MonoMethodMessage_t1976_il2cpp_TypeInfo/* declaring_type */
	, &IDictionary_t444_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3881/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Runtime.Remoting.Messaging.MonoMethodMessage::get_ArgCount()
extern const MethodInfo MonoMethodMessage_get_ArgCount_m10630_MethodInfo = 
{
	"get_ArgCount"/* name */
	, (methodPointerType)&MonoMethodMessage_get_ArgCount_m10630/* method */
	, &MonoMethodMessage_t1976_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3882/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Runtime.Remoting.Messaging.MonoMethodMessage::get_Args()
extern const MethodInfo MonoMethodMessage_get_Args_m10631_MethodInfo = 
{
	"get_Args"/* name */
	, (methodPointerType)&MonoMethodMessage_get_Args_m10631/* method */
	, &MonoMethodMessage_t1976_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t224_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3883/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Messaging.LogicalCallContext System.Runtime.Remoting.Messaging.MonoMethodMessage::get_LogicalCallContext()
extern const MethodInfo MonoMethodMessage_get_LogicalCallContext_m10632_MethodInfo = 
{
	"get_LogicalCallContext"/* name */
	, (methodPointerType)&MonoMethodMessage_get_LogicalCallContext_m10632/* method */
	, &MonoMethodMessage_t1976_il2cpp_TypeInfo/* declaring_type */
	, &LogicalCallContext_t1987_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3884/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodBase System.Runtime.Remoting.Messaging.MonoMethodMessage::get_MethodBase()
extern const MethodInfo MonoMethodMessage_get_MethodBase_m10633_MethodInfo = 
{
	"get_MethodBase"/* name */
	, (methodPointerType)&MonoMethodMessage_get_MethodBase_m10633/* method */
	, &MonoMethodMessage_t1976_il2cpp_TypeInfo/* declaring_type */
	, &MethodBase_t1102_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3885/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Messaging.MonoMethodMessage::get_MethodName()
extern const MethodInfo MonoMethodMessage_get_MethodName_m10634_MethodInfo = 
{
	"get_MethodName"/* name */
	, (methodPointerType)&MonoMethodMessage_get_MethodName_m10634/* method */
	, &MonoMethodMessage_t1976_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3886/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.MonoMethodMessage::get_MethodSignature()
extern const MethodInfo MonoMethodMessage_get_MethodSignature_m10635_MethodInfo = 
{
	"get_MethodSignature"/* name */
	, (methodPointerType)&MonoMethodMessage_get_MethodSignature_m10635/* method */
	, &MonoMethodMessage_t1976_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3887/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Messaging.MonoMethodMessage::get_TypeName()
extern const MethodInfo MonoMethodMessage_get_TypeName_m10636_MethodInfo = 
{
	"get_TypeName"/* name */
	, (methodPointerType)&MonoMethodMessage_get_TypeName_m10636/* method */
	, &MonoMethodMessage_t1976_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3888/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Messaging.MonoMethodMessage::get_Uri()
extern const MethodInfo MonoMethodMessage_get_Uri_m10637_MethodInfo = 
{
	"get_Uri"/* name */
	, (methodPointerType)&MonoMethodMessage_get_Uri_m10637/* method */
	, &MonoMethodMessage_t1976_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3889/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo MonoMethodMessage_t1976_MonoMethodMessage_set_Uri_m10638_ParameterInfos[] = 
{
	{"value", 0, 134222384, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MonoMethodMessage::set_Uri(System.String)
extern const MethodInfo MonoMethodMessage_set_Uri_m10638_MethodInfo = 
{
	"set_Uri"/* name */
	, (methodPointerType)&MonoMethodMessage_set_Uri_m10638/* method */
	, &MonoMethodMessage_t1976_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, MonoMethodMessage_t1976_MonoMethodMessage_set_Uri_m10638_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3890/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo MonoMethodMessage_t1976_MonoMethodMessage_GetArg_m10639_ParameterInfos[] = 
{
	{"arg_num", 0, 134222385, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.MonoMethodMessage::GetArg(System.Int32)
extern const MethodInfo MonoMethodMessage_GetArg_m10639_MethodInfo = 
{
	"GetArg"/* name */
	, (methodPointerType)&MonoMethodMessage_GetArg_m10639/* method */
	, &MonoMethodMessage_t1976_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t253/* invoker_method */
	, MonoMethodMessage_t1976_MonoMethodMessage_GetArg_m10639_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3891/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Exception System.Runtime.Remoting.Messaging.MonoMethodMessage::get_Exception()
extern const MethodInfo MonoMethodMessage_get_Exception_m10640_MethodInfo = 
{
	"get_Exception"/* name */
	, (methodPointerType)&MonoMethodMessage_get_Exception_m10640/* method */
	, &MonoMethodMessage_t1976_il2cpp_TypeInfo/* declaring_type */
	, &Exception_t232_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3892/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Runtime.Remoting.Messaging.MonoMethodMessage::get_OutArgCount()
extern const MethodInfo MonoMethodMessage_get_OutArgCount_m10641_MethodInfo = 
{
	"get_OutArgCount"/* name */
	, (methodPointerType)&MonoMethodMessage_get_OutArgCount_m10641/* method */
	, &MonoMethodMessage_t1976_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3893/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Runtime.Remoting.Messaging.MonoMethodMessage::get_OutArgs()
extern const MethodInfo MonoMethodMessage_get_OutArgs_m10642_MethodInfo = 
{
	"get_OutArgs"/* name */
	, (methodPointerType)&MonoMethodMessage_get_OutArgs_m10642/* method */
	, &MonoMethodMessage_t1976_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t224_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3894/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.MonoMethodMessage::get_ReturnValue()
extern const MethodInfo MonoMethodMessage_get_ReturnValue_m10643_MethodInfo = 
{
	"get_ReturnValue"/* name */
	, (methodPointerType)&MonoMethodMessage_get_ReturnValue_m10643/* method */
	, &MonoMethodMessage_t1976_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 18/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3895/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType CallType_t1992_0_0_0;
extern void* RuntimeInvoker_CallType_t1992 (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Messaging.CallType System.Runtime.Remoting.Messaging.MonoMethodMessage::get_CallType()
extern const MethodInfo MonoMethodMessage_get_CallType_m10644_MethodInfo = 
{
	"get_CallType"/* name */
	, (methodPointerType)&MonoMethodMessage_get_CallType_m10644/* method */
	, &MonoMethodMessage_t1976_il2cpp_TypeInfo/* declaring_type */
	, &CallType_t1992_0_0_0/* return_type */
	, RuntimeInvoker_CallType_t1992/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2179/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3896/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MonoMethodMessage_t1976_MethodInfos[] =
{
	&MonoMethodMessage_get_Properties_m10629_MethodInfo,
	&MonoMethodMessage_get_ArgCount_m10630_MethodInfo,
	&MonoMethodMessage_get_Args_m10631_MethodInfo,
	&MonoMethodMessage_get_LogicalCallContext_m10632_MethodInfo,
	&MonoMethodMessage_get_MethodBase_m10633_MethodInfo,
	&MonoMethodMessage_get_MethodName_m10634_MethodInfo,
	&MonoMethodMessage_get_MethodSignature_m10635_MethodInfo,
	&MonoMethodMessage_get_TypeName_m10636_MethodInfo,
	&MonoMethodMessage_get_Uri_m10637_MethodInfo,
	&MonoMethodMessage_set_Uri_m10638_MethodInfo,
	&MonoMethodMessage_GetArg_m10639_MethodInfo,
	&MonoMethodMessage_get_Exception_m10640_MethodInfo,
	&MonoMethodMessage_get_OutArgCount_m10641_MethodInfo,
	&MonoMethodMessage_get_OutArgs_m10642_MethodInfo,
	&MonoMethodMessage_get_ReturnValue_m10643_MethodInfo,
	&MonoMethodMessage_get_CallType_m10644_MethodInfo,
	NULL
};
extern const MethodInfo MonoMethodMessage_get_Properties_m10629_MethodInfo;
static const PropertyInfo MonoMethodMessage_t1976____Properties_PropertyInfo = 
{
	&MonoMethodMessage_t1976_il2cpp_TypeInfo/* parent */
	, "Properties"/* name */
	, &MonoMethodMessage_get_Properties_m10629_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoMethodMessage_get_ArgCount_m10630_MethodInfo;
static const PropertyInfo MonoMethodMessage_t1976____ArgCount_PropertyInfo = 
{
	&MonoMethodMessage_t1976_il2cpp_TypeInfo/* parent */
	, "ArgCount"/* name */
	, &MonoMethodMessage_get_ArgCount_m10630_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoMethodMessage_get_Args_m10631_MethodInfo;
static const PropertyInfo MonoMethodMessage_t1976____Args_PropertyInfo = 
{
	&MonoMethodMessage_t1976_il2cpp_TypeInfo/* parent */
	, "Args"/* name */
	, &MonoMethodMessage_get_Args_m10631_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoMethodMessage_get_LogicalCallContext_m10632_MethodInfo;
static const PropertyInfo MonoMethodMessage_t1976____LogicalCallContext_PropertyInfo = 
{
	&MonoMethodMessage_t1976_il2cpp_TypeInfo/* parent */
	, "LogicalCallContext"/* name */
	, &MonoMethodMessage_get_LogicalCallContext_m10632_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoMethodMessage_get_MethodBase_m10633_MethodInfo;
static const PropertyInfo MonoMethodMessage_t1976____MethodBase_PropertyInfo = 
{
	&MonoMethodMessage_t1976_il2cpp_TypeInfo/* parent */
	, "MethodBase"/* name */
	, &MonoMethodMessage_get_MethodBase_m10633_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoMethodMessage_get_MethodName_m10634_MethodInfo;
static const PropertyInfo MonoMethodMessage_t1976____MethodName_PropertyInfo = 
{
	&MonoMethodMessage_t1976_il2cpp_TypeInfo/* parent */
	, "MethodName"/* name */
	, &MonoMethodMessage_get_MethodName_m10634_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoMethodMessage_get_MethodSignature_m10635_MethodInfo;
static const PropertyInfo MonoMethodMessage_t1976____MethodSignature_PropertyInfo = 
{
	&MonoMethodMessage_t1976_il2cpp_TypeInfo/* parent */
	, "MethodSignature"/* name */
	, &MonoMethodMessage_get_MethodSignature_m10635_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoMethodMessage_get_TypeName_m10636_MethodInfo;
static const PropertyInfo MonoMethodMessage_t1976____TypeName_PropertyInfo = 
{
	&MonoMethodMessage_t1976_il2cpp_TypeInfo/* parent */
	, "TypeName"/* name */
	, &MonoMethodMessage_get_TypeName_m10636_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoMethodMessage_get_Uri_m10637_MethodInfo;
extern const MethodInfo MonoMethodMessage_set_Uri_m10638_MethodInfo;
static const PropertyInfo MonoMethodMessage_t1976____Uri_PropertyInfo = 
{
	&MonoMethodMessage_t1976_il2cpp_TypeInfo/* parent */
	, "Uri"/* name */
	, &MonoMethodMessage_get_Uri_m10637_MethodInfo/* get */
	, &MonoMethodMessage_set_Uri_m10638_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoMethodMessage_get_Exception_m10640_MethodInfo;
static const PropertyInfo MonoMethodMessage_t1976____Exception_PropertyInfo = 
{
	&MonoMethodMessage_t1976_il2cpp_TypeInfo/* parent */
	, "Exception"/* name */
	, &MonoMethodMessage_get_Exception_m10640_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoMethodMessage_get_OutArgCount_m10641_MethodInfo;
static const PropertyInfo MonoMethodMessage_t1976____OutArgCount_PropertyInfo = 
{
	&MonoMethodMessage_t1976_il2cpp_TypeInfo/* parent */
	, "OutArgCount"/* name */
	, &MonoMethodMessage_get_OutArgCount_m10641_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoMethodMessage_get_OutArgs_m10642_MethodInfo;
static const PropertyInfo MonoMethodMessage_t1976____OutArgs_PropertyInfo = 
{
	&MonoMethodMessage_t1976_il2cpp_TypeInfo/* parent */
	, "OutArgs"/* name */
	, &MonoMethodMessage_get_OutArgs_m10642_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoMethodMessage_get_ReturnValue_m10643_MethodInfo;
static const PropertyInfo MonoMethodMessage_t1976____ReturnValue_PropertyInfo = 
{
	&MonoMethodMessage_t1976_il2cpp_TypeInfo/* parent */
	, "ReturnValue"/* name */
	, &MonoMethodMessage_get_ReturnValue_m10643_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoMethodMessage_get_CallType_m10644_MethodInfo;
static const PropertyInfo MonoMethodMessage_t1976____CallType_PropertyInfo = 
{
	&MonoMethodMessage_t1976_il2cpp_TypeInfo/* parent */
	, "CallType"/* name */
	, &MonoMethodMessage_get_CallType_m10644_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* MonoMethodMessage_t1976_PropertyInfos[] =
{
	&MonoMethodMessage_t1976____Properties_PropertyInfo,
	&MonoMethodMessage_t1976____ArgCount_PropertyInfo,
	&MonoMethodMessage_t1976____Args_PropertyInfo,
	&MonoMethodMessage_t1976____LogicalCallContext_PropertyInfo,
	&MonoMethodMessage_t1976____MethodBase_PropertyInfo,
	&MonoMethodMessage_t1976____MethodName_PropertyInfo,
	&MonoMethodMessage_t1976____MethodSignature_PropertyInfo,
	&MonoMethodMessage_t1976____TypeName_PropertyInfo,
	&MonoMethodMessage_t1976____Uri_PropertyInfo,
	&MonoMethodMessage_t1976____Exception_PropertyInfo,
	&MonoMethodMessage_t1976____OutArgCount_PropertyInfo,
	&MonoMethodMessage_t1976____OutArgs_PropertyInfo,
	&MonoMethodMessage_t1976____ReturnValue_PropertyInfo,
	&MonoMethodMessage_t1976____CallType_PropertyInfo,
	NULL
};
extern const MethodInfo MonoMethodMessage_GetArg_m10639_MethodInfo;
static const Il2CppMethodReference MonoMethodMessage_t1976_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&MonoMethodMessage_set_Uri_m10638_MethodInfo,
	&MonoMethodMessage_get_Properties_m10629_MethodInfo,
	&MonoMethodMessage_get_ArgCount_m10630_MethodInfo,
	&MonoMethodMessage_get_Args_m10631_MethodInfo,
	&MonoMethodMessage_get_LogicalCallContext_m10632_MethodInfo,
	&MonoMethodMessage_get_MethodBase_m10633_MethodInfo,
	&MonoMethodMessage_get_MethodName_m10634_MethodInfo,
	&MonoMethodMessage_get_MethodSignature_m10635_MethodInfo,
	&MonoMethodMessage_get_TypeName_m10636_MethodInfo,
	&MonoMethodMessage_get_Uri_m10637_MethodInfo,
	&MonoMethodMessage_GetArg_m10639_MethodInfo,
	&MonoMethodMessage_get_Exception_m10640_MethodInfo,
	&MonoMethodMessage_get_OutArgCount_m10641_MethodInfo,
	&MonoMethodMessage_get_OutArgs_m10642_MethodInfo,
	&MonoMethodMessage_get_ReturnValue_m10643_MethodInfo,
};
static bool MonoMethodMessage_t1976_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* MonoMethodMessage_t1976_InterfacesTypeInfos[] = 
{
	&IInternalMessage_t2303_0_0_0,
	&IMessage_t1978_0_0_0,
	&IMethodCallMessage_t2289_0_0_0,
	&IMethodMessage_t1990_0_0_0,
	&IMethodReturnMessage_t2288_0_0_0,
};
static Il2CppInterfaceOffsetPair MonoMethodMessage_t1976_InterfacesOffsets[] = 
{
	{ &IInternalMessage_t2303_0_0_0, 4},
	{ &IMessage_t1978_0_0_0, 5},
	{ &IMethodCallMessage_t2289_0_0_0, 6},
	{ &IMethodMessage_t1990_0_0_0, 6},
	{ &IMethodReturnMessage_t2288_0_0_0, 15},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MonoMethodMessage_t1976_1_0_0;
struct MonoMethodMessage_t1976;
const Il2CppTypeDefinitionMetadata MonoMethodMessage_t1976_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, MonoMethodMessage_t1976_InterfacesTypeInfos/* implementedInterfaces */
	, MonoMethodMessage_t1976_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MonoMethodMessage_t1976_VTable/* vtableMethods */
	, MonoMethodMessage_t1976_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2012/* fieldStart */

};
TypeInfo MonoMethodMessage_t1976_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoMethodMessage"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, MonoMethodMessage_t1976_MethodInfos/* methods */
	, MonoMethodMessage_t1976_PropertyInfos/* properties */
	, NULL/* events */
	, &MonoMethodMessage_t1976_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoMethodMessage_t1976_0_0_0/* byval_arg */
	, &MonoMethodMessage_t1976_1_0_0/* this_arg */
	, &MonoMethodMessage_t1976_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoMethodMessage_t1976)/* instance_size */
	, sizeof (MonoMethodMessage_t1976)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 16/* method_count */
	, 14/* property_count */
	, 10/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 19/* vtable_count */
	, 5/* interfaces_count */
	, 5/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.CallType
#include "mscorlib_System_Runtime_Remoting_Messaging_CallType.h"
// Metadata Definition System.Runtime.Remoting.Messaging.CallType
extern TypeInfo CallType_t1992_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.CallType
#include "mscorlib_System_Runtime_Remoting_Messaging_CallTypeMethodDeclarations.h"
static const MethodInfo* CallType_t1992_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference CallType_t1992_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool CallType_t1992_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair CallType_t1992_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CallType_t1992_1_0_0;
const Il2CppTypeDefinitionMetadata CallType_t1992_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CallType_t1992_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, CallType_t1992_VTable/* vtableMethods */
	, CallType_t1992_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2022/* fieldStart */

};
TypeInfo CallType_t1992_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CallType"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, CallType_t1992_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CallType_t1992_0_0_0/* byval_arg */
	, &CallType_t1992_1_0_0/* this_arg */
	, &CallType_t1992_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CallType_t1992)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (CallType_t1992)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.OneWayAttribute
#include "mscorlib_System_Runtime_Remoting_Messaging_OneWayAttribute.h"
// Metadata Definition System.Runtime.Remoting.Messaging.OneWayAttribute
extern TypeInfo OneWayAttribute_t1993_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.OneWayAttribute
#include "mscorlib_System_Runtime_Remoting_Messaging_OneWayAttributeMethodDeclarations.h"
static const MethodInfo* OneWayAttribute_t1993_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference OneWayAttribute_t1993_VTable[] =
{
	&Attribute_Equals_m5384_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Attribute_GetHashCode_m5253_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool OneWayAttribute_t1993_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair OneWayAttribute_t1993_InterfacesOffsets[] = 
{
	{ &_Attribute_t1145_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType OneWayAttribute_t1993_0_0_0;
extern const Il2CppType OneWayAttribute_t1993_1_0_0;
struct OneWayAttribute_t1993;
const Il2CppTypeDefinitionMetadata OneWayAttribute_t1993_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, OneWayAttribute_t1993_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t805_0_0_0/* parent */
	, OneWayAttribute_t1993_VTable/* vtableMethods */
	, OneWayAttribute_t1993_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo OneWayAttribute_t1993_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "OneWayAttribute"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, OneWayAttribute_t1993_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &OneWayAttribute_t1993_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 586/* custom_attributes_cache */
	, &OneWayAttribute_t1993_0_0_0/* byval_arg */
	, &OneWayAttribute_t1993_1_0_0/* this_arg */
	, &OneWayAttribute_t1993_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OneWayAttribute_t1993)/* instance_size */
	, sizeof (OneWayAttribute_t1993)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.RemotingSurrogate
#include "mscorlib_System_Runtime_Remoting_Messaging_RemotingSurrogate.h"
// Metadata Definition System.Runtime.Remoting.Messaging.RemotingSurrogate
extern TypeInfo RemotingSurrogate_t1994_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.RemotingSurrogate
#include "mscorlib_System_Runtime_Remoting_Messaging_RemotingSurrogateMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.RemotingSurrogate::.ctor()
extern const MethodInfo RemotingSurrogate__ctor_m10645_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RemotingSurrogate__ctor_m10645/* method */
	, &RemotingSurrogate_t1994_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3897/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType SerializationInfo_t1043_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
static const ParameterInfo RemotingSurrogate_t1994_RemotingSurrogate_GetObjectData_m10646_ParameterInfos[] = 
{
	{"obj", 0, 134222386, 0, &Object_t_0_0_0},
	{"si", 1, 134222387, 0, &SerializationInfo_t1043_0_0_0},
	{"sc", 2, 134222388, 0, &StreamingContext_t1044_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.RemotingSurrogate::GetObjectData(System.Object,System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo RemotingSurrogate_GetObjectData_m10646_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&RemotingSurrogate_GetObjectData_m10646/* method */
	, &RemotingSurrogate_t1994_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t_StreamingContext_t1044/* invoker_method */
	, RemotingSurrogate_t1994_RemotingSurrogate_GetObjectData_m10646_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3898/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType SerializationInfo_t1043_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
extern const Il2CppType ISurrogateSelector_t1996_0_0_0;
extern const Il2CppType ISurrogateSelector_t1996_0_0_0;
static const ParameterInfo RemotingSurrogate_t1994_RemotingSurrogate_SetObjectData_m10647_ParameterInfos[] = 
{
	{"obj", 0, 134222389, 0, &Object_t_0_0_0},
	{"si", 1, 134222390, 0, &SerializationInfo_t1043_0_0_0},
	{"sc", 2, 134222391, 0, &StreamingContext_t1044_0_0_0},
	{"selector", 3, 134222392, 0, &ISurrogateSelector_t1996_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_StreamingContext_t1044_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.RemotingSurrogate::SetObjectData(System.Object,System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext,System.Runtime.Serialization.ISurrogateSelector)
extern const MethodInfo RemotingSurrogate_SetObjectData_m10647_MethodInfo = 
{
	"SetObjectData"/* name */
	, (methodPointerType)&RemotingSurrogate_SetObjectData_m10647/* method */
	, &RemotingSurrogate_t1994_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_StreamingContext_t1044_Object_t/* invoker_method */
	, RemotingSurrogate_t1994_RemotingSurrogate_SetObjectData_m10647_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3899/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* RemotingSurrogate_t1994_MethodInfos[] =
{
	&RemotingSurrogate__ctor_m10645_MethodInfo,
	&RemotingSurrogate_GetObjectData_m10646_MethodInfo,
	&RemotingSurrogate_SetObjectData_m10647_MethodInfo,
	NULL
};
extern const MethodInfo RemotingSurrogate_GetObjectData_m10646_MethodInfo;
extern const MethodInfo RemotingSurrogate_SetObjectData_m10647_MethodInfo;
static const Il2CppMethodReference RemotingSurrogate_t1994_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&RemotingSurrogate_GetObjectData_m10646_MethodInfo,
	&RemotingSurrogate_SetObjectData_m10647_MethodInfo,
	&RemotingSurrogate_GetObjectData_m10646_MethodInfo,
	&RemotingSurrogate_SetObjectData_m10647_MethodInfo,
};
static bool RemotingSurrogate_t1994_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ISerializationSurrogate_t2056_0_0_0;
static const Il2CppType* RemotingSurrogate_t1994_InterfacesTypeInfos[] = 
{
	&ISerializationSurrogate_t2056_0_0_0,
};
static Il2CppInterfaceOffsetPair RemotingSurrogate_t1994_InterfacesOffsets[] = 
{
	{ &ISerializationSurrogate_t2056_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RemotingSurrogate_t1994_0_0_0;
extern const Il2CppType RemotingSurrogate_t1994_1_0_0;
struct RemotingSurrogate_t1994;
const Il2CppTypeDefinitionMetadata RemotingSurrogate_t1994_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, RemotingSurrogate_t1994_InterfacesTypeInfos/* implementedInterfaces */
	, RemotingSurrogate_t1994_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, RemotingSurrogate_t1994_VTable/* vtableMethods */
	, RemotingSurrogate_t1994_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo RemotingSurrogate_t1994_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RemotingSurrogate"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, RemotingSurrogate_t1994_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &RemotingSurrogate_t1994_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RemotingSurrogate_t1994_0_0_0/* byval_arg */
	, &RemotingSurrogate_t1994_1_0_0/* this_arg */
	, &RemotingSurrogate_t1994_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RemotingSurrogate_t1994)/* instance_size */
	, sizeof (RemotingSurrogate_t1994)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.ObjRefSurrogate
#include "mscorlib_System_Runtime_Remoting_Messaging_ObjRefSurrogate.h"
// Metadata Definition System.Runtime.Remoting.Messaging.ObjRefSurrogate
extern TypeInfo ObjRefSurrogate_t1995_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.ObjRefSurrogate
#include "mscorlib_System_Runtime_Remoting_Messaging_ObjRefSurrogateMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.ObjRefSurrogate::.ctor()
extern const MethodInfo ObjRefSurrogate__ctor_m10648_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ObjRefSurrogate__ctor_m10648/* method */
	, &ObjRefSurrogate_t1995_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3900/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType SerializationInfo_t1043_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
static const ParameterInfo ObjRefSurrogate_t1995_ObjRefSurrogate_GetObjectData_m10649_ParameterInfos[] = 
{
	{"obj", 0, 134222393, 0, &Object_t_0_0_0},
	{"si", 1, 134222394, 0, &SerializationInfo_t1043_0_0_0},
	{"sc", 2, 134222395, 0, &StreamingContext_t1044_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.ObjRefSurrogate::GetObjectData(System.Object,System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo ObjRefSurrogate_GetObjectData_m10649_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&ObjRefSurrogate_GetObjectData_m10649/* method */
	, &ObjRefSurrogate_t1995_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t_StreamingContext_t1044/* invoker_method */
	, ObjRefSurrogate_t1995_ObjRefSurrogate_GetObjectData_m10649_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3901/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType SerializationInfo_t1043_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
extern const Il2CppType ISurrogateSelector_t1996_0_0_0;
static const ParameterInfo ObjRefSurrogate_t1995_ObjRefSurrogate_SetObjectData_m10650_ParameterInfos[] = 
{
	{"obj", 0, 134222396, 0, &Object_t_0_0_0},
	{"si", 1, 134222397, 0, &SerializationInfo_t1043_0_0_0},
	{"sc", 2, 134222398, 0, &StreamingContext_t1044_0_0_0},
	{"selector", 3, 134222399, 0, &ISurrogateSelector_t1996_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_StreamingContext_t1044_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.ObjRefSurrogate::SetObjectData(System.Object,System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext,System.Runtime.Serialization.ISurrogateSelector)
extern const MethodInfo ObjRefSurrogate_SetObjectData_m10650_MethodInfo = 
{
	"SetObjectData"/* name */
	, (methodPointerType)&ObjRefSurrogate_SetObjectData_m10650/* method */
	, &ObjRefSurrogate_t1995_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_StreamingContext_t1044_Object_t/* invoker_method */
	, ObjRefSurrogate_t1995_ObjRefSurrogate_SetObjectData_m10650_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3902/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ObjRefSurrogate_t1995_MethodInfos[] =
{
	&ObjRefSurrogate__ctor_m10648_MethodInfo,
	&ObjRefSurrogate_GetObjectData_m10649_MethodInfo,
	&ObjRefSurrogate_SetObjectData_m10650_MethodInfo,
	NULL
};
extern const MethodInfo ObjRefSurrogate_GetObjectData_m10649_MethodInfo;
extern const MethodInfo ObjRefSurrogate_SetObjectData_m10650_MethodInfo;
static const Il2CppMethodReference ObjRefSurrogate_t1995_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&ObjRefSurrogate_GetObjectData_m10649_MethodInfo,
	&ObjRefSurrogate_SetObjectData_m10650_MethodInfo,
	&ObjRefSurrogate_GetObjectData_m10649_MethodInfo,
	&ObjRefSurrogate_SetObjectData_m10650_MethodInfo,
};
static bool ObjRefSurrogate_t1995_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* ObjRefSurrogate_t1995_InterfacesTypeInfos[] = 
{
	&ISerializationSurrogate_t2056_0_0_0,
};
static Il2CppInterfaceOffsetPair ObjRefSurrogate_t1995_InterfacesOffsets[] = 
{
	{ &ISerializationSurrogate_t2056_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ObjRefSurrogate_t1995_0_0_0;
extern const Il2CppType ObjRefSurrogate_t1995_1_0_0;
struct ObjRefSurrogate_t1995;
const Il2CppTypeDefinitionMetadata ObjRefSurrogate_t1995_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ObjRefSurrogate_t1995_InterfacesTypeInfos/* implementedInterfaces */
	, ObjRefSurrogate_t1995_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ObjRefSurrogate_t1995_VTable/* vtableMethods */
	, ObjRefSurrogate_t1995_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ObjRefSurrogate_t1995_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ObjRefSurrogate"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, ObjRefSurrogate_t1995_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ObjRefSurrogate_t1995_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ObjRefSurrogate_t1995_0_0_0/* byval_arg */
	, &ObjRefSurrogate_t1995_1_0_0/* this_arg */
	, &ObjRefSurrogate_t1995_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ObjRefSurrogate_t1995)/* instance_size */
	, sizeof (ObjRefSurrogate_t1995)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.RemotingSurrogateSelector
#include "mscorlib_System_Runtime_Remoting_Messaging_RemotingSurrogate_0.h"
// Metadata Definition System.Runtime.Remoting.Messaging.RemotingSurrogateSelector
extern TypeInfo RemotingSurrogateSelector_t1997_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.RemotingSurrogateSelector
#include "mscorlib_System_Runtime_Remoting_Messaging_RemotingSurrogate_0MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.RemotingSurrogateSelector::.ctor()
extern const MethodInfo RemotingSurrogateSelector__ctor_m10651_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RemotingSurrogateSelector__ctor_m10651/* method */
	, &RemotingSurrogateSelector_t1997_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3903/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.RemotingSurrogateSelector::.cctor()
extern const MethodInfo RemotingSurrogateSelector__cctor_m10652_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&RemotingSurrogateSelector__cctor_m10652/* method */
	, &RemotingSurrogateSelector_t1997_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3904/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
extern const Il2CppType ISurrogateSelector_t1996_1_0_2;
extern const Il2CppType ISurrogateSelector_t1996_1_0_0;
static const ParameterInfo RemotingSurrogateSelector_t1997_RemotingSurrogateSelector_GetSurrogate_m10653_ParameterInfos[] = 
{
	{"type", 0, 134222400, 0, &Type_t_0_0_0},
	{"context", 1, 134222401, 0, &StreamingContext_t1044_0_0_0},
	{"ssout", 2, 134222402, 0, &ISurrogateSelector_t1996_1_0_2},
};
extern void* RuntimeInvoker_Object_t_Object_t_StreamingContext_t1044_ISurrogateSelectorU26_t2759 (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Serialization.ISerializationSurrogate System.Runtime.Remoting.Messaging.RemotingSurrogateSelector::GetSurrogate(System.Type,System.Runtime.Serialization.StreamingContext,System.Runtime.Serialization.ISurrogateSelector&)
extern const MethodInfo RemotingSurrogateSelector_GetSurrogate_m10653_MethodInfo = 
{
	"GetSurrogate"/* name */
	, (methodPointerType)&RemotingSurrogateSelector_GetSurrogate_m10653/* method */
	, &RemotingSurrogateSelector_t1997_il2cpp_TypeInfo/* declaring_type */
	, &ISerializationSurrogate_t2056_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_StreamingContext_t1044_ISurrogateSelectorU26_t2759/* invoker_method */
	, RemotingSurrogateSelector_t1997_RemotingSurrogateSelector_GetSurrogate_m10653_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3905/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* RemotingSurrogateSelector_t1997_MethodInfos[] =
{
	&RemotingSurrogateSelector__ctor_m10651_MethodInfo,
	&RemotingSurrogateSelector__cctor_m10652_MethodInfo,
	&RemotingSurrogateSelector_GetSurrogate_m10653_MethodInfo,
	NULL
};
extern const MethodInfo RemotingSurrogateSelector_GetSurrogate_m10653_MethodInfo;
static const Il2CppMethodReference RemotingSurrogateSelector_t1997_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&RemotingSurrogateSelector_GetSurrogate_m10653_MethodInfo,
	&RemotingSurrogateSelector_GetSurrogate_m10653_MethodInfo,
};
static bool RemotingSurrogateSelector_t1997_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* RemotingSurrogateSelector_t1997_InterfacesTypeInfos[] = 
{
	&ISurrogateSelector_t1996_0_0_0,
};
static Il2CppInterfaceOffsetPair RemotingSurrogateSelector_t1997_InterfacesOffsets[] = 
{
	{ &ISurrogateSelector_t1996_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RemotingSurrogateSelector_t1997_0_0_0;
extern const Il2CppType RemotingSurrogateSelector_t1997_1_0_0;
struct RemotingSurrogateSelector_t1997;
const Il2CppTypeDefinitionMetadata RemotingSurrogateSelector_t1997_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, RemotingSurrogateSelector_t1997_InterfacesTypeInfos/* implementedInterfaces */
	, RemotingSurrogateSelector_t1997_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, RemotingSurrogateSelector_t1997_VTable/* vtableMethods */
	, RemotingSurrogateSelector_t1997_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2027/* fieldStart */

};
TypeInfo RemotingSurrogateSelector_t1997_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RemotingSurrogateSelector"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, RemotingSurrogateSelector_t1997_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &RemotingSurrogateSelector_t1997_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 587/* custom_attributes_cache */
	, &RemotingSurrogateSelector_t1997_0_0_0/* byval_arg */
	, &RemotingSurrogateSelector_t1997_1_0_0/* this_arg */
	, &RemotingSurrogateSelector_t1997_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RemotingSurrogateSelector_t1997)/* instance_size */
	, sizeof (RemotingSurrogateSelector_t1997)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(RemotingSurrogateSelector_t1997_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.ReturnMessage
#include "mscorlib_System_Runtime_Remoting_Messaging_ReturnMessage.h"
// Metadata Definition System.Runtime.Remoting.Messaging.ReturnMessage
extern TypeInfo ReturnMessage_t1998_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.ReturnMessage
#include "mscorlib_System_Runtime_Remoting_Messaging_ReturnMessageMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType ObjectU5BU5D_t224_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType LogicalCallContext_t1987_0_0_0;
extern const Il2CppType IMethodCallMessage_t2289_0_0_0;
static const ParameterInfo ReturnMessage_t1998_ReturnMessage__ctor_m10654_ParameterInfos[] = 
{
	{"ret", 0, 134222403, 0, &Object_t_0_0_0},
	{"outArgs", 1, 134222404, 0, &ObjectU5BU5D_t224_0_0_0},
	{"outArgsCount", 2, 134222405, 0, &Int32_t253_0_0_0},
	{"callCtx", 3, 134222406, 0, &LogicalCallContext_t1987_0_0_0},
	{"mcm", 4, 134222407, 0, &IMethodCallMessage_t2289_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t_Int32_t253_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.ReturnMessage::.ctor(System.Object,System.Object[],System.Int32,System.Runtime.Remoting.Messaging.LogicalCallContext,System.Runtime.Remoting.Messaging.IMethodCallMessage)
extern const MethodInfo ReturnMessage__ctor_m10654_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ReturnMessage__ctor_m10654/* method */
	, &ReturnMessage_t1998_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t_Int32_t253_Object_t_Object_t/* invoker_method */
	, ReturnMessage_t1998_ReturnMessage__ctor_m10654_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3906/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Exception_t232_0_0_0;
extern const Il2CppType IMethodCallMessage_t2289_0_0_0;
static const ParameterInfo ReturnMessage_t1998_ReturnMessage__ctor_m10655_ParameterInfos[] = 
{
	{"e", 0, 134222408, 0, &Exception_t232_0_0_0},
	{"mcm", 1, 134222409, 0, &IMethodCallMessage_t2289_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.ReturnMessage::.ctor(System.Exception,System.Runtime.Remoting.Messaging.IMethodCallMessage)
extern const MethodInfo ReturnMessage__ctor_m10655_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ReturnMessage__ctor_m10655/* method */
	, &ReturnMessage_t1998_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, ReturnMessage_t1998_ReturnMessage__ctor_m10655_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3907/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo ReturnMessage_t1998_ReturnMessage_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m10656_ParameterInfos[] = 
{
	{"value", 0, 134222410, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.ReturnMessage::System.Runtime.Remoting.Messaging.IInternalMessage.set_Uri(System.String)
extern const MethodInfo ReturnMessage_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m10656_MethodInfo = 
{
	"System.Runtime.Remoting.Messaging.IInternalMessage.set_Uri"/* name */
	, (methodPointerType)&ReturnMessage_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m10656/* method */
	, &ReturnMessage_t1998_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, ReturnMessage_t1998_ReturnMessage_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m10656_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3908/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Runtime.Remoting.Messaging.ReturnMessage::get_ArgCount()
extern const MethodInfo ReturnMessage_get_ArgCount_m10657_MethodInfo = 
{
	"get_ArgCount"/* name */
	, (methodPointerType)&ReturnMessage_get_ArgCount_m10657/* method */
	, &ReturnMessage_t1998_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3909/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Runtime.Remoting.Messaging.ReturnMessage::get_Args()
extern const MethodInfo ReturnMessage_get_Args_m10658_MethodInfo = 
{
	"get_Args"/* name */
	, (methodPointerType)&ReturnMessage_get_Args_m10658/* method */
	, &ReturnMessage_t1998_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t224_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3910/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Messaging.LogicalCallContext System.Runtime.Remoting.Messaging.ReturnMessage::get_LogicalCallContext()
extern const MethodInfo ReturnMessage_get_LogicalCallContext_m10659_MethodInfo = 
{
	"get_LogicalCallContext"/* name */
	, (methodPointerType)&ReturnMessage_get_LogicalCallContext_m10659/* method */
	, &ReturnMessage_t1998_il2cpp_TypeInfo/* declaring_type */
	, &LogicalCallContext_t1987_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3911/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodBase System.Runtime.Remoting.Messaging.ReturnMessage::get_MethodBase()
extern const MethodInfo ReturnMessage_get_MethodBase_m10660_MethodInfo = 
{
	"get_MethodBase"/* name */
	, (methodPointerType)&ReturnMessage_get_MethodBase_m10660/* method */
	, &ReturnMessage_t1998_il2cpp_TypeInfo/* declaring_type */
	, &MethodBase_t1102_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3912/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Messaging.ReturnMessage::get_MethodName()
extern const MethodInfo ReturnMessage_get_MethodName_m10661_MethodInfo = 
{
	"get_MethodName"/* name */
	, (methodPointerType)&ReturnMessage_get_MethodName_m10661/* method */
	, &ReturnMessage_t1998_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3913/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.ReturnMessage::get_MethodSignature()
extern const MethodInfo ReturnMessage_get_MethodSignature_m10662_MethodInfo = 
{
	"get_MethodSignature"/* name */
	, (methodPointerType)&ReturnMessage_get_MethodSignature_m10662/* method */
	, &ReturnMessage_t1998_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3914/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.IDictionary System.Runtime.Remoting.Messaging.ReturnMessage::get_Properties()
extern const MethodInfo ReturnMessage_get_Properties_m10663_MethodInfo = 
{
	"get_Properties"/* name */
	, (methodPointerType)&ReturnMessage_get_Properties_m10663/* method */
	, &ReturnMessage_t1998_il2cpp_TypeInfo/* declaring_type */
	, &IDictionary_t444_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 19/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3915/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Messaging.ReturnMessage::get_TypeName()
extern const MethodInfo ReturnMessage_get_TypeName_m10664_MethodInfo = 
{
	"get_TypeName"/* name */
	, (methodPointerType)&ReturnMessage_get_TypeName_m10664/* method */
	, &ReturnMessage_t1998_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3916/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Messaging.ReturnMessage::get_Uri()
extern const MethodInfo ReturnMessage_get_Uri_m10665_MethodInfo = 
{
	"get_Uri"/* name */
	, (methodPointerType)&ReturnMessage_get_Uri_m10665/* method */
	, &ReturnMessage_t1998_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3917/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo ReturnMessage_t1998_ReturnMessage_set_Uri_m10666_ParameterInfos[] = 
{
	{"value", 0, 134222411, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.ReturnMessage::set_Uri(System.String)
extern const MethodInfo ReturnMessage_set_Uri_m10666_MethodInfo = 
{
	"set_Uri"/* name */
	, (methodPointerType)&ReturnMessage_set_Uri_m10666/* method */
	, &ReturnMessage_t1998_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, ReturnMessage_t1998_ReturnMessage_set_Uri_m10666_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 20/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3918/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo ReturnMessage_t1998_ReturnMessage_GetArg_m10667_ParameterInfos[] = 
{
	{"argNum", 0, 134222412, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.ReturnMessage::GetArg(System.Int32)
extern const MethodInfo ReturnMessage_GetArg_m10667_MethodInfo = 
{
	"GetArg"/* name */
	, (methodPointerType)&ReturnMessage_GetArg_m10667/* method */
	, &ReturnMessage_t1998_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t253/* invoker_method */
	, ReturnMessage_t1998_ReturnMessage_GetArg_m10667_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3919/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Exception System.Runtime.Remoting.Messaging.ReturnMessage::get_Exception()
extern const MethodInfo ReturnMessage_get_Exception_m10668_MethodInfo = 
{
	"get_Exception"/* name */
	, (methodPointerType)&ReturnMessage_get_Exception_m10668/* method */
	, &ReturnMessage_t1998_il2cpp_TypeInfo/* declaring_type */
	, &Exception_t232_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3920/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Runtime.Remoting.Messaging.ReturnMessage::get_OutArgCount()
extern const MethodInfo ReturnMessage_get_OutArgCount_m10669_MethodInfo = 
{
	"get_OutArgCount"/* name */
	, (methodPointerType)&ReturnMessage_get_OutArgCount_m10669/* method */
	, &ReturnMessage_t1998_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3921/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Runtime.Remoting.Messaging.ReturnMessage::get_OutArgs()
extern const MethodInfo ReturnMessage_get_OutArgs_m10670_MethodInfo = 
{
	"get_OutArgs"/* name */
	, (methodPointerType)&ReturnMessage_get_OutArgs_m10670/* method */
	, &ReturnMessage_t1998_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t224_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3922/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.ReturnMessage::get_ReturnValue()
extern const MethodInfo ReturnMessage_get_ReturnValue_m10671_MethodInfo = 
{
	"get_ReturnValue"/* name */
	, (methodPointerType)&ReturnMessage_get_ReturnValue_m10671/* method */
	, &ReturnMessage_t1998_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 21/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3923/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ReturnMessage_t1998_MethodInfos[] =
{
	&ReturnMessage__ctor_m10654_MethodInfo,
	&ReturnMessage__ctor_m10655_MethodInfo,
	&ReturnMessage_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m10656_MethodInfo,
	&ReturnMessage_get_ArgCount_m10657_MethodInfo,
	&ReturnMessage_get_Args_m10658_MethodInfo,
	&ReturnMessage_get_LogicalCallContext_m10659_MethodInfo,
	&ReturnMessage_get_MethodBase_m10660_MethodInfo,
	&ReturnMessage_get_MethodName_m10661_MethodInfo,
	&ReturnMessage_get_MethodSignature_m10662_MethodInfo,
	&ReturnMessage_get_Properties_m10663_MethodInfo,
	&ReturnMessage_get_TypeName_m10664_MethodInfo,
	&ReturnMessage_get_Uri_m10665_MethodInfo,
	&ReturnMessage_set_Uri_m10666_MethodInfo,
	&ReturnMessage_GetArg_m10667_MethodInfo,
	&ReturnMessage_get_Exception_m10668_MethodInfo,
	&ReturnMessage_get_OutArgCount_m10669_MethodInfo,
	&ReturnMessage_get_OutArgs_m10670_MethodInfo,
	&ReturnMessage_get_ReturnValue_m10671_MethodInfo,
	NULL
};
extern const MethodInfo ReturnMessage_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m10656_MethodInfo;
static const PropertyInfo ReturnMessage_t1998____System_Runtime_Remoting_Messaging_IInternalMessage_Uri_PropertyInfo = 
{
	&ReturnMessage_t1998_il2cpp_TypeInfo/* parent */
	, "System.Runtime.Remoting.Messaging.IInternalMessage.Uri"/* name */
	, NULL/* get */
	, &ReturnMessage_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m10656_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ReturnMessage_get_ArgCount_m10657_MethodInfo;
static const PropertyInfo ReturnMessage_t1998____ArgCount_PropertyInfo = 
{
	&ReturnMessage_t1998_il2cpp_TypeInfo/* parent */
	, "ArgCount"/* name */
	, &ReturnMessage_get_ArgCount_m10657_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ReturnMessage_get_Args_m10658_MethodInfo;
static const PropertyInfo ReturnMessage_t1998____Args_PropertyInfo = 
{
	&ReturnMessage_t1998_il2cpp_TypeInfo/* parent */
	, "Args"/* name */
	, &ReturnMessage_get_Args_m10658_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ReturnMessage_get_LogicalCallContext_m10659_MethodInfo;
static const PropertyInfo ReturnMessage_t1998____LogicalCallContext_PropertyInfo = 
{
	&ReturnMessage_t1998_il2cpp_TypeInfo/* parent */
	, "LogicalCallContext"/* name */
	, &ReturnMessage_get_LogicalCallContext_m10659_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ReturnMessage_get_MethodBase_m10660_MethodInfo;
static const PropertyInfo ReturnMessage_t1998____MethodBase_PropertyInfo = 
{
	&ReturnMessage_t1998_il2cpp_TypeInfo/* parent */
	, "MethodBase"/* name */
	, &ReturnMessage_get_MethodBase_m10660_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ReturnMessage_get_MethodName_m10661_MethodInfo;
static const PropertyInfo ReturnMessage_t1998____MethodName_PropertyInfo = 
{
	&ReturnMessage_t1998_il2cpp_TypeInfo/* parent */
	, "MethodName"/* name */
	, &ReturnMessage_get_MethodName_m10661_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ReturnMessage_get_MethodSignature_m10662_MethodInfo;
static const PropertyInfo ReturnMessage_t1998____MethodSignature_PropertyInfo = 
{
	&ReturnMessage_t1998_il2cpp_TypeInfo/* parent */
	, "MethodSignature"/* name */
	, &ReturnMessage_get_MethodSignature_m10662_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ReturnMessage_get_Properties_m10663_MethodInfo;
static const PropertyInfo ReturnMessage_t1998____Properties_PropertyInfo = 
{
	&ReturnMessage_t1998_il2cpp_TypeInfo/* parent */
	, "Properties"/* name */
	, &ReturnMessage_get_Properties_m10663_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ReturnMessage_get_TypeName_m10664_MethodInfo;
static const PropertyInfo ReturnMessage_t1998____TypeName_PropertyInfo = 
{
	&ReturnMessage_t1998_il2cpp_TypeInfo/* parent */
	, "TypeName"/* name */
	, &ReturnMessage_get_TypeName_m10664_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ReturnMessage_get_Uri_m10665_MethodInfo;
extern const MethodInfo ReturnMessage_set_Uri_m10666_MethodInfo;
static const PropertyInfo ReturnMessage_t1998____Uri_PropertyInfo = 
{
	&ReturnMessage_t1998_il2cpp_TypeInfo/* parent */
	, "Uri"/* name */
	, &ReturnMessage_get_Uri_m10665_MethodInfo/* get */
	, &ReturnMessage_set_Uri_m10666_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ReturnMessage_get_Exception_m10668_MethodInfo;
static const PropertyInfo ReturnMessage_t1998____Exception_PropertyInfo = 
{
	&ReturnMessage_t1998_il2cpp_TypeInfo/* parent */
	, "Exception"/* name */
	, &ReturnMessage_get_Exception_m10668_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ReturnMessage_get_OutArgCount_m10669_MethodInfo;
static const PropertyInfo ReturnMessage_t1998____OutArgCount_PropertyInfo = 
{
	&ReturnMessage_t1998_il2cpp_TypeInfo/* parent */
	, "OutArgCount"/* name */
	, &ReturnMessage_get_OutArgCount_m10669_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ReturnMessage_get_OutArgs_m10670_MethodInfo;
static const PropertyInfo ReturnMessage_t1998____OutArgs_PropertyInfo = 
{
	&ReturnMessage_t1998_il2cpp_TypeInfo/* parent */
	, "OutArgs"/* name */
	, &ReturnMessage_get_OutArgs_m10670_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ReturnMessage_get_ReturnValue_m10671_MethodInfo;
static const PropertyInfo ReturnMessage_t1998____ReturnValue_PropertyInfo = 
{
	&ReturnMessage_t1998_il2cpp_TypeInfo/* parent */
	, "ReturnValue"/* name */
	, &ReturnMessage_get_ReturnValue_m10671_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ReturnMessage_t1998_PropertyInfos[] =
{
	&ReturnMessage_t1998____System_Runtime_Remoting_Messaging_IInternalMessage_Uri_PropertyInfo,
	&ReturnMessage_t1998____ArgCount_PropertyInfo,
	&ReturnMessage_t1998____Args_PropertyInfo,
	&ReturnMessage_t1998____LogicalCallContext_PropertyInfo,
	&ReturnMessage_t1998____MethodBase_PropertyInfo,
	&ReturnMessage_t1998____MethodName_PropertyInfo,
	&ReturnMessage_t1998____MethodSignature_PropertyInfo,
	&ReturnMessage_t1998____Properties_PropertyInfo,
	&ReturnMessage_t1998____TypeName_PropertyInfo,
	&ReturnMessage_t1998____Uri_PropertyInfo,
	&ReturnMessage_t1998____Exception_PropertyInfo,
	&ReturnMessage_t1998____OutArgCount_PropertyInfo,
	&ReturnMessage_t1998____OutArgs_PropertyInfo,
	&ReturnMessage_t1998____ReturnValue_PropertyInfo,
	NULL
};
extern const MethodInfo ReturnMessage_GetArg_m10667_MethodInfo;
static const Il2CppMethodReference ReturnMessage_t1998_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&ReturnMessage_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m10656_MethodInfo,
	&ReturnMessage_get_Properties_m10663_MethodInfo,
	&ReturnMessage_get_ArgCount_m10657_MethodInfo,
	&ReturnMessage_get_Args_m10658_MethodInfo,
	&ReturnMessage_get_LogicalCallContext_m10659_MethodInfo,
	&ReturnMessage_get_MethodBase_m10660_MethodInfo,
	&ReturnMessage_get_MethodName_m10661_MethodInfo,
	&ReturnMessage_get_MethodSignature_m10662_MethodInfo,
	&ReturnMessage_get_TypeName_m10664_MethodInfo,
	&ReturnMessage_get_Uri_m10665_MethodInfo,
	&ReturnMessage_GetArg_m10667_MethodInfo,
	&ReturnMessage_get_Exception_m10668_MethodInfo,
	&ReturnMessage_get_OutArgCount_m10669_MethodInfo,
	&ReturnMessage_get_OutArgs_m10670_MethodInfo,
	&ReturnMessage_get_ReturnValue_m10671_MethodInfo,
	&ReturnMessage_get_Properties_m10663_MethodInfo,
	&ReturnMessage_set_Uri_m10666_MethodInfo,
	&ReturnMessage_get_ReturnValue_m10671_MethodInfo,
};
static bool ReturnMessage_t1998_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* ReturnMessage_t1998_InterfacesTypeInfos[] = 
{
	&IInternalMessage_t2303_0_0_0,
	&IMessage_t1978_0_0_0,
	&IMethodMessage_t1990_0_0_0,
	&IMethodReturnMessage_t2288_0_0_0,
};
static Il2CppInterfaceOffsetPair ReturnMessage_t1998_InterfacesOffsets[] = 
{
	{ &IInternalMessage_t2303_0_0_0, 4},
	{ &IMessage_t1978_0_0_0, 5},
	{ &IMethodMessage_t1990_0_0_0, 6},
	{ &IMethodReturnMessage_t2288_0_0_0, 15},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ReturnMessage_t1998_0_0_0;
extern const Il2CppType ReturnMessage_t1998_1_0_0;
struct ReturnMessage_t1998;
const Il2CppTypeDefinitionMetadata ReturnMessage_t1998_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ReturnMessage_t1998_InterfacesTypeInfos/* implementedInterfaces */
	, ReturnMessage_t1998_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ReturnMessage_t1998_VTable/* vtableMethods */
	, ReturnMessage_t1998_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2031/* fieldStart */

};
TypeInfo ReturnMessage_t1998_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ReturnMessage"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, ReturnMessage_t1998_MethodInfos/* methods */
	, ReturnMessage_t1998_PropertyInfos/* properties */
	, NULL/* events */
	, &ReturnMessage_t1998_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 588/* custom_attributes_cache */
	, &ReturnMessage_t1998_0_0_0/* byval_arg */
	, &ReturnMessage_t1998_1_0_0/* this_arg */
	, &ReturnMessage_t1998_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ReturnMessage_t1998)/* instance_size */
	, sizeof (ReturnMessage_t1998)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 18/* method_count */
	, 14/* property_count */
	, 13/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 22/* vtable_count */
	, 4/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.Runtime.Remoting.Proxies.ProxyAttribute
#include "mscorlib_System_Runtime_Remoting_Proxies_ProxyAttribute.h"
// Metadata Definition System.Runtime.Remoting.Proxies.ProxyAttribute
extern TypeInfo ProxyAttribute_t1999_il2cpp_TypeInfo;
// System.Runtime.Remoting.Proxies.ProxyAttribute
#include "mscorlib_System_Runtime_Remoting_Proxies_ProxyAttributeMethodDeclarations.h"
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo ProxyAttribute_t1999_ProxyAttribute_CreateInstance_m10672_ParameterInfos[] = 
{
	{"serverType", 0, 134222413, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.MarshalByRefObject System.Runtime.Remoting.Proxies.ProxyAttribute::CreateInstance(System.Type)
extern const MethodInfo ProxyAttribute_CreateInstance_m10672_MethodInfo = 
{
	"CreateInstance"/* name */
	, (methodPointerType)&ProxyAttribute_CreateInstance_m10672/* method */
	, &ProxyAttribute_t1999_il2cpp_TypeInfo/* declaring_type */
	, &MarshalByRefObject_t1308_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ProxyAttribute_t1999_ProxyAttribute_CreateInstance_m10672_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3924/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjRef_t2008_0_0_0;
extern const Il2CppType ObjRef_t2008_0_0_0;
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Context_t1963_0_0_0;
static const ParameterInfo ProxyAttribute_t1999_ProxyAttribute_CreateProxy_m10673_ParameterInfos[] = 
{
	{"objRef", 0, 134222414, 0, &ObjRef_t2008_0_0_0},
	{"serverType", 1, 134222415, 0, &Type_t_0_0_0},
	{"serverObject", 2, 134222416, 0, &Object_t_0_0_0},
	{"serverContext", 3, 134222417, 0, &Context_t1963_0_0_0},
};
extern const Il2CppType RealProxy_t2000_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Proxies.RealProxy System.Runtime.Remoting.Proxies.ProxyAttribute::CreateProxy(System.Runtime.Remoting.ObjRef,System.Type,System.Object,System.Runtime.Remoting.Contexts.Context)
extern const MethodInfo ProxyAttribute_CreateProxy_m10673_MethodInfo = 
{
	"CreateProxy"/* name */
	, (methodPointerType)&ProxyAttribute_CreateProxy_m10673/* method */
	, &ProxyAttribute_t1999_il2cpp_TypeInfo/* declaring_type */
	, &RealProxy_t2000_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, ProxyAttribute_t1999_ProxyAttribute_CreateProxy_m10673_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3925/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IConstructionCallMessage_t2284_0_0_0;
static const ParameterInfo ProxyAttribute_t1999_ProxyAttribute_GetPropertiesForNewContext_m10674_ParameterInfos[] = 
{
	{"msg", 0, 134222418, 0, &IConstructionCallMessage_t2284_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Proxies.ProxyAttribute::GetPropertiesForNewContext(System.Runtime.Remoting.Activation.IConstructionCallMessage)
extern const MethodInfo ProxyAttribute_GetPropertiesForNewContext_m10674_MethodInfo = 
{
	"GetPropertiesForNewContext"/* name */
	, (methodPointerType)&ProxyAttribute_GetPropertiesForNewContext_m10674/* method */
	, &ProxyAttribute_t1999_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, ProxyAttribute_t1999_ProxyAttribute_GetPropertiesForNewContext_m10674_ParameterInfos/* parameters */
	, 590/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3926/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Context_t1963_0_0_0;
extern const Il2CppType IConstructionCallMessage_t2284_0_0_0;
static const ParameterInfo ProxyAttribute_t1999_ProxyAttribute_IsContextOK_m10675_ParameterInfos[] = 
{
	{"ctx", 0, 134222419, 0, &Context_t1963_0_0_0},
	{"msg", 1, 134222420, 0, &IConstructionCallMessage_t2284_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.Proxies.ProxyAttribute::IsContextOK(System.Runtime.Remoting.Contexts.Context,System.Runtime.Remoting.Activation.IConstructionCallMessage)
extern const MethodInfo ProxyAttribute_IsContextOK_m10675_MethodInfo = 
{
	"IsContextOK"/* name */
	, (methodPointerType)&ProxyAttribute_IsContextOK_m10675/* method */
	, &ProxyAttribute_t1999_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t_Object_t/* invoker_method */
	, ProxyAttribute_t1999_ProxyAttribute_IsContextOK_m10675_ParameterInfos/* parameters */
	, 591/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3927/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ProxyAttribute_t1999_MethodInfos[] =
{
	&ProxyAttribute_CreateInstance_m10672_MethodInfo,
	&ProxyAttribute_CreateProxy_m10673_MethodInfo,
	&ProxyAttribute_GetPropertiesForNewContext_m10674_MethodInfo,
	&ProxyAttribute_IsContextOK_m10675_MethodInfo,
	NULL
};
extern const MethodInfo ProxyAttribute_GetPropertiesForNewContext_m10674_MethodInfo;
extern const MethodInfo ProxyAttribute_IsContextOK_m10675_MethodInfo;
extern const MethodInfo ProxyAttribute_CreateInstance_m10672_MethodInfo;
extern const MethodInfo ProxyAttribute_CreateProxy_m10673_MethodInfo;
static const Il2CppMethodReference ProxyAttribute_t1999_VTable[] =
{
	&Attribute_Equals_m5384_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Attribute_GetHashCode_m5253_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&ProxyAttribute_GetPropertiesForNewContext_m10674_MethodInfo,
	&ProxyAttribute_IsContextOK_m10675_MethodInfo,
	&ProxyAttribute_CreateInstance_m10672_MethodInfo,
	&ProxyAttribute_CreateProxy_m10673_MethodInfo,
};
static bool ProxyAttribute_t1999_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* ProxyAttribute_t1999_InterfacesTypeInfos[] = 
{
	&IContextAttribute_t2299_0_0_0,
};
static Il2CppInterfaceOffsetPair ProxyAttribute_t1999_InterfacesOffsets[] = 
{
	{ &_Attribute_t1145_0_0_0, 4},
	{ &IContextAttribute_t2299_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ProxyAttribute_t1999_0_0_0;
extern const Il2CppType ProxyAttribute_t1999_1_0_0;
struct ProxyAttribute_t1999;
const Il2CppTypeDefinitionMetadata ProxyAttribute_t1999_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ProxyAttribute_t1999_InterfacesTypeInfos/* implementedInterfaces */
	, ProxyAttribute_t1999_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t805_0_0_0/* parent */
	, ProxyAttribute_t1999_VTable/* vtableMethods */
	, ProxyAttribute_t1999_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ProxyAttribute_t1999_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ProxyAttribute"/* name */
	, "System.Runtime.Remoting.Proxies"/* namespaze */
	, ProxyAttribute_t1999_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ProxyAttribute_t1999_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 589/* custom_attributes_cache */
	, &ProxyAttribute_t1999_0_0_0/* byval_arg */
	, &ProxyAttribute_t1999_1_0_0/* this_arg */
	, &ProxyAttribute_t1999_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ProxyAttribute_t1999)/* instance_size */
	, sizeof (ProxyAttribute_t1999)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 1/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Runtime.Remoting.Proxies.TransparentProxy
#include "mscorlib_System_Runtime_Remoting_Proxies_TransparentProxy.h"
// Metadata Definition System.Runtime.Remoting.Proxies.TransparentProxy
extern TypeInfo TransparentProxy_t2001_il2cpp_TypeInfo;
// System.Runtime.Remoting.Proxies.TransparentProxy
#include "mscorlib_System_Runtime_Remoting_Proxies_TransparentProxyMethodDeclarations.h"
static const MethodInfo* TransparentProxy_t2001_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference TransparentProxy_t2001_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool TransparentProxy_t2001_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TransparentProxy_t2001_0_0_0;
extern const Il2CppType TransparentProxy_t2001_1_0_0;
struct TransparentProxy_t2001;
const Il2CppTypeDefinitionMetadata TransparentProxy_t2001_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TransparentProxy_t2001_VTable/* vtableMethods */
	, TransparentProxy_t2001_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2044/* fieldStart */

};
TypeInfo TransparentProxy_t2001_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TransparentProxy"/* name */
	, "System.Runtime.Remoting.Proxies"/* namespaze */
	, TransparentProxy_t2001_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TransparentProxy_t2001_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TransparentProxy_t2001_0_0_0/* byval_arg */
	, &TransparentProxy_t2001_1_0_0/* this_arg */
	, &TransparentProxy_t2001_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TransparentProxy_t2001)/* instance_size */
	, sizeof (TransparentProxy_t2001)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Proxies.RealProxy
#include "mscorlib_System_Runtime_Remoting_Proxies_RealProxy.h"
// Metadata Definition System.Runtime.Remoting.Proxies.RealProxy
extern TypeInfo RealProxy_t2000_il2cpp_TypeInfo;
// System.Runtime.Remoting.Proxies.RealProxy
#include "mscorlib_System_Runtime_Remoting_Proxies_RealProxyMethodDeclarations.h"
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo RealProxy_t2000_RealProxy__ctor_m10676_ParameterInfos[] = 
{
	{"classToProxy", 0, 134222421, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Proxies.RealProxy::.ctor(System.Type)
extern const MethodInfo RealProxy__ctor_m10676_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RealProxy__ctor_m10676/* method */
	, &RealProxy_t2000_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, RealProxy_t2000_RealProxy__ctor_m10676_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3928/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType ClientIdentity_t2010_0_0_0;
extern const Il2CppType ClientIdentity_t2010_0_0_0;
static const ParameterInfo RealProxy_t2000_RealProxy__ctor_m10677_ParameterInfos[] = 
{
	{"classToProxy", 0, 134222422, 0, &Type_t_0_0_0},
	{"identity", 1, 134222423, 0, &ClientIdentity_t2010_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Proxies.RealProxy::.ctor(System.Type,System.Runtime.Remoting.ClientIdentity)
extern const MethodInfo RealProxy__ctor_m10677_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RealProxy__ctor_m10677/* method */
	, &RealProxy_t2000_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, RealProxy_t2000_RealProxy__ctor_m10677_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3929/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo RealProxy_t2000_RealProxy__ctor_m10678_ParameterInfos[] = 
{
	{"classToProxy", 0, 134222424, 0, &Type_t_0_0_0},
	{"stub", 1, 134222425, 0, &IntPtr_t_0_0_0},
	{"stubData", 2, 134222426, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_IntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Proxies.RealProxy::.ctor(System.Type,System.IntPtr,System.Object)
extern const MethodInfo RealProxy__ctor_m10678_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RealProxy__ctor_m10678/* method */
	, &RealProxy_t2000_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_IntPtr_t_Object_t/* invoker_method */
	, RealProxy_t2000_RealProxy__ctor_m10678_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3930/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo RealProxy_t2000_RealProxy_InternalGetProxyType_m10679_ParameterInfos[] = 
{
	{"transparentProxy", 0, 134222427, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Runtime.Remoting.Proxies.RealProxy::InternalGetProxyType(System.Object)
extern const MethodInfo RealProxy_InternalGetProxyType_m10679_MethodInfo = 
{
	"InternalGetProxyType"/* name */
	, (methodPointerType)&RealProxy_InternalGetProxyType_m10679/* method */
	, &RealProxy_t2000_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, RealProxy_t2000_RealProxy_InternalGetProxyType_m10679_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3931/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Runtime.Remoting.Proxies.RealProxy::GetProxiedType()
extern const MethodInfo RealProxy_GetProxiedType_m10680_MethodInfo = 
{
	"GetProxiedType"/* name */
	, (methodPointerType)&RealProxy_GetProxiedType_m10680/* method */
	, &RealProxy_t2000_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3932/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1043_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
static const ParameterInfo RealProxy_t2000_RealProxy_GetObjectData_m10681_ParameterInfos[] = 
{
	{"info", 0, 134222428, 0, &SerializationInfo_t1043_0_0_0},
	{"context", 1, 134222429, 0, &StreamingContext_t1044_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Proxies.RealProxy::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo RealProxy_GetObjectData_m10681_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&RealProxy_GetObjectData_m10681/* method */
	, &RealProxy_t2000_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044/* invoker_method */
	, RealProxy_t2000_RealProxy_GetObjectData_m10681_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3933/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Identity_t2002_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Identity System.Runtime.Remoting.Proxies.RealProxy::get_ObjectIdentity()
extern const MethodInfo RealProxy_get_ObjectIdentity_m10682_MethodInfo = 
{
	"get_ObjectIdentity"/* name */
	, (methodPointerType)&RealProxy_get_ObjectIdentity_m10682/* method */
	, &RealProxy_t2000_il2cpp_TypeInfo/* declaring_type */
	, &Identity_t2002_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2179/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3934/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Identity_t2002_0_0_0;
static const ParameterInfo RealProxy_t2000_RealProxy_set_ObjectIdentity_m10683_ParameterInfos[] = 
{
	{"value", 0, 134222430, 0, &Identity_t2002_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Proxies.RealProxy::set_ObjectIdentity(System.Runtime.Remoting.Identity)
extern const MethodInfo RealProxy_set_ObjectIdentity_m10683_MethodInfo = 
{
	"set_ObjectIdentity"/* name */
	, (methodPointerType)&RealProxy_set_ObjectIdentity_m10683/* method */
	, &RealProxy_t2000_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, RealProxy_t2000_RealProxy_set_ObjectIdentity_m10683_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2179/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3935/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo RealProxy_t2000_RealProxy_InternalGetTransparentProxy_m10684_ParameterInfos[] = 
{
	{"className", 0, 134222431, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Proxies.RealProxy::InternalGetTransparentProxy(System.String)
extern const MethodInfo RealProxy_InternalGetTransparentProxy_m10684_MethodInfo = 
{
	"InternalGetTransparentProxy"/* name */
	, (methodPointerType)&RealProxy_InternalGetTransparentProxy_m10684/* method */
	, &RealProxy_t2000_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, RealProxy_t2000_RealProxy_InternalGetTransparentProxy_m10684_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 451/* flags */
	, 4096/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3936/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Proxies.RealProxy::GetTransparentProxy()
extern const MethodInfo RealProxy_GetTransparentProxy_m10685_MethodInfo = 
{
	"GetTransparentProxy"/* name */
	, (methodPointerType)&RealProxy_GetTransparentProxy_m10685/* method */
	, &RealProxy_t2000_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3937/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo RealProxy_t2000_RealProxy_SetTargetDomain_m10686_ParameterInfos[] = 
{
	{"domainId", 0, 134222432, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Proxies.RealProxy::SetTargetDomain(System.Int32)
extern const MethodInfo RealProxy_SetTargetDomain_m10686_MethodInfo = 
{
	"SetTargetDomain"/* name */
	, (methodPointerType)&RealProxy_SetTargetDomain_m10686/* method */
	, &RealProxy_t2000_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, RealProxy_t2000_RealProxy_SetTargetDomain_m10686_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3938/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* RealProxy_t2000_MethodInfos[] =
{
	&RealProxy__ctor_m10676_MethodInfo,
	&RealProxy__ctor_m10677_MethodInfo,
	&RealProxy__ctor_m10678_MethodInfo,
	&RealProxy_InternalGetProxyType_m10679_MethodInfo,
	&RealProxy_GetProxiedType_m10680_MethodInfo,
	&RealProxy_GetObjectData_m10681_MethodInfo,
	&RealProxy_get_ObjectIdentity_m10682_MethodInfo,
	&RealProxy_set_ObjectIdentity_m10683_MethodInfo,
	&RealProxy_InternalGetTransparentProxy_m10684_MethodInfo,
	&RealProxy_GetTransparentProxy_m10685_MethodInfo,
	&RealProxy_SetTargetDomain_m10686_MethodInfo,
	NULL
};
extern const MethodInfo RealProxy_get_ObjectIdentity_m10682_MethodInfo;
extern const MethodInfo RealProxy_set_ObjectIdentity_m10683_MethodInfo;
static const PropertyInfo RealProxy_t2000____ObjectIdentity_PropertyInfo = 
{
	&RealProxy_t2000_il2cpp_TypeInfo/* parent */
	, "ObjectIdentity"/* name */
	, &RealProxy_get_ObjectIdentity_m10682_MethodInfo/* get */
	, &RealProxy_set_ObjectIdentity_m10683_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* RealProxy_t2000_PropertyInfos[] =
{
	&RealProxy_t2000____ObjectIdentity_PropertyInfo,
	NULL
};
extern const MethodInfo RealProxy_GetObjectData_m10681_MethodInfo;
extern const MethodInfo RealProxy_InternalGetTransparentProxy_m10684_MethodInfo;
extern const MethodInfo RealProxy_GetTransparentProxy_m10685_MethodInfo;
static const Il2CppMethodReference RealProxy_t2000_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&RealProxy_GetObjectData_m10681_MethodInfo,
	&RealProxy_InternalGetTransparentProxy_m10684_MethodInfo,
	&RealProxy_GetTransparentProxy_m10685_MethodInfo,
};
static bool RealProxy_t2000_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RealProxy_t2000_1_0_0;
struct RealProxy_t2000;
const Il2CppTypeDefinitionMetadata RealProxy_t2000_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, RealProxy_t2000_VTable/* vtableMethods */
	, RealProxy_t2000_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2045/* fieldStart */

};
TypeInfo RealProxy_t2000_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RealProxy"/* name */
	, "System.Runtime.Remoting.Proxies"/* namespaze */
	, RealProxy_t2000_MethodInfos/* methods */
	, RealProxy_t2000_PropertyInfos/* properties */
	, NULL/* events */
	, &RealProxy_t2000_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 592/* custom_attributes_cache */
	, &RealProxy_t2000_0_0_0/* byval_arg */
	, &RealProxy_t2000_1_0_0/* this_arg */
	, &RealProxy_t2000_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RealProxy_t2000)/* instance_size */
	, sizeof (RealProxy_t2000)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 1/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Proxies.RemotingProxy
#include "mscorlib_System_Runtime_Remoting_Proxies_RemotingProxy.h"
// Metadata Definition System.Runtime.Remoting.Proxies.RemotingProxy
extern TypeInfo RemotingProxy_t2003_il2cpp_TypeInfo;
// System.Runtime.Remoting.Proxies.RemotingProxy
#include "mscorlib_System_Runtime_Remoting_Proxies_RemotingProxyMethodDeclarations.h"
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType ClientIdentity_t2010_0_0_0;
static const ParameterInfo RemotingProxy_t2003_RemotingProxy__ctor_m10687_ParameterInfos[] = 
{
	{"type", 0, 134222433, 0, &Type_t_0_0_0},
	{"identity", 1, 134222434, 0, &ClientIdentity_t2010_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Proxies.RemotingProxy::.ctor(System.Type,System.Runtime.Remoting.ClientIdentity)
extern const MethodInfo RemotingProxy__ctor_m10687_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RemotingProxy__ctor_m10687/* method */
	, &RemotingProxy_t2003_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, RemotingProxy_t2003_RemotingProxy__ctor_m10687_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3939/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType ObjectU5BU5D_t224_0_0_0;
static const ParameterInfo RemotingProxy_t2003_RemotingProxy__ctor_m10688_ParameterInfos[] = 
{
	{"type", 0, 134222435, 0, &Type_t_0_0_0},
	{"activationUrl", 1, 134222436, 0, &String_t_0_0_0},
	{"activationAttributes", 2, 134222437, 0, &ObjectU5BU5D_t224_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Proxies.RemotingProxy::.ctor(System.Type,System.String,System.Object[])
extern const MethodInfo RemotingProxy__ctor_m10688_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RemotingProxy__ctor_m10688/* method */
	, &RemotingProxy_t2003_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t/* invoker_method */
	, RemotingProxy_t2003_RemotingProxy__ctor_m10688_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3940/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Proxies.RemotingProxy::.cctor()
extern const MethodInfo RemotingProxy__cctor_m10689_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&RemotingProxy__cctor_m10689/* method */
	, &RemotingProxy_t2003_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3941/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Proxies.RemotingProxy::get_TypeName()
extern const MethodInfo RemotingProxy_get_TypeName_m10690_MethodInfo = 
{
	"get_TypeName"/* name */
	, (methodPointerType)&RemotingProxy_get_TypeName_m10690/* method */
	, &RemotingProxy_t2003_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3942/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Proxies.RemotingProxy::Finalize()
extern const MethodInfo RemotingProxy_Finalize_m10691_MethodInfo = 
{
	"Finalize"/* name */
	, (methodPointerType)&RemotingProxy_Finalize_m10691/* method */
	, &RemotingProxy_t2003_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3943/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* RemotingProxy_t2003_MethodInfos[] =
{
	&RemotingProxy__ctor_m10687_MethodInfo,
	&RemotingProxy__ctor_m10688_MethodInfo,
	&RemotingProxy__cctor_m10689_MethodInfo,
	&RemotingProxy_get_TypeName_m10690_MethodInfo,
	&RemotingProxy_Finalize_m10691_MethodInfo,
	NULL
};
extern const MethodInfo RemotingProxy_get_TypeName_m10690_MethodInfo;
static const PropertyInfo RemotingProxy_t2003____TypeName_PropertyInfo = 
{
	&RemotingProxy_t2003_il2cpp_TypeInfo/* parent */
	, "TypeName"/* name */
	, &RemotingProxy_get_TypeName_m10690_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* RemotingProxy_t2003_PropertyInfos[] =
{
	&RemotingProxy_t2003____TypeName_PropertyInfo,
	NULL
};
extern const MethodInfo RemotingProxy_Finalize_m10691_MethodInfo;
static const Il2CppMethodReference RemotingProxy_t2003_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&RemotingProxy_Finalize_m10691_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&RealProxy_GetObjectData_m10681_MethodInfo,
	&RealProxy_InternalGetTransparentProxy_m10684_MethodInfo,
	&RealProxy_GetTransparentProxy_m10685_MethodInfo,
	&RemotingProxy_get_TypeName_m10690_MethodInfo,
};
static bool RemotingProxy_t2003_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IRemotingTypeInfo_t2012_0_0_0;
static const Il2CppType* RemotingProxy_t2003_InterfacesTypeInfos[] = 
{
	&IRemotingTypeInfo_t2012_0_0_0,
};
static Il2CppInterfaceOffsetPair RemotingProxy_t2003_InterfacesOffsets[] = 
{
	{ &IRemotingTypeInfo_t2012_0_0_0, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RemotingProxy_t2003_0_0_0;
extern const Il2CppType RemotingProxy_t2003_1_0_0;
struct RemotingProxy_t2003;
const Il2CppTypeDefinitionMetadata RemotingProxy_t2003_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, RemotingProxy_t2003_InterfacesTypeInfos/* implementedInterfaces */
	, RemotingProxy_t2003_InterfacesOffsets/* interfaceOffsets */
	, &RealProxy_t2000_0_0_0/* parent */
	, RemotingProxy_t2003_VTable/* vtableMethods */
	, RemotingProxy_t2003_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2050/* fieldStart */

};
TypeInfo RemotingProxy_t2003_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RemotingProxy"/* name */
	, "System.Runtime.Remoting.Proxies"/* namespaze */
	, RemotingProxy_t2003_MethodInfos/* methods */
	, RemotingProxy_t2003_PropertyInfos/* properties */
	, NULL/* events */
	, &RemotingProxy_t2003_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RemotingProxy_t2003_0_0_0/* byval_arg */
	, &RemotingProxy_t2003_1_0_0/* this_arg */
	, &RemotingProxy_t2003_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RemotingProxy_t2003)/* instance_size */
	, sizeof (RemotingProxy_t2003)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(RemotingProxy_t2003_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
