﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Serialization.Formatters.Binary.MessageFormatter
struct MessageFormatter_t2029;
// System.IO.BinaryWriter
struct BinaryWriter_t1806;
// System.Object
struct Object_t;
// System.Runtime.Remoting.Messaging.Header[]
struct HeaderU5BU5D_t2251;
// System.Runtime.Serialization.ISurrogateSelector
struct ISurrogateSelector_t1996;
// System.IO.BinaryReader
struct BinaryReader_t1805;
// System.Runtime.Remoting.Messaging.HeaderHandler
struct HeaderHandler_t2252;
// System.Runtime.Serialization.Formatters.Binary.BinaryFormatter
struct BinaryFormatter_t424;
// System.Runtime.Remoting.Messaging.IMethodCallMessage
struct IMethodCallMessage_t2289;
// System.Object[]
struct ObjectU5BU5D_t224;
// System.Type
struct Type_t;
// System.Collections.IDictionary
struct IDictionary_t444;
// System.String[]
struct StringU5BU5D_t243;
// System.String
struct String_t;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Runtime.Serialization.Formatters.FormatterAssemblyStyle
#include "mscorlib_System_Runtime_Serialization_Formatters_FormatterAs.h"
// System.Runtime.Serialization.Formatters.FormatterTypeStyle
#include "mscorlib_System_Runtime_Serialization_Formatters_FormatterTy.h"
// System.Runtime.Serialization.Formatters.Binary.BinaryElement
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Bina_1.h"

// System.Void System.Runtime.Serialization.Formatters.Binary.MessageFormatter::WriteMethodCall(System.IO.BinaryWriter,System.Object,System.Runtime.Remoting.Messaging.Header[],System.Runtime.Serialization.ISurrogateSelector,System.Runtime.Serialization.StreamingContext,System.Runtime.Serialization.Formatters.FormatterAssemblyStyle,System.Runtime.Serialization.Formatters.FormatterTypeStyle)
extern "C" void MessageFormatter_WriteMethodCall_m10811 (Object_t * __this /* static, unused */, BinaryWriter_t1806 * ___writer, Object_t * ___obj, HeaderU5BU5D_t2251* ___headers, Object_t * ___surrogateSelector, StreamingContext_t1044  ___context, int32_t ___assemblyFormat, int32_t ___typeFormat, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.Formatters.Binary.MessageFormatter::WriteMethodResponse(System.IO.BinaryWriter,System.Object,System.Runtime.Remoting.Messaging.Header[],System.Runtime.Serialization.ISurrogateSelector,System.Runtime.Serialization.StreamingContext,System.Runtime.Serialization.Formatters.FormatterAssemblyStyle,System.Runtime.Serialization.Formatters.FormatterTypeStyle)
extern "C" void MessageFormatter_WriteMethodResponse_m10812 (Object_t * __this /* static, unused */, BinaryWriter_t1806 * ___writer, Object_t * ___obj, HeaderU5BU5D_t2251* ___headers, Object_t * ___surrogateSelector, StreamingContext_t1044  ___context, int32_t ___assemblyFormat, int32_t ___typeFormat, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Serialization.Formatters.Binary.MessageFormatter::ReadMethodCall(System.Runtime.Serialization.Formatters.Binary.BinaryElement,System.IO.BinaryReader,System.Boolean,System.Runtime.Remoting.Messaging.HeaderHandler,System.Runtime.Serialization.Formatters.Binary.BinaryFormatter)
extern "C" Object_t * MessageFormatter_ReadMethodCall_m10813 (Object_t * __this /* static, unused */, uint8_t ___elem, BinaryReader_t1805 * ___reader, bool ___hasHeaders, HeaderHandler_t2252 * ___headerHandler, BinaryFormatter_t424 * ___formatter, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Serialization.Formatters.Binary.MessageFormatter::ReadMethodResponse(System.Runtime.Serialization.Formatters.Binary.BinaryElement,System.IO.BinaryReader,System.Boolean,System.Runtime.Remoting.Messaging.HeaderHandler,System.Runtime.Remoting.Messaging.IMethodCallMessage,System.Runtime.Serialization.Formatters.Binary.BinaryFormatter)
extern "C" Object_t * MessageFormatter_ReadMethodResponse_m10814 (Object_t * __this /* static, unused */, uint8_t ___elem, BinaryReader_t1805 * ___reader, bool ___hasHeaders, HeaderHandler_t2252 * ___headerHandler, Object_t * ___methodCallMessage, BinaryFormatter_t424 * ___formatter, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.Serialization.Formatters.Binary.MessageFormatter::AllTypesArePrimitive(System.Object[])
extern "C" bool MessageFormatter_AllTypesArePrimitive_m10815 (Object_t * __this /* static, unused */, ObjectU5BU5D_t224* ___objects, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.Serialization.Formatters.Binary.MessageFormatter::IsMethodPrimitive(System.Type)
extern "C" bool MessageFormatter_IsMethodPrimitive_m10816 (Object_t * __this /* static, unused */, Type_t * ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Runtime.Serialization.Formatters.Binary.MessageFormatter::GetExtraProperties(System.Collections.IDictionary,System.String[])
extern "C" ObjectU5BU5D_t224* MessageFormatter_GetExtraProperties_m10817 (Object_t * __this /* static, unused */, Object_t * ___properties, StringU5BU5D_t243* ___internalKeys, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.Serialization.Formatters.Binary.MessageFormatter::IsInternalKey(System.String,System.String[])
extern "C" bool MessageFormatter_IsInternalKey_m10818 (Object_t * __this /* static, unused */, String_t* ___key, StringU5BU5D_t243* ___internalKeys, const MethodInfo* method) IL2CPP_METHOD_ATTR;
