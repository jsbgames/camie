﻿#pragma once
#include <stdint.h>
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t326;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.EventSystems.BaseEventData
struct  BaseEventData_t453  : public Object_t
{
	// UnityEngine.EventSystems.EventSystem UnityEngine.EventSystems.BaseEventData::m_EventSystem
	EventSystem_t326 * ___m_EventSystem_0;
	// System.Boolean UnityEngine.EventSystems.BaseEventData::m_Used
	bool ___m_Used_1;
};
