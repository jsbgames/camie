﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets.ImageEffects.ContrastEnhance
struct ContrastEnhance_t89;
// UnityEngine.RenderTexture
struct RenderTexture_t101;

// System.Void UnityStandardAssets.ImageEffects.ContrastEnhance::.ctor()
extern "C" void ContrastEnhance__ctor_m247 (ContrastEnhance_t89 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityStandardAssets.ImageEffects.ContrastEnhance::CheckResources()
extern "C" bool ContrastEnhance_CheckResources_m248 (ContrastEnhance_t89 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.ImageEffects.ContrastEnhance::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void ContrastEnhance_OnRenderImage_m249 (ContrastEnhance_t89 * __this, RenderTexture_t101 * ___source, RenderTexture_t101 * ___destination, const MethodInfo* method) IL2CPP_METHOD_ATTR;
