﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<System.Int32>
struct DefaultComparer_t3439;

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.Int32>::.ctor()
extern "C" void DefaultComparer__ctor_m22126_gshared (DefaultComparer_t3439 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m22126(__this, method) (( void (*) (DefaultComparer_t3439 *, const MethodInfo*))DefaultComparer__ctor_m22126_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.Int32>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m22127_gshared (DefaultComparer_t3439 * __this, int32_t ___x, int32_t ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m22127(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t3439 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Compare_m22127_gshared)(__this, ___x, ___y, method)
