﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<SCR_JoystickDirection>
struct InternalEnumerator_1_t2977;
// System.Object
struct Object_t;
// SCR_JoystickDirection
struct SCR_JoystickDirection_t366;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<SCR_JoystickDirection>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m15799(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2977 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m13513_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<SCR_JoystickDirection>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15800(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2977 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13515_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<SCR_JoystickDirection>::Dispose()
#define InternalEnumerator_1_Dispose_m15801(__this, method) (( void (*) (InternalEnumerator_1_t2977 *, const MethodInfo*))InternalEnumerator_1_Dispose_m13517_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<SCR_JoystickDirection>::MoveNext()
#define InternalEnumerator_1_MoveNext_m15802(__this, method) (( bool (*) (InternalEnumerator_1_t2977 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m13519_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<SCR_JoystickDirection>::get_Current()
#define InternalEnumerator_1_get_Current_m15803(__this, method) (( SCR_JoystickDirection_t366 * (*) (InternalEnumerator_1_t2977 *, const MethodInfo*))InternalEnumerator_1_get_Current_m13521_gshared)(__this, method)
