﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementDefinition>
struct InternalEnumerator_1_t2879;
// System.Object
struct Object_t;
// UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementDefinition
struct ReplacementDefinition_t166;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementDefinition>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m14366(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2879 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m13513_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementDefinition>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14367(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2879 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13515_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementDefinition>::Dispose()
#define InternalEnumerator_1_Dispose_m14368(__this, method) (( void (*) (InternalEnumerator_1_t2879 *, const MethodInfo*))InternalEnumerator_1_Dispose_m13517_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementDefinition>::MoveNext()
#define InternalEnumerator_1_MoveNext_m14369(__this, method) (( bool (*) (InternalEnumerator_1_t2879 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m13519_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementDefinition>::get_Current()
#define InternalEnumerator_1_get_Current_m14370(__this, method) (( ReplacementDefinition_t166 * (*) (InternalEnumerator_1_t2879 *, const MethodInfo*))InternalEnumerator_1_get_Current_m13521_gshared)(__this, method)
