﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.AssemblyKeyFileAttribute
struct AssemblyKeyFileAttribute_t1475;
// System.String
struct String_t;

// System.Void System.Reflection.AssemblyKeyFileAttribute::.ctor(System.String)
extern "C" void AssemblyKeyFileAttribute__ctor_m6544 (AssemblyKeyFileAttribute_t1475 * __this, String_t* ___keyFile, const MethodInfo* method) IL2CPP_METHOD_ATTR;
