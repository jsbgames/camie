﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SCR_Spider
struct SCR_Spider_t384;
// UnityEngine.Collider
struct Collider_t138;
// System.Collections.IEnumerator
struct IEnumerator_t217;

// System.Void SCR_Spider::.ctor()
extern "C" void SCR_Spider__ctor_m1551 (SCR_Spider_t384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Spider::Start()
extern "C" void SCR_Spider_Start_m1552 (SCR_Spider_t384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Spider::FixedUpdate()
extern "C" void SCR_Spider_FixedUpdate_m1553 (SCR_Spider_t384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Spider::OnTriggerEnter(UnityEngine.Collider)
extern "C" void SCR_Spider_OnTriggerEnter_m1554 (SCR_Spider_t384 * __this, Collider_t138 * ___other, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Spider::Activate()
extern "C" void SCR_Spider_Activate_m1555 (SCR_Spider_t384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Spider::Reset()
extern "C" void SCR_Spider_Reset_m1556 (SCR_Spider_t384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Spider::DropLeaves()
extern "C" void SCR_Spider_DropLeaves_m1557 (SCR_Spider_t384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Spider::DrawWebString()
extern "C" void SCR_Spider_DrawWebString_m1558 (SCR_Spider_t384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator SCR_Spider::ChaseAvatar()
extern "C" Object_t * SCR_Spider_ChaseAvatar_m1559 (SCR_Spider_t384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
