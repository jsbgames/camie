﻿#pragma once
#include <stdint.h>
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// SCR_LevelManager
struct  SCR_LevelManager_t379  : public MonoBehaviour_t3
{
	// System.Int32 SCR_LevelManager::currentLevel
	int32_t ___currentLevel_3;
	// System.Int32 SCR_LevelManager::gameplayLevel
	int32_t ___gameplayLevel_4;
	// System.Boolean SCR_LevelManager::isMenu
	bool ___isMenu_5;
};
