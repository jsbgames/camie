﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "stringLiterals.h"
// <Module>
#include "AssemblyU2DCSharp_U3CModuleU3E.h"
// Metadata Definition <Module>
extern TypeInfo U3CModuleU3E_t286_il2cpp_TypeInfo;
// <Module>
#include "AssemblyU2DCSharp_U3CModuleU3EMethodDeclarations.h"
static const MethodInfo* U3CModuleU3E_t286_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType U3CModuleU3E_t286_0_0_0;
extern const Il2CppType U3CModuleU3E_t286_1_0_0;
struct U3CModuleU3E_t286;
const Il2CppTypeDefinitionMetadata U3CModuleU3E_t286_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U3CModuleU3E_t286_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<Module>"/* name */
	, ""/* namespaze */
	, U3CModuleU3E_t286_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U3CModuleU3E_t286_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U3CModuleU3E_t286_0_0_0/* byval_arg */
	, &U3CModuleU3E_t286_1_0_0/* this_arg */
	, &U3CModuleU3E_t286_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CModuleU3E_t286)/* instance_size */
	, sizeof (U3CModuleU3E_t286)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition MultiKeyDictionary`3
extern TypeInfo MultiKeyDictionary_3_t431_il2cpp_TypeInfo;
extern const Il2CppGenericContainer MultiKeyDictionary_3_t431_Il2CppGenericContainer;
extern TypeInfo MultiKeyDictionary_3_t431_gp_T1_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter MultiKeyDictionary_3_t431_gp_T1_0_il2cpp_TypeInfo_GenericParamFull = { &MultiKeyDictionary_3_t431_Il2CppGenericContainer, NULL, "T1", 0, 0 };
extern TypeInfo MultiKeyDictionary_3_t431_gp_T2_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter MultiKeyDictionary_3_t431_gp_T2_1_il2cpp_TypeInfo_GenericParamFull = { &MultiKeyDictionary_3_t431_Il2CppGenericContainer, NULL, "T2", 1, 0 };
extern TypeInfo MultiKeyDictionary_3_t431_gp_T3_2_il2cpp_TypeInfo;
extern const Il2CppGenericParameter MultiKeyDictionary_3_t431_gp_T3_2_il2cpp_TypeInfo_GenericParamFull = { &MultiKeyDictionary_3_t431_Il2CppGenericContainer, NULL, "T3", 2, 0 };
static const Il2CppGenericParameter* MultiKeyDictionary_3_t431_Il2CppGenericParametersArray[3] = 
{
	&MultiKeyDictionary_3_t431_gp_T1_0_il2cpp_TypeInfo_GenericParamFull,
	&MultiKeyDictionary_3_t431_gp_T2_1_il2cpp_TypeInfo_GenericParamFull,
	&MultiKeyDictionary_3_t431_gp_T3_2_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer MultiKeyDictionary_3_t431_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&MultiKeyDictionary_3_t431_il2cpp_TypeInfo, 3, 0, MultiKeyDictionary_3_t431_Il2CppGenericParametersArray };
extern const Il2CppType Void_t272_0_0_0;
// System.Void MultiKeyDictionary`3::.ctor()
extern const MethodInfo MultiKeyDictionary_3__ctor_m1833_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &MultiKeyDictionary_3_t431_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType MultiKeyDictionary_3_t431_gp_0_0_0_0;
extern const Il2CppType MultiKeyDictionary_3_t431_gp_0_0_0_0;
static const ParameterInfo MultiKeyDictionary_3_t431_MultiKeyDictionary_3_get_Item_m1834_ParameterInfos[] = 
{
	{"key", 0, 134217729, 0, &MultiKeyDictionary_3_t431_gp_0_0_0_0},
};
extern const Il2CppType Dictionary_2_t437_0_0_0;
// System.Collections.Generic.Dictionary`2<T2,T3> MultiKeyDictionary`3::get_Item(T1)
extern const MethodInfo MultiKeyDictionary_3_get_Item_m1834_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &MultiKeyDictionary_3_t431_il2cpp_TypeInfo/* declaring_type */
	, &Dictionary_2_t437_0_0_0/* return_type */
	, NULL/* invoker_method */
	, MultiKeyDictionary_3_t431_MultiKeyDictionary_3_get_Item_m1834_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType MultiKeyDictionary_3_t431_gp_0_0_0_0;
extern const Il2CppType MultiKeyDictionary_3_t431_gp_1_0_0_0;
extern const Il2CppType MultiKeyDictionary_3_t431_gp_1_0_0_0;
static const ParameterInfo MultiKeyDictionary_3_t431_MultiKeyDictionary_3_ContainsKey_m1835_ParameterInfos[] = 
{
	{"key1", 0, 134217730, 0, &MultiKeyDictionary_3_t431_gp_0_0_0_0},
	{"key2", 1, 134217731, 0, &MultiKeyDictionary_3_t431_gp_1_0_0_0},
};
extern const Il2CppType Boolean_t273_0_0_0;
// System.Boolean MultiKeyDictionary`3::ContainsKey(T1,T2)
extern const MethodInfo MultiKeyDictionary_3_ContainsKey_m1835_MethodInfo = 
{
	"ContainsKey"/* name */
	, NULL/* method */
	, &MultiKeyDictionary_3_t431_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, NULL/* invoker_method */
	, MultiKeyDictionary_3_t431_MultiKeyDictionary_3_ContainsKey_m1835_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MultiKeyDictionary_3_t431_MethodInfos[] =
{
	&MultiKeyDictionary_3__ctor_m1833_MethodInfo,
	&MultiKeyDictionary_3_get_Item_m1834_MethodInfo,
	&MultiKeyDictionary_3_ContainsKey_m1835_MethodInfo,
	NULL
};
extern const MethodInfo MultiKeyDictionary_3_get_Item_m1834_MethodInfo;
static const PropertyInfo MultiKeyDictionary_3_t431____Item_PropertyInfo = 
{
	&MultiKeyDictionary_3_t431_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &MultiKeyDictionary_3_get_Item_m1834_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* MultiKeyDictionary_3_t431_PropertyInfos[] =
{
	&MultiKeyDictionary_3_t431____Item_PropertyInfo,
	NULL
};
extern const MethodInfo Object_Equals_m1073_MethodInfo;
extern const MethodInfo Object_Finalize_m1048_MethodInfo;
extern const MethodInfo Object_GetHashCode_m1074_MethodInfo;
extern const MethodInfo Object_ToString_m1075_MethodInfo;
extern const Il2CppGenericMethod Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1846_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2_GetObjectData_m1847_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2_get_Count_m1848_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1849_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1850_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2_System_Collections_ICollection_CopyTo_m1851_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1852_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1853_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2_Clear_m1854_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1855_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1856_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1857_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1858_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2_Add_m1859_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2_Remove_m1860_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2_TryGetValue_m1861_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2_get_Item_m1862_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2_set_Item_m1863_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1864_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m1865_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2_System_Collections_IDictionary_get_Item_m1866_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2_System_Collections_IDictionary_set_Item_m1867_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2_System_Collections_IDictionary_Add_m1868_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2_System_Collections_IDictionary_Contains_m1869_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1870_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2_System_Collections_IDictionary_Remove_m1871_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2_OnDeserialization_m1872_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2_ContainsKey_m1873_GenericMethod;
static const Il2CppMethodReference MultiKeyDictionary_3_t431_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1846_GenericMethod,
	&Dictionary_2_GetObjectData_m1847_GenericMethod,
	&Dictionary_2_get_Count_m1848_GenericMethod,
	&Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1849_GenericMethod,
	&Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1850_GenericMethod,
	&Dictionary_2_System_Collections_ICollection_CopyTo_m1851_GenericMethod,
	&Dictionary_2_get_Count_m1848_GenericMethod,
	&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1852_GenericMethod,
	&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1853_GenericMethod,
	&Dictionary_2_Clear_m1854_GenericMethod,
	&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1855_GenericMethod,
	&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1856_GenericMethod,
	&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1857_GenericMethod,
	&Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1858_GenericMethod,
	&Dictionary_2_Add_m1859_GenericMethod,
	&Dictionary_2_Remove_m1860_GenericMethod,
	&Dictionary_2_TryGetValue_m1861_GenericMethod,
	&Dictionary_2_get_Item_m1862_GenericMethod,
	&Dictionary_2_set_Item_m1863_GenericMethod,
	&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1864_GenericMethod,
	&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m1865_GenericMethod,
	&Dictionary_2_System_Collections_IDictionary_get_Item_m1866_GenericMethod,
	&Dictionary_2_System_Collections_IDictionary_set_Item_m1867_GenericMethod,
	&Dictionary_2_System_Collections_IDictionary_Add_m1868_GenericMethod,
	&Dictionary_2_System_Collections_IDictionary_Contains_m1869_GenericMethod,
	&Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1870_GenericMethod,
	&Dictionary_2_System_Collections_IDictionary_Remove_m1871_GenericMethod,
	&Dictionary_2_OnDeserialization_m1872_GenericMethod,
	&Dictionary_2_ContainsKey_m1873_GenericMethod,
	&Dictionary_2_GetObjectData_m1847_GenericMethod,
	&Dictionary_2_OnDeserialization_m1872_GenericMethod,
};
static bool MultiKeyDictionary_3_t431_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	true,
	true,
	true,
	true,
	true,
	true,
	true,
	true,
	true,
	true,
	true,
	true,
	true,
	true,
	true,
	true,
	true,
	true,
	true,
	true,
	true,
	true,
	true,
	true,
	true,
	true,
	true,
	true,
	true,
	true,
	true,
};
extern const Il2CppType IEnumerable_t438_0_0_0;
extern const Il2CppType ISerializable_t439_0_0_0;
extern const Il2CppType ICollection_t440_0_0_0;
extern const Il2CppType ICollection_1_t441_0_0_0;
extern const Il2CppType IEnumerable_1_t442_0_0_0;
extern const Il2CppType IDictionary_2_t443_0_0_0;
extern const Il2CppType IDictionary_t444_0_0_0;
extern const Il2CppType IDeserializationCallback_t445_0_0_0;
static Il2CppInterfaceOffsetPair MultiKeyDictionary_3_t431_InterfacesOffsets[] = 
{
	{ &IEnumerable_t438_0_0_0, 4},
	{ &ISerializable_t439_0_0_0, 5},
	{ &ICollection_t440_0_0_0, 6},
	{ &ICollection_1_t441_0_0_0, 10},
	{ &IEnumerable_1_t442_0_0_0, 17},
	{ &IDictionary_2_t443_0_0_0, 18},
	{ &IDictionary_t444_0_0_0, 25},
	{ &IDeserializationCallback_t445_0_0_0, 31},
};
extern const Il2CppGenericMethod Dictionary_2__ctor_m1874_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2__ctor_m1875_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2_ContainsKey_m1876_GenericMethod;
static Il2CppRGCTXDefinition MultiKeyDictionary_3_t431_RGCTXData[8] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, &Dictionary_2__ctor_m1874_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Dictionary_2_ContainsKey_m1873_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&Dictionary_2_t437_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Dictionary_2__ctor_m1875_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Dictionary_2_Add_m1859_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Dictionary_2_TryGetValue_m1861_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Dictionary_2_ContainsKey_m1876_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType MultiKeyDictionary_3_t431_0_0_0;
extern const Il2CppType MultiKeyDictionary_3_t431_1_0_0;
extern const Il2CppType Dictionary_2_t446_0_0_0;
struct MultiKeyDictionary_3_t431;
const Il2CppTypeDefinitionMetadata MultiKeyDictionary_3_t431_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MultiKeyDictionary_3_t431_InterfacesOffsets/* interfaceOffsets */
	, &Dictionary_2_t446_0_0_0/* parent */
	, MultiKeyDictionary_3_t431_VTable/* vtableMethods */
	, MultiKeyDictionary_3_t431_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, MultiKeyDictionary_3_t431_RGCTXData/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo MultiKeyDictionary_3_t431_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "MultiKeyDictionary`3"/* name */
	, ""/* namespaze */
	, MultiKeyDictionary_3_t431_MethodInfos/* methods */
	, MultiKeyDictionary_3_t431_PropertyInfos/* properties */
	, NULL/* events */
	, &MultiKeyDictionary_3_t431_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2/* custom_attributes_cache */
	, &MultiKeyDictionary_3_t431_0_0_0/* byval_arg */
	, &MultiKeyDictionary_3_t431_1_0_0/* this_arg */
	, &MultiKeyDictionary_3_t431_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &MultiKeyDictionary_3_t431_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 35/* vtable_count */
	, 0/* interfaces_count */
	, 8/* interface_offsets_count */

};
// Images
#include "AssemblyU2DCSharp_Images.h"
// Metadata Definition Images
extern TypeInfo Images_t288_il2cpp_TypeInfo;
// Images
#include "AssemblyU2DCSharp_ImagesMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void Images::.ctor()
extern const MethodInfo Images__ctor_m1079_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Images__ctor_m1079/* method */
	, &Images_t288_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Images_t288_MethodInfos[] =
{
	&Images__ctor_m1079_MethodInfo,
	NULL
};
static const Il2CppMethodReference Images_t288_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool Images_t288_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType Images_t288_0_0_0;
extern const Il2CppType Images_t288_1_0_0;
extern const Il2CppType Object_t_0_0_0;
struct Images_t288;
const Il2CppTypeDefinitionMetadata Images_t288_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Images_t288_VTable/* vtableMethods */
	, Images_t288_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 0/* fieldStart */

};
TypeInfo Images_t288_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "Images"/* name */
	, ""/* namespaze */
	, Images_t288_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Images_t288_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Images_t288_0_0_0/* byval_arg */
	, &Images_t288_1_0_0/* this_arg */
	, &Images_t288_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Images_t288)/* instance_size */
	, sizeof (Images_t288)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 29/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Reporter/_LogType
#include "AssemblyU2DCSharp_Reporter__LogType.h"
// Metadata Definition Reporter/_LogType
extern TypeInfo _LogType_t289_il2cpp_TypeInfo;
// Reporter/_LogType
#include "AssemblyU2DCSharp_Reporter__LogTypeMethodDeclarations.h"
static const MethodInfo* _LogType_t289_MethodInfos[] =
{
	NULL
};
extern const MethodInfo Enum_Equals_m1051_MethodInfo;
extern const MethodInfo Enum_GetHashCode_m1052_MethodInfo;
extern const MethodInfo Enum_ToString_m1053_MethodInfo;
extern const MethodInfo Enum_ToString_m1054_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToBoolean_m1055_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToByte_m1056_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToChar_m1057_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDateTime_m1058_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDecimal_m1059_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDouble_m1060_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt16_m1061_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt32_m1062_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt64_m1063_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToSByte_m1064_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToSingle_m1065_MethodInfo;
extern const MethodInfo Enum_ToString_m1066_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToType_m1067_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt16_m1068_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt32_m1069_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt64_m1070_MethodInfo;
extern const MethodInfo Enum_CompareTo_m1071_MethodInfo;
extern const MethodInfo Enum_GetTypeCode_m1072_MethodInfo;
static const Il2CppMethodReference _LogType_t289_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool _LogType_t289_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IFormattable_t275_0_0_0;
extern const Il2CppType IConvertible_t276_0_0_0;
extern const Il2CppType IComparable_t277_0_0_0;
static Il2CppInterfaceOffsetPair _LogType_t289_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType _LogType_t289_0_0_0;
extern const Il2CppType _LogType_t289_1_0_0;
extern const Il2CppType Enum_t278_0_0_0;
extern TypeInfo Reporter_t295_il2cpp_TypeInfo;
extern const Il2CppType Reporter_t295_0_0_0;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t253_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata _LogType_t289_DefinitionMetadata = 
{
	&Reporter_t295_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, _LogType_t289_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, _LogType_t289_VTable/* vtableMethods */
	, _LogType_t289_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 29/* fieldStart */

};
TypeInfo _LogType_t289_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "_LogType"/* name */
	, ""/* namespaze */
	, _LogType_t289_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &_LogType_t289_0_0_0/* byval_arg */
	, &_LogType_t289_1_0_0/* this_arg */
	, &_LogType_t289_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (_LogType_t289)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (_LogType_t289)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Reporter/Sample
#include "AssemblyU2DCSharp_Reporter_Sample.h"
// Metadata Definition Reporter/Sample
extern TypeInfo Sample_t290_il2cpp_TypeInfo;
// Reporter/Sample
#include "AssemblyU2DCSharp_Reporter_SampleMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void Reporter/Sample::.ctor()
extern const MethodInfo Sample__ctor_m1080_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Sample__ctor_m1080/* method */
	, &Sample_t290_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 38/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single Reporter/Sample::MemSize()
extern const MethodInfo Sample_MemSize_m1081_MethodInfo = 
{
	"MemSize"/* name */
	, (methodPointerType)&Sample_MemSize_m1081/* method */
	, &Sample_t290_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 39/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Sample_t290_MethodInfos[] =
{
	&Sample__ctor_m1080_MethodInfo,
	&Sample_MemSize_m1081_MethodInfo,
	NULL
};
static const Il2CppMethodReference Sample_t290_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool Sample_t290_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType Sample_t290_0_0_0;
extern const Il2CppType Sample_t290_1_0_0;
struct Sample_t290;
const Il2CppTypeDefinitionMetadata Sample_t290_DefinitionMetadata = 
{
	&Reporter_t295_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Sample_t290_VTable/* vtableMethods */
	, Sample_t290_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 35/* fieldStart */

};
TypeInfo Sample_t290_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "Sample"/* name */
	, ""/* namespaze */
	, Sample_t290_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Sample_t290_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Sample_t290_0_0_0/* byval_arg */
	, &Sample_t290_1_0_0/* this_arg */
	, &Sample_t290_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Sample_t290)/* instance_size */
	, sizeof (Sample_t290)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048578/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Reporter/Log
#include "AssemblyU2DCSharp_Reporter_Log.h"
// Metadata Definition Reporter/Log
extern TypeInfo Log_t291_il2cpp_TypeInfo;
// Reporter/Log
#include "AssemblyU2DCSharp_Reporter_LogMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void Reporter/Log::.ctor()
extern const MethodInfo Log__ctor_m1082_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Log__ctor_m1082/* method */
	, &Log_t291_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 40/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Log_t291_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// Reporter/Log Reporter/Log::CreateCopy()
extern const MethodInfo Log_CreateCopy_m1083_MethodInfo = 
{
	"CreateCopy"/* name */
	, (methodPointerType)&Log_CreateCopy_m1083/* method */
	, &Log_t291_il2cpp_TypeInfo/* declaring_type */
	, &Log_t291_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 41/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single Reporter/Log::GetMemoryUsage()
extern const MethodInfo Log_GetMemoryUsage_m1084_MethodInfo = 
{
	"GetMemoryUsage"/* name */
	, (methodPointerType)&Log_GetMemoryUsage_m1084/* method */
	, &Log_t291_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 42/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Log_t291_MethodInfos[] =
{
	&Log__ctor_m1082_MethodInfo,
	&Log_CreateCopy_m1083_MethodInfo,
	&Log_GetMemoryUsage_m1084_MethodInfo,
	NULL
};
static const Il2CppMethodReference Log_t291_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool Log_t291_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType Log_t291_1_0_0;
struct Log_t291;
const Il2CppTypeDefinitionMetadata Log_t291_DefinitionMetadata = 
{
	&Reporter_t295_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Log_t291_VTable/* vtableMethods */
	, Log_t291_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 40/* fieldStart */

};
TypeInfo Log_t291_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "Log"/* name */
	, ""/* namespaze */
	, Log_t291_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Log_t291_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Log_t291_0_0_0/* byval_arg */
	, &Log_t291_1_0_0/* this_arg */
	, &Log_t291_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Log_t291)/* instance_size */
	, sizeof (Log_t291)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048578/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Reporter/ReportView
#include "AssemblyU2DCSharp_Reporter_ReportView.h"
// Metadata Definition Reporter/ReportView
extern TypeInfo ReportView_t292_il2cpp_TypeInfo;
// Reporter/ReportView
#include "AssemblyU2DCSharp_Reporter_ReportViewMethodDeclarations.h"
static const MethodInfo* ReportView_t292_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference ReportView_t292_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool ReportView_t292_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ReportView_t292_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType ReportView_t292_0_0_0;
extern const Il2CppType ReportView_t292_1_0_0;
const Il2CppTypeDefinitionMetadata ReportView_t292_DefinitionMetadata = 
{
	&Reporter_t295_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ReportView_t292_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, ReportView_t292_VTable/* vtableMethods */
	, ReportView_t292_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 45/* fieldStart */

};
TypeInfo ReportView_t292_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "ReportView"/* name */
	, ""/* namespaze */
	, ReportView_t292_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ReportView_t292_0_0_0/* byval_arg */
	, &ReportView_t292_1_0_0/* this_arg */
	, &ReportView_t292_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ReportView_t292)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ReportView_t292)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Reporter/DetailView
#include "AssemblyU2DCSharp_Reporter_DetailView.h"
// Metadata Definition Reporter/DetailView
extern TypeInfo DetailView_t293_il2cpp_TypeInfo;
// Reporter/DetailView
#include "AssemblyU2DCSharp_Reporter_DetailViewMethodDeclarations.h"
static const MethodInfo* DetailView_t293_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference DetailView_t293_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool DetailView_t293_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair DetailView_t293_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType DetailView_t293_0_0_0;
extern const Il2CppType DetailView_t293_1_0_0;
const Il2CppTypeDefinitionMetadata DetailView_t293_DefinitionMetadata = 
{
	&Reporter_t295_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DetailView_t293_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, DetailView_t293_VTable/* vtableMethods */
	, DetailView_t293_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 50/* fieldStart */

};
TypeInfo DetailView_t293_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "DetailView"/* name */
	, ""/* namespaze */
	, DetailView_t293_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DetailView_t293_0_0_0/* byval_arg */
	, &DetailView_t293_1_0_0/* this_arg */
	, &DetailView_t293_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DetailView_t293)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (DetailView_t293)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Reporter/<readInfo>c__Iterator0
#include "AssemblyU2DCSharp_Reporter_U3CreadInfoU3Ec__Iterator0.h"
// Metadata Definition Reporter/<readInfo>c__Iterator0
extern TypeInfo U3CreadInfoU3Ec__Iterator0_t296_il2cpp_TypeInfo;
// Reporter/<readInfo>c__Iterator0
#include "AssemblyU2DCSharp_Reporter_U3CreadInfoU3Ec__Iterator0MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void Reporter/<readInfo>c__Iterator0::.ctor()
extern const MethodInfo U3CreadInfoU3Ec__Iterator0__ctor_m1085_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CreadInfoU3Ec__Iterator0__ctor_m1085/* method */
	, &U3CreadInfoU3Ec__Iterator0_t296_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 43/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object Reporter/<readInfo>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern const MethodInfo U3CreadInfoU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1086_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<object>.get_Current"/* name */
	, (methodPointerType)&U3CreadInfoU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1086/* method */
	, &U3CreadInfoU3Ec__Iterator0_t296_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 6/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 44/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object Reporter/<readInfo>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern const MethodInfo U3CreadInfoU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1087_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&U3CreadInfoU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1087/* method */
	, &U3CreadInfoU3Ec__Iterator0_t296_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 7/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 45/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean Reporter/<readInfo>c__Iterator0::MoveNext()
extern const MethodInfo U3CreadInfoU3Ec__Iterator0_MoveNext_m1088_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&U3CreadInfoU3Ec__Iterator0_MoveNext_m1088/* method */
	, &U3CreadInfoU3Ec__Iterator0_t296_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 46/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void Reporter/<readInfo>c__Iterator0::Dispose()
extern const MethodInfo U3CreadInfoU3Ec__Iterator0_Dispose_m1089_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&U3CreadInfoU3Ec__Iterator0_Dispose_m1089/* method */
	, &U3CreadInfoU3Ec__Iterator0_t296_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 8/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 47/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void Reporter/<readInfo>c__Iterator0::Reset()
extern const MethodInfo U3CreadInfoU3Ec__Iterator0_Reset_m1090_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&U3CreadInfoU3Ec__Iterator0_Reset_m1090/* method */
	, &U3CreadInfoU3Ec__Iterator0_t296_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 9/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 48/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CreadInfoU3Ec__Iterator0_t296_MethodInfos[] =
{
	&U3CreadInfoU3Ec__Iterator0__ctor_m1085_MethodInfo,
	&U3CreadInfoU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1086_MethodInfo,
	&U3CreadInfoU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1087_MethodInfo,
	&U3CreadInfoU3Ec__Iterator0_MoveNext_m1088_MethodInfo,
	&U3CreadInfoU3Ec__Iterator0_Dispose_m1089_MethodInfo,
	&U3CreadInfoU3Ec__Iterator0_Reset_m1090_MethodInfo,
	NULL
};
extern const MethodInfo U3CreadInfoU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1086_MethodInfo;
static const PropertyInfo U3CreadInfoU3Ec__Iterator0_t296____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo = 
{
	&U3CreadInfoU3Ec__Iterator0_t296_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<object>.Current"/* name */
	, &U3CreadInfoU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1086_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo U3CreadInfoU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1087_MethodInfo;
static const PropertyInfo U3CreadInfoU3Ec__Iterator0_t296____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CreadInfoU3Ec__Iterator0_t296_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CreadInfoU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1087_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* U3CreadInfoU3Ec__Iterator0_t296_PropertyInfos[] =
{
	&U3CreadInfoU3Ec__Iterator0_t296____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo,
	&U3CreadInfoU3Ec__Iterator0_t296____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern const MethodInfo U3CreadInfoU3Ec__Iterator0_Dispose_m1089_MethodInfo;
extern const MethodInfo U3CreadInfoU3Ec__Iterator0_MoveNext_m1088_MethodInfo;
extern const MethodInfo U3CreadInfoU3Ec__Iterator0_Reset_m1090_MethodInfo;
static const Il2CppMethodReference U3CreadInfoU3Ec__Iterator0_t296_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&U3CreadInfoU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1086_MethodInfo,
	&U3CreadInfoU3Ec__Iterator0_Dispose_m1089_MethodInfo,
	&U3CreadInfoU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1087_MethodInfo,
	&U3CreadInfoU3Ec__Iterator0_MoveNext_m1088_MethodInfo,
	&U3CreadInfoU3Ec__Iterator0_Reset_m1090_MethodInfo,
};
static bool U3CreadInfoU3Ec__Iterator0_t296_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IEnumerator_1_t284_0_0_0;
extern const Il2CppType IDisposable_t233_0_0_0;
extern const Il2CppType IEnumerator_t217_0_0_0;
static const Il2CppType* U3CreadInfoU3Ec__Iterator0_t296_InterfacesTypeInfos[] = 
{
	&IEnumerator_1_t284_0_0_0,
	&IDisposable_t233_0_0_0,
	&IEnumerator_t217_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CreadInfoU3Ec__Iterator0_t296_InterfacesOffsets[] = 
{
	{ &IEnumerator_1_t284_0_0_0, 4},
	{ &IDisposable_t233_0_0_0, 5},
	{ &IEnumerator_t217_0_0_0, 6},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType U3CreadInfoU3Ec__Iterator0_t296_0_0_0;
extern const Il2CppType U3CreadInfoU3Ec__Iterator0_t296_1_0_0;
struct U3CreadInfoU3Ec__Iterator0_t296;
const Il2CppTypeDefinitionMetadata U3CreadInfoU3Ec__Iterator0_t296_DefinitionMetadata = 
{
	&Reporter_t295_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CreadInfoU3Ec__Iterator0_t296_InterfacesTypeInfos/* implementedInterfaces */
	, U3CreadInfoU3Ec__Iterator0_t296_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CreadInfoU3Ec__Iterator0_t296_VTable/* vtableMethods */
	, U3CreadInfoU3Ec__Iterator0_t296_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 54/* fieldStart */

};
TypeInfo U3CreadInfoU3Ec__Iterator0_t296_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<readInfo>c__Iterator0"/* name */
	, ""/* namespaze */
	, U3CreadInfoU3Ec__Iterator0_t296_MethodInfos/* methods */
	, U3CreadInfoU3Ec__Iterator0_t296_PropertyInfos/* properties */
	, NULL/* events */
	, &U3CreadInfoU3Ec__Iterator0_t296_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 5/* custom_attributes_cache */
	, &U3CreadInfoU3Ec__Iterator0_t296_0_0_0/* byval_arg */
	, &U3CreadInfoU3Ec__Iterator0_t296_1_0_0/* this_arg */
	, &U3CreadInfoU3Ec__Iterator0_t296_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CreadInfoU3Ec__Iterator0_t296)/* instance_size */
	, sizeof (U3CreadInfoU3Ec__Iterator0_t296)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Reporter
#include "AssemblyU2DCSharp_Reporter.h"
// Metadata Definition Reporter
// Reporter
#include "AssemblyU2DCSharp_ReporterMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void Reporter::.ctor()
extern const MethodInfo Reporter__ctor_m1091_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Reporter__ctor_m1091/* method */
	, &Reporter_t295_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void Reporter::.cctor()
extern const MethodInfo Reporter__cctor_m1092_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&Reporter__cctor_m1092/* method */
	, &Reporter_t295_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single Reporter::get_TotalMemUsage()
extern const MethodInfo Reporter_get_TotalMemUsage_m1093_MethodInfo = 
{
	"get_TotalMemUsage"/* name */
	, (methodPointerType)&Reporter_get_TotalMemUsage_m1093/* method */
	, &Reporter_t295_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 7/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void Reporter::Awake()
extern const MethodInfo Reporter_Awake_m1094_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&Reporter_Awake_m1094/* method */
	, &Reporter_t295_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 8/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void Reporter::OnEnable()
extern const MethodInfo Reporter_OnEnable_m1095_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&Reporter_OnEnable_m1095/* method */
	, &Reporter_t295_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 9/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void Reporter::OnDisable()
extern const MethodInfo Reporter_OnDisable_m1096_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&Reporter_OnDisable_m1096/* method */
	, &Reporter_t295_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 10/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void Reporter::addSample()
extern const MethodInfo Reporter_addSample_m1097_MethodInfo = 
{
	"addSample"/* name */
	, (methodPointerType)&Reporter_addSample_m1097/* method */
	, &Reporter_t295_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 11/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void Reporter::Initialize()
extern const MethodInfo Reporter_Initialize_m1098_MethodInfo = 
{
	"Initialize"/* name */
	, (methodPointerType)&Reporter_Initialize_m1098/* method */
	, &Reporter_t295_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 12/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void Reporter::initializeStyle()
extern const MethodInfo Reporter_initializeStyle_m1099_MethodInfo = 
{
	"initializeStyle"/* name */
	, (methodPointerType)&Reporter_initializeStyle_m1099/* method */
	, &Reporter_t295_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 13/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void Reporter::Start()
extern const MethodInfo Reporter_Start_m1100_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&Reporter_Start_m1100/* method */
	, &Reporter_t295_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 14/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void Reporter::clear()
extern const MethodInfo Reporter_clear_m1101_MethodInfo = 
{
	"clear"/* name */
	, (methodPointerType)&Reporter_clear_m1101/* method */
	, &Reporter_t295_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 15/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void Reporter::calculateCurrentLog()
extern const MethodInfo Reporter_calculateCurrentLog_m1102_MethodInfo = 
{
	"calculateCurrentLog"/* name */
	, (methodPointerType)&Reporter_calculateCurrentLog_m1102/* method */
	, &Reporter_t295_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 16/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void Reporter::DrawInfo()
extern const MethodInfo Reporter_DrawInfo_m1103_MethodInfo = 
{
	"DrawInfo"/* name */
	, (methodPointerType)&Reporter_DrawInfo_m1103/* method */
	, &Reporter_t295_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 17/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void Reporter::drawInfo_enableDisableToolBarButtons()
extern const MethodInfo Reporter_drawInfo_enableDisableToolBarButtons_m1104_MethodInfo = 
{
	"drawInfo_enableDisableToolBarButtons"/* name */
	, (methodPointerType)&Reporter_drawInfo_enableDisableToolBarButtons_m1104/* method */
	, &Reporter_t295_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 18/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void Reporter::DrawReport()
extern const MethodInfo Reporter_DrawReport_m1105_MethodInfo = 
{
	"DrawReport"/* name */
	, (methodPointerType)&Reporter_DrawReport_m1105/* method */
	, &Reporter_t295_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 19/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void Reporter::drawToolBar()
extern const MethodInfo Reporter_drawToolBar_m1106_MethodInfo = 
{
	"drawToolBar"/* name */
	, (methodPointerType)&Reporter_drawToolBar_m1106/* method */
	, &Reporter_t295_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 20/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void Reporter::DrawLogs()
extern const MethodInfo Reporter_DrawLogs_m1107_MethodInfo = 
{
	"DrawLogs"/* name */
	, (methodPointerType)&Reporter_DrawLogs_m1107/* method */
	, &Reporter_t295_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 21/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void Reporter::drawGraph()
extern const MethodInfo Reporter_drawGraph_m1108_MethodInfo = 
{
	"drawGraph"/* name */
	, (methodPointerType)&Reporter_drawGraph_m1108/* method */
	, &Reporter_t295_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 22/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void Reporter::drawStack()
extern const MethodInfo Reporter_drawStack_m1109_MethodInfo = 
{
	"drawStack"/* name */
	, (methodPointerType)&Reporter_drawStack_m1109/* method */
	, &Reporter_t295_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 23/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void Reporter::OnGUIDraw()
extern const MethodInfo Reporter_OnGUIDraw_m1110_MethodInfo = 
{
	"OnGUIDraw"/* name */
	, (methodPointerType)&Reporter_OnGUIDraw_m1110/* method */
	, &Reporter_t295_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 24/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean Reporter::isGestureDone()
extern const MethodInfo Reporter_isGestureDone_m1111_MethodInfo = 
{
	"isGestureDone"/* name */
	, (methodPointerType)&Reporter_isGestureDone_m1111/* method */
	, &Reporter_t295_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 25/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean Reporter::isDoubleClickDone()
extern const MethodInfo Reporter_isDoubleClickDone_m1112_MethodInfo = 
{
	"isDoubleClickDone"/* name */
	, (methodPointerType)&Reporter_isDoubleClickDone_m1112/* method */
	, &Reporter_t295_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 26/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t6_0_0_0;
extern void* RuntimeInvoker_Vector2_t6 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector2 Reporter::getDownPos()
extern const MethodInfo Reporter_getDownPos_m1113_MethodInfo = 
{
	"getDownPos"/* name */
	, (methodPointerType)&Reporter_getDownPos_m1113/* method */
	, &Reporter_t295_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t6_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t6/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 27/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Vector2_t6 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector2 Reporter::getDrag()
extern const MethodInfo Reporter_getDrag_m1114_MethodInfo = 
{
	"getDrag"/* name */
	, (methodPointerType)&Reporter_getDrag_m1114/* method */
	, &Reporter_t295_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t6_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t6/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 28/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void Reporter::calculateStartIndex()
extern const MethodInfo Reporter_calculateStartIndex_m1115_MethodInfo = 
{
	"calculateStartIndex"/* name */
	, (methodPointerType)&Reporter_calculateStartIndex_m1115/* method */
	, &Reporter_t295_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 29/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void Reporter::doShow()
extern const MethodInfo Reporter_doShow_m1116_MethodInfo = 
{
	"doShow"/* name */
	, (methodPointerType)&Reporter_doShow_m1116/* method */
	, &Reporter_t295_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 30/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void Reporter::Update()
extern const MethodInfo Reporter_Update_m1117_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&Reporter_Update_m1117/* method */
	, &Reporter_t295_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 31/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType LogType_t447_0_0_0;
extern const Il2CppType LogType_t447_0_0_0;
static const ParameterInfo Reporter_t295_Reporter_CaptureLog_m1118_ParameterInfos[] = 
{
	{"condition", 0, 134217732, 0, &String_t_0_0_0},
	{"stacktrace", 1, 134217733, 0, &String_t_0_0_0},
	{"type", 2, 134217734, 0, &LogType_t447_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void Reporter::CaptureLog(System.String,System.String,UnityEngine.LogType)
extern const MethodInfo Reporter_CaptureLog_m1118_MethodInfo = 
{
	"CaptureLog"/* name */
	, (methodPointerType)&Reporter_CaptureLog_m1118/* method */
	, &Reporter_t295_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t_Int32_t253/* invoker_method */
	, Reporter_t295_Reporter_CaptureLog_m1118_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 32/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType LogType_t447_0_0_0;
static const ParameterInfo Reporter_t295_Reporter_AddLog_m1119_ParameterInfos[] = 
{
	{"condition", 0, 134217735, 0, &String_t_0_0_0},
	{"stacktrace", 1, 134217736, 0, &String_t_0_0_0},
	{"type", 2, 134217737, 0, &LogType_t447_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void Reporter::AddLog(System.String,System.String,UnityEngine.LogType)
extern const MethodInfo Reporter_AddLog_m1119_MethodInfo = 
{
	"AddLog"/* name */
	, (methodPointerType)&Reporter_AddLog_m1119/* method */
	, &Reporter_t295_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t_Int32_t253/* invoker_method */
	, Reporter_t295_Reporter_AddLog_m1119_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 33/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType LogType_t447_0_0_0;
static const ParameterInfo Reporter_t295_Reporter_CaptureLogThread_m1120_ParameterInfos[] = 
{
	{"condition", 0, 134217738, 0, &String_t_0_0_0},
	{"stacktrace", 1, 134217739, 0, &String_t_0_0_0},
	{"type", 2, 134217740, 0, &LogType_t447_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void Reporter::CaptureLogThread(System.String,System.String,UnityEngine.LogType)
extern const MethodInfo Reporter_CaptureLogThread_m1120_MethodInfo = 
{
	"CaptureLogThread"/* name */
	, (methodPointerType)&Reporter_CaptureLogThread_m1120/* method */
	, &Reporter_t295_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t_Int32_t253/* invoker_method */
	, Reporter_t295_Reporter_CaptureLogThread_m1120_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 34/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void Reporter::OnLevelWasLoaded()
extern const MethodInfo Reporter_OnLevelWasLoaded_m1121_MethodInfo = 
{
	"OnLevelWasLoaded"/* name */
	, (methodPointerType)&Reporter_OnLevelWasLoaded_m1121/* method */
	, &Reporter_t295_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 35/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void Reporter::OnApplicationQuit()
extern const MethodInfo Reporter_OnApplicationQuit_m1122_MethodInfo = 
{
	"OnApplicationQuit"/* name */
	, (methodPointerType)&Reporter_OnApplicationQuit_m1122/* method */
	, &Reporter_t295_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 36/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.IEnumerator Reporter::readInfo()
extern const MethodInfo Reporter_readInfo_m1123_MethodInfo = 
{
	"readInfo"/* name */
	, (methodPointerType)&Reporter_readInfo_m1123/* method */
	, &Reporter_t295_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t217_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 4/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 37/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Reporter_t295_MethodInfos[] =
{
	&Reporter__ctor_m1091_MethodInfo,
	&Reporter__cctor_m1092_MethodInfo,
	&Reporter_get_TotalMemUsage_m1093_MethodInfo,
	&Reporter_Awake_m1094_MethodInfo,
	&Reporter_OnEnable_m1095_MethodInfo,
	&Reporter_OnDisable_m1096_MethodInfo,
	&Reporter_addSample_m1097_MethodInfo,
	&Reporter_Initialize_m1098_MethodInfo,
	&Reporter_initializeStyle_m1099_MethodInfo,
	&Reporter_Start_m1100_MethodInfo,
	&Reporter_clear_m1101_MethodInfo,
	&Reporter_calculateCurrentLog_m1102_MethodInfo,
	&Reporter_DrawInfo_m1103_MethodInfo,
	&Reporter_drawInfo_enableDisableToolBarButtons_m1104_MethodInfo,
	&Reporter_DrawReport_m1105_MethodInfo,
	&Reporter_drawToolBar_m1106_MethodInfo,
	&Reporter_DrawLogs_m1107_MethodInfo,
	&Reporter_drawGraph_m1108_MethodInfo,
	&Reporter_drawStack_m1109_MethodInfo,
	&Reporter_OnGUIDraw_m1110_MethodInfo,
	&Reporter_isGestureDone_m1111_MethodInfo,
	&Reporter_isDoubleClickDone_m1112_MethodInfo,
	&Reporter_getDownPos_m1113_MethodInfo,
	&Reporter_getDrag_m1114_MethodInfo,
	&Reporter_calculateStartIndex_m1115_MethodInfo,
	&Reporter_doShow_m1116_MethodInfo,
	&Reporter_Update_m1117_MethodInfo,
	&Reporter_CaptureLog_m1118_MethodInfo,
	&Reporter_AddLog_m1119_MethodInfo,
	&Reporter_CaptureLogThread_m1120_MethodInfo,
	&Reporter_OnLevelWasLoaded_m1121_MethodInfo,
	&Reporter_OnApplicationQuit_m1122_MethodInfo,
	&Reporter_readInfo_m1123_MethodInfo,
	NULL
};
extern const MethodInfo Reporter_get_TotalMemUsage_m1093_MethodInfo;
static const PropertyInfo Reporter_t295____TotalMemUsage_PropertyInfo = 
{
	&Reporter_t295_il2cpp_TypeInfo/* parent */
	, "TotalMemUsage"/* name */
	, &Reporter_get_TotalMemUsage_m1093_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Reporter_t295_PropertyInfos[] =
{
	&Reporter_t295____TotalMemUsage_PropertyInfo,
	NULL
};
static const Il2CppType* Reporter_t295_il2cpp_TypeInfo__nestedTypes[6] =
{
	&_LogType_t289_0_0_0,
	&Sample_t290_0_0_0,
	&Log_t291_0_0_0,
	&ReportView_t292_0_0_0,
	&DetailView_t293_0_0_0,
	&U3CreadInfoU3Ec__Iterator0_t296_0_0_0,
};
extern const MethodInfo Object_Equals_m1047_MethodInfo;
extern const MethodInfo Object_GetHashCode_m1049_MethodInfo;
extern const MethodInfo Object_ToString_m1050_MethodInfo;
static const Il2CppMethodReference Reporter_t295_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool Reporter_t295_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType Reporter_t295_1_0_0;
extern const Il2CppType MonoBehaviour_t3_0_0_0;
struct Reporter_t295;
const Il2CppTypeDefinitionMetadata Reporter_t295_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Reporter_t295_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, Reporter_t295_VTable/* vtableMethods */
	, Reporter_t295_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 61/* fieldStart */

};
TypeInfo Reporter_t295_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "Reporter"/* name */
	, ""/* namespaze */
	, Reporter_t295_MethodInfos/* methods */
	, Reporter_t295_PropertyInfos/* properties */
	, NULL/* events */
	, &Reporter_t295_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Reporter_t295_0_0_0/* byval_arg */
	, &Reporter_t295_1_0_0/* this_arg */
	, &Reporter_t295_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Reporter_t295)/* instance_size */
	, sizeof (Reporter_t295)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Reporter_t295_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 33/* method_count */
	, 1/* property_count */
	, 147/* field_count */
	, 0/* event_count */
	, 6/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// ReporterGUI
#include "AssemblyU2DCSharp_ReporterGUI.h"
// Metadata Definition ReporterGUI
extern TypeInfo ReporterGUI_t305_il2cpp_TypeInfo;
// ReporterGUI
#include "AssemblyU2DCSharp_ReporterGUIMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void ReporterGUI::.ctor()
extern const MethodInfo ReporterGUI__ctor_m1124_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ReporterGUI__ctor_m1124/* method */
	, &ReporterGUI_t305_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 49/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void ReporterGUI::Awake()
extern const MethodInfo ReporterGUI_Awake_m1125_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&ReporterGUI_Awake_m1125/* method */
	, &ReporterGUI_t305_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 50/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void ReporterGUI::OnGUI()
extern const MethodInfo ReporterGUI_OnGUI_m1126_MethodInfo = 
{
	"OnGUI"/* name */
	, (methodPointerType)&ReporterGUI_OnGUI_m1126/* method */
	, &ReporterGUI_t305_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 51/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ReporterGUI_t305_MethodInfos[] =
{
	&ReporterGUI__ctor_m1124_MethodInfo,
	&ReporterGUI_Awake_m1125_MethodInfo,
	&ReporterGUI_OnGUI_m1126_MethodInfo,
	NULL
};
static const Il2CppMethodReference ReporterGUI_t305_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool ReporterGUI_t305_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType ReporterGUI_t305_0_0_0;
extern const Il2CppType ReporterGUI_t305_1_0_0;
struct ReporterGUI_t305;
const Il2CppTypeDefinitionMetadata ReporterGUI_t305_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, ReporterGUI_t305_VTable/* vtableMethods */
	, ReporterGUI_t305_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 208/* fieldStart */

};
TypeInfo ReporterGUI_t305_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "ReporterGUI"/* name */
	, ""/* namespaze */
	, ReporterGUI_t305_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ReporterGUI_t305_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ReporterGUI_t305_0_0_0/* byval_arg */
	, &ReporterGUI_t305_1_0_0/* this_arg */
	, &ReporterGUI_t305_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ReporterGUI_t305)/* instance_size */
	, sizeof (ReporterGUI_t305)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// ReporterMessageReceiver
#include "AssemblyU2DCSharp_ReporterMessageReceiver.h"
// Metadata Definition ReporterMessageReceiver
extern TypeInfo ReporterMessageReceiver_t306_il2cpp_TypeInfo;
// ReporterMessageReceiver
#include "AssemblyU2DCSharp_ReporterMessageReceiverMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void ReporterMessageReceiver::.ctor()
extern const MethodInfo ReporterMessageReceiver__ctor_m1127_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ReporterMessageReceiver__ctor_m1127/* method */
	, &ReporterMessageReceiver_t306_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 52/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void ReporterMessageReceiver::Start()
extern const MethodInfo ReporterMessageReceiver_Start_m1128_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&ReporterMessageReceiver_Start_m1128/* method */
	, &ReporterMessageReceiver_t306_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 53/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void ReporterMessageReceiver::OnPreStart()
extern const MethodInfo ReporterMessageReceiver_OnPreStart_m1129_MethodInfo = 
{
	"OnPreStart"/* name */
	, (methodPointerType)&ReporterMessageReceiver_OnPreStart_m1129/* method */
	, &ReporterMessageReceiver_t306_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 54/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void ReporterMessageReceiver::OnHideReporter()
extern const MethodInfo ReporterMessageReceiver_OnHideReporter_m1130_MethodInfo = 
{
	"OnHideReporter"/* name */
	, (methodPointerType)&ReporterMessageReceiver_OnHideReporter_m1130/* method */
	, &ReporterMessageReceiver_t306_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 55/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void ReporterMessageReceiver::OnShowReporter()
extern const MethodInfo ReporterMessageReceiver_OnShowReporter_m1131_MethodInfo = 
{
	"OnShowReporter"/* name */
	, (methodPointerType)&ReporterMessageReceiver_OnShowReporter_m1131/* method */
	, &ReporterMessageReceiver_t306_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 56/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Log_t291_0_0_0;
static const ParameterInfo ReporterMessageReceiver_t306_ReporterMessageReceiver_OnLog_m1132_ParameterInfos[] = 
{
	{"log", 0, 134217741, 0, &Log_t291_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void ReporterMessageReceiver::OnLog(Reporter/Log)
extern const MethodInfo ReporterMessageReceiver_OnLog_m1132_MethodInfo = 
{
	"OnLog"/* name */
	, (methodPointerType)&ReporterMessageReceiver_OnLog_m1132/* method */
	, &ReporterMessageReceiver_t306_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, ReporterMessageReceiver_t306_ReporterMessageReceiver_OnLog_m1132_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 57/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ReporterMessageReceiver_t306_MethodInfos[] =
{
	&ReporterMessageReceiver__ctor_m1127_MethodInfo,
	&ReporterMessageReceiver_Start_m1128_MethodInfo,
	&ReporterMessageReceiver_OnPreStart_m1129_MethodInfo,
	&ReporterMessageReceiver_OnHideReporter_m1130_MethodInfo,
	&ReporterMessageReceiver_OnShowReporter_m1131_MethodInfo,
	&ReporterMessageReceiver_OnLog_m1132_MethodInfo,
	NULL
};
static const Il2CppMethodReference ReporterMessageReceiver_t306_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool ReporterMessageReceiver_t306_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType ReporterMessageReceiver_t306_0_0_0;
extern const Il2CppType ReporterMessageReceiver_t306_1_0_0;
struct ReporterMessageReceiver_t306;
const Il2CppTypeDefinitionMetadata ReporterMessageReceiver_t306_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, ReporterMessageReceiver_t306_VTable/* vtableMethods */
	, ReporterMessageReceiver_t306_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 209/* fieldStart */

};
TypeInfo ReporterMessageReceiver_t306_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "ReporterMessageReceiver"/* name */
	, ""/* namespaze */
	, ReporterMessageReceiver_t306_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ReporterMessageReceiver_t306_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ReporterMessageReceiver_t306_0_0_0/* byval_arg */
	, &ReporterMessageReceiver_t306_1_0_0/* this_arg */
	, &ReporterMessageReceiver_t306_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ReporterMessageReceiver_t306)/* instance_size */
	, sizeof (ReporterMessageReceiver_t306)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Rotate
#include "AssemblyU2DCSharp_Rotate.h"
// Metadata Definition Rotate
extern TypeInfo Rotate_t307_il2cpp_TypeInfo;
// Rotate
#include "AssemblyU2DCSharp_RotateMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void Rotate::.ctor()
extern const MethodInfo Rotate__ctor_m1133_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Rotate__ctor_m1133/* method */
	, &Rotate_t307_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 58/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void Rotate::Start()
extern const MethodInfo Rotate_Start_m1134_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&Rotate_Start_m1134/* method */
	, &Rotate_t307_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 59/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void Rotate::Update()
extern const MethodInfo Rotate_Update_m1135_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&Rotate_Update_m1135/* method */
	, &Rotate_t307_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 60/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Rotate_t307_MethodInfos[] =
{
	&Rotate__ctor_m1133_MethodInfo,
	&Rotate_Start_m1134_MethodInfo,
	&Rotate_Update_m1135_MethodInfo,
	NULL
};
static const Il2CppMethodReference Rotate_t307_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool Rotate_t307_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType Rotate_t307_0_0_0;
extern const Il2CppType Rotate_t307_1_0_0;
struct Rotate_t307;
const Il2CppTypeDefinitionMetadata Rotate_t307_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, Rotate_t307_VTable/* vtableMethods */
	, Rotate_t307_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 210/* fieldStart */

};
TypeInfo Rotate_t307_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "Rotate"/* name */
	, ""/* namespaze */
	, Rotate_t307_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Rotate_t307_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Rotate_t307_0_0_0/* byval_arg */
	, &Rotate_t307_1_0_0/* this_arg */
	, &Rotate_t307_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Rotate_t307)/* instance_size */
	, sizeof (Rotate_t307)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// TestReporter
#include "AssemblyU2DCSharp_TestReporter.h"
// Metadata Definition TestReporter
extern TypeInfo TestReporter_t308_il2cpp_TypeInfo;
// TestReporter
#include "AssemblyU2DCSharp_TestReporterMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void TestReporter::.ctor()
extern const MethodInfo TestReporter__ctor_m1136_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TestReporter__ctor_m1136/* method */
	, &TestReporter_t308_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 61/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void TestReporter::Start()
extern const MethodInfo TestReporter_Start_m1137_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&TestReporter_Start_m1137/* method */
	, &TestReporter_t308_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 62/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void TestReporter::threadLogTest()
extern const MethodInfo TestReporter_threadLogTest_m1138_MethodInfo = 
{
	"threadLogTest"/* name */
	, (methodPointerType)&TestReporter_threadLogTest_m1138/* method */
	, &TestReporter_t308_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 63/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void TestReporter::Update()
extern const MethodInfo TestReporter_Update_m1139_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&TestReporter_Update_m1139/* method */
	, &TestReporter_t308_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 64/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void TestReporter::OnGUI()
extern const MethodInfo TestReporter_OnGUI_m1140_MethodInfo = 
{
	"OnGUI"/* name */
	, (methodPointerType)&TestReporter_OnGUI_m1140/* method */
	, &TestReporter_t308_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 65/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TestReporter_t308_MethodInfos[] =
{
	&TestReporter__ctor_m1136_MethodInfo,
	&TestReporter_Start_m1137_MethodInfo,
	&TestReporter_threadLogTest_m1138_MethodInfo,
	&TestReporter_Update_m1139_MethodInfo,
	&TestReporter_OnGUI_m1140_MethodInfo,
	NULL
};
static const Il2CppMethodReference TestReporter_t308_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool TestReporter_t308_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType TestReporter_t308_0_0_0;
extern const Il2CppType TestReporter_t308_1_0_0;
struct TestReporter_t308;
const Il2CppTypeDefinitionMetadata TestReporter_t308_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, TestReporter_t308_VTable/* vtableMethods */
	, TestReporter_t308_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 211/* fieldStart */

};
TypeInfo TestReporter_t308_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "TestReporter"/* name */
	, ""/* namespaze */
	, TestReporter_t308_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TestReporter_t308_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TestReporter_t308_0_0_0/* byval_arg */
	, &TestReporter_t308_1_0_0/* this_arg */
	, &TestReporter_t308_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TestReporter_t308)/* instance_size */
	, sizeof (TestReporter_t308)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 13/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// EventSystemChecker/<Start>c__Iterator1
#include "AssemblyU2DCSharp_EventSystemChecker_U3CStartU3Ec__Iterator1.h"
// Metadata Definition EventSystemChecker/<Start>c__Iterator1
extern TypeInfo U3CStartU3Ec__Iterator1_t310_il2cpp_TypeInfo;
// EventSystemChecker/<Start>c__Iterator1
#include "AssemblyU2DCSharp_EventSystemChecker_U3CStartU3Ec__Iterator1MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void EventSystemChecker/<Start>c__Iterator1::.ctor()
extern const MethodInfo U3CStartU3Ec__Iterator1__ctor_m1141_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CStartU3Ec__Iterator1__ctor_m1141/* method */
	, &U3CStartU3Ec__Iterator1_t310_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 68/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object EventSystemChecker/<Start>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern const MethodInfo U3CStartU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1142_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<object>.get_Current"/* name */
	, (methodPointerType)&U3CStartU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1142/* method */
	, &U3CStartU3Ec__Iterator1_t310_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 12/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 69/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object EventSystemChecker/<Start>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern const MethodInfo U3CStartU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m1143_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&U3CStartU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m1143/* method */
	, &U3CStartU3Ec__Iterator1_t310_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 13/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 70/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean EventSystemChecker/<Start>c__Iterator1::MoveNext()
extern const MethodInfo U3CStartU3Ec__Iterator1_MoveNext_m1144_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&U3CStartU3Ec__Iterator1_MoveNext_m1144/* method */
	, &U3CStartU3Ec__Iterator1_t310_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 71/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void EventSystemChecker/<Start>c__Iterator1::Dispose()
extern const MethodInfo U3CStartU3Ec__Iterator1_Dispose_m1145_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&U3CStartU3Ec__Iterator1_Dispose_m1145/* method */
	, &U3CStartU3Ec__Iterator1_t310_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 14/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 72/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void EventSystemChecker/<Start>c__Iterator1::Reset()
extern const MethodInfo U3CStartU3Ec__Iterator1_Reset_m1146_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&U3CStartU3Ec__Iterator1_Reset_m1146/* method */
	, &U3CStartU3Ec__Iterator1_t310_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 15/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 73/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CStartU3Ec__Iterator1_t310_MethodInfos[] =
{
	&U3CStartU3Ec__Iterator1__ctor_m1141_MethodInfo,
	&U3CStartU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1142_MethodInfo,
	&U3CStartU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m1143_MethodInfo,
	&U3CStartU3Ec__Iterator1_MoveNext_m1144_MethodInfo,
	&U3CStartU3Ec__Iterator1_Dispose_m1145_MethodInfo,
	&U3CStartU3Ec__Iterator1_Reset_m1146_MethodInfo,
	NULL
};
extern const MethodInfo U3CStartU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1142_MethodInfo;
static const PropertyInfo U3CStartU3Ec__Iterator1_t310____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo = 
{
	&U3CStartU3Ec__Iterator1_t310_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<object>.Current"/* name */
	, &U3CStartU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1142_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo U3CStartU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m1143_MethodInfo;
static const PropertyInfo U3CStartU3Ec__Iterator1_t310____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CStartU3Ec__Iterator1_t310_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CStartU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m1143_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* U3CStartU3Ec__Iterator1_t310_PropertyInfos[] =
{
	&U3CStartU3Ec__Iterator1_t310____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo,
	&U3CStartU3Ec__Iterator1_t310____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern const MethodInfo U3CStartU3Ec__Iterator1_Dispose_m1145_MethodInfo;
extern const MethodInfo U3CStartU3Ec__Iterator1_MoveNext_m1144_MethodInfo;
extern const MethodInfo U3CStartU3Ec__Iterator1_Reset_m1146_MethodInfo;
static const Il2CppMethodReference U3CStartU3Ec__Iterator1_t310_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&U3CStartU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1142_MethodInfo,
	&U3CStartU3Ec__Iterator1_Dispose_m1145_MethodInfo,
	&U3CStartU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m1143_MethodInfo,
	&U3CStartU3Ec__Iterator1_MoveNext_m1144_MethodInfo,
	&U3CStartU3Ec__Iterator1_Reset_m1146_MethodInfo,
};
static bool U3CStartU3Ec__Iterator1_t310_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* U3CStartU3Ec__Iterator1_t310_InterfacesTypeInfos[] = 
{
	&IEnumerator_1_t284_0_0_0,
	&IDisposable_t233_0_0_0,
	&IEnumerator_t217_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CStartU3Ec__Iterator1_t310_InterfacesOffsets[] = 
{
	{ &IEnumerator_1_t284_0_0_0, 4},
	{ &IDisposable_t233_0_0_0, 5},
	{ &IEnumerator_t217_0_0_0, 6},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType U3CStartU3Ec__Iterator1_t310_0_0_0;
extern const Il2CppType U3CStartU3Ec__Iterator1_t310_1_0_0;
extern TypeInfo EventSystemChecker_t309_il2cpp_TypeInfo;
extern const Il2CppType EventSystemChecker_t309_0_0_0;
struct U3CStartU3Ec__Iterator1_t310;
const Il2CppTypeDefinitionMetadata U3CStartU3Ec__Iterator1_t310_DefinitionMetadata = 
{
	&EventSystemChecker_t309_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CStartU3Ec__Iterator1_t310_InterfacesTypeInfos/* implementedInterfaces */
	, U3CStartU3Ec__Iterator1_t310_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CStartU3Ec__Iterator1_t310_VTable/* vtableMethods */
	, U3CStartU3Ec__Iterator1_t310_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 224/* fieldStart */

};
TypeInfo U3CStartU3Ec__Iterator1_t310_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<Start>c__Iterator1"/* name */
	, ""/* namespaze */
	, U3CStartU3Ec__Iterator1_t310_MethodInfos/* methods */
	, U3CStartU3Ec__Iterator1_t310_PropertyInfos/* properties */
	, NULL/* events */
	, &U3CStartU3Ec__Iterator1_t310_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 11/* custom_attributes_cache */
	, &U3CStartU3Ec__Iterator1_t310_0_0_0/* byval_arg */
	, &U3CStartU3Ec__Iterator1_t310_1_0_0/* this_arg */
	, &U3CStartU3Ec__Iterator1_t310_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CStartU3Ec__Iterator1_t310)/* instance_size */
	, sizeof (U3CStartU3Ec__Iterator1_t310)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// EventSystemChecker
#include "AssemblyU2DCSharp_EventSystemChecker.h"
// Metadata Definition EventSystemChecker
// EventSystemChecker
#include "AssemblyU2DCSharp_EventSystemCheckerMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void EventSystemChecker::.ctor()
extern const MethodInfo EventSystemChecker__ctor_m1147_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&EventSystemChecker__ctor_m1147/* method */
	, &EventSystemChecker_t309_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 66/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.IEnumerator EventSystemChecker::Start()
extern const MethodInfo EventSystemChecker_Start_m1148_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&EventSystemChecker_Start_m1148/* method */
	, &EventSystemChecker_t309_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t217_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 10/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 67/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* EventSystemChecker_t309_MethodInfos[] =
{
	&EventSystemChecker__ctor_m1147_MethodInfo,
	&EventSystemChecker_Start_m1148_MethodInfo,
	NULL
};
static const Il2CppType* EventSystemChecker_t309_il2cpp_TypeInfo__nestedTypes[1] =
{
	&U3CStartU3Ec__Iterator1_t310_0_0_0,
};
static const Il2CppMethodReference EventSystemChecker_t309_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool EventSystemChecker_t309_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType EventSystemChecker_t309_1_0_0;
struct EventSystemChecker_t309;
const Il2CppTypeDefinitionMetadata EventSystemChecker_t309_DefinitionMetadata = 
{
	NULL/* declaringType */
	, EventSystemChecker_t309_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, EventSystemChecker_t309_VTable/* vtableMethods */
	, EventSystemChecker_t309_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 227/* fieldStart */

};
TypeInfo EventSystemChecker_t309_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "EventSystemChecker"/* name */
	, ""/* namespaze */
	, EventSystemChecker_t309_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &EventSystemChecker_t309_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &EventSystemChecker_t309_0_0_0/* byval_arg */
	, &EventSystemChecker_t309_1_0_0/* this_arg */
	, &EventSystemChecker_t309_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EventSystemChecker_t309)/* instance_size */
	, sizeof (EventSystemChecker_t309)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// MenuSceneLoader
#include "AssemblyU2DCSharp_MenuSceneLoader.h"
// Metadata Definition MenuSceneLoader
extern TypeInfo MenuSceneLoader_t311_il2cpp_TypeInfo;
// MenuSceneLoader
#include "AssemblyU2DCSharp_MenuSceneLoaderMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void MenuSceneLoader::.ctor()
extern const MethodInfo MenuSceneLoader__ctor_m1149_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MenuSceneLoader__ctor_m1149/* method */
	, &MenuSceneLoader_t311_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 74/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void MenuSceneLoader::Awake()
extern const MethodInfo MenuSceneLoader_Awake_m1150_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&MenuSceneLoader_Awake_m1150/* method */
	, &MenuSceneLoader_t311_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 75/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MenuSceneLoader_t311_MethodInfos[] =
{
	&MenuSceneLoader__ctor_m1149_MethodInfo,
	&MenuSceneLoader_Awake_m1150_MethodInfo,
	NULL
};
static const Il2CppMethodReference MenuSceneLoader_t311_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool MenuSceneLoader_t311_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType MenuSceneLoader_t311_0_0_0;
extern const Il2CppType MenuSceneLoader_t311_1_0_0;
struct MenuSceneLoader_t311;
const Il2CppTypeDefinitionMetadata MenuSceneLoader_t311_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, MenuSceneLoader_t311_VTable/* vtableMethods */
	, MenuSceneLoader_t311_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 228/* fieldStart */

};
TypeInfo MenuSceneLoader_t311_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "MenuSceneLoader"/* name */
	, ""/* namespaze */
	, MenuSceneLoader_t311_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &MenuSceneLoader_t311_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MenuSceneLoader_t311_0_0_0/* byval_arg */
	, &MenuSceneLoader_t311_1_0_0/* this_arg */
	, &MenuSceneLoader_t311_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MenuSceneLoader_t311)/* instance_size */
	, sizeof (MenuSceneLoader_t311)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// PauseMenu
#include "AssemblyU2DCSharp_PauseMenu.h"
// Metadata Definition PauseMenu
extern TypeInfo PauseMenu_t313_il2cpp_TypeInfo;
// PauseMenu
#include "AssemblyU2DCSharp_PauseMenuMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void PauseMenu::.ctor()
extern const MethodInfo PauseMenu__ctor_m1151_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PauseMenu__ctor_m1151/* method */
	, &PauseMenu_t313_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 76/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void PauseMenu::Awake()
extern const MethodInfo PauseMenu_Awake_m1152_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&PauseMenu_Awake_m1152/* method */
	, &PauseMenu_t313_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 77/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void PauseMenu::MenuOn()
extern const MethodInfo PauseMenu_MenuOn_m1153_MethodInfo = 
{
	"MenuOn"/* name */
	, (methodPointerType)&PauseMenu_MenuOn_m1153/* method */
	, &PauseMenu_t313_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 78/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void PauseMenu::MenuOff()
extern const MethodInfo PauseMenu_MenuOff_m1154_MethodInfo = 
{
	"MenuOff"/* name */
	, (methodPointerType)&PauseMenu_MenuOff_m1154/* method */
	, &PauseMenu_t313_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 79/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void PauseMenu::OnMenuStatusChange()
extern const MethodInfo PauseMenu_OnMenuStatusChange_m1155_MethodInfo = 
{
	"OnMenuStatusChange"/* name */
	, (methodPointerType)&PauseMenu_OnMenuStatusChange_m1155/* method */
	, &PauseMenu_t313_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 80/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* PauseMenu_t313_MethodInfos[] =
{
	&PauseMenu__ctor_m1151_MethodInfo,
	&PauseMenu_Awake_m1152_MethodInfo,
	&PauseMenu_MenuOn_m1153_MethodInfo,
	&PauseMenu_MenuOff_m1154_MethodInfo,
	&PauseMenu_OnMenuStatusChange_m1155_MethodInfo,
	NULL
};
static const Il2CppMethodReference PauseMenu_t313_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool PauseMenu_t313_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType PauseMenu_t313_0_0_0;
extern const Il2CppType PauseMenu_t313_1_0_0;
struct PauseMenu_t313;
const Il2CppTypeDefinitionMetadata PauseMenu_t313_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, PauseMenu_t313_VTable/* vtableMethods */
	, PauseMenu_t313_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 230/* fieldStart */

};
TypeInfo PauseMenu_t313_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "PauseMenu"/* name */
	, ""/* namespaze */
	, PauseMenu_t313_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &PauseMenu_t313_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PauseMenu_t313_0_0_0/* byval_arg */
	, &PauseMenu_t313_1_0_0/* this_arg */
	, &PauseMenu_t313_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PauseMenu_t313)/* instance_size */
	, sizeof (PauseMenu_t313)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// ReturnToMainMenu
#include "AssemblyU2DCSharp_ReturnToMainMenu.h"
// Metadata Definition ReturnToMainMenu
extern TypeInfo ReturnToMainMenu_t314_il2cpp_TypeInfo;
// ReturnToMainMenu
#include "AssemblyU2DCSharp_ReturnToMainMenuMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void ReturnToMainMenu::.ctor()
extern const MethodInfo ReturnToMainMenu__ctor_m1156_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ReturnToMainMenu__ctor_m1156/* method */
	, &ReturnToMainMenu_t314_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 81/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void ReturnToMainMenu::Start()
extern const MethodInfo ReturnToMainMenu_Start_m1157_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&ReturnToMainMenu_Start_m1157/* method */
	, &ReturnToMainMenu_t314_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 82/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo ReturnToMainMenu_t314_ReturnToMainMenu_OnLevelWasLoaded_m1158_ParameterInfos[] = 
{
	{"level", 0, 134217742, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void ReturnToMainMenu::OnLevelWasLoaded(System.Int32)
extern const MethodInfo ReturnToMainMenu_OnLevelWasLoaded_m1158_MethodInfo = 
{
	"OnLevelWasLoaded"/* name */
	, (methodPointerType)&ReturnToMainMenu_OnLevelWasLoaded_m1158/* method */
	, &ReturnToMainMenu_t314_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, ReturnToMainMenu_t314_ReturnToMainMenu_OnLevelWasLoaded_m1158_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 83/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void ReturnToMainMenu::Update()
extern const MethodInfo ReturnToMainMenu_Update_m1159_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&ReturnToMainMenu_Update_m1159/* method */
	, &ReturnToMainMenu_t314_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 84/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void ReturnToMainMenu::GoBackToMainMenu()
extern const MethodInfo ReturnToMainMenu_GoBackToMainMenu_m1160_MethodInfo = 
{
	"GoBackToMainMenu"/* name */
	, (methodPointerType)&ReturnToMainMenu_GoBackToMainMenu_m1160/* method */
	, &ReturnToMainMenu_t314_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 85/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ReturnToMainMenu_t314_MethodInfos[] =
{
	&ReturnToMainMenu__ctor_m1156_MethodInfo,
	&ReturnToMainMenu_Start_m1157_MethodInfo,
	&ReturnToMainMenu_OnLevelWasLoaded_m1158_MethodInfo,
	&ReturnToMainMenu_Update_m1159_MethodInfo,
	&ReturnToMainMenu_GoBackToMainMenu_m1160_MethodInfo,
	NULL
};
static const Il2CppMethodReference ReturnToMainMenu_t314_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool ReturnToMainMenu_t314_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType ReturnToMainMenu_t314_0_0_0;
extern const Il2CppType ReturnToMainMenu_t314_1_0_0;
struct ReturnToMainMenu_t314;
const Il2CppTypeDefinitionMetadata ReturnToMainMenu_t314_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, ReturnToMainMenu_t314_VTable/* vtableMethods */
	, ReturnToMainMenu_t314_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 234/* fieldStart */

};
TypeInfo ReturnToMainMenu_t314_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "ReturnToMainMenu"/* name */
	, ""/* namespaze */
	, ReturnToMainMenu_t314_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ReturnToMainMenu_t314_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ReturnToMainMenu_t314_0_0_0/* byval_arg */
	, &ReturnToMainMenu_t314_1_0_0/* this_arg */
	, &ReturnToMainMenu_t314_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ReturnToMainMenu_t314)/* instance_size */
	, sizeof (ReturnToMainMenu_t314)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SceneAndURLLoader
#include "AssemblyU2DCSharp_SceneAndURLLoader.h"
// Metadata Definition SceneAndURLLoader
extern TypeInfo SceneAndURLLoader_t315_il2cpp_TypeInfo;
// SceneAndURLLoader
#include "AssemblyU2DCSharp_SceneAndURLLoaderMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SceneAndURLLoader::.ctor()
extern const MethodInfo SceneAndURLLoader__ctor_m1161_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SceneAndURLLoader__ctor_m1161/* method */
	, &SceneAndURLLoader_t315_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 86/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SceneAndURLLoader::Awake()
extern const MethodInfo SceneAndURLLoader_Awake_m1162_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&SceneAndURLLoader_Awake_m1162/* method */
	, &SceneAndURLLoader_t315_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 87/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo SceneAndURLLoader_t315_SceneAndURLLoader_SceneLoad_m1163_ParameterInfos[] = 
{
	{"sceneName", 0, 134217743, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void SceneAndURLLoader::SceneLoad(System.String)
extern const MethodInfo SceneAndURLLoader_SceneLoad_m1163_MethodInfo = 
{
	"SceneLoad"/* name */
	, (methodPointerType)&SceneAndURLLoader_SceneLoad_m1163/* method */
	, &SceneAndURLLoader_t315_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, SceneAndURLLoader_t315_SceneAndURLLoader_SceneLoad_m1163_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 88/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo SceneAndURLLoader_t315_SceneAndURLLoader_LoadURL_m1164_ParameterInfos[] = 
{
	{"url", 0, 134217744, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void SceneAndURLLoader::LoadURL(System.String)
extern const MethodInfo SceneAndURLLoader_LoadURL_m1164_MethodInfo = 
{
	"LoadURL"/* name */
	, (methodPointerType)&SceneAndURLLoader_LoadURL_m1164/* method */
	, &SceneAndURLLoader_t315_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, SceneAndURLLoader_t315_SceneAndURLLoader_LoadURL_m1164_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 89/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SceneAndURLLoader_t315_MethodInfos[] =
{
	&SceneAndURLLoader__ctor_m1161_MethodInfo,
	&SceneAndURLLoader_Awake_m1162_MethodInfo,
	&SceneAndURLLoader_SceneLoad_m1163_MethodInfo,
	&SceneAndURLLoader_LoadURL_m1164_MethodInfo,
	NULL
};
static const Il2CppMethodReference SceneAndURLLoader_t315_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool SceneAndURLLoader_t315_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType SceneAndURLLoader_t315_0_0_0;
extern const Il2CppType SceneAndURLLoader_t315_1_0_0;
struct SceneAndURLLoader_t315;
const Il2CppTypeDefinitionMetadata SceneAndURLLoader_t315_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, SceneAndURLLoader_t315_VTable/* vtableMethods */
	, SceneAndURLLoader_t315_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 235/* fieldStart */

};
TypeInfo SceneAndURLLoader_t315_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "SceneAndURLLoader"/* name */
	, ""/* namespaze */
	, SceneAndURLLoader_t315_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SceneAndURLLoader_t315_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SceneAndURLLoader_t315_0_0_0/* byval_arg */
	, &SceneAndURLLoader_t315_1_0_0/* this_arg */
	, &SceneAndURLLoader_t315_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SceneAndURLLoader_t315)/* instance_size */
	, sizeof (SceneAndURLLoader_t315)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraSwitch
#include "AssemblyU2DCSharp_CameraSwitch.h"
// Metadata Definition CameraSwitch
extern TypeInfo CameraSwitch_t317_il2cpp_TypeInfo;
// CameraSwitch
#include "AssemblyU2DCSharp_CameraSwitchMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void CameraSwitch::.ctor()
extern const MethodInfo CameraSwitch__ctor_m1165_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CameraSwitch__ctor_m1165/* method */
	, &CameraSwitch_t317_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 90/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void CameraSwitch::OnEnable()
extern const MethodInfo CameraSwitch_OnEnable_m1166_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&CameraSwitch_OnEnable_m1166/* method */
	, &CameraSwitch_t317_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 91/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void CameraSwitch::NextCamera()
extern const MethodInfo CameraSwitch_NextCamera_m1167_MethodInfo = 
{
	"NextCamera"/* name */
	, (methodPointerType)&CameraSwitch_NextCamera_m1167/* method */
	, &CameraSwitch_t317_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 92/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CameraSwitch_t317_MethodInfos[] =
{
	&CameraSwitch__ctor_m1165_MethodInfo,
	&CameraSwitch_OnEnable_m1166_MethodInfo,
	&CameraSwitch_NextCamera_m1167_MethodInfo,
	NULL
};
static const Il2CppMethodReference CameraSwitch_t317_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool CameraSwitch_t317_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraSwitch_t317_0_0_0;
extern const Il2CppType CameraSwitch_t317_1_0_0;
struct CameraSwitch_t317;
const Il2CppTypeDefinitionMetadata CameraSwitch_t317_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, CameraSwitch_t317_VTable/* vtableMethods */
	, CameraSwitch_t317_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 236/* fieldStart */

};
TypeInfo CameraSwitch_t317_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraSwitch"/* name */
	, ""/* namespaze */
	, CameraSwitch_t317_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CameraSwitch_t317_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CameraSwitch_t317_0_0_0/* byval_arg */
	, &CameraSwitch_t317_1_0_0/* this_arg */
	, &CameraSwitch_t317_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraSwitch_t317)/* instance_size */
	, sizeof (CameraSwitch_t317)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// LevelReset
#include "AssemblyU2DCSharp_LevelReset.h"
// Metadata Definition LevelReset
extern TypeInfo LevelReset_t318_il2cpp_TypeInfo;
// LevelReset
#include "AssemblyU2DCSharp_LevelResetMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void LevelReset::.ctor()
extern const MethodInfo LevelReset__ctor_m1168_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&LevelReset__ctor_m1168/* method */
	, &LevelReset_t318_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 93/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t215_0_0_0;
extern const Il2CppType PointerEventData_t215_0_0_0;
static const ParameterInfo LevelReset_t318_LevelReset_OnPointerClick_m1169_ParameterInfos[] = 
{
	{"data", 0, 134217745, 0, &PointerEventData_t215_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void LevelReset::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern const MethodInfo LevelReset_OnPointerClick_m1169_MethodInfo = 
{
	"OnPointerClick"/* name */
	, (methodPointerType)&LevelReset_OnPointerClick_m1169/* method */
	, &LevelReset_t318_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, LevelReset_t318_LevelReset_OnPointerClick_m1169_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 94/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void LevelReset::Update()
extern const MethodInfo LevelReset_Update_m1170_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&LevelReset_Update_m1170/* method */
	, &LevelReset_t318_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 95/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* LevelReset_t318_MethodInfos[] =
{
	&LevelReset__ctor_m1168_MethodInfo,
	&LevelReset_OnPointerClick_m1169_MethodInfo,
	&LevelReset_Update_m1170_MethodInfo,
	NULL
};
extern const MethodInfo LevelReset_OnPointerClick_m1169_MethodInfo;
static const Il2CppMethodReference LevelReset_t318_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	&LevelReset_OnPointerClick_m1169_MethodInfo,
};
static bool LevelReset_t318_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IPointerClickHandler_t448_0_0_0;
extern const Il2CppType IEventSystemHandler_t281_0_0_0;
static const Il2CppType* LevelReset_t318_InterfacesTypeInfos[] = 
{
	&IPointerClickHandler_t448_0_0_0,
	&IEventSystemHandler_t281_0_0_0,
};
static Il2CppInterfaceOffsetPair LevelReset_t318_InterfacesOffsets[] = 
{
	{ &IPointerClickHandler_t448_0_0_0, 4},
	{ &IEventSystemHandler_t281_0_0_0, 5},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType LevelReset_t318_0_0_0;
extern const Il2CppType LevelReset_t318_1_0_0;
struct LevelReset_t318;
const Il2CppTypeDefinitionMetadata LevelReset_t318_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, LevelReset_t318_InterfacesTypeInfos/* implementedInterfaces */
	, LevelReset_t318_InterfacesOffsets/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, LevelReset_t318_VTable/* vtableMethods */
	, LevelReset_t318_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo LevelReset_t318_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "LevelReset"/* name */
	, ""/* namespaze */
	, LevelReset_t318_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &LevelReset_t318_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &LevelReset_t318_0_0_0/* byval_arg */
	, &LevelReset_t318_1_0_0/* this_arg */
	, &LevelReset_t318_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LevelReset_t318)/* instance_size */
	, sizeof (LevelReset_t318)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityStandardAssets.SceneUtils.ParticleSceneControls/Mode
#include "AssemblyU2DCSharp_UnityStandardAssets_SceneUtils_ParticleSce.h"
// Metadata Definition UnityStandardAssets.SceneUtils.ParticleSceneControls/Mode
extern TypeInfo Mode_t319_il2cpp_TypeInfo;
// UnityStandardAssets.SceneUtils.ParticleSceneControls/Mode
#include "AssemblyU2DCSharp_UnityStandardAssets_SceneUtils_ParticleSceMethodDeclarations.h"
static const MethodInfo* Mode_t319_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference Mode_t319_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool Mode_t319_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Mode_t319_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType Mode_t319_0_0_0;
extern const Il2CppType Mode_t319_1_0_0;
extern TypeInfo ParticleSceneControls_t327_il2cpp_TypeInfo;
extern const Il2CppType ParticleSceneControls_t327_0_0_0;
const Il2CppTypeDefinitionMetadata Mode_t319_DefinitionMetadata = 
{
	&ParticleSceneControls_t327_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Mode_t319_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, Mode_t319_VTable/* vtableMethods */
	, Mode_t319_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 239/* fieldStart */

};
TypeInfo Mode_t319_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "Mode"/* name */
	, ""/* namespaze */
	, Mode_t319_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Mode_t319_0_0_0/* byval_arg */
	, &Mode_t319_1_0_0/* this_arg */
	, &Mode_t319_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Mode_t319)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Mode_t319)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityStandardAssets.SceneUtils.ParticleSceneControls/AlignMode
#include "AssemblyU2DCSharp_UnityStandardAssets_SceneUtils_ParticleSce_0.h"
// Metadata Definition UnityStandardAssets.SceneUtils.ParticleSceneControls/AlignMode
extern TypeInfo AlignMode_t320_il2cpp_TypeInfo;
// UnityStandardAssets.SceneUtils.ParticleSceneControls/AlignMode
#include "AssemblyU2DCSharp_UnityStandardAssets_SceneUtils_ParticleSce_0MethodDeclarations.h"
static const MethodInfo* AlignMode_t320_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference AlignMode_t320_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool AlignMode_t320_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair AlignMode_t320_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType AlignMode_t320_0_0_0;
extern const Il2CppType AlignMode_t320_1_0_0;
const Il2CppTypeDefinitionMetadata AlignMode_t320_DefinitionMetadata = 
{
	&ParticleSceneControls_t327_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AlignMode_t320_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, AlignMode_t320_VTable/* vtableMethods */
	, AlignMode_t320_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 243/* fieldStart */

};
TypeInfo AlignMode_t320_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "AlignMode"/* name */
	, ""/* namespaze */
	, AlignMode_t320_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AlignMode_t320_0_0_0/* byval_arg */
	, &AlignMode_t320_1_0_0/* this_arg */
	, &AlignMode_t320_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AlignMode_t320)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (AlignMode_t320)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystem
#include "AssemblyU2DCSharp_UnityStandardAssets_SceneUtils_ParticleSce_1.h"
// Metadata Definition UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystem
extern TypeInfo DemoParticleSystem_t321_il2cpp_TypeInfo;
// UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystem
#include "AssemblyU2DCSharp_UnityStandardAssets_SceneUtils_ParticleSce_1MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystem::.ctor()
extern const MethodInfo DemoParticleSystem__ctor_m1171_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DemoParticleSystem__ctor_m1171/* method */
	, &DemoParticleSystem_t321_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 105/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* DemoParticleSystem_t321_MethodInfos[] =
{
	&DemoParticleSystem__ctor_m1171_MethodInfo,
	NULL
};
static const Il2CppMethodReference DemoParticleSystem_t321_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool DemoParticleSystem_t321_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType DemoParticleSystem_t321_0_0_0;
extern const Il2CppType DemoParticleSystem_t321_1_0_0;
struct DemoParticleSystem_t321;
const Il2CppTypeDefinitionMetadata DemoParticleSystem_t321_DefinitionMetadata = 
{
	&ParticleSceneControls_t327_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, DemoParticleSystem_t321_VTable/* vtableMethods */
	, DemoParticleSystem_t321_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 246/* fieldStart */

};
TypeInfo DemoParticleSystem_t321_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "DemoParticleSystem"/* name */
	, ""/* namespaze */
	, DemoParticleSystem_t321_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &DemoParticleSystem_t321_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DemoParticleSystem_t321_0_0_0/* byval_arg */
	, &DemoParticleSystem_t321_1_0_0/* this_arg */
	, &DemoParticleSystem_t321_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DemoParticleSystem_t321)/* instance_size */
	, sizeof (DemoParticleSystem_t321)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056770/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystemList
#include "AssemblyU2DCSharp_UnityStandardAssets_SceneUtils_ParticleSce_2.h"
// Metadata Definition UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystemList
extern TypeInfo DemoParticleSystemList_t323_il2cpp_TypeInfo;
// UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystemList
#include "AssemblyU2DCSharp_UnityStandardAssets_SceneUtils_ParticleSce_2MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystemList::.ctor()
extern const MethodInfo DemoParticleSystemList__ctor_m1172_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DemoParticleSystemList__ctor_m1172/* method */
	, &DemoParticleSystemList_t323_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 106/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* DemoParticleSystemList_t323_MethodInfos[] =
{
	&DemoParticleSystemList__ctor_m1172_MethodInfo,
	NULL
};
static const Il2CppMethodReference DemoParticleSystemList_t323_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool DemoParticleSystemList_t323_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType DemoParticleSystemList_t323_0_0_0;
extern const Il2CppType DemoParticleSystemList_t323_1_0_0;
struct DemoParticleSystemList_t323;
const Il2CppTypeDefinitionMetadata DemoParticleSystemList_t323_DefinitionMetadata = 
{
	&ParticleSceneControls_t327_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, DemoParticleSystemList_t323_VTable/* vtableMethods */
	, DemoParticleSystemList_t323_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 253/* fieldStart */

};
TypeInfo DemoParticleSystemList_t323_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "DemoParticleSystemList"/* name */
	, ""/* namespaze */
	, DemoParticleSystemList_t323_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &DemoParticleSystemList_t323_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DemoParticleSystemList_t323_0_0_0/* byval_arg */
	, &DemoParticleSystemList_t323_1_0_0/* this_arg */
	, &DemoParticleSystemList_t323_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DemoParticleSystemList_t323)/* instance_size */
	, sizeof (DemoParticleSystemList_t323)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056770/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.SceneUtils.ParticleSceneControls
#include "AssemblyU2DCSharp_UnityStandardAssets_SceneUtils_ParticleSce_3.h"
// Metadata Definition UnityStandardAssets.SceneUtils.ParticleSceneControls
// UnityStandardAssets.SceneUtils.ParticleSceneControls
#include "AssemblyU2DCSharp_UnityStandardAssets_SceneUtils_ParticleSce_3MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls::.ctor()
extern const MethodInfo ParticleSceneControls__ctor_m1173_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ParticleSceneControls__ctor_m1173/* method */
	, &ParticleSceneControls_t327_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 96/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls::.cctor()
extern const MethodInfo ParticleSceneControls__cctor_m1174_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&ParticleSceneControls__cctor_m1174/* method */
	, &ParticleSceneControls_t327_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 97/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls::Awake()
extern const MethodInfo ParticleSceneControls_Awake_m1175_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&ParticleSceneControls_Awake_m1175/* method */
	, &ParticleSceneControls_t327_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 98/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls::OnDisable()
extern const MethodInfo ParticleSceneControls_OnDisable_m1176_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&ParticleSceneControls_OnDisable_m1176/* method */
	, &ParticleSceneControls_t327_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 99/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls::Previous()
extern const MethodInfo ParticleSceneControls_Previous_m1177_MethodInfo = 
{
	"Previous"/* name */
	, (methodPointerType)&ParticleSceneControls_Previous_m1177/* method */
	, &ParticleSceneControls_t327_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 100/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls::Next()
extern const MethodInfo ParticleSceneControls_Next_m1178_MethodInfo = 
{
	"Next"/* name */
	, (methodPointerType)&ParticleSceneControls_Next_m1178/* method */
	, &ParticleSceneControls_t327_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 101/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls::Update()
extern const MethodInfo ParticleSceneControls_Update_m1179_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&ParticleSceneControls_Update_m1179/* method */
	, &ParticleSceneControls_t327_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 102/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityStandardAssets.SceneUtils.ParticleSceneControls::CheckForGuiCollision()
extern const MethodInfo ParticleSceneControls_CheckForGuiCollision_m1180_MethodInfo = 
{
	"CheckForGuiCollision"/* name */
	, (methodPointerType)&ParticleSceneControls_CheckForGuiCollision_m1180/* method */
	, &ParticleSceneControls_t327_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 103/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo ParticleSceneControls_t327_ParticleSceneControls_Select_m1181_ParameterInfos[] = 
{
	{"i", 0, 134217746, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls::Select(System.Int32)
extern const MethodInfo ParticleSceneControls_Select_m1181_MethodInfo = 
{
	"Select"/* name */
	, (methodPointerType)&ParticleSceneControls_Select_m1181/* method */
	, &ParticleSceneControls_t327_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, ParticleSceneControls_t327_ParticleSceneControls_Select_m1181_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 104/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ParticleSceneControls_t327_MethodInfos[] =
{
	&ParticleSceneControls__ctor_m1173_MethodInfo,
	&ParticleSceneControls__cctor_m1174_MethodInfo,
	&ParticleSceneControls_Awake_m1175_MethodInfo,
	&ParticleSceneControls_OnDisable_m1176_MethodInfo,
	&ParticleSceneControls_Previous_m1177_MethodInfo,
	&ParticleSceneControls_Next_m1178_MethodInfo,
	&ParticleSceneControls_Update_m1179_MethodInfo,
	&ParticleSceneControls_CheckForGuiCollision_m1180_MethodInfo,
	&ParticleSceneControls_Select_m1181_MethodInfo,
	NULL
};
static const Il2CppType* ParticleSceneControls_t327_il2cpp_TypeInfo__nestedTypes[4] =
{
	&Mode_t319_0_0_0,
	&AlignMode_t320_0_0_0,
	&DemoParticleSystem_t321_0_0_0,
	&DemoParticleSystemList_t323_0_0_0,
};
static const Il2CppMethodReference ParticleSceneControls_t327_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool ParticleSceneControls_t327_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType ParticleSceneControls_t327_1_0_0;
struct ParticleSceneControls_t327;
const Il2CppTypeDefinitionMetadata ParticleSceneControls_t327_DefinitionMetadata = 
{
	NULL/* declaringType */
	, ParticleSceneControls_t327_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, ParticleSceneControls_t327_VTable/* vtableMethods */
	, ParticleSceneControls_t327_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 254/* fieldStart */

};
TypeInfo ParticleSceneControls_t327_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "ParticleSceneControls"/* name */
	, "UnityStandardAssets.SceneUtils"/* namespaze */
	, ParticleSceneControls_t327_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ParticleSceneControls_t327_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ParticleSceneControls_t327_0_0_0/* byval_arg */
	, &ParticleSceneControls_t327_1_0_0/* this_arg */
	, &ParticleSceneControls_t327_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ParticleSceneControls_t327)/* instance_size */
	, sizeof (ParticleSceneControls_t327)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ParticleSceneControls_t327_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 0/* property_count */
	, 18/* field_count */
	, 0/* event_count */
	, 4/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.SceneUtils.PlaceTargetWithMouse
#include "AssemblyU2DCSharp_UnityStandardAssets_SceneUtils_PlaceTarget.h"
// Metadata Definition UnityStandardAssets.SceneUtils.PlaceTargetWithMouse
extern TypeInfo PlaceTargetWithMouse_t328_il2cpp_TypeInfo;
// UnityStandardAssets.SceneUtils.PlaceTargetWithMouse
#include "AssemblyU2DCSharp_UnityStandardAssets_SceneUtils_PlaceTargetMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.SceneUtils.PlaceTargetWithMouse::.ctor()
extern const MethodInfo PlaceTargetWithMouse__ctor_m1182_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PlaceTargetWithMouse__ctor_m1182/* method */
	, &PlaceTargetWithMouse_t328_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 107/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.SceneUtils.PlaceTargetWithMouse::Update()
extern const MethodInfo PlaceTargetWithMouse_Update_m1183_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&PlaceTargetWithMouse_Update_m1183/* method */
	, &PlaceTargetWithMouse_t328_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 108/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* PlaceTargetWithMouse_t328_MethodInfos[] =
{
	&PlaceTargetWithMouse__ctor_m1182_MethodInfo,
	&PlaceTargetWithMouse_Update_m1183_MethodInfo,
	NULL
};
static const Il2CppMethodReference PlaceTargetWithMouse_t328_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool PlaceTargetWithMouse_t328_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType PlaceTargetWithMouse_t328_0_0_0;
extern const Il2CppType PlaceTargetWithMouse_t328_1_0_0;
struct PlaceTargetWithMouse_t328;
const Il2CppTypeDefinitionMetadata PlaceTargetWithMouse_t328_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, PlaceTargetWithMouse_t328_VTable/* vtableMethods */
	, PlaceTargetWithMouse_t328_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 272/* fieldStart */

};
TypeInfo PlaceTargetWithMouse_t328_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "PlaceTargetWithMouse"/* name */
	, "UnityStandardAssets.SceneUtils"/* namespaze */
	, PlaceTargetWithMouse_t328_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &PlaceTargetWithMouse_t328_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PlaceTargetWithMouse_t328_0_0_0/* byval_arg */
	, &PlaceTargetWithMouse_t328_1_0_0/* this_arg */
	, &PlaceTargetWithMouse_t328_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PlaceTargetWithMouse_t328)/* instance_size */
	, sizeof (PlaceTargetWithMouse_t328)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.SceneUtils.SlowMoButton
#include "AssemblyU2DCSharp_UnityStandardAssets_SceneUtils_SlowMoButto.h"
// Metadata Definition UnityStandardAssets.SceneUtils.SlowMoButton
extern TypeInfo SlowMoButton_t330_il2cpp_TypeInfo;
// UnityStandardAssets.SceneUtils.SlowMoButton
#include "AssemblyU2DCSharp_UnityStandardAssets_SceneUtils_SlowMoButtoMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.SceneUtils.SlowMoButton::.ctor()
extern const MethodInfo SlowMoButton__ctor_m1184_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SlowMoButton__ctor_m1184/* method */
	, &SlowMoButton_t330_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 109/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.SceneUtils.SlowMoButton::Start()
extern const MethodInfo SlowMoButton_Start_m1185_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&SlowMoButton_Start_m1185/* method */
	, &SlowMoButton_t330_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 110/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.SceneUtils.SlowMoButton::OnDestroy()
extern const MethodInfo SlowMoButton_OnDestroy_m1186_MethodInfo = 
{
	"OnDestroy"/* name */
	, (methodPointerType)&SlowMoButton_OnDestroy_m1186/* method */
	, &SlowMoButton_t330_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 111/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.SceneUtils.SlowMoButton::ChangeSpeed()
extern const MethodInfo SlowMoButton_ChangeSpeed_m1187_MethodInfo = 
{
	"ChangeSpeed"/* name */
	, (methodPointerType)&SlowMoButton_ChangeSpeed_m1187/* method */
	, &SlowMoButton_t330_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 112/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SlowMoButton_t330_MethodInfos[] =
{
	&SlowMoButton__ctor_m1184_MethodInfo,
	&SlowMoButton_Start_m1185_MethodInfo,
	&SlowMoButton_OnDestroy_m1186_MethodInfo,
	&SlowMoButton_ChangeSpeed_m1187_MethodInfo,
	NULL
};
static const Il2CppMethodReference SlowMoButton_t330_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool SlowMoButton_t330_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType SlowMoButton_t330_0_0_0;
extern const Il2CppType SlowMoButton_t330_1_0_0;
struct SlowMoButton_t330;
const Il2CppTypeDefinitionMetadata SlowMoButton_t330_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, SlowMoButton_t330_VTable/* vtableMethods */
	, SlowMoButton_t330_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 274/* fieldStart */

};
TypeInfo SlowMoButton_t330_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "SlowMoButton"/* name */
	, "UnityStandardAssets.SceneUtils"/* namespaze */
	, SlowMoButton_t330_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SlowMoButton_t330_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SlowMoButton_t330_0_0_0/* byval_arg */
	, &SlowMoButton_t330_1_0_0/* this_arg */
	, &SlowMoButton_t330_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SlowMoButton_t330)/* instance_size */
	, sizeof (SlowMoButton_t330)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition IFollowWaypoints
extern TypeInfo IFollowWaypoints_t433_il2cpp_TypeInfo;
extern const Il2CppType SCR_WaypointU5BU5D_t361_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// SCR_Waypoint[] IFollowWaypoints::GetWaypoints()
extern const MethodInfo IFollowWaypoints_GetWaypoints_m1836_MethodInfo = 
{
	"GetWaypoints"/* name */
	, NULL/* method */
	, &IFollowWaypoints_t433_il2cpp_TypeInfo/* declaring_type */
	, &SCR_WaypointU5BU5D_t361_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 113/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SCR_WaypointU5BU5D_t361_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo IFollowWaypoints_t433_IFollowWaypoints_SortWaypoints_m1837_ParameterInfos[] = 
{
	{"unorderedArray", 0, 134217747, 0, &SCR_WaypointU5BU5D_t361_0_0_0},
	{"isInit", 1, 134217748, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// SCR_Waypoint[] IFollowWaypoints::SortWaypoints(SCR_Waypoint[],System.Boolean)
extern const MethodInfo IFollowWaypoints_SortWaypoints_m1837_MethodInfo = 
{
	"SortWaypoints"/* name */
	, NULL/* method */
	, &IFollowWaypoints_t433_il2cpp_TypeInfo/* declaring_type */
	, &SCR_WaypointU5BU5D_t361_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_SByte_t274/* invoker_method */
	, IFollowWaypoints_t433_IFollowWaypoints_SortWaypoints_m1837_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 114/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void IFollowWaypoints::UnChildWaypoints()
extern const MethodInfo IFollowWaypoints_UnChildWaypoints_m1838_MethodInfo = 
{
	"UnChildWaypoints"/* name */
	, NULL/* method */
	, &IFollowWaypoints_t433_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 115/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SCR_Waypoint_t358_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// SCR_Waypoint IFollowWaypoints::GetNextWaypoint()
extern const MethodInfo IFollowWaypoints_GetNextWaypoint_m1839_MethodInfo = 
{
	"GetNextWaypoint"/* name */
	, NULL/* method */
	, &IFollowWaypoints_t433_il2cpp_TypeInfo/* declaring_type */
	, &SCR_Waypoint_t358_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 116/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SCR_Waypoint_t358_0_0_0;
static const ParameterInfo IFollowWaypoints_t433_IFollowWaypoints_MoveToWaypoint_m1840_ParameterInfos[] = 
{
	{"wp", 0, 134217749, 0, &SCR_Waypoint_t358_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.IEnumerator IFollowWaypoints::MoveToWaypoint(SCR_Waypoint)
extern const MethodInfo IFollowWaypoints_MoveToWaypoint_m1840_MethodInfo = 
{
	"MoveToWaypoint"/* name */
	, NULL/* method */
	, &IFollowWaypoints_t433_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t217_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, IFollowWaypoints_t433_IFollowWaypoints_MoveToWaypoint_m1840_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 117/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void IFollowWaypoints::StartMove()
extern const MethodInfo IFollowWaypoints_StartMove_m1841_MethodInfo = 
{
	"StartMove"/* name */
	, NULL/* method */
	, &IFollowWaypoints_t433_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 118/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void IFollowWaypoints::StopMove()
extern const MethodInfo IFollowWaypoints_StopMove_m1842_MethodInfo = 
{
	"StopMove"/* name */
	, NULL/* method */
	, &IFollowWaypoints_t433_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 119/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IFollowWaypoints_t433_MethodInfos[] =
{
	&IFollowWaypoints_GetWaypoints_m1836_MethodInfo,
	&IFollowWaypoints_SortWaypoints_m1837_MethodInfo,
	&IFollowWaypoints_UnChildWaypoints_m1838_MethodInfo,
	&IFollowWaypoints_GetNextWaypoint_m1839_MethodInfo,
	&IFollowWaypoints_MoveToWaypoint_m1840_MethodInfo,
	&IFollowWaypoints_StartMove_m1841_MethodInfo,
	&IFollowWaypoints_StopMove_m1842_MethodInfo,
	NULL
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType IFollowWaypoints_t433_0_0_0;
extern const Il2CppType IFollowWaypoints_t433_1_0_0;
struct IFollowWaypoints_t433;
const Il2CppTypeDefinitionMetadata IFollowWaypoints_t433_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IFollowWaypoints_t433_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "IFollowWaypoints"/* name */
	, ""/* namespaze */
	, IFollowWaypoints_t433_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IFollowWaypoints_t433_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IFollowWaypoints_t433_0_0_0/* byval_arg */
	, &IFollowWaypoints_t433_1_0_0/* this_arg */
	, &IFollowWaypoints_t433_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 160/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SCR_Collectible
#include "AssemblyU2DCSharp_SCR_Collectible.h"
// Metadata Definition SCR_Collectible
extern TypeInfo SCR_Collectible_t331_il2cpp_TypeInfo;
// SCR_Collectible
#include "AssemblyU2DCSharp_SCR_CollectibleMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Collectible::.ctor()
extern const MethodInfo SCR_Collectible__ctor_m1188_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SCR_Collectible__ctor_m1188/* method */
	, &SCR_Collectible_t331_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 120/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Collectible::Collect()
extern const MethodInfo SCR_Collectible_Collect_m1843_MethodInfo = 
{
	"Collect"/* name */
	, NULL/* method */
	, &SCR_Collectible_t331_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1476/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 121/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 SCR_Collectible::get_PointValue()
extern const MethodInfo SCR_Collectible_get_PointValue_m1189_MethodInfo = 
{
	"get_PointValue"/* name */
	, (methodPointerType)&SCR_Collectible_get_PointValue_m1189/* method */
	, &SCR_Collectible_t331_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 122/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean SCR_Collectible::get_IsTaken()
extern const MethodInfo SCR_Collectible_get_IsTaken_m1190_MethodInfo = 
{
	"get_IsTaken"/* name */
	, (methodPointerType)&SCR_Collectible_get_IsTaken_m1190/* method */
	, &SCR_Collectible_t331_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 123/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo SCR_Collectible_t331_SCR_Collectible_set_IsTaken_m1191_ParameterInfos[] = 
{
	{"value", 0, 134217750, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Collectible::set_IsTaken(System.Boolean)
extern const MethodInfo SCR_Collectible_set_IsTaken_m1191_MethodInfo = 
{
	"set_IsTaken"/* name */
	, (methodPointerType)&SCR_Collectible_set_IsTaken_m1191/* method */
	, &SCR_Collectible_t331_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274/* invoker_method */
	, SCR_Collectible_t331_SCR_Collectible_set_IsTaken_m1191_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 124/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SCR_Collectible_t331_MethodInfos[] =
{
	&SCR_Collectible__ctor_m1188_MethodInfo,
	&SCR_Collectible_Collect_m1843_MethodInfo,
	&SCR_Collectible_get_PointValue_m1189_MethodInfo,
	&SCR_Collectible_get_IsTaken_m1190_MethodInfo,
	&SCR_Collectible_set_IsTaken_m1191_MethodInfo,
	NULL
};
extern const MethodInfo SCR_Collectible_get_PointValue_m1189_MethodInfo;
static const PropertyInfo SCR_Collectible_t331____PointValue_PropertyInfo = 
{
	&SCR_Collectible_t331_il2cpp_TypeInfo/* parent */
	, "PointValue"/* name */
	, &SCR_Collectible_get_PointValue_m1189_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo SCR_Collectible_get_IsTaken_m1190_MethodInfo;
extern const MethodInfo SCR_Collectible_set_IsTaken_m1191_MethodInfo;
static const PropertyInfo SCR_Collectible_t331____IsTaken_PropertyInfo = 
{
	&SCR_Collectible_t331_il2cpp_TypeInfo/* parent */
	, "IsTaken"/* name */
	, &SCR_Collectible_get_IsTaken_m1190_MethodInfo/* get */
	, &SCR_Collectible_set_IsTaken_m1191_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* SCR_Collectible_t331_PropertyInfos[] =
{
	&SCR_Collectible_t331____PointValue_PropertyInfo,
	&SCR_Collectible_t331____IsTaken_PropertyInfo,
	NULL
};
static const Il2CppMethodReference SCR_Collectible_t331_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	NULL,
};
static bool SCR_Collectible_t331_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType SCR_Collectible_t331_0_0_0;
extern const Il2CppType SCR_Collectible_t331_1_0_0;
struct SCR_Collectible_t331;
const Il2CppTypeDefinitionMetadata SCR_Collectible_t331_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, SCR_Collectible_t331_VTable/* vtableMethods */
	, SCR_Collectible_t331_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 280/* fieldStart */

};
TypeInfo SCR_Collectible_t331_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "SCR_Collectible"/* name */
	, ""/* namespaze */
	, SCR_Collectible_t331_MethodInfos/* methods */
	, SCR_Collectible_t331_PropertyInfos/* properties */
	, NULL/* events */
	, &SCR_Collectible_t331_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SCR_Collectible_t331_0_0_0/* byval_arg */
	, &SCR_Collectible_t331_1_0_0/* this_arg */
	, &SCR_Collectible_t331_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SCR_Collectible_t331)/* instance_size */
	, sizeof (SCR_Collectible_t331)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SCR_Menu/<AlphaLerp>c__Iterator2
#include "AssemblyU2DCSharp_SCR_Menu_U3CAlphaLerpU3Ec__Iterator2.h"
// Metadata Definition SCR_Menu/<AlphaLerp>c__Iterator2
extern TypeInfo U3CAlphaLerpU3Ec__Iterator2_t332_il2cpp_TypeInfo;
// SCR_Menu/<AlphaLerp>c__Iterator2
#include "AssemblyU2DCSharp_SCR_Menu_U3CAlphaLerpU3Ec__Iterator2MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Menu/<AlphaLerp>c__Iterator2::.ctor()
extern const MethodInfo U3CAlphaLerpU3Ec__Iterator2__ctor_m1192_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CAlphaLerpU3Ec__Iterator2__ctor_m1192/* method */
	, &U3CAlphaLerpU3Ec__Iterator2_t332_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 142/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object SCR_Menu/<AlphaLerp>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern const MethodInfo U3CAlphaLerpU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1193_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<object>.get_Current"/* name */
	, (methodPointerType)&U3CAlphaLerpU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1193/* method */
	, &U3CAlphaLerpU3Ec__Iterator2_t332_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 19/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 143/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object SCR_Menu/<AlphaLerp>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern const MethodInfo U3CAlphaLerpU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m1194_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&U3CAlphaLerpU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m1194/* method */
	, &U3CAlphaLerpU3Ec__Iterator2_t332_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 20/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 144/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean SCR_Menu/<AlphaLerp>c__Iterator2::MoveNext()
extern const MethodInfo U3CAlphaLerpU3Ec__Iterator2_MoveNext_m1195_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&U3CAlphaLerpU3Ec__Iterator2_MoveNext_m1195/* method */
	, &U3CAlphaLerpU3Ec__Iterator2_t332_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 145/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Menu/<AlphaLerp>c__Iterator2::Dispose()
extern const MethodInfo U3CAlphaLerpU3Ec__Iterator2_Dispose_m1196_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&U3CAlphaLerpU3Ec__Iterator2_Dispose_m1196/* method */
	, &U3CAlphaLerpU3Ec__Iterator2_t332_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 21/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 146/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Menu/<AlphaLerp>c__Iterator2::Reset()
extern const MethodInfo U3CAlphaLerpU3Ec__Iterator2_Reset_m1197_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&U3CAlphaLerpU3Ec__Iterator2_Reset_m1197/* method */
	, &U3CAlphaLerpU3Ec__Iterator2_t332_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 22/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 147/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CAlphaLerpU3Ec__Iterator2_t332_MethodInfos[] =
{
	&U3CAlphaLerpU3Ec__Iterator2__ctor_m1192_MethodInfo,
	&U3CAlphaLerpU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1193_MethodInfo,
	&U3CAlphaLerpU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m1194_MethodInfo,
	&U3CAlphaLerpU3Ec__Iterator2_MoveNext_m1195_MethodInfo,
	&U3CAlphaLerpU3Ec__Iterator2_Dispose_m1196_MethodInfo,
	&U3CAlphaLerpU3Ec__Iterator2_Reset_m1197_MethodInfo,
	NULL
};
extern const MethodInfo U3CAlphaLerpU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1193_MethodInfo;
static const PropertyInfo U3CAlphaLerpU3Ec__Iterator2_t332____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo = 
{
	&U3CAlphaLerpU3Ec__Iterator2_t332_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<object>.Current"/* name */
	, &U3CAlphaLerpU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1193_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo U3CAlphaLerpU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m1194_MethodInfo;
static const PropertyInfo U3CAlphaLerpU3Ec__Iterator2_t332____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CAlphaLerpU3Ec__Iterator2_t332_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CAlphaLerpU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m1194_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* U3CAlphaLerpU3Ec__Iterator2_t332_PropertyInfos[] =
{
	&U3CAlphaLerpU3Ec__Iterator2_t332____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo,
	&U3CAlphaLerpU3Ec__Iterator2_t332____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern const MethodInfo U3CAlphaLerpU3Ec__Iterator2_Dispose_m1196_MethodInfo;
extern const MethodInfo U3CAlphaLerpU3Ec__Iterator2_MoveNext_m1195_MethodInfo;
extern const MethodInfo U3CAlphaLerpU3Ec__Iterator2_Reset_m1197_MethodInfo;
static const Il2CppMethodReference U3CAlphaLerpU3Ec__Iterator2_t332_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&U3CAlphaLerpU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1193_MethodInfo,
	&U3CAlphaLerpU3Ec__Iterator2_Dispose_m1196_MethodInfo,
	&U3CAlphaLerpU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m1194_MethodInfo,
	&U3CAlphaLerpU3Ec__Iterator2_MoveNext_m1195_MethodInfo,
	&U3CAlphaLerpU3Ec__Iterator2_Reset_m1197_MethodInfo,
};
static bool U3CAlphaLerpU3Ec__Iterator2_t332_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* U3CAlphaLerpU3Ec__Iterator2_t332_InterfacesTypeInfos[] = 
{
	&IEnumerator_1_t284_0_0_0,
	&IDisposable_t233_0_0_0,
	&IEnumerator_t217_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CAlphaLerpU3Ec__Iterator2_t332_InterfacesOffsets[] = 
{
	{ &IEnumerator_1_t284_0_0_0, 4},
	{ &IDisposable_t233_0_0_0, 5},
	{ &IEnumerator_t217_0_0_0, 6},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType U3CAlphaLerpU3Ec__Iterator2_t332_0_0_0;
extern const Il2CppType U3CAlphaLerpU3Ec__Iterator2_t332_1_0_0;
extern TypeInfo SCR_Menu_t336_il2cpp_TypeInfo;
extern const Il2CppType SCR_Menu_t336_0_0_0;
struct U3CAlphaLerpU3Ec__Iterator2_t332;
const Il2CppTypeDefinitionMetadata U3CAlphaLerpU3Ec__Iterator2_t332_DefinitionMetadata = 
{
	&SCR_Menu_t336_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CAlphaLerpU3Ec__Iterator2_t332_InterfacesTypeInfos/* implementedInterfaces */
	, U3CAlphaLerpU3Ec__Iterator2_t332_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CAlphaLerpU3Ec__Iterator2_t332_VTable/* vtableMethods */
	, U3CAlphaLerpU3Ec__Iterator2_t332_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 282/* fieldStart */

};
TypeInfo U3CAlphaLerpU3Ec__Iterator2_t332_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<AlphaLerp>c__Iterator2"/* name */
	, ""/* namespaze */
	, U3CAlphaLerpU3Ec__Iterator2_t332_MethodInfos/* methods */
	, U3CAlphaLerpU3Ec__Iterator2_t332_PropertyInfos/* properties */
	, NULL/* events */
	, &U3CAlphaLerpU3Ec__Iterator2_t332_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 18/* custom_attributes_cache */
	, &U3CAlphaLerpU3Ec__Iterator2_t332_0_0_0/* byval_arg */
	, &U3CAlphaLerpU3Ec__Iterator2_t332_1_0_0/* this_arg */
	, &U3CAlphaLerpU3Ec__Iterator2_t332_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CAlphaLerpU3Ec__Iterator2_t332)/* instance_size */
	, sizeof (U3CAlphaLerpU3Ec__Iterator2_t332)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// SCR_Menu/<AlphaLerp>c__Iterator3
#include "AssemblyU2DCSharp_SCR_Menu_U3CAlphaLerpU3Ec__Iterator3.h"
// Metadata Definition SCR_Menu/<AlphaLerp>c__Iterator3
extern TypeInfo U3CAlphaLerpU3Ec__Iterator3_t333_il2cpp_TypeInfo;
// SCR_Menu/<AlphaLerp>c__Iterator3
#include "AssemblyU2DCSharp_SCR_Menu_U3CAlphaLerpU3Ec__Iterator3MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Menu/<AlphaLerp>c__Iterator3::.ctor()
extern const MethodInfo U3CAlphaLerpU3Ec__Iterator3__ctor_m1198_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CAlphaLerpU3Ec__Iterator3__ctor_m1198/* method */
	, &U3CAlphaLerpU3Ec__Iterator3_t333_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 148/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object SCR_Menu/<AlphaLerp>c__Iterator3::System.Collections.Generic.IEnumerator<object>.get_Current()
extern const MethodInfo U3CAlphaLerpU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1199_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<object>.get_Current"/* name */
	, (methodPointerType)&U3CAlphaLerpU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1199/* method */
	, &U3CAlphaLerpU3Ec__Iterator3_t333_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 24/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 149/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object SCR_Menu/<AlphaLerp>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern const MethodInfo U3CAlphaLerpU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m1200_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&U3CAlphaLerpU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m1200/* method */
	, &U3CAlphaLerpU3Ec__Iterator3_t333_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 25/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 150/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean SCR_Menu/<AlphaLerp>c__Iterator3::MoveNext()
extern const MethodInfo U3CAlphaLerpU3Ec__Iterator3_MoveNext_m1201_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&U3CAlphaLerpU3Ec__Iterator3_MoveNext_m1201/* method */
	, &U3CAlphaLerpU3Ec__Iterator3_t333_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 151/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Menu/<AlphaLerp>c__Iterator3::Dispose()
extern const MethodInfo U3CAlphaLerpU3Ec__Iterator3_Dispose_m1202_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&U3CAlphaLerpU3Ec__Iterator3_Dispose_m1202/* method */
	, &U3CAlphaLerpU3Ec__Iterator3_t333_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 26/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 152/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Menu/<AlphaLerp>c__Iterator3::Reset()
extern const MethodInfo U3CAlphaLerpU3Ec__Iterator3_Reset_m1203_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&U3CAlphaLerpU3Ec__Iterator3_Reset_m1203/* method */
	, &U3CAlphaLerpU3Ec__Iterator3_t333_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 27/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 153/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CAlphaLerpU3Ec__Iterator3_t333_MethodInfos[] =
{
	&U3CAlphaLerpU3Ec__Iterator3__ctor_m1198_MethodInfo,
	&U3CAlphaLerpU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1199_MethodInfo,
	&U3CAlphaLerpU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m1200_MethodInfo,
	&U3CAlphaLerpU3Ec__Iterator3_MoveNext_m1201_MethodInfo,
	&U3CAlphaLerpU3Ec__Iterator3_Dispose_m1202_MethodInfo,
	&U3CAlphaLerpU3Ec__Iterator3_Reset_m1203_MethodInfo,
	NULL
};
extern const MethodInfo U3CAlphaLerpU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1199_MethodInfo;
static const PropertyInfo U3CAlphaLerpU3Ec__Iterator3_t333____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo = 
{
	&U3CAlphaLerpU3Ec__Iterator3_t333_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<object>.Current"/* name */
	, &U3CAlphaLerpU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1199_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo U3CAlphaLerpU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m1200_MethodInfo;
static const PropertyInfo U3CAlphaLerpU3Ec__Iterator3_t333____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CAlphaLerpU3Ec__Iterator3_t333_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CAlphaLerpU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m1200_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* U3CAlphaLerpU3Ec__Iterator3_t333_PropertyInfos[] =
{
	&U3CAlphaLerpU3Ec__Iterator3_t333____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo,
	&U3CAlphaLerpU3Ec__Iterator3_t333____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern const MethodInfo U3CAlphaLerpU3Ec__Iterator3_Dispose_m1202_MethodInfo;
extern const MethodInfo U3CAlphaLerpU3Ec__Iterator3_MoveNext_m1201_MethodInfo;
extern const MethodInfo U3CAlphaLerpU3Ec__Iterator3_Reset_m1203_MethodInfo;
static const Il2CppMethodReference U3CAlphaLerpU3Ec__Iterator3_t333_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&U3CAlphaLerpU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1199_MethodInfo,
	&U3CAlphaLerpU3Ec__Iterator3_Dispose_m1202_MethodInfo,
	&U3CAlphaLerpU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m1200_MethodInfo,
	&U3CAlphaLerpU3Ec__Iterator3_MoveNext_m1201_MethodInfo,
	&U3CAlphaLerpU3Ec__Iterator3_Reset_m1203_MethodInfo,
};
static bool U3CAlphaLerpU3Ec__Iterator3_t333_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* U3CAlphaLerpU3Ec__Iterator3_t333_InterfacesTypeInfos[] = 
{
	&IEnumerator_1_t284_0_0_0,
	&IDisposable_t233_0_0_0,
	&IEnumerator_t217_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CAlphaLerpU3Ec__Iterator3_t333_InterfacesOffsets[] = 
{
	{ &IEnumerator_1_t284_0_0_0, 4},
	{ &IDisposable_t233_0_0_0, 5},
	{ &IEnumerator_t217_0_0_0, 6},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType U3CAlphaLerpU3Ec__Iterator3_t333_0_0_0;
extern const Il2CppType U3CAlphaLerpU3Ec__Iterator3_t333_1_0_0;
struct U3CAlphaLerpU3Ec__Iterator3_t333;
const Il2CppTypeDefinitionMetadata U3CAlphaLerpU3Ec__Iterator3_t333_DefinitionMetadata = 
{
	&SCR_Menu_t336_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CAlphaLerpU3Ec__Iterator3_t333_InterfacesTypeInfos/* implementedInterfaces */
	, U3CAlphaLerpU3Ec__Iterator3_t333_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CAlphaLerpU3Ec__Iterator3_t333_VTable/* vtableMethods */
	, U3CAlphaLerpU3Ec__Iterator3_t333_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 294/* fieldStart */

};
TypeInfo U3CAlphaLerpU3Ec__Iterator3_t333_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<AlphaLerp>c__Iterator3"/* name */
	, ""/* namespaze */
	, U3CAlphaLerpU3Ec__Iterator3_t333_MethodInfos/* methods */
	, U3CAlphaLerpU3Ec__Iterator3_t333_PropertyInfos/* properties */
	, NULL/* events */
	, &U3CAlphaLerpU3Ec__Iterator3_t333_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 23/* custom_attributes_cache */
	, &U3CAlphaLerpU3Ec__Iterator3_t333_0_0_0/* byval_arg */
	, &U3CAlphaLerpU3Ec__Iterator3_t333_1_0_0/* this_arg */
	, &U3CAlphaLerpU3Ec__Iterator3_t333_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CAlphaLerpU3Ec__Iterator3_t333)/* instance_size */
	, sizeof (U3CAlphaLerpU3Ec__Iterator3_t333)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// SCR_Menu
#include "AssemblyU2DCSharp_SCR_Menu.h"
// Metadata Definition SCR_Menu
// SCR_Menu
#include "AssemblyU2DCSharp_SCR_MenuMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Menu::.ctor()
extern const MethodInfo SCR_Menu__ctor_m1204_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SCR_Menu__ctor_m1204/* method */
	, &SCR_Menu_t336_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 125/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Menu::Awake()
extern const MethodInfo SCR_Menu_Awake_m1205_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&SCR_Menu_Awake_m1205/* method */
	, &SCR_Menu_t336_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 126/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Menu::OnEnable()
extern const MethodInfo SCR_Menu_OnEnable_m1206_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&SCR_Menu_OnEnable_m1206/* method */
	, &SCR_Menu_t336_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 127/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TransformU5BU5D_t141_0_0_0;
extern const Il2CppType TransformU5BU5D_t141_0_0_0;
static const ParameterInfo SCR_Menu_t336_SCR_Menu_FillDictionary_m1207_ParameterInfos[] = 
{
	{"children", 0, 134217751, 0, &TransformU5BU5D_t141_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Menu::FillDictionary(UnityEngine.Transform[])
extern const MethodInfo SCR_Menu_FillDictionary_m1207_MethodInfo = 
{
	"FillDictionary"/* name */
	, (methodPointerType)&SCR_Menu_FillDictionary_m1207/* method */
	, &SCR_Menu_t336_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, SCR_Menu_t336_SCR_Menu_FillDictionary_m1207_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 128/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo SCR_Menu_t336_SCR_Menu_ShowMenuItem_m1208_ParameterInfos[] = 
{
	{"itemName", 0, 134217752, 0, &String_t_0_0_0},
	{"show", 1, 134217753, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Menu::ShowMenuItem(System.String,System.Boolean)
extern const MethodInfo SCR_Menu_ShowMenuItem_m1208_MethodInfo = 
{
	"ShowMenuItem"/* name */
	, (methodPointerType)&SCR_Menu_ShowMenuItem_m1208/* method */
	, &SCR_Menu_t336_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_SByte_t274/* invoker_method */
	, SCR_Menu_t336_SCR_Menu_ShowMenuItem_m1208_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 129/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Menu::ResetMenu()
extern const MethodInfo SCR_Menu_ResetMenu_m1209_MethodInfo = 
{
	"ResetMenu"/* name */
	, (methodPointerType)&SCR_Menu_ResetMenu_m1209/* method */
	, &SCR_Menu_t336_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 130/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo SCR_Menu_t336_SCR_Menu_GetMenuItem_m1210_ParameterInfos[] = 
{
	{"name", 0, 134217754, 0, &String_t_0_0_0},
};
extern const Il2CppType GameObject_t78_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.GameObject SCR_Menu::GetMenuItem(System.String)
extern const MethodInfo SCR_Menu_GetMenuItem_m1210_MethodInfo = 
{
	"GetMenuItem"/* name */
	, (methodPointerType)&SCR_Menu_GetMenuItem_m1210/* method */
	, &SCR_Menu_t336_il2cpp_TypeInfo/* declaring_type */
	, &GameObject_t78_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, SCR_Menu_t336_SCR_Menu_GetMenuItem_m1210_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 131/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo SCR_Menu_t336_SCR_Menu_ModifyText_m1211_ParameterInfos[] = 
{
	{"textGuiName", 0, 134217755, 0, &String_t_0_0_0},
	{"textToWrite", 1, 134217756, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Menu::ModifyText(System.String,System.String)
extern const MethodInfo SCR_Menu_ModifyText_m1211_MethodInfo = 
{
	"ModifyText"/* name */
	, (methodPointerType)&SCR_Menu_ModifyText_m1211/* method */
	, &SCR_Menu_t336_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, SCR_Menu_t336_SCR_Menu_ModifyText_m1211_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 132/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Text_t316_0_0_0;
extern const Il2CppType Text_t316_0_0_0;
extern const Il2CppType Single_t254_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo SCR_Menu_t336_SCR_Menu_AlphaLerp_m1212_ParameterInfos[] = 
{
	{"text", 0, 134217757, 0, &Text_t316_0_0_0},
	{"speed", 1, 134217758, 0, &Single_t254_0_0_0},
	{"isRepeating", 2, 134217759, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Single_t254_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Collections.IEnumerator SCR_Menu::AlphaLerp(UnityEngine.UI.Text,System.Single,System.Boolean)
extern const MethodInfo SCR_Menu_AlphaLerp_m1212_MethodInfo = 
{
	"AlphaLerp"/* name */
	, (methodPointerType)&SCR_Menu_AlphaLerp_m1212/* method */
	, &SCR_Menu_t336_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t217_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Single_t254_SByte_t274/* invoker_method */
	, SCR_Menu_t336_SCR_Menu_AlphaLerp_m1212_ParameterInfos/* parameters */
	, 16/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 133/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Image_t48_0_0_0;
extern const Il2CppType Image_t48_0_0_0;
extern const Il2CppType Single_t254_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo SCR_Menu_t336_SCR_Menu_AlphaLerp_m1213_ParameterInfos[] = 
{
	{"image", 0, 134217760, 0, &Image_t48_0_0_0},
	{"speed", 1, 134217761, 0, &Single_t254_0_0_0},
	{"isRepeating", 2, 134217762, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Single_t254_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Collections.IEnumerator SCR_Menu::AlphaLerp(UnityEngine.UI.Image,System.Single,System.Boolean)
extern const MethodInfo SCR_Menu_AlphaLerp_m1213_MethodInfo = 
{
	"AlphaLerp"/* name */
	, (methodPointerType)&SCR_Menu_AlphaLerp_m1213/* method */
	, &SCR_Menu_t336_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t217_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Single_t254_SByte_t274/* invoker_method */
	, SCR_Menu_t336_SCR_Menu_AlphaLerp_m1213_ParameterInfos/* parameters */
	, 17/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 134/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Menu::Options()
extern const MethodInfo SCR_Menu_Options_m1214_MethodInfo = 
{
	"Options"/* name */
	, (methodPointerType)&SCR_Menu_Options_m1214/* method */
	, &SCR_Menu_t336_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 135/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo SCR_Menu_t336_SCR_Menu_SwitchLanguage_m1215_ParameterInfos[] = 
{
	{"isFrench", 0, 134217763, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Menu::SwitchLanguage(System.Boolean)
extern const MethodInfo SCR_Menu_SwitchLanguage_m1215_MethodInfo = 
{
	"SwitchLanguage"/* name */
	, (methodPointerType)&SCR_Menu_SwitchLanguage_m1215/* method */
	, &SCR_Menu_t336_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274/* invoker_method */
	, SCR_Menu_t336_SCR_Menu_SwitchLanguage_m1215_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 136/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo SCR_Menu_t336_SCR_Menu_ToggleSound_m1216_ParameterInfos[] = 
{
	{"soundOn", 0, 134217764, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Menu::ToggleSound(System.Boolean)
extern const MethodInfo SCR_Menu_ToggleSound_m1216_MethodInfo = 
{
	"ToggleSound"/* name */
	, (methodPointerType)&SCR_Menu_ToggleSound_m1216/* method */
	, &SCR_Menu_t336_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274/* invoker_method */
	, SCR_Menu_t336_SCR_Menu_ToggleSound_m1216_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 137/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Menu::SetMenuLanguage()
extern const MethodInfo SCR_Menu_SetMenuLanguage_m1217_MethodInfo = 
{
	"SetMenuLanguage"/* name */
	, (methodPointerType)&SCR_Menu_SetMenuLanguage_m1217/* method */
	, &SCR_Menu_t336_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 138/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Menu::ReturnToMainMenu()
extern const MethodInfo SCR_Menu_ReturnToMainMenu_m1218_MethodInfo = 
{
	"ReturnToMainMenu"/* name */
	, (methodPointerType)&SCR_Menu_ReturnToMainMenu_m1218/* method */
	, &SCR_Menu_t336_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 139/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo SCR_Menu_t336_SCR_Menu_LoadLevel_m1219_ParameterInfos[] = 
{
	{"level", 0, 134217765, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Menu::LoadLevel(System.Int32)
extern const MethodInfo SCR_Menu_LoadLevel_m1219_MethodInfo = 
{
	"LoadLevel"/* name */
	, (methodPointerType)&SCR_Menu_LoadLevel_m1219/* method */
	, &SCR_Menu_t336_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, SCR_Menu_t336_SCR_Menu_LoadLevel_m1219_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 140/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Menu::ButtonSound()
extern const MethodInfo SCR_Menu_ButtonSound_m1220_MethodInfo = 
{
	"ButtonSound"/* name */
	, (methodPointerType)&SCR_Menu_ButtonSound_m1220/* method */
	, &SCR_Menu_t336_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 141/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SCR_Menu_t336_MethodInfos[] =
{
	&SCR_Menu__ctor_m1204_MethodInfo,
	&SCR_Menu_Awake_m1205_MethodInfo,
	&SCR_Menu_OnEnable_m1206_MethodInfo,
	&SCR_Menu_FillDictionary_m1207_MethodInfo,
	&SCR_Menu_ShowMenuItem_m1208_MethodInfo,
	&SCR_Menu_ResetMenu_m1209_MethodInfo,
	&SCR_Menu_GetMenuItem_m1210_MethodInfo,
	&SCR_Menu_ModifyText_m1211_MethodInfo,
	&SCR_Menu_AlphaLerp_m1212_MethodInfo,
	&SCR_Menu_AlphaLerp_m1213_MethodInfo,
	&SCR_Menu_Options_m1214_MethodInfo,
	&SCR_Menu_SwitchLanguage_m1215_MethodInfo,
	&SCR_Menu_ToggleSound_m1216_MethodInfo,
	&SCR_Menu_SetMenuLanguage_m1217_MethodInfo,
	&SCR_Menu_ReturnToMainMenu_m1218_MethodInfo,
	&SCR_Menu_LoadLevel_m1219_MethodInfo,
	&SCR_Menu_ButtonSound_m1220_MethodInfo,
	NULL
};
static const Il2CppType* SCR_Menu_t336_il2cpp_TypeInfo__nestedTypes[2] =
{
	&U3CAlphaLerpU3Ec__Iterator2_t332_0_0_0,
	&U3CAlphaLerpU3Ec__Iterator3_t333_0_0_0,
};
static const Il2CppMethodReference SCR_Menu_t336_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool SCR_Menu_t336_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType SCR_Menu_t336_1_0_0;
struct SCR_Menu_t336;
const Il2CppTypeDefinitionMetadata SCR_Menu_t336_DefinitionMetadata = 
{
	NULL/* declaringType */
	, SCR_Menu_t336_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, SCR_Menu_t336_VTable/* vtableMethods */
	, SCR_Menu_t336_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 305/* fieldStart */

};
TypeInfo SCR_Menu_t336_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "SCR_Menu"/* name */
	, ""/* namespaze */
	, SCR_Menu_t336_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SCR_Menu_t336_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SCR_Menu_t336_0_0_0/* byval_arg */
	, &SCR_Menu_t336_1_0_0/* this_arg */
	, &SCR_Menu_t336_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SCR_Menu_t336)/* instance_size */
	, sizeof (SCR_Menu_t336)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 17/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SCR_SlowAvatar
#include "AssemblyU2DCSharp_SCR_SlowAvatar.h"
// Metadata Definition SCR_SlowAvatar
extern TypeInfo SCR_SlowAvatar_t337_il2cpp_TypeInfo;
// SCR_SlowAvatar
#include "AssemblyU2DCSharp_SCR_SlowAvatarMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_SlowAvatar::.ctor()
extern const MethodInfo SCR_SlowAvatar__ctor_m1221_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SCR_SlowAvatar__ctor_m1221/* method */
	, &SCR_SlowAvatar_t337_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 154/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_SlowAvatar::TriggerEnterEffect()
extern const MethodInfo SCR_SlowAvatar_TriggerEnterEffect_m1844_MethodInfo = 
{
	"TriggerEnterEffect"/* name */
	, NULL/* method */
	, &SCR_SlowAvatar_t337_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1476/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 155/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_SlowAvatar::TriggerExitEffet()
extern const MethodInfo SCR_SlowAvatar_TriggerExitEffet_m1845_MethodInfo = 
{
	"TriggerExitEffet"/* name */
	, NULL/* method */
	, &SCR_SlowAvatar_t337_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1476/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 156/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Collider_t138_0_0_0;
extern const Il2CppType Collider_t138_0_0_0;
static const ParameterInfo SCR_SlowAvatar_t337_SCR_SlowAvatar_OnTriggerEnter_m1222_ParameterInfos[] = 
{
	{"other", 0, 134217766, 0, &Collider_t138_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_SlowAvatar::OnTriggerEnter(UnityEngine.Collider)
extern const MethodInfo SCR_SlowAvatar_OnTriggerEnter_m1222_MethodInfo = 
{
	"OnTriggerEnter"/* name */
	, (methodPointerType)&SCR_SlowAvatar_OnTriggerEnter_m1222/* method */
	, &SCR_SlowAvatar_t337_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, SCR_SlowAvatar_t337_SCR_SlowAvatar_OnTriggerEnter_m1222_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 157/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_SlowAvatar::OnTriggerExit()
extern const MethodInfo SCR_SlowAvatar_OnTriggerExit_m1223_MethodInfo = 
{
	"OnTriggerExit"/* name */
	, (methodPointerType)&SCR_SlowAvatar_OnTriggerExit_m1223/* method */
	, &SCR_SlowAvatar_t337_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 158/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SCR_SlowAvatar_t337_MethodInfos[] =
{
	&SCR_SlowAvatar__ctor_m1221_MethodInfo,
	&SCR_SlowAvatar_TriggerEnterEffect_m1844_MethodInfo,
	&SCR_SlowAvatar_TriggerExitEffet_m1845_MethodInfo,
	&SCR_SlowAvatar_OnTriggerEnter_m1222_MethodInfo,
	&SCR_SlowAvatar_OnTriggerExit_m1223_MethodInfo,
	NULL
};
static const Il2CppMethodReference SCR_SlowAvatar_t337_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	NULL,
	NULL,
};
static bool SCR_SlowAvatar_t337_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType SCR_SlowAvatar_t337_0_0_0;
extern const Il2CppType SCR_SlowAvatar_t337_1_0_0;
struct SCR_SlowAvatar_t337;
const Il2CppTypeDefinitionMetadata SCR_SlowAvatar_t337_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, SCR_SlowAvatar_t337_VTable/* vtableMethods */
	, SCR_SlowAvatar_t337_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 308/* fieldStart */

};
TypeInfo SCR_SlowAvatar_t337_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "SCR_SlowAvatar"/* name */
	, ""/* namespaze */
	, SCR_SlowAvatar_t337_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SCR_SlowAvatar_t337_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SCR_SlowAvatar_t337_0_0_0/* byval_arg */
	, &SCR_SlowAvatar_t337_1_0_0/* this_arg */
	, &SCR_SlowAvatar_t337_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SCR_SlowAvatar_t337)/* instance_size */
	, sizeof (SCR_SlowAvatar_t337)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SCR_Camie/<GoToFrontCollisionPoint>c__Iterator4
#include "AssemblyU2DCSharp_SCR_Camie_U3CGoToFrontCollisionPointU3Ec__.h"
// Metadata Definition SCR_Camie/<GoToFrontCollisionPoint>c__Iterator4
extern TypeInfo U3CGoToFrontCollisionPointU3Ec__Iterator4_t339_il2cpp_TypeInfo;
// SCR_Camie/<GoToFrontCollisionPoint>c__Iterator4
#include "AssemblyU2DCSharp_SCR_Camie_U3CGoToFrontCollisionPointU3Ec__MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Camie/<GoToFrontCollisionPoint>c__Iterator4::.ctor()
extern const MethodInfo U3CGoToFrontCollisionPointU3Ec__Iterator4__ctor_m1224_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CGoToFrontCollisionPointU3Ec__Iterator4__ctor_m1224/* method */
	, &U3CGoToFrontCollisionPointU3Ec__Iterator4_t339_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 187/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object SCR_Camie/<GoToFrontCollisionPoint>c__Iterator4::System.Collections.Generic.IEnumerator<object>.get_Current()
extern const MethodInfo U3CGoToFrontCollisionPointU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1225_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<object>.get_Current"/* name */
	, (methodPointerType)&U3CGoToFrontCollisionPointU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1225/* method */
	, &U3CGoToFrontCollisionPointU3Ec__Iterator4_t339_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 33/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 188/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object SCR_Camie/<GoToFrontCollisionPoint>c__Iterator4::System.Collections.IEnumerator.get_Current()
extern const MethodInfo U3CGoToFrontCollisionPointU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m1226_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&U3CGoToFrontCollisionPointU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m1226/* method */
	, &U3CGoToFrontCollisionPointU3Ec__Iterator4_t339_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 34/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 189/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean SCR_Camie/<GoToFrontCollisionPoint>c__Iterator4::MoveNext()
extern const MethodInfo U3CGoToFrontCollisionPointU3Ec__Iterator4_MoveNext_m1227_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&U3CGoToFrontCollisionPointU3Ec__Iterator4_MoveNext_m1227/* method */
	, &U3CGoToFrontCollisionPointU3Ec__Iterator4_t339_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 190/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Camie/<GoToFrontCollisionPoint>c__Iterator4::Dispose()
extern const MethodInfo U3CGoToFrontCollisionPointU3Ec__Iterator4_Dispose_m1228_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&U3CGoToFrontCollisionPointU3Ec__Iterator4_Dispose_m1228/* method */
	, &U3CGoToFrontCollisionPointU3Ec__Iterator4_t339_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 35/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 191/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Camie/<GoToFrontCollisionPoint>c__Iterator4::Reset()
extern const MethodInfo U3CGoToFrontCollisionPointU3Ec__Iterator4_Reset_m1229_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&U3CGoToFrontCollisionPointU3Ec__Iterator4_Reset_m1229/* method */
	, &U3CGoToFrontCollisionPointU3Ec__Iterator4_t339_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 36/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 192/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CGoToFrontCollisionPointU3Ec__Iterator4_t339_MethodInfos[] =
{
	&U3CGoToFrontCollisionPointU3Ec__Iterator4__ctor_m1224_MethodInfo,
	&U3CGoToFrontCollisionPointU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1225_MethodInfo,
	&U3CGoToFrontCollisionPointU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m1226_MethodInfo,
	&U3CGoToFrontCollisionPointU3Ec__Iterator4_MoveNext_m1227_MethodInfo,
	&U3CGoToFrontCollisionPointU3Ec__Iterator4_Dispose_m1228_MethodInfo,
	&U3CGoToFrontCollisionPointU3Ec__Iterator4_Reset_m1229_MethodInfo,
	NULL
};
extern const MethodInfo U3CGoToFrontCollisionPointU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1225_MethodInfo;
static const PropertyInfo U3CGoToFrontCollisionPointU3Ec__Iterator4_t339____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo = 
{
	&U3CGoToFrontCollisionPointU3Ec__Iterator4_t339_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<object>.Current"/* name */
	, &U3CGoToFrontCollisionPointU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1225_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo U3CGoToFrontCollisionPointU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m1226_MethodInfo;
static const PropertyInfo U3CGoToFrontCollisionPointU3Ec__Iterator4_t339____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CGoToFrontCollisionPointU3Ec__Iterator4_t339_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CGoToFrontCollisionPointU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m1226_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* U3CGoToFrontCollisionPointU3Ec__Iterator4_t339_PropertyInfos[] =
{
	&U3CGoToFrontCollisionPointU3Ec__Iterator4_t339____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo,
	&U3CGoToFrontCollisionPointU3Ec__Iterator4_t339____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern const MethodInfo U3CGoToFrontCollisionPointU3Ec__Iterator4_Dispose_m1228_MethodInfo;
extern const MethodInfo U3CGoToFrontCollisionPointU3Ec__Iterator4_MoveNext_m1227_MethodInfo;
extern const MethodInfo U3CGoToFrontCollisionPointU3Ec__Iterator4_Reset_m1229_MethodInfo;
static const Il2CppMethodReference U3CGoToFrontCollisionPointU3Ec__Iterator4_t339_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&U3CGoToFrontCollisionPointU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1225_MethodInfo,
	&U3CGoToFrontCollisionPointU3Ec__Iterator4_Dispose_m1228_MethodInfo,
	&U3CGoToFrontCollisionPointU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m1226_MethodInfo,
	&U3CGoToFrontCollisionPointU3Ec__Iterator4_MoveNext_m1227_MethodInfo,
	&U3CGoToFrontCollisionPointU3Ec__Iterator4_Reset_m1229_MethodInfo,
};
static bool U3CGoToFrontCollisionPointU3Ec__Iterator4_t339_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* U3CGoToFrontCollisionPointU3Ec__Iterator4_t339_InterfacesTypeInfos[] = 
{
	&IEnumerator_1_t284_0_0_0,
	&IDisposable_t233_0_0_0,
	&IEnumerator_t217_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CGoToFrontCollisionPointU3Ec__Iterator4_t339_InterfacesOffsets[] = 
{
	{ &IEnumerator_1_t284_0_0_0, 4},
	{ &IDisposable_t233_0_0_0, 5},
	{ &IEnumerator_t217_0_0_0, 6},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType U3CGoToFrontCollisionPointU3Ec__Iterator4_t339_0_0_0;
extern const Il2CppType U3CGoToFrontCollisionPointU3Ec__Iterator4_t339_1_0_0;
extern TypeInfo SCR_Camie_t338_il2cpp_TypeInfo;
extern const Il2CppType SCR_Camie_t338_0_0_0;
struct U3CGoToFrontCollisionPointU3Ec__Iterator4_t339;
const Il2CppTypeDefinitionMetadata U3CGoToFrontCollisionPointU3Ec__Iterator4_t339_DefinitionMetadata = 
{
	&SCR_Camie_t338_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CGoToFrontCollisionPointU3Ec__Iterator4_t339_InterfacesTypeInfos/* implementedInterfaces */
	, U3CGoToFrontCollisionPointU3Ec__Iterator4_t339_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CGoToFrontCollisionPointU3Ec__Iterator4_t339_VTable/* vtableMethods */
	, U3CGoToFrontCollisionPointU3Ec__Iterator4_t339_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 309/* fieldStart */

};
TypeInfo U3CGoToFrontCollisionPointU3Ec__Iterator4_t339_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<GoToFrontCollisionPoint>c__Iterator4"/* name */
	, ""/* namespaze */
	, U3CGoToFrontCollisionPointU3Ec__Iterator4_t339_MethodInfos/* methods */
	, U3CGoToFrontCollisionPointU3Ec__Iterator4_t339_PropertyInfos/* properties */
	, NULL/* events */
	, &U3CGoToFrontCollisionPointU3Ec__Iterator4_t339_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 32/* custom_attributes_cache */
	, &U3CGoToFrontCollisionPointU3Ec__Iterator4_t339_0_0_0/* byval_arg */
	, &U3CGoToFrontCollisionPointU3Ec__Iterator4_t339_1_0_0/* this_arg */
	, &U3CGoToFrontCollisionPointU3Ec__Iterator4_t339_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CGoToFrontCollisionPointU3Ec__Iterator4_t339)/* instance_size */
	, sizeof (U3CGoToFrontCollisionPointU3Ec__Iterator4_t339)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// SCR_Camie/<LandOnCollisionPoint>c__Iterator5
#include "AssemblyU2DCSharp_SCR_Camie_U3CLandOnCollisionPointU3Ec__Ite.h"
// Metadata Definition SCR_Camie/<LandOnCollisionPoint>c__Iterator5
extern TypeInfo U3CLandOnCollisionPointU3Ec__Iterator5_t340_il2cpp_TypeInfo;
// SCR_Camie/<LandOnCollisionPoint>c__Iterator5
#include "AssemblyU2DCSharp_SCR_Camie_U3CLandOnCollisionPointU3Ec__IteMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Camie/<LandOnCollisionPoint>c__Iterator5::.ctor()
extern const MethodInfo U3CLandOnCollisionPointU3Ec__Iterator5__ctor_m1230_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CLandOnCollisionPointU3Ec__Iterator5__ctor_m1230/* method */
	, &U3CLandOnCollisionPointU3Ec__Iterator5_t340_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 193/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object SCR_Camie/<LandOnCollisionPoint>c__Iterator5::System.Collections.Generic.IEnumerator<object>.get_Current()
extern const MethodInfo U3CLandOnCollisionPointU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1231_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<object>.get_Current"/* name */
	, (methodPointerType)&U3CLandOnCollisionPointU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1231/* method */
	, &U3CLandOnCollisionPointU3Ec__Iterator5_t340_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 38/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 194/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object SCR_Camie/<LandOnCollisionPoint>c__Iterator5::System.Collections.IEnumerator.get_Current()
extern const MethodInfo U3CLandOnCollisionPointU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m1232_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&U3CLandOnCollisionPointU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m1232/* method */
	, &U3CLandOnCollisionPointU3Ec__Iterator5_t340_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 39/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 195/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean SCR_Camie/<LandOnCollisionPoint>c__Iterator5::MoveNext()
extern const MethodInfo U3CLandOnCollisionPointU3Ec__Iterator5_MoveNext_m1233_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&U3CLandOnCollisionPointU3Ec__Iterator5_MoveNext_m1233/* method */
	, &U3CLandOnCollisionPointU3Ec__Iterator5_t340_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 196/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Camie/<LandOnCollisionPoint>c__Iterator5::Dispose()
extern const MethodInfo U3CLandOnCollisionPointU3Ec__Iterator5_Dispose_m1234_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&U3CLandOnCollisionPointU3Ec__Iterator5_Dispose_m1234/* method */
	, &U3CLandOnCollisionPointU3Ec__Iterator5_t340_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 40/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 197/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Camie/<LandOnCollisionPoint>c__Iterator5::Reset()
extern const MethodInfo U3CLandOnCollisionPointU3Ec__Iterator5_Reset_m1235_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&U3CLandOnCollisionPointU3Ec__Iterator5_Reset_m1235/* method */
	, &U3CLandOnCollisionPointU3Ec__Iterator5_t340_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 41/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 198/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CLandOnCollisionPointU3Ec__Iterator5_t340_MethodInfos[] =
{
	&U3CLandOnCollisionPointU3Ec__Iterator5__ctor_m1230_MethodInfo,
	&U3CLandOnCollisionPointU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1231_MethodInfo,
	&U3CLandOnCollisionPointU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m1232_MethodInfo,
	&U3CLandOnCollisionPointU3Ec__Iterator5_MoveNext_m1233_MethodInfo,
	&U3CLandOnCollisionPointU3Ec__Iterator5_Dispose_m1234_MethodInfo,
	&U3CLandOnCollisionPointU3Ec__Iterator5_Reset_m1235_MethodInfo,
	NULL
};
extern const MethodInfo U3CLandOnCollisionPointU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1231_MethodInfo;
static const PropertyInfo U3CLandOnCollisionPointU3Ec__Iterator5_t340____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo = 
{
	&U3CLandOnCollisionPointU3Ec__Iterator5_t340_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<object>.Current"/* name */
	, &U3CLandOnCollisionPointU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1231_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo U3CLandOnCollisionPointU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m1232_MethodInfo;
static const PropertyInfo U3CLandOnCollisionPointU3Ec__Iterator5_t340____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CLandOnCollisionPointU3Ec__Iterator5_t340_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CLandOnCollisionPointU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m1232_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* U3CLandOnCollisionPointU3Ec__Iterator5_t340_PropertyInfos[] =
{
	&U3CLandOnCollisionPointU3Ec__Iterator5_t340____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo,
	&U3CLandOnCollisionPointU3Ec__Iterator5_t340____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern const MethodInfo U3CLandOnCollisionPointU3Ec__Iterator5_Dispose_m1234_MethodInfo;
extern const MethodInfo U3CLandOnCollisionPointU3Ec__Iterator5_MoveNext_m1233_MethodInfo;
extern const MethodInfo U3CLandOnCollisionPointU3Ec__Iterator5_Reset_m1235_MethodInfo;
static const Il2CppMethodReference U3CLandOnCollisionPointU3Ec__Iterator5_t340_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&U3CLandOnCollisionPointU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1231_MethodInfo,
	&U3CLandOnCollisionPointU3Ec__Iterator5_Dispose_m1234_MethodInfo,
	&U3CLandOnCollisionPointU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m1232_MethodInfo,
	&U3CLandOnCollisionPointU3Ec__Iterator5_MoveNext_m1233_MethodInfo,
	&U3CLandOnCollisionPointU3Ec__Iterator5_Reset_m1235_MethodInfo,
};
static bool U3CLandOnCollisionPointU3Ec__Iterator5_t340_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* U3CLandOnCollisionPointU3Ec__Iterator5_t340_InterfacesTypeInfos[] = 
{
	&IEnumerator_1_t284_0_0_0,
	&IDisposable_t233_0_0_0,
	&IEnumerator_t217_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CLandOnCollisionPointU3Ec__Iterator5_t340_InterfacesOffsets[] = 
{
	{ &IEnumerator_1_t284_0_0_0, 4},
	{ &IDisposable_t233_0_0_0, 5},
	{ &IEnumerator_t217_0_0_0, 6},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType U3CLandOnCollisionPointU3Ec__Iterator5_t340_0_0_0;
extern const Il2CppType U3CLandOnCollisionPointU3Ec__Iterator5_t340_1_0_0;
struct U3CLandOnCollisionPointU3Ec__Iterator5_t340;
const Il2CppTypeDefinitionMetadata U3CLandOnCollisionPointU3Ec__Iterator5_t340_DefinitionMetadata = 
{
	&SCR_Camie_t338_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CLandOnCollisionPointU3Ec__Iterator5_t340_InterfacesTypeInfos/* implementedInterfaces */
	, U3CLandOnCollisionPointU3Ec__Iterator5_t340_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CLandOnCollisionPointU3Ec__Iterator5_t340_VTable/* vtableMethods */
	, U3CLandOnCollisionPointU3Ec__Iterator5_t340_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 316/* fieldStart */

};
TypeInfo U3CLandOnCollisionPointU3Ec__Iterator5_t340_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<LandOnCollisionPoint>c__Iterator5"/* name */
	, ""/* namespaze */
	, U3CLandOnCollisionPointU3Ec__Iterator5_t340_MethodInfos/* methods */
	, U3CLandOnCollisionPointU3Ec__Iterator5_t340_PropertyInfos/* properties */
	, NULL/* events */
	, &U3CLandOnCollisionPointU3Ec__Iterator5_t340_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 37/* custom_attributes_cache */
	, &U3CLandOnCollisionPointU3Ec__Iterator5_t340_0_0_0/* byval_arg */
	, &U3CLandOnCollisionPointU3Ec__Iterator5_t340_1_0_0/* this_arg */
	, &U3CLandOnCollisionPointU3Ec__Iterator5_t340_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CLandOnCollisionPointU3Ec__Iterator5_t340)/* instance_size */
	, sizeof (U3CLandOnCollisionPointU3Ec__Iterator5_t340)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// SCR_Camie/<Jump>c__Iterator6
#include "AssemblyU2DCSharp_SCR_Camie_U3CJumpU3Ec__Iterator6.h"
// Metadata Definition SCR_Camie/<Jump>c__Iterator6
extern TypeInfo U3CJumpU3Ec__Iterator6_t341_il2cpp_TypeInfo;
// SCR_Camie/<Jump>c__Iterator6
#include "AssemblyU2DCSharp_SCR_Camie_U3CJumpU3Ec__Iterator6MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Camie/<Jump>c__Iterator6::.ctor()
extern const MethodInfo U3CJumpU3Ec__Iterator6__ctor_m1236_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CJumpU3Ec__Iterator6__ctor_m1236/* method */
	, &U3CJumpU3Ec__Iterator6_t341_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 199/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object SCR_Camie/<Jump>c__Iterator6::System.Collections.Generic.IEnumerator<object>.get_Current()
extern const MethodInfo U3CJumpU3Ec__Iterator6_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1237_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<object>.get_Current"/* name */
	, (methodPointerType)&U3CJumpU3Ec__Iterator6_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1237/* method */
	, &U3CJumpU3Ec__Iterator6_t341_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 43/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 200/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object SCR_Camie/<Jump>c__Iterator6::System.Collections.IEnumerator.get_Current()
extern const MethodInfo U3CJumpU3Ec__Iterator6_System_Collections_IEnumerator_get_Current_m1238_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&U3CJumpU3Ec__Iterator6_System_Collections_IEnumerator_get_Current_m1238/* method */
	, &U3CJumpU3Ec__Iterator6_t341_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 44/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 201/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean SCR_Camie/<Jump>c__Iterator6::MoveNext()
extern const MethodInfo U3CJumpU3Ec__Iterator6_MoveNext_m1239_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&U3CJumpU3Ec__Iterator6_MoveNext_m1239/* method */
	, &U3CJumpU3Ec__Iterator6_t341_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 202/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Camie/<Jump>c__Iterator6::Dispose()
extern const MethodInfo U3CJumpU3Ec__Iterator6_Dispose_m1240_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&U3CJumpU3Ec__Iterator6_Dispose_m1240/* method */
	, &U3CJumpU3Ec__Iterator6_t341_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 45/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 203/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Camie/<Jump>c__Iterator6::Reset()
extern const MethodInfo U3CJumpU3Ec__Iterator6_Reset_m1241_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&U3CJumpU3Ec__Iterator6_Reset_m1241/* method */
	, &U3CJumpU3Ec__Iterator6_t341_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 46/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 204/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CJumpU3Ec__Iterator6_t341_MethodInfos[] =
{
	&U3CJumpU3Ec__Iterator6__ctor_m1236_MethodInfo,
	&U3CJumpU3Ec__Iterator6_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1237_MethodInfo,
	&U3CJumpU3Ec__Iterator6_System_Collections_IEnumerator_get_Current_m1238_MethodInfo,
	&U3CJumpU3Ec__Iterator6_MoveNext_m1239_MethodInfo,
	&U3CJumpU3Ec__Iterator6_Dispose_m1240_MethodInfo,
	&U3CJumpU3Ec__Iterator6_Reset_m1241_MethodInfo,
	NULL
};
extern const MethodInfo U3CJumpU3Ec__Iterator6_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1237_MethodInfo;
static const PropertyInfo U3CJumpU3Ec__Iterator6_t341____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo = 
{
	&U3CJumpU3Ec__Iterator6_t341_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<object>.Current"/* name */
	, &U3CJumpU3Ec__Iterator6_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1237_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo U3CJumpU3Ec__Iterator6_System_Collections_IEnumerator_get_Current_m1238_MethodInfo;
static const PropertyInfo U3CJumpU3Ec__Iterator6_t341____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CJumpU3Ec__Iterator6_t341_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CJumpU3Ec__Iterator6_System_Collections_IEnumerator_get_Current_m1238_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* U3CJumpU3Ec__Iterator6_t341_PropertyInfos[] =
{
	&U3CJumpU3Ec__Iterator6_t341____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo,
	&U3CJumpU3Ec__Iterator6_t341____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern const MethodInfo U3CJumpU3Ec__Iterator6_Dispose_m1240_MethodInfo;
extern const MethodInfo U3CJumpU3Ec__Iterator6_MoveNext_m1239_MethodInfo;
extern const MethodInfo U3CJumpU3Ec__Iterator6_Reset_m1241_MethodInfo;
static const Il2CppMethodReference U3CJumpU3Ec__Iterator6_t341_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&U3CJumpU3Ec__Iterator6_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1237_MethodInfo,
	&U3CJumpU3Ec__Iterator6_Dispose_m1240_MethodInfo,
	&U3CJumpU3Ec__Iterator6_System_Collections_IEnumerator_get_Current_m1238_MethodInfo,
	&U3CJumpU3Ec__Iterator6_MoveNext_m1239_MethodInfo,
	&U3CJumpU3Ec__Iterator6_Reset_m1241_MethodInfo,
};
static bool U3CJumpU3Ec__Iterator6_t341_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* U3CJumpU3Ec__Iterator6_t341_InterfacesTypeInfos[] = 
{
	&IEnumerator_1_t284_0_0_0,
	&IDisposable_t233_0_0_0,
	&IEnumerator_t217_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CJumpU3Ec__Iterator6_t341_InterfacesOffsets[] = 
{
	{ &IEnumerator_1_t284_0_0_0, 4},
	{ &IDisposable_t233_0_0_0, 5},
	{ &IEnumerator_t217_0_0_0, 6},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType U3CJumpU3Ec__Iterator6_t341_0_0_0;
extern const Il2CppType U3CJumpU3Ec__Iterator6_t341_1_0_0;
struct U3CJumpU3Ec__Iterator6_t341;
const Il2CppTypeDefinitionMetadata U3CJumpU3Ec__Iterator6_t341_DefinitionMetadata = 
{
	&SCR_Camie_t338_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CJumpU3Ec__Iterator6_t341_InterfacesTypeInfos/* implementedInterfaces */
	, U3CJumpU3Ec__Iterator6_t341_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CJumpU3Ec__Iterator6_t341_VTable/* vtableMethods */
	, U3CJumpU3Ec__Iterator6_t341_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 323/* fieldStart */

};
TypeInfo U3CJumpU3Ec__Iterator6_t341_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<Jump>c__Iterator6"/* name */
	, ""/* namespaze */
	, U3CJumpU3Ec__Iterator6_t341_MethodInfos/* methods */
	, U3CJumpU3Ec__Iterator6_t341_PropertyInfos/* properties */
	, NULL/* events */
	, &U3CJumpU3Ec__Iterator6_t341_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 42/* custom_attributes_cache */
	, &U3CJumpU3Ec__Iterator6_t341_0_0_0/* byval_arg */
	, &U3CJumpU3Ec__Iterator6_t341_1_0_0/* this_arg */
	, &U3CJumpU3Ec__Iterator6_t341_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CJumpU3Ec__Iterator6_t341)/* instance_size */
	, sizeof (U3CJumpU3Ec__Iterator6_t341)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// SCR_Camie
#include "AssemblyU2DCSharp_SCR_Camie.h"
// Metadata Definition SCR_Camie
// SCR_Camie
#include "AssemblyU2DCSharp_SCR_CamieMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Camie::.ctor()
extern const MethodInfo SCR_Camie__ctor_m1242_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SCR_Camie__ctor_m1242/* method */
	, &SCR_Camie_t338_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 159/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Camie::Start()
extern const MethodInfo SCR_Camie_Start_m1243_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&SCR_Camie_Start_m1243/* method */
	, &SCR_Camie_t338_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 160/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Camie::Update()
extern const MethodInfo SCR_Camie_Update_m1244_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&SCR_Camie_Update_m1244/* method */
	, &SCR_Camie_t338_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 161/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Collision_t146_0_0_0;
extern const Il2CppType Collision_t146_0_0_0;
static const ParameterInfo SCR_Camie_t338_SCR_Camie_OnCollisionEnter_m1245_ParameterInfos[] = 
{
	{"info", 0, 134217767, 0, &Collision_t146_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Camie::OnCollisionEnter(UnityEngine.Collision)
extern const MethodInfo SCR_Camie_OnCollisionEnter_m1245_MethodInfo = 
{
	"OnCollisionEnter"/* name */
	, (methodPointerType)&SCR_Camie_OnCollisionEnter_m1245/* method */
	, &SCR_Camie_t338_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, SCR_Camie_t338_SCR_Camie_OnCollisionEnter_m1245_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 162/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector3_t4_0_0_0;
extern const Il2CppType Vector3_t4_0_0_0;
extern const Il2CppType Vector3_t4_0_0_0;
static const ParameterInfo SCR_Camie_t338_SCR_Camie_GoToFrontCollisionPoint_m1246_ParameterInfos[] = 
{
	{"point", 0, 134217768, 0, &Vector3_t4_0_0_0},
	{"normal", 1, 134217769, 0, &Vector3_t4_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Vector3_t4_Vector3_t4 (const MethodInfo* method, void* obj, void** args);
// System.Collections.IEnumerator SCR_Camie::GoToFrontCollisionPoint(UnityEngine.Vector3,UnityEngine.Vector3)
extern const MethodInfo SCR_Camie_GoToFrontCollisionPoint_m1246_MethodInfo = 
{
	"GoToFrontCollisionPoint"/* name */
	, (methodPointerType)&SCR_Camie_GoToFrontCollisionPoint_m1246/* method */
	, &SCR_Camie_t338_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t217_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Vector3_t4_Vector3_t4/* invoker_method */
	, SCR_Camie_t338_SCR_Camie_GoToFrontCollisionPoint_m1246_ParameterInfos/* parameters */
	, 29/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 163/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Camie::ResetActiveCollisionDetector()
extern const MethodInfo SCR_Camie_ResetActiveCollisionDetector_m1247_MethodInfo = 
{
	"ResetActiveCollisionDetector"/* name */
	, (methodPointerType)&SCR_Camie_ResetActiveCollisionDetector_m1247/* method */
	, &SCR_Camie_t338_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 164/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector3_t4_0_0_0;
extern const Il2CppType Vector3_t4_0_0_0;
static const ParameterInfo SCR_Camie_t338_SCR_Camie_LandOnCollisionPoint_m1248_ParameterInfos[] = 
{
	{"point", 0, 134217770, 0, &Vector3_t4_0_0_0},
	{"normal", 1, 134217771, 0, &Vector3_t4_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Vector3_t4_Vector3_t4 (const MethodInfo* method, void* obj, void** args);
// System.Collections.IEnumerator SCR_Camie::LandOnCollisionPoint(UnityEngine.Vector3,UnityEngine.Vector3)
extern const MethodInfo SCR_Camie_LandOnCollisionPoint_m1248_MethodInfo = 
{
	"LandOnCollisionPoint"/* name */
	, (methodPointerType)&SCR_Camie_LandOnCollisionPoint_m1248/* method */
	, &SCR_Camie_t338_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t217_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Vector3_t4_Vector3_t4/* invoker_method */
	, SCR_Camie_t338_SCR_Camie_LandOnCollisionPoint_m1248_ParameterInfos/* parameters */
	, 30/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 165/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector3_t4_0_0_0;
static const ParameterInfo SCR_Camie_t338_SCR_Camie_Jump_m1249_ParameterInfos[] = 
{
	{"normal", 0, 134217772, 0, &Vector3_t4_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Vector3_t4 (const MethodInfo* method, void* obj, void** args);
// System.Collections.IEnumerator SCR_Camie::Jump(UnityEngine.Vector3)
extern const MethodInfo SCR_Camie_Jump_m1249_MethodInfo = 
{
	"Jump"/* name */
	, (methodPointerType)&SCR_Camie_Jump_m1249/* method */
	, &SCR_Camie_t338_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t217_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Vector3_t4/* invoker_method */
	, SCR_Camie_t338_SCR_Camie_Jump_m1249_ParameterInfos/* parameters */
	, 31/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 166/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Camie::GetDirection()
extern const MethodInfo SCR_Camie_GetDirection_m1250_MethodInfo = 
{
	"GetDirection"/* name */
	, (methodPointerType)&SCR_Camie_GetDirection_m1250/* method */
	, &SCR_Camie_t338_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 167/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo SCR_Camie_t338_SCR_Camie_DecreaseSpeed_m1251_ParameterInfos[] = 
{
	{"toPercent", 0, 134217773, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Camie::DecreaseSpeed(System.Single)
extern const MethodInfo SCR_Camie_DecreaseSpeed_m1251_MethodInfo = 
{
	"DecreaseSpeed"/* name */
	, (methodPointerType)&SCR_Camie_DecreaseSpeed_m1251/* method */
	, &SCR_Camie_t338_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Single_t254/* invoker_method */
	, SCR_Camie_t338_SCR_Camie_DecreaseSpeed_m1251_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 168/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Camie::NormalSpeed()
extern const MethodInfo SCR_Camie_NormalSpeed_m1252_MethodInfo = 
{
	"NormalSpeed"/* name */
	, (methodPointerType)&SCR_Camie_NormalSpeed_m1252/* method */
	, &SCR_Camie_t338_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 169/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Camie::Respawn()
extern const MethodInfo SCR_Camie_Respawn_m1253_MethodInfo = 
{
	"Respawn"/* name */
	, (methodPointerType)&SCR_Camie_Respawn_m1253/* method */
	, &SCR_Camie_t338_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 170/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Camie::BreakIdle()
extern const MethodInfo SCR_Camie_BreakIdle_m1254_MethodInfo = 
{
	"BreakIdle"/* name */
	, (methodPointerType)&SCR_Camie_BreakIdle_m1254/* method */
	, &SCR_Camie_t338_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 171/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Camie::ResetIdle()
extern const MethodInfo SCR_Camie_ResetIdle_m1255_MethodInfo = 
{
	"ResetIdle"/* name */
	, (methodPointerType)&SCR_Camie_ResetIdle_m1255/* method */
	, &SCR_Camie_t338_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 172/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo SCR_Camie_t338_SCR_Camie_ToggleWings_m1256_ParameterInfos[] = 
{
	{"show", 0, 134217774, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Camie::ToggleWings(System.Boolean)
extern const MethodInfo SCR_Camie_ToggleWings_m1256_MethodInfo = 
{
	"ToggleWings"/* name */
	, (methodPointerType)&SCR_Camie_ToggleWings_m1256/* method */
	, &SCR_Camie_t338_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274/* invoker_method */
	, SCR_Camie_t338_SCR_Camie_ToggleWings_m1256_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 173/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SCR_Goutte_t347_0_0_0;
extern const Il2CppType SCR_Goutte_t347_0_0_0;
static const ParameterInfo SCR_Camie_t338_SCR_Camie_InBubble_m1257_ParameterInfos[] = 
{
	{"goutte", 0, 134217775, 0, &SCR_Goutte_t347_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Camie::InBubble(SCR_Goutte)
extern const MethodInfo SCR_Camie_InBubble_m1257_MethodInfo = 
{
	"InBubble"/* name */
	, (methodPointerType)&SCR_Camie_InBubble_m1257/* method */
	, &SCR_Camie_t338_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, SCR_Camie_t338_SCR_Camie_InBubble_m1257_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 174/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Camie::EndBubble()
extern const MethodInfo SCR_Camie_EndBubble_m1258_MethodInfo = 
{
	"EndBubble"/* name */
	, (methodPointerType)&SCR_Camie_EndBubble_m1258/* method */
	, &SCR_Camie_t338_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 175/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Camie::LosePucerons()
extern const MethodInfo SCR_Camie_LosePucerons_m1259_MethodInfo = 
{
	"LosePucerons"/* name */
	, (methodPointerType)&SCR_Camie_LosePucerons_m1259/* method */
	, &SCR_Camie_t338_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 176/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean SCR_Camie::get_IsGrounded()
extern const MethodInfo SCR_Camie_get_IsGrounded_m1260_MethodInfo = 
{
	"get_IsGrounded"/* name */
	, (methodPointerType)&SCR_Camie_get_IsGrounded_m1260/* method */
	, &SCR_Camie_t338_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 177/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean SCR_Camie::get_IsFlying()
extern const MethodInfo SCR_Camie_get_IsFlying_m1261_MethodInfo = 
{
	"get_IsFlying"/* name */
	, (methodPointerType)&SCR_Camie_get_IsFlying_m1261/* method */
	, &SCR_Camie_t338_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 178/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean SCR_Camie::get_CanFly()
extern const MethodInfo SCR_Camie_get_CanFly_m1262_MethodInfo = 
{
	"get_CanFly"/* name */
	, (methodPointerType)&SCR_Camie_get_CanFly_m1262/* method */
	, &SCR_Camie_t338_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 179/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo SCR_Camie_t338_SCR_Camie_set_CanFly_m1263_ParameterInfos[] = 
{
	{"value", 0, 134217776, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Camie::set_CanFly(System.Boolean)
extern const MethodInfo SCR_Camie_set_CanFly_m1263_MethodInfo = 
{
	"set_CanFly"/* name */
	, (methodPointerType)&SCR_Camie_set_CanFly_m1263/* method */
	, &SCR_Camie_t338_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274/* invoker_method */
	, SCR_Camie_t338_SCR_Camie_set_CanFly_m1263_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 180/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean SCR_Camie::get_IsSlowed()
extern const MethodInfo SCR_Camie_get_IsSlowed_m1264_MethodInfo = 
{
	"get_IsSlowed"/* name */
	, (methodPointerType)&SCR_Camie_get_IsSlowed_m1264/* method */
	, &SCR_Camie_t338_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 181/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Animator_t9_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Animator SCR_Camie::get_MyAnimations()
extern const MethodInfo SCR_Camie_get_MyAnimations_m1265_MethodInfo = 
{
	"get_MyAnimations"/* name */
	, (methodPointerType)&SCR_Camie_get_MyAnimations_m1265/* method */
	, &SCR_Camie_t338_il2cpp_TypeInfo/* declaring_type */
	, &Animator_t9_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 182/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Vector3_t4 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector3 SCR_Camie::get_ColliderCenter()
extern const MethodInfo SCR_Camie_get_ColliderCenter_m1266_MethodInfo = 
{
	"get_ColliderCenter"/* name */
	, (methodPointerType)&SCR_Camie_get_ColliderCenter_m1266/* method */
	, &SCR_Camie_t338_il2cpp_TypeInfo/* declaring_type */
	, &Vector3_t4_0_0_0/* return_type */
	, RuntimeInvoker_Vector3_t4/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 183/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SCR_CamieDirections_t348_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// SCR_CamieDirections SCR_Camie::get_Director()
extern const MethodInfo SCR_Camie_get_Director_m1267_MethodInfo = 
{
	"get_Director"/* name */
	, (methodPointerType)&SCR_Camie_get_Director_m1267/* method */
	, &SCR_Camie_t338_il2cpp_TypeInfo/* declaring_type */
	, &SCR_CamieDirections_t348_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 184/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SCR_CamieDirections_t348_0_0_0;
static const ParameterInfo SCR_Camie_t338_SCR_Camie_set_Director_m1268_ParameterInfos[] = 
{
	{"value", 0, 134217777, 0, &SCR_CamieDirections_t348_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Camie::set_Director(SCR_CamieDirections)
extern const MethodInfo SCR_Camie_set_Director_m1268_MethodInfo = 
{
	"set_Director"/* name */
	, (methodPointerType)&SCR_Camie_set_Director_m1268/* method */
	, &SCR_Camie_t338_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, SCR_Camie_t338_SCR_Camie_set_Director_m1268_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 185/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SCR_Respawn_t344_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// SCR_Respawn SCR_Camie::get_Respawner()
extern const MethodInfo SCR_Camie_get_Respawner_m1269_MethodInfo = 
{
	"get_Respawner"/* name */
	, (methodPointerType)&SCR_Camie_get_Respawner_m1269/* method */
	, &SCR_Camie_t338_il2cpp_TypeInfo/* declaring_type */
	, &SCR_Respawn_t344_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 186/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SCR_Camie_t338_MethodInfos[] =
{
	&SCR_Camie__ctor_m1242_MethodInfo,
	&SCR_Camie_Start_m1243_MethodInfo,
	&SCR_Camie_Update_m1244_MethodInfo,
	&SCR_Camie_OnCollisionEnter_m1245_MethodInfo,
	&SCR_Camie_GoToFrontCollisionPoint_m1246_MethodInfo,
	&SCR_Camie_ResetActiveCollisionDetector_m1247_MethodInfo,
	&SCR_Camie_LandOnCollisionPoint_m1248_MethodInfo,
	&SCR_Camie_Jump_m1249_MethodInfo,
	&SCR_Camie_GetDirection_m1250_MethodInfo,
	&SCR_Camie_DecreaseSpeed_m1251_MethodInfo,
	&SCR_Camie_NormalSpeed_m1252_MethodInfo,
	&SCR_Camie_Respawn_m1253_MethodInfo,
	&SCR_Camie_BreakIdle_m1254_MethodInfo,
	&SCR_Camie_ResetIdle_m1255_MethodInfo,
	&SCR_Camie_ToggleWings_m1256_MethodInfo,
	&SCR_Camie_InBubble_m1257_MethodInfo,
	&SCR_Camie_EndBubble_m1258_MethodInfo,
	&SCR_Camie_LosePucerons_m1259_MethodInfo,
	&SCR_Camie_get_IsGrounded_m1260_MethodInfo,
	&SCR_Camie_get_IsFlying_m1261_MethodInfo,
	&SCR_Camie_get_CanFly_m1262_MethodInfo,
	&SCR_Camie_set_CanFly_m1263_MethodInfo,
	&SCR_Camie_get_IsSlowed_m1264_MethodInfo,
	&SCR_Camie_get_MyAnimations_m1265_MethodInfo,
	&SCR_Camie_get_ColliderCenter_m1266_MethodInfo,
	&SCR_Camie_get_Director_m1267_MethodInfo,
	&SCR_Camie_set_Director_m1268_MethodInfo,
	&SCR_Camie_get_Respawner_m1269_MethodInfo,
	NULL
};
extern const MethodInfo SCR_Camie_get_IsGrounded_m1260_MethodInfo;
static const PropertyInfo SCR_Camie_t338____IsGrounded_PropertyInfo = 
{
	&SCR_Camie_t338_il2cpp_TypeInfo/* parent */
	, "IsGrounded"/* name */
	, &SCR_Camie_get_IsGrounded_m1260_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo SCR_Camie_get_IsFlying_m1261_MethodInfo;
static const PropertyInfo SCR_Camie_t338____IsFlying_PropertyInfo = 
{
	&SCR_Camie_t338_il2cpp_TypeInfo/* parent */
	, "IsFlying"/* name */
	, &SCR_Camie_get_IsFlying_m1261_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo SCR_Camie_get_CanFly_m1262_MethodInfo;
extern const MethodInfo SCR_Camie_set_CanFly_m1263_MethodInfo;
static const PropertyInfo SCR_Camie_t338____CanFly_PropertyInfo = 
{
	&SCR_Camie_t338_il2cpp_TypeInfo/* parent */
	, "CanFly"/* name */
	, &SCR_Camie_get_CanFly_m1262_MethodInfo/* get */
	, &SCR_Camie_set_CanFly_m1263_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo SCR_Camie_get_IsSlowed_m1264_MethodInfo;
static const PropertyInfo SCR_Camie_t338____IsSlowed_PropertyInfo = 
{
	&SCR_Camie_t338_il2cpp_TypeInfo/* parent */
	, "IsSlowed"/* name */
	, &SCR_Camie_get_IsSlowed_m1264_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo SCR_Camie_get_MyAnimations_m1265_MethodInfo;
static const PropertyInfo SCR_Camie_t338____MyAnimations_PropertyInfo = 
{
	&SCR_Camie_t338_il2cpp_TypeInfo/* parent */
	, "MyAnimations"/* name */
	, &SCR_Camie_get_MyAnimations_m1265_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo SCR_Camie_get_ColliderCenter_m1266_MethodInfo;
static const PropertyInfo SCR_Camie_t338____ColliderCenter_PropertyInfo = 
{
	&SCR_Camie_t338_il2cpp_TypeInfo/* parent */
	, "ColliderCenter"/* name */
	, &SCR_Camie_get_ColliderCenter_m1266_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo SCR_Camie_get_Director_m1267_MethodInfo;
extern const MethodInfo SCR_Camie_set_Director_m1268_MethodInfo;
static const PropertyInfo SCR_Camie_t338____Director_PropertyInfo = 
{
	&SCR_Camie_t338_il2cpp_TypeInfo/* parent */
	, "Director"/* name */
	, &SCR_Camie_get_Director_m1267_MethodInfo/* get */
	, &SCR_Camie_set_Director_m1268_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo SCR_Camie_get_Respawner_m1269_MethodInfo;
static const PropertyInfo SCR_Camie_t338____Respawner_PropertyInfo = 
{
	&SCR_Camie_t338_il2cpp_TypeInfo/* parent */
	, "Respawner"/* name */
	, &SCR_Camie_get_Respawner_m1269_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* SCR_Camie_t338_PropertyInfos[] =
{
	&SCR_Camie_t338____IsGrounded_PropertyInfo,
	&SCR_Camie_t338____IsFlying_PropertyInfo,
	&SCR_Camie_t338____CanFly_PropertyInfo,
	&SCR_Camie_t338____IsSlowed_PropertyInfo,
	&SCR_Camie_t338____MyAnimations_PropertyInfo,
	&SCR_Camie_t338____ColliderCenter_PropertyInfo,
	&SCR_Camie_t338____Director_PropertyInfo,
	&SCR_Camie_t338____Respawner_PropertyInfo,
	NULL
};
static const Il2CppType* SCR_Camie_t338_il2cpp_TypeInfo__nestedTypes[3] =
{
	&U3CGoToFrontCollisionPointU3Ec__Iterator4_t339_0_0_0,
	&U3CLandOnCollisionPointU3Ec__Iterator5_t340_0_0_0,
	&U3CJumpU3Ec__Iterator6_t341_0_0_0,
};
static const Il2CppMethodReference SCR_Camie_t338_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool SCR_Camie_t338_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType SCR_Camie_t338_1_0_0;
struct SCR_Camie_t338;
const Il2CppTypeDefinitionMetadata SCR_Camie_t338_DefinitionMetadata = 
{
	NULL/* declaringType */
	, SCR_Camie_t338_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, SCR_Camie_t338_VTable/* vtableMethods */
	, SCR_Camie_t338_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 327/* fieldStart */

};
TypeInfo SCR_Camie_t338_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "SCR_Camie"/* name */
	, ""/* namespaze */
	, SCR_Camie_t338_MethodInfos/* methods */
	, SCR_Camie_t338_PropertyInfos/* properties */
	, NULL/* events */
	, &SCR_Camie_t338_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SCR_Camie_t338_0_0_0/* byval_arg */
	, &SCR_Camie_t338_1_0_0/* this_arg */
	, &SCR_Camie_t338_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SCR_Camie_t338)/* instance_size */
	, sizeof (SCR_Camie_t338)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 28/* method_count */
	, 8/* property_count */
	, 37/* field_count */
	, 0/* event_count */
	, 3/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SCR_CollisionChecker
#include "AssemblyU2DCSharp_SCR_CollisionChecker.h"
// Metadata Definition SCR_CollisionChecker
extern TypeInfo SCR_CollisionChecker_t349_il2cpp_TypeInfo;
// SCR_CollisionChecker
#include "AssemblyU2DCSharp_SCR_CollisionCheckerMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_CollisionChecker::.ctor()
extern const MethodInfo SCR_CollisionChecker__ctor_m1270_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SCR_CollisionChecker__ctor_m1270/* method */
	, &SCR_CollisionChecker_t349_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 205/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_CollisionChecker::Start()
extern const MethodInfo SCR_CollisionChecker_Start_m1271_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&SCR_CollisionChecker_Start_m1271/* method */
	, &SCR_CollisionChecker_t349_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 206/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_CollisionChecker::Update()
extern const MethodInfo SCR_CollisionChecker_Update_m1272_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&SCR_CollisionChecker_Update_m1272/* method */
	, &SCR_CollisionChecker_t349_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 207/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Collision_t146_0_0_0;
static const ParameterInfo SCR_CollisionChecker_t349_SCR_CollisionChecker_OnCollisionEnter_m1273_ParameterInfos[] = 
{
	{"info", 0, 134217778, 0, &Collision_t146_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_CollisionChecker::OnCollisionEnter(UnityEngine.Collision)
extern const MethodInfo SCR_CollisionChecker_OnCollisionEnter_m1273_MethodInfo = 
{
	"OnCollisionEnter"/* name */
	, (methodPointerType)&SCR_CollisionChecker_OnCollisionEnter_m1273/* method */
	, &SCR_CollisionChecker_t349_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, SCR_CollisionChecker_t349_SCR_CollisionChecker_OnCollisionEnter_m1273_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 208/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SCR_CollisionChecker_t349_MethodInfos[] =
{
	&SCR_CollisionChecker__ctor_m1270_MethodInfo,
	&SCR_CollisionChecker_Start_m1271_MethodInfo,
	&SCR_CollisionChecker_Update_m1272_MethodInfo,
	&SCR_CollisionChecker_OnCollisionEnter_m1273_MethodInfo,
	NULL
};
static const Il2CppMethodReference SCR_CollisionChecker_t349_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool SCR_CollisionChecker_t349_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType SCR_CollisionChecker_t349_0_0_0;
extern const Il2CppType SCR_CollisionChecker_t349_1_0_0;
struct SCR_CollisionChecker_t349;
const Il2CppTypeDefinitionMetadata SCR_CollisionChecker_t349_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, SCR_CollisionChecker_t349_VTable/* vtableMethods */
	, SCR_CollisionChecker_t349_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 364/* fieldStart */

};
TypeInfo SCR_CollisionChecker_t349_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "SCR_CollisionChecker"/* name */
	, ""/* namespaze */
	, SCR_CollisionChecker_t349_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SCR_CollisionChecker_t349_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SCR_CollisionChecker_t349_0_0_0/* byval_arg */
	, &SCR_CollisionChecker_t349_1_0_0/* this_arg */
	, &SCR_CollisionChecker_t349_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SCR_CollisionChecker_t349)/* instance_size */
	, sizeof (SCR_CollisionChecker_t349)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SCR_ResetIdle
#include "AssemblyU2DCSharp_SCR_ResetIdle.h"
// Metadata Definition SCR_ResetIdle
extern TypeInfo SCR_ResetIdle_t350_il2cpp_TypeInfo;
// SCR_ResetIdle
#include "AssemblyU2DCSharp_SCR_ResetIdleMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_ResetIdle::.ctor()
extern const MethodInfo SCR_ResetIdle__ctor_m1274_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SCR_ResetIdle__ctor_m1274/* method */
	, &SCR_ResetIdle_t350_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 209/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_ResetIdle::Start()
extern const MethodInfo SCR_ResetIdle_Start_m1275_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&SCR_ResetIdle_Start_m1275/* method */
	, &SCR_ResetIdle_t350_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 210/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_ResetIdle::ResetIdle()
extern const MethodInfo SCR_ResetIdle_ResetIdle_m1276_MethodInfo = 
{
	"ResetIdle"/* name */
	, (methodPointerType)&SCR_ResetIdle_ResetIdle_m1276/* method */
	, &SCR_ResetIdle_t350_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 211/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_ResetIdle::Land()
extern const MethodInfo SCR_ResetIdle_Land_m1277_MethodInfo = 
{
	"Land"/* name */
	, (methodPointerType)&SCR_ResetIdle_Land_m1277/* method */
	, &SCR_ResetIdle_t350_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 212/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_ResetIdle::OutOfBubble()
extern const MethodInfo SCR_ResetIdle_OutOfBubble_m1278_MethodInfo = 
{
	"OutOfBubble"/* name */
	, (methodPointerType)&SCR_ResetIdle_OutOfBubble_m1278/* method */
	, &SCR_ResetIdle_t350_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 213/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SCR_ResetIdle_t350_MethodInfos[] =
{
	&SCR_ResetIdle__ctor_m1274_MethodInfo,
	&SCR_ResetIdle_Start_m1275_MethodInfo,
	&SCR_ResetIdle_ResetIdle_m1276_MethodInfo,
	&SCR_ResetIdle_Land_m1277_MethodInfo,
	&SCR_ResetIdle_OutOfBubble_m1278_MethodInfo,
	NULL
};
static const Il2CppMethodReference SCR_ResetIdle_t350_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool SCR_ResetIdle_t350_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType SCR_ResetIdle_t350_0_0_0;
extern const Il2CppType SCR_ResetIdle_t350_1_0_0;
struct SCR_ResetIdle_t350;
const Il2CppTypeDefinitionMetadata SCR_ResetIdle_t350_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, SCR_ResetIdle_t350_VTable/* vtableMethods */
	, SCR_ResetIdle_t350_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 368/* fieldStart */

};
TypeInfo SCR_ResetIdle_t350_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "SCR_ResetIdle"/* name */
	, ""/* namespaze */
	, SCR_ResetIdle_t350_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SCR_ResetIdle_t350_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SCR_ResetIdle_t350_0_0_0/* byval_arg */
	, &SCR_ResetIdle_t350_1_0_0/* this_arg */
	, &SCR_ResetIdle_t350_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SCR_ResetIdle_t350)/* instance_size */
	, sizeof (SCR_ResetIdle_t350)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SCR_Respawn
#include "AssemblyU2DCSharp_SCR_Respawn.h"
// Metadata Definition SCR_Respawn
extern TypeInfo SCR_Respawn_t344_il2cpp_TypeInfo;
// SCR_Respawn
#include "AssemblyU2DCSharp_SCR_RespawnMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Respawn::.ctor()
extern const MethodInfo SCR_Respawn__ctor_m1279_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SCR_Respawn__ctor_m1279/* method */
	, &SCR_Respawn_t344_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 214/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Respawn::Start()
extern const MethodInfo SCR_Respawn_Start_m1280_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&SCR_Respawn_Start_m1280/* method */
	, &SCR_Respawn_t344_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 215/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Respawn::StartRespawn()
extern const MethodInfo SCR_Respawn_StartRespawn_m1281_MethodInfo = 
{
	"StartRespawn"/* name */
	, (methodPointerType)&SCR_Respawn_StartRespawn_m1281/* method */
	, &SCR_Respawn_t344_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 216/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Respawn::EndRespawn()
extern const MethodInfo SCR_Respawn_EndRespawn_m1282_MethodInfo = 
{
	"EndRespawn"/* name */
	, (methodPointerType)&SCR_Respawn_EndRespawn_m1282/* method */
	, &SCR_Respawn_t344_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 217/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean SCR_Respawn::get_IsRespawning()
extern const MethodInfo SCR_Respawn_get_IsRespawning_m1283_MethodInfo = 
{
	"get_IsRespawning"/* name */
	, (methodPointerType)&SCR_Respawn_get_IsRespawning_m1283/* method */
	, &SCR_Respawn_t344_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 218/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo SCR_Respawn_t344_SCR_Respawn_set_IsRespawning_m1284_ParameterInfos[] = 
{
	{"value", 0, 134217779, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Respawn::set_IsRespawning(System.Boolean)
extern const MethodInfo SCR_Respawn_set_IsRespawning_m1284_MethodInfo = 
{
	"set_IsRespawning"/* name */
	, (methodPointerType)&SCR_Respawn_set_IsRespawning_m1284/* method */
	, &SCR_Respawn_t344_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274/* invoker_method */
	, SCR_Respawn_t344_SCR_Respawn_set_IsRespawning_m1284_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 219/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SCR_Respawn_t344_MethodInfos[] =
{
	&SCR_Respawn__ctor_m1279_MethodInfo,
	&SCR_Respawn_Start_m1280_MethodInfo,
	&SCR_Respawn_StartRespawn_m1281_MethodInfo,
	&SCR_Respawn_EndRespawn_m1282_MethodInfo,
	&SCR_Respawn_get_IsRespawning_m1283_MethodInfo,
	&SCR_Respawn_set_IsRespawning_m1284_MethodInfo,
	NULL
};
extern const MethodInfo SCR_Respawn_get_IsRespawning_m1283_MethodInfo;
extern const MethodInfo SCR_Respawn_set_IsRespawning_m1284_MethodInfo;
static const PropertyInfo SCR_Respawn_t344____IsRespawning_PropertyInfo = 
{
	&SCR_Respawn_t344_il2cpp_TypeInfo/* parent */
	, "IsRespawning"/* name */
	, &SCR_Respawn_get_IsRespawning_m1283_MethodInfo/* get */
	, &SCR_Respawn_set_IsRespawning_m1284_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* SCR_Respawn_t344_PropertyInfos[] =
{
	&SCR_Respawn_t344____IsRespawning_PropertyInfo,
	NULL
};
static const Il2CppMethodReference SCR_Respawn_t344_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool SCR_Respawn_t344_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType SCR_Respawn_t344_1_0_0;
struct SCR_Respawn_t344;
const Il2CppTypeDefinitionMetadata SCR_Respawn_t344_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, SCR_Respawn_t344_VTable/* vtableMethods */
	, SCR_Respawn_t344_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 369/* fieldStart */

};
TypeInfo SCR_Respawn_t344_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "SCR_Respawn"/* name */
	, ""/* namespaze */
	, SCR_Respawn_t344_MethodInfos/* methods */
	, SCR_Respawn_t344_PropertyInfos/* properties */
	, NULL/* events */
	, &SCR_Respawn_t344_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SCR_Respawn_t344_0_0_0/* byval_arg */
	, &SCR_Respawn_t344_1_0_0/* this_arg */
	, &SCR_Respawn_t344_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SCR_Respawn_t344)/* instance_size */
	, sizeof (SCR_Respawn_t344)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SCR_Wings
#include "AssemblyU2DCSharp_SCR_Wings.h"
// Metadata Definition SCR_Wings
extern TypeInfo SCR_Wings_t351_il2cpp_TypeInfo;
// SCR_Wings
#include "AssemblyU2DCSharp_SCR_WingsMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Wings::.ctor()
extern const MethodInfo SCR_Wings__ctor_m1285_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SCR_Wings__ctor_m1285/* method */
	, &SCR_Wings_t351_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 220/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Wings::Awake()
extern const MethodInfo SCR_Wings_Awake_m1286_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&SCR_Wings_Awake_m1286/* method */
	, &SCR_Wings_t351_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 221/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo SCR_Wings_t351_SCR_Wings_SetVisible_m1287_ParameterInfos[] = 
{
	{"isVisible", 0, 134217780, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Wings::SetVisible(System.Boolean)
extern const MethodInfo SCR_Wings_SetVisible_m1287_MethodInfo = 
{
	"SetVisible"/* name */
	, (methodPointerType)&SCR_Wings_SetVisible_m1287/* method */
	, &SCR_Wings_t351_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274/* invoker_method */
	, SCR_Wings_t351_SCR_Wings_SetVisible_m1287_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 222/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SCR_Wings_t351_MethodInfos[] =
{
	&SCR_Wings__ctor_m1285_MethodInfo,
	&SCR_Wings_Awake_m1286_MethodInfo,
	&SCR_Wings_SetVisible_m1287_MethodInfo,
	NULL
};
static const Il2CppMethodReference SCR_Wings_t351_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool SCR_Wings_t351_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType SCR_Wings_t351_0_0_0;
extern const Il2CppType SCR_Wings_t351_1_0_0;
struct SCR_Wings_t351;
const Il2CppTypeDefinitionMetadata SCR_Wings_t351_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, SCR_Wings_t351_VTable/* vtableMethods */
	, SCR_Wings_t351_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 373/* fieldStart */

};
TypeInfo SCR_Wings_t351_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "SCR_Wings"/* name */
	, ""/* namespaze */
	, SCR_Wings_t351_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SCR_Wings_t351_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SCR_Wings_t351_0_0_0/* byval_arg */
	, &SCR_Wings_t351_1_0_0/* this_arg */
	, &SCR_Wings_t351_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SCR_Wings_t351)/* instance_size */
	, sizeof (SCR_Wings_t351)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SCR_FollowAvatar/<ZoomInOn>c__Iterator7
#include "AssemblyU2DCSharp_SCR_FollowAvatar_U3CZoomInOnU3Ec__Iterator.h"
// Metadata Definition SCR_FollowAvatar/<ZoomInOn>c__Iterator7
extern TypeInfo U3CZoomInOnU3Ec__Iterator7_t353_il2cpp_TypeInfo;
// SCR_FollowAvatar/<ZoomInOn>c__Iterator7
#include "AssemblyU2DCSharp_SCR_FollowAvatar_U3CZoomInOnU3Ec__IteratorMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_FollowAvatar/<ZoomInOn>c__Iterator7::.ctor()
extern const MethodInfo U3CZoomInOnU3Ec__Iterator7__ctor_m1288_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CZoomInOnU3Ec__Iterator7__ctor_m1288/* method */
	, &U3CZoomInOnU3Ec__Iterator7_t353_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 230/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object SCR_FollowAvatar/<ZoomInOn>c__Iterator7::System.Collections.Generic.IEnumerator<object>.get_Current()
extern const MethodInfo U3CZoomInOnU3Ec__Iterator7_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1289_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<object>.get_Current"/* name */
	, (methodPointerType)&U3CZoomInOnU3Ec__Iterator7_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1289/* method */
	, &U3CZoomInOnU3Ec__Iterator7_t353_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 53/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 231/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object SCR_FollowAvatar/<ZoomInOn>c__Iterator7::System.Collections.IEnumerator.get_Current()
extern const MethodInfo U3CZoomInOnU3Ec__Iterator7_System_Collections_IEnumerator_get_Current_m1290_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&U3CZoomInOnU3Ec__Iterator7_System_Collections_IEnumerator_get_Current_m1290/* method */
	, &U3CZoomInOnU3Ec__Iterator7_t353_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 54/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 232/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean SCR_FollowAvatar/<ZoomInOn>c__Iterator7::MoveNext()
extern const MethodInfo U3CZoomInOnU3Ec__Iterator7_MoveNext_m1291_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&U3CZoomInOnU3Ec__Iterator7_MoveNext_m1291/* method */
	, &U3CZoomInOnU3Ec__Iterator7_t353_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 233/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_FollowAvatar/<ZoomInOn>c__Iterator7::Dispose()
extern const MethodInfo U3CZoomInOnU3Ec__Iterator7_Dispose_m1292_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&U3CZoomInOnU3Ec__Iterator7_Dispose_m1292/* method */
	, &U3CZoomInOnU3Ec__Iterator7_t353_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 55/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 234/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_FollowAvatar/<ZoomInOn>c__Iterator7::Reset()
extern const MethodInfo U3CZoomInOnU3Ec__Iterator7_Reset_m1293_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&U3CZoomInOnU3Ec__Iterator7_Reset_m1293/* method */
	, &U3CZoomInOnU3Ec__Iterator7_t353_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 56/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 235/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CZoomInOnU3Ec__Iterator7_t353_MethodInfos[] =
{
	&U3CZoomInOnU3Ec__Iterator7__ctor_m1288_MethodInfo,
	&U3CZoomInOnU3Ec__Iterator7_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1289_MethodInfo,
	&U3CZoomInOnU3Ec__Iterator7_System_Collections_IEnumerator_get_Current_m1290_MethodInfo,
	&U3CZoomInOnU3Ec__Iterator7_MoveNext_m1291_MethodInfo,
	&U3CZoomInOnU3Ec__Iterator7_Dispose_m1292_MethodInfo,
	&U3CZoomInOnU3Ec__Iterator7_Reset_m1293_MethodInfo,
	NULL
};
extern const MethodInfo U3CZoomInOnU3Ec__Iterator7_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1289_MethodInfo;
static const PropertyInfo U3CZoomInOnU3Ec__Iterator7_t353____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo = 
{
	&U3CZoomInOnU3Ec__Iterator7_t353_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<object>.Current"/* name */
	, &U3CZoomInOnU3Ec__Iterator7_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1289_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo U3CZoomInOnU3Ec__Iterator7_System_Collections_IEnumerator_get_Current_m1290_MethodInfo;
static const PropertyInfo U3CZoomInOnU3Ec__Iterator7_t353____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CZoomInOnU3Ec__Iterator7_t353_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CZoomInOnU3Ec__Iterator7_System_Collections_IEnumerator_get_Current_m1290_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* U3CZoomInOnU3Ec__Iterator7_t353_PropertyInfos[] =
{
	&U3CZoomInOnU3Ec__Iterator7_t353____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo,
	&U3CZoomInOnU3Ec__Iterator7_t353____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern const MethodInfo U3CZoomInOnU3Ec__Iterator7_Dispose_m1292_MethodInfo;
extern const MethodInfo U3CZoomInOnU3Ec__Iterator7_MoveNext_m1291_MethodInfo;
extern const MethodInfo U3CZoomInOnU3Ec__Iterator7_Reset_m1293_MethodInfo;
static const Il2CppMethodReference U3CZoomInOnU3Ec__Iterator7_t353_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&U3CZoomInOnU3Ec__Iterator7_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1289_MethodInfo,
	&U3CZoomInOnU3Ec__Iterator7_Dispose_m1292_MethodInfo,
	&U3CZoomInOnU3Ec__Iterator7_System_Collections_IEnumerator_get_Current_m1290_MethodInfo,
	&U3CZoomInOnU3Ec__Iterator7_MoveNext_m1291_MethodInfo,
	&U3CZoomInOnU3Ec__Iterator7_Reset_m1293_MethodInfo,
};
static bool U3CZoomInOnU3Ec__Iterator7_t353_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* U3CZoomInOnU3Ec__Iterator7_t353_InterfacesTypeInfos[] = 
{
	&IEnumerator_1_t284_0_0_0,
	&IDisposable_t233_0_0_0,
	&IEnumerator_t217_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CZoomInOnU3Ec__Iterator7_t353_InterfacesOffsets[] = 
{
	{ &IEnumerator_1_t284_0_0_0, 4},
	{ &IDisposable_t233_0_0_0, 5},
	{ &IEnumerator_t217_0_0_0, 6},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType U3CZoomInOnU3Ec__Iterator7_t353_0_0_0;
extern const Il2CppType U3CZoomInOnU3Ec__Iterator7_t353_1_0_0;
extern TypeInfo SCR_FollowAvatar_t352_il2cpp_TypeInfo;
extern const Il2CppType SCR_FollowAvatar_t352_0_0_0;
struct U3CZoomInOnU3Ec__Iterator7_t353;
const Il2CppTypeDefinitionMetadata U3CZoomInOnU3Ec__Iterator7_t353_DefinitionMetadata = 
{
	&SCR_FollowAvatar_t352_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CZoomInOnU3Ec__Iterator7_t353_InterfacesTypeInfos/* implementedInterfaces */
	, U3CZoomInOnU3Ec__Iterator7_t353_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CZoomInOnU3Ec__Iterator7_t353_VTable/* vtableMethods */
	, U3CZoomInOnU3Ec__Iterator7_t353_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 374/* fieldStart */

};
TypeInfo U3CZoomInOnU3Ec__Iterator7_t353_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<ZoomInOn>c__Iterator7"/* name */
	, ""/* namespaze */
	, U3CZoomInOnU3Ec__Iterator7_t353_MethodInfos/* methods */
	, U3CZoomInOnU3Ec__Iterator7_t353_PropertyInfos/* properties */
	, NULL/* events */
	, &U3CZoomInOnU3Ec__Iterator7_t353_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 52/* custom_attributes_cache */
	, &U3CZoomInOnU3Ec__Iterator7_t353_0_0_0/* byval_arg */
	, &U3CZoomInOnU3Ec__Iterator7_t353_1_0_0/* this_arg */
	, &U3CZoomInOnU3Ec__Iterator7_t353_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CZoomInOnU3Ec__Iterator7_t353)/* instance_size */
	, sizeof (U3CZoomInOnU3Ec__Iterator7_t353)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// SCR_FollowAvatar/<FollowAvatar>c__Iterator8
#include "AssemblyU2DCSharp_SCR_FollowAvatar_U3CFollowAvatarU3Ec__Iter.h"
// Metadata Definition SCR_FollowAvatar/<FollowAvatar>c__Iterator8
extern TypeInfo U3CFollowAvatarU3Ec__Iterator8_t354_il2cpp_TypeInfo;
// SCR_FollowAvatar/<FollowAvatar>c__Iterator8
#include "AssemblyU2DCSharp_SCR_FollowAvatar_U3CFollowAvatarU3Ec__IterMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_FollowAvatar/<FollowAvatar>c__Iterator8::.ctor()
extern const MethodInfo U3CFollowAvatarU3Ec__Iterator8__ctor_m1294_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CFollowAvatarU3Ec__Iterator8__ctor_m1294/* method */
	, &U3CFollowAvatarU3Ec__Iterator8_t354_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 236/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object SCR_FollowAvatar/<FollowAvatar>c__Iterator8::System.Collections.Generic.IEnumerator<object>.get_Current()
extern const MethodInfo U3CFollowAvatarU3Ec__Iterator8_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1295_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<object>.get_Current"/* name */
	, (methodPointerType)&U3CFollowAvatarU3Ec__Iterator8_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1295/* method */
	, &U3CFollowAvatarU3Ec__Iterator8_t354_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 58/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 237/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object SCR_FollowAvatar/<FollowAvatar>c__Iterator8::System.Collections.IEnumerator.get_Current()
extern const MethodInfo U3CFollowAvatarU3Ec__Iterator8_System_Collections_IEnumerator_get_Current_m1296_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&U3CFollowAvatarU3Ec__Iterator8_System_Collections_IEnumerator_get_Current_m1296/* method */
	, &U3CFollowAvatarU3Ec__Iterator8_t354_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 59/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 238/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean SCR_FollowAvatar/<FollowAvatar>c__Iterator8::MoveNext()
extern const MethodInfo U3CFollowAvatarU3Ec__Iterator8_MoveNext_m1297_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&U3CFollowAvatarU3Ec__Iterator8_MoveNext_m1297/* method */
	, &U3CFollowAvatarU3Ec__Iterator8_t354_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 239/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_FollowAvatar/<FollowAvatar>c__Iterator8::Dispose()
extern const MethodInfo U3CFollowAvatarU3Ec__Iterator8_Dispose_m1298_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&U3CFollowAvatarU3Ec__Iterator8_Dispose_m1298/* method */
	, &U3CFollowAvatarU3Ec__Iterator8_t354_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 60/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 240/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_FollowAvatar/<FollowAvatar>c__Iterator8::Reset()
extern const MethodInfo U3CFollowAvatarU3Ec__Iterator8_Reset_m1299_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&U3CFollowAvatarU3Ec__Iterator8_Reset_m1299/* method */
	, &U3CFollowAvatarU3Ec__Iterator8_t354_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 61/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 241/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CFollowAvatarU3Ec__Iterator8_t354_MethodInfos[] =
{
	&U3CFollowAvatarU3Ec__Iterator8__ctor_m1294_MethodInfo,
	&U3CFollowAvatarU3Ec__Iterator8_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1295_MethodInfo,
	&U3CFollowAvatarU3Ec__Iterator8_System_Collections_IEnumerator_get_Current_m1296_MethodInfo,
	&U3CFollowAvatarU3Ec__Iterator8_MoveNext_m1297_MethodInfo,
	&U3CFollowAvatarU3Ec__Iterator8_Dispose_m1298_MethodInfo,
	&U3CFollowAvatarU3Ec__Iterator8_Reset_m1299_MethodInfo,
	NULL
};
extern const MethodInfo U3CFollowAvatarU3Ec__Iterator8_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1295_MethodInfo;
static const PropertyInfo U3CFollowAvatarU3Ec__Iterator8_t354____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo = 
{
	&U3CFollowAvatarU3Ec__Iterator8_t354_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<object>.Current"/* name */
	, &U3CFollowAvatarU3Ec__Iterator8_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1295_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo U3CFollowAvatarU3Ec__Iterator8_System_Collections_IEnumerator_get_Current_m1296_MethodInfo;
static const PropertyInfo U3CFollowAvatarU3Ec__Iterator8_t354____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CFollowAvatarU3Ec__Iterator8_t354_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CFollowAvatarU3Ec__Iterator8_System_Collections_IEnumerator_get_Current_m1296_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* U3CFollowAvatarU3Ec__Iterator8_t354_PropertyInfos[] =
{
	&U3CFollowAvatarU3Ec__Iterator8_t354____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo,
	&U3CFollowAvatarU3Ec__Iterator8_t354____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern const MethodInfo U3CFollowAvatarU3Ec__Iterator8_Dispose_m1298_MethodInfo;
extern const MethodInfo U3CFollowAvatarU3Ec__Iterator8_MoveNext_m1297_MethodInfo;
extern const MethodInfo U3CFollowAvatarU3Ec__Iterator8_Reset_m1299_MethodInfo;
static const Il2CppMethodReference U3CFollowAvatarU3Ec__Iterator8_t354_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&U3CFollowAvatarU3Ec__Iterator8_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1295_MethodInfo,
	&U3CFollowAvatarU3Ec__Iterator8_Dispose_m1298_MethodInfo,
	&U3CFollowAvatarU3Ec__Iterator8_System_Collections_IEnumerator_get_Current_m1296_MethodInfo,
	&U3CFollowAvatarU3Ec__Iterator8_MoveNext_m1297_MethodInfo,
	&U3CFollowAvatarU3Ec__Iterator8_Reset_m1299_MethodInfo,
};
static bool U3CFollowAvatarU3Ec__Iterator8_t354_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* U3CFollowAvatarU3Ec__Iterator8_t354_InterfacesTypeInfos[] = 
{
	&IEnumerator_1_t284_0_0_0,
	&IDisposable_t233_0_0_0,
	&IEnumerator_t217_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CFollowAvatarU3Ec__Iterator8_t354_InterfacesOffsets[] = 
{
	{ &IEnumerator_1_t284_0_0_0, 4},
	{ &IDisposable_t233_0_0_0, 5},
	{ &IEnumerator_t217_0_0_0, 6},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType U3CFollowAvatarU3Ec__Iterator8_t354_0_0_0;
extern const Il2CppType U3CFollowAvatarU3Ec__Iterator8_t354_1_0_0;
struct U3CFollowAvatarU3Ec__Iterator8_t354;
const Il2CppTypeDefinitionMetadata U3CFollowAvatarU3Ec__Iterator8_t354_DefinitionMetadata = 
{
	&SCR_FollowAvatar_t352_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CFollowAvatarU3Ec__Iterator8_t354_InterfacesTypeInfos/* implementedInterfaces */
	, U3CFollowAvatarU3Ec__Iterator8_t354_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CFollowAvatarU3Ec__Iterator8_t354_VTable/* vtableMethods */
	, U3CFollowAvatarU3Ec__Iterator8_t354_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 382/* fieldStart */

};
TypeInfo U3CFollowAvatarU3Ec__Iterator8_t354_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<FollowAvatar>c__Iterator8"/* name */
	, ""/* namespaze */
	, U3CFollowAvatarU3Ec__Iterator8_t354_MethodInfos/* methods */
	, U3CFollowAvatarU3Ec__Iterator8_t354_PropertyInfos/* properties */
	, NULL/* events */
	, &U3CFollowAvatarU3Ec__Iterator8_t354_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 57/* custom_attributes_cache */
	, &U3CFollowAvatarU3Ec__Iterator8_t354_0_0_0/* byval_arg */
	, &U3CFollowAvatarU3Ec__Iterator8_t354_1_0_0/* this_arg */
	, &U3CFollowAvatarU3Ec__Iterator8_t354_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CFollowAvatarU3Ec__Iterator8_t354)/* instance_size */
	, sizeof (U3CFollowAvatarU3Ec__Iterator8_t354)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// SCR_FollowAvatar
#include "AssemblyU2DCSharp_SCR_FollowAvatar.h"
// Metadata Definition SCR_FollowAvatar
// SCR_FollowAvatar
#include "AssemblyU2DCSharp_SCR_FollowAvatarMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_FollowAvatar::.ctor()
extern const MethodInfo SCR_FollowAvatar__ctor_m1300_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SCR_FollowAvatar__ctor_m1300/* method */
	, &SCR_FollowAvatar_t352_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 223/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_FollowAvatar::Start()
extern const MethodInfo SCR_FollowAvatar_Start_m1301_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&SCR_FollowAvatar_Start_m1301/* method */
	, &SCR_FollowAvatar_t352_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 224/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_FollowAvatar::DeleteExtraCameras()
extern const MethodInfo SCR_FollowAvatar_DeleteExtraCameras_m1302_MethodInfo = 
{
	"DeleteExtraCameras"/* name */
	, (methodPointerType)&SCR_FollowAvatar_DeleteExtraCameras_m1302/* method */
	, &SCR_FollowAvatar_t352_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 225/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_FollowAvatar::SetBounds()
extern const MethodInfo SCR_FollowAvatar_SetBounds_m1303_MethodInfo = 
{
	"SetBounds"/* name */
	, (methodPointerType)&SCR_FollowAvatar_SetBounds_m1303/* method */
	, &SCR_FollowAvatar_t352_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 226/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Transform_t1_0_0_0;
extern const Il2CppType Transform_t1_0_0_0;
static const ParameterInfo SCR_FollowAvatar_t352_SCR_FollowAvatar_ZoomInOn_m1304_ParameterInfos[] = 
{
	{"zoomTo", 0, 134217781, 0, &Transform_t1_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.IEnumerator SCR_FollowAvatar::ZoomInOn(UnityEngine.Transform)
extern const MethodInfo SCR_FollowAvatar_ZoomInOn_m1304_MethodInfo = 
{
	"ZoomInOn"/* name */
	, (methodPointerType)&SCR_FollowAvatar_ZoomInOn_m1304/* method */
	, &SCR_FollowAvatar_t352_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t217_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, SCR_FollowAvatar_t352_SCR_FollowAvatar_ZoomInOn_m1304_ParameterInfos/* parameters */
	, 50/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 227/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.IEnumerator SCR_FollowAvatar::FollowAvatar()
extern const MethodInfo SCR_FollowAvatar_FollowAvatar_m1305_MethodInfo = 
{
	"FollowAvatar"/* name */
	, (methodPointerType)&SCR_FollowAvatar_FollowAvatar_m1305/* method */
	, &SCR_FollowAvatar_t352_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t217_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 51/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 228/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector3_t4_0_0_0;
static const ParameterInfo SCR_FollowAvatar_t352_SCR_FollowAvatar_BindPosition_m1306_ParameterInfos[] = 
{
	{"newPosition", 0, 134217782, 0, &Vector3_t4_0_0_0},
};
extern void* RuntimeInvoker_Vector3_t4_Vector3_t4 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector3 SCR_FollowAvatar::BindPosition(UnityEngine.Vector3)
extern const MethodInfo SCR_FollowAvatar_BindPosition_m1306_MethodInfo = 
{
	"BindPosition"/* name */
	, (methodPointerType)&SCR_FollowAvatar_BindPosition_m1306/* method */
	, &SCR_FollowAvatar_t352_il2cpp_TypeInfo/* declaring_type */
	, &Vector3_t4_0_0_0/* return_type */
	, RuntimeInvoker_Vector3_t4_Vector3_t4/* invoker_method */
	, SCR_FollowAvatar_t352_SCR_FollowAvatar_BindPosition_m1306_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 229/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SCR_FollowAvatar_t352_MethodInfos[] =
{
	&SCR_FollowAvatar__ctor_m1300_MethodInfo,
	&SCR_FollowAvatar_Start_m1301_MethodInfo,
	&SCR_FollowAvatar_DeleteExtraCameras_m1302_MethodInfo,
	&SCR_FollowAvatar_SetBounds_m1303_MethodInfo,
	&SCR_FollowAvatar_ZoomInOn_m1304_MethodInfo,
	&SCR_FollowAvatar_FollowAvatar_m1305_MethodInfo,
	&SCR_FollowAvatar_BindPosition_m1306_MethodInfo,
	NULL
};
static const Il2CppType* SCR_FollowAvatar_t352_il2cpp_TypeInfo__nestedTypes[2] =
{
	&U3CZoomInOnU3Ec__Iterator7_t353_0_0_0,
	&U3CFollowAvatarU3Ec__Iterator8_t354_0_0_0,
};
static const Il2CppMethodReference SCR_FollowAvatar_t352_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool SCR_FollowAvatar_t352_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType SCR_FollowAvatar_t352_1_0_0;
struct SCR_FollowAvatar_t352;
const Il2CppTypeDefinitionMetadata SCR_FollowAvatar_t352_DefinitionMetadata = 
{
	NULL/* declaringType */
	, SCR_FollowAvatar_t352_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, SCR_FollowAvatar_t352_VTable/* vtableMethods */
	, SCR_FollowAvatar_t352_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 387/* fieldStart */

};
TypeInfo SCR_FollowAvatar_t352_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "SCR_FollowAvatar"/* name */
	, ""/* namespaze */
	, SCR_FollowAvatar_t352_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SCR_FollowAvatar_t352_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SCR_FollowAvatar_t352_0_0_0/* byval_arg */
	, &SCR_FollowAvatar_t352_1_0_0/* this_arg */
	, &SCR_FollowAvatar_t352_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SCR_FollowAvatar_t352)/* instance_size */
	, sizeof (SCR_FollowAvatar_t352)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(SCR_FollowAvatar_t352_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 9/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SCR_Puceron
#include "AssemblyU2DCSharp_SCR_Puceron.h"
// Metadata Definition SCR_Puceron
extern TypeInfo SCR_Puceron_t356_il2cpp_TypeInfo;
// SCR_Puceron
#include "AssemblyU2DCSharp_SCR_PuceronMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Puceron::.ctor()
extern const MethodInfo SCR_Puceron__ctor_m1307_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SCR_Puceron__ctor_m1307/* method */
	, &SCR_Puceron_t356_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 242/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Puceron::Start()
extern const MethodInfo SCR_Puceron_Start_m1308_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&SCR_Puceron_Start_m1308/* method */
	, &SCR_Puceron_t356_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 243/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Puceron::UnRotate()
extern const MethodInfo SCR_Puceron_UnRotate_m1309_MethodInfo = 
{
	"UnRotate"/* name */
	, (methodPointerType)&SCR_Puceron_UnRotate_m1309/* method */
	, &SCR_Puceron_t356_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 244/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Collider_t138_0_0_0;
static const ParameterInfo SCR_Puceron_t356_SCR_Puceron_OnTriggerEnter_m1310_ParameterInfos[] = 
{
	{"other", 0, 134217783, 0, &Collider_t138_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Puceron::OnTriggerEnter(UnityEngine.Collider)
extern const MethodInfo SCR_Puceron_OnTriggerEnter_m1310_MethodInfo = 
{
	"OnTriggerEnter"/* name */
	, (methodPointerType)&SCR_Puceron_OnTriggerEnter_m1310/* method */
	, &SCR_Puceron_t356_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, SCR_Puceron_t356_SCR_Puceron_OnTriggerEnter_m1310_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 245/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Puceron::Update()
extern const MethodInfo SCR_Puceron_Update_m1311_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&SCR_Puceron_Update_m1311/* method */
	, &SCR_Puceron_t356_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 246/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Puceron::Collect()
extern const MethodInfo SCR_Puceron_Collect_m1312_MethodInfo = 
{
	"Collect"/* name */
	, (methodPointerType)&SCR_Puceron_Collect_m1312/* method */
	, &SCR_Puceron_t356_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 247/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Puceron::Pool()
extern const MethodInfo SCR_Puceron_Pool_m1313_MethodInfo = 
{
	"Pool"/* name */
	, (methodPointerType)&SCR_Puceron_Pool_m1313/* method */
	, &SCR_Puceron_t356_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 248/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Puceron::ReturnToGame()
extern const MethodInfo SCR_Puceron_ReturnToGame_m1314_MethodInfo = 
{
	"ReturnToGame"/* name */
	, (methodPointerType)&SCR_Puceron_ReturnToGame_m1314/* method */
	, &SCR_Puceron_t356_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 249/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SCR_Puceron_t356_MethodInfos[] =
{
	&SCR_Puceron__ctor_m1307_MethodInfo,
	&SCR_Puceron_Start_m1308_MethodInfo,
	&SCR_Puceron_UnRotate_m1309_MethodInfo,
	&SCR_Puceron_OnTriggerEnter_m1310_MethodInfo,
	&SCR_Puceron_Update_m1311_MethodInfo,
	&SCR_Puceron_Collect_m1312_MethodInfo,
	&SCR_Puceron_Pool_m1313_MethodInfo,
	&SCR_Puceron_ReturnToGame_m1314_MethodInfo,
	NULL
};
extern const MethodInfo SCR_Puceron_Collect_m1312_MethodInfo;
static const Il2CppMethodReference SCR_Puceron_t356_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	&SCR_Puceron_Collect_m1312_MethodInfo,
};
static bool SCR_Puceron_t356_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType SCR_Puceron_t356_0_0_0;
extern const Il2CppType SCR_Puceron_t356_1_0_0;
struct SCR_Puceron_t356;
const Il2CppTypeDefinitionMetadata SCR_Puceron_t356_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &SCR_Collectible_t331_0_0_0/* parent */
	, SCR_Puceron_t356_VTable/* vtableMethods */
	, SCR_Puceron_t356_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 396/* fieldStart */

};
TypeInfo SCR_Puceron_t356_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "SCR_Puceron"/* name */
	, ""/* namespaze */
	, SCR_Puceron_t356_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SCR_Puceron_t356_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SCR_Puceron_t356_0_0_0/* byval_arg */
	, &SCR_Puceron_t356_1_0_0/* this_arg */
	, &SCR_Puceron_t356_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SCR_Puceron_t356)/* instance_size */
	, sizeof (SCR_Puceron_t356)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SCR_PuceronMove/TrajectoryType
#include "AssemblyU2DCSharp_SCR_PuceronMove_TrajectoryType.h"
// Metadata Definition SCR_PuceronMove/TrajectoryType
extern TypeInfo TrajectoryType_t357_il2cpp_TypeInfo;
// SCR_PuceronMove/TrajectoryType
#include "AssemblyU2DCSharp_SCR_PuceronMove_TrajectoryTypeMethodDeclarations.h"
static const MethodInfo* TrajectoryType_t357_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference TrajectoryType_t357_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool TrajectoryType_t357_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TrajectoryType_t357_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType TrajectoryType_t357_0_0_0;
extern const Il2CppType TrajectoryType_t357_1_0_0;
extern TypeInfo SCR_PuceronMove_t359_il2cpp_TypeInfo;
extern const Il2CppType SCR_PuceronMove_t359_0_0_0;
const Il2CppTypeDefinitionMetadata TrajectoryType_t357_DefinitionMetadata = 
{
	&SCR_PuceronMove_t359_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TrajectoryType_t357_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, TrajectoryType_t357_VTable/* vtableMethods */
	, TrajectoryType_t357_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 399/* fieldStart */

};
TypeInfo TrajectoryType_t357_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "TrajectoryType"/* name */
	, ""/* namespaze */
	, TrajectoryType_t357_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TrajectoryType_t357_0_0_0/* byval_arg */
	, &TrajectoryType_t357_1_0_0/* this_arg */
	, &TrajectoryType_t357_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TrajectoryType_t357)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TrajectoryType_t357)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// SCR_PuceronMove/<MoveToWaypoint>c__Iterator9
#include "AssemblyU2DCSharp_SCR_PuceronMove_U3CMoveToWaypointU3Ec__Ite.h"
// Metadata Definition SCR_PuceronMove/<MoveToWaypoint>c__Iterator9
extern TypeInfo U3CMoveToWaypointU3Ec__Iterator9_t360_il2cpp_TypeInfo;
// SCR_PuceronMove/<MoveToWaypoint>c__Iterator9
#include "AssemblyU2DCSharp_SCR_PuceronMove_U3CMoveToWaypointU3Ec__IteMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_PuceronMove/<MoveToWaypoint>c__Iterator9::.ctor()
extern const MethodInfo U3CMoveToWaypointU3Ec__Iterator9__ctor_m1315_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CMoveToWaypointU3Ec__Iterator9__ctor_m1315/* method */
	, &U3CMoveToWaypointU3Ec__Iterator9_t360_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 262/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object SCR_PuceronMove/<MoveToWaypoint>c__Iterator9::System.Collections.Generic.IEnumerator<object>.get_Current()
extern const MethodInfo U3CMoveToWaypointU3Ec__Iterator9_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1316_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<object>.get_Current"/* name */
	, (methodPointerType)&U3CMoveToWaypointU3Ec__Iterator9_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1316/* method */
	, &U3CMoveToWaypointU3Ec__Iterator9_t360_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 64/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 263/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object SCR_PuceronMove/<MoveToWaypoint>c__Iterator9::System.Collections.IEnumerator.get_Current()
extern const MethodInfo U3CMoveToWaypointU3Ec__Iterator9_System_Collections_IEnumerator_get_Current_m1317_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&U3CMoveToWaypointU3Ec__Iterator9_System_Collections_IEnumerator_get_Current_m1317/* method */
	, &U3CMoveToWaypointU3Ec__Iterator9_t360_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 65/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 264/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean SCR_PuceronMove/<MoveToWaypoint>c__Iterator9::MoveNext()
extern const MethodInfo U3CMoveToWaypointU3Ec__Iterator9_MoveNext_m1318_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&U3CMoveToWaypointU3Ec__Iterator9_MoveNext_m1318/* method */
	, &U3CMoveToWaypointU3Ec__Iterator9_t360_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 265/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_PuceronMove/<MoveToWaypoint>c__Iterator9::Dispose()
extern const MethodInfo U3CMoveToWaypointU3Ec__Iterator9_Dispose_m1319_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&U3CMoveToWaypointU3Ec__Iterator9_Dispose_m1319/* method */
	, &U3CMoveToWaypointU3Ec__Iterator9_t360_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 66/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 266/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_PuceronMove/<MoveToWaypoint>c__Iterator9::Reset()
extern const MethodInfo U3CMoveToWaypointU3Ec__Iterator9_Reset_m1320_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&U3CMoveToWaypointU3Ec__Iterator9_Reset_m1320/* method */
	, &U3CMoveToWaypointU3Ec__Iterator9_t360_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 67/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 267/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CMoveToWaypointU3Ec__Iterator9_t360_MethodInfos[] =
{
	&U3CMoveToWaypointU3Ec__Iterator9__ctor_m1315_MethodInfo,
	&U3CMoveToWaypointU3Ec__Iterator9_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1316_MethodInfo,
	&U3CMoveToWaypointU3Ec__Iterator9_System_Collections_IEnumerator_get_Current_m1317_MethodInfo,
	&U3CMoveToWaypointU3Ec__Iterator9_MoveNext_m1318_MethodInfo,
	&U3CMoveToWaypointU3Ec__Iterator9_Dispose_m1319_MethodInfo,
	&U3CMoveToWaypointU3Ec__Iterator9_Reset_m1320_MethodInfo,
	NULL
};
extern const MethodInfo U3CMoveToWaypointU3Ec__Iterator9_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1316_MethodInfo;
static const PropertyInfo U3CMoveToWaypointU3Ec__Iterator9_t360____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo = 
{
	&U3CMoveToWaypointU3Ec__Iterator9_t360_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<object>.Current"/* name */
	, &U3CMoveToWaypointU3Ec__Iterator9_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1316_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo U3CMoveToWaypointU3Ec__Iterator9_System_Collections_IEnumerator_get_Current_m1317_MethodInfo;
static const PropertyInfo U3CMoveToWaypointU3Ec__Iterator9_t360____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CMoveToWaypointU3Ec__Iterator9_t360_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CMoveToWaypointU3Ec__Iterator9_System_Collections_IEnumerator_get_Current_m1317_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* U3CMoveToWaypointU3Ec__Iterator9_t360_PropertyInfos[] =
{
	&U3CMoveToWaypointU3Ec__Iterator9_t360____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo,
	&U3CMoveToWaypointU3Ec__Iterator9_t360____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern const MethodInfo U3CMoveToWaypointU3Ec__Iterator9_Dispose_m1319_MethodInfo;
extern const MethodInfo U3CMoveToWaypointU3Ec__Iterator9_MoveNext_m1318_MethodInfo;
extern const MethodInfo U3CMoveToWaypointU3Ec__Iterator9_Reset_m1320_MethodInfo;
static const Il2CppMethodReference U3CMoveToWaypointU3Ec__Iterator9_t360_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&U3CMoveToWaypointU3Ec__Iterator9_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1316_MethodInfo,
	&U3CMoveToWaypointU3Ec__Iterator9_Dispose_m1319_MethodInfo,
	&U3CMoveToWaypointU3Ec__Iterator9_System_Collections_IEnumerator_get_Current_m1317_MethodInfo,
	&U3CMoveToWaypointU3Ec__Iterator9_MoveNext_m1318_MethodInfo,
	&U3CMoveToWaypointU3Ec__Iterator9_Reset_m1320_MethodInfo,
};
static bool U3CMoveToWaypointU3Ec__Iterator9_t360_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* U3CMoveToWaypointU3Ec__Iterator9_t360_InterfacesTypeInfos[] = 
{
	&IEnumerator_1_t284_0_0_0,
	&IDisposable_t233_0_0_0,
	&IEnumerator_t217_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CMoveToWaypointU3Ec__Iterator9_t360_InterfacesOffsets[] = 
{
	{ &IEnumerator_1_t284_0_0_0, 4},
	{ &IDisposable_t233_0_0_0, 5},
	{ &IEnumerator_t217_0_0_0, 6},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType U3CMoveToWaypointU3Ec__Iterator9_t360_0_0_0;
extern const Il2CppType U3CMoveToWaypointU3Ec__Iterator9_t360_1_0_0;
struct U3CMoveToWaypointU3Ec__Iterator9_t360;
const Il2CppTypeDefinitionMetadata U3CMoveToWaypointU3Ec__Iterator9_t360_DefinitionMetadata = 
{
	&SCR_PuceronMove_t359_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CMoveToWaypointU3Ec__Iterator9_t360_InterfacesTypeInfos/* implementedInterfaces */
	, U3CMoveToWaypointU3Ec__Iterator9_t360_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CMoveToWaypointU3Ec__Iterator9_t360_VTable/* vtableMethods */
	, U3CMoveToWaypointU3Ec__Iterator9_t360_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 402/* fieldStart */

};
TypeInfo U3CMoveToWaypointU3Ec__Iterator9_t360_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<MoveToWaypoint>c__Iterator9"/* name */
	, ""/* namespaze */
	, U3CMoveToWaypointU3Ec__Iterator9_t360_MethodInfos/* methods */
	, U3CMoveToWaypointU3Ec__Iterator9_t360_PropertyInfos/* properties */
	, NULL/* events */
	, &U3CMoveToWaypointU3Ec__Iterator9_t360_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 63/* custom_attributes_cache */
	, &U3CMoveToWaypointU3Ec__Iterator9_t360_0_0_0/* byval_arg */
	, &U3CMoveToWaypointU3Ec__Iterator9_t360_1_0_0/* this_arg */
	, &U3CMoveToWaypointU3Ec__Iterator9_t360_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CMoveToWaypointU3Ec__Iterator9_t360)/* instance_size */
	, sizeof (U3CMoveToWaypointU3Ec__Iterator9_t360)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// SCR_PuceronMove
#include "AssemblyU2DCSharp_SCR_PuceronMove.h"
// Metadata Definition SCR_PuceronMove
// SCR_PuceronMove
#include "AssemblyU2DCSharp_SCR_PuceronMoveMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_PuceronMove::.ctor()
extern const MethodInfo SCR_PuceronMove__ctor_m1321_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SCR_PuceronMove__ctor_m1321/* method */
	, &SCR_PuceronMove_t359_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 250/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_PuceronMove::Start()
extern const MethodInfo SCR_PuceronMove_Start_m1322_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&SCR_PuceronMove_Start_m1322/* method */
	, &SCR_PuceronMove_t359_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 251/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_PuceronMove::Update()
extern const MethodInfo SCR_PuceronMove_Update_m1323_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&SCR_PuceronMove_Update_m1323/* method */
	, &SCR_PuceronMove_t359_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 252/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_PuceronMove::Collect()
extern const MethodInfo SCR_PuceronMove_Collect_m1324_MethodInfo = 
{
	"Collect"/* name */
	, (methodPointerType)&SCR_PuceronMove_Collect_m1324/* method */
	, &SCR_PuceronMove_t359_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 253/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_PuceronMove::ReturnToGame()
extern const MethodInfo SCR_PuceronMove_ReturnToGame_m1325_MethodInfo = 
{
	"ReturnToGame"/* name */
	, (methodPointerType)&SCR_PuceronMove_ReturnToGame_m1325/* method */
	, &SCR_PuceronMove_t359_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 254/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// SCR_Waypoint[] SCR_PuceronMove::GetWaypoints()
extern const MethodInfo SCR_PuceronMove_GetWaypoints_m1326_MethodInfo = 
{
	"GetWaypoints"/* name */
	, (methodPointerType)&SCR_PuceronMove_GetWaypoints_m1326/* method */
	, &SCR_PuceronMove_t359_il2cpp_TypeInfo/* declaring_type */
	, &SCR_WaypointU5BU5D_t361_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 255/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SCR_WaypointU5BU5D_t361_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo SCR_PuceronMove_t359_SCR_PuceronMove_SortWaypoints_m1327_ParameterInfos[] = 
{
	{"unorderedArray", 0, 134217784, 0, &SCR_WaypointU5BU5D_t361_0_0_0},
	{"isInit", 1, 134217785, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// SCR_Waypoint[] SCR_PuceronMove::SortWaypoints(SCR_Waypoint[],System.Boolean)
extern const MethodInfo SCR_PuceronMove_SortWaypoints_m1327_MethodInfo = 
{
	"SortWaypoints"/* name */
	, (methodPointerType)&SCR_PuceronMove_SortWaypoints_m1327/* method */
	, &SCR_PuceronMove_t359_il2cpp_TypeInfo/* declaring_type */
	, &SCR_WaypointU5BU5D_t361_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_SByte_t274/* invoker_method */
	, SCR_PuceronMove_t359_SCR_PuceronMove_SortWaypoints_m1327_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 256/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_PuceronMove::UnChildWaypoints()
extern const MethodInfo SCR_PuceronMove_UnChildWaypoints_m1328_MethodInfo = 
{
	"UnChildWaypoints"/* name */
	, (methodPointerType)&SCR_PuceronMove_UnChildWaypoints_m1328/* method */
	, &SCR_PuceronMove_t359_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 257/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// SCR_Waypoint SCR_PuceronMove::GetNextWaypoint()
extern const MethodInfo SCR_PuceronMove_GetNextWaypoint_m1329_MethodInfo = 
{
	"GetNextWaypoint"/* name */
	, (methodPointerType)&SCR_PuceronMove_GetNextWaypoint_m1329/* method */
	, &SCR_PuceronMove_t359_il2cpp_TypeInfo/* declaring_type */
	, &SCR_Waypoint_t358_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 258/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SCR_Waypoint_t358_0_0_0;
static const ParameterInfo SCR_PuceronMove_t359_SCR_PuceronMove_MoveToWaypoint_m1330_ParameterInfos[] = 
{
	{"wp", 0, 134217786, 0, &SCR_Waypoint_t358_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.IEnumerator SCR_PuceronMove::MoveToWaypoint(SCR_Waypoint)
extern const MethodInfo SCR_PuceronMove_MoveToWaypoint_m1330_MethodInfo = 
{
	"MoveToWaypoint"/* name */
	, (methodPointerType)&SCR_PuceronMove_MoveToWaypoint_m1330/* method */
	, &SCR_PuceronMove_t359_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t217_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, SCR_PuceronMove_t359_SCR_PuceronMove_MoveToWaypoint_m1330_ParameterInfos/* parameters */
	, 62/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 259/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_PuceronMove::StartMove()
extern const MethodInfo SCR_PuceronMove_StartMove_m1331_MethodInfo = 
{
	"StartMove"/* name */
	, (methodPointerType)&SCR_PuceronMove_StartMove_m1331/* method */
	, &SCR_PuceronMove_t359_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 260/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_PuceronMove::StopMove()
extern const MethodInfo SCR_PuceronMove_StopMove_m1332_MethodInfo = 
{
	"StopMove"/* name */
	, (methodPointerType)&SCR_PuceronMove_StopMove_m1332/* method */
	, &SCR_PuceronMove_t359_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 261/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SCR_PuceronMove_t359_MethodInfos[] =
{
	&SCR_PuceronMove__ctor_m1321_MethodInfo,
	&SCR_PuceronMove_Start_m1322_MethodInfo,
	&SCR_PuceronMove_Update_m1323_MethodInfo,
	&SCR_PuceronMove_Collect_m1324_MethodInfo,
	&SCR_PuceronMove_ReturnToGame_m1325_MethodInfo,
	&SCR_PuceronMove_GetWaypoints_m1326_MethodInfo,
	&SCR_PuceronMove_SortWaypoints_m1327_MethodInfo,
	&SCR_PuceronMove_UnChildWaypoints_m1328_MethodInfo,
	&SCR_PuceronMove_GetNextWaypoint_m1329_MethodInfo,
	&SCR_PuceronMove_MoveToWaypoint_m1330_MethodInfo,
	&SCR_PuceronMove_StartMove_m1331_MethodInfo,
	&SCR_PuceronMove_StopMove_m1332_MethodInfo,
	NULL
};
static const Il2CppType* SCR_PuceronMove_t359_il2cpp_TypeInfo__nestedTypes[2] =
{
	&TrajectoryType_t357_0_0_0,
	&U3CMoveToWaypointU3Ec__Iterator9_t360_0_0_0,
};
extern const MethodInfo SCR_PuceronMove_Collect_m1324_MethodInfo;
extern const MethodInfo SCR_PuceronMove_GetWaypoints_m1326_MethodInfo;
extern const MethodInfo SCR_PuceronMove_SortWaypoints_m1327_MethodInfo;
extern const MethodInfo SCR_PuceronMove_UnChildWaypoints_m1328_MethodInfo;
extern const MethodInfo SCR_PuceronMove_GetNextWaypoint_m1329_MethodInfo;
extern const MethodInfo SCR_PuceronMove_MoveToWaypoint_m1330_MethodInfo;
extern const MethodInfo SCR_PuceronMove_StartMove_m1331_MethodInfo;
extern const MethodInfo SCR_PuceronMove_StopMove_m1332_MethodInfo;
static const Il2CppMethodReference SCR_PuceronMove_t359_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	&SCR_PuceronMove_Collect_m1324_MethodInfo,
	&SCR_PuceronMove_GetWaypoints_m1326_MethodInfo,
	&SCR_PuceronMove_SortWaypoints_m1327_MethodInfo,
	&SCR_PuceronMove_UnChildWaypoints_m1328_MethodInfo,
	&SCR_PuceronMove_GetNextWaypoint_m1329_MethodInfo,
	&SCR_PuceronMove_MoveToWaypoint_m1330_MethodInfo,
	&SCR_PuceronMove_StartMove_m1331_MethodInfo,
	&SCR_PuceronMove_StopMove_m1332_MethodInfo,
};
static bool SCR_PuceronMove_t359_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* SCR_PuceronMove_t359_InterfacesTypeInfos[] = 
{
	&IFollowWaypoints_t433_0_0_0,
};
static Il2CppInterfaceOffsetPair SCR_PuceronMove_t359_InterfacesOffsets[] = 
{
	{ &IFollowWaypoints_t433_0_0_0, 5},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType SCR_PuceronMove_t359_1_0_0;
struct SCR_PuceronMove_t359;
const Il2CppTypeDefinitionMetadata SCR_PuceronMove_t359_DefinitionMetadata = 
{
	NULL/* declaringType */
	, SCR_PuceronMove_t359_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, SCR_PuceronMove_t359_InterfacesTypeInfos/* implementedInterfaces */
	, SCR_PuceronMove_t359_InterfacesOffsets/* interfaceOffsets */
	, &SCR_Puceron_t356_0_0_0/* parent */
	, SCR_PuceronMove_t359_VTable/* vtableMethods */
	, SCR_PuceronMove_t359_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 408/* fieldStart */

};
TypeInfo SCR_PuceronMove_t359_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "SCR_PuceronMove"/* name */
	, ""/* namespaze */
	, SCR_PuceronMove_t359_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SCR_PuceronMove_t359_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SCR_PuceronMove_t359_0_0_0/* byval_arg */
	, &SCR_PuceronMove_t359_1_0_0/* this_arg */
	, &SCR_PuceronMove_t359_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SCR_PuceronMove_t359)/* instance_size */
	, sizeof (SCR_PuceronMove_t359)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 12/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 12/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// SCR_Star
#include "AssemblyU2DCSharp_SCR_Star.h"
// Metadata Definition SCR_Star
extern TypeInfo SCR_Star_t362_il2cpp_TypeInfo;
// SCR_Star
#include "AssemblyU2DCSharp_SCR_StarMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Star::.ctor()
extern const MethodInfo SCR_Star__ctor_m1333_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SCR_Star__ctor_m1333/* method */
	, &SCR_Star_t362_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 268/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Star::Start()
extern const MethodInfo SCR_Star_Start_m1334_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&SCR_Star_Start_m1334/* method */
	, &SCR_Star_t362_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 269/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Collider_t138_0_0_0;
static const ParameterInfo SCR_Star_t362_SCR_Star_OnTriggerEnter_m1335_ParameterInfos[] = 
{
	{"other", 0, 134217787, 0, &Collider_t138_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Star::OnTriggerEnter(UnityEngine.Collider)
extern const MethodInfo SCR_Star_OnTriggerEnter_m1335_MethodInfo = 
{
	"OnTriggerEnter"/* name */
	, (methodPointerType)&SCR_Star_OnTriggerEnter_m1335/* method */
	, &SCR_Star_t362_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, SCR_Star_t362_SCR_Star_OnTriggerEnter_m1335_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 270/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Star::Kill()
extern const MethodInfo SCR_Star_Kill_m1336_MethodInfo = 
{
	"Kill"/* name */
	, (methodPointerType)&SCR_Star_Kill_m1336/* method */
	, &SCR_Star_t362_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 271/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Star::Collect()
extern const MethodInfo SCR_Star_Collect_m1337_MethodInfo = 
{
	"Collect"/* name */
	, (methodPointerType)&SCR_Star_Collect_m1337/* method */
	, &SCR_Star_t362_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 272/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SCR_Star_t362_MethodInfos[] =
{
	&SCR_Star__ctor_m1333_MethodInfo,
	&SCR_Star_Start_m1334_MethodInfo,
	&SCR_Star_OnTriggerEnter_m1335_MethodInfo,
	&SCR_Star_Kill_m1336_MethodInfo,
	&SCR_Star_Collect_m1337_MethodInfo,
	NULL
};
extern const MethodInfo SCR_Star_Collect_m1337_MethodInfo;
static const Il2CppMethodReference SCR_Star_t362_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	&SCR_Star_Collect_m1337_MethodInfo,
};
static bool SCR_Star_t362_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType SCR_Star_t362_0_0_0;
extern const Il2CppType SCR_Star_t362_1_0_0;
struct SCR_Star_t362;
const Il2CppTypeDefinitionMetadata SCR_Star_t362_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &SCR_Collectible_t331_0_0_0/* parent */
	, SCR_Star_t362_VTable/* vtableMethods */
	, SCR_Star_t362_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 415/* fieldStart */

};
TypeInfo SCR_Star_t362_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "SCR_Star"/* name */
	, ""/* namespaze */
	, SCR_Star_t362_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SCR_Star_t362_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SCR_Star_t362_0_0_0/* byval_arg */
	, &SCR_Star_t362_1_0_0/* this_arg */
	, &SCR_Star_t362_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SCR_Star_t362)/* instance_size */
	, sizeof (SCR_Star_t362)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SCR_FlyButton
#include "AssemblyU2DCSharp_SCR_FlyButton.h"
// Metadata Definition SCR_FlyButton
extern TypeInfo SCR_FlyButton_t345_il2cpp_TypeInfo;
// SCR_FlyButton
#include "AssemblyU2DCSharp_SCR_FlyButtonMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_FlyButton::.ctor()
extern const MethodInfo SCR_FlyButton__ctor_m1338_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SCR_FlyButton__ctor_m1338/* method */
	, &SCR_FlyButton_t345_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 273/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_FlyButton::Start()
extern const MethodInfo SCR_FlyButton_Start_m1339_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&SCR_FlyButton_Start_m1339/* method */
	, &SCR_FlyButton_t345_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 274/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_FlyButton::Update()
extern const MethodInfo SCR_FlyButton_Update_m1340_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&SCR_FlyButton_Update_m1340/* method */
	, &SCR_FlyButton_t345_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 275/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_FlyButton::OnDisable()
extern const MethodInfo SCR_FlyButton_OnDisable_m1341_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&SCR_FlyButton_OnDisable_m1341/* method */
	, &SCR_FlyButton_t345_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 276/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t215_0_0_0;
static const ParameterInfo SCR_FlyButton_t345_SCR_FlyButton_OnPointerDown_m1342_ParameterInfos[] = 
{
	{"eventData", 0, 134217788, 0, &PointerEventData_t215_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_FlyButton::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern const MethodInfo SCR_FlyButton_OnPointerDown_m1342_MethodInfo = 
{
	"OnPointerDown"/* name */
	, (methodPointerType)&SCR_FlyButton_OnPointerDown_m1342/* method */
	, &SCR_FlyButton_t345_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, SCR_FlyButton_t345_SCR_FlyButton_OnPointerDown_m1342_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 277/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t215_0_0_0;
static const ParameterInfo SCR_FlyButton_t345_SCR_FlyButton_OnPointerUp_m1343_ParameterInfos[] = 
{
	{"eventData", 0, 134217789, 0, &PointerEventData_t215_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_FlyButton::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern const MethodInfo SCR_FlyButton_OnPointerUp_m1343_MethodInfo = 
{
	"OnPointerUp"/* name */
	, (methodPointerType)&SCR_FlyButton_OnPointerUp_m1343/* method */
	, &SCR_FlyButton_t345_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, SCR_FlyButton_t345_SCR_FlyButton_OnPointerUp_m1343_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 278/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo SCR_FlyButton_t345_SCR_FlyButton_ShowButton_m1344_ParameterInfos[] = 
{
	{"show", 0, 134217790, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_FlyButton::ShowButton(System.Boolean)
extern const MethodInfo SCR_FlyButton_ShowButton_m1344_MethodInfo = 
{
	"ShowButton"/* name */
	, (methodPointerType)&SCR_FlyButton_ShowButton_m1344/* method */
	, &SCR_FlyButton_t345_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274/* invoker_method */
	, SCR_FlyButton_t345_SCR_FlyButton_ShowButton_m1344_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 279/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean SCR_FlyButton::get_IsPressed()
extern const MethodInfo SCR_FlyButton_get_IsPressed_m1345_MethodInfo = 
{
	"get_IsPressed"/* name */
	, (methodPointerType)&SCR_FlyButton_get_IsPressed_m1345/* method */
	, &SCR_FlyButton_t345_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 280/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo SCR_FlyButton_t345_SCR_FlyButton_set_IsPressed_m1346_ParameterInfos[] = 
{
	{"value", 0, 134217791, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_FlyButton::set_IsPressed(System.Boolean)
extern const MethodInfo SCR_FlyButton_set_IsPressed_m1346_MethodInfo = 
{
	"set_IsPressed"/* name */
	, (methodPointerType)&SCR_FlyButton_set_IsPressed_m1346/* method */
	, &SCR_FlyButton_t345_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274/* invoker_method */
	, SCR_FlyButton_t345_SCR_FlyButton_set_IsPressed_m1346_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 281/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SCR_FlyButton_t345_MethodInfos[] =
{
	&SCR_FlyButton__ctor_m1338_MethodInfo,
	&SCR_FlyButton_Start_m1339_MethodInfo,
	&SCR_FlyButton_Update_m1340_MethodInfo,
	&SCR_FlyButton_OnDisable_m1341_MethodInfo,
	&SCR_FlyButton_OnPointerDown_m1342_MethodInfo,
	&SCR_FlyButton_OnPointerUp_m1343_MethodInfo,
	&SCR_FlyButton_ShowButton_m1344_MethodInfo,
	&SCR_FlyButton_get_IsPressed_m1345_MethodInfo,
	&SCR_FlyButton_set_IsPressed_m1346_MethodInfo,
	NULL
};
extern const MethodInfo SCR_FlyButton_get_IsPressed_m1345_MethodInfo;
extern const MethodInfo SCR_FlyButton_set_IsPressed_m1346_MethodInfo;
static const PropertyInfo SCR_FlyButton_t345____IsPressed_PropertyInfo = 
{
	&SCR_FlyButton_t345_il2cpp_TypeInfo/* parent */
	, "IsPressed"/* name */
	, &SCR_FlyButton_get_IsPressed_m1345_MethodInfo/* get */
	, &SCR_FlyButton_set_IsPressed_m1346_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* SCR_FlyButton_t345_PropertyInfos[] =
{
	&SCR_FlyButton_t345____IsPressed_PropertyInfo,
	NULL
};
extern const MethodInfo SCR_FlyButton_OnPointerDown_m1342_MethodInfo;
extern const MethodInfo SCR_FlyButton_OnPointerUp_m1343_MethodInfo;
static const Il2CppMethodReference SCR_FlyButton_t345_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	&SCR_FlyButton_OnPointerDown_m1342_MethodInfo,
	&SCR_FlyButton_OnPointerUp_m1343_MethodInfo,
};
static bool SCR_FlyButton_t345_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IPointerDownHandler_t280_0_0_0;
extern const Il2CppType IPointerUpHandler_t282_0_0_0;
static const Il2CppType* SCR_FlyButton_t345_InterfacesTypeInfos[] = 
{
	&IEventSystemHandler_t281_0_0_0,
	&IPointerDownHandler_t280_0_0_0,
	&IPointerUpHandler_t282_0_0_0,
};
static Il2CppInterfaceOffsetPair SCR_FlyButton_t345_InterfacesOffsets[] = 
{
	{ &IEventSystemHandler_t281_0_0_0, 4},
	{ &IPointerDownHandler_t280_0_0_0, 4},
	{ &IPointerUpHandler_t282_0_0_0, 5},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType SCR_FlyButton_t345_0_0_0;
extern const Il2CppType SCR_FlyButton_t345_1_0_0;
struct SCR_FlyButton_t345;
const Il2CppTypeDefinitionMetadata SCR_FlyButton_t345_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, SCR_FlyButton_t345_InterfacesTypeInfos/* implementedInterfaces */
	, SCR_FlyButton_t345_InterfacesOffsets/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, SCR_FlyButton_t345_VTable/* vtableMethods */
	, SCR_FlyButton_t345_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 416/* fieldStart */

};
TypeInfo SCR_FlyButton_t345_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "SCR_FlyButton"/* name */
	, ""/* namespaze */
	, SCR_FlyButton_t345_MethodInfos/* methods */
	, SCR_FlyButton_t345_PropertyInfos/* properties */
	, NULL/* events */
	, &SCR_FlyButton_t345_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SCR_FlyButton_t345_0_0_0/* byval_arg */
	, &SCR_FlyButton_t345_1_0_0/* this_arg */
	, &SCR_FlyButton_t345_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SCR_FlyButton_t345)/* instance_size */
	, sizeof (SCR_FlyButton_t345)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 1/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// SCR_Joystick/Direction
#include "AssemblyU2DCSharp_SCR_Joystick_Direction.h"
// Metadata Definition SCR_Joystick/Direction
extern TypeInfo Direction_t363_il2cpp_TypeInfo;
// SCR_Joystick/Direction
#include "AssemblyU2DCSharp_SCR_Joystick_DirectionMethodDeclarations.h"
static const MethodInfo* Direction_t363_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference Direction_t363_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool Direction_t363_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Direction_t363_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType Direction_t363_0_0_0;
extern const Il2CppType Direction_t363_1_0_0;
extern TypeInfo SCR_Joystick_t346_il2cpp_TypeInfo;
extern const Il2CppType SCR_Joystick_t346_0_0_0;
const Il2CppTypeDefinitionMetadata Direction_t363_DefinitionMetadata = 
{
	&SCR_Joystick_t346_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Direction_t363_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, Direction_t363_VTable/* vtableMethods */
	, Direction_t363_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 420/* fieldStart */

};
TypeInfo Direction_t363_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "Direction"/* name */
	, ""/* namespaze */
	, Direction_t363_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Direction_t363_0_0_0/* byval_arg */
	, &Direction_t363_1_0_0/* this_arg */
	, &Direction_t363_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Direction_t363)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Direction_t363)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// SCR_Joystick
#include "AssemblyU2DCSharp_SCR_Joystick.h"
// Metadata Definition SCR_Joystick
// SCR_Joystick
#include "AssemblyU2DCSharp_SCR_JoystickMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Joystick::.ctor()
extern const MethodInfo SCR_Joystick__ctor_m1347_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SCR_Joystick__ctor_m1347/* method */
	, &SCR_Joystick_t346_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 282/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Joystick::Start()
extern const MethodInfo SCR_Joystick_Start_m1348_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&SCR_Joystick_Start_m1348/* method */
	, &SCR_Joystick_t346_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 283/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Joystick::OnLevelWasLoaded()
extern const MethodInfo SCR_Joystick_OnLevelWasLoaded_m1349_MethodInfo = 
{
	"OnLevelWasLoaded"/* name */
	, (methodPointerType)&SCR_Joystick_OnLevelWasLoaded_m1349/* method */
	, &SCR_Joystick_t346_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 284/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Joystick::OnDisable()
extern const MethodInfo SCR_Joystick_OnDisable_m1350_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&SCR_Joystick_OnDisable_m1350/* method */
	, &SCR_Joystick_t346_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 285/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Joystick::Update()
extern const MethodInfo SCR_Joystick_Update_m1351_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&SCR_Joystick_Update_m1351/* method */
	, &SCR_Joystick_t346_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 286/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t215_0_0_0;
static const ParameterInfo SCR_Joystick_t346_SCR_Joystick_OnPointerDown_m1352_ParameterInfos[] = 
{
	{"eventData", 0, 134217792, 0, &PointerEventData_t215_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Joystick::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern const MethodInfo SCR_Joystick_OnPointerDown_m1352_MethodInfo = 
{
	"OnPointerDown"/* name */
	, (methodPointerType)&SCR_Joystick_OnPointerDown_m1352/* method */
	, &SCR_Joystick_t346_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, SCR_Joystick_t346_SCR_Joystick_OnPointerDown_m1352_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 287/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t215_0_0_0;
static const ParameterInfo SCR_Joystick_t346_SCR_Joystick_OnPointerUp_m1353_ParameterInfos[] = 
{
	{"eventData", 0, 134217793, 0, &PointerEventData_t215_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Joystick::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern const MethodInfo SCR_Joystick_OnPointerUp_m1353_MethodInfo = 
{
	"OnPointerUp"/* name */
	, (methodPointerType)&SCR_Joystick_OnPointerUp_m1353/* method */
	, &SCR_Joystick_t346_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, SCR_Joystick_t346_SCR_Joystick_OnPointerUp_m1353_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 288/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Joystick::FollowTouch()
extern const MethodInfo SCR_Joystick_FollowTouch_m1354_MethodInfo = 
{
	"FollowTouch"/* name */
	, (methodPointerType)&SCR_Joystick_FollowTouch_m1354/* method */
	, &SCR_Joystick_t346_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 289/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Joystick::AdjustPosition()
extern const MethodInfo SCR_Joystick_AdjustPosition_m1355_MethodInfo = 
{
	"AdjustPosition"/* name */
	, (methodPointerType)&SCR_Joystick_AdjustPosition_m1355/* method */
	, &SCR_Joystick_t346_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 290/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector3_t4_0_0_0;
static const ParameterInfo SCR_Joystick_t346_SCR_Joystick_ToAvatarAngle_m1356_ParameterInfos[] = 
{
	{"vector", 0, 134217794, 0, &Vector3_t4_0_0_0},
};
extern void* RuntimeInvoker_Vector3_t4_Vector3_t4 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector3 SCR_Joystick::ToAvatarAngle(UnityEngine.Vector3)
extern const MethodInfo SCR_Joystick_ToAvatarAngle_m1356_MethodInfo = 
{
	"ToAvatarAngle"/* name */
	, (methodPointerType)&SCR_Joystick_ToAvatarAngle_m1356/* method */
	, &SCR_Joystick_t346_il2cpp_TypeInfo/* declaring_type */
	, &Vector3_t4_0_0_0/* return_type */
	, RuntimeInvoker_Vector3_t4_Vector3_t4/* invoker_method */
	, SCR_Joystick_t346_SCR_Joystick_ToAvatarAngle_m1356_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 291/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Direction_t363 (const MethodInfo* method, void* obj, void** args);
// SCR_Joystick/Direction SCR_Joystick::GetClosestDirection()
extern const MethodInfo SCR_Joystick_GetClosestDirection_m1357_MethodInfo = 
{
	"GetClosestDirection"/* name */
	, (methodPointerType)&SCR_Joystick_GetClosestDirection_m1357/* method */
	, &SCR_Joystick_t346_il2cpp_TypeInfo/* declaring_type */
	, &Direction_t363_0_0_0/* return_type */
	, RuntimeInvoker_Direction_t363/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 292/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Direction_t363_0_0_0;
static const ParameterInfo SCR_Joystick_t346_SCR_Joystick_SwitchDirections_m1358_ParameterInfos[] = 
{
	{"newDir", 0, 134217795, 0, &Direction_t363_0_0_0},
};
extern void* RuntimeInvoker_Direction_t363_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// SCR_Joystick/Direction SCR_Joystick::SwitchDirections(SCR_Joystick/Direction)
extern const MethodInfo SCR_Joystick_SwitchDirections_m1358_MethodInfo = 
{
	"SwitchDirections"/* name */
	, (methodPointerType)&SCR_Joystick_SwitchDirections_m1358/* method */
	, &SCR_Joystick_t346_il2cpp_TypeInfo/* declaring_type */
	, &Direction_t363_0_0_0/* return_type */
	, RuntimeInvoker_Direction_t363_Int32_t253/* invoker_method */
	, SCR_Joystick_t346_SCR_Joystick_SwitchDirections_m1358_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 293/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean SCR_Joystick::get_IsDragging()
extern const MethodInfo SCR_Joystick_get_IsDragging_m1359_MethodInfo = 
{
	"get_IsDragging"/* name */
	, (methodPointerType)&SCR_Joystick_get_IsDragging_m1359/* method */
	, &SCR_Joystick_t346_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 294/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single SCR_Joystick::get_HorizontalAxis()
extern const MethodInfo SCR_Joystick_get_HorizontalAxis_m1360_MethodInfo = 
{
	"get_HorizontalAxis"/* name */
	, (methodPointerType)&SCR_Joystick_get_HorizontalAxis_m1360/* method */
	, &SCR_Joystick_t346_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 295/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single SCR_Joystick::get_VerticalAxis()
extern const MethodInfo SCR_Joystick_get_VerticalAxis_m1361_MethodInfo = 
{
	"get_VerticalAxis"/* name */
	, (methodPointerType)&SCR_Joystick_get_VerticalAxis_m1361/* method */
	, &SCR_Joystick_t346_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 296/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Direction_t363 (const MethodInfo* method, void* obj, void** args);
// SCR_Joystick/Direction SCR_Joystick::get_PointingTo()
extern const MethodInfo SCR_Joystick_get_PointingTo_m1362_MethodInfo = 
{
	"get_PointingTo"/* name */
	, (methodPointerType)&SCR_Joystick_get_PointingTo_m1362/* method */
	, &SCR_Joystick_t346_il2cpp_TypeInfo/* declaring_type */
	, &Direction_t363_0_0_0/* return_type */
	, RuntimeInvoker_Direction_t363/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 297/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SCR_Joystick_t346_MethodInfos[] =
{
	&SCR_Joystick__ctor_m1347_MethodInfo,
	&SCR_Joystick_Start_m1348_MethodInfo,
	&SCR_Joystick_OnLevelWasLoaded_m1349_MethodInfo,
	&SCR_Joystick_OnDisable_m1350_MethodInfo,
	&SCR_Joystick_Update_m1351_MethodInfo,
	&SCR_Joystick_OnPointerDown_m1352_MethodInfo,
	&SCR_Joystick_OnPointerUp_m1353_MethodInfo,
	&SCR_Joystick_FollowTouch_m1354_MethodInfo,
	&SCR_Joystick_AdjustPosition_m1355_MethodInfo,
	&SCR_Joystick_ToAvatarAngle_m1356_MethodInfo,
	&SCR_Joystick_GetClosestDirection_m1357_MethodInfo,
	&SCR_Joystick_SwitchDirections_m1358_MethodInfo,
	&SCR_Joystick_get_IsDragging_m1359_MethodInfo,
	&SCR_Joystick_get_HorizontalAxis_m1360_MethodInfo,
	&SCR_Joystick_get_VerticalAxis_m1361_MethodInfo,
	&SCR_Joystick_get_PointingTo_m1362_MethodInfo,
	NULL
};
extern const MethodInfo SCR_Joystick_get_IsDragging_m1359_MethodInfo;
static const PropertyInfo SCR_Joystick_t346____IsDragging_PropertyInfo = 
{
	&SCR_Joystick_t346_il2cpp_TypeInfo/* parent */
	, "IsDragging"/* name */
	, &SCR_Joystick_get_IsDragging_m1359_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo SCR_Joystick_get_HorizontalAxis_m1360_MethodInfo;
static const PropertyInfo SCR_Joystick_t346____HorizontalAxis_PropertyInfo = 
{
	&SCR_Joystick_t346_il2cpp_TypeInfo/* parent */
	, "HorizontalAxis"/* name */
	, &SCR_Joystick_get_HorizontalAxis_m1360_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo SCR_Joystick_get_VerticalAxis_m1361_MethodInfo;
static const PropertyInfo SCR_Joystick_t346____VerticalAxis_PropertyInfo = 
{
	&SCR_Joystick_t346_il2cpp_TypeInfo/* parent */
	, "VerticalAxis"/* name */
	, &SCR_Joystick_get_VerticalAxis_m1361_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo SCR_Joystick_get_PointingTo_m1362_MethodInfo;
static const PropertyInfo SCR_Joystick_t346____PointingTo_PropertyInfo = 
{
	&SCR_Joystick_t346_il2cpp_TypeInfo/* parent */
	, "PointingTo"/* name */
	, &SCR_Joystick_get_PointingTo_m1362_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* SCR_Joystick_t346_PropertyInfos[] =
{
	&SCR_Joystick_t346____IsDragging_PropertyInfo,
	&SCR_Joystick_t346____HorizontalAxis_PropertyInfo,
	&SCR_Joystick_t346____VerticalAxis_PropertyInfo,
	&SCR_Joystick_t346____PointingTo_PropertyInfo,
	NULL
};
static const Il2CppType* SCR_Joystick_t346_il2cpp_TypeInfo__nestedTypes[1] =
{
	&Direction_t363_0_0_0,
};
extern const MethodInfo SCR_Joystick_OnPointerDown_m1352_MethodInfo;
extern const MethodInfo SCR_Joystick_OnPointerUp_m1353_MethodInfo;
static const Il2CppMethodReference SCR_Joystick_t346_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	&SCR_Joystick_OnPointerDown_m1352_MethodInfo,
	&SCR_Joystick_OnPointerUp_m1353_MethodInfo,
};
static bool SCR_Joystick_t346_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* SCR_Joystick_t346_InterfacesTypeInfos[] = 
{
	&IEventSystemHandler_t281_0_0_0,
	&IPointerDownHandler_t280_0_0_0,
	&IPointerUpHandler_t282_0_0_0,
};
static Il2CppInterfaceOffsetPair SCR_Joystick_t346_InterfacesOffsets[] = 
{
	{ &IEventSystemHandler_t281_0_0_0, 4},
	{ &IPointerDownHandler_t280_0_0_0, 4},
	{ &IPointerUpHandler_t282_0_0_0, 5},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType SCR_Joystick_t346_1_0_0;
struct SCR_Joystick_t346;
const Il2CppTypeDefinitionMetadata SCR_Joystick_t346_DefinitionMetadata = 
{
	NULL/* declaringType */
	, SCR_Joystick_t346_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, SCR_Joystick_t346_InterfacesTypeInfos/* implementedInterfaces */
	, SCR_Joystick_t346_InterfacesOffsets/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, SCR_Joystick_t346_VTable/* vtableMethods */
	, SCR_Joystick_t346_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 423/* fieldStart */

};
TypeInfo SCR_Joystick_t346_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "SCR_Joystick"/* name */
	, ""/* namespaze */
	, SCR_Joystick_t346_MethodInfos/* methods */
	, SCR_Joystick_t346_PropertyInfos/* properties */
	, NULL/* events */
	, &SCR_Joystick_t346_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SCR_Joystick_t346_0_0_0/* byval_arg */
	, &SCR_Joystick_t346_1_0_0/* this_arg */
	, &SCR_Joystick_t346_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SCR_Joystick_t346)/* instance_size */
	, sizeof (SCR_Joystick_t346)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 16/* method_count */
	, 4/* property_count */
	, 13/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 6/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// SCR_JoystickDirection
#include "AssemblyU2DCSharp_SCR_JoystickDirection.h"
// Metadata Definition SCR_JoystickDirection
extern TypeInfo SCR_JoystickDirection_t366_il2cpp_TypeInfo;
// SCR_JoystickDirection
#include "AssemblyU2DCSharp_SCR_JoystickDirectionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_JoystickDirection::.ctor()
extern const MethodInfo SCR_JoystickDirection__ctor_m1363_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SCR_JoystickDirection__ctor_m1363/* method */
	, &SCR_JoystickDirection_t366_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 298/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SCR_JoystickDirection_t366_MethodInfos[] =
{
	&SCR_JoystickDirection__ctor_m1363_MethodInfo,
	NULL
};
static const Il2CppMethodReference SCR_JoystickDirection_t366_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool SCR_JoystickDirection_t366_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType SCR_JoystickDirection_t366_0_0_0;
extern const Il2CppType SCR_JoystickDirection_t366_1_0_0;
struct SCR_JoystickDirection_t366;
const Il2CppTypeDefinitionMetadata SCR_JoystickDirection_t366_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, SCR_JoystickDirection_t366_VTable/* vtableMethods */
	, SCR_JoystickDirection_t366_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 436/* fieldStart */

};
TypeInfo SCR_JoystickDirection_t366_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "SCR_JoystickDirection"/* name */
	, ""/* namespaze */
	, SCR_JoystickDirection_t366_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SCR_JoystickDirection_t366_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SCR_JoystickDirection_t366_0_0_0/* byval_arg */
	, &SCR_JoystickDirection_t366_1_0_0/* this_arg */
	, &SCR_JoystickDirection_t366_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SCR_JoystickDirection_t366)/* instance_size */
	, sizeof (SCR_JoystickDirection_t366)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SCR_LevelEndMenu
#include "AssemblyU2DCSharp_SCR_LevelEndMenu.h"
// Metadata Definition SCR_LevelEndMenu
extern TypeInfo SCR_LevelEndMenu_t367_il2cpp_TypeInfo;
// SCR_LevelEndMenu
#include "AssemblyU2DCSharp_SCR_LevelEndMenuMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_LevelEndMenu::.ctor()
extern const MethodInfo SCR_LevelEndMenu__ctor_m1364_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SCR_LevelEndMenu__ctor_m1364/* method */
	, &SCR_LevelEndMenu_t367_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 299/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_LevelEndMenu::Awake()
extern const MethodInfo SCR_LevelEndMenu_Awake_m1365_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&SCR_LevelEndMenu_Awake_m1365/* method */
	, &SCR_LevelEndMenu_t367_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 300/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_LevelEndMenu::OnEnable()
extern const MethodInfo SCR_LevelEndMenu_OnEnable_m1366_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&SCR_LevelEndMenu_OnEnable_m1366/* method */
	, &SCR_LevelEndMenu_t367_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 301/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_LevelEndMenu::AssignButtonImages()
extern const MethodInfo SCR_LevelEndMenu_AssignButtonImages_m1367_MethodInfo = 
{
	"AssignButtonImages"/* name */
	, (methodPointerType)&SCR_LevelEndMenu_AssignButtonImages_m1367/* method */
	, &SCR_LevelEndMenu_t367_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 302/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_LevelEndMenu::Reload()
extern const MethodInfo SCR_LevelEndMenu_Reload_m1368_MethodInfo = 
{
	"Reload"/* name */
	, (methodPointerType)&SCR_LevelEndMenu_Reload_m1368/* method */
	, &SCR_LevelEndMenu_t367_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 303/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_LevelEndMenu::LoadNext()
extern const MethodInfo SCR_LevelEndMenu_LoadNext_m1369_MethodInfo = 
{
	"LoadNext"/* name */
	, (methodPointerType)&SCR_LevelEndMenu_LoadNext_m1369/* method */
	, &SCR_LevelEndMenu_t367_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 304/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_LevelEndMenu::NewHighScore()
extern const MethodInfo SCR_LevelEndMenu_NewHighScore_m1370_MethodInfo = 
{
	"NewHighScore"/* name */
	, (methodPointerType)&SCR_LevelEndMenu_NewHighScore_m1370/* method */
	, &SCR_LevelEndMenu_t367_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 305/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SCR_LevelEndMenu_t367_MethodInfos[] =
{
	&SCR_LevelEndMenu__ctor_m1364_MethodInfo,
	&SCR_LevelEndMenu_Awake_m1365_MethodInfo,
	&SCR_LevelEndMenu_OnEnable_m1366_MethodInfo,
	&SCR_LevelEndMenu_AssignButtonImages_m1367_MethodInfo,
	&SCR_LevelEndMenu_Reload_m1368_MethodInfo,
	&SCR_LevelEndMenu_LoadNext_m1369_MethodInfo,
	&SCR_LevelEndMenu_NewHighScore_m1370_MethodInfo,
	NULL
};
static const Il2CppMethodReference SCR_LevelEndMenu_t367_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool SCR_LevelEndMenu_t367_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType SCR_LevelEndMenu_t367_0_0_0;
extern const Il2CppType SCR_LevelEndMenu_t367_1_0_0;
struct SCR_LevelEndMenu_t367;
const Il2CppTypeDefinitionMetadata SCR_LevelEndMenu_t367_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &SCR_Menu_t336_0_0_0/* parent */
	, SCR_LevelEndMenu_t367_VTable/* vtableMethods */
	, SCR_LevelEndMenu_t367_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 437/* fieldStart */

};
TypeInfo SCR_LevelEndMenu_t367_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "SCR_LevelEndMenu"/* name */
	, ""/* namespaze */
	, SCR_LevelEndMenu_t367_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SCR_LevelEndMenu_t367_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SCR_LevelEndMenu_t367_0_0_0/* byval_arg */
	, &SCR_LevelEndMenu_t367_1_0_0/* this_arg */
	, &SCR_LevelEndMenu_t367_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SCR_LevelEndMenu_t367)/* instance_size */
	, sizeof (SCR_LevelEndMenu_t367)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(SCR_LevelEndMenu_t367_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SCR_LevelStartCountdown/<Rescale>c__IteratorA
#include "AssemblyU2DCSharp_SCR_LevelStartCountdown_U3CRescaleU3Ec__It.h"
// Metadata Definition SCR_LevelStartCountdown/<Rescale>c__IteratorA
extern TypeInfo U3CRescaleU3Ec__IteratorA_t371_il2cpp_TypeInfo;
// SCR_LevelStartCountdown/<Rescale>c__IteratorA
#include "AssemblyU2DCSharp_SCR_LevelStartCountdown_U3CRescaleU3Ec__ItMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_LevelStartCountdown/<Rescale>c__IteratorA::.ctor()
extern const MethodInfo U3CRescaleU3Ec__IteratorA__ctor_m1371_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CRescaleU3Ec__IteratorA__ctor_m1371/* method */
	, &U3CRescaleU3Ec__IteratorA_t371_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 315/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object SCR_LevelStartCountdown/<Rescale>c__IteratorA::System.Collections.Generic.IEnumerator<object>.get_Current()
extern const MethodInfo U3CRescaleU3Ec__IteratorA_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1372_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<object>.get_Current"/* name */
	, (methodPointerType)&U3CRescaleU3Ec__IteratorA_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1372/* method */
	, &U3CRescaleU3Ec__IteratorA_t371_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 70/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 316/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object SCR_LevelStartCountdown/<Rescale>c__IteratorA::System.Collections.IEnumerator.get_Current()
extern const MethodInfo U3CRescaleU3Ec__IteratorA_System_Collections_IEnumerator_get_Current_m1373_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&U3CRescaleU3Ec__IteratorA_System_Collections_IEnumerator_get_Current_m1373/* method */
	, &U3CRescaleU3Ec__IteratorA_t371_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 71/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 317/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean SCR_LevelStartCountdown/<Rescale>c__IteratorA::MoveNext()
extern const MethodInfo U3CRescaleU3Ec__IteratorA_MoveNext_m1374_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&U3CRescaleU3Ec__IteratorA_MoveNext_m1374/* method */
	, &U3CRescaleU3Ec__IteratorA_t371_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 318/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_LevelStartCountdown/<Rescale>c__IteratorA::Dispose()
extern const MethodInfo U3CRescaleU3Ec__IteratorA_Dispose_m1375_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&U3CRescaleU3Ec__IteratorA_Dispose_m1375/* method */
	, &U3CRescaleU3Ec__IteratorA_t371_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 72/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 319/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_LevelStartCountdown/<Rescale>c__IteratorA::Reset()
extern const MethodInfo U3CRescaleU3Ec__IteratorA_Reset_m1376_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&U3CRescaleU3Ec__IteratorA_Reset_m1376/* method */
	, &U3CRescaleU3Ec__IteratorA_t371_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 73/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 320/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CRescaleU3Ec__IteratorA_t371_MethodInfos[] =
{
	&U3CRescaleU3Ec__IteratorA__ctor_m1371_MethodInfo,
	&U3CRescaleU3Ec__IteratorA_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1372_MethodInfo,
	&U3CRescaleU3Ec__IteratorA_System_Collections_IEnumerator_get_Current_m1373_MethodInfo,
	&U3CRescaleU3Ec__IteratorA_MoveNext_m1374_MethodInfo,
	&U3CRescaleU3Ec__IteratorA_Dispose_m1375_MethodInfo,
	&U3CRescaleU3Ec__IteratorA_Reset_m1376_MethodInfo,
	NULL
};
extern const MethodInfo U3CRescaleU3Ec__IteratorA_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1372_MethodInfo;
static const PropertyInfo U3CRescaleU3Ec__IteratorA_t371____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo = 
{
	&U3CRescaleU3Ec__IteratorA_t371_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<object>.Current"/* name */
	, &U3CRescaleU3Ec__IteratorA_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1372_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo U3CRescaleU3Ec__IteratorA_System_Collections_IEnumerator_get_Current_m1373_MethodInfo;
static const PropertyInfo U3CRescaleU3Ec__IteratorA_t371____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CRescaleU3Ec__IteratorA_t371_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CRescaleU3Ec__IteratorA_System_Collections_IEnumerator_get_Current_m1373_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* U3CRescaleU3Ec__IteratorA_t371_PropertyInfos[] =
{
	&U3CRescaleU3Ec__IteratorA_t371____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo,
	&U3CRescaleU3Ec__IteratorA_t371____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern const MethodInfo U3CRescaleU3Ec__IteratorA_Dispose_m1375_MethodInfo;
extern const MethodInfo U3CRescaleU3Ec__IteratorA_MoveNext_m1374_MethodInfo;
extern const MethodInfo U3CRescaleU3Ec__IteratorA_Reset_m1376_MethodInfo;
static const Il2CppMethodReference U3CRescaleU3Ec__IteratorA_t371_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&U3CRescaleU3Ec__IteratorA_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1372_MethodInfo,
	&U3CRescaleU3Ec__IteratorA_Dispose_m1375_MethodInfo,
	&U3CRescaleU3Ec__IteratorA_System_Collections_IEnumerator_get_Current_m1373_MethodInfo,
	&U3CRescaleU3Ec__IteratorA_MoveNext_m1374_MethodInfo,
	&U3CRescaleU3Ec__IteratorA_Reset_m1376_MethodInfo,
};
static bool U3CRescaleU3Ec__IteratorA_t371_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* U3CRescaleU3Ec__IteratorA_t371_InterfacesTypeInfos[] = 
{
	&IEnumerator_1_t284_0_0_0,
	&IDisposable_t233_0_0_0,
	&IEnumerator_t217_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CRescaleU3Ec__IteratorA_t371_InterfacesOffsets[] = 
{
	{ &IEnumerator_1_t284_0_0_0, 4},
	{ &IDisposable_t233_0_0_0, 5},
	{ &IEnumerator_t217_0_0_0, 6},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType U3CRescaleU3Ec__IteratorA_t371_0_0_0;
extern const Il2CppType U3CRescaleU3Ec__IteratorA_t371_1_0_0;
extern TypeInfo SCR_LevelStartCountdown_t370_il2cpp_TypeInfo;
extern const Il2CppType SCR_LevelStartCountdown_t370_0_0_0;
struct U3CRescaleU3Ec__IteratorA_t371;
const Il2CppTypeDefinitionMetadata U3CRescaleU3Ec__IteratorA_t371_DefinitionMetadata = 
{
	&SCR_LevelStartCountdown_t370_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CRescaleU3Ec__IteratorA_t371_InterfacesTypeInfos/* implementedInterfaces */
	, U3CRescaleU3Ec__IteratorA_t371_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CRescaleU3Ec__IteratorA_t371_VTable/* vtableMethods */
	, U3CRescaleU3Ec__IteratorA_t371_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 442/* fieldStart */

};
TypeInfo U3CRescaleU3Ec__IteratorA_t371_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<Rescale>c__IteratorA"/* name */
	, ""/* namespaze */
	, U3CRescaleU3Ec__IteratorA_t371_MethodInfos/* methods */
	, U3CRescaleU3Ec__IteratorA_t371_PropertyInfos/* properties */
	, NULL/* events */
	, &U3CRescaleU3Ec__IteratorA_t371_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 69/* custom_attributes_cache */
	, &U3CRescaleU3Ec__IteratorA_t371_0_0_0/* byval_arg */
	, &U3CRescaleU3Ec__IteratorA_t371_1_0_0/* this_arg */
	, &U3CRescaleU3Ec__IteratorA_t371_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CRescaleU3Ec__IteratorA_t371)/* instance_size */
	, sizeof (U3CRescaleU3Ec__IteratorA_t371)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// SCR_LevelStartCountdown
#include "AssemblyU2DCSharp_SCR_LevelStartCountdown.h"
// Metadata Definition SCR_LevelStartCountdown
// SCR_LevelStartCountdown
#include "AssemblyU2DCSharp_SCR_LevelStartCountdownMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_LevelStartCountdown::.ctor()
extern const MethodInfo SCR_LevelStartCountdown__ctor_m1377_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SCR_LevelStartCountdown__ctor_m1377/* method */
	, &SCR_LevelStartCountdown_t370_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 306/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_LevelStartCountdown::Awake()
extern const MethodInfo SCR_LevelStartCountdown_Awake_m1378_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&SCR_LevelStartCountdown_Awake_m1378/* method */
	, &SCR_LevelStartCountdown_t370_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 307/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_LevelStartCountdown::OnEnable()
extern const MethodInfo SCR_LevelStartCountdown_OnEnable_m1379_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&SCR_LevelStartCountdown_OnEnable_m1379/* method */
	, &SCR_LevelStartCountdown_t370_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 308/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_LevelStartCountdown::OnDisable()
extern const MethodInfo SCR_LevelStartCountdown_OnDisable_m1380_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&SCR_LevelStartCountdown_OnDisable_m1380/* method */
	, &SCR_LevelStartCountdown_t370_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 309/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_LevelStartCountdown::Countdown()
extern const MethodInfo SCR_LevelStartCountdown_Countdown_m1381_MethodInfo = 
{
	"Countdown"/* name */
	, (methodPointerType)&SCR_LevelStartCountdown_Countdown_m1381/* method */
	, &SCR_LevelStartCountdown_t370_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 310/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
extern const Il2CppType Transform_t1_0_0_0;
static const ParameterInfo SCR_LevelStartCountdown_t370_SCR_LevelStartCountdown_Rescale_m1382_ParameterInfos[] = 
{
	{"up", 0, 134217796, 0, &Boolean_t273_0_0_0},
	{"objToScale", 1, 134217797, 0, &Transform_t1_0_0_0},
};
extern void* RuntimeInvoker_Object_t_SByte_t274_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.IEnumerator SCR_LevelStartCountdown::Rescale(System.Boolean,UnityEngine.Transform)
extern const MethodInfo SCR_LevelStartCountdown_Rescale_m1382_MethodInfo = 
{
	"Rescale"/* name */
	, (methodPointerType)&SCR_LevelStartCountdown_Rescale_m1382/* method */
	, &SCR_LevelStartCountdown_t370_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t217_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_SByte_t274_Object_t/* invoker_method */
	, SCR_LevelStartCountdown_t370_SCR_LevelStartCountdown_Rescale_m1382_ParameterInfos/* parameters */
	, 68/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 311/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_LevelStartCountdown::Reinitialize()
extern const MethodInfo SCR_LevelStartCountdown_Reinitialize_m1383_MethodInfo = 
{
	"Reinitialize"/* name */
	, (methodPointerType)&SCR_LevelStartCountdown_Reinitialize_m1383/* method */
	, &SCR_LevelStartCountdown_t370_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 312/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_LevelStartCountdown::HideAll()
extern const MethodInfo SCR_LevelStartCountdown_HideAll_m1384_MethodInfo = 
{
	"HideAll"/* name */
	, (methodPointerType)&SCR_LevelStartCountdown_HideAll_m1384/* method */
	, &SCR_LevelStartCountdown_t370_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 313/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_LevelStartCountdown::FillCountdownArray()
extern const MethodInfo SCR_LevelStartCountdown_FillCountdownArray_m1385_MethodInfo = 
{
	"FillCountdownArray"/* name */
	, (methodPointerType)&SCR_LevelStartCountdown_FillCountdownArray_m1385/* method */
	, &SCR_LevelStartCountdown_t370_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 314/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SCR_LevelStartCountdown_t370_MethodInfos[] =
{
	&SCR_LevelStartCountdown__ctor_m1377_MethodInfo,
	&SCR_LevelStartCountdown_Awake_m1378_MethodInfo,
	&SCR_LevelStartCountdown_OnEnable_m1379_MethodInfo,
	&SCR_LevelStartCountdown_OnDisable_m1380_MethodInfo,
	&SCR_LevelStartCountdown_Countdown_m1381_MethodInfo,
	&SCR_LevelStartCountdown_Rescale_m1382_MethodInfo,
	&SCR_LevelStartCountdown_Reinitialize_m1383_MethodInfo,
	&SCR_LevelStartCountdown_HideAll_m1384_MethodInfo,
	&SCR_LevelStartCountdown_FillCountdownArray_m1385_MethodInfo,
	NULL
};
static const Il2CppType* SCR_LevelStartCountdown_t370_il2cpp_TypeInfo__nestedTypes[1] =
{
	&U3CRescaleU3Ec__IteratorA_t371_0_0_0,
};
static const Il2CppMethodReference SCR_LevelStartCountdown_t370_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool SCR_LevelStartCountdown_t370_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType SCR_LevelStartCountdown_t370_1_0_0;
struct SCR_LevelStartCountdown_t370;
const Il2CppTypeDefinitionMetadata SCR_LevelStartCountdown_t370_DefinitionMetadata = 
{
	NULL/* declaringType */
	, SCR_LevelStartCountdown_t370_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &SCR_Menu_t336_0_0_0/* parent */
	, SCR_LevelStartCountdown_t370_VTable/* vtableMethods */
	, SCR_LevelStartCountdown_t370_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 450/* fieldStart */

};
TypeInfo SCR_LevelStartCountdown_t370_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "SCR_LevelStartCountdown"/* name */
	, ""/* namespaze */
	, SCR_LevelStartCountdown_t370_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SCR_LevelStartCountdown_t370_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SCR_LevelStartCountdown_t370_0_0_0/* byval_arg */
	, &SCR_LevelStartCountdown_t370_1_0_0/* this_arg */
	, &SCR_LevelStartCountdown_t370_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SCR_LevelStartCountdown_t370)/* instance_size */
	, sizeof (SCR_LevelStartCountdown_t370)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(SCR_LevelStartCountdown_t370_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SCR_MainMenu
#include "AssemblyU2DCSharp_SCR_MainMenu.h"
// Metadata Definition SCR_MainMenu
extern TypeInfo SCR_MainMenu_t372_il2cpp_TypeInfo;
// SCR_MainMenu
#include "AssemblyU2DCSharp_SCR_MainMenuMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_MainMenu::.ctor()
extern const MethodInfo SCR_MainMenu__ctor_m1386_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SCR_MainMenu__ctor_m1386/* method */
	, &SCR_MainMenu_t372_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 321/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_MainMenu::.cctor()
extern const MethodInfo SCR_MainMenu__cctor_m1387_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&SCR_MainMenu__cctor_m1387/* method */
	, &SCR_MainMenu_t372_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 322/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_MainMenu::Awake()
extern const MethodInfo SCR_MainMenu_Awake_m1388_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&SCR_MainMenu_Awake_m1388/* method */
	, &SCR_MainMenu_t372_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 323/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_MainMenu::OnEnable()
extern const MethodInfo SCR_MainMenu_OnEnable_m1389_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&SCR_MainMenu_OnEnable_m1389/* method */
	, &SCR_MainMenu_t372_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 324/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_MainMenu::Update()
extern const MethodInfo SCR_MainMenu_Update_m1390_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&SCR_MainMenu_Update_m1390/* method */
	, &SCR_MainMenu_t372_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 325/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_MainMenu::OpenMenu()
extern const MethodInfo SCR_MainMenu_OpenMenu_m1391_MethodInfo = 
{
	"OpenMenu"/* name */
	, (methodPointerType)&SCR_MainMenu_OpenMenu_m1391/* method */
	, &SCR_MainMenu_t372_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 326/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_MainMenu::LevelSetsInit()
extern const MethodInfo SCR_MainMenu_LevelSetsInit_m1392_MethodInfo = 
{
	"LevelSetsInit"/* name */
	, (methodPointerType)&SCR_MainMenu_LevelSetsInit_m1392/* method */
	, &SCR_MainMenu_t372_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 327/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_MainMenu::ShowTutorials()
extern const MethodInfo SCR_MainMenu_ShowTutorials_m1393_MethodInfo = 
{
	"ShowTutorials"/* name */
	, (methodPointerType)&SCR_MainMenu_ShowTutorials_m1393/* method */
	, &SCR_MainMenu_t372_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 328/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo SCR_MainMenu_t372_SCR_MainMenu_ShowLevelInfo_m1394_ParameterInfos[] = 
{
	{"level", 0, 134217798, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_MainMenu::ShowLevelInfo(System.Int32)
extern const MethodInfo SCR_MainMenu_ShowLevelInfo_m1394_MethodInfo = 
{
	"ShowLevelInfo"/* name */
	, (methodPointerType)&SCR_MainMenu_ShowLevelInfo_m1394/* method */
	, &SCR_MainMenu_t372_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, SCR_MainMenu_t372_SCR_MainMenu_ShowLevelInfo_m1394_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 329/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_MainMenu::StartLevel()
extern const MethodInfo SCR_MainMenu_StartLevel_m1395_MethodInfo = 
{
	"StartLevel"/* name */
	, (methodPointerType)&SCR_MainMenu_StartLevel_m1395/* method */
	, &SCR_MainMenu_t372_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 330/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo SCR_MainMenu_t372_SCR_MainMenu_ScrollLevelSets_m1396_ParameterInfos[] = 
{
	{"goRight", 0, 134217799, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_MainMenu::ScrollLevelSets(System.Boolean)
extern const MethodInfo SCR_MainMenu_ScrollLevelSets_m1396_MethodInfo = 
{
	"ScrollLevelSets"/* name */
	, (methodPointerType)&SCR_MainMenu_ScrollLevelSets_m1396/* method */
	, &SCR_MainMenu_t372_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274/* invoker_method */
	, SCR_MainMenu_t372_SCR_MainMenu_ScrollLevelSets_m1396_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 331/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean SCR_MainMenu::get_IsFromSplashScreen()
extern const MethodInfo SCR_MainMenu_get_IsFromSplashScreen_m1397_MethodInfo = 
{
	"get_IsFromSplashScreen"/* name */
	, (methodPointerType)&SCR_MainMenu_get_IsFromSplashScreen_m1397/* method */
	, &SCR_MainMenu_t372_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 332/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo SCR_MainMenu_t372_SCR_MainMenu_set_IsFromSplashScreen_m1398_ParameterInfos[] = 
{
	{"value", 0, 134217800, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_MainMenu::set_IsFromSplashScreen(System.Boolean)
extern const MethodInfo SCR_MainMenu_set_IsFromSplashScreen_m1398_MethodInfo = 
{
	"set_IsFromSplashScreen"/* name */
	, (methodPointerType)&SCR_MainMenu_set_IsFromSplashScreen_m1398/* method */
	, &SCR_MainMenu_t372_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274/* invoker_method */
	, SCR_MainMenu_t372_SCR_MainMenu_set_IsFromSplashScreen_m1398_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 333/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SCR_MainMenu_t372_MethodInfos[] =
{
	&SCR_MainMenu__ctor_m1386_MethodInfo,
	&SCR_MainMenu__cctor_m1387_MethodInfo,
	&SCR_MainMenu_Awake_m1388_MethodInfo,
	&SCR_MainMenu_OnEnable_m1389_MethodInfo,
	&SCR_MainMenu_Update_m1390_MethodInfo,
	&SCR_MainMenu_OpenMenu_m1391_MethodInfo,
	&SCR_MainMenu_LevelSetsInit_m1392_MethodInfo,
	&SCR_MainMenu_ShowTutorials_m1393_MethodInfo,
	&SCR_MainMenu_ShowLevelInfo_m1394_MethodInfo,
	&SCR_MainMenu_StartLevel_m1395_MethodInfo,
	&SCR_MainMenu_ScrollLevelSets_m1396_MethodInfo,
	&SCR_MainMenu_get_IsFromSplashScreen_m1397_MethodInfo,
	&SCR_MainMenu_set_IsFromSplashScreen_m1398_MethodInfo,
	NULL
};
extern const MethodInfo SCR_MainMenu_get_IsFromSplashScreen_m1397_MethodInfo;
extern const MethodInfo SCR_MainMenu_set_IsFromSplashScreen_m1398_MethodInfo;
static const PropertyInfo SCR_MainMenu_t372____IsFromSplashScreen_PropertyInfo = 
{
	&SCR_MainMenu_t372_il2cpp_TypeInfo/* parent */
	, "IsFromSplashScreen"/* name */
	, &SCR_MainMenu_get_IsFromSplashScreen_m1397_MethodInfo/* get */
	, &SCR_MainMenu_set_IsFromSplashScreen_m1398_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* SCR_MainMenu_t372_PropertyInfos[] =
{
	&SCR_MainMenu_t372____IsFromSplashScreen_PropertyInfo,
	NULL
};
static const Il2CppMethodReference SCR_MainMenu_t372_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool SCR_MainMenu_t372_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType SCR_MainMenu_t372_0_0_0;
extern const Il2CppType SCR_MainMenu_t372_1_0_0;
struct SCR_MainMenu_t372;
const Il2CppTypeDefinitionMetadata SCR_MainMenu_t372_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &SCR_Menu_t336_0_0_0/* parent */
	, SCR_MainMenu_t372_VTable/* vtableMethods */
	, SCR_MainMenu_t372_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 454/* fieldStart */

};
TypeInfo SCR_MainMenu_t372_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "SCR_MainMenu"/* name */
	, ""/* namespaze */
	, SCR_MainMenu_t372_MethodInfos/* methods */
	, SCR_MainMenu_t372_PropertyInfos/* properties */
	, NULL/* events */
	, &SCR_MainMenu_t372_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SCR_MainMenu_t372_0_0_0/* byval_arg */
	, &SCR_MainMenu_t372_1_0_0/* this_arg */
	, &SCR_MainMenu_t372_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SCR_MainMenu_t372)/* instance_size */
	, sizeof (SCR_MainMenu_t372)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(SCR_MainMenu_t372_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 13/* method_count */
	, 1/* property_count */
	, 13/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SCR_PauseMenu
#include "AssemblyU2DCSharp_SCR_PauseMenu.h"
// Metadata Definition SCR_PauseMenu
extern TypeInfo SCR_PauseMenu_t374_il2cpp_TypeInfo;
// SCR_PauseMenu
#include "AssemblyU2DCSharp_SCR_PauseMenuMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_PauseMenu::.ctor()
extern const MethodInfo SCR_PauseMenu__ctor_m1399_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SCR_PauseMenu__ctor_m1399/* method */
	, &SCR_PauseMenu_t374_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 334/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_PauseMenu::Awake()
extern const MethodInfo SCR_PauseMenu_Awake_m1400_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&SCR_PauseMenu_Awake_m1400/* method */
	, &SCR_PauseMenu_t374_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 335/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_PauseMenu::OnEnable()
extern const MethodInfo SCR_PauseMenu_OnEnable_m1401_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&SCR_PauseMenu_OnEnable_m1401/* method */
	, &SCR_PauseMenu_t374_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 336/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo SCR_PauseMenu_t374_SCR_PauseMenu_PauseGame_m1402_ParameterInfos[] = 
{
	{"pause", 0, 134217801, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_PauseMenu::PauseGame(System.Boolean)
extern const MethodInfo SCR_PauseMenu_PauseGame_m1402_MethodInfo = 
{
	"PauseGame"/* name */
	, (methodPointerType)&SCR_PauseMenu_PauseGame_m1402/* method */
	, &SCR_PauseMenu_t374_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274/* invoker_method */
	, SCR_PauseMenu_t374_SCR_PauseMenu_PauseGame_m1402_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 337/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo SCR_PauseMenu_t374_SCR_PauseMenu_ShowLadybugCollectibles_m1403_ParameterInfos[] = 
{
	{"collected", 0, 134217802, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_PauseMenu::ShowLadybugCollectibles(System.Int32)
extern const MethodInfo SCR_PauseMenu_ShowLadybugCollectibles_m1403_MethodInfo = 
{
	"ShowLadybugCollectibles"/* name */
	, (methodPointerType)&SCR_PauseMenu_ShowLadybugCollectibles_m1403/* method */
	, &SCR_PauseMenu_t374_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, SCR_PauseMenu_t374_SCR_PauseMenu_ShowLadybugCollectibles_m1403_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 338/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SCR_PauseMenu_t374_MethodInfos[] =
{
	&SCR_PauseMenu__ctor_m1399_MethodInfo,
	&SCR_PauseMenu_Awake_m1400_MethodInfo,
	&SCR_PauseMenu_OnEnable_m1401_MethodInfo,
	&SCR_PauseMenu_PauseGame_m1402_MethodInfo,
	&SCR_PauseMenu_ShowLadybugCollectibles_m1403_MethodInfo,
	NULL
};
static const Il2CppMethodReference SCR_PauseMenu_t374_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool SCR_PauseMenu_t374_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType SCR_PauseMenu_t374_0_0_0;
extern const Il2CppType SCR_PauseMenu_t374_1_0_0;
struct SCR_PauseMenu_t374;
const Il2CppTypeDefinitionMetadata SCR_PauseMenu_t374_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &SCR_Menu_t336_0_0_0/* parent */
	, SCR_PauseMenu_t374_VTable/* vtableMethods */
	, SCR_PauseMenu_t374_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 467/* fieldStart */

};
TypeInfo SCR_PauseMenu_t374_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "SCR_PauseMenu"/* name */
	, ""/* namespaze */
	, SCR_PauseMenu_t374_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SCR_PauseMenu_t374_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SCR_PauseMenu_t374_0_0_0/* byval_arg */
	, &SCR_PauseMenu_t374_1_0_0/* this_arg */
	, &SCR_PauseMenu_t374_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SCR_PauseMenu_t374)/* instance_size */
	, sizeof (SCR_PauseMenu_t374)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SCR_GUIManager/TextType
#include "AssemblyU2DCSharp_SCR_GUIManager_TextType.h"
// Metadata Definition SCR_GUIManager/TextType
extern TypeInfo TextType_t375_il2cpp_TypeInfo;
// SCR_GUIManager/TextType
#include "AssemblyU2DCSharp_SCR_GUIManager_TextTypeMethodDeclarations.h"
static const MethodInfo* TextType_t375_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference TextType_t375_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool TextType_t375_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TextType_t375_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType TextType_t375_0_0_0;
extern const Il2CppType TextType_t375_1_0_0;
extern TypeInfo SCR_GUIManager_t378_il2cpp_TypeInfo;
extern const Il2CppType SCR_GUIManager_t378_0_0_0;
const Il2CppTypeDefinitionMetadata TextType_t375_DefinitionMetadata = 
{
	&SCR_GUIManager_t378_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TextType_t375_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, TextType_t375_VTable/* vtableMethods */
	, TextType_t375_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 469/* fieldStart */

};
TypeInfo TextType_t375_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "TextType"/* name */
	, ""/* namespaze */
	, TextType_t375_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TextType_t375_0_0_0/* byval_arg */
	, &TextType_t375_1_0_0/* this_arg */
	, &TextType_t375_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TextType_t375)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TextType_t375)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// SCR_GUIManager
#include "AssemblyU2DCSharp_SCR_GUIManager.h"
// Metadata Definition SCR_GUIManager
// SCR_GUIManager
#include "AssemblyU2DCSharp_SCR_GUIManagerMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_GUIManager::.ctor()
extern const MethodInfo SCR_GUIManager__ctor_m1404_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SCR_GUIManager__ctor_m1404/* method */
	, &SCR_GUIManager_t378_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 339/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_GUIManager::Awake()
extern const MethodInfo SCR_GUIManager_Awake_m1405_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&SCR_GUIManager_Awake_m1405/* method */
	, &SCR_GUIManager_t378_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 340/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_GUIManager::OnLevelWasLoaded()
extern const MethodInfo SCR_GUIManager_OnLevelWasLoaded_m1406_MethodInfo = 
{
	"OnLevelWasLoaded"/* name */
	, (methodPointerType)&SCR_GUIManager_OnLevelWasLoaded_m1406/* method */
	, &SCR_GUIManager_t378_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 341/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_GUIManager::LevelInit()
extern const MethodInfo SCR_GUIManager_LevelInit_m1407_MethodInfo = 
{
	"LevelInit"/* name */
	, (methodPointerType)&SCR_GUIManager_LevelInit_m1407/* method */
	, &SCR_GUIManager_t378_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 342/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_GUIManager::CloseAllMenus()
extern const MethodInfo SCR_GUIManager_CloseAllMenus_m1408_MethodInfo = 
{
	"CloseAllMenus"/* name */
	, (methodPointerType)&SCR_GUIManager_CloseAllMenus_m1408/* method */
	, &SCR_GUIManager_t378_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 343/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo SCR_GUIManager_t378_SCR_GUIManager_OpenMenu_m1409_ParameterInfos[] = 
{
	{"menuName", 0, 134217803, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_GUIManager::OpenMenu(System.String)
extern const MethodInfo SCR_GUIManager_OpenMenu_m1409_MethodInfo = 
{
	"OpenMenu"/* name */
	, (methodPointerType)&SCR_GUIManager_OpenMenu_m1409/* method */
	, &SCR_GUIManager_t378_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, SCR_GUIManager_t378_SCR_GUIManager_OpenMenu_m1409_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 344/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_GUIManager::OpenLevelMenus()
extern const MethodInfo SCR_GUIManager_OpenLevelMenus_m1410_MethodInfo = 
{
	"OpenLevelMenus"/* name */
	, (methodPointerType)&SCR_GUIManager_OpenLevelMenus_m1410/* method */
	, &SCR_GUIManager_t378_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 345/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TextType_t375_0_0_0;
static const ParameterInfo SCR_GUIManager_t378_SCR_GUIManager_RelayTextInfo_m1411_ParameterInfos[] = 
{
	{"collectibleType", 0, 134217804, 0, &TextType_t375_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_GUIManager::RelayTextInfo(SCR_GUIManager/TextType)
extern const MethodInfo SCR_GUIManager_RelayTextInfo_m1411_MethodInfo = 
{
	"RelayTextInfo"/* name */
	, (methodPointerType)&SCR_GUIManager_RelayTextInfo_m1411/* method */
	, &SCR_GUIManager_t378_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, SCR_GUIManager_t378_SCR_GUIManager_RelayTextInfo_m1411_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 346/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo SCR_GUIManager_t378_SCR_GUIManager_ToggleLanguage_m1412_ParameterInfos[] = 
{
	{"french", 0, 134217805, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_GUIManager::ToggleLanguage(System.Boolean)
extern const MethodInfo SCR_GUIManager_ToggleLanguage_m1412_MethodInfo = 
{
	"ToggleLanguage"/* name */
	, (methodPointerType)&SCR_GUIManager_ToggleLanguage_m1412/* method */
	, &SCR_GUIManager_t378_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274/* invoker_method */
	, SCR_GUIManager_t378_SCR_GUIManager_ToggleLanguage_m1412_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 347/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean SCR_GUIManager::get_IsFrench()
extern const MethodInfo SCR_GUIManager_get_IsFrench_m1413_MethodInfo = 
{
	"get_IsFrench"/* name */
	, (methodPointerType)&SCR_GUIManager_get_IsFrench_m1413/* method */
	, &SCR_GUIManager_t378_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 348/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// SCR_LevelEndMenu SCR_GUIManager::get_LevelEndMenu()
extern const MethodInfo SCR_GUIManager_get_LevelEndMenu_m1414_MethodInfo = 
{
	"get_LevelEndMenu"/* name */
	, (methodPointerType)&SCR_GUIManager_get_LevelEndMenu_m1414/* method */
	, &SCR_GUIManager_t378_il2cpp_TypeInfo/* declaring_type */
	, &SCR_LevelEndMenu_t367_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 349/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// SCR_MainMenu SCR_GUIManager::get_MainMenu()
extern const MethodInfo SCR_GUIManager_get_MainMenu_m1415_MethodInfo = 
{
	"get_MainMenu"/* name */
	, (methodPointerType)&SCR_GUIManager_get_MainMenu_m1415/* method */
	, &SCR_GUIManager_t378_il2cpp_TypeInfo/* declaring_type */
	, &SCR_MainMenu_t372_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 350/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SCR_GUIManager_t378_MethodInfos[] =
{
	&SCR_GUIManager__ctor_m1404_MethodInfo,
	&SCR_GUIManager_Awake_m1405_MethodInfo,
	&SCR_GUIManager_OnLevelWasLoaded_m1406_MethodInfo,
	&SCR_GUIManager_LevelInit_m1407_MethodInfo,
	&SCR_GUIManager_CloseAllMenus_m1408_MethodInfo,
	&SCR_GUIManager_OpenMenu_m1409_MethodInfo,
	&SCR_GUIManager_OpenLevelMenus_m1410_MethodInfo,
	&SCR_GUIManager_RelayTextInfo_m1411_MethodInfo,
	&SCR_GUIManager_ToggleLanguage_m1412_MethodInfo,
	&SCR_GUIManager_get_IsFrench_m1413_MethodInfo,
	&SCR_GUIManager_get_LevelEndMenu_m1414_MethodInfo,
	&SCR_GUIManager_get_MainMenu_m1415_MethodInfo,
	NULL
};
extern const MethodInfo SCR_GUIManager_get_IsFrench_m1413_MethodInfo;
static const PropertyInfo SCR_GUIManager_t378____IsFrench_PropertyInfo = 
{
	&SCR_GUIManager_t378_il2cpp_TypeInfo/* parent */
	, "IsFrench"/* name */
	, &SCR_GUIManager_get_IsFrench_m1413_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo SCR_GUIManager_get_LevelEndMenu_m1414_MethodInfo;
static const PropertyInfo SCR_GUIManager_t378____LevelEndMenu_PropertyInfo = 
{
	&SCR_GUIManager_t378_il2cpp_TypeInfo/* parent */
	, "LevelEndMenu"/* name */
	, &SCR_GUIManager_get_LevelEndMenu_m1414_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo SCR_GUIManager_get_MainMenu_m1415_MethodInfo;
static const PropertyInfo SCR_GUIManager_t378____MainMenu_PropertyInfo = 
{
	&SCR_GUIManager_t378_il2cpp_TypeInfo/* parent */
	, "MainMenu"/* name */
	, &SCR_GUIManager_get_MainMenu_m1415_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* SCR_GUIManager_t378_PropertyInfos[] =
{
	&SCR_GUIManager_t378____IsFrench_PropertyInfo,
	&SCR_GUIManager_t378____LevelEndMenu_PropertyInfo,
	&SCR_GUIManager_t378____MainMenu_PropertyInfo,
	NULL
};
static const Il2CppType* SCR_GUIManager_t378_il2cpp_TypeInfo__nestedTypes[1] =
{
	&TextType_t375_0_0_0,
};
static const Il2CppMethodReference SCR_GUIManager_t378_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool SCR_GUIManager_t378_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType SCR_GUIManager_t378_1_0_0;
struct SCR_GUIManager_t378;
const Il2CppTypeDefinitionMetadata SCR_GUIManager_t378_DefinitionMetadata = 
{
	NULL/* declaringType */
	, SCR_GUIManager_t378_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, SCR_GUIManager_t378_VTable/* vtableMethods */
	, SCR_GUIManager_t378_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 474/* fieldStart */

};
TypeInfo SCR_GUIManager_t378_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "SCR_GUIManager"/* name */
	, ""/* namespaze */
	, SCR_GUIManager_t378_MethodInfos/* methods */
	, SCR_GUIManager_t378_PropertyInfos/* properties */
	, NULL/* events */
	, &SCR_GUIManager_t378_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SCR_GUIManager_t378_0_0_0/* byval_arg */
	, &SCR_GUIManager_t378_1_0_0/* this_arg */
	, &SCR_GUIManager_t378_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SCR_GUIManager_t378)/* instance_size */
	, sizeof (SCR_GUIManager_t378)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 12/* method_count */
	, 3/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SCR_LevelManager
#include "AssemblyU2DCSharp_SCR_LevelManager.h"
// Metadata Definition SCR_LevelManager
extern TypeInfo SCR_LevelManager_t379_il2cpp_TypeInfo;
// SCR_LevelManager
#include "AssemblyU2DCSharp_SCR_LevelManagerMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_LevelManager::.ctor()
extern const MethodInfo SCR_LevelManager__ctor_m1416_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SCR_LevelManager__ctor_m1416/* method */
	, &SCR_LevelManager_t379_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 351/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_LevelManager::Awake()
extern const MethodInfo SCR_LevelManager_Awake_m1417_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&SCR_LevelManager_Awake_m1417/* method */
	, &SCR_LevelManager_t379_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 352/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_LevelManager::OnLevelWasLoaded()
extern const MethodInfo SCR_LevelManager_OnLevelWasLoaded_m1418_MethodInfo = 
{
	"OnLevelWasLoaded"/* name */
	, (methodPointerType)&SCR_LevelManager_OnLevelWasLoaded_m1418/* method */
	, &SCR_LevelManager_t379_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 353/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_LevelManager::EndLevel()
extern const MethodInfo SCR_LevelManager_EndLevel_m1419_MethodInfo = 
{
	"EndLevel"/* name */
	, (methodPointerType)&SCR_LevelManager_EndLevel_m1419/* method */
	, &SCR_LevelManager_t379_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 354/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo SCR_LevelManager_t379_SCR_LevelManager_LoadLevel_m1420_ParameterInfos[] = 
{
	{"level", 0, 134217806, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_LevelManager::LoadLevel(System.Int32)
extern const MethodInfo SCR_LevelManager_LoadLevel_m1420_MethodInfo = 
{
	"LoadLevel"/* name */
	, (methodPointerType)&SCR_LevelManager_LoadLevel_m1420/* method */
	, &SCR_LevelManager_t379_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, SCR_LevelManager_t379_SCR_LevelManager_LoadLevel_m1420_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 355/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 SCR_LevelManager::get_CurrentLevel()
extern const MethodInfo SCR_LevelManager_get_CurrentLevel_m1421_MethodInfo = 
{
	"get_CurrentLevel"/* name */
	, (methodPointerType)&SCR_LevelManager_get_CurrentLevel_m1421/* method */
	, &SCR_LevelManager_t379_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 356/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 SCR_LevelManager::get_GameplayLevel()
extern const MethodInfo SCR_LevelManager_get_GameplayLevel_m1422_MethodInfo = 
{
	"get_GameplayLevel"/* name */
	, (methodPointerType)&SCR_LevelManager_get_GameplayLevel_m1422/* method */
	, &SCR_LevelManager_t379_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 357/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean SCR_LevelManager::get_IsMenu()
extern const MethodInfo SCR_LevelManager_get_IsMenu_m1423_MethodInfo = 
{
	"get_IsMenu"/* name */
	, (methodPointerType)&SCR_LevelManager_get_IsMenu_m1423/* method */
	, &SCR_LevelManager_t379_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 358/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SCR_LevelManager_t379_MethodInfos[] =
{
	&SCR_LevelManager__ctor_m1416_MethodInfo,
	&SCR_LevelManager_Awake_m1417_MethodInfo,
	&SCR_LevelManager_OnLevelWasLoaded_m1418_MethodInfo,
	&SCR_LevelManager_EndLevel_m1419_MethodInfo,
	&SCR_LevelManager_LoadLevel_m1420_MethodInfo,
	&SCR_LevelManager_get_CurrentLevel_m1421_MethodInfo,
	&SCR_LevelManager_get_GameplayLevel_m1422_MethodInfo,
	&SCR_LevelManager_get_IsMenu_m1423_MethodInfo,
	NULL
};
extern const MethodInfo SCR_LevelManager_get_CurrentLevel_m1421_MethodInfo;
static const PropertyInfo SCR_LevelManager_t379____CurrentLevel_PropertyInfo = 
{
	&SCR_LevelManager_t379_il2cpp_TypeInfo/* parent */
	, "CurrentLevel"/* name */
	, &SCR_LevelManager_get_CurrentLevel_m1421_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo SCR_LevelManager_get_GameplayLevel_m1422_MethodInfo;
static const PropertyInfo SCR_LevelManager_t379____GameplayLevel_PropertyInfo = 
{
	&SCR_LevelManager_t379_il2cpp_TypeInfo/* parent */
	, "GameplayLevel"/* name */
	, &SCR_LevelManager_get_GameplayLevel_m1422_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo SCR_LevelManager_get_IsMenu_m1423_MethodInfo;
static const PropertyInfo SCR_LevelManager_t379____IsMenu_PropertyInfo = 
{
	&SCR_LevelManager_t379_il2cpp_TypeInfo/* parent */
	, "IsMenu"/* name */
	, &SCR_LevelManager_get_IsMenu_m1423_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* SCR_LevelManager_t379_PropertyInfos[] =
{
	&SCR_LevelManager_t379____CurrentLevel_PropertyInfo,
	&SCR_LevelManager_t379____GameplayLevel_PropertyInfo,
	&SCR_LevelManager_t379____IsMenu_PropertyInfo,
	NULL
};
static const Il2CppMethodReference SCR_LevelManager_t379_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool SCR_LevelManager_t379_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType SCR_LevelManager_t379_0_0_0;
extern const Il2CppType SCR_LevelManager_t379_1_0_0;
struct SCR_LevelManager_t379;
const Il2CppTypeDefinitionMetadata SCR_LevelManager_t379_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, SCR_LevelManager_t379_VTable/* vtableMethods */
	, SCR_LevelManager_t379_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 480/* fieldStart */

};
TypeInfo SCR_LevelManager_t379_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "SCR_LevelManager"/* name */
	, ""/* namespaze */
	, SCR_LevelManager_t379_MethodInfos/* methods */
	, SCR_LevelManager_t379_PropertyInfos/* properties */
	, NULL/* events */
	, &SCR_LevelManager_t379_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SCR_LevelManager_t379_0_0_0/* byval_arg */
	, &SCR_LevelManager_t379_1_0_0/* this_arg */
	, &SCR_LevelManager_t379_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SCR_LevelManager_t379)/* instance_size */
	, sizeof (SCR_LevelManager_t379)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 3/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SCR_ManagerComponents
#include "AssemblyU2DCSharp_SCR_ManagerComponents.h"
// Metadata Definition SCR_ManagerComponents
extern TypeInfo SCR_ManagerComponents_t380_il2cpp_TypeInfo;
// SCR_ManagerComponents
#include "AssemblyU2DCSharp_SCR_ManagerComponentsMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_ManagerComponents::.ctor()
extern const MethodInfo SCR_ManagerComponents__ctor_m1424_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SCR_ManagerComponents__ctor_m1424/* method */
	, &SCR_ManagerComponents_t380_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 359/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_ManagerComponents::Awake()
extern const MethodInfo SCR_ManagerComponents_Awake_m1425_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&SCR_ManagerComponents_Awake_m1425/* method */
	, &SCR_ManagerComponents_t380_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 360/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_ManagerComponents::OnLevelWasLoaded()
extern const MethodInfo SCR_ManagerComponents_OnLevelWasLoaded_m1426_MethodInfo = 
{
	"OnLevelWasLoaded"/* name */
	, (methodPointerType)&SCR_ManagerComponents_OnLevelWasLoaded_m1426/* method */
	, &SCR_ManagerComponents_t380_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 361/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SCR_ManagerComponents_t380_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// SCR_ManagerComponents SCR_ManagerComponents::get_ManagerInstance()
extern const MethodInfo SCR_ManagerComponents_get_ManagerInstance_m1427_MethodInfo = 
{
	"get_ManagerInstance"/* name */
	, (methodPointerType)&SCR_ManagerComponents_get_ManagerInstance_m1427/* method */
	, &SCR_ManagerComponents_t380_il2cpp_TypeInfo/* declaring_type */
	, &SCR_ManagerComponents_t380_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 362/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SCR_ManagerComponents_t380_0_0_0;
static const ParameterInfo SCR_ManagerComponents_t380_SCR_ManagerComponents_set_ManagerInstance_m1428_ParameterInfos[] = 
{
	{"value", 0, 134217807, 0, &SCR_ManagerComponents_t380_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_ManagerComponents::set_ManagerInstance(SCR_ManagerComponents)
extern const MethodInfo SCR_ManagerComponents_set_ManagerInstance_m1428_MethodInfo = 
{
	"set_ManagerInstance"/* name */
	, (methodPointerType)&SCR_ManagerComponents_set_ManagerInstance_m1428/* method */
	, &SCR_ManagerComponents_t380_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, SCR_ManagerComponents_t380_SCR_ManagerComponents_set_ManagerInstance_m1428_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 363/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SCR_PointManager_t381_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// SCR_PointManager SCR_ManagerComponents::get_PointManager()
extern const MethodInfo SCR_ManagerComponents_get_PointManager_m1429_MethodInfo = 
{
	"get_PointManager"/* name */
	, (methodPointerType)&SCR_ManagerComponents_get_PointManager_m1429/* method */
	, &SCR_ManagerComponents_t380_il2cpp_TypeInfo/* declaring_type */
	, &SCR_PointManager_t381_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 364/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SCR_PointManager_t381_0_0_0;
static const ParameterInfo SCR_ManagerComponents_t380_SCR_ManagerComponents_set_PointManager_m1430_ParameterInfos[] = 
{
	{"value", 0, 134217808, 0, &SCR_PointManager_t381_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_ManagerComponents::set_PointManager(SCR_PointManager)
extern const MethodInfo SCR_ManagerComponents_set_PointManager_m1430_MethodInfo = 
{
	"set_PointManager"/* name */
	, (methodPointerType)&SCR_ManagerComponents_set_PointManager_m1430/* method */
	, &SCR_ManagerComponents_t380_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, SCR_ManagerComponents_t380_SCR_ManagerComponents_set_PointManager_m1430_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 365/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// SCR_LevelManager SCR_ManagerComponents::get_LevelManager()
extern const MethodInfo SCR_ManagerComponents_get_LevelManager_m1431_MethodInfo = 
{
	"get_LevelManager"/* name */
	, (methodPointerType)&SCR_ManagerComponents_get_LevelManager_m1431/* method */
	, &SCR_ManagerComponents_t380_il2cpp_TypeInfo/* declaring_type */
	, &SCR_LevelManager_t379_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 366/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SCR_LevelManager_t379_0_0_0;
static const ParameterInfo SCR_ManagerComponents_t380_SCR_ManagerComponents_set_LevelManager_m1432_ParameterInfos[] = 
{
	{"value", 0, 134217809, 0, &SCR_LevelManager_t379_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_ManagerComponents::set_LevelManager(SCR_LevelManager)
extern const MethodInfo SCR_ManagerComponents_set_LevelManager_m1432_MethodInfo = 
{
	"set_LevelManager"/* name */
	, (methodPointerType)&SCR_ManagerComponents_set_LevelManager_m1432/* method */
	, &SCR_ManagerComponents_t380_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, SCR_ManagerComponents_t380_SCR_ManagerComponents_set_LevelManager_m1432_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 367/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SCR_SaveData_t382_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// SCR_SaveData SCR_ManagerComponents::get_SaveData()
extern const MethodInfo SCR_ManagerComponents_get_SaveData_m1433_MethodInfo = 
{
	"get_SaveData"/* name */
	, (methodPointerType)&SCR_ManagerComponents_get_SaveData_m1433/* method */
	, &SCR_ManagerComponents_t380_il2cpp_TypeInfo/* declaring_type */
	, &SCR_SaveData_t382_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 368/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SCR_SaveData_t382_0_0_0;
static const ParameterInfo SCR_ManagerComponents_t380_SCR_ManagerComponents_set_SaveData_m1434_ParameterInfos[] = 
{
	{"value", 0, 134217810, 0, &SCR_SaveData_t382_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_ManagerComponents::set_SaveData(SCR_SaveData)
extern const MethodInfo SCR_ManagerComponents_set_SaveData_m1434_MethodInfo = 
{
	"set_SaveData"/* name */
	, (methodPointerType)&SCR_ManagerComponents_set_SaveData_m1434/* method */
	, &SCR_ManagerComponents_t380_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, SCR_ManagerComponents_t380_SCR_ManagerComponents_set_SaveData_m1434_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 369/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SCR_TimeManager_t383_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// SCR_TimeManager SCR_ManagerComponents::get_TimeManager()
extern const MethodInfo SCR_ManagerComponents_get_TimeManager_m1435_MethodInfo = 
{
	"get_TimeManager"/* name */
	, (methodPointerType)&SCR_ManagerComponents_get_TimeManager_m1435/* method */
	, &SCR_ManagerComponents_t380_il2cpp_TypeInfo/* declaring_type */
	, &SCR_TimeManager_t383_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 370/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SCR_TimeManager_t383_0_0_0;
static const ParameterInfo SCR_ManagerComponents_t380_SCR_ManagerComponents_set_TimeManager_m1436_ParameterInfos[] = 
{
	{"value", 0, 134217811, 0, &SCR_TimeManager_t383_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_ManagerComponents::set_TimeManager(SCR_TimeManager)
extern const MethodInfo SCR_ManagerComponents_set_TimeManager_m1436_MethodInfo = 
{
	"set_TimeManager"/* name */
	, (methodPointerType)&SCR_ManagerComponents_set_TimeManager_m1436/* method */
	, &SCR_ManagerComponents_t380_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, SCR_ManagerComponents_t380_SCR_ManagerComponents_set_TimeManager_m1436_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 371/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// SCR_GUIManager SCR_ManagerComponents::get_GUIManager()
extern const MethodInfo SCR_ManagerComponents_get_GUIManager_m1437_MethodInfo = 
{
	"get_GUIManager"/* name */
	, (methodPointerType)&SCR_ManagerComponents_get_GUIManager_m1437/* method */
	, &SCR_ManagerComponents_t380_il2cpp_TypeInfo/* declaring_type */
	, &SCR_GUIManager_t378_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 372/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SCR_GUIManager_t378_0_0_0;
static const ParameterInfo SCR_ManagerComponents_t380_SCR_ManagerComponents_set_GUIManager_m1438_ParameterInfos[] = 
{
	{"value", 0, 134217812, 0, &SCR_GUIManager_t378_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_ManagerComponents::set_GUIManager(SCR_GUIManager)
extern const MethodInfo SCR_ManagerComponents_set_GUIManager_m1438_MethodInfo = 
{
	"set_GUIManager"/* name */
	, (methodPointerType)&SCR_ManagerComponents_set_GUIManager_m1438/* method */
	, &SCR_ManagerComponents_t380_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, SCR_ManagerComponents_t380_SCR_ManagerComponents_set_GUIManager_m1438_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 373/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SCR_SoundManager_t335_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// SCR_SoundManager SCR_ManagerComponents::get_SoundManager()
extern const MethodInfo SCR_ManagerComponents_get_SoundManager_m1439_MethodInfo = 
{
	"get_SoundManager"/* name */
	, (methodPointerType)&SCR_ManagerComponents_get_SoundManager_m1439/* method */
	, &SCR_ManagerComponents_t380_il2cpp_TypeInfo/* declaring_type */
	, &SCR_SoundManager_t335_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 374/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SCR_SoundManager_t335_0_0_0;
static const ParameterInfo SCR_ManagerComponents_t380_SCR_ManagerComponents_set_SoundManager_m1440_ParameterInfos[] = 
{
	{"value", 0, 134217813, 0, &SCR_SoundManager_t335_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_ManagerComponents::set_SoundManager(SCR_SoundManager)
extern const MethodInfo SCR_ManagerComponents_set_SoundManager_m1440_MethodInfo = 
{
	"set_SoundManager"/* name */
	, (methodPointerType)&SCR_ManagerComponents_set_SoundManager_m1440/* method */
	, &SCR_ManagerComponents_t380_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, SCR_ManagerComponents_t380_SCR_ManagerComponents_set_SoundManager_m1440_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 375/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// SCR_Camie SCR_ManagerComponents::get_AvatarMove()
extern const MethodInfo SCR_ManagerComponents_get_AvatarMove_m1441_MethodInfo = 
{
	"get_AvatarMove"/* name */
	, (methodPointerType)&SCR_ManagerComponents_get_AvatarMove_m1441/* method */
	, &SCR_ManagerComponents_t380_il2cpp_TypeInfo/* declaring_type */
	, &SCR_Camie_t338_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 376/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SCR_Camie_t338_0_0_0;
static const ParameterInfo SCR_ManagerComponents_t380_SCR_ManagerComponents_set_AvatarMove_m1442_ParameterInfos[] = 
{
	{"value", 0, 134217814, 0, &SCR_Camie_t338_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_ManagerComponents::set_AvatarMove(SCR_Camie)
extern const MethodInfo SCR_ManagerComponents_set_AvatarMove_m1442_MethodInfo = 
{
	"set_AvatarMove"/* name */
	, (methodPointerType)&SCR_ManagerComponents_set_AvatarMove_m1442/* method */
	, &SCR_ManagerComponents_t380_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, SCR_ManagerComponents_t380_SCR_ManagerComponents_set_AvatarMove_m1442_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 377/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SCR_Spider_t384_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// SCR_Spider SCR_ManagerComponents::get_Spider()
extern const MethodInfo SCR_ManagerComponents_get_Spider_m1443_MethodInfo = 
{
	"get_Spider"/* name */
	, (methodPointerType)&SCR_ManagerComponents_get_Spider_m1443/* method */
	, &SCR_ManagerComponents_t380_il2cpp_TypeInfo/* declaring_type */
	, &SCR_Spider_t384_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 378/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SCR_Spider_t384_0_0_0;
static const ParameterInfo SCR_ManagerComponents_t380_SCR_ManagerComponents_set_Spider_m1444_ParameterInfos[] = 
{
	{"value", 0, 134217815, 0, &SCR_Spider_t384_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_ManagerComponents::set_Spider(SCR_Spider)
extern const MethodInfo SCR_ManagerComponents_set_Spider_m1444_MethodInfo = 
{
	"set_Spider"/* name */
	, (methodPointerType)&SCR_ManagerComponents_set_Spider_m1444/* method */
	, &SCR_ManagerComponents_t380_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, SCR_ManagerComponents_t380_SCR_ManagerComponents_set_Spider_m1444_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 379/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// SCR_Joystick SCR_ManagerComponents::get_Joystick()
extern const MethodInfo SCR_ManagerComponents_get_Joystick_m1445_MethodInfo = 
{
	"get_Joystick"/* name */
	, (methodPointerType)&SCR_ManagerComponents_get_Joystick_m1445/* method */
	, &SCR_ManagerComponents_t380_il2cpp_TypeInfo/* declaring_type */
	, &SCR_Joystick_t346_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 380/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SCR_Joystick_t346_0_0_0;
static const ParameterInfo SCR_ManagerComponents_t380_SCR_ManagerComponents_set_Joystick_m1446_ParameterInfos[] = 
{
	{"value", 0, 134217816, 0, &SCR_Joystick_t346_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_ManagerComponents::set_Joystick(SCR_Joystick)
extern const MethodInfo SCR_ManagerComponents_set_Joystick_m1446_MethodInfo = 
{
	"set_Joystick"/* name */
	, (methodPointerType)&SCR_ManagerComponents_set_Joystick_m1446/* method */
	, &SCR_ManagerComponents_t380_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, SCR_ManagerComponents_t380_SCR_ManagerComponents_set_Joystick_m1446_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 381/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// SCR_FlyButton SCR_ManagerComponents::get_FlyButton()
extern const MethodInfo SCR_ManagerComponents_get_FlyButton_m1447_MethodInfo = 
{
	"get_FlyButton"/* name */
	, (methodPointerType)&SCR_ManagerComponents_get_FlyButton_m1447/* method */
	, &SCR_ManagerComponents_t380_il2cpp_TypeInfo/* declaring_type */
	, &SCR_FlyButton_t345_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 382/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SCR_FlyButton_t345_0_0_0;
static const ParameterInfo SCR_ManagerComponents_t380_SCR_ManagerComponents_set_FlyButton_m1448_ParameterInfos[] = 
{
	{"value", 0, 134217817, 0, &SCR_FlyButton_t345_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_ManagerComponents::set_FlyButton(SCR_FlyButton)
extern const MethodInfo SCR_ManagerComponents_set_FlyButton_m1448_MethodInfo = 
{
	"set_FlyButton"/* name */
	, (methodPointerType)&SCR_ManagerComponents_set_FlyButton_m1448/* method */
	, &SCR_ManagerComponents_t380_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, SCR_ManagerComponents_t380_SCR_ManagerComponents_set_FlyButton_m1448_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 383/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Transform SCR_ManagerComponents::get_LeftFrame()
extern const MethodInfo SCR_ManagerComponents_get_LeftFrame_m1449_MethodInfo = 
{
	"get_LeftFrame"/* name */
	, (methodPointerType)&SCR_ManagerComponents_get_LeftFrame_m1449/* method */
	, &SCR_ManagerComponents_t380_il2cpp_TypeInfo/* declaring_type */
	, &Transform_t1_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 384/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Transform_t1_0_0_0;
static const ParameterInfo SCR_ManagerComponents_t380_SCR_ManagerComponents_set_LeftFrame_m1450_ParameterInfos[] = 
{
	{"value", 0, 134217818, 0, &Transform_t1_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_ManagerComponents::set_LeftFrame(UnityEngine.Transform)
extern const MethodInfo SCR_ManagerComponents_set_LeftFrame_m1450_MethodInfo = 
{
	"set_LeftFrame"/* name */
	, (methodPointerType)&SCR_ManagerComponents_set_LeftFrame_m1450/* method */
	, &SCR_ManagerComponents_t380_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, SCR_ManagerComponents_t380_SCR_ManagerComponents_set_LeftFrame_m1450_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 385/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Transform SCR_ManagerComponents::get_RightFrame()
extern const MethodInfo SCR_ManagerComponents_get_RightFrame_m1451_MethodInfo = 
{
	"get_RightFrame"/* name */
	, (methodPointerType)&SCR_ManagerComponents_get_RightFrame_m1451/* method */
	, &SCR_ManagerComponents_t380_il2cpp_TypeInfo/* declaring_type */
	, &Transform_t1_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 386/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Transform_t1_0_0_0;
static const ParameterInfo SCR_ManagerComponents_t380_SCR_ManagerComponents_set_RightFrame_m1452_ParameterInfos[] = 
{
	{"value", 0, 134217819, 0, &Transform_t1_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_ManagerComponents::set_RightFrame(UnityEngine.Transform)
extern const MethodInfo SCR_ManagerComponents_set_RightFrame_m1452_MethodInfo = 
{
	"set_RightFrame"/* name */
	, (methodPointerType)&SCR_ManagerComponents_set_RightFrame_m1452/* method */
	, &SCR_ManagerComponents_t380_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, SCR_ManagerComponents_t380_SCR_ManagerComponents_set_RightFrame_m1452_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 387/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Transform SCR_ManagerComponents::get_BottomFrame()
extern const MethodInfo SCR_ManagerComponents_get_BottomFrame_m1453_MethodInfo = 
{
	"get_BottomFrame"/* name */
	, (methodPointerType)&SCR_ManagerComponents_get_BottomFrame_m1453/* method */
	, &SCR_ManagerComponents_t380_il2cpp_TypeInfo/* declaring_type */
	, &Transform_t1_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 388/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Transform_t1_0_0_0;
static const ParameterInfo SCR_ManagerComponents_t380_SCR_ManagerComponents_set_BottomFrame_m1454_ParameterInfos[] = 
{
	{"value", 0, 134217820, 0, &Transform_t1_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_ManagerComponents::set_BottomFrame(UnityEngine.Transform)
extern const MethodInfo SCR_ManagerComponents_set_BottomFrame_m1454_MethodInfo = 
{
	"set_BottomFrame"/* name */
	, (methodPointerType)&SCR_ManagerComponents_set_BottomFrame_m1454/* method */
	, &SCR_ManagerComponents_t380_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, SCR_ManagerComponents_t380_SCR_ManagerComponents_set_BottomFrame_m1454_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 389/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SCR_ManagerComponents_t380_MethodInfos[] =
{
	&SCR_ManagerComponents__ctor_m1424_MethodInfo,
	&SCR_ManagerComponents_Awake_m1425_MethodInfo,
	&SCR_ManagerComponents_OnLevelWasLoaded_m1426_MethodInfo,
	&SCR_ManagerComponents_get_ManagerInstance_m1427_MethodInfo,
	&SCR_ManagerComponents_set_ManagerInstance_m1428_MethodInfo,
	&SCR_ManagerComponents_get_PointManager_m1429_MethodInfo,
	&SCR_ManagerComponents_set_PointManager_m1430_MethodInfo,
	&SCR_ManagerComponents_get_LevelManager_m1431_MethodInfo,
	&SCR_ManagerComponents_set_LevelManager_m1432_MethodInfo,
	&SCR_ManagerComponents_get_SaveData_m1433_MethodInfo,
	&SCR_ManagerComponents_set_SaveData_m1434_MethodInfo,
	&SCR_ManagerComponents_get_TimeManager_m1435_MethodInfo,
	&SCR_ManagerComponents_set_TimeManager_m1436_MethodInfo,
	&SCR_ManagerComponents_get_GUIManager_m1437_MethodInfo,
	&SCR_ManagerComponents_set_GUIManager_m1438_MethodInfo,
	&SCR_ManagerComponents_get_SoundManager_m1439_MethodInfo,
	&SCR_ManagerComponents_set_SoundManager_m1440_MethodInfo,
	&SCR_ManagerComponents_get_AvatarMove_m1441_MethodInfo,
	&SCR_ManagerComponents_set_AvatarMove_m1442_MethodInfo,
	&SCR_ManagerComponents_get_Spider_m1443_MethodInfo,
	&SCR_ManagerComponents_set_Spider_m1444_MethodInfo,
	&SCR_ManagerComponents_get_Joystick_m1445_MethodInfo,
	&SCR_ManagerComponents_set_Joystick_m1446_MethodInfo,
	&SCR_ManagerComponents_get_FlyButton_m1447_MethodInfo,
	&SCR_ManagerComponents_set_FlyButton_m1448_MethodInfo,
	&SCR_ManagerComponents_get_LeftFrame_m1449_MethodInfo,
	&SCR_ManagerComponents_set_LeftFrame_m1450_MethodInfo,
	&SCR_ManagerComponents_get_RightFrame_m1451_MethodInfo,
	&SCR_ManagerComponents_set_RightFrame_m1452_MethodInfo,
	&SCR_ManagerComponents_get_BottomFrame_m1453_MethodInfo,
	&SCR_ManagerComponents_set_BottomFrame_m1454_MethodInfo,
	NULL
};
extern const MethodInfo SCR_ManagerComponents_get_ManagerInstance_m1427_MethodInfo;
extern const MethodInfo SCR_ManagerComponents_set_ManagerInstance_m1428_MethodInfo;
static const PropertyInfo SCR_ManagerComponents_t380____ManagerInstance_PropertyInfo = 
{
	&SCR_ManagerComponents_t380_il2cpp_TypeInfo/* parent */
	, "ManagerInstance"/* name */
	, &SCR_ManagerComponents_get_ManagerInstance_m1427_MethodInfo/* get */
	, &SCR_ManagerComponents_set_ManagerInstance_m1428_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo SCR_ManagerComponents_get_PointManager_m1429_MethodInfo;
extern const MethodInfo SCR_ManagerComponents_set_PointManager_m1430_MethodInfo;
static const PropertyInfo SCR_ManagerComponents_t380____PointManager_PropertyInfo = 
{
	&SCR_ManagerComponents_t380_il2cpp_TypeInfo/* parent */
	, "PointManager"/* name */
	, &SCR_ManagerComponents_get_PointManager_m1429_MethodInfo/* get */
	, &SCR_ManagerComponents_set_PointManager_m1430_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo SCR_ManagerComponents_get_LevelManager_m1431_MethodInfo;
extern const MethodInfo SCR_ManagerComponents_set_LevelManager_m1432_MethodInfo;
static const PropertyInfo SCR_ManagerComponents_t380____LevelManager_PropertyInfo = 
{
	&SCR_ManagerComponents_t380_il2cpp_TypeInfo/* parent */
	, "LevelManager"/* name */
	, &SCR_ManagerComponents_get_LevelManager_m1431_MethodInfo/* get */
	, &SCR_ManagerComponents_set_LevelManager_m1432_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo SCR_ManagerComponents_get_SaveData_m1433_MethodInfo;
extern const MethodInfo SCR_ManagerComponents_set_SaveData_m1434_MethodInfo;
static const PropertyInfo SCR_ManagerComponents_t380____SaveData_PropertyInfo = 
{
	&SCR_ManagerComponents_t380_il2cpp_TypeInfo/* parent */
	, "SaveData"/* name */
	, &SCR_ManagerComponents_get_SaveData_m1433_MethodInfo/* get */
	, &SCR_ManagerComponents_set_SaveData_m1434_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo SCR_ManagerComponents_get_TimeManager_m1435_MethodInfo;
extern const MethodInfo SCR_ManagerComponents_set_TimeManager_m1436_MethodInfo;
static const PropertyInfo SCR_ManagerComponents_t380____TimeManager_PropertyInfo = 
{
	&SCR_ManagerComponents_t380_il2cpp_TypeInfo/* parent */
	, "TimeManager"/* name */
	, &SCR_ManagerComponents_get_TimeManager_m1435_MethodInfo/* get */
	, &SCR_ManagerComponents_set_TimeManager_m1436_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo SCR_ManagerComponents_get_GUIManager_m1437_MethodInfo;
extern const MethodInfo SCR_ManagerComponents_set_GUIManager_m1438_MethodInfo;
static const PropertyInfo SCR_ManagerComponents_t380____GUIManager_PropertyInfo = 
{
	&SCR_ManagerComponents_t380_il2cpp_TypeInfo/* parent */
	, "GUIManager"/* name */
	, &SCR_ManagerComponents_get_GUIManager_m1437_MethodInfo/* get */
	, &SCR_ManagerComponents_set_GUIManager_m1438_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo SCR_ManagerComponents_get_SoundManager_m1439_MethodInfo;
extern const MethodInfo SCR_ManagerComponents_set_SoundManager_m1440_MethodInfo;
static const PropertyInfo SCR_ManagerComponents_t380____SoundManager_PropertyInfo = 
{
	&SCR_ManagerComponents_t380_il2cpp_TypeInfo/* parent */
	, "SoundManager"/* name */
	, &SCR_ManagerComponents_get_SoundManager_m1439_MethodInfo/* get */
	, &SCR_ManagerComponents_set_SoundManager_m1440_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo SCR_ManagerComponents_get_AvatarMove_m1441_MethodInfo;
extern const MethodInfo SCR_ManagerComponents_set_AvatarMove_m1442_MethodInfo;
static const PropertyInfo SCR_ManagerComponents_t380____AvatarMove_PropertyInfo = 
{
	&SCR_ManagerComponents_t380_il2cpp_TypeInfo/* parent */
	, "AvatarMove"/* name */
	, &SCR_ManagerComponents_get_AvatarMove_m1441_MethodInfo/* get */
	, &SCR_ManagerComponents_set_AvatarMove_m1442_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo SCR_ManagerComponents_get_Spider_m1443_MethodInfo;
extern const MethodInfo SCR_ManagerComponents_set_Spider_m1444_MethodInfo;
static const PropertyInfo SCR_ManagerComponents_t380____Spider_PropertyInfo = 
{
	&SCR_ManagerComponents_t380_il2cpp_TypeInfo/* parent */
	, "Spider"/* name */
	, &SCR_ManagerComponents_get_Spider_m1443_MethodInfo/* get */
	, &SCR_ManagerComponents_set_Spider_m1444_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo SCR_ManagerComponents_get_Joystick_m1445_MethodInfo;
extern const MethodInfo SCR_ManagerComponents_set_Joystick_m1446_MethodInfo;
static const PropertyInfo SCR_ManagerComponents_t380____Joystick_PropertyInfo = 
{
	&SCR_ManagerComponents_t380_il2cpp_TypeInfo/* parent */
	, "Joystick"/* name */
	, &SCR_ManagerComponents_get_Joystick_m1445_MethodInfo/* get */
	, &SCR_ManagerComponents_set_Joystick_m1446_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo SCR_ManagerComponents_get_FlyButton_m1447_MethodInfo;
extern const MethodInfo SCR_ManagerComponents_set_FlyButton_m1448_MethodInfo;
static const PropertyInfo SCR_ManagerComponents_t380____FlyButton_PropertyInfo = 
{
	&SCR_ManagerComponents_t380_il2cpp_TypeInfo/* parent */
	, "FlyButton"/* name */
	, &SCR_ManagerComponents_get_FlyButton_m1447_MethodInfo/* get */
	, &SCR_ManagerComponents_set_FlyButton_m1448_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo SCR_ManagerComponents_get_LeftFrame_m1449_MethodInfo;
extern const MethodInfo SCR_ManagerComponents_set_LeftFrame_m1450_MethodInfo;
static const PropertyInfo SCR_ManagerComponents_t380____LeftFrame_PropertyInfo = 
{
	&SCR_ManagerComponents_t380_il2cpp_TypeInfo/* parent */
	, "LeftFrame"/* name */
	, &SCR_ManagerComponents_get_LeftFrame_m1449_MethodInfo/* get */
	, &SCR_ManagerComponents_set_LeftFrame_m1450_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo SCR_ManagerComponents_get_RightFrame_m1451_MethodInfo;
extern const MethodInfo SCR_ManagerComponents_set_RightFrame_m1452_MethodInfo;
static const PropertyInfo SCR_ManagerComponents_t380____RightFrame_PropertyInfo = 
{
	&SCR_ManagerComponents_t380_il2cpp_TypeInfo/* parent */
	, "RightFrame"/* name */
	, &SCR_ManagerComponents_get_RightFrame_m1451_MethodInfo/* get */
	, &SCR_ManagerComponents_set_RightFrame_m1452_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo SCR_ManagerComponents_get_BottomFrame_m1453_MethodInfo;
extern const MethodInfo SCR_ManagerComponents_set_BottomFrame_m1454_MethodInfo;
static const PropertyInfo SCR_ManagerComponents_t380____BottomFrame_PropertyInfo = 
{
	&SCR_ManagerComponents_t380_il2cpp_TypeInfo/* parent */
	, "BottomFrame"/* name */
	, &SCR_ManagerComponents_get_BottomFrame_m1453_MethodInfo/* get */
	, &SCR_ManagerComponents_set_BottomFrame_m1454_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* SCR_ManagerComponents_t380_PropertyInfos[] =
{
	&SCR_ManagerComponents_t380____ManagerInstance_PropertyInfo,
	&SCR_ManagerComponents_t380____PointManager_PropertyInfo,
	&SCR_ManagerComponents_t380____LevelManager_PropertyInfo,
	&SCR_ManagerComponents_t380____SaveData_PropertyInfo,
	&SCR_ManagerComponents_t380____TimeManager_PropertyInfo,
	&SCR_ManagerComponents_t380____GUIManager_PropertyInfo,
	&SCR_ManagerComponents_t380____SoundManager_PropertyInfo,
	&SCR_ManagerComponents_t380____AvatarMove_PropertyInfo,
	&SCR_ManagerComponents_t380____Spider_PropertyInfo,
	&SCR_ManagerComponents_t380____Joystick_PropertyInfo,
	&SCR_ManagerComponents_t380____FlyButton_PropertyInfo,
	&SCR_ManagerComponents_t380____LeftFrame_PropertyInfo,
	&SCR_ManagerComponents_t380____RightFrame_PropertyInfo,
	&SCR_ManagerComponents_t380____BottomFrame_PropertyInfo,
	NULL
};
static const Il2CppMethodReference SCR_ManagerComponents_t380_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool SCR_ManagerComponents_t380_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType SCR_ManagerComponents_t380_1_0_0;
struct SCR_ManagerComponents_t380;
const Il2CppTypeDefinitionMetadata SCR_ManagerComponents_t380_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, SCR_ManagerComponents_t380_VTable/* vtableMethods */
	, SCR_ManagerComponents_t380_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 484/* fieldStart */

};
TypeInfo SCR_ManagerComponents_t380_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "SCR_ManagerComponents"/* name */
	, ""/* namespaze */
	, SCR_ManagerComponents_t380_MethodInfos/* methods */
	, SCR_ManagerComponents_t380_PropertyInfos/* properties */
	, NULL/* events */
	, &SCR_ManagerComponents_t380_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SCR_ManagerComponents_t380_0_0_0/* byval_arg */
	, &SCR_ManagerComponents_t380_1_0_0/* this_arg */
	, &SCR_ManagerComponents_t380_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SCR_ManagerComponents_t380)/* instance_size */
	, sizeof (SCR_ManagerComponents_t380)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(SCR_ManagerComponents_t380_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 31/* method_count */
	, 14/* property_count */
	, 14/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SCR_ManagerInstantiator
#include "AssemblyU2DCSharp_SCR_ManagerInstantiator.h"
// Metadata Definition SCR_ManagerInstantiator
extern TypeInfo SCR_ManagerInstantiator_t385_il2cpp_TypeInfo;
// SCR_ManagerInstantiator
#include "AssemblyU2DCSharp_SCR_ManagerInstantiatorMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_ManagerInstantiator::.ctor()
extern const MethodInfo SCR_ManagerInstantiator__ctor_m1455_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SCR_ManagerInstantiator__ctor_m1455/* method */
	, &SCR_ManagerInstantiator_t385_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 390/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_ManagerInstantiator::Awake()
extern const MethodInfo SCR_ManagerInstantiator_Awake_m1456_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&SCR_ManagerInstantiator_Awake_m1456/* method */
	, &SCR_ManagerInstantiator_t385_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 391/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SCR_ManagerInstantiator_t385_MethodInfos[] =
{
	&SCR_ManagerInstantiator__ctor_m1455_MethodInfo,
	&SCR_ManagerInstantiator_Awake_m1456_MethodInfo,
	NULL
};
static const Il2CppMethodReference SCR_ManagerInstantiator_t385_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool SCR_ManagerInstantiator_t385_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType SCR_ManagerInstantiator_t385_0_0_0;
extern const Il2CppType SCR_ManagerInstantiator_t385_1_0_0;
struct SCR_ManagerInstantiator_t385;
const Il2CppTypeDefinitionMetadata SCR_ManagerInstantiator_t385_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, SCR_ManagerInstantiator_t385_VTable/* vtableMethods */
	, SCR_ManagerInstantiator_t385_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 498/* fieldStart */

};
TypeInfo SCR_ManagerInstantiator_t385_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "SCR_ManagerInstantiator"/* name */
	, ""/* namespaze */
	, SCR_ManagerInstantiator_t385_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SCR_ManagerInstantiator_t385_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SCR_ManagerInstantiator_t385_0_0_0/* byval_arg */
	, &SCR_ManagerInstantiator_t385_1_0_0/* this_arg */
	, &SCR_ManagerInstantiator_t385_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SCR_ManagerInstantiator_t385)/* instance_size */
	, sizeof (SCR_ManagerInstantiator_t385)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SCR_PointManager/CollectibleType
#include "AssemblyU2DCSharp_SCR_PointManager_CollectibleType.h"
// Metadata Definition SCR_PointManager/CollectibleType
extern TypeInfo CollectibleType_t386_il2cpp_TypeInfo;
// SCR_PointManager/CollectibleType
#include "AssemblyU2DCSharp_SCR_PointManager_CollectibleTypeMethodDeclarations.h"
static const MethodInfo* CollectibleType_t386_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference CollectibleType_t386_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool CollectibleType_t386_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair CollectibleType_t386_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CollectibleType_t386_0_0_0;
extern const Il2CppType CollectibleType_t386_1_0_0;
extern TypeInfo SCR_PointManager_t381_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata CollectibleType_t386_DefinitionMetadata = 
{
	&SCR_PointManager_t381_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CollectibleType_t386_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, CollectibleType_t386_VTable/* vtableMethods */
	, CollectibleType_t386_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 499/* fieldStart */

};
TypeInfo CollectibleType_t386_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CollectibleType"/* name */
	, ""/* namespaze */
	, CollectibleType_t386_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CollectibleType_t386_0_0_0/* byval_arg */
	, &CollectibleType_t386_1_0_0/* this_arg */
	, &CollectibleType_t386_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CollectibleType_t386)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (CollectibleType_t386)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// SCR_PointManager
#include "AssemblyU2DCSharp_SCR_PointManager.h"
// Metadata Definition SCR_PointManager
// SCR_PointManager
#include "AssemblyU2DCSharp_SCR_PointManagerMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_PointManager::.ctor()
extern const MethodInfo SCR_PointManager__ctor_m1457_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SCR_PointManager__ctor_m1457/* method */
	, &SCR_PointManager_t381_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 392/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_PointManager::Awake()
extern const MethodInfo SCR_PointManager_Awake_m1458_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&SCR_PointManager_Awake_m1458/* method */
	, &SCR_PointManager_t381_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 393/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_PointManager::Start()
extern const MethodInfo SCR_PointManager_Start_m1459_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&SCR_PointManager_Start_m1459/* method */
	, &SCR_PointManager_t381_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 394/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_PointManager::OnLevelWasLoaded()
extern const MethodInfo SCR_PointManager_OnLevelWasLoaded_m1460_MethodInfo = 
{
	"OnLevelWasLoaded"/* name */
	, (methodPointerType)&SCR_PointManager_OnLevelWasLoaded_m1460/* method */
	, &SCR_PointManager_t381_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 395/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_PointManager::Update()
extern const MethodInfo SCR_PointManager_Update_m1461_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&SCR_PointManager_Update_m1461/* method */
	, &SCR_PointManager_t381_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 396/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_PointManager::LevelInit()
extern const MethodInfo SCR_PointManager_LevelInit_m1462_MethodInfo = 
{
	"LevelInit"/* name */
	, (methodPointerType)&SCR_PointManager_LevelInit_m1462/* method */
	, &SCR_PointManager_t381_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 397/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_PointManager::StartScoring()
extern const MethodInfo SCR_PointManager_StartScoring_m1463_MethodInfo = 
{
	"StartScoring"/* name */
	, (methodPointerType)&SCR_PointManager_StartScoring_m1463/* method */
	, &SCR_PointManager_t381_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 398/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_PointManager::IncrementMovingPucerons()
extern const MethodInfo SCR_PointManager_IncrementMovingPucerons_m1464_MethodInfo = 
{
	"IncrementMovingPucerons"/* name */
	, (methodPointerType)&SCR_PointManager_IncrementMovingPucerons_m1464/* method */
	, &SCR_PointManager_t381_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 399/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_PointManager::ValidateScores()
extern const MethodInfo SCR_PointManager_ValidateScores_m1465_MethodInfo = 
{
	"ValidateScores"/* name */
	, (methodPointerType)&SCR_PointManager_ValidateScores_m1465/* method */
	, &SCR_PointManager_t381_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 400/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType CollectibleType_t386_0_0_0;
static const ParameterInfo SCR_PointManager_t381_SCR_PointManager_AcquireCollectible_m1466_ParameterInfos[] = 
{
	{"type", 0, 134217821, 0, &CollectibleType_t386_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_PointManager::AcquireCollectible(SCR_PointManager/CollectibleType)
extern const MethodInfo SCR_PointManager_AcquireCollectible_m1466_MethodInfo = 
{
	"AcquireCollectible"/* name */
	, (methodPointerType)&SCR_PointManager_AcquireCollectible_m1466/* method */
	, &SCR_PointManager_t381_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, SCR_PointManager_t381_SCR_PointManager_AcquireCollectible_m1466_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 401/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_PointManager::LosePucerons()
extern const MethodInfo SCR_PointManager_LosePucerons_m1467_MethodInfo = 
{
	"LosePucerons"/* name */
	, (methodPointerType)&SCR_PointManager_LosePucerons_m1467/* method */
	, &SCR_PointManager_t381_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 402/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo SCR_PointManager_t381_SCR_PointManager_CompareHighscore_m1468_ParameterInfos[] = 
{
	{"level", 0, 134217822, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Boolean SCR_PointManager::CompareHighscore(System.Int32)
extern const MethodInfo SCR_PointManager_CompareHighscore_m1468_MethodInfo = 
{
	"CompareHighscore"/* name */
	, (methodPointerType)&SCR_PointManager_CompareHighscore_m1468/* method */
	, &SCR_PointManager_t381_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Int32_t253/* invoker_method */
	, SCR_PointManager_t381_SCR_PointManager_CompareHighscore_m1468_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 403/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo SCR_PointManager_t381_SCR_PointManager_SetNewHighScore_m1469_ParameterInfos[] = 
{
	{"level", 0, 134217823, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_PointManager::SetNewHighScore(System.Int32)
extern const MethodInfo SCR_PointManager_SetNewHighScore_m1469_MethodInfo = 
{
	"SetNewHighScore"/* name */
	, (methodPointerType)&SCR_PointManager_SetNewHighScore_m1469/* method */
	, &SCR_PointManager_t381_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, SCR_PointManager_t381_SCR_PointManager_SetNewHighScore_m1469_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 404/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 SCR_PointManager::get_LadybugsCurrentLevel()
extern const MethodInfo SCR_PointManager_get_LadybugsCurrentLevel_m1470_MethodInfo = 
{
	"get_LadybugsCurrentLevel"/* name */
	, (methodPointerType)&SCR_PointManager_get_LadybugsCurrentLevel_m1470/* method */
	, &SCR_PointManager_t381_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 405/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 SCR_PointManager::get_PuceronsCurrentLevel()
extern const MethodInfo SCR_PointManager_get_PuceronsCurrentLevel_m1471_MethodInfo = 
{
	"get_PuceronsCurrentLevel"/* name */
	, (methodPointerType)&SCR_PointManager_get_PuceronsCurrentLevel_m1471/* method */
	, &SCR_PointManager_t381_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 406/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 SCR_PointManager::get_TotalScore()
extern const MethodInfo SCR_PointManager_get_TotalScore_m1472_MethodInfo = 
{
	"get_TotalScore"/* name */
	, (methodPointerType)&SCR_PointManager_get_TotalScore_m1472/* method */
	, &SCR_PointManager_t381_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 407/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32U5BU5D_t242_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32[] SCR_PointManager::get_HighScores()
extern const MethodInfo SCR_PointManager_get_HighScores_m1473_MethodInfo = 
{
	"get_HighScores"/* name */
	, (methodPointerType)&SCR_PointManager_get_HighScores_m1473/* method */
	, &SCR_PointManager_t381_il2cpp_TypeInfo/* declaring_type */
	, &Int32U5BU5D_t242_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 408/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32U5BU5D_t242_0_0_0;
static const ParameterInfo SCR_PointManager_t381_SCR_PointManager_set_HighScores_m1474_ParameterInfos[] = 
{
	{"value", 0, 134217824, 0, &Int32U5BU5D_t242_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_PointManager::set_HighScores(System.Int32[])
extern const MethodInfo SCR_PointManager_set_HighScores_m1474_MethodInfo = 
{
	"set_HighScores"/* name */
	, (methodPointerType)&SCR_PointManager_set_HighScores_m1474/* method */
	, &SCR_PointManager_t381_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, SCR_PointManager_t381_SCR_PointManager_set_HighScores_m1474_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 409/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 SCR_PointManager::get_ScoreCumul()
extern const MethodInfo SCR_PointManager_get_ScoreCumul_m1475_MethodInfo = 
{
	"get_ScoreCumul"/* name */
	, (methodPointerType)&SCR_PointManager_get_ScoreCumul_m1475/* method */
	, &SCR_PointManager_t381_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 410/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32[] SCR_PointManager::get_PointsForLevelAccess()
extern const MethodInfo SCR_PointManager_get_PointsForLevelAccess_m1476_MethodInfo = 
{
	"get_PointsForLevelAccess"/* name */
	, (methodPointerType)&SCR_PointManager_get_PointsForLevelAccess_m1476/* method */
	, &SCR_PointManager_t381_il2cpp_TypeInfo/* declaring_type */
	, &Int32U5BU5D_t242_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 411/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean SCR_PointManager::get_IsScoreCounting()
extern const MethodInfo SCR_PointManager_get_IsScoreCounting_m1477_MethodInfo = 
{
	"get_IsScoreCounting"/* name */
	, (methodPointerType)&SCR_PointManager_get_IsScoreCounting_m1477/* method */
	, &SCR_PointManager_t381_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 412/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType List_1_t387_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.Generic.List`1<SCR_Puceron> SCR_PointManager::get_PuceronsCollected()
extern const MethodInfo SCR_PointManager_get_PuceronsCollected_m1478_MethodInfo = 
{
	"get_PuceronsCollected"/* name */
	, (methodPointerType)&SCR_PointManager_get_PuceronsCollected_m1478/* method */
	, &SCR_PointManager_t381_il2cpp_TypeInfo/* declaring_type */
	, &List_1_t387_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 413/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType List_1_t387_0_0_0;
static const ParameterInfo SCR_PointManager_t381_SCR_PointManager_set_PuceronsCollected_m1479_ParameterInfos[] = 
{
	{"value", 0, 134217825, 0, &List_1_t387_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_PointManager::set_PuceronsCollected(System.Collections.Generic.List`1<SCR_Puceron>)
extern const MethodInfo SCR_PointManager_set_PuceronsCollected_m1479_MethodInfo = 
{
	"set_PuceronsCollected"/* name */
	, (methodPointerType)&SCR_PointManager_set_PuceronsCollected_m1479/* method */
	, &SCR_PointManager_t381_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, SCR_PointManager_t381_SCR_PointManager_set_PuceronsCollected_m1479_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 414/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SCR_PointManager_t381_MethodInfos[] =
{
	&SCR_PointManager__ctor_m1457_MethodInfo,
	&SCR_PointManager_Awake_m1458_MethodInfo,
	&SCR_PointManager_Start_m1459_MethodInfo,
	&SCR_PointManager_OnLevelWasLoaded_m1460_MethodInfo,
	&SCR_PointManager_Update_m1461_MethodInfo,
	&SCR_PointManager_LevelInit_m1462_MethodInfo,
	&SCR_PointManager_StartScoring_m1463_MethodInfo,
	&SCR_PointManager_IncrementMovingPucerons_m1464_MethodInfo,
	&SCR_PointManager_ValidateScores_m1465_MethodInfo,
	&SCR_PointManager_AcquireCollectible_m1466_MethodInfo,
	&SCR_PointManager_LosePucerons_m1467_MethodInfo,
	&SCR_PointManager_CompareHighscore_m1468_MethodInfo,
	&SCR_PointManager_SetNewHighScore_m1469_MethodInfo,
	&SCR_PointManager_get_LadybugsCurrentLevel_m1470_MethodInfo,
	&SCR_PointManager_get_PuceronsCurrentLevel_m1471_MethodInfo,
	&SCR_PointManager_get_TotalScore_m1472_MethodInfo,
	&SCR_PointManager_get_HighScores_m1473_MethodInfo,
	&SCR_PointManager_set_HighScores_m1474_MethodInfo,
	&SCR_PointManager_get_ScoreCumul_m1475_MethodInfo,
	&SCR_PointManager_get_PointsForLevelAccess_m1476_MethodInfo,
	&SCR_PointManager_get_IsScoreCounting_m1477_MethodInfo,
	&SCR_PointManager_get_PuceronsCollected_m1478_MethodInfo,
	&SCR_PointManager_set_PuceronsCollected_m1479_MethodInfo,
	NULL
};
extern const MethodInfo SCR_PointManager_get_LadybugsCurrentLevel_m1470_MethodInfo;
static const PropertyInfo SCR_PointManager_t381____LadybugsCurrentLevel_PropertyInfo = 
{
	&SCR_PointManager_t381_il2cpp_TypeInfo/* parent */
	, "LadybugsCurrentLevel"/* name */
	, &SCR_PointManager_get_LadybugsCurrentLevel_m1470_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo SCR_PointManager_get_PuceronsCurrentLevel_m1471_MethodInfo;
static const PropertyInfo SCR_PointManager_t381____PuceronsCurrentLevel_PropertyInfo = 
{
	&SCR_PointManager_t381_il2cpp_TypeInfo/* parent */
	, "PuceronsCurrentLevel"/* name */
	, &SCR_PointManager_get_PuceronsCurrentLevel_m1471_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo SCR_PointManager_get_TotalScore_m1472_MethodInfo;
static const PropertyInfo SCR_PointManager_t381____TotalScore_PropertyInfo = 
{
	&SCR_PointManager_t381_il2cpp_TypeInfo/* parent */
	, "TotalScore"/* name */
	, &SCR_PointManager_get_TotalScore_m1472_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo SCR_PointManager_get_HighScores_m1473_MethodInfo;
extern const MethodInfo SCR_PointManager_set_HighScores_m1474_MethodInfo;
static const PropertyInfo SCR_PointManager_t381____HighScores_PropertyInfo = 
{
	&SCR_PointManager_t381_il2cpp_TypeInfo/* parent */
	, "HighScores"/* name */
	, &SCR_PointManager_get_HighScores_m1473_MethodInfo/* get */
	, &SCR_PointManager_set_HighScores_m1474_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo SCR_PointManager_get_ScoreCumul_m1475_MethodInfo;
static const PropertyInfo SCR_PointManager_t381____ScoreCumul_PropertyInfo = 
{
	&SCR_PointManager_t381_il2cpp_TypeInfo/* parent */
	, "ScoreCumul"/* name */
	, &SCR_PointManager_get_ScoreCumul_m1475_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo SCR_PointManager_get_PointsForLevelAccess_m1476_MethodInfo;
static const PropertyInfo SCR_PointManager_t381____PointsForLevelAccess_PropertyInfo = 
{
	&SCR_PointManager_t381_il2cpp_TypeInfo/* parent */
	, "PointsForLevelAccess"/* name */
	, &SCR_PointManager_get_PointsForLevelAccess_m1476_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo SCR_PointManager_get_IsScoreCounting_m1477_MethodInfo;
static const PropertyInfo SCR_PointManager_t381____IsScoreCounting_PropertyInfo = 
{
	&SCR_PointManager_t381_il2cpp_TypeInfo/* parent */
	, "IsScoreCounting"/* name */
	, &SCR_PointManager_get_IsScoreCounting_m1477_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo SCR_PointManager_get_PuceronsCollected_m1478_MethodInfo;
extern const MethodInfo SCR_PointManager_set_PuceronsCollected_m1479_MethodInfo;
static const PropertyInfo SCR_PointManager_t381____PuceronsCollected_PropertyInfo = 
{
	&SCR_PointManager_t381_il2cpp_TypeInfo/* parent */
	, "PuceronsCollected"/* name */
	, &SCR_PointManager_get_PuceronsCollected_m1478_MethodInfo/* get */
	, &SCR_PointManager_set_PuceronsCollected_m1479_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* SCR_PointManager_t381_PropertyInfos[] =
{
	&SCR_PointManager_t381____LadybugsCurrentLevel_PropertyInfo,
	&SCR_PointManager_t381____PuceronsCurrentLevel_PropertyInfo,
	&SCR_PointManager_t381____TotalScore_PropertyInfo,
	&SCR_PointManager_t381____HighScores_PropertyInfo,
	&SCR_PointManager_t381____ScoreCumul_PropertyInfo,
	&SCR_PointManager_t381____PointsForLevelAccess_PropertyInfo,
	&SCR_PointManager_t381____IsScoreCounting_PropertyInfo,
	&SCR_PointManager_t381____PuceronsCollected_PropertyInfo,
	NULL
};
static const Il2CppType* SCR_PointManager_t381_il2cpp_TypeInfo__nestedTypes[1] =
{
	&CollectibleType_t386_0_0_0,
};
static const Il2CppMethodReference SCR_PointManager_t381_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool SCR_PointManager_t381_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType SCR_PointManager_t381_1_0_0;
struct SCR_PointManager_t381;
const Il2CppTypeDefinitionMetadata SCR_PointManager_t381_DefinitionMetadata = 
{
	NULL/* declaringType */
	, SCR_PointManager_t381_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, SCR_PointManager_t381_VTable/* vtableMethods */
	, SCR_PointManager_t381_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 502/* fieldStart */

};
TypeInfo SCR_PointManager_t381_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "SCR_PointManager"/* name */
	, ""/* namespaze */
	, SCR_PointManager_t381_MethodInfos/* methods */
	, SCR_PointManager_t381_PropertyInfos/* properties */
	, NULL/* events */
	, &SCR_PointManager_t381_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SCR_PointManager_t381_0_0_0/* byval_arg */
	, &SCR_PointManager_t381_1_0_0/* this_arg */
	, &SCR_PointManager_t381_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SCR_PointManager_t381)/* instance_size */
	, sizeof (SCR_PointManager_t381)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 23/* method_count */
	, 8/* property_count */
	, 15/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SCR_SaveData
#include "AssemblyU2DCSharp_SCR_SaveData.h"
// Metadata Definition SCR_SaveData
extern TypeInfo SCR_SaveData_t382_il2cpp_TypeInfo;
// SCR_SaveData
#include "AssemblyU2DCSharp_SCR_SaveDataMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_SaveData::.ctor()
extern const MethodInfo SCR_SaveData__ctor_m1480_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SCR_SaveData__ctor_m1480/* method */
	, &SCR_SaveData_t382_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 415/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_SaveData::Awake()
extern const MethodInfo SCR_SaveData_Awake_m1481_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&SCR_SaveData_Awake_m1481/* method */
	, &SCR_SaveData_t382_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 416/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_SaveData::Start()
extern const MethodInfo SCR_SaveData_Start_m1482_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&SCR_SaveData_Start_m1482/* method */
	, &SCR_SaveData_t382_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 417/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo SCR_SaveData_t382_SCR_SaveData_GetDataValues_m1483_ParameterInfos[] = 
{
	{"level", 0, 134217826, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_SaveData::GetDataValues(System.Int32)
extern const MethodInfo SCR_SaveData_GetDataValues_m1483_MethodInfo = 
{
	"GetDataValues"/* name */
	, (methodPointerType)&SCR_SaveData_GetDataValues_m1483/* method */
	, &SCR_SaveData_t382_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, SCR_SaveData_t382_SCR_SaveData_GetDataValues_m1483_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 418/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_SaveData::SetDataValues()
extern const MethodInfo SCR_SaveData_SetDataValues_m1484_MethodInfo = 
{
	"SetDataValues"/* name */
	, (methodPointerType)&SCR_SaveData_SetDataValues_m1484/* method */
	, &SCR_SaveData_t382_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 419/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo SCR_SaveData_t382_SCR_SaveData_Save_m1485_ParameterInfos[] = 
{
	{"level", 0, 134217827, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_SaveData::Save(System.Int32)
extern const MethodInfo SCR_SaveData_Save_m1485_MethodInfo = 
{
	"Save"/* name */
	, (methodPointerType)&SCR_SaveData_Save_m1485/* method */
	, &SCR_SaveData_t382_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, SCR_SaveData_t382_SCR_SaveData_Save_m1485_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 420/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_SaveData::Load()
extern const MethodInfo SCR_SaveData_Load_m1486_MethodInfo = 
{
	"Load"/* name */
	, (methodPointerType)&SCR_SaveData_Load_m1486/* method */
	, &SCR_SaveData_t382_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 421/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_SaveData::ResetSave()
extern const MethodInfo SCR_SaveData_ResetSave_m1487_MethodInfo = 
{
	"ResetSave"/* name */
	, (methodPointerType)&SCR_SaveData_ResetSave_m1487/* method */
	, &SCR_SaveData_t382_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 422/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SCR_SaveData_t382_MethodInfos[] =
{
	&SCR_SaveData__ctor_m1480_MethodInfo,
	&SCR_SaveData_Awake_m1481_MethodInfo,
	&SCR_SaveData_Start_m1482_MethodInfo,
	&SCR_SaveData_GetDataValues_m1483_MethodInfo,
	&SCR_SaveData_SetDataValues_m1484_MethodInfo,
	&SCR_SaveData_Save_m1485_MethodInfo,
	&SCR_SaveData_Load_m1486_MethodInfo,
	&SCR_SaveData_ResetSave_m1487_MethodInfo,
	NULL
};
static const Il2CppMethodReference SCR_SaveData_t382_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool SCR_SaveData_t382_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType SCR_SaveData_t382_1_0_0;
struct SCR_SaveData_t382;
const Il2CppTypeDefinitionMetadata SCR_SaveData_t382_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, SCR_SaveData_t382_VTable/* vtableMethods */
	, SCR_SaveData_t382_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 517/* fieldStart */

};
TypeInfo SCR_SaveData_t382_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "SCR_SaveData"/* name */
	, ""/* namespaze */
	, SCR_SaveData_t382_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SCR_SaveData_t382_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SCR_SaveData_t382_0_0_0/* byval_arg */
	, &SCR_SaveData_t382_1_0_0/* this_arg */
	, &SCR_SaveData_t382_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SCR_SaveData_t382)/* instance_size */
	, sizeof (SCR_SaveData_t382)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Data
#include "AssemblyU2DCSharp_Data.h"
// Metadata Definition Data
extern TypeInfo Data_t388_il2cpp_TypeInfo;
// Data
#include "AssemblyU2DCSharp_DataMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void Data::.ctor()
extern const MethodInfo Data__ctor_m1488_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Data__ctor_m1488/* method */
	, &Data_t388_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 423/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Data_t388_MethodInfos[] =
{
	&Data__ctor_m1488_MethodInfo,
	NULL
};
static const Il2CppMethodReference Data_t388_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool Data_t388_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType Data_t388_0_0_0;
extern const Il2CppType Data_t388_1_0_0;
struct Data_t388;
const Il2CppTypeDefinitionMetadata Data_t388_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Data_t388_VTable/* vtableMethods */
	, Data_t388_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 520/* fieldStart */

};
TypeInfo Data_t388_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "Data"/* name */
	, ""/* namespaze */
	, Data_t388_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Data_t388_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Data_t388_0_0_0/* byval_arg */
	, &Data_t388_1_0_0/* this_arg */
	, &Data_t388_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Data_t388)/* instance_size */
	, sizeof (Data_t388)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SCR_SoundManager
#include "AssemblyU2DCSharp_SCR_SoundManager.h"
// Metadata Definition SCR_SoundManager
extern TypeInfo SCR_SoundManager_t335_il2cpp_TypeInfo;
// SCR_SoundManager
#include "AssemblyU2DCSharp_SCR_SoundManagerMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_SoundManager::.ctor()
extern const MethodInfo SCR_SoundManager__ctor_m1489_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SCR_SoundManager__ctor_m1489/* method */
	, &SCR_SoundManager_t335_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 424/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_SoundManager::Awake()
extern const MethodInfo SCR_SoundManager_Awake_m1490_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&SCR_SoundManager_Awake_m1490/* method */
	, &SCR_SoundManager_t335_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 425/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_SoundManager::Start()
extern const MethodInfo SCR_SoundManager_Start_m1491_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&SCR_SoundManager_Start_m1491/* method */
	, &SCR_SoundManager_t335_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 426/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_SoundManager::OnLevelWasLoaded()
extern const MethodInfo SCR_SoundManager_OnLevelWasLoaded_m1492_MethodInfo = 
{
	"OnLevelWasLoaded"/* name */
	, (methodPointerType)&SCR_SoundManager_OnLevelWasLoaded_m1492/* method */
	, &SCR_SoundManager_t335_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 427/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_SoundManager::FillSoundDictionary()
extern const MethodInfo SCR_SoundManager_FillSoundDictionary_m1493_MethodInfo = 
{
	"FillSoundDictionary"/* name */
	, (methodPointerType)&SCR_SoundManager_FillSoundDictionary_m1493/* method */
	, &SCR_SoundManager_t335_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 428/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_SoundManager::LevelInit()
extern const MethodInfo SCR_SoundManager_LevelInit_m1494_MethodInfo = 
{
	"LevelInit"/* name */
	, (methodPointerType)&SCR_SoundManager_LevelInit_m1494/* method */
	, &SCR_SoundManager_t335_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 429/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo SCR_SoundManager_t335_SCR_SoundManager_PlaySound_m1495_ParameterInfos[] = 
{
	{"name", 0, 134217828, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_SoundManager::PlaySound(System.String)
extern const MethodInfo SCR_SoundManager_PlaySound_m1495_MethodInfo = 
{
	"PlaySound"/* name */
	, (methodPointerType)&SCR_SoundManager_PlaySound_m1495/* method */
	, &SCR_SoundManager_t335_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, SCR_SoundManager_t335_SCR_SoundManager_PlaySound_m1495_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 430/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo SCR_SoundManager_t335_SCR_SoundManager_StopSound_m1496_ParameterInfos[] = 
{
	{"name", 0, 134217829, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_SoundManager::StopSound(System.String)
extern const MethodInfo SCR_SoundManager_StopSound_m1496_MethodInfo = 
{
	"StopSound"/* name */
	, (methodPointerType)&SCR_SoundManager_StopSound_m1496/* method */
	, &SCR_SoundManager_t335_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, SCR_SoundManager_t335_SCR_SoundManager_StopSound_m1496_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 431/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_SoundManager::StopAllSounds()
extern const MethodInfo SCR_SoundManager_StopAllSounds_m1497_MethodInfo = 
{
	"StopAllSounds"/* name */
	, (methodPointerType)&SCR_SoundManager_StopAllSounds_m1497/* method */
	, &SCR_SoundManager_t335_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 432/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean SCR_SoundManager::get_SoundOn()
extern const MethodInfo SCR_SoundManager_get_SoundOn_m1498_MethodInfo = 
{
	"get_SoundOn"/* name */
	, (methodPointerType)&SCR_SoundManager_get_SoundOn_m1498/* method */
	, &SCR_SoundManager_t335_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 433/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo SCR_SoundManager_t335_SCR_SoundManager_set_SoundOn_m1499_ParameterInfos[] = 
{
	{"value", 0, 134217830, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_SoundManager::set_SoundOn(System.Boolean)
extern const MethodInfo SCR_SoundManager_set_SoundOn_m1499_MethodInfo = 
{
	"set_SoundOn"/* name */
	, (methodPointerType)&SCR_SoundManager_set_SoundOn_m1499/* method */
	, &SCR_SoundManager_t335_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274/* invoker_method */
	, SCR_SoundManager_t335_SCR_SoundManager_set_SoundOn_m1499_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 434/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SCR_SoundManager_t335_MethodInfos[] =
{
	&SCR_SoundManager__ctor_m1489_MethodInfo,
	&SCR_SoundManager_Awake_m1490_MethodInfo,
	&SCR_SoundManager_Start_m1491_MethodInfo,
	&SCR_SoundManager_OnLevelWasLoaded_m1492_MethodInfo,
	&SCR_SoundManager_FillSoundDictionary_m1493_MethodInfo,
	&SCR_SoundManager_LevelInit_m1494_MethodInfo,
	&SCR_SoundManager_PlaySound_m1495_MethodInfo,
	&SCR_SoundManager_StopSound_m1496_MethodInfo,
	&SCR_SoundManager_StopAllSounds_m1497_MethodInfo,
	&SCR_SoundManager_get_SoundOn_m1498_MethodInfo,
	&SCR_SoundManager_set_SoundOn_m1499_MethodInfo,
	NULL
};
extern const MethodInfo SCR_SoundManager_get_SoundOn_m1498_MethodInfo;
extern const MethodInfo SCR_SoundManager_set_SoundOn_m1499_MethodInfo;
static const PropertyInfo SCR_SoundManager_t335____SoundOn_PropertyInfo = 
{
	&SCR_SoundManager_t335_il2cpp_TypeInfo/* parent */
	, "SoundOn"/* name */
	, &SCR_SoundManager_get_SoundOn_m1498_MethodInfo/* get */
	, &SCR_SoundManager_set_SoundOn_m1499_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* SCR_SoundManager_t335_PropertyInfos[] =
{
	&SCR_SoundManager_t335____SoundOn_PropertyInfo,
	NULL
};
static const Il2CppMethodReference SCR_SoundManager_t335_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool SCR_SoundManager_t335_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType SCR_SoundManager_t335_1_0_0;
struct SCR_SoundManager_t335;
const Il2CppTypeDefinitionMetadata SCR_SoundManager_t335_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, SCR_SoundManager_t335_VTable/* vtableMethods */
	, SCR_SoundManager_t335_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 521/* fieldStart */

};
TypeInfo SCR_SoundManager_t335_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "SCR_SoundManager"/* name */
	, ""/* namespaze */
	, SCR_SoundManager_t335_MethodInfos/* methods */
	, SCR_SoundManager_t335_PropertyInfos/* properties */
	, NULL/* events */
	, &SCR_SoundManager_t335_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SCR_SoundManager_t335_0_0_0/* byval_arg */
	, &SCR_SoundManager_t335_1_0_0/* this_arg */
	, &SCR_SoundManager_t335_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SCR_SoundManager_t335)/* instance_size */
	, sizeof (SCR_SoundManager_t335)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SCR_TimeManager
#include "AssemblyU2DCSharp_SCR_TimeManager.h"
// Metadata Definition SCR_TimeManager
extern TypeInfo SCR_TimeManager_t383_il2cpp_TypeInfo;
// SCR_TimeManager
#include "AssemblyU2DCSharp_SCR_TimeManagerMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_TimeManager::.ctor()
extern const MethodInfo SCR_TimeManager__ctor_m1500_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SCR_TimeManager__ctor_m1500/* method */
	, &SCR_TimeManager_t383_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 435/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_TimeManager::Awake()
extern const MethodInfo SCR_TimeManager_Awake_m1501_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&SCR_TimeManager_Awake_m1501/* method */
	, &SCR_TimeManager_t383_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 436/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_TimeManager::Start()
extern const MethodInfo SCR_TimeManager_Start_m1502_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&SCR_TimeManager_Start_m1502/* method */
	, &SCR_TimeManager_t383_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 437/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_TimeManager::OnLevelWasLoaded()
extern const MethodInfo SCR_TimeManager_OnLevelWasLoaded_m1503_MethodInfo = 
{
	"OnLevelWasLoaded"/* name */
	, (methodPointerType)&SCR_TimeManager_OnLevelWasLoaded_m1503/* method */
	, &SCR_TimeManager_t383_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 438/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_TimeManager::Update()
extern const MethodInfo SCR_TimeManager_Update_m1504_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&SCR_TimeManager_Update_m1504/* method */
	, &SCR_TimeManager_t383_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 439/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo SCR_TimeManager_t383_SCR_TimeManager_SetTimer_m1505_ParameterInfos[] = 
{
	{"turnOn", 0, 134217831, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_TimeManager::SetTimer(System.Boolean)
extern const MethodInfo SCR_TimeManager_SetTimer_m1505_MethodInfo = 
{
	"SetTimer"/* name */
	, (methodPointerType)&SCR_TimeManager_SetTimer_m1505/* method */
	, &SCR_TimeManager_t383_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274/* invoker_method */
	, SCR_TimeManager_t383_SCR_TimeManager_SetTimer_m1505_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 440/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_TimeManager::Pause()
extern const MethodInfo SCR_TimeManager_Pause_m1506_MethodInfo = 
{
	"Pause"/* name */
	, (methodPointerType)&SCR_TimeManager_Pause_m1506/* method */
	, &SCR_TimeManager_t383_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 441/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean SCR_TimeManager::get_IsPaused()
extern const MethodInfo SCR_TimeManager_get_IsPaused_m1507_MethodInfo = 
{
	"get_IsPaused"/* name */
	, (methodPointerType)&SCR_TimeManager_get_IsPaused_m1507/* method */
	, &SCR_TimeManager_t383_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 442/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean SCR_TimeManager::get_IsCounting()
extern const MethodInfo SCR_TimeManager_get_IsCounting_m1508_MethodInfo = 
{
	"get_IsCounting"/* name */
	, (methodPointerType)&SCR_TimeManager_get_IsCounting_m1508/* method */
	, &SCR_TimeManager_t383_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 443/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single SCR_TimeManager::get_TimeSinceLevelStart()
extern const MethodInfo SCR_TimeManager_get_TimeSinceLevelStart_m1509_MethodInfo = 
{
	"get_TimeSinceLevelStart"/* name */
	, (methodPointerType)&SCR_TimeManager_get_TimeSinceLevelStart_m1509/* method */
	, &SCR_TimeManager_t383_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 444/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SCR_TimeManager_t383_MethodInfos[] =
{
	&SCR_TimeManager__ctor_m1500_MethodInfo,
	&SCR_TimeManager_Awake_m1501_MethodInfo,
	&SCR_TimeManager_Start_m1502_MethodInfo,
	&SCR_TimeManager_OnLevelWasLoaded_m1503_MethodInfo,
	&SCR_TimeManager_Update_m1504_MethodInfo,
	&SCR_TimeManager_SetTimer_m1505_MethodInfo,
	&SCR_TimeManager_Pause_m1506_MethodInfo,
	&SCR_TimeManager_get_IsPaused_m1507_MethodInfo,
	&SCR_TimeManager_get_IsCounting_m1508_MethodInfo,
	&SCR_TimeManager_get_TimeSinceLevelStart_m1509_MethodInfo,
	NULL
};
extern const MethodInfo SCR_TimeManager_get_IsPaused_m1507_MethodInfo;
static const PropertyInfo SCR_TimeManager_t383____IsPaused_PropertyInfo = 
{
	&SCR_TimeManager_t383_il2cpp_TypeInfo/* parent */
	, "IsPaused"/* name */
	, &SCR_TimeManager_get_IsPaused_m1507_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo SCR_TimeManager_get_IsCounting_m1508_MethodInfo;
static const PropertyInfo SCR_TimeManager_t383____IsCounting_PropertyInfo = 
{
	&SCR_TimeManager_t383_il2cpp_TypeInfo/* parent */
	, "IsCounting"/* name */
	, &SCR_TimeManager_get_IsCounting_m1508_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo SCR_TimeManager_get_TimeSinceLevelStart_m1509_MethodInfo;
static const PropertyInfo SCR_TimeManager_t383____TimeSinceLevelStart_PropertyInfo = 
{
	&SCR_TimeManager_t383_il2cpp_TypeInfo/* parent */
	, "TimeSinceLevelStart"/* name */
	, &SCR_TimeManager_get_TimeSinceLevelStart_m1509_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* SCR_TimeManager_t383_PropertyInfos[] =
{
	&SCR_TimeManager_t383____IsPaused_PropertyInfo,
	&SCR_TimeManager_t383____IsCounting_PropertyInfo,
	&SCR_TimeManager_t383____TimeSinceLevelStart_PropertyInfo,
	NULL
};
static const Il2CppMethodReference SCR_TimeManager_t383_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool SCR_TimeManager_t383_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType SCR_TimeManager_t383_1_0_0;
struct SCR_TimeManager_t383;
const Il2CppTypeDefinitionMetadata SCR_TimeManager_t383_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, SCR_TimeManager_t383_VTable/* vtableMethods */
	, SCR_TimeManager_t383_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 523/* fieldStart */

};
TypeInfo SCR_TimeManager_t383_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "SCR_TimeManager"/* name */
	, ""/* namespaze */
	, SCR_TimeManager_t383_MethodInfos/* methods */
	, SCR_TimeManager_t383_PropertyInfos/* properties */
	, NULL/* events */
	, &SCR_TimeManager_t383_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SCR_TimeManager_t383_0_0_0/* byval_arg */
	, &SCR_TimeManager_t383_1_0_0/* this_arg */
	, &SCR_TimeManager_t383_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SCR_TimeManager_t383)/* instance_size */
	, sizeof (SCR_TimeManager_t383)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 3/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SCR_Bird/<Landing>c__IteratorB
#include "AssemblyU2DCSharp_SCR_Bird_U3CLandingU3Ec__IteratorB.h"
// Metadata Definition SCR_Bird/<Landing>c__IteratorB
extern TypeInfo U3CLandingU3Ec__IteratorB_t391_il2cpp_TypeInfo;
// SCR_Bird/<Landing>c__IteratorB
#include "AssemblyU2DCSharp_SCR_Bird_U3CLandingU3Ec__IteratorBMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Bird/<Landing>c__IteratorB::.ctor()
extern const MethodInfo U3CLandingU3Ec__IteratorB__ctor_m1510_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CLandingU3Ec__IteratorB__ctor_m1510/* method */
	, &U3CLandingU3Ec__IteratorB_t391_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 460/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object SCR_Bird/<Landing>c__IteratorB::System.Collections.Generic.IEnumerator<object>.get_Current()
extern const MethodInfo U3CLandingU3Ec__IteratorB_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1511_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<object>.get_Current"/* name */
	, (methodPointerType)&U3CLandingU3Ec__IteratorB_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1511/* method */
	, &U3CLandingU3Ec__IteratorB_t391_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 77/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 461/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object SCR_Bird/<Landing>c__IteratorB::System.Collections.IEnumerator.get_Current()
extern const MethodInfo U3CLandingU3Ec__IteratorB_System_Collections_IEnumerator_get_Current_m1512_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&U3CLandingU3Ec__IteratorB_System_Collections_IEnumerator_get_Current_m1512/* method */
	, &U3CLandingU3Ec__IteratorB_t391_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 78/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 462/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean SCR_Bird/<Landing>c__IteratorB::MoveNext()
extern const MethodInfo U3CLandingU3Ec__IteratorB_MoveNext_m1513_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&U3CLandingU3Ec__IteratorB_MoveNext_m1513/* method */
	, &U3CLandingU3Ec__IteratorB_t391_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 463/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Bird/<Landing>c__IteratorB::Dispose()
extern const MethodInfo U3CLandingU3Ec__IteratorB_Dispose_m1514_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&U3CLandingU3Ec__IteratorB_Dispose_m1514/* method */
	, &U3CLandingU3Ec__IteratorB_t391_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 79/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 464/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Bird/<Landing>c__IteratorB::Reset()
extern const MethodInfo U3CLandingU3Ec__IteratorB_Reset_m1515_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&U3CLandingU3Ec__IteratorB_Reset_m1515/* method */
	, &U3CLandingU3Ec__IteratorB_t391_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 80/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 465/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CLandingU3Ec__IteratorB_t391_MethodInfos[] =
{
	&U3CLandingU3Ec__IteratorB__ctor_m1510_MethodInfo,
	&U3CLandingU3Ec__IteratorB_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1511_MethodInfo,
	&U3CLandingU3Ec__IteratorB_System_Collections_IEnumerator_get_Current_m1512_MethodInfo,
	&U3CLandingU3Ec__IteratorB_MoveNext_m1513_MethodInfo,
	&U3CLandingU3Ec__IteratorB_Dispose_m1514_MethodInfo,
	&U3CLandingU3Ec__IteratorB_Reset_m1515_MethodInfo,
	NULL
};
extern const MethodInfo U3CLandingU3Ec__IteratorB_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1511_MethodInfo;
static const PropertyInfo U3CLandingU3Ec__IteratorB_t391____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo = 
{
	&U3CLandingU3Ec__IteratorB_t391_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<object>.Current"/* name */
	, &U3CLandingU3Ec__IteratorB_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1511_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo U3CLandingU3Ec__IteratorB_System_Collections_IEnumerator_get_Current_m1512_MethodInfo;
static const PropertyInfo U3CLandingU3Ec__IteratorB_t391____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CLandingU3Ec__IteratorB_t391_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CLandingU3Ec__IteratorB_System_Collections_IEnumerator_get_Current_m1512_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* U3CLandingU3Ec__IteratorB_t391_PropertyInfos[] =
{
	&U3CLandingU3Ec__IteratorB_t391____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo,
	&U3CLandingU3Ec__IteratorB_t391____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern const MethodInfo U3CLandingU3Ec__IteratorB_Dispose_m1514_MethodInfo;
extern const MethodInfo U3CLandingU3Ec__IteratorB_MoveNext_m1513_MethodInfo;
extern const MethodInfo U3CLandingU3Ec__IteratorB_Reset_m1515_MethodInfo;
static const Il2CppMethodReference U3CLandingU3Ec__IteratorB_t391_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&U3CLandingU3Ec__IteratorB_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1511_MethodInfo,
	&U3CLandingU3Ec__IteratorB_Dispose_m1514_MethodInfo,
	&U3CLandingU3Ec__IteratorB_System_Collections_IEnumerator_get_Current_m1512_MethodInfo,
	&U3CLandingU3Ec__IteratorB_MoveNext_m1513_MethodInfo,
	&U3CLandingU3Ec__IteratorB_Reset_m1515_MethodInfo,
};
static bool U3CLandingU3Ec__IteratorB_t391_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* U3CLandingU3Ec__IteratorB_t391_InterfacesTypeInfos[] = 
{
	&IEnumerator_1_t284_0_0_0,
	&IDisposable_t233_0_0_0,
	&IEnumerator_t217_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CLandingU3Ec__IteratorB_t391_InterfacesOffsets[] = 
{
	{ &IEnumerator_1_t284_0_0_0, 4},
	{ &IDisposable_t233_0_0_0, 5},
	{ &IEnumerator_t217_0_0_0, 6},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType U3CLandingU3Ec__IteratorB_t391_0_0_0;
extern const Il2CppType U3CLandingU3Ec__IteratorB_t391_1_0_0;
extern TypeInfo SCR_Bird_t390_il2cpp_TypeInfo;
extern const Il2CppType SCR_Bird_t390_0_0_0;
struct U3CLandingU3Ec__IteratorB_t391;
const Il2CppTypeDefinitionMetadata U3CLandingU3Ec__IteratorB_t391_DefinitionMetadata = 
{
	&SCR_Bird_t390_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CLandingU3Ec__IteratorB_t391_InterfacesTypeInfos/* implementedInterfaces */
	, U3CLandingU3Ec__IteratorB_t391_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CLandingU3Ec__IteratorB_t391_VTable/* vtableMethods */
	, U3CLandingU3Ec__IteratorB_t391_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 526/* fieldStart */

};
TypeInfo U3CLandingU3Ec__IteratorB_t391_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<Landing>c__IteratorB"/* name */
	, ""/* namespaze */
	, U3CLandingU3Ec__IteratorB_t391_MethodInfos/* methods */
	, U3CLandingU3Ec__IteratorB_t391_PropertyInfos/* properties */
	, NULL/* events */
	, &U3CLandingU3Ec__IteratorB_t391_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 76/* custom_attributes_cache */
	, &U3CLandingU3Ec__IteratorB_t391_0_0_0/* byval_arg */
	, &U3CLandingU3Ec__IteratorB_t391_1_0_0/* this_arg */
	, &U3CLandingU3Ec__IteratorB_t391_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CLandingU3Ec__IteratorB_t391)/* instance_size */
	, sizeof (U3CLandingU3Ec__IteratorB_t391)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// SCR_Bird
#include "AssemblyU2DCSharp_SCR_Bird.h"
// Metadata Definition SCR_Bird
// SCR_Bird
#include "AssemblyU2DCSharp_SCR_BirdMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Bird::.ctor()
extern const MethodInfo SCR_Bird__ctor_m1516_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SCR_Bird__ctor_m1516/* method */
	, &SCR_Bird_t390_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 445/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Bird::Start()
extern const MethodInfo SCR_Bird_Start_m1517_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&SCR_Bird_Start_m1517/* method */
	, &SCR_Bird_t390_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 446/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Bird::Update()
extern const MethodInfo SCR_Bird_Update_m1518_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&SCR_Bird_Update_m1518/* method */
	, &SCR_Bird_t390_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 447/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Bird::SetToIdlePosition()
extern const MethodInfo SCR_Bird_SetToIdlePosition_m1519_MethodInfo = 
{
	"SetToIdlePosition"/* name */
	, (methodPointerType)&SCR_Bird_SetToIdlePosition_m1519/* method */
	, &SCR_Bird_t390_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 448/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Bird::Fly()
extern const MethodInfo SCR_Bird_Fly_m1520_MethodInfo = 
{
	"Fly"/* name */
	, (methodPointerType)&SCR_Bird_Fly_m1520/* method */
	, &SCR_Bird_t390_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 449/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.IEnumerator SCR_Bird::Landing()
extern const MethodInfo SCR_Bird_Landing_m1521_MethodInfo = 
{
	"Landing"/* name */
	, (methodPointerType)&SCR_Bird_Landing_m1521/* method */
	, &SCR_Bird_t390_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t217_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 75/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 450/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Bird::AttackStart()
extern const MethodInfo SCR_Bird_AttackStart_m1522_MethodInfo = 
{
	"AttackStart"/* name */
	, (methodPointerType)&SCR_Bird_AttackStart_m1522/* method */
	, &SCR_Bird_t390_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 451/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Bird::Attack()
extern const MethodInfo SCR_Bird_Attack_m1523_MethodInfo = 
{
	"Attack"/* name */
	, (methodPointerType)&SCR_Bird_Attack_m1523/* method */
	, &SCR_Bird_t390_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 452/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Bird::GetTargetPoint()
extern const MethodInfo SCR_Bird_GetTargetPoint_m1524_MethodInfo = 
{
	"GetTargetPoint"/* name */
	, (methodPointerType)&SCR_Bird_GetTargetPoint_m1524/* method */
	, &SCR_Bird_t390_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 453/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Bird::GoIdle()
extern const MethodInfo SCR_Bird_GoIdle_m1525_MethodInfo = 
{
	"GoIdle"/* name */
	, (methodPointerType)&SCR_Bird_GoIdle_m1525/* method */
	, &SCR_Bird_t390_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 454/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Bird::CanMove()
extern const MethodInfo SCR_Bird_CanMove_m1526_MethodInfo = 
{
	"CanMove"/* name */
	, (methodPointerType)&SCR_Bird_CanMove_m1526/* method */
	, &SCR_Bird_t390_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 455/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Bird::BlobShadow()
extern const MethodInfo SCR_Bird_BlobShadow_m1527_MethodInfo = 
{
	"BlobShadow"/* name */
	, (methodPointerType)&SCR_Bird_BlobShadow_m1527/* method */
	, &SCR_Bird_t390_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 456/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Bird::EndAttack()
extern const MethodInfo SCR_Bird_EndAttack_m1528_MethodInfo = 
{
	"EndAttack"/* name */
	, (methodPointerType)&SCR_Bird_EndAttack_m1528/* method */
	, &SCR_Bird_t390_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 457/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Bird::Pool()
extern const MethodInfo SCR_Bird_Pool_m1529_MethodInfo = 
{
	"Pool"/* name */
	, (methodPointerType)&SCR_Bird_Pool_m1529/* method */
	, &SCR_Bird_t390_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 458/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Bird::AddAttack()
extern const MethodInfo SCR_Bird_AddAttack_m1530_MethodInfo = 
{
	"AddAttack"/* name */
	, (methodPointerType)&SCR_Bird_AddAttack_m1530/* method */
	, &SCR_Bird_t390_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 459/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SCR_Bird_t390_MethodInfos[] =
{
	&SCR_Bird__ctor_m1516_MethodInfo,
	&SCR_Bird_Start_m1517_MethodInfo,
	&SCR_Bird_Update_m1518_MethodInfo,
	&SCR_Bird_SetToIdlePosition_m1519_MethodInfo,
	&SCR_Bird_Fly_m1520_MethodInfo,
	&SCR_Bird_Landing_m1521_MethodInfo,
	&SCR_Bird_AttackStart_m1522_MethodInfo,
	&SCR_Bird_Attack_m1523_MethodInfo,
	&SCR_Bird_GetTargetPoint_m1524_MethodInfo,
	&SCR_Bird_GoIdle_m1525_MethodInfo,
	&SCR_Bird_CanMove_m1526_MethodInfo,
	&SCR_Bird_BlobShadow_m1527_MethodInfo,
	&SCR_Bird_EndAttack_m1528_MethodInfo,
	&SCR_Bird_Pool_m1529_MethodInfo,
	&SCR_Bird_AddAttack_m1530_MethodInfo,
	NULL
};
static const Il2CppType* SCR_Bird_t390_il2cpp_TypeInfo__nestedTypes[1] =
{
	&U3CLandingU3Ec__IteratorB_t391_0_0_0,
};
static const Il2CppMethodReference SCR_Bird_t390_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool SCR_Bird_t390_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType SCR_Bird_t390_1_0_0;
struct SCR_Bird_t390;
const Il2CppTypeDefinitionMetadata SCR_Bird_t390_DefinitionMetadata = 
{
	NULL/* declaringType */
	, SCR_Bird_t390_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, SCR_Bird_t390_VTable/* vtableMethods */
	, SCR_Bird_t390_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 529/* fieldStart */

};
TypeInfo SCR_Bird_t390_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "SCR_Bird"/* name */
	, ""/* namespaze */
	, SCR_Bird_t390_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SCR_Bird_t390_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SCR_Bird_t390_0_0_0/* byval_arg */
	, &SCR_Bird_t390_1_0_0/* this_arg */
	, &SCR_Bird_t390_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SCR_Bird_t390)/* instance_size */
	, sizeof (SCR_Bird_t390)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 15/* method_count */
	, 0/* property_count */
	, 14/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SCR_FallOutOfScreen
#include "AssemblyU2DCSharp_SCR_FallOutOfScreen.h"
// Metadata Definition SCR_FallOutOfScreen
extern TypeInfo SCR_FallOutOfScreen_t394_il2cpp_TypeInfo;
// SCR_FallOutOfScreen
#include "AssemblyU2DCSharp_SCR_FallOutOfScreenMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_FallOutOfScreen::.ctor()
extern const MethodInfo SCR_FallOutOfScreen__ctor_m1531_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SCR_FallOutOfScreen__ctor_m1531/* method */
	, &SCR_FallOutOfScreen_t394_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 466/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_FallOutOfScreen::OnBecameVisible()
extern const MethodInfo SCR_FallOutOfScreen_OnBecameVisible_m1532_MethodInfo = 
{
	"OnBecameVisible"/* name */
	, (methodPointerType)&SCR_FallOutOfScreen_OnBecameVisible_m1532/* method */
	, &SCR_FallOutOfScreen_t394_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 467/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Collider_t138_0_0_0;
static const ParameterInfo SCR_FallOutOfScreen_t394_SCR_FallOutOfScreen_OnTriggerEnter_m1533_ParameterInfos[] = 
{
	{"other", 0, 134217832, 0, &Collider_t138_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_FallOutOfScreen::OnTriggerEnter(UnityEngine.Collider)
extern const MethodInfo SCR_FallOutOfScreen_OnTriggerEnter_m1533_MethodInfo = 
{
	"OnTriggerEnter"/* name */
	, (methodPointerType)&SCR_FallOutOfScreen_OnTriggerEnter_m1533/* method */
	, &SCR_FallOutOfScreen_t394_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, SCR_FallOutOfScreen_t394_SCR_FallOutOfScreen_OnTriggerEnter_m1533_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 468/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_FallOutOfScreen::CallAvatarRespawn()
extern const MethodInfo SCR_FallOutOfScreen_CallAvatarRespawn_m1534_MethodInfo = 
{
	"CallAvatarRespawn"/* name */
	, (methodPointerType)&SCR_FallOutOfScreen_CallAvatarRespawn_m1534/* method */
	, &SCR_FallOutOfScreen_t394_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 469/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SCR_FallOutOfScreen_t394_MethodInfos[] =
{
	&SCR_FallOutOfScreen__ctor_m1531_MethodInfo,
	&SCR_FallOutOfScreen_OnBecameVisible_m1532_MethodInfo,
	&SCR_FallOutOfScreen_OnTriggerEnter_m1533_MethodInfo,
	&SCR_FallOutOfScreen_CallAvatarRespawn_m1534_MethodInfo,
	NULL
};
static const Il2CppMethodReference SCR_FallOutOfScreen_t394_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool SCR_FallOutOfScreen_t394_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType SCR_FallOutOfScreen_t394_0_0_0;
extern const Il2CppType SCR_FallOutOfScreen_t394_1_0_0;
struct SCR_FallOutOfScreen_t394;
const Il2CppTypeDefinitionMetadata SCR_FallOutOfScreen_t394_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, SCR_FallOutOfScreen_t394_VTable/* vtableMethods */
	, SCR_FallOutOfScreen_t394_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo SCR_FallOutOfScreen_t394_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "SCR_FallOutOfScreen"/* name */
	, ""/* namespaze */
	, SCR_FallOutOfScreen_t394_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SCR_FallOutOfScreen_t394_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SCR_FallOutOfScreen_t394_0_0_0/* byval_arg */
	, &SCR_FallOutOfScreen_t394_1_0_0/* this_arg */
	, &SCR_FallOutOfScreen_t394_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SCR_FallOutOfScreen_t394)/* instance_size */
	, sizeof (SCR_FallOutOfScreen_t394)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SCR_Goutte
#include "AssemblyU2DCSharp_SCR_Goutte.h"
// Metadata Definition SCR_Goutte
extern TypeInfo SCR_Goutte_t347_il2cpp_TypeInfo;
// SCR_Goutte
#include "AssemblyU2DCSharp_SCR_GoutteMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Goutte::.ctor()
extern const MethodInfo SCR_Goutte__ctor_m1535_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SCR_Goutte__ctor_m1535/* method */
	, &SCR_Goutte_t347_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 470/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Goutte::TriggerEnterEffect()
extern const MethodInfo SCR_Goutte_TriggerEnterEffect_m1536_MethodInfo = 
{
	"TriggerEnterEffect"/* name */
	, (methodPointerType)&SCR_Goutte_TriggerEnterEffect_m1536/* method */
	, &SCR_Goutte_t347_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 471/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Goutte::TriggerExitEffet()
extern const MethodInfo SCR_Goutte_TriggerExitEffet_m1537_MethodInfo = 
{
	"TriggerExitEffet"/* name */
	, (methodPointerType)&SCR_Goutte_TriggerExitEffet_m1537/* method */
	, &SCR_Goutte_t347_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 472/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SCR_Goutte_t347_MethodInfos[] =
{
	&SCR_Goutte__ctor_m1535_MethodInfo,
	&SCR_Goutte_TriggerEnterEffect_m1536_MethodInfo,
	&SCR_Goutte_TriggerExitEffet_m1537_MethodInfo,
	NULL
};
extern const MethodInfo SCR_Goutte_TriggerEnterEffect_m1536_MethodInfo;
extern const MethodInfo SCR_Goutte_TriggerExitEffet_m1537_MethodInfo;
static const Il2CppMethodReference SCR_Goutte_t347_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	&SCR_Goutte_TriggerEnterEffect_m1536_MethodInfo,
	&SCR_Goutte_TriggerExitEffet_m1537_MethodInfo,
};
static bool SCR_Goutte_t347_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType SCR_Goutte_t347_1_0_0;
struct SCR_Goutte_t347;
const Il2CppTypeDefinitionMetadata SCR_Goutte_t347_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &SCR_SlowAvatar_t337_0_0_0/* parent */
	, SCR_Goutte_t347_VTable/* vtableMethods */
	, SCR_Goutte_t347_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo SCR_Goutte_t347_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "SCR_Goutte"/* name */
	, ""/* namespaze */
	, SCR_Goutte_t347_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SCR_Goutte_t347_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SCR_Goutte_t347_0_0_0/* byval_arg */
	, &SCR_Goutte_t347_1_0_0/* this_arg */
	, &SCR_Goutte_t347_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SCR_Goutte_t347)/* instance_size */
	, sizeof (SCR_Goutte_t347)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SCR_Hurt
#include "AssemblyU2DCSharp_SCR_Hurt.h"
// Metadata Definition SCR_Hurt
extern TypeInfo SCR_Hurt_t393_il2cpp_TypeInfo;
// SCR_Hurt
#include "AssemblyU2DCSharp_SCR_HurtMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Hurt::.ctor()
extern const MethodInfo SCR_Hurt__ctor_m1538_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SCR_Hurt__ctor_m1538/* method */
	, &SCR_Hurt_t393_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 473/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Collider_t138_0_0_0;
static const ParameterInfo SCR_Hurt_t393_SCR_Hurt_OnTriggerEnter_m1539_ParameterInfos[] = 
{
	{"other", 0, 134217833, 0, &Collider_t138_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Hurt::OnTriggerEnter(UnityEngine.Collider)
extern const MethodInfo SCR_Hurt_OnTriggerEnter_m1539_MethodInfo = 
{
	"OnTriggerEnter"/* name */
	, (methodPointerType)&SCR_Hurt_OnTriggerEnter_m1539/* method */
	, &SCR_Hurt_t393_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, SCR_Hurt_t393_SCR_Hurt_OnTriggerEnter_m1539_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 474/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Hurt::AvatarWasHurt()
extern const MethodInfo SCR_Hurt_AvatarWasHurt_m1540_MethodInfo = 
{
	"AvatarWasHurt"/* name */
	, (methodPointerType)&SCR_Hurt_AvatarWasHurt_m1540/* method */
	, &SCR_Hurt_t393_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 475/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SCR_Hurt_t393_MethodInfos[] =
{
	&SCR_Hurt__ctor_m1538_MethodInfo,
	&SCR_Hurt_OnTriggerEnter_m1539_MethodInfo,
	&SCR_Hurt_AvatarWasHurt_m1540_MethodInfo,
	NULL
};
static const Il2CppMethodReference SCR_Hurt_t393_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool SCR_Hurt_t393_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType SCR_Hurt_t393_0_0_0;
extern const Il2CppType SCR_Hurt_t393_1_0_0;
struct SCR_Hurt_t393;
const Il2CppTypeDefinitionMetadata SCR_Hurt_t393_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, SCR_Hurt_t393_VTable/* vtableMethods */
	, SCR_Hurt_t393_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo SCR_Hurt_t393_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "SCR_Hurt"/* name */
	, ""/* namespaze */
	, SCR_Hurt_t393_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SCR_Hurt_t393_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SCR_Hurt_t393_0_0_0/* byval_arg */
	, &SCR_Hurt_t393_1_0_0/* this_arg */
	, &SCR_Hurt_t393_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SCR_Hurt_t393)/* instance_size */
	, sizeof (SCR_Hurt_t393)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SCR_Waypoint
#include "AssemblyU2DCSharp_SCR_Waypoint.h"
// Metadata Definition SCR_Waypoint
extern TypeInfo SCR_Waypoint_t358_il2cpp_TypeInfo;
// SCR_Waypoint
#include "AssemblyU2DCSharp_SCR_WaypointMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Waypoint::.ctor()
extern const MethodInfo SCR_Waypoint__ctor_m1541_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SCR_Waypoint__ctor_m1541/* method */
	, &SCR_Waypoint_t358_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 476/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SCR_Waypoint_t358_MethodInfos[] =
{
	&SCR_Waypoint__ctor_m1541_MethodInfo,
	NULL
};
static const Il2CppMethodReference SCR_Waypoint_t358_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool SCR_Waypoint_t358_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType SCR_Waypoint_t358_1_0_0;
struct SCR_Waypoint_t358;
const Il2CppTypeDefinitionMetadata SCR_Waypoint_t358_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, SCR_Waypoint_t358_VTable/* vtableMethods */
	, SCR_Waypoint_t358_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 543/* fieldStart */

};
TypeInfo SCR_Waypoint_t358_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "SCR_Waypoint"/* name */
	, ""/* namespaze */
	, SCR_Waypoint_t358_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SCR_Waypoint_t358_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SCR_Waypoint_t358_0_0_0/* byval_arg */
	, &SCR_Waypoint_t358_1_0_0/* this_arg */
	, &SCR_Waypoint_t358_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SCR_Waypoint_t358)/* instance_size */
	, sizeof (SCR_Waypoint_t358)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Scr_ShadowTarget
#include "AssemblyU2DCSharp_Scr_ShadowTarget.h"
// Metadata Definition Scr_ShadowTarget
extern TypeInfo Scr_ShadowTarget_t392_il2cpp_TypeInfo;
// Scr_ShadowTarget
#include "AssemblyU2DCSharp_Scr_ShadowTargetMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void Scr_ShadowTarget::.ctor()
extern const MethodInfo Scr_ShadowTarget__ctor_m1542_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Scr_ShadowTarget__ctor_m1542/* method */
	, &Scr_ShadowTarget_t392_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 477/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void Scr_ShadowTarget::Start()
extern const MethodInfo Scr_ShadowTarget_Start_m1543_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&Scr_ShadowTarget_Start_m1543/* method */
	, &Scr_ShadowTarget_t392_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 478/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void Scr_ShadowTarget::Update()
extern const MethodInfo Scr_ShadowTarget_Update_m1544_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&Scr_ShadowTarget_Update_m1544/* method */
	, &Scr_ShadowTarget_t392_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 479/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Scr_ShadowTarget_t392_MethodInfos[] =
{
	&Scr_ShadowTarget__ctor_m1542_MethodInfo,
	&Scr_ShadowTarget_Start_m1543_MethodInfo,
	&Scr_ShadowTarget_Update_m1544_MethodInfo,
	NULL
};
static const Il2CppMethodReference Scr_ShadowTarget_t392_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool Scr_ShadowTarget_t392_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType Scr_ShadowTarget_t392_0_0_0;
extern const Il2CppType Scr_ShadowTarget_t392_1_0_0;
struct Scr_ShadowTarget_t392;
const Il2CppTypeDefinitionMetadata Scr_ShadowTarget_t392_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, Scr_ShadowTarget_t392_VTable/* vtableMethods */
	, Scr_ShadowTarget_t392_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 544/* fieldStart */

};
TypeInfo Scr_ShadowTarget_t392_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "Scr_ShadowTarget"/* name */
	, ""/* namespaze */
	, Scr_ShadowTarget_t392_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Scr_ShadowTarget_t392_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Scr_ShadowTarget_t392_0_0_0/* byval_arg */
	, &Scr_ShadowTarget_t392_1_0_0/* this_arg */
	, &Scr_ShadowTarget_t392_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Scr_ShadowTarget_t392)/* instance_size */
	, sizeof (Scr_ShadowTarget_t392)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SCR_Spider/<ChaseAvatar>c__IteratorC
#include "AssemblyU2DCSharp_SCR_Spider_U3CChaseAvatarU3Ec__IteratorC.h"
// Metadata Definition SCR_Spider/<ChaseAvatar>c__IteratorC
extern TypeInfo U3CChaseAvatarU3Ec__IteratorC_t396_il2cpp_TypeInfo;
// SCR_Spider/<ChaseAvatar>c__IteratorC
#include "AssemblyU2DCSharp_SCR_Spider_U3CChaseAvatarU3Ec__IteratorCMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Spider/<ChaseAvatar>c__IteratorC::.ctor()
extern const MethodInfo U3CChaseAvatarU3Ec__IteratorC__ctor_m1545_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CChaseAvatarU3Ec__IteratorC__ctor_m1545/* method */
	, &U3CChaseAvatarU3Ec__IteratorC_t396_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 489/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object SCR_Spider/<ChaseAvatar>c__IteratorC::System.Collections.Generic.IEnumerator<object>.get_Current()
extern const MethodInfo U3CChaseAvatarU3Ec__IteratorC_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1546_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<object>.get_Current"/* name */
	, (methodPointerType)&U3CChaseAvatarU3Ec__IteratorC_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1546/* method */
	, &U3CChaseAvatarU3Ec__IteratorC_t396_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 83/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 490/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object SCR_Spider/<ChaseAvatar>c__IteratorC::System.Collections.IEnumerator.get_Current()
extern const MethodInfo U3CChaseAvatarU3Ec__IteratorC_System_Collections_IEnumerator_get_Current_m1547_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&U3CChaseAvatarU3Ec__IteratorC_System_Collections_IEnumerator_get_Current_m1547/* method */
	, &U3CChaseAvatarU3Ec__IteratorC_t396_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 84/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 491/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean SCR_Spider/<ChaseAvatar>c__IteratorC::MoveNext()
extern const MethodInfo U3CChaseAvatarU3Ec__IteratorC_MoveNext_m1548_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&U3CChaseAvatarU3Ec__IteratorC_MoveNext_m1548/* method */
	, &U3CChaseAvatarU3Ec__IteratorC_t396_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 492/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Spider/<ChaseAvatar>c__IteratorC::Dispose()
extern const MethodInfo U3CChaseAvatarU3Ec__IteratorC_Dispose_m1549_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&U3CChaseAvatarU3Ec__IteratorC_Dispose_m1549/* method */
	, &U3CChaseAvatarU3Ec__IteratorC_t396_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 85/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 493/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Spider/<ChaseAvatar>c__IteratorC::Reset()
extern const MethodInfo U3CChaseAvatarU3Ec__IteratorC_Reset_m1550_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&U3CChaseAvatarU3Ec__IteratorC_Reset_m1550/* method */
	, &U3CChaseAvatarU3Ec__IteratorC_t396_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 86/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 494/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CChaseAvatarU3Ec__IteratorC_t396_MethodInfos[] =
{
	&U3CChaseAvatarU3Ec__IteratorC__ctor_m1545_MethodInfo,
	&U3CChaseAvatarU3Ec__IteratorC_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1546_MethodInfo,
	&U3CChaseAvatarU3Ec__IteratorC_System_Collections_IEnumerator_get_Current_m1547_MethodInfo,
	&U3CChaseAvatarU3Ec__IteratorC_MoveNext_m1548_MethodInfo,
	&U3CChaseAvatarU3Ec__IteratorC_Dispose_m1549_MethodInfo,
	&U3CChaseAvatarU3Ec__IteratorC_Reset_m1550_MethodInfo,
	NULL
};
extern const MethodInfo U3CChaseAvatarU3Ec__IteratorC_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1546_MethodInfo;
static const PropertyInfo U3CChaseAvatarU3Ec__IteratorC_t396____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo = 
{
	&U3CChaseAvatarU3Ec__IteratorC_t396_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<object>.Current"/* name */
	, &U3CChaseAvatarU3Ec__IteratorC_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1546_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo U3CChaseAvatarU3Ec__IteratorC_System_Collections_IEnumerator_get_Current_m1547_MethodInfo;
static const PropertyInfo U3CChaseAvatarU3Ec__IteratorC_t396____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CChaseAvatarU3Ec__IteratorC_t396_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CChaseAvatarU3Ec__IteratorC_System_Collections_IEnumerator_get_Current_m1547_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* U3CChaseAvatarU3Ec__IteratorC_t396_PropertyInfos[] =
{
	&U3CChaseAvatarU3Ec__IteratorC_t396____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo,
	&U3CChaseAvatarU3Ec__IteratorC_t396____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern const MethodInfo U3CChaseAvatarU3Ec__IteratorC_Dispose_m1549_MethodInfo;
extern const MethodInfo U3CChaseAvatarU3Ec__IteratorC_MoveNext_m1548_MethodInfo;
extern const MethodInfo U3CChaseAvatarU3Ec__IteratorC_Reset_m1550_MethodInfo;
static const Il2CppMethodReference U3CChaseAvatarU3Ec__IteratorC_t396_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&U3CChaseAvatarU3Ec__IteratorC_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1546_MethodInfo,
	&U3CChaseAvatarU3Ec__IteratorC_Dispose_m1549_MethodInfo,
	&U3CChaseAvatarU3Ec__IteratorC_System_Collections_IEnumerator_get_Current_m1547_MethodInfo,
	&U3CChaseAvatarU3Ec__IteratorC_MoveNext_m1548_MethodInfo,
	&U3CChaseAvatarU3Ec__IteratorC_Reset_m1550_MethodInfo,
};
static bool U3CChaseAvatarU3Ec__IteratorC_t396_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* U3CChaseAvatarU3Ec__IteratorC_t396_InterfacesTypeInfos[] = 
{
	&IEnumerator_1_t284_0_0_0,
	&IDisposable_t233_0_0_0,
	&IEnumerator_t217_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CChaseAvatarU3Ec__IteratorC_t396_InterfacesOffsets[] = 
{
	{ &IEnumerator_1_t284_0_0_0, 4},
	{ &IDisposable_t233_0_0_0, 5},
	{ &IEnumerator_t217_0_0_0, 6},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType U3CChaseAvatarU3Ec__IteratorC_t396_0_0_0;
extern const Il2CppType U3CChaseAvatarU3Ec__IteratorC_t396_1_0_0;
extern TypeInfo SCR_Spider_t384_il2cpp_TypeInfo;
struct U3CChaseAvatarU3Ec__IteratorC_t396;
const Il2CppTypeDefinitionMetadata U3CChaseAvatarU3Ec__IteratorC_t396_DefinitionMetadata = 
{
	&SCR_Spider_t384_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CChaseAvatarU3Ec__IteratorC_t396_InterfacesTypeInfos/* implementedInterfaces */
	, U3CChaseAvatarU3Ec__IteratorC_t396_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CChaseAvatarU3Ec__IteratorC_t396_VTable/* vtableMethods */
	, U3CChaseAvatarU3Ec__IteratorC_t396_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 552/* fieldStart */

};
TypeInfo U3CChaseAvatarU3Ec__IteratorC_t396_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<ChaseAvatar>c__IteratorC"/* name */
	, ""/* namespaze */
	, U3CChaseAvatarU3Ec__IteratorC_t396_MethodInfos/* methods */
	, U3CChaseAvatarU3Ec__IteratorC_t396_PropertyInfos/* properties */
	, NULL/* events */
	, &U3CChaseAvatarU3Ec__IteratorC_t396_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 82/* custom_attributes_cache */
	, &U3CChaseAvatarU3Ec__IteratorC_t396_0_0_0/* byval_arg */
	, &U3CChaseAvatarU3Ec__IteratorC_t396_1_0_0/* this_arg */
	, &U3CChaseAvatarU3Ec__IteratorC_t396_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CChaseAvatarU3Ec__IteratorC_t396)/* instance_size */
	, sizeof (U3CChaseAvatarU3Ec__IteratorC_t396)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// SCR_Spider
#include "AssemblyU2DCSharp_SCR_Spider.h"
// Metadata Definition SCR_Spider
// SCR_Spider
#include "AssemblyU2DCSharp_SCR_SpiderMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Spider::.ctor()
extern const MethodInfo SCR_Spider__ctor_m1551_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SCR_Spider__ctor_m1551/* method */
	, &SCR_Spider_t384_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 480/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Spider::Start()
extern const MethodInfo SCR_Spider_Start_m1552_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&SCR_Spider_Start_m1552/* method */
	, &SCR_Spider_t384_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 481/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Spider::FixedUpdate()
extern const MethodInfo SCR_Spider_FixedUpdate_m1553_MethodInfo = 
{
	"FixedUpdate"/* name */
	, (methodPointerType)&SCR_Spider_FixedUpdate_m1553/* method */
	, &SCR_Spider_t384_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 482/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Collider_t138_0_0_0;
static const ParameterInfo SCR_Spider_t384_SCR_Spider_OnTriggerEnter_m1554_ParameterInfos[] = 
{
	{"other", 0, 134217834, 0, &Collider_t138_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Spider::OnTriggerEnter(UnityEngine.Collider)
extern const MethodInfo SCR_Spider_OnTriggerEnter_m1554_MethodInfo = 
{
	"OnTriggerEnter"/* name */
	, (methodPointerType)&SCR_Spider_OnTriggerEnter_m1554/* method */
	, &SCR_Spider_t384_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, SCR_Spider_t384_SCR_Spider_OnTriggerEnter_m1554_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 483/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Spider::Activate()
extern const MethodInfo SCR_Spider_Activate_m1555_MethodInfo = 
{
	"Activate"/* name */
	, (methodPointerType)&SCR_Spider_Activate_m1555/* method */
	, &SCR_Spider_t384_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 484/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Spider::Reset()
extern const MethodInfo SCR_Spider_Reset_m1556_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&SCR_Spider_Reset_m1556/* method */
	, &SCR_Spider_t384_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 485/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Spider::DropLeaves()
extern const MethodInfo SCR_Spider_DropLeaves_m1557_MethodInfo = 
{
	"DropLeaves"/* name */
	, (methodPointerType)&SCR_Spider_DropLeaves_m1557/* method */
	, &SCR_Spider_t384_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 486/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Spider::DrawWebString()
extern const MethodInfo SCR_Spider_DrawWebString_m1558_MethodInfo = 
{
	"DrawWebString"/* name */
	, (methodPointerType)&SCR_Spider_DrawWebString_m1558/* method */
	, &SCR_Spider_t384_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 487/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.IEnumerator SCR_Spider::ChaseAvatar()
extern const MethodInfo SCR_Spider_ChaseAvatar_m1559_MethodInfo = 
{
	"ChaseAvatar"/* name */
	, (methodPointerType)&SCR_Spider_ChaseAvatar_m1559/* method */
	, &SCR_Spider_t384_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t217_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 81/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 488/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SCR_Spider_t384_MethodInfos[] =
{
	&SCR_Spider__ctor_m1551_MethodInfo,
	&SCR_Spider_Start_m1552_MethodInfo,
	&SCR_Spider_FixedUpdate_m1553_MethodInfo,
	&SCR_Spider_OnTriggerEnter_m1554_MethodInfo,
	&SCR_Spider_Activate_m1555_MethodInfo,
	&SCR_Spider_Reset_m1556_MethodInfo,
	&SCR_Spider_DropLeaves_m1557_MethodInfo,
	&SCR_Spider_DrawWebString_m1558_MethodInfo,
	&SCR_Spider_ChaseAvatar_m1559_MethodInfo,
	NULL
};
static const Il2CppType* SCR_Spider_t384_il2cpp_TypeInfo__nestedTypes[1] =
{
	&U3CChaseAvatarU3Ec__IteratorC_t396_0_0_0,
};
static const Il2CppMethodReference SCR_Spider_t384_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool SCR_Spider_t384_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType SCR_Spider_t384_1_0_0;
struct SCR_Spider_t384;
const Il2CppTypeDefinitionMetadata SCR_Spider_t384_DefinitionMetadata = 
{
	NULL/* declaringType */
	, SCR_Spider_t384_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, SCR_Spider_t384_VTable/* vtableMethods */
	, SCR_Spider_t384_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 557/* fieldStart */

};
TypeInfo SCR_Spider_t384_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "SCR_Spider"/* name */
	, ""/* namespaze */
	, SCR_Spider_t384_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SCR_Spider_t384_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SCR_Spider_t384_0_0_0/* byval_arg */
	, &SCR_Spider_t384_1_0_0/* this_arg */
	, &SCR_Spider_t384_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SCR_Spider_t384)/* instance_size */
	, sizeof (SCR_Spider_t384)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SCR_Web/<DecreaseSize>c__IteratorD
#include "AssemblyU2DCSharp_SCR_Web_U3CDecreaseSizeU3Ec__IteratorD.h"
// Metadata Definition SCR_Web/<DecreaseSize>c__IteratorD
extern TypeInfo U3CDecreaseSizeU3Ec__IteratorD_t399_il2cpp_TypeInfo;
// SCR_Web/<DecreaseSize>c__IteratorD
#include "AssemblyU2DCSharp_SCR_Web_U3CDecreaseSizeU3Ec__IteratorDMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Web/<DecreaseSize>c__IteratorD::.ctor()
extern const MethodInfo U3CDecreaseSizeU3Ec__IteratorD__ctor_m1560_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CDecreaseSizeU3Ec__IteratorD__ctor_m1560/* method */
	, &U3CDecreaseSizeU3Ec__IteratorD_t399_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 500/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object SCR_Web/<DecreaseSize>c__IteratorD::System.Collections.Generic.IEnumerator<object>.get_Current()
extern const MethodInfo U3CDecreaseSizeU3Ec__IteratorD_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1561_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<object>.get_Current"/* name */
	, (methodPointerType)&U3CDecreaseSizeU3Ec__IteratorD_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1561/* method */
	, &U3CDecreaseSizeU3Ec__IteratorD_t399_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 89/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 501/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object SCR_Web/<DecreaseSize>c__IteratorD::System.Collections.IEnumerator.get_Current()
extern const MethodInfo U3CDecreaseSizeU3Ec__IteratorD_System_Collections_IEnumerator_get_Current_m1562_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&U3CDecreaseSizeU3Ec__IteratorD_System_Collections_IEnumerator_get_Current_m1562/* method */
	, &U3CDecreaseSizeU3Ec__IteratorD_t399_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 90/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 502/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean SCR_Web/<DecreaseSize>c__IteratorD::MoveNext()
extern const MethodInfo U3CDecreaseSizeU3Ec__IteratorD_MoveNext_m1563_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&U3CDecreaseSizeU3Ec__IteratorD_MoveNext_m1563/* method */
	, &U3CDecreaseSizeU3Ec__IteratorD_t399_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 503/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Web/<DecreaseSize>c__IteratorD::Dispose()
extern const MethodInfo U3CDecreaseSizeU3Ec__IteratorD_Dispose_m1564_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&U3CDecreaseSizeU3Ec__IteratorD_Dispose_m1564/* method */
	, &U3CDecreaseSizeU3Ec__IteratorD_t399_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 91/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 504/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Web/<DecreaseSize>c__IteratorD::Reset()
extern const MethodInfo U3CDecreaseSizeU3Ec__IteratorD_Reset_m1565_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&U3CDecreaseSizeU3Ec__IteratorD_Reset_m1565/* method */
	, &U3CDecreaseSizeU3Ec__IteratorD_t399_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 92/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 505/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CDecreaseSizeU3Ec__IteratorD_t399_MethodInfos[] =
{
	&U3CDecreaseSizeU3Ec__IteratorD__ctor_m1560_MethodInfo,
	&U3CDecreaseSizeU3Ec__IteratorD_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1561_MethodInfo,
	&U3CDecreaseSizeU3Ec__IteratorD_System_Collections_IEnumerator_get_Current_m1562_MethodInfo,
	&U3CDecreaseSizeU3Ec__IteratorD_MoveNext_m1563_MethodInfo,
	&U3CDecreaseSizeU3Ec__IteratorD_Dispose_m1564_MethodInfo,
	&U3CDecreaseSizeU3Ec__IteratorD_Reset_m1565_MethodInfo,
	NULL
};
extern const MethodInfo U3CDecreaseSizeU3Ec__IteratorD_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1561_MethodInfo;
static const PropertyInfo U3CDecreaseSizeU3Ec__IteratorD_t399____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo = 
{
	&U3CDecreaseSizeU3Ec__IteratorD_t399_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<object>.Current"/* name */
	, &U3CDecreaseSizeU3Ec__IteratorD_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1561_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo U3CDecreaseSizeU3Ec__IteratorD_System_Collections_IEnumerator_get_Current_m1562_MethodInfo;
static const PropertyInfo U3CDecreaseSizeU3Ec__IteratorD_t399____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CDecreaseSizeU3Ec__IteratorD_t399_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CDecreaseSizeU3Ec__IteratorD_System_Collections_IEnumerator_get_Current_m1562_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* U3CDecreaseSizeU3Ec__IteratorD_t399_PropertyInfos[] =
{
	&U3CDecreaseSizeU3Ec__IteratorD_t399____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo,
	&U3CDecreaseSizeU3Ec__IteratorD_t399____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern const MethodInfo U3CDecreaseSizeU3Ec__IteratorD_Dispose_m1564_MethodInfo;
extern const MethodInfo U3CDecreaseSizeU3Ec__IteratorD_MoveNext_m1563_MethodInfo;
extern const MethodInfo U3CDecreaseSizeU3Ec__IteratorD_Reset_m1565_MethodInfo;
static const Il2CppMethodReference U3CDecreaseSizeU3Ec__IteratorD_t399_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&U3CDecreaseSizeU3Ec__IteratorD_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1561_MethodInfo,
	&U3CDecreaseSizeU3Ec__IteratorD_Dispose_m1564_MethodInfo,
	&U3CDecreaseSizeU3Ec__IteratorD_System_Collections_IEnumerator_get_Current_m1562_MethodInfo,
	&U3CDecreaseSizeU3Ec__IteratorD_MoveNext_m1563_MethodInfo,
	&U3CDecreaseSizeU3Ec__IteratorD_Reset_m1565_MethodInfo,
};
static bool U3CDecreaseSizeU3Ec__IteratorD_t399_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* U3CDecreaseSizeU3Ec__IteratorD_t399_InterfacesTypeInfos[] = 
{
	&IEnumerator_1_t284_0_0_0,
	&IDisposable_t233_0_0_0,
	&IEnumerator_t217_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CDecreaseSizeU3Ec__IteratorD_t399_InterfacesOffsets[] = 
{
	{ &IEnumerator_1_t284_0_0_0, 4},
	{ &IDisposable_t233_0_0_0, 5},
	{ &IEnumerator_t217_0_0_0, 6},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType U3CDecreaseSizeU3Ec__IteratorD_t399_0_0_0;
extern const Il2CppType U3CDecreaseSizeU3Ec__IteratorD_t399_1_0_0;
extern TypeInfo SCR_Web_t398_il2cpp_TypeInfo;
extern const Il2CppType SCR_Web_t398_0_0_0;
struct U3CDecreaseSizeU3Ec__IteratorD_t399;
const Il2CppTypeDefinitionMetadata U3CDecreaseSizeU3Ec__IteratorD_t399_DefinitionMetadata = 
{
	&SCR_Web_t398_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CDecreaseSizeU3Ec__IteratorD_t399_InterfacesTypeInfos/* implementedInterfaces */
	, U3CDecreaseSizeU3Ec__IteratorD_t399_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CDecreaseSizeU3Ec__IteratorD_t399_VTable/* vtableMethods */
	, U3CDecreaseSizeU3Ec__IteratorD_t399_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 564/* fieldStart */

};
TypeInfo U3CDecreaseSizeU3Ec__IteratorD_t399_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<DecreaseSize>c__IteratorD"/* name */
	, ""/* namespaze */
	, U3CDecreaseSizeU3Ec__IteratorD_t399_MethodInfos/* methods */
	, U3CDecreaseSizeU3Ec__IteratorD_t399_PropertyInfos/* properties */
	, NULL/* events */
	, &U3CDecreaseSizeU3Ec__IteratorD_t399_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 88/* custom_attributes_cache */
	, &U3CDecreaseSizeU3Ec__IteratorD_t399_0_0_0/* byval_arg */
	, &U3CDecreaseSizeU3Ec__IteratorD_t399_1_0_0/* this_arg */
	, &U3CDecreaseSizeU3Ec__IteratorD_t399_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CDecreaseSizeU3Ec__IteratorD_t399)/* instance_size */
	, sizeof (U3CDecreaseSizeU3Ec__IteratorD_t399)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// SCR_Web
#include "AssemblyU2DCSharp_SCR_Web.h"
// Metadata Definition SCR_Web
// SCR_Web
#include "AssemblyU2DCSharp_SCR_WebMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Web::.ctor()
extern const MethodInfo SCR_Web__ctor_m1566_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SCR_Web__ctor_m1566/* method */
	, &SCR_Web_t398_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 495/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Web::Start()
extern const MethodInfo SCR_Web_Start_m1567_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&SCR_Web_Start_m1567/* method */
	, &SCR_Web_t398_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 496/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Web::TriggerEnterEffect()
extern const MethodInfo SCR_Web_TriggerEnterEffect_m1568_MethodInfo = 
{
	"TriggerEnterEffect"/* name */
	, (methodPointerType)&SCR_Web_TriggerEnterEffect_m1568/* method */
	, &SCR_Web_t398_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 497/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_Web::TriggerExitEffet()
extern const MethodInfo SCR_Web_TriggerExitEffet_m1569_MethodInfo = 
{
	"TriggerExitEffet"/* name */
	, (methodPointerType)&SCR_Web_TriggerExitEffet_m1569/* method */
	, &SCR_Web_t398_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 498/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.IEnumerator SCR_Web::DecreaseSize()
extern const MethodInfo SCR_Web_DecreaseSize_m1570_MethodInfo = 
{
	"DecreaseSize"/* name */
	, (methodPointerType)&SCR_Web_DecreaseSize_m1570/* method */
	, &SCR_Web_t398_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t217_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 87/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 499/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SCR_Web_t398_MethodInfos[] =
{
	&SCR_Web__ctor_m1566_MethodInfo,
	&SCR_Web_Start_m1567_MethodInfo,
	&SCR_Web_TriggerEnterEffect_m1568_MethodInfo,
	&SCR_Web_TriggerExitEffet_m1569_MethodInfo,
	&SCR_Web_DecreaseSize_m1570_MethodInfo,
	NULL
};
static const Il2CppType* SCR_Web_t398_il2cpp_TypeInfo__nestedTypes[1] =
{
	&U3CDecreaseSizeU3Ec__IteratorD_t399_0_0_0,
};
extern const MethodInfo SCR_Web_TriggerEnterEffect_m1568_MethodInfo;
extern const MethodInfo SCR_Web_TriggerExitEffet_m1569_MethodInfo;
static const Il2CppMethodReference SCR_Web_t398_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	&SCR_Web_TriggerEnterEffect_m1568_MethodInfo,
	&SCR_Web_TriggerExitEffet_m1569_MethodInfo,
};
static bool SCR_Web_t398_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType SCR_Web_t398_1_0_0;
struct SCR_Web_t398;
const Il2CppTypeDefinitionMetadata SCR_Web_t398_DefinitionMetadata = 
{
	NULL/* declaringType */
	, SCR_Web_t398_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &SCR_SlowAvatar_t337_0_0_0/* parent */
	, SCR_Web_t398_VTable/* vtableMethods */
	, SCR_Web_t398_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 568/* fieldStart */

};
TypeInfo SCR_Web_t398_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "SCR_Web"/* name */
	, ""/* namespaze */
	, SCR_Web_t398_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SCR_Web_t398_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SCR_Web_t398_0_0_0/* byval_arg */
	, &SCR_Web_t398_1_0_0/* this_arg */
	, &SCR_Web_t398_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SCR_Web_t398)/* instance_size */
	, sizeof (SCR_Web_t398)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SCR_CamieDirections
#include "AssemblyU2DCSharp_SCR_CamieDirections.h"
// Metadata Definition SCR_CamieDirections
extern TypeInfo SCR_CamieDirections_t348_il2cpp_TypeInfo;
// SCR_CamieDirections
#include "AssemblyU2DCSharp_SCR_CamieDirectionsMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_CamieDirections::.ctor()
extern const MethodInfo SCR_CamieDirections__ctor_m1571_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SCR_CamieDirections__ctor_m1571/* method */
	, &SCR_CamieDirections_t348_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 506/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_CamieDirections::Start()
extern const MethodInfo SCR_CamieDirections_Start_m1572_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&SCR_CamieDirections_Start_m1572/* method */
	, &SCR_CamieDirections_t348_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 507/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SCR_CamieDirections::Update()
extern const MethodInfo SCR_CamieDirections_Update_m1573_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&SCR_CamieDirections_Update_m1573/* method */
	, &SCR_CamieDirections_t348_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 508/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TransformU5BU5D_t141_0_0_0;
static const ParameterInfo SCR_CamieDirections_t348_SCR_CamieDirections_ReorderByName_m1574_ParameterInfos[] = 
{
	{"array", 0, 134217835, 0, &TransformU5BU5D_t141_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Transform[] SCR_CamieDirections::ReorderByName(UnityEngine.Transform[])
extern const MethodInfo SCR_CamieDirections_ReorderByName_m1574_MethodInfo = 
{
	"ReorderByName"/* name */
	, (methodPointerType)&SCR_CamieDirections_ReorderByName_m1574/* method */
	, &SCR_CamieDirections_t348_il2cpp_TypeInfo/* declaring_type */
	, &TransformU5BU5D_t141_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, SCR_CamieDirections_t348_SCR_CamieDirections_ReorderByName_m1574_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 509/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType QuaternionU5BU5D_t400_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Quaternion[] SCR_CamieDirections::get_Angles()
extern const MethodInfo SCR_CamieDirections_get_Angles_m1575_MethodInfo = 
{
	"get_Angles"/* name */
	, (methodPointerType)&SCR_CamieDirections_get_Angles_m1575/* method */
	, &SCR_CamieDirections_t348_il2cpp_TypeInfo/* declaring_type */
	, &QuaternionU5BU5D_t400_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 510/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector3U5BU5D_t210_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector3[] SCR_CamieDirections::get_Normals()
extern const MethodInfo SCR_CamieDirections_get_Normals_m1576_MethodInfo = 
{
	"get_Normals"/* name */
	, (methodPointerType)&SCR_CamieDirections_get_Normals_m1576/* method */
	, &SCR_CamieDirections_t348_il2cpp_TypeInfo/* declaring_type */
	, &Vector3U5BU5D_t210_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 511/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SCR_CamieDirections_t348_MethodInfos[] =
{
	&SCR_CamieDirections__ctor_m1571_MethodInfo,
	&SCR_CamieDirections_Start_m1572_MethodInfo,
	&SCR_CamieDirections_Update_m1573_MethodInfo,
	&SCR_CamieDirections_ReorderByName_m1574_MethodInfo,
	&SCR_CamieDirections_get_Angles_m1575_MethodInfo,
	&SCR_CamieDirections_get_Normals_m1576_MethodInfo,
	NULL
};
extern const MethodInfo SCR_CamieDirections_get_Angles_m1575_MethodInfo;
static const PropertyInfo SCR_CamieDirections_t348____Angles_PropertyInfo = 
{
	&SCR_CamieDirections_t348_il2cpp_TypeInfo/* parent */
	, "Angles"/* name */
	, &SCR_CamieDirections_get_Angles_m1575_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo SCR_CamieDirections_get_Normals_m1576_MethodInfo;
static const PropertyInfo SCR_CamieDirections_t348____Normals_PropertyInfo = 
{
	&SCR_CamieDirections_t348_il2cpp_TypeInfo/* parent */
	, "Normals"/* name */
	, &SCR_CamieDirections_get_Normals_m1576_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* SCR_CamieDirections_t348_PropertyInfos[] =
{
	&SCR_CamieDirections_t348____Angles_PropertyInfo,
	&SCR_CamieDirections_t348____Normals_PropertyInfo,
	NULL
};
static const Il2CppMethodReference SCR_CamieDirections_t348_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool SCR_CamieDirections_t348_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType SCR_CamieDirections_t348_1_0_0;
struct SCR_CamieDirections_t348;
const Il2CppTypeDefinitionMetadata SCR_CamieDirections_t348_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, SCR_CamieDirections_t348_VTable/* vtableMethods */
	, SCR_CamieDirections_t348_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 572/* fieldStart */

};
TypeInfo SCR_CamieDirections_t348_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "SCR_CamieDirections"/* name */
	, ""/* namespaze */
	, SCR_CamieDirections_t348_MethodInfos/* methods */
	, SCR_CamieDirections_t348_PropertyInfos/* properties */
	, NULL/* events */
	, &SCR_CamieDirections_t348_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SCR_CamieDirections_t348_0_0_0/* byval_arg */
	, &SCR_CamieDirections_t348_1_0_0/* this_arg */
	, &SCR_CamieDirections_t348_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SCR_CamieDirections_t348)/* instance_size */
	, sizeof (SCR_CamieDirections_t348)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
