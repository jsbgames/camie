﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.DrivenRectTransformTracker
struct DrivenRectTransformTracker_t571;
// UnityEngine.Object
struct Object_t164;
struct Object_t164_marshaled;
// UnityEngine.RectTransform
struct RectTransform_t364;
// UnityEngine.DrivenTransformProperties
#include "UnityEngine_UnityEngine_DrivenTransformProperties.h"

// System.Void UnityEngine.DrivenRectTransformTracker::Add(UnityEngine.Object,UnityEngine.RectTransform,UnityEngine.DrivenTransformProperties)
extern "C" void DrivenRectTransformTracker_Add_m3352 (DrivenRectTransformTracker_t571 * __this, Object_t164 * ___driver, RectTransform_t364 * ___rectTransform, int32_t ___drivenProperties, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.DrivenRectTransformTracker::Clear()
extern "C" void DrivenRectTransformTracker_Clear_m3350 (DrivenRectTransformTracker_t571 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
