﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets.Utility.ParticleSystemDestroyer
struct ParticleSystemDestroyer_t189;
// System.Collections.IEnumerator
struct IEnumerator_t217;

// System.Void UnityStandardAssets.Utility.ParticleSystemDestroyer::.ctor()
extern "C" void ParticleSystemDestroyer__ctor_m523 (ParticleSystemDestroyer_t189 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UnityStandardAssets.Utility.ParticleSystemDestroyer::Start()
extern "C" Object_t * ParticleSystemDestroyer_Start_m524 (ParticleSystemDestroyer_t189 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Utility.ParticleSystemDestroyer::Stop()
extern "C" void ParticleSystemDestroyer_Stop_m525 (ParticleSystemDestroyer_t189 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
