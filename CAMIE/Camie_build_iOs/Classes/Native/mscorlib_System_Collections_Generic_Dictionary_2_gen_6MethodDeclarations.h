﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean>
struct Dictionary_2_t373;
// System.Collections.Generic.ICollection`1<System.Int32>
struct ICollection_1_t3581;
// System.Collections.Generic.ICollection`1<System.Boolean>
struct ICollection_1_t3594;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Boolean>
struct KeyCollection_t3000;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Boolean>
struct ValueCollection_t3001;
// System.Collections.Generic.IEqualityComparer`1<System.Int32>
struct IEqualityComparer_1_t2980;
// System.Collections.Generic.IDictionary`2<System.Int32,System.Boolean>
struct IDictionary_2_t3595;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1043;
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Boolean>[]
struct KeyValuePair_2U5BU5D_t3596;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t217;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Boolean>>
struct IEnumerator_1_t3597;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1429;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Boolean>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_14.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Boolean>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__12.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean>::.ctor()
// System.Collections.Generic.Dictionary`2<System.Int32,System.Byte>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_25MethodDeclarations.h"
#define Dictionary_2__ctor_m1782(__this, method) (( void (*) (Dictionary_2_t373 *, const MethodInfo*))Dictionary_2__ctor_m15814_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m15815(__this, ___comparer, method) (( void (*) (Dictionary_2_t373 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m15816_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
#define Dictionary_2__ctor_m15817(__this, ___dictionary, method) (( void (*) (Dictionary_2_t373 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m15818_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean>::.ctor(System.Int32)
#define Dictionary_2__ctor_m15819(__this, ___capacity, method) (( void (*) (Dictionary_2_t373 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m15820_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m15821(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t373 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m15822_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2__ctor_m15823(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t373 *, SerializationInfo_t1043 *, StreamingContext_t1044 , const MethodInfo*))Dictionary_2__ctor_m15824_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m15825(__this, method) (( Object_t* (*) (Dictionary_2_t373 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m15826_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m15827(__this, method) (( Object_t* (*) (Dictionary_2_t373 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m15828_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean>::System.Collections.IDictionary.get_Item(System.Object)
#define Dictionary_2_System_Collections_IDictionary_get_Item_m15829(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t373 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m15830_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_set_Item_m15831(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t373 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m15832_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_Add_m15833(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t373 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m15834_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean>::System.Collections.IDictionary.Contains(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Contains_m15835(__this, ___key, method) (( bool (*) (Dictionary_2_t373 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m15836_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean>::System.Collections.IDictionary.Remove(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Remove_m15837(__this, ___key, method) (( void (*) (Dictionary_2_t373 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m15838_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean>::System.Collections.ICollection.get_IsSynchronized()
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m15839(__this, method) (( bool (*) (Dictionary_2_t373 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m15840_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean>::System.Collections.ICollection.get_SyncRoot()
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m15841(__this, method) (( Object_t * (*) (Dictionary_2_t373 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m15842_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m15843(__this, method) (( bool (*) (Dictionary_2_t373 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m15844_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m15845(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t373 *, KeyValuePair_2_t2999 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m15846_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m15847(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t373 *, KeyValuePair_2_t2999 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m15848_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m15849(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t373 *, KeyValuePair_2U5BU5D_t3596*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m15850_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m15851(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t373 *, KeyValuePair_2_t2999 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m15852_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Dictionary_2_System_Collections_ICollection_CopyTo_m15853(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t373 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m15854_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean>::System.Collections.IEnumerable.GetEnumerator()
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m15855(__this, method) (( Object_t * (*) (Dictionary_2_t373 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m15856_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m15857(__this, method) (( Object_t* (*) (Dictionary_2_t373 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m15858_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean>::System.Collections.IDictionary.GetEnumerator()
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m15859(__this, method) (( Object_t * (*) (Dictionary_2_t373 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m15860_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean>::get_Count()
#define Dictionary_2_get_Count_m15861(__this, method) (( int32_t (*) (Dictionary_2_t373 *, const MethodInfo*))Dictionary_2_get_Count_m15862_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean>::get_Item(TKey)
#define Dictionary_2_get_Item_m15863(__this, ___key, method) (( bool (*) (Dictionary_2_t373 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m15864_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean>::set_Item(TKey,TValue)
#define Dictionary_2_set_Item_m15865(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t373 *, int32_t, bool, const MethodInfo*))Dictionary_2_set_Item_m15866_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2_Init_m15867(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t373 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m15868_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean>::InitArrays(System.Int32)
#define Dictionary_2_InitArrays_m15869(__this, ___size, method) (( void (*) (Dictionary_2_t373 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m15870_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean>::CopyToCheck(System.Array,System.Int32)
#define Dictionary_2_CopyToCheck_m15871(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t373 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m15872_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean>::make_pair(TKey,TValue)
#define Dictionary_2_make_pair_m15873(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t2999  (*) (Object_t * /* static, unused */, int32_t, bool, const MethodInfo*))Dictionary_2_make_pair_m15874_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean>::pick_key(TKey,TValue)
#define Dictionary_2_pick_key_m15875(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, int32_t, bool, const MethodInfo*))Dictionary_2_pick_key_m15876_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean>::pick_value(TKey,TValue)
#define Dictionary_2_pick_value_m15877(__this /* static, unused */, ___key, ___value, method) (( bool (*) (Object_t * /* static, unused */, int32_t, bool, const MethodInfo*))Dictionary_2_pick_value_m15878_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_CopyTo_m15879(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t373 *, KeyValuePair_2U5BU5D_t3596*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m15880_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean>::Resize()
#define Dictionary_2_Resize_m15881(__this, method) (( void (*) (Dictionary_2_t373 *, const MethodInfo*))Dictionary_2_Resize_m15882_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean>::Add(TKey,TValue)
#define Dictionary_2_Add_m15883(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t373 *, int32_t, bool, const MethodInfo*))Dictionary_2_Add_m15884_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean>::Clear()
#define Dictionary_2_Clear_m15885(__this, method) (( void (*) (Dictionary_2_t373 *, const MethodInfo*))Dictionary_2_Clear_m15886_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean>::ContainsKey(TKey)
#define Dictionary_2_ContainsKey_m15887(__this, ___key, method) (( bool (*) (Dictionary_2_t373 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m15888_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean>::ContainsValue(TValue)
#define Dictionary_2_ContainsValue_m15889(__this, ___value, method) (( bool (*) (Dictionary_2_t373 *, bool, const MethodInfo*))Dictionary_2_ContainsValue_m15890_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2_GetObjectData_m15891(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t373 *, SerializationInfo_t1043 *, StreamingContext_t1044 , const MethodInfo*))Dictionary_2_GetObjectData_m15892_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean>::OnDeserialization(System.Object)
#define Dictionary_2_OnDeserialization_m15893(__this, ___sender, method) (( void (*) (Dictionary_2_t373 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m15894_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean>::Remove(TKey)
#define Dictionary_2_Remove_m15895(__this, ___key, method) (( bool (*) (Dictionary_2_t373 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m15896_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean>::TryGetValue(TKey,TValue&)
#define Dictionary_2_TryGetValue_m15897(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t373 *, int32_t, bool*, const MethodInfo*))Dictionary_2_TryGetValue_m15898_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean>::get_Keys()
#define Dictionary_2_get_Keys_m15899(__this, method) (( KeyCollection_t3000 * (*) (Dictionary_2_t373 *, const MethodInfo*))Dictionary_2_get_Keys_m15900_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean>::get_Values()
#define Dictionary_2_get_Values_m15901(__this, method) (( ValueCollection_t3001 * (*) (Dictionary_2_t373 *, const MethodInfo*))Dictionary_2_get_Values_m15902_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean>::ToTKey(System.Object)
#define Dictionary_2_ToTKey_m15903(__this, ___key, method) (( int32_t (*) (Dictionary_2_t373 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m15904_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean>::ToTValue(System.Object)
#define Dictionary_2_ToTValue_m15905(__this, ___value, method) (( bool (*) (Dictionary_2_t373 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m15906_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_ContainsKeyValuePair_m15907(__this, ___pair, method) (( bool (*) (Dictionary_2_t373 *, KeyValuePair_2_t2999 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m15908_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m15909(__this, method) (( Enumerator_t3002  (*) (Dictionary_2_t373 *, const MethodInfo*))Dictionary_2_GetEnumerator_m15910_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean>::<CopyTo>m__0(TKey,TValue)
#define Dictionary_2_U3CCopyToU3Em__0_m15911(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t1430  (*) (Object_t * /* static, unused */, int32_t, bool, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m15912_gshared)(__this /* static, unused */, ___key, ___value, method)
