﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>
struct List_1_t493;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.EventSystems.PointerInputModule/MouseState
struct  MouseState_t494  : public Object_t
{
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState> UnityEngine.EventSystems.PointerInputModule/MouseState::m_TrackedButtons
	List_1_t493 * ___m_TrackedButtons_0;
};
