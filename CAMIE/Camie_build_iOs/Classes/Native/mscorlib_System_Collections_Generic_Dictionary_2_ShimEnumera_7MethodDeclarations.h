﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>
struct ShimEnumerator_t3436;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t1366;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m22114_gshared (ShimEnumerator_t3436 * __this, Dictionary_2_t1366 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m22114(__this, ___host, method) (( void (*) (ShimEnumerator_t3436 *, Dictionary_2_t1366 *, const MethodInfo*))ShimEnumerator__ctor_m22114_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m22115_gshared (ShimEnumerator_t3436 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m22115(__this, method) (( bool (*) (ShimEnumerator_t3436 *, const MethodInfo*))ShimEnumerator_MoveNext_m22115_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>::get_Entry()
extern "C" DictionaryEntry_t1430  ShimEnumerator_get_Entry_m22116_gshared (ShimEnumerator_t3436 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m22116(__this, method) (( DictionaryEntry_t1430  (*) (ShimEnumerator_t3436 *, const MethodInfo*))ShimEnumerator_get_Entry_m22116_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m22117_gshared (ShimEnumerator_t3436 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m22117(__this, method) (( Object_t * (*) (ShimEnumerator_t3436 *, const MethodInfo*))ShimEnumerator_get_Key_m22117_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m22118_gshared (ShimEnumerator_t3436 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m22118(__this, method) (( Object_t * (*) (ShimEnumerator_t3436 *, const MethodInfo*))ShimEnumerator_get_Value_m22118_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m22119_gshared (ShimEnumerator_t3436 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m22119(__this, method) (( Object_t * (*) (ShimEnumerator_t3436 *, const MethodInfo*))ShimEnumerator_get_Current_m22119_gshared)(__this, method)
