﻿#pragma once
#include <stdint.h>
// Reporter/Sample[]
struct SampleU5BU5D_t2894;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<Reporter/Sample>
struct  List_1_t297  : public Object_t
{
	// T[] System.Collections.Generic.List`1<Reporter/Sample>::_items
	SampleU5BU5D_t2894* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<Reporter/Sample>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<Reporter/Sample>::_version
	int32_t ____version_3;
};
struct List_1_t297_StaticFields{
	// T[] System.Collections.Generic.List`1<Reporter/Sample>::EmptyArray
	SampleU5BU5D_t2894* ___EmptyArray_4;
};
