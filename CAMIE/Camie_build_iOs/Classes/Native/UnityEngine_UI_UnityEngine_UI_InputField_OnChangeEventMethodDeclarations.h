﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.InputField/OnChangeEvent
struct OnChangeEvent_t544;

// System.Void UnityEngine.UI.InputField/OnChangeEvent::.ctor()
extern "C" void OnChangeEvent__ctor_m2356 (OnChangeEvent_t544 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
