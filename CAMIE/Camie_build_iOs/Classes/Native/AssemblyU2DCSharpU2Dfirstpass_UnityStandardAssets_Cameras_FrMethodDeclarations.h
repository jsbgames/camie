﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets.Cameras.FreeLookCam
struct FreeLookCam_t18;

// System.Void UnityStandardAssets.Cameras.FreeLookCam::.ctor()
extern "C" void FreeLookCam__ctor_m30 (FreeLookCam_t18 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Cameras.FreeLookCam::Awake()
extern "C" void FreeLookCam_Awake_m31 (FreeLookCam_t18 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Cameras.FreeLookCam::Update()
extern "C" void FreeLookCam_Update_m32 (FreeLookCam_t18 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Cameras.FreeLookCam::OnDisable()
extern "C" void FreeLookCam_OnDisable_m33 (FreeLookCam_t18 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Cameras.FreeLookCam::FollowTarget(System.Single)
extern "C" void FreeLookCam_FollowTarget_m34 (FreeLookCam_t18 * __this, float ___deltaTime, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Cameras.FreeLookCam::HandleRotationMovement()
extern "C" void FreeLookCam_HandleRotationMovement_m35 (FreeLookCam_t18 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
