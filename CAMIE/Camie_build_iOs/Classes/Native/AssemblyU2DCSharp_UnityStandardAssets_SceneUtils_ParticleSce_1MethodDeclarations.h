﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystem
struct DemoParticleSystem_t321;

// System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystem::.ctor()
extern "C" void DemoParticleSystem__ctor_m1171 (DemoParticleSystem_t321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
