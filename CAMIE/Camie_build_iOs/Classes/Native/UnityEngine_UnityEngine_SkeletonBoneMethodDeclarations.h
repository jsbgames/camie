﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.SkeletonBone
struct SkeletonBone_t893;
struct SkeletonBone_t893_marshaled;

void SkeletonBone_t893_marshal(const SkeletonBone_t893& unmarshaled, SkeletonBone_t893_marshaled& marshaled);
void SkeletonBone_t893_marshal_back(const SkeletonBone_t893_marshaled& marshaled, SkeletonBone_t893& unmarshaled);
void SkeletonBone_t893_marshal_cleanup(SkeletonBone_t893_marshaled& marshaled);
