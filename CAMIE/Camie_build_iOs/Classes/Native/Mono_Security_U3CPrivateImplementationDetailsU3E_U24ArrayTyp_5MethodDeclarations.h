﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$12
struct U24ArrayTypeU2412_t1632;
struct U24ArrayTypeU2412_t1632_marshaled;

void U24ArrayTypeU2412_t1632_marshal(const U24ArrayTypeU2412_t1632& unmarshaled, U24ArrayTypeU2412_t1632_marshaled& marshaled);
void U24ArrayTypeU2412_t1632_marshal_back(const U24ArrayTypeU2412_t1632_marshaled& marshaled, U24ArrayTypeU2412_t1632& unmarshaled);
void U24ArrayTypeU2412_t1632_marshal_cleanup(U24ArrayTypeU2412_t1632_marshaled& marshaled);
