﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.TimeSpan>
struct DefaultComparer_t3512;
// System.TimeSpan
#include "mscorlib_System_TimeSpan.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.TimeSpan>::.ctor()
extern "C" void DefaultComparer__ctor_m22542_gshared (DefaultComparer_t3512 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m22542(__this, method) (( void (*) (DefaultComparer_t3512 *, const MethodInfo*))DefaultComparer__ctor_m22542_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.TimeSpan>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m22543_gshared (DefaultComparer_t3512 * __this, TimeSpan_t1337  ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m22543(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t3512 *, TimeSpan_t1337 , const MethodInfo*))DefaultComparer_GetHashCode_m22543_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.TimeSpan>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m22544_gshared (DefaultComparer_t3512 * __this, TimeSpan_t1337  ___x, TimeSpan_t1337  ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m22544(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t3512 *, TimeSpan_t1337 , TimeSpan_t1337 , const MethodInfo*))DefaultComparer_Equals_m22544_gshared)(__this, ___x, ___y, method)
