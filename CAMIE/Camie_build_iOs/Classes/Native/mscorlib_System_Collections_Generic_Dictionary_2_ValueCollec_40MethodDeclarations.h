﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Byte>
struct Enumerator_t3414;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Byte>
struct Dictionary_2_t3406;

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Byte>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m21898_gshared (Enumerator_t3414 * __this, Dictionary_2_t3406 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m21898(__this, ___host, method) (( void (*) (Enumerator_t3414 *, Dictionary_2_t3406 *, const MethodInfo*))Enumerator__ctor_m21898_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Byte>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m21899_gshared (Enumerator_t3414 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m21899(__this, method) (( Object_t * (*) (Enumerator_t3414 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m21899_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Byte>::Dispose()
extern "C" void Enumerator_Dispose_m21900_gshared (Enumerator_t3414 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m21900(__this, method) (( void (*) (Enumerator_t3414 *, const MethodInfo*))Enumerator_Dispose_m21900_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Byte>::MoveNext()
extern "C" bool Enumerator_MoveNext_m21901_gshared (Enumerator_t3414 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m21901(__this, method) (( bool (*) (Enumerator_t3414 *, const MethodInfo*))Enumerator_MoveNext_m21901_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Byte>::get_Current()
extern "C" uint8_t Enumerator_get_Current_m21902_gshared (Enumerator_t3414 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m21902(__this, method) (( uint8_t (*) (Enumerator_t3414 *, const MethodInfo*))Enumerator_get_Current_m21902_gshared)(__this, method)
