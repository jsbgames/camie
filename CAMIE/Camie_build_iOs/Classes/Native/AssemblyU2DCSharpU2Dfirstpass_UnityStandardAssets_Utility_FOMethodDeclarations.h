﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets.Utility.FOVKick/<FOVKickUp>c__Iterator4
struct U3CFOVKickUpU3Ec__Iterator4_t179;
// System.Object
struct Object_t;

// System.Void UnityStandardAssets.Utility.FOVKick/<FOVKickUp>c__Iterator4::.ctor()
extern "C" void U3CFOVKickUpU3Ec__Iterator4__ctor_m473 (U3CFOVKickUpU3Ec__Iterator4_t179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityStandardAssets.Utility.FOVKick/<FOVKickUp>c__Iterator4::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CFOVKickUpU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m474 (U3CFOVKickUpU3Ec__Iterator4_t179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityStandardAssets.Utility.FOVKick/<FOVKickUp>c__Iterator4::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CFOVKickUpU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m475 (U3CFOVKickUpU3Ec__Iterator4_t179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityStandardAssets.Utility.FOVKick/<FOVKickUp>c__Iterator4::MoveNext()
extern "C" bool U3CFOVKickUpU3Ec__Iterator4_MoveNext_m476 (U3CFOVKickUpU3Ec__Iterator4_t179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Utility.FOVKick/<FOVKickUp>c__Iterator4::Dispose()
extern "C" void U3CFOVKickUpU3Ec__Iterator4_Dispose_m477 (U3CFOVKickUpU3Ec__Iterator4_t179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Utility.FOVKick/<FOVKickUp>c__Iterator4::Reset()
extern "C" void U3CFOVKickUpU3Ec__Iterator4_Reset_m478 (U3CFOVKickUpU3Ec__Iterator4_t179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
