﻿#pragma once
#include <stdint.h>
// System.Collections.ArrayList
struct ArrayList_t1271;
// System.Object
#include "mscorlib_System_Object.h"
// System.Security.Cryptography.OidCollection
struct  OidCollection_t1336  : public Object_t
{
	// System.Collections.ArrayList System.Security.Cryptography.OidCollection::_list
	ArrayList_t1271 * ____list_0;
	// System.Boolean System.Security.Cryptography.OidCollection::_readOnly
	bool ____readOnly_1;
};
