﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets.ImageEffects.NoiseAndScratches
struct NoiseAndScratches_t111;
// UnityEngine.Material
struct Material_t55;
// UnityEngine.RenderTexture
struct RenderTexture_t101;

// System.Void UnityStandardAssets.ImageEffects.NoiseAndScratches::.ctor()
extern "C" void NoiseAndScratches__ctor_m321 (NoiseAndScratches_t111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.ImageEffects.NoiseAndScratches::Start()
extern "C" void NoiseAndScratches_Start_m322 (NoiseAndScratches_t111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material UnityStandardAssets.ImageEffects.NoiseAndScratches::get_material()
extern "C" Material_t55 * NoiseAndScratches_get_material_m323 (NoiseAndScratches_t111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.ImageEffects.NoiseAndScratches::OnDisable()
extern "C" void NoiseAndScratches_OnDisable_m324 (NoiseAndScratches_t111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.ImageEffects.NoiseAndScratches::SanitizeParameters()
extern "C" void NoiseAndScratches_SanitizeParameters_m325 (NoiseAndScratches_t111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.ImageEffects.NoiseAndScratches::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void NoiseAndScratches_OnRenderImage_m326 (NoiseAndScratches_t111 * __this, RenderTexture_t101 * ___source, RenderTexture_t101 * ___destination, const MethodInfo* method) IL2CPP_METHOD_ATTR;
