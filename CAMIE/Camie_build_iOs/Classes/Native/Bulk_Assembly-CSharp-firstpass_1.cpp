﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
// UnityStandardAssets.ImageEffects.Triangles
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_63.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
// UnityStandardAssets.ImageEffects.Triangles
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_63MethodDeclarations.h"

// System.Void
#include "mscorlib_System_Void.h"
// System.Boolean
#include "mscorlib_System_Boolean.h"
// System.Int32
#include "mscorlib_System_Int32.h"
#include "UnityEngine_ArrayTypes.h"
// UnityEngine.Mesh
#include "UnityEngine_UnityEngine_Mesh.h"
// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"
// System.Single
#include "mscorlib_System_Single.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
#include "mscorlib_ArrayTypes.h"
// UnityEngine.HideFlags
#include "UnityEngine_UnityEngine_HideFlags.h"
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
// UnityEngine.Object
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
// UnityEngine.Mathf
#include "UnityEngine_UnityEngine_MathfMethodDeclarations.h"
// UnityEngine.Mesh
#include "UnityEngine_UnityEngine_MeshMethodDeclarations.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3MethodDeclarations.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2MethodDeclarations.h"

// System.Array
#include "mscorlib_System_Array.h"

// System.Void UnityStandardAssets.ImageEffects.Triangles::.ctor()
extern "C" void Triangles__ctor_m381 (Triangles_t130 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m677(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.Triangles::.cctor()
extern "C" void Triangles__cctor_m382 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Boolean UnityStandardAssets.ImageEffects.Triangles::HasMeshes()
extern TypeInfo* Triangles_t130_il2cpp_TypeInfo_var;
extern "C" bool Triangles_HasMeshes_m383 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Triangles_t130_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(63);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Triangles_t130_il2cpp_TypeInfo_var);
		MeshU5BU5D_t113* L_0 = ((Triangles_t130_StaticFields*)Triangles_t130_il2cpp_TypeInfo_var->static_fields)->___meshes_0;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return 0;
	}

IL_000c:
	{
		V_0 = 0;
		goto IL_002b;
	}

IL_0013:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Triangles_t130_il2cpp_TypeInfo_var);
		MeshU5BU5D_t113* L_1 = ((Triangles_t130_StaticFields*)Triangles_t130_il2cpp_TypeInfo_var->static_fields)->___meshes_0;
		int32_t L_2 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		bool L_4 = Object_op_Equality_m640(NULL /*static, unused*/, (Object_t164 *)NULL, (*(Mesh_t216 **)(Mesh_t216 **)SZArrayLdElema(L_1, L_3)), /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0027;
		}
	}
	{
		return 0;
	}

IL_0027:
	{
		int32_t L_5 = V_0;
		V_0 = ((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_002b:
	{
		int32_t L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Triangles_t130_il2cpp_TypeInfo_var);
		MeshU5BU5D_t113* L_7 = ((Triangles_t130_StaticFields*)Triangles_t130_il2cpp_TypeInfo_var->static_fields)->___meshes_0;
		NullCheck(L_7);
		if ((((int32_t)L_6) < ((int32_t)(((int32_t)(((Array_t *)L_7)->max_length))))))
		{
			goto IL_0013;
		}
	}
	{
		return 1;
	}
}
// System.Void UnityStandardAssets.ImageEffects.Triangles::Cleanup()
extern TypeInfo* Triangles_t130_il2cpp_TypeInfo_var;
extern "C" void Triangles_Cleanup_m384 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Triangles_t130_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(63);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Triangles_t130_il2cpp_TypeInfo_var);
		MeshU5BU5D_t113* L_0 = ((Triangles_t130_StaticFields*)Triangles_t130_il2cpp_TypeInfo_var->static_fields)->___meshes_0;
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		V_0 = 0;
		goto IL_003c;
	}

IL_0012:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Triangles_t130_il2cpp_TypeInfo_var);
		MeshU5BU5D_t113* L_1 = ((Triangles_t130_StaticFields*)Triangles_t130_il2cpp_TypeInfo_var->static_fields)->___meshes_0;
		int32_t L_2 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		bool L_4 = Object_op_Inequality_m623(NULL /*static, unused*/, (Object_t164 *)NULL, (*(Mesh_t216 **)(Mesh_t216 **)SZArrayLdElema(L_1, L_3)), /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0038;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Triangles_t130_il2cpp_TypeInfo_var);
		MeshU5BU5D_t113* L_5 = ((Triangles_t130_StaticFields*)Triangles_t130_il2cpp_TypeInfo_var->static_fields)->___meshes_0;
		int32_t L_6 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		Object_DestroyImmediate_m761(NULL /*static, unused*/, (*(Mesh_t216 **)(Mesh_t216 **)SZArrayLdElema(L_5, L_7)), /*hidden argument*/NULL);
		MeshU5BU5D_t113* L_8 = ((Triangles_t130_StaticFields*)Triangles_t130_il2cpp_TypeInfo_var->static_fields)->___meshes_0;
		int32_t L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		ArrayElementTypeCheck (L_8, NULL);
		*((Mesh_t216 **)(Mesh_t216 **)SZArrayLdElema(L_8, L_9)) = (Mesh_t216 *)NULL;
	}

IL_0038:
	{
		int32_t L_10 = V_0;
		V_0 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_003c:
	{
		int32_t L_11 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Triangles_t130_il2cpp_TypeInfo_var);
		MeshU5BU5D_t113* L_12 = ((Triangles_t130_StaticFields*)Triangles_t130_il2cpp_TypeInfo_var->static_fields)->___meshes_0;
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)(((Array_t *)L_12)->max_length))))))
		{
			goto IL_0012;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Triangles_t130_il2cpp_TypeInfo_var);
		((Triangles_t130_StaticFields*)Triangles_t130_il2cpp_TypeInfo_var->static_fields)->___meshes_0 = (MeshU5BU5D_t113*)NULL;
		return;
	}
}
// UnityEngine.Mesh[] UnityStandardAssets.ImageEffects.Triangles::GetMeshes(System.Int32,System.Int32)
extern TypeInfo* Triangles_t130_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t218_il2cpp_TypeInfo_var;
extern TypeInfo* MeshU5BU5D_t113_il2cpp_TypeInfo_var;
extern "C" MeshU5BU5D_t113* Triangles_GetMeshes_m385 (Object_t * __this /* static, unused */, int32_t ___totalWidth, int32_t ___totalHeight, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Triangles_t130_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(63);
		Mathf_t218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		MeshU5BU5D_t113_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(59);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Triangles_t130_il2cpp_TypeInfo_var);
		bool L_0 = Triangles_HasMeshes_m383(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Triangles_t130_il2cpp_TypeInfo_var);
		int32_t L_1 = ((Triangles_t130_StaticFields*)Triangles_t130_il2cpp_TypeInfo_var->static_fields)->___currentTris_1;
		int32_t L_2 = ___totalWidth;
		int32_t L_3 = ___totalHeight;
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)((int32_t)L_2*(int32_t)L_3))))))
		{
			goto IL_001d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Triangles_t130_il2cpp_TypeInfo_var);
		MeshU5BU5D_t113* L_4 = ((Triangles_t130_StaticFields*)Triangles_t130_il2cpp_TypeInfo_var->static_fields)->___meshes_0;
		return L_4;
	}

IL_001d:
	{
		V_0 = ((int32_t)21666);
		int32_t L_5 = ___totalWidth;
		int32_t L_6 = ___totalHeight;
		V_1 = ((int32_t)((int32_t)L_5*(int32_t)L_6));
		int32_t L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Triangles_t130_il2cpp_TypeInfo_var);
		((Triangles_t130_StaticFields*)Triangles_t130_il2cpp_TypeInfo_var->static_fields)->___currentTris_1 = L_7;
		int32_t L_8 = V_1;
		int32_t L_9 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		int32_t L_10 = Mathf_CeilToInt_m867(NULL /*static, unused*/, ((float)((float)((float)((float)(1.0f)*(float)(((float)L_8))))/(float)((float)((float)(1.0f)*(float)(((float)L_9)))))), /*hidden argument*/NULL);
		V_2 = L_10;
		int32_t L_11 = V_2;
		((Triangles_t130_StaticFields*)Triangles_t130_il2cpp_TypeInfo_var->static_fields)->___meshes_0 = ((MeshU5BU5D_t113*)SZArrayNew(MeshU5BU5D_t113_il2cpp_TypeInfo_var, L_11));
		V_3 = 0;
		V_4 = 0;
		V_3 = 0;
		goto IL_0089;
	}

IL_005b:
	{
		int32_t L_12 = V_1;
		int32_t L_13 = V_3;
		int32_t L_14 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		int32_t L_15 = Mathf_Clamp_m712(NULL /*static, unused*/, ((int32_t)((int32_t)L_12-(int32_t)L_13)), 0, L_14, /*hidden argument*/NULL);
		int32_t L_16 = Mathf_FloorToInt_m815(NULL /*static, unused*/, (((float)L_15)), /*hidden argument*/NULL);
		V_5 = L_16;
		IL2CPP_RUNTIME_CLASS_INIT(Triangles_t130_il2cpp_TypeInfo_var);
		MeshU5BU5D_t113* L_17 = ((Triangles_t130_StaticFields*)Triangles_t130_il2cpp_TypeInfo_var->static_fields)->___meshes_0;
		int32_t L_18 = V_4;
		int32_t L_19 = V_5;
		int32_t L_20 = V_3;
		int32_t L_21 = ___totalWidth;
		int32_t L_22 = ___totalHeight;
		Mesh_t216 * L_23 = Triangles_GetMesh_m386(NULL /*static, unused*/, L_19, L_20, L_21, L_22, /*hidden argument*/NULL);
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		ArrayElementTypeCheck (L_17, L_23);
		*((Mesh_t216 **)(Mesh_t216 **)SZArrayLdElema(L_17, L_18)) = (Mesh_t216 *)L_23;
		int32_t L_24 = V_4;
		V_4 = ((int32_t)((int32_t)L_24+(int32_t)1));
		int32_t L_25 = V_3;
		int32_t L_26 = V_0;
		V_3 = ((int32_t)((int32_t)L_25+(int32_t)L_26));
	}

IL_0089:
	{
		int32_t L_27 = V_3;
		int32_t L_28 = V_1;
		if ((((int32_t)L_27) < ((int32_t)L_28)))
		{
			goto IL_005b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Triangles_t130_il2cpp_TypeInfo_var);
		MeshU5BU5D_t113* L_29 = ((Triangles_t130_StaticFields*)Triangles_t130_il2cpp_TypeInfo_var->static_fields)->___meshes_0;
		return L_29;
	}
}
// UnityEngine.Mesh UnityStandardAssets.ImageEffects.Triangles::GetMesh(System.Int32,System.Int32,System.Int32,System.Int32)
extern TypeInfo* Mesh_t216_il2cpp_TypeInfo_var;
extern TypeInfo* Vector3U5BU5D_t210_il2cpp_TypeInfo_var;
extern TypeInfo* Vector2U5BU5D_t237_il2cpp_TypeInfo_var;
extern TypeInfo* Int32U5BU5D_t242_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t218_il2cpp_TypeInfo_var;
extern "C" Mesh_t216 * Triangles_GetMesh_m386 (Object_t * __this /* static, unused */, int32_t ___triCount, int32_t ___triOffset, int32_t ___totalWidth, int32_t ___totalHeight, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mesh_t216_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(60);
		Vector3U5BU5D_t210_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(61);
		Vector2U5BU5D_t237_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(38);
		Int32U5BU5D_t242_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(53);
		Mathf_t218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	Mesh_t216 * V_0 = {0};
	Vector3U5BU5D_t210* V_1 = {0};
	Vector2U5BU5D_t237* V_2 = {0};
	Vector2U5BU5D_t237* V_3 = {0};
	Int32U5BU5D_t242* V_4 = {0};
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	float V_8 = 0.0f;
	float V_9 = 0.0f;
	Vector3_t4  V_10 = {0};
	{
		Mesh_t216 * L_0 = (Mesh_t216 *)il2cpp_codegen_object_new (Mesh_t216_il2cpp_TypeInfo_var);
		Mesh__ctor_m868(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Mesh_t216 * L_1 = V_0;
		NullCheck(L_1);
		Object_set_hideFlags_m764(L_1, ((int32_t)52), /*hidden argument*/NULL);
		int32_t L_2 = ___triCount;
		V_1 = ((Vector3U5BU5D_t210*)SZArrayNew(Vector3U5BU5D_t210_il2cpp_TypeInfo_var, ((int32_t)((int32_t)L_2*(int32_t)3))));
		int32_t L_3 = ___triCount;
		V_2 = ((Vector2U5BU5D_t237*)SZArrayNew(Vector2U5BU5D_t237_il2cpp_TypeInfo_var, ((int32_t)((int32_t)L_3*(int32_t)3))));
		int32_t L_4 = ___triCount;
		V_3 = ((Vector2U5BU5D_t237*)SZArrayNew(Vector2U5BU5D_t237_il2cpp_TypeInfo_var, ((int32_t)((int32_t)L_4*(int32_t)3))));
		int32_t L_5 = ___triCount;
		V_4 = ((Int32U5BU5D_t242*)SZArrayNew(Int32U5BU5D_t242_il2cpp_TypeInfo_var, ((int32_t)((int32_t)L_5*(int32_t)3))));
		V_5 = 0;
		goto IL_017f;
	}

IL_003b:
	{
		int32_t L_6 = V_5;
		V_6 = ((int32_t)((int32_t)L_6*(int32_t)3));
		int32_t L_7 = ___triOffset;
		int32_t L_8 = V_5;
		V_7 = ((int32_t)((int32_t)L_7+(int32_t)L_8));
		int32_t L_9 = V_7;
		int32_t L_10 = ___totalWidth;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_11 = floorf((((float)((int32_t)((int32_t)L_9%(int32_t)L_10)))));
		int32_t L_12 = ___totalWidth;
		V_8 = ((float)((float)L_11/(float)(((float)L_12))));
		int32_t L_13 = V_7;
		int32_t L_14 = ___totalWidth;
		float L_15 = floorf((((float)((int32_t)((int32_t)L_13/(int32_t)L_14)))));
		int32_t L_16 = ___totalHeight;
		V_9 = ((float)((float)L_15/(float)(((float)L_16))));
		float L_17 = V_8;
		float L_18 = V_9;
		Vector3__ctor_m612((&V_10), ((float)((float)((float)((float)L_17*(float)(2.0f)))-(float)(1.0f))), ((float)((float)((float)((float)L_18*(float)(2.0f)))-(float)(1.0f))), (1.0f), /*hidden argument*/NULL);
		Vector3U5BU5D_t210* L_19 = V_1;
		int32_t L_20 = V_6;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, L_20);
		Vector3_t4  L_21 = V_10;
		*((Vector3_t4 *)(Vector3_t4 *)SZArrayLdElema(L_19, L_20)) = L_21;
		Vector3U5BU5D_t210* L_22 = V_1;
		int32_t L_23 = V_6;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, ((int32_t)((int32_t)L_23+(int32_t)1)));
		Vector3_t4  L_24 = V_10;
		*((Vector3_t4 *)(Vector3_t4 *)SZArrayLdElema(L_22, ((int32_t)((int32_t)L_23+(int32_t)1)))) = L_24;
		Vector3U5BU5D_t210* L_25 = V_1;
		int32_t L_26 = V_6;
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, ((int32_t)((int32_t)L_26+(int32_t)2)));
		Vector3_t4  L_27 = V_10;
		*((Vector3_t4 *)(Vector3_t4 *)SZArrayLdElema(L_25, ((int32_t)((int32_t)L_26+(int32_t)2)))) = L_27;
		Vector2U5BU5D_t237* L_28 = V_2;
		int32_t L_29 = V_6;
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, L_29);
		Vector2_t6  L_30 = {0};
		Vector2__ctor_m630(&L_30, (0.0f), (0.0f), /*hidden argument*/NULL);
		*((Vector2_t6 *)(Vector2_t6 *)SZArrayLdElema(L_28, L_29)) = L_30;
		Vector2U5BU5D_t237* L_31 = V_2;
		int32_t L_32 = V_6;
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, ((int32_t)((int32_t)L_32+(int32_t)1)));
		Vector2_t6  L_33 = {0};
		Vector2__ctor_m630(&L_33, (1.0f), (0.0f), /*hidden argument*/NULL);
		*((Vector2_t6 *)(Vector2_t6 *)SZArrayLdElema(L_31, ((int32_t)((int32_t)L_32+(int32_t)1)))) = L_33;
		Vector2U5BU5D_t237* L_34 = V_2;
		int32_t L_35 = V_6;
		NullCheck(L_34);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_34, ((int32_t)((int32_t)L_35+(int32_t)2)));
		Vector2_t6  L_36 = {0};
		Vector2__ctor_m630(&L_36, (0.0f), (1.0f), /*hidden argument*/NULL);
		*((Vector2_t6 *)(Vector2_t6 *)SZArrayLdElema(L_34, ((int32_t)((int32_t)L_35+(int32_t)2)))) = L_36;
		Vector2U5BU5D_t237* L_37 = V_3;
		int32_t L_38 = V_6;
		NullCheck(L_37);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_37, L_38);
		float L_39 = V_8;
		float L_40 = V_9;
		Vector2_t6  L_41 = {0};
		Vector2__ctor_m630(&L_41, L_39, L_40, /*hidden argument*/NULL);
		*((Vector2_t6 *)(Vector2_t6 *)SZArrayLdElema(L_37, L_38)) = L_41;
		Vector2U5BU5D_t237* L_42 = V_3;
		int32_t L_43 = V_6;
		NullCheck(L_42);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_42, ((int32_t)((int32_t)L_43+(int32_t)1)));
		float L_44 = V_8;
		float L_45 = V_9;
		Vector2_t6  L_46 = {0};
		Vector2__ctor_m630(&L_46, L_44, L_45, /*hidden argument*/NULL);
		*((Vector2_t6 *)(Vector2_t6 *)SZArrayLdElema(L_42, ((int32_t)((int32_t)L_43+(int32_t)1)))) = L_46;
		Vector2U5BU5D_t237* L_47 = V_3;
		int32_t L_48 = V_6;
		NullCheck(L_47);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_47, ((int32_t)((int32_t)L_48+(int32_t)2)));
		float L_49 = V_8;
		float L_50 = V_9;
		Vector2_t6  L_51 = {0};
		Vector2__ctor_m630(&L_51, L_49, L_50, /*hidden argument*/NULL);
		*((Vector2_t6 *)(Vector2_t6 *)SZArrayLdElema(L_47, ((int32_t)((int32_t)L_48+(int32_t)2)))) = L_51;
		Int32U5BU5D_t242* L_52 = V_4;
		int32_t L_53 = V_6;
		int32_t L_54 = V_6;
		NullCheck(L_52);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_52, L_53);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_52, L_53)) = (int32_t)L_54;
		Int32U5BU5D_t242* L_55 = V_4;
		int32_t L_56 = V_6;
		int32_t L_57 = V_6;
		NullCheck(L_55);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_55, ((int32_t)((int32_t)L_56+(int32_t)1)));
		*((int32_t*)(int32_t*)SZArrayLdElema(L_55, ((int32_t)((int32_t)L_56+(int32_t)1)))) = (int32_t)((int32_t)((int32_t)L_57+(int32_t)1));
		Int32U5BU5D_t242* L_58 = V_4;
		int32_t L_59 = V_6;
		int32_t L_60 = V_6;
		NullCheck(L_58);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_58, ((int32_t)((int32_t)L_59+(int32_t)2)));
		*((int32_t*)(int32_t*)SZArrayLdElema(L_58, ((int32_t)((int32_t)L_59+(int32_t)2)))) = (int32_t)((int32_t)((int32_t)L_60+(int32_t)2));
		int32_t L_61 = V_5;
		V_5 = ((int32_t)((int32_t)L_61+(int32_t)1));
	}

IL_017f:
	{
		int32_t L_62 = V_5;
		int32_t L_63 = ___triCount;
		if ((((int32_t)L_62) < ((int32_t)L_63)))
		{
			goto IL_003b;
		}
	}
	{
		Mesh_t216 * L_64 = V_0;
		Vector3U5BU5D_t210* L_65 = V_1;
		NullCheck(L_64);
		Mesh_set_vertices_m869(L_64, L_65, /*hidden argument*/NULL);
		Mesh_t216 * L_66 = V_0;
		Int32U5BU5D_t242* L_67 = V_4;
		NullCheck(L_66);
		Mesh_set_triangles_m870(L_66, L_67, /*hidden argument*/NULL);
		Mesh_t216 * L_68 = V_0;
		Vector2U5BU5D_t237* L_69 = V_2;
		NullCheck(L_68);
		Mesh_set_uv_m871(L_68, L_69, /*hidden argument*/NULL);
		Mesh_t216 * L_70 = V_0;
		Vector2U5BU5D_t237* L_71 = V_3;
		NullCheck(L_70);
		Mesh_set_uv2_m872(L_70, L_71, /*hidden argument*/NULL);
		Mesh_t216 * L_72 = V_0;
		return L_72;
	}
}
// UnityStandardAssets.ImageEffects.Twirl
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_64.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.ImageEffects.Twirl
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_64MethodDeclarations.h"

// UnityEngine.RenderTexture
#include "UnityEngine_UnityEngine_RenderTexture.h"
// UnityEngine.Material
#include "UnityEngine_UnityEngine_Material.h"
// UnityStandardAssets.ImageEffects.ImageEffectBase
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_25MethodDeclarations.h"
// UnityStandardAssets.ImageEffects.ImageEffects
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_42MethodDeclarations.h"


// System.Void UnityStandardAssets.ImageEffects.Twirl::.ctor()
extern "C" void Twirl__ctor_m387 (Twirl_t131 * __this, const MethodInfo* method)
{
	{
		Vector2_t6  L_0 = {0};
		Vector2__ctor_m630(&L_0, (0.3f), (0.3f), /*hidden argument*/NULL);
		__this->___radius_4 = L_0;
		__this->___angle_5 = (50.0f);
		Vector2_t6  L_1 = {0};
		Vector2__ctor_m630(&L_1, (0.5f), (0.5f), /*hidden argument*/NULL);
		__this->___center_6 = L_1;
		ImageEffectBase__ctor_m304(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.Twirl::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void Twirl_OnRenderImage_m388 (Twirl_t131 * __this, RenderTexture_t101 * ___source, RenderTexture_t101 * ___destination, const MethodInfo* method)
{
	{
		Material_t55 * L_0 = ImageEffectBase_get_material_m306(__this, /*hidden argument*/NULL);
		RenderTexture_t101 * L_1 = ___source;
		RenderTexture_t101 * L_2 = ___destination;
		float L_3 = (__this->___angle_5);
		Vector2_t6  L_4 = (__this->___center_6);
		Vector2_t6  L_5 = (__this->___radius_4);
		ImageEffects_RenderDistortion_m309(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration/AberrationMode
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_65.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration/AberrationMode
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_65MethodDeclarations.h"



// UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_66.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_66MethodDeclarations.h"

// UnityEngine.Shader
#include "UnityEngine_UnityEngine_Shader.h"
// UnityStandardAssets.ImageEffects.PostEffectsBase
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_1.h"
// UnityEngine.Texture
#include "UnityEngine_UnityEngine_Texture.h"
// UnityEngine.RenderTextureFormat
#include "UnityEngine_UnityEngine_RenderTextureFormat.h"
// System.String
#include "mscorlib_System_String.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// UnityEngine.Mathf
#include "UnityEngine_UnityEngine_Mathf.h"
// UnityEngine.TextureWrapMode
#include "UnityEngine_UnityEngine_TextureWrapMode.h"
// UnityStandardAssets.ImageEffects.PostEffectsBase
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_1MethodDeclarations.h"
// UnityEngine.Graphics
#include "UnityEngine_UnityEngine_GraphicsMethodDeclarations.h"
// UnityEngine.RenderTexture
#include "UnityEngine_UnityEngine_RenderTextureMethodDeclarations.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4MethodDeclarations.h"
// UnityEngine.Material
#include "UnityEngine_UnityEngine_MaterialMethodDeclarations.h"
// UnityEngine.Texture
#include "UnityEngine_UnityEngine_TextureMethodDeclarations.h"


// System.Void UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration::.ctor()
extern "C" void VignetteAndChromaticAberration__ctor_m389 (VignetteAndChromaticAberration_t133 * __this, const MethodInfo* method)
{
	{
		__this->___intensity_6 = (0.375f);
		__this->___chromaticAberration_7 = (0.2f);
		__this->___axialAberration_8 = (0.5f);
		__this->___blurSpread_10 = (0.75f);
		__this->___luminanceDependency_11 = (0.25f);
		__this->___blurDistance_12 = (2.5f);
		PostEffectsBase__ctor_m327(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration::CheckResources()
extern "C" bool VignetteAndChromaticAberration_CheckResources_m390 (VignetteAndChromaticAberration_t133 * __this, const MethodInfo* method)
{
	{
		PostEffectsBase_CheckSupport_m334(__this, 0, /*hidden argument*/NULL);
		Shader_t54 * L_0 = (__this->___vignetteShader_13);
		Material_t55 * L_1 = (__this->___m_VignetteMaterial_16);
		Material_t55 * L_2 = PostEffectsBase_CheckShaderAndCreateMaterial_m328(__this, L_0, L_1, /*hidden argument*/NULL);
		__this->___m_VignetteMaterial_16 = L_2;
		Shader_t54 * L_3 = (__this->___separableBlurShader_14);
		Material_t55 * L_4 = (__this->___m_SeparableBlurMaterial_17);
		Material_t55 * L_5 = PostEffectsBase_CheckShaderAndCreateMaterial_m328(__this, L_3, L_4, /*hidden argument*/NULL);
		__this->___m_SeparableBlurMaterial_17 = L_5;
		Shader_t54 * L_6 = (__this->___chromAberrationShader_15);
		Material_t55 * L_7 = (__this->___m_ChromAberrationMaterial_18);
		Material_t55 * L_8 = PostEffectsBase_CheckShaderAndCreateMaterial_m328(__this, L_6, L_7, /*hidden argument*/NULL);
		__this->___m_ChromAberrationMaterial_18 = L_8;
		bool L_9 = (((PostEffectsBase_t57 *)__this)->___isSupported_4);
		if (L_9)
		{
			goto IL_0061;
		}
	}
	{
		PostEffectsBase_ReportAutoDisable_m337(__this, /*hidden argument*/NULL);
	}

IL_0061:
	{
		bool L_10 = (((PostEffectsBase_t57 *)__this)->___isSupported_4);
		return L_10;
	}
}
// System.Void UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern TypeInfo* Mathf_t218_il2cpp_TypeInfo_var;
extern "C" void VignetteAndChromaticAberration_OnRenderImage_m391 (VignetteAndChromaticAberration_t133 * __this, RenderTexture_t101 * ___source, RenderTexture_t101 * ___destination, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	RenderTexture_t101 * V_5 = {0};
	RenderTexture_t101 * V_6 = {0};
	int32_t V_7 = 0;
	RenderTexture_t101 * V_8 = {0};
	int32_t G_B5_0 = 0;
	RenderTexture_t101 * G_B17_0 = {0};
	Material_t55 * G_B19_0 = {0};
	RenderTexture_t101 * G_B19_1 = {0};
	RenderTexture_t101 * G_B19_2 = {0};
	Material_t55 * G_B18_0 = {0};
	RenderTexture_t101 * G_B18_1 = {0};
	RenderTexture_t101 * G_B18_2 = {0};
	int32_t G_B20_0 = 0;
	Material_t55 * G_B20_1 = {0};
	RenderTexture_t101 * G_B20_2 = {0};
	RenderTexture_t101 * G_B20_3 = {0};
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(4 /* System.Boolean UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration::CheckResources() */, __this);
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		RenderTexture_t101 * L_1 = ___source;
		RenderTexture_t101 * L_2 = ___destination;
		Graphics_Blit_m737(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return;
	}

IL_0013:
	{
		RenderTexture_t101 * L_3 = ___source;
		NullCheck(L_3);
		int32_t L_4 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_3);
		V_0 = L_4;
		RenderTexture_t101 * L_5 = ___source;
		NullCheck(L_5);
		int32_t L_6 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_5);
		V_1 = L_6;
		float L_7 = (__this->___blur_9);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_8 = fabsf(L_7);
		if ((((float)L_8) > ((float)(0.0f))))
		{
			goto IL_004a;
		}
	}
	{
		float L_9 = (__this->___intensity_6);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_10 = fabsf(L_9);
		G_B5_0 = ((((float)L_10) > ((float)(0.0f)))? 1 : 0);
		goto IL_004b;
	}

IL_004a:
	{
		G_B5_0 = 1;
	}

IL_004b:
	{
		V_2 = G_B5_0;
		int32_t L_11 = V_0;
		int32_t L_12 = V_1;
		V_3 = ((float)((float)((float)((float)(1.0f)*(float)(((float)L_11))))/(float)((float)((float)(1.0f)*(float)(((float)L_12))))));
		V_5 = (RenderTexture_t101 *)NULL;
		V_6 = (RenderTexture_t101 *)NULL;
		bool L_13 = V_2;
		if (!L_13)
		{
			goto IL_01cb;
		}
	}
	{
		int32_t L_14 = V_0;
		int32_t L_15 = V_1;
		RenderTexture_t101 * L_16 = ___source;
		NullCheck(L_16);
		int32_t L_17 = RenderTexture_get_format_m747(L_16, /*hidden argument*/NULL);
		RenderTexture_t101 * L_18 = RenderTexture_GetTemporary_m749(NULL /*static, unused*/, L_14, L_15, 0, L_17, /*hidden argument*/NULL);
		V_5 = L_18;
		float L_19 = (__this->___blur_9);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_20 = fabsf(L_19);
		if ((!(((float)L_20) > ((float)(0.0f)))))
		{
			goto IL_017e;
		}
	}
	{
		int32_t L_21 = V_0;
		int32_t L_22 = V_1;
		RenderTexture_t101 * L_23 = ___source;
		NullCheck(L_23);
		int32_t L_24 = RenderTexture_get_format_m747(L_23, /*hidden argument*/NULL);
		RenderTexture_t101 * L_25 = RenderTexture_GetTemporary_m749(NULL /*static, unused*/, ((int32_t)((int32_t)L_21/(int32_t)2)), ((int32_t)((int32_t)L_22/(int32_t)2)), 0, L_24, /*hidden argument*/NULL);
		V_6 = L_25;
		RenderTexture_t101 * L_26 = ___source;
		RenderTexture_t101 * L_27 = V_6;
		Material_t55 * L_28 = (__this->___m_ChromAberrationMaterial_18);
		Graphics_Blit_m742(NULL /*static, unused*/, L_26, L_27, L_28, 0, /*hidden argument*/NULL);
		V_7 = 0;
		goto IL_0176;
	}

IL_00ba:
	{
		Material_t55 * L_29 = (__this->___m_SeparableBlurMaterial_17);
		float L_30 = (__this->___blurSpread_10);
		Vector4_t236  L_31 = {0};
		Vector4__ctor_m751(&L_31, (0.0f), ((float)((float)L_30*(float)(0.001953125f))), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_29);
		Material_SetVector_m752(L_29, (String_t*) &_stringLiteral34, L_31, /*hidden argument*/NULL);
		int32_t L_32 = V_0;
		int32_t L_33 = V_1;
		RenderTexture_t101 * L_34 = ___source;
		NullCheck(L_34);
		int32_t L_35 = RenderTexture_get_format_m747(L_34, /*hidden argument*/NULL);
		RenderTexture_t101 * L_36 = RenderTexture_GetTemporary_m749(NULL /*static, unused*/, ((int32_t)((int32_t)L_32/(int32_t)2)), ((int32_t)((int32_t)L_33/(int32_t)2)), 0, L_35, /*hidden argument*/NULL);
		V_8 = L_36;
		RenderTexture_t101 * L_37 = V_6;
		RenderTexture_t101 * L_38 = V_8;
		Material_t55 * L_39 = (__this->___m_SeparableBlurMaterial_17);
		Graphics_Blit_m739(NULL /*static, unused*/, L_37, L_38, L_39, /*hidden argument*/NULL);
		RenderTexture_t101 * L_40 = V_6;
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_40, /*hidden argument*/NULL);
		Material_t55 * L_41 = (__this->___m_SeparableBlurMaterial_17);
		float L_42 = (__this->___blurSpread_10);
		float L_43 = V_3;
		Vector4_t236  L_44 = {0};
		Vector4__ctor_m751(&L_44, ((float)((float)((float)((float)L_42*(float)(0.001953125f)))/(float)L_43)), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_41);
		Material_SetVector_m752(L_41, (String_t*) &_stringLiteral34, L_44, /*hidden argument*/NULL);
		int32_t L_45 = V_0;
		int32_t L_46 = V_1;
		RenderTexture_t101 * L_47 = ___source;
		NullCheck(L_47);
		int32_t L_48 = RenderTexture_get_format_m747(L_47, /*hidden argument*/NULL);
		RenderTexture_t101 * L_49 = RenderTexture_GetTemporary_m749(NULL /*static, unused*/, ((int32_t)((int32_t)L_45/(int32_t)2)), ((int32_t)((int32_t)L_46/(int32_t)2)), 0, L_48, /*hidden argument*/NULL);
		V_6 = L_49;
		RenderTexture_t101 * L_50 = V_8;
		RenderTexture_t101 * L_51 = V_6;
		Material_t55 * L_52 = (__this->___m_SeparableBlurMaterial_17);
		Graphics_Blit_m739(NULL /*static, unused*/, L_50, L_51, L_52, /*hidden argument*/NULL);
		RenderTexture_t101 * L_53 = V_8;
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		int32_t L_54 = V_7;
		V_7 = ((int32_t)((int32_t)L_54+(int32_t)1));
	}

IL_0176:
	{
		int32_t L_55 = V_7;
		if ((((int32_t)L_55) < ((int32_t)2)))
		{
			goto IL_00ba;
		}
	}

IL_017e:
	{
		Material_t55 * L_56 = (__this->___m_VignetteMaterial_16);
		float L_57 = (__this->___intensity_6);
		NullCheck(L_56);
		Material_SetFloat_m738(L_56, (String_t*) &_stringLiteral28, L_57, /*hidden argument*/NULL);
		Material_t55 * L_58 = (__this->___m_VignetteMaterial_16);
		float L_59 = (__this->___blur_9);
		NullCheck(L_58);
		Material_SetFloat_m738(L_58, (String_t*) &_stringLiteral168, L_59, /*hidden argument*/NULL);
		Material_t55 * L_60 = (__this->___m_VignetteMaterial_16);
		RenderTexture_t101 * L_61 = V_6;
		NullCheck(L_60);
		Material_SetTexture_m759(L_60, (String_t*) &_stringLiteral169, L_61, /*hidden argument*/NULL);
		RenderTexture_t101 * L_62 = ___source;
		RenderTexture_t101 * L_63 = V_5;
		Material_t55 * L_64 = (__this->___m_VignetteMaterial_16);
		Graphics_Blit_m742(NULL /*static, unused*/, L_62, L_63, L_64, 0, /*hidden argument*/NULL);
	}

IL_01cb:
	{
		Material_t55 * L_65 = (__this->___m_ChromAberrationMaterial_18);
		float L_66 = (__this->___chromaticAberration_7);
		NullCheck(L_65);
		Material_SetFloat_m738(L_65, (String_t*) &_stringLiteral170, L_66, /*hidden argument*/NULL);
		Material_t55 * L_67 = (__this->___m_ChromAberrationMaterial_18);
		float L_68 = (__this->___axialAberration_8);
		NullCheck(L_67);
		Material_SetFloat_m738(L_67, (String_t*) &_stringLiteral171, L_68, /*hidden argument*/NULL);
		Material_t55 * L_69 = (__this->___m_ChromAberrationMaterial_18);
		float L_70 = (__this->___blurDistance_12);
		float L_71 = (__this->___blurDistance_12);
		Vector2_t6  L_72 = {0};
		Vector2__ctor_m630(&L_72, ((-L_70)), L_71, /*hidden argument*/NULL);
		Vector4_t236  L_73 = Vector4_op_Implicit_m875(NULL /*static, unused*/, L_72, /*hidden argument*/NULL);
		NullCheck(L_69);
		Material_SetVector_m752(L_69, (String_t*) &_stringLiteral172, L_73, /*hidden argument*/NULL);
		Material_t55 * L_74 = (__this->___m_ChromAberrationMaterial_18);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_75 = ((Mathf_t218_StaticFields*)Mathf_t218_il2cpp_TypeInfo_var->static_fields)->___Epsilon_0;
		float L_76 = (__this->___luminanceDependency_11);
		float L_77 = Mathf_Max_m782(NULL /*static, unused*/, L_75, L_76, /*hidden argument*/NULL);
		NullCheck(L_74);
		Material_SetFloat_m738(L_74, (String_t*) &_stringLiteral173, ((float)((float)(1.0f)/(float)L_77)), /*hidden argument*/NULL);
		bool L_78 = V_2;
		if (!L_78)
		{
			goto IL_0257;
		}
	}
	{
		RenderTexture_t101 * L_79 = V_5;
		NullCheck(L_79);
		Texture_set_wrapMode_m784(L_79, 1, /*hidden argument*/NULL);
		goto IL_025e;
	}

IL_0257:
	{
		RenderTexture_t101 * L_80 = ___source;
		NullCheck(L_80);
		Texture_set_wrapMode_m784(L_80, 1, /*hidden argument*/NULL);
	}

IL_025e:
	{
		bool L_81 = V_2;
		if (!L_81)
		{
			goto IL_026b;
		}
	}
	{
		RenderTexture_t101 * L_82 = V_5;
		G_B17_0 = L_82;
		goto IL_026c;
	}

IL_026b:
	{
		RenderTexture_t101 * L_83 = ___source;
		G_B17_0 = L_83;
	}

IL_026c:
	{
		RenderTexture_t101 * L_84 = ___destination;
		Material_t55 * L_85 = (__this->___m_ChromAberrationMaterial_18);
		int32_t L_86 = (__this->___mode_5);
		G_B18_0 = L_85;
		G_B18_1 = L_84;
		G_B18_2 = G_B17_0;
		if ((!(((uint32_t)L_86) == ((uint32_t)1))))
		{
			G_B19_0 = L_85;
			G_B19_1 = L_84;
			G_B19_2 = G_B17_0;
			goto IL_0285;
		}
	}
	{
		G_B20_0 = 2;
		G_B20_1 = G_B18_0;
		G_B20_2 = G_B18_1;
		G_B20_3 = G_B18_2;
		goto IL_0286;
	}

IL_0285:
	{
		G_B20_0 = 1;
		G_B20_1 = G_B19_0;
		G_B20_2 = G_B19_1;
		G_B20_3 = G_B19_2;
	}

IL_0286:
	{
		Graphics_Blit_m742(NULL /*static, unused*/, G_B20_3, G_B20_2, G_B20_1, G_B20_0, /*hidden argument*/NULL);
		RenderTexture_t101 * L_87 = V_5;
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_87, /*hidden argument*/NULL);
		RenderTexture_t101 * L_88 = V_6;
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_88, /*hidden argument*/NULL);
		return;
	}
}
// UnityStandardAssets.ImageEffects.Vortex
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_67.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.ImageEffects.Vortex
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_67MethodDeclarations.h"



// System.Void UnityStandardAssets.ImageEffects.Vortex::.ctor()
extern "C" void Vortex__ctor_m392 (Vortex_t134 * __this, const MethodInfo* method)
{
	{
		Vector2_t6  L_0 = {0};
		Vector2__ctor_m630(&L_0, (0.4f), (0.4f), /*hidden argument*/NULL);
		__this->___radius_4 = L_0;
		__this->___angle_5 = (50.0f);
		Vector2_t6  L_1 = {0};
		Vector2__ctor_m630(&L_1, (0.5f), (0.5f), /*hidden argument*/NULL);
		__this->___center_6 = L_1;
		ImageEffectBase__ctor_m304(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.Vortex::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void Vortex_OnRenderImage_m393 (Vortex_t134 * __this, RenderTexture_t101 * ___source, RenderTexture_t101 * ___destination, const MethodInfo* method)
{
	{
		Material_t55 * L_0 = ImageEffectBase_get_material_m306(__this, /*hidden argument*/NULL);
		RenderTexture_t101 * L_1 = ___source;
		RenderTexture_t101 * L_2 = ___destination;
		float L_3 = (__this->___angle_5);
		Vector2_t6  L_4 = (__this->___center_6);
		Vector2_t6  L_5 = (__this->___radius_4);
		ImageEffects_RenderDistortion_m309(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// UnityStandardAssets.Effects.AfterburnerPhysicsForce
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Effects_Af.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.Effects.AfterburnerPhysicsForce
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Effects_AfMethodDeclarations.h"

// UnityEngine.Collider
#include "UnityEngine_UnityEngine_Collider.h"
// UnityEngine.SphereCollider
#include "UnityEngine_UnityEngine_SphereCollider.h"
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_Transform.h"
// UnityEngine.Rigidbody
#include "UnityEngine_UnityEngine_Rigidbody.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviourMethodDeclarations.h"
// UnityEngine.Component
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_TransformMethodDeclarations.h"
// UnityEngine.SphereCollider
#include "UnityEngine_UnityEngine_SphereColliderMethodDeclarations.h"
// UnityEngine.Physics
#include "UnityEngine_UnityEngine_PhysicsMethodDeclarations.h"
// UnityEngine.Collider
#include "UnityEngine_UnityEngine_ColliderMethodDeclarations.h"
// UnityEngine.Rigidbody
#include "UnityEngine_UnityEngine_RigidbodyMethodDeclarations.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_ColorMethodDeclarations.h"
// UnityEngine.Gizmos
#include "UnityEngine_UnityEngine_GizmosMethodDeclarations.h"
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_QuaternionMethodDeclarations.h"
struct Component_t219;
struct Collider_t138;
// UnityEngine.Component
#include "UnityEngine_UnityEngine_Component.h"
struct Component_t219;
struct Object_t;
// Declaration !!0 UnityEngine.Component::GetComponent<System.Object>()
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C" Object_t * Component_GetComponent_TisObject_t_m614_gshared (Component_t219 * __this, const MethodInfo* method);
#define Component_GetComponent_TisObject_t_m614(__this, method) (( Object_t * (*) (Component_t219 *, const MethodInfo*))Component_GetComponent_TisObject_t_m614_gshared)(__this, method)
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.Collider>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Collider>()
#define Component_GetComponent_TisCollider_t138_m883(__this, method) (( Collider_t138 * (*) (Component_t219 *, const MethodInfo*))Component_GetComponent_TisObject_t_m614_gshared)(__this, method)


// System.Void UnityStandardAssets.Effects.AfterburnerPhysicsForce::.ctor()
extern "C" void AfterburnerPhysicsForce__ctor_m394 (AfterburnerPhysicsForce_t137 * __this, const MethodInfo* method)
{
	{
		__this->___effectAngle_2 = (15.0f);
		__this->___effectWidth_3 = (1.0f);
		__this->___effectDistance_4 = (10.0f);
		__this->___force_5 = (10.0f);
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Effects.AfterburnerPhysicsForce::OnEnable()
extern TypeInfo* SphereCollider_t136_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisCollider_t138_m883_MethodInfo_var;
extern "C" void AfterburnerPhysicsForce_OnEnable_m395 (AfterburnerPhysicsForce_t137 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SphereCollider_t136_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(65);
		Component_GetComponent_TisCollider_t138_m883_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483661);
		s_Il2CppMethodIntialized = true;
	}
	{
		Collider_t138 * L_0 = Component_GetComponent_TisCollider_t138_m883(__this, /*hidden argument*/Component_GetComponent_TisCollider_t138_m883_MethodInfo_var);
		__this->___m_Sphere_7 = ((SphereCollider_t136 *)IsInst(L_0, SphereCollider_t136_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void UnityStandardAssets.Effects.AfterburnerPhysicsForce::FixedUpdate()
extern TypeInfo* Mathf_t218_il2cpp_TypeInfo_var;
extern "C" void AfterburnerPhysicsForce_FixedUpdate_m396 (AfterburnerPhysicsForce_t137 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Vector3_t4  V_1 = {0};
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	Vector3_t4  V_4 = {0};
	{
		Transform_t1 * L_0 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t4  L_1 = Transform_get_position_m593(L_0, /*hidden argument*/NULL);
		SphereCollider_t136 * L_2 = (__this->___m_Sphere_7);
		NullCheck(L_2);
		Vector3_t4  L_3 = SphereCollider_get_center_m884(L_2, /*hidden argument*/NULL);
		Vector3_t4  L_4 = Vector3_op_Addition_m604(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/NULL);
		SphereCollider_t136 * L_5 = (__this->___m_Sphere_7);
		NullCheck(L_5);
		float L_6 = SphereCollider_get_radius_m885(L_5, /*hidden argument*/NULL);
		ColliderU5BU5D_t135* L_7 = Physics_OverlapSphere_m685(NULL /*static, unused*/, L_4, L_6, /*hidden argument*/NULL);
		__this->___m_Cols_6 = L_7;
		V_0 = 0;
		goto IL_016c;
	}

IL_0038:
	{
		ColliderU5BU5D_t135* L_8 = (__this->___m_Cols_6);
		int32_t L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		int32_t L_10 = L_9;
		NullCheck((*(Collider_t138 **)(Collider_t138 **)SZArrayLdElema(L_8, L_10)));
		Rigidbody_t14 * L_11 = Collider_get_attachedRigidbody_m687((*(Collider_t138 **)(Collider_t138 **)SZArrayLdElema(L_8, L_10)), /*hidden argument*/NULL);
		bool L_12 = Object_op_Inequality_m623(NULL /*static, unused*/, L_11, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0168;
		}
	}
	{
		Transform_t1 * L_13 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		ColliderU5BU5D_t135* L_14 = (__this->___m_Cols_6);
		int32_t L_15 = V_0;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		NullCheck((*(Collider_t138 **)(Collider_t138 **)SZArrayLdElema(L_14, L_16)));
		Transform_t1 * L_17 = Component_get_transform_m594((*(Collider_t138 **)(Collider_t138 **)SZArrayLdElema(L_14, L_16)), /*hidden argument*/NULL);
		NullCheck(L_17);
		Vector3_t4  L_18 = Transform_get_position_m593(L_17, /*hidden argument*/NULL);
		NullCheck(L_13);
		Vector3_t4  L_19 = Transform_InverseTransformPoint_m671(L_13, L_18, /*hidden argument*/NULL);
		V_1 = L_19;
		Vector3_t4  L_20 = V_1;
		float L_21 = ((&V_1)->___z_3);
		Vector3_t4  L_22 = {0};
		Vector3__ctor_m612(&L_22, (0.0f), (0.0f), L_21, /*hidden argument*/NULL);
		float L_23 = (__this->___effectWidth_3);
		Vector3_t4  L_24 = Vector3_MoveTowards_m603(NULL /*static, unused*/, L_20, L_22, ((float)((float)L_23*(float)(0.5f))), /*hidden argument*/NULL);
		V_1 = L_24;
		float L_25 = ((&V_1)->___x_1);
		float L_26 = ((&V_1)->___z_3);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_27 = atan2f(L_25, L_26);
		float L_28 = fabsf(((float)((float)L_27*(float)(57.29578f))));
		V_2 = L_28;
		float L_29 = (__this->___effectDistance_4);
		float L_30 = Vector3_get_magnitude_m647((&V_1), /*hidden argument*/NULL);
		float L_31 = Mathf_InverseLerp_m651(NULL /*static, unused*/, L_29, (0.0f), L_30, /*hidden argument*/NULL);
		V_3 = L_31;
		float L_32 = V_3;
		float L_33 = (__this->___effectAngle_2);
		float L_34 = V_2;
		float L_35 = Mathf_InverseLerp_m651(NULL /*static, unused*/, L_33, (0.0f), L_34, /*hidden argument*/NULL);
		V_3 = ((float)((float)L_32*(float)L_35));
		ColliderU5BU5D_t135* L_36 = (__this->___m_Cols_6);
		int32_t L_37 = V_0;
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, L_37);
		int32_t L_38 = L_37;
		NullCheck((*(Collider_t138 **)(Collider_t138 **)SZArrayLdElema(L_36, L_38)));
		Transform_t1 * L_39 = Component_get_transform_m594((*(Collider_t138 **)(Collider_t138 **)SZArrayLdElema(L_36, L_38)), /*hidden argument*/NULL);
		NullCheck(L_39);
		Vector3_t4  L_40 = Transform_get_position_m593(L_39, /*hidden argument*/NULL);
		Transform_t1 * L_41 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_41);
		Vector3_t4  L_42 = Transform_get_position_m593(L_41, /*hidden argument*/NULL);
		Vector3_t4  L_43 = Vector3_op_Subtraction_m595(NULL /*static, unused*/, L_40, L_42, /*hidden argument*/NULL);
		V_4 = L_43;
		ColliderU5BU5D_t135* L_44 = (__this->___m_Cols_6);
		int32_t L_45 = V_0;
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, L_45);
		int32_t L_46 = L_45;
		NullCheck((*(Collider_t138 **)(Collider_t138 **)SZArrayLdElema(L_44, L_46)));
		Rigidbody_t14 * L_47 = Collider_get_attachedRigidbody_m687((*(Collider_t138 **)(Collider_t138 **)SZArrayLdElema(L_44, L_46)), /*hidden argument*/NULL);
		Vector3_t4  L_48 = Vector3_get_normalized_m648((&V_4), /*hidden argument*/NULL);
		float L_49 = (__this->___force_5);
		Vector3_t4  L_50 = Vector3_op_Multiply_m600(NULL /*static, unused*/, L_48, L_49, /*hidden argument*/NULL);
		float L_51 = V_3;
		Vector3_t4  L_52 = Vector3_op_Multiply_m600(NULL /*static, unused*/, L_50, L_51, /*hidden argument*/NULL);
		ColliderU5BU5D_t135* L_53 = (__this->___m_Cols_6);
		int32_t L_54 = V_0;
		NullCheck(L_53);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_53, L_54);
		int32_t L_55 = L_54;
		NullCheck((*(Collider_t138 **)(Collider_t138 **)SZArrayLdElema(L_53, L_55)));
		Transform_t1 * L_56 = Component_get_transform_m594((*(Collider_t138 **)(Collider_t138 **)SZArrayLdElema(L_53, L_55)), /*hidden argument*/NULL);
		NullCheck(L_56);
		Vector3_t4  L_57 = Transform_get_position_m593(L_56, /*hidden argument*/NULL);
		Transform_t1 * L_58 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		float L_59 = ((&V_1)->___z_3);
		NullCheck(L_58);
		Vector3_t4  L_60 = Transform_TransformPoint_m886(L_58, (0.0f), (0.0f), L_59, /*hidden argument*/NULL);
		Vector3_t4  L_61 = Vector3_Lerp_m652(NULL /*static, unused*/, L_57, L_60, (0.1f), /*hidden argument*/NULL);
		NullCheck(L_47);
		Rigidbody_AddForceAtPosition_m887(L_47, L_52, L_61, /*hidden argument*/NULL);
	}

IL_0168:
	{
		int32_t L_62 = V_0;
		V_0 = ((int32_t)((int32_t)L_62+(int32_t)1));
	}

IL_016c:
	{
		int32_t L_63 = V_0;
		ColliderU5BU5D_t135* L_64 = (__this->___m_Cols_6);
		NullCheck(L_64);
		if ((((int32_t)L_63) < ((int32_t)(((int32_t)(((Array_t *)L_64)->max_length))))))
		{
			goto IL_0038;
		}
	}
	{
		return;
	}
}
// System.Void UnityStandardAssets.Effects.AfterburnerPhysicsForce::OnDrawGizmosSelected()
extern TypeInfo* SphereCollider_t136_il2cpp_TypeInfo_var;
extern TypeInfo* Vector3U5BU5D_t210_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisCollider_t138_m883_MethodInfo_var;
extern "C" void AfterburnerPhysicsForce_OnDrawGizmosSelected_m397 (AfterburnerPhysicsForce_t137 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SphereCollider_t136_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(65);
		Vector3U5BU5D_t210_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(61);
		Component_GetComponent_TisCollider_t138_m883_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483661);
		s_Il2CppMethodIntialized = true;
	}
	Vector3U5BU5D_t210* V_0 = {0};
	Vector3U5BU5D_t210* V_1 = {0};
	int32_t V_2 = 0;
	Vector3_t4  V_3 = {0};
	Vector3_t4  V_4 = {0};
	{
		SphereCollider_t136 * L_0 = (__this->___m_Sphere_7);
		bool L_1 = Object_op_Equality_m640(NULL /*static, unused*/, L_0, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		Collider_t138 * L_2 = Component_GetComponent_TisCollider_t138_m883(__this, /*hidden argument*/Component_GetComponent_TisCollider_t138_m883_MethodInfo_var);
		__this->___m_Sphere_7 = ((SphereCollider_t136 *)IsInst(L_2, SphereCollider_t136_il2cpp_TypeInfo_var));
	}

IL_0022:
	{
		SphereCollider_t136 * L_3 = (__this->___m_Sphere_7);
		float L_4 = (__this->___effectDistance_4);
		NullCheck(L_3);
		SphereCollider_set_radius_m888(L_3, ((float)((float)L_4*(float)(0.5f))), /*hidden argument*/NULL);
		SphereCollider_t136 * L_5 = (__this->___m_Sphere_7);
		float L_6 = (__this->___effectDistance_4);
		Vector3_t4  L_7 = {0};
		Vector3__ctor_m612(&L_7, (0.0f), (0.0f), ((float)((float)L_6*(float)(0.5f))), /*hidden argument*/NULL);
		NullCheck(L_5);
		SphereCollider_set_center_m889(L_5, L_7, /*hidden argument*/NULL);
		Vector3U5BU5D_t210* L_8 = ((Vector3U5BU5D_t210*)SZArrayNew(Vector3U5BU5D_t210_il2cpp_TypeInfo_var, 4));
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 0);
		Vector3_t4  L_9 = Vector3_get_up_m642(NULL /*static, unused*/, /*hidden argument*/NULL);
		*((Vector3_t4 *)(Vector3_t4 *)SZArrayLdElema(L_8, 0)) = L_9;
		Vector3U5BU5D_t210* L_10 = L_8;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 1);
		Vector3_t4  L_11 = Vector3_get_up_m642(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4  L_12 = Vector3_op_UnaryNegation_m682(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		*((Vector3_t4 *)(Vector3_t4 *)SZArrayLdElema(L_10, 1)) = L_12;
		Vector3U5BU5D_t210* L_13 = L_10;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 2);
		Vector3_t4  L_14 = Vector3_get_right_m597(NULL /*static, unused*/, /*hidden argument*/NULL);
		*((Vector3_t4 *)(Vector3_t4 *)SZArrayLdElema(L_13, 2)) = L_14;
		Vector3U5BU5D_t210* L_15 = L_13;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 3);
		Vector3_t4  L_16 = Vector3_get_right_m597(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4  L_17 = Vector3_op_UnaryNegation_m682(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		*((Vector3_t4 *)(Vector3_t4 *)SZArrayLdElema(L_15, 3)) = L_17;
		V_0 = L_15;
		Vector3U5BU5D_t210* L_18 = ((Vector3U5BU5D_t210*)SZArrayNew(Vector3U5BU5D_t210_il2cpp_TypeInfo_var, 4));
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 0);
		Vector3_t4  L_19 = Vector3_get_right_m597(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4  L_20 = Vector3_op_UnaryNegation_m682(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		*((Vector3_t4 *)(Vector3_t4 *)SZArrayLdElema(L_18, 0)) = L_20;
		Vector3U5BU5D_t210* L_21 = L_18;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, 1);
		Vector3_t4  L_22 = Vector3_get_right_m597(NULL /*static, unused*/, /*hidden argument*/NULL);
		*((Vector3_t4 *)(Vector3_t4 *)SZArrayLdElema(L_21, 1)) = L_22;
		Vector3U5BU5D_t210* L_23 = L_21;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 2);
		Vector3_t4  L_24 = Vector3_get_up_m642(NULL /*static, unused*/, /*hidden argument*/NULL);
		*((Vector3_t4 *)(Vector3_t4 *)SZArrayLdElema(L_23, 2)) = L_24;
		Vector3U5BU5D_t210* L_25 = L_23;
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, 3);
		Vector3_t4  L_26 = Vector3_get_up_m642(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4  L_27 = Vector3_op_UnaryNegation_m682(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		*((Vector3_t4 *)(Vector3_t4 *)SZArrayLdElema(L_25, 3)) = L_27;
		V_1 = L_25;
		Color_t65  L_28 = {0};
		Color__ctor_m746(&L_28, (0.0f), (1.0f), (0.0f), (0.5f), /*hidden argument*/NULL);
		Gizmos_set_color_m890(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
		V_2 = 0;
		goto IL_01ca;
	}

IL_012e:
	{
		Transform_t1 * L_29 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_29);
		Vector3_t4  L_30 = Transform_get_position_m593(L_29, /*hidden argument*/NULL);
		Transform_t1 * L_31 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_31);
		Quaternion_t19  L_32 = Transform_get_rotation_m656(L_31, /*hidden argument*/NULL);
		Vector3U5BU5D_t210* L_33 = V_0;
		int32_t L_34 = V_2;
		NullCheck(L_33);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_33, L_34);
		Vector3_t4  L_35 = Quaternion_op_Multiply_m891(NULL /*static, unused*/, L_32, (*(Vector3_t4 *)((Vector3_t4 *)(Vector3_t4 *)SZArrayLdElema(L_33, L_34))), /*hidden argument*/NULL);
		float L_36 = (__this->___effectWidth_3);
		Vector3_t4  L_37 = Vector3_op_Multiply_m600(NULL /*static, unused*/, L_35, L_36, /*hidden argument*/NULL);
		Vector3_t4  L_38 = Vector3_op_Multiply_m600(NULL /*static, unused*/, L_37, (0.5f), /*hidden argument*/NULL);
		Vector3_t4  L_39 = Vector3_op_Addition_m604(NULL /*static, unused*/, L_30, L_38, /*hidden argument*/NULL);
		V_3 = L_39;
		Transform_t1 * L_40 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		float L_41 = (__this->___effectAngle_2);
		Vector3U5BU5D_t210* L_42 = V_1;
		int32_t L_43 = V_2;
		NullCheck(L_42);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_42, L_43);
		Quaternion_t19  L_44 = Quaternion_AngleAxis_m892(NULL /*static, unused*/, L_41, (*(Vector3_t4 *)((Vector3_t4 *)(Vector3_t4 *)SZArrayLdElema(L_42, L_43))), /*hidden argument*/NULL);
		Vector3_t4  L_45 = Vector3_get_forward_m605(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4  L_46 = Quaternion_op_Multiply_m891(NULL /*static, unused*/, L_44, L_45, /*hidden argument*/NULL);
		NullCheck(L_40);
		Vector3_t4  L_47 = Transform_TransformDirection_m893(L_40, L_46, /*hidden argument*/NULL);
		V_4 = L_47;
		Vector3_t4  L_48 = V_3;
		Vector3_t4  L_49 = V_3;
		Vector3_t4  L_50 = V_4;
		SphereCollider_t136 * L_51 = (__this->___m_Sphere_7);
		NullCheck(L_51);
		float L_52 = SphereCollider_get_radius_m885(L_51, /*hidden argument*/NULL);
		Vector3_t4  L_53 = Vector3_op_Multiply_m600(NULL /*static, unused*/, L_50, L_52, /*hidden argument*/NULL);
		Vector3_t4  L_54 = Vector3_op_Multiply_m600(NULL /*static, unused*/, L_53, (2.0f), /*hidden argument*/NULL);
		Vector3_t4  L_55 = Vector3_op_Addition_m604(NULL /*static, unused*/, L_49, L_54, /*hidden argument*/NULL);
		Gizmos_DrawLine_m894(NULL /*static, unused*/, L_48, L_55, /*hidden argument*/NULL);
		int32_t L_56 = V_2;
		V_2 = ((int32_t)((int32_t)L_56+(int32_t)1));
	}

IL_01ca:
	{
		int32_t L_57 = V_2;
		if ((((int32_t)L_57) < ((int32_t)4)))
		{
			goto IL_012e;
		}
	}
	{
		return;
	}
}
// UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>c__Iterator0
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Effects_Ex.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>c__Iterator0
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Effects_ExMethodDeclarations.h"

// System.Object
#include "mscorlib_System_Object.h"
// System.UInt32
#include "mscorlib_System_UInt32.h"
// UnityStandardAssets.Effects.ExplosionFireAndDebris
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Effects_Ex_0.h"
// UnityStandardAssets.Effects.ParticleSystemMultiplier
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Effects_Pa.h"
// UnityEngine.Ray
#include "UnityEngine_UnityEngine_Ray.h"
// UnityEngine.RaycastHit
#include "UnityEngine_UnityEngine_RaycastHit.h"
// System.NotSupportedException
#include "mscorlib_System_NotSupportedException.h"
// UnityEngine.Random
#include "UnityEngine_UnityEngine_RandomMethodDeclarations.h"
// UnityEngine.Ray
#include "UnityEngine_UnityEngine_RayMethodDeclarations.h"
// UnityEngine.RaycastHit
#include "UnityEngine_UnityEngine_RaycastHitMethodDeclarations.h"
// UnityStandardAssets.Effects.ExplosionFireAndDebris
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Effects_Ex_0MethodDeclarations.h"
// System.NotSupportedException
#include "mscorlib_System_NotSupportedExceptionMethodDeclarations.h"
struct Component_t219;
struct ParticleSystemMultiplier_t156;
// Declaration !!0 UnityEngine.Component::GetComponent<UnityStandardAssets.Effects.ParticleSystemMultiplier>()
// !!0 UnityEngine.Component::GetComponent<UnityStandardAssets.Effects.ParticleSystemMultiplier>()
#define Component_GetComponent_TisParticleSystemMultiplier_t156_m895(__this, method) (( ParticleSystemMultiplier_t156 * (*) (Component_t219 *, const MethodInfo*))Component_GetComponent_TisObject_t_m614_gshared)(__this, method)


// System.Void UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>c__Iterator0::.ctor()
extern "C" void U3CStartU3Ec__Iterator0__ctor_m398 (U3CStartU3Ec__Iterator0_t140 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m677(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m399 (U3CStartU3Ec__Iterator0_t140 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_16);
		return L_0;
	}
}
// System.Object UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m400 (U3CStartU3Ec__Iterator0_t140 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_16);
		return L_0;
	}
}
// System.Boolean UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>c__Iterator0::MoveNext()
extern const MethodInfo* Component_GetComponent_TisParticleSystemMultiplier_t156_m895_MethodInfo_var;
extern "C" bool U3CStartU3Ec__Iterator0_MoveNext_m401 (U3CStartU3Ec__Iterator0_t140 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisParticleSystemMultiplier_t156_m895_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483662);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_15);
		V_0 = L_0;
		__this->___U24PC_15 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0100;
		}
	}
	{
		goto IL_02fe;
	}

IL_0021:
	{
		ExplosionFireAndDebris_t139 * L_2 = (__this->___U3CU3Ef__this_17);
		NullCheck(L_2);
		ParticleSystemMultiplier_t156 * L_3 = Component_GetComponent_TisParticleSystemMultiplier_t156_m895(L_2, /*hidden argument*/Component_GetComponent_TisParticleSystemMultiplier_t156_m895_MethodInfo_var);
		NullCheck(L_3);
		float L_4 = (L_3->___multiplier_2);
		__this->___U3CmultiplierU3E__0_0 = L_4;
		__this->___U3CnU3E__1_1 = 0;
		goto IL_00ce;
	}

IL_0043:
	{
		ExplosionFireAndDebris_t139 * L_5 = (__this->___U3CU3Ef__this_17);
		NullCheck(L_5);
		TransformU5BU5D_t141* L_6 = (L_5->___debrisPrefabs_2);
		ExplosionFireAndDebris_t139 * L_7 = (__this->___U3CU3Ef__this_17);
		NullCheck(L_7);
		TransformU5BU5D_t141* L_8 = (L_7->___debrisPrefabs_2);
		NullCheck(L_8);
		int32_t L_9 = Random_Range_m896(NULL /*static, unused*/, 0, (((int32_t)(((Array_t *)L_8)->max_length))), /*hidden argument*/NULL);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_9);
		int32_t L_10 = L_9;
		__this->___U3CprefabU3E__2_2 = (*(Transform_t1 **)(Transform_t1 **)SZArrayLdElema(L_6, L_10));
		ExplosionFireAndDebris_t139 * L_11 = (__this->___U3CU3Ef__this_17);
		NullCheck(L_11);
		Transform_t1 * L_12 = Component_get_transform_m594(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t4  L_13 = Transform_get_position_m593(L_12, /*hidden argument*/NULL);
		Vector3_t4  L_14 = Random_get_insideUnitSphere_m897(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4  L_15 = Vector3_op_Multiply_m600(NULL /*static, unused*/, L_14, (3.0f), /*hidden argument*/NULL);
		float L_16 = (__this->___U3CmultiplierU3E__0_0);
		Vector3_t4  L_17 = Vector3_op_Multiply_m600(NULL /*static, unused*/, L_15, L_16, /*hidden argument*/NULL);
		Vector3_t4  L_18 = Vector3_op_Addition_m604(NULL /*static, unused*/, L_13, L_17, /*hidden argument*/NULL);
		__this->___U3CposU3E__3_3 = L_18;
		Quaternion_t19  L_19 = Random_get_rotation_m898(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___U3CrotU3E__4_4 = L_19;
		Transform_t1 * L_20 = (__this->___U3CprefabU3E__2_2);
		Vector3_t4  L_21 = (__this->___U3CposU3E__3_3);
		Quaternion_t19  L_22 = (__this->___U3CrotU3E__4_4);
		Object_Instantiate_m899(NULL /*static, unused*/, L_20, L_21, L_22, /*hidden argument*/NULL);
		int32_t L_23 = (__this->___U3CnU3E__1_1);
		__this->___U3CnU3E__1_1 = ((int32_t)((int32_t)L_23+(int32_t)1));
	}

IL_00ce:
	{
		int32_t L_24 = (__this->___U3CnU3E__1_1);
		ExplosionFireAndDebris_t139 * L_25 = (__this->___U3CU3Ef__this_17);
		NullCheck(L_25);
		int32_t L_26 = (L_25->___numDebrisPieces_4);
		float L_27 = (__this->___U3CmultiplierU3E__0_0);
		if ((((float)(((float)L_24))) < ((float)((float)((float)(((float)L_26))*(float)L_27)))))
		{
			goto IL_0043;
		}
	}
	{
		__this->___U24current_16 = NULL;
		__this->___U24PC_15 = 1;
		goto IL_0300;
	}

IL_0100:
	{
		float L_28 = (__this->___U3CmultiplierU3E__0_0);
		__this->___U3CrU3E__5_5 = ((float)((float)(10.0f)*(float)L_28));
		ExplosionFireAndDebris_t139 * L_29 = (__this->___U3CU3Ef__this_17);
		NullCheck(L_29);
		Transform_t1 * L_30 = Component_get_transform_m594(L_29, /*hidden argument*/NULL);
		NullCheck(L_30);
		Vector3_t4  L_31 = Transform_get_position_m593(L_30, /*hidden argument*/NULL);
		float L_32 = (__this->___U3CrU3E__5_5);
		ColliderU5BU5D_t135* L_33 = Physics_OverlapSphere_m685(NULL /*static, unused*/, L_31, L_32, /*hidden argument*/NULL);
		__this->___U3CcolsU3E__6_6 = L_33;
		ColliderU5BU5D_t135* L_34 = (__this->___U3CcolsU3E__6_6);
		__this->___U3CU24s_8U3E__7_7 = L_34;
		__this->___U3CU24s_9U3E__8_8 = 0;
		goto IL_021e;
	}

IL_014b:
	{
		ColliderU5BU5D_t135* L_35 = (__this->___U3CU24s_8U3E__7_7);
		int32_t L_36 = (__this->___U3CU24s_9U3E__8_8);
		NullCheck(L_35);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_35, L_36);
		int32_t L_37 = L_36;
		__this->___U3CcolU3E__9_9 = (*(Collider_t138 **)(Collider_t138 **)SZArrayLdElema(L_35, L_37));
		ExplosionFireAndDebris_t139 * L_38 = (__this->___U3CU3Ef__this_17);
		NullCheck(L_38);
		int32_t L_39 = (L_38->___numFires_5);
		if ((((int32_t)L_39) <= ((int32_t)0)))
		{
			goto IL_0210;
		}
	}
	{
		ExplosionFireAndDebris_t139 * L_40 = (__this->___U3CU3Ef__this_17);
		NullCheck(L_40);
		Transform_t1 * L_41 = Component_get_transform_m594(L_40, /*hidden argument*/NULL);
		NullCheck(L_41);
		Vector3_t4  L_42 = Transform_get_position_m593(L_41, /*hidden argument*/NULL);
		Collider_t138 * L_43 = (__this->___U3CcolU3E__9_9);
		NullCheck(L_43);
		Transform_t1 * L_44 = Component_get_transform_m594(L_43, /*hidden argument*/NULL);
		NullCheck(L_44);
		Vector3_t4  L_45 = Transform_get_position_m593(L_44, /*hidden argument*/NULL);
		ExplosionFireAndDebris_t139 * L_46 = (__this->___U3CU3Ef__this_17);
		NullCheck(L_46);
		Transform_t1 * L_47 = Component_get_transform_m594(L_46, /*hidden argument*/NULL);
		NullCheck(L_47);
		Vector3_t4  L_48 = Transform_get_position_m593(L_47, /*hidden argument*/NULL);
		Vector3_t4  L_49 = Vector3_op_Subtraction_m595(NULL /*static, unused*/, L_45, L_48, /*hidden argument*/NULL);
		Ray_t26  L_50 = {0};
		Ray__ctor_m900(&L_50, L_42, L_49, /*hidden argument*/NULL);
		__this->___U3CfireRayU3E__10_10 = L_50;
		Collider_t138 * L_51 = (__this->___U3CcolU3E__9_9);
		Ray_t26  L_52 = (__this->___U3CfireRayU3E__10_10);
		RaycastHit_t24 * L_53 = &(__this->___U3CfireHitU3E__11_11);
		float L_54 = (__this->___U3CrU3E__5_5);
		NullCheck(L_51);
		bool L_55 = Collider_Raycast_m901(L_51, L_52, L_53, L_54, /*hidden argument*/NULL);
		if (!L_55)
		{
			goto IL_0210;
		}
	}
	{
		ExplosionFireAndDebris_t139 * L_56 = (__this->___U3CU3Ef__this_17);
		Collider_t138 * L_57 = (__this->___U3CcolU3E__9_9);
		NullCheck(L_57);
		Transform_t1 * L_58 = Component_get_transform_m594(L_57, /*hidden argument*/NULL);
		RaycastHit_t24 * L_59 = &(__this->___U3CfireHitU3E__11_11);
		Vector3_t4  L_60 = RaycastHit_get_point_m693(L_59, /*hidden argument*/NULL);
		RaycastHit_t24 * L_61 = &(__this->___U3CfireHitU3E__11_11);
		Vector3_t4  L_62 = RaycastHit_get_normal_m902(L_61, /*hidden argument*/NULL);
		NullCheck(L_56);
		ExplosionFireAndDebris_AddFire_m406(L_56, L_58, L_60, L_62, /*hidden argument*/NULL);
		ExplosionFireAndDebris_t139 * L_63 = (__this->___U3CU3Ef__this_17);
		ExplosionFireAndDebris_t139 * L_64 = L_63;
		NullCheck(L_64);
		int32_t L_65 = (L_64->___numFires_5);
		NullCheck(L_64);
		L_64->___numFires_5 = ((int32_t)((int32_t)L_65-(int32_t)1));
	}

IL_0210:
	{
		int32_t L_66 = (__this->___U3CU24s_9U3E__8_8);
		__this->___U3CU24s_9U3E__8_8 = ((int32_t)((int32_t)L_66+(int32_t)1));
	}

IL_021e:
	{
		int32_t L_67 = (__this->___U3CU24s_9U3E__8_8);
		ColliderU5BU5D_t135* L_68 = (__this->___U3CU24s_8U3E__7_7);
		NullCheck(L_68);
		if ((((int32_t)L_67) < ((int32_t)(((int32_t)(((Array_t *)L_68)->max_length))))))
		{
			goto IL_014b;
		}
	}
	{
		__this->___U3CtestRU3E__12_12 = (0.0f);
		goto IL_02d5;
	}

IL_0241:
	{
		ExplosionFireAndDebris_t139 * L_69 = (__this->___U3CU3Ef__this_17);
		NullCheck(L_69);
		Transform_t1 * L_70 = Component_get_transform_m594(L_69, /*hidden argument*/NULL);
		NullCheck(L_70);
		Vector3_t4  L_71 = Transform_get_position_m593(L_70, /*hidden argument*/NULL);
		Vector3_t4  L_72 = Vector3_get_up_m642(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4  L_73 = Vector3_op_Addition_m604(NULL /*static, unused*/, L_71, L_72, /*hidden argument*/NULL);
		Vector3_t4  L_74 = Random_get_onUnitSphere_m903(NULL /*static, unused*/, /*hidden argument*/NULL);
		Ray_t26  L_75 = {0};
		Ray__ctor_m900(&L_75, L_73, L_74, /*hidden argument*/NULL);
		__this->___U3CfireRayU3E__13_13 = L_75;
		Ray_t26  L_76 = (__this->___U3CfireRayU3E__13_13);
		RaycastHit_t24 * L_77 = &(__this->___U3CfireHitU3E__14_14);
		float L_78 = (__this->___U3CtestRU3E__12_12);
		bool L_79 = Physics_Raycast_m904(NULL /*static, unused*/, L_76, L_77, L_78, /*hidden argument*/NULL);
		if (!L_79)
		{
			goto IL_02bc;
		}
	}
	{
		ExplosionFireAndDebris_t139 * L_80 = (__this->___U3CU3Ef__this_17);
		RaycastHit_t24 * L_81 = &(__this->___U3CfireHitU3E__14_14);
		Vector3_t4  L_82 = RaycastHit_get_point_m693(L_81, /*hidden argument*/NULL);
		RaycastHit_t24 * L_83 = &(__this->___U3CfireHitU3E__14_14);
		Vector3_t4  L_84 = RaycastHit_get_normal_m902(L_83, /*hidden argument*/NULL);
		NullCheck(L_80);
		ExplosionFireAndDebris_AddFire_m406(L_80, (Transform_t1 *)NULL, L_82, L_84, /*hidden argument*/NULL);
		ExplosionFireAndDebris_t139 * L_85 = (__this->___U3CU3Ef__this_17);
		ExplosionFireAndDebris_t139 * L_86 = L_85;
		NullCheck(L_86);
		int32_t L_87 = (L_86->___numFires_5);
		NullCheck(L_86);
		L_86->___numFires_5 = ((int32_t)((int32_t)L_87-(int32_t)1));
	}

IL_02bc:
	{
		float L_88 = (__this->___U3CtestRU3E__12_12);
		float L_89 = (__this->___U3CrU3E__5_5);
		__this->___U3CtestRU3E__12_12 = ((float)((float)L_88+(float)((float)((float)L_89*(float)(0.1f)))));
	}

IL_02d5:
	{
		ExplosionFireAndDebris_t139 * L_90 = (__this->___U3CU3Ef__this_17);
		NullCheck(L_90);
		int32_t L_91 = (L_90->___numFires_5);
		if ((((int32_t)L_91) <= ((int32_t)0)))
		{
			goto IL_02f7;
		}
	}
	{
		float L_92 = (__this->___U3CtestRU3E__12_12);
		float L_93 = (__this->___U3CrU3E__5_5);
		if ((((float)L_92) < ((float)L_93)))
		{
			goto IL_0241;
		}
	}

IL_02f7:
	{
		__this->___U24PC_15 = (-1);
	}

IL_02fe:
	{
		return 0;
	}

IL_0300:
	{
		return 1;
	}
	// Dead block : IL_0302: ldloc.1
}
// System.Void UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>c__Iterator0::Dispose()
extern "C" void U3CStartU3Ec__Iterator0_Dispose_m402 (U3CStartU3Ec__Iterator0_t140 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_15 = (-1);
		return;
	}
}
// System.Void UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>c__Iterator0::Reset()
extern TypeInfo* NotSupportedException_t244_il2cpp_TypeInfo_var;
extern "C" void U3CStartU3Ec__Iterator0_Reset_m403 (U3CStartU3Ec__Iterator0_t140 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t244_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t244 * L_0 = (NotSupportedException_t244 *)il2cpp_codegen_object_new (NotSupportedException_t244_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m905(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
#ifndef _MSC_VER
#else
#endif



// System.Void UnityStandardAssets.Effects.ExplosionFireAndDebris::.ctor()
extern "C" void ExplosionFireAndDebris__ctor_m404 (ExplosionFireAndDebris_t139 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator UnityStandardAssets.Effects.ExplosionFireAndDebris::Start()
extern TypeInfo* U3CStartU3Ec__Iterator0_t140_il2cpp_TypeInfo_var;
extern "C" Object_t * ExplosionFireAndDebris_Start_m405 (ExplosionFireAndDebris_t139 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CStartU3Ec__Iterator0_t140_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(68);
		s_Il2CppMethodIntialized = true;
	}
	U3CStartU3Ec__Iterator0_t140 * V_0 = {0};
	{
		U3CStartU3Ec__Iterator0_t140 * L_0 = (U3CStartU3Ec__Iterator0_t140 *)il2cpp_codegen_object_new (U3CStartU3Ec__Iterator0_t140_il2cpp_TypeInfo_var);
		U3CStartU3Ec__Iterator0__ctor_m398(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CStartU3Ec__Iterator0_t140 * L_1 = V_0;
		NullCheck(L_1);
		L_1->___U3CU3Ef__this_17 = __this;
		U3CStartU3Ec__Iterator0_t140 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityStandardAssets.Effects.ExplosionFireAndDebris::AddFire(UnityEngine.Transform,UnityEngine.Vector3,UnityEngine.Vector3)
extern TypeInfo* Transform_t1_il2cpp_TypeInfo_var;
extern "C" void ExplosionFireAndDebris_AddFire_m406 (ExplosionFireAndDebris_t139 * __this, Transform_t1 * ___t, Vector3_t4  ___pos, Vector3_t4  ___normal, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Transform_t1_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(28);
		s_Il2CppMethodIntialized = true;
	}
	Transform_t1 * V_0 = {0};
	{
		Vector3_t4  L_0 = ___pos;
		Vector3_t4  L_1 = ___normal;
		Vector3_t4  L_2 = Vector3_op_Multiply_m600(NULL /*static, unused*/, L_1, (0.5f), /*hidden argument*/NULL);
		Vector3_t4  L_3 = Vector3_op_Addition_m604(NULL /*static, unused*/, L_0, L_2, /*hidden argument*/NULL);
		___pos = L_3;
		Transform_t1 * L_4 = (__this->___firePrefab_3);
		Vector3_t4  L_5 = ___pos;
		Quaternion_t19  L_6 = Quaternion_get_identity_m788(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_t164 * L_7 = Object_Instantiate_m899(NULL /*static, unused*/, L_4, L_5, L_6, /*hidden argument*/NULL);
		V_0 = ((Transform_t1 *)Castclass(L_7, Transform_t1_il2cpp_TypeInfo_var));
		Transform_t1 * L_8 = V_0;
		Transform_t1 * L_9 = ___t;
		NullCheck(L_8);
		Transform_set_parent_m596(L_8, L_9, /*hidden argument*/NULL);
		return;
	}
}
// UnityStandardAssets.Effects.ExplosionPhysicsForce/<Start>c__Iterator1
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Effects_Ex_1.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.Effects.ExplosionPhysicsForce/<Start>c__Iterator1
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Effects_Ex_1MethodDeclarations.h"

// UnityStandardAssets.Effects.ExplosionPhysicsForce
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Effects_Ex_2.h"
// System.Collections.Generic.List`1<UnityEngine.Rigidbody>
#include "mscorlib_System_Collections_Generic_List_1_gen_0.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.Rigidbody>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen.h"
// UnityEngine.ForceMode
#include "UnityEngine_UnityEngine_ForceMode.h"
// System.Collections.Generic.List`1<UnityEngine.Rigidbody>
#include "mscorlib_System_Collections_Generic_List_1_gen_0MethodDeclarations.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.Rigidbody>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_genMethodDeclarations.h"


// System.Void UnityStandardAssets.Effects.ExplosionPhysicsForce/<Start>c__Iterator1::.ctor()
extern "C" void U3CStartU3Ec__Iterator1__ctor_m407 (U3CStartU3Ec__Iterator1_t144 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m677(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object UnityStandardAssets.Effects.ExplosionPhysicsForce/<Start>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CStartU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m408 (U3CStartU3Ec__Iterator1_t144 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_10);
		return L_0;
	}
}
// System.Object UnityStandardAssets.Effects.ExplosionPhysicsForce/<Start>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CStartU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m409 (U3CStartU3Ec__Iterator1_t144 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_10);
		return L_0;
	}
}
// System.Boolean UnityStandardAssets.Effects.ExplosionPhysicsForce/<Start>c__Iterator1::MoveNext()
extern TypeInfo* List_1_t142_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t145_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t233_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisParticleSystemMultiplier_t156_m895_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m906_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m907_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m908_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m910_MethodInfo_var;
extern "C" bool U3CStartU3Ec__Iterator1_MoveNext_m410 (U3CStartU3Ec__Iterator1_t144 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t142_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(69);
		Enumerator_t145_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(70);
		IDisposable_t233_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		Component_GetComponent_TisParticleSystemMultiplier_t156_m895_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483662);
		List_1__ctor_m906_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483663);
		List_1_GetEnumerator_m907_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483664);
		Enumerator_get_Current_m908_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483665);
		Enumerator_MoveNext_m910_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483666);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	Exception_t232 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t232 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (__this->___U24PC_9);
		V_0 = L_0;
		__this->___U24PC_9 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0034;
		}
	}
	{
		goto IL_01af;
	}

IL_0021:
	{
		__this->___U24current_10 = NULL;
		__this->___U24PC_9 = 1;
		goto IL_01b1;
	}

IL_0034:
	{
		ExplosionPhysicsForce_t143 * L_2 = (__this->___U3CU3Ef__this_11);
		NullCheck(L_2);
		ParticleSystemMultiplier_t156 * L_3 = Component_GetComponent_TisParticleSystemMultiplier_t156_m895(L_2, /*hidden argument*/Component_GetComponent_TisParticleSystemMultiplier_t156_m895_MethodInfo_var);
		NullCheck(L_3);
		float L_4 = (L_3->___multiplier_2);
		__this->___U3CmultiplierU3E__0_0 = L_4;
		float L_5 = (__this->___U3CmultiplierU3E__0_0);
		__this->___U3CrU3E__1_1 = ((float)((float)(10.0f)*(float)L_5));
		ExplosionPhysicsForce_t143 * L_6 = (__this->___U3CU3Ef__this_11);
		NullCheck(L_6);
		Transform_t1 * L_7 = Component_get_transform_m594(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_t4  L_8 = Transform_get_position_m593(L_7, /*hidden argument*/NULL);
		float L_9 = (__this->___U3CrU3E__1_1);
		ColliderU5BU5D_t135* L_10 = Physics_OverlapSphere_m685(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		__this->___U3CcolsU3E__2_2 = L_10;
		List_1_t142 * L_11 = (List_1_t142 *)il2cpp_codegen_object_new (List_1_t142_il2cpp_TypeInfo_var);
		List_1__ctor_m906(L_11, /*hidden argument*/List_1__ctor_m906_MethodInfo_var);
		__this->___U3CrigidbodiesU3E__3_3 = L_11;
		ColliderU5BU5D_t135* L_12 = (__this->___U3CcolsU3E__2_2);
		__this->___U3CU24s_10U3E__4_4 = L_12;
		__this->___U3CU24s_11U3E__5_5 = 0;
		goto IL_0108;
	}

IL_00a0:
	{
		ColliderU5BU5D_t135* L_13 = (__this->___U3CU24s_10U3E__4_4);
		int32_t L_14 = (__this->___U3CU24s_11U3E__5_5);
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		int32_t L_15 = L_14;
		__this->___U3CcolU3E__6_6 = (*(Collider_t138 **)(Collider_t138 **)SZArrayLdElema(L_13, L_15));
		Collider_t138 * L_16 = (__this->___U3CcolU3E__6_6);
		NullCheck(L_16);
		Rigidbody_t14 * L_17 = Collider_get_attachedRigidbody_m687(L_16, /*hidden argument*/NULL);
		bool L_18 = Object_op_Inequality_m623(NULL /*static, unused*/, L_17, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_00fa;
		}
	}
	{
		List_1_t142 * L_19 = (__this->___U3CrigidbodiesU3E__3_3);
		Collider_t138 * L_20 = (__this->___U3CcolU3E__6_6);
		NullCheck(L_20);
		Rigidbody_t14 * L_21 = Collider_get_attachedRigidbody_m687(L_20, /*hidden argument*/NULL);
		NullCheck(L_19);
		bool L_22 = (bool)VirtFuncInvoker1< bool, Rigidbody_t14 * >::Invoke(24 /* System.Boolean System.Collections.Generic.List`1<UnityEngine.Rigidbody>::Contains(!0) */, L_19, L_21);
		if (L_22)
		{
			goto IL_00fa;
		}
	}
	{
		List_1_t142 * L_23 = (__this->___U3CrigidbodiesU3E__3_3);
		Collider_t138 * L_24 = (__this->___U3CcolU3E__6_6);
		NullCheck(L_24);
		Rigidbody_t14 * L_25 = Collider_get_attachedRigidbody_m687(L_24, /*hidden argument*/NULL);
		NullCheck(L_23);
		VirtActionInvoker1< Rigidbody_t14 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody>::Add(!0) */, L_23, L_25);
	}

IL_00fa:
	{
		int32_t L_26 = (__this->___U3CU24s_11U3E__5_5);
		__this->___U3CU24s_11U3E__5_5 = ((int32_t)((int32_t)L_26+(int32_t)1));
	}

IL_0108:
	{
		int32_t L_27 = (__this->___U3CU24s_11U3E__5_5);
		ColliderU5BU5D_t135* L_28 = (__this->___U3CU24s_10U3E__4_4);
		NullCheck(L_28);
		if ((((int32_t)L_27) < ((int32_t)(((int32_t)(((Array_t *)L_28)->max_length))))))
		{
			goto IL_00a0;
		}
	}
	{
		List_1_t142 * L_29 = (__this->___U3CrigidbodiesU3E__3_3);
		NullCheck(L_29);
		Enumerator_t145  L_30 = List_1_GetEnumerator_m907(L_29, /*hidden argument*/List_1_GetEnumerator_m907_MethodInfo_var);
		__this->___U3CU24s_12U3E__7_7 = L_30;
	}

IL_012c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0182;
		}

IL_0131:
		{
			Enumerator_t145 * L_31 = &(__this->___U3CU24s_12U3E__7_7);
			Rigidbody_t14 * L_32 = Enumerator_get_Current_m908(L_31, /*hidden argument*/Enumerator_get_Current_m908_MethodInfo_var);
			__this->___U3CrbU3E__8_8 = L_32;
			Rigidbody_t14 * L_33 = (__this->___U3CrbU3E__8_8);
			ExplosionPhysicsForce_t143 * L_34 = (__this->___U3CU3Ef__this_11);
			NullCheck(L_34);
			float L_35 = (L_34->___explosionForce_2);
			float L_36 = (__this->___U3CmultiplierU3E__0_0);
			ExplosionPhysicsForce_t143 * L_37 = (__this->___U3CU3Ef__this_11);
			NullCheck(L_37);
			Transform_t1 * L_38 = Component_get_transform_m594(L_37, /*hidden argument*/NULL);
			NullCheck(L_38);
			Vector3_t4  L_39 = Transform_get_position_m593(L_38, /*hidden argument*/NULL);
			float L_40 = (__this->___U3CrU3E__1_1);
			float L_41 = (__this->___U3CmultiplierU3E__0_0);
			NullCheck(L_33);
			Rigidbody_AddExplosionForce_m909(L_33, ((float)((float)L_35*(float)L_36)), L_39, L_40, ((float)((float)(1.0f)*(float)L_41)), 1, /*hidden argument*/NULL);
		}

IL_0182:
		{
			Enumerator_t145 * L_42 = &(__this->___U3CU24s_12U3E__7_7);
			bool L_43 = Enumerator_MoveNext_m910(L_42, /*hidden argument*/Enumerator_MoveNext_m910_MethodInfo_var);
			if (L_43)
			{
				goto IL_0131;
			}
		}

IL_0192:
		{
			IL2CPP_LEAVE(0x1A8, FINALLY_0197);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t232 *)e.ex;
		goto FINALLY_0197;
	}

FINALLY_0197:
	{ // begin finally (depth: 1)
		Enumerator_t145  L_44 = (__this->___U3CU24s_12U3E__7_7);
		Enumerator_t145  L_45 = L_44;
		Object_t * L_46 = Box(Enumerator_t145_il2cpp_TypeInfo_var, &L_45);
		NullCheck(L_46);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t233_il2cpp_TypeInfo_var, L_46);
		IL2CPP_END_FINALLY(407)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(407)
	{
		IL2CPP_JUMP_TBL(0x1A8, IL_01a8)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t232 *)
	}

IL_01a8:
	{
		__this->___U24PC_9 = (-1);
	}

IL_01af:
	{
		return 0;
	}

IL_01b1:
	{
		return 1;
	}
	// Dead block : IL_01b3: ldloc.1
}
// System.Void UnityStandardAssets.Effects.ExplosionPhysicsForce/<Start>c__Iterator1::Dispose()
extern "C" void U3CStartU3Ec__Iterator1_Dispose_m411 (U3CStartU3Ec__Iterator1_t144 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_9 = (-1);
		return;
	}
}
// System.Void UnityStandardAssets.Effects.ExplosionPhysicsForce/<Start>c__Iterator1::Reset()
extern TypeInfo* NotSupportedException_t244_il2cpp_TypeInfo_var;
extern "C" void U3CStartU3Ec__Iterator1_Reset_m412 (U3CStartU3Ec__Iterator1_t144 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t244_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t244 * L_0 = (NotSupportedException_t244 *)il2cpp_codegen_object_new (NotSupportedException_t244_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m905(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.Effects.ExplosionPhysicsForce
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Effects_Ex_2MethodDeclarations.h"



// System.Void UnityStandardAssets.Effects.ExplosionPhysicsForce::.ctor()
extern "C" void ExplosionPhysicsForce__ctor_m413 (ExplosionPhysicsForce_t143 * __this, const MethodInfo* method)
{
	{
		__this->___explosionForce_2 = (4.0f);
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator UnityStandardAssets.Effects.ExplosionPhysicsForce::Start()
extern TypeInfo* U3CStartU3Ec__Iterator1_t144_il2cpp_TypeInfo_var;
extern "C" Object_t * ExplosionPhysicsForce_Start_m414 (ExplosionPhysicsForce_t143 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CStartU3Ec__Iterator1_t144_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(71);
		s_Il2CppMethodIntialized = true;
	}
	U3CStartU3Ec__Iterator1_t144 * V_0 = {0};
	{
		U3CStartU3Ec__Iterator1_t144 * L_0 = (U3CStartU3Ec__Iterator1_t144 *)il2cpp_codegen_object_new (U3CStartU3Ec__Iterator1_t144_il2cpp_TypeInfo_var);
		U3CStartU3Ec__Iterator1__ctor_m407(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CStartU3Ec__Iterator1_t144 * L_1 = V_0;
		NullCheck(L_1);
		L_1->___U3CU3Ef__this_11 = __this;
		U3CStartU3Ec__Iterator1_t144 * L_2 = V_0;
		return L_2;
	}
}
// UnityStandardAssets.Effects.Explosive/<OnCollisionEnter>c__Iterator2
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Effects_Ex_3.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.Effects.Explosive/<OnCollisionEnter>c__Iterator2
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Effects_Ex_3MethodDeclarations.h"

// UnityStandardAssets.Effects.Explosive
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Effects_Ex_4.h"
// UnityEngine.Collision
#include "UnityEngine_UnityEngine_Collision.h"
// UnityEngine.ContactPoint
#include "UnityEngine_UnityEngine_ContactPoint.h"
// UnityStandardAssets.Utility.ObjectResetter
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Ob_0.h"
// UnityEngine.Behaviour
#include "UnityEngine_UnityEngine_BehaviourMethodDeclarations.h"
// UnityEngine.Collision
#include "UnityEngine_UnityEngine_CollisionMethodDeclarations.h"
// UnityEngine.ContactPoint
#include "UnityEngine_UnityEngine_ContactPointMethodDeclarations.h"
// UnityStandardAssets.Utility.ObjectResetter
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Ob_0MethodDeclarations.h"


// System.Void UnityStandardAssets.Effects.Explosive/<OnCollisionEnter>c__Iterator2::.ctor()
extern "C" void U3COnCollisionEnterU3Ec__Iterator2__ctor_m415 (U3COnCollisionEnterU3Ec__Iterator2_t148 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m677(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object UnityStandardAssets.Effects.Explosive/<OnCollisionEnter>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3COnCollisionEnterU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m416 (U3COnCollisionEnterU3Ec__Iterator2_t148 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_3);
		return L_0;
	}
}
// System.Object UnityStandardAssets.Effects.Explosive/<OnCollisionEnter>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3COnCollisionEnterU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m417 (U3COnCollisionEnterU3Ec__Iterator2_t148 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_3);
		return L_0;
	}
}
// System.Boolean UnityStandardAssets.Effects.Explosive/<OnCollisionEnter>c__Iterator2::MoveNext()
extern "C" bool U3COnCollisionEnterU3Ec__Iterator2_MoveNext_m418 (U3COnCollisionEnterU3Ec__Iterator2_t148 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	Vector3_t4  V_1 = {0};
	bool V_2 = false;
	{
		int32_t L_0 = (__this->___U24PC_2);
		V_0 = L_0;
		__this->___U24PC_2 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_014a;
		}
	}
	{
		goto IL_0151;
	}

IL_0021:
	{
		Explosive_t147 * L_2 = (__this->___U3CU3Ef__this_5);
		NullCheck(L_2);
		bool L_3 = Behaviour_get_enabled_m911(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0137;
		}
	}
	{
		Collision_t146 * L_4 = (__this->___col_0);
		NullCheck(L_4);
		ContactPointU5BU5D_t245* L_5 = Collision_get_contacts_m912(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		if ((((int32_t)(((int32_t)(((Array_t *)L_5)->max_length)))) <= ((int32_t)0)))
		{
			goto IL_0137;
		}
	}
	{
		Collision_t146 * L_6 = (__this->___col_0);
		NullCheck(L_6);
		Vector3_t4  L_7 = Collision_get_relativeVelocity_m913(L_6, /*hidden argument*/NULL);
		Collision_t146 * L_8 = (__this->___col_0);
		NullCheck(L_8);
		ContactPointU5BU5D_t245* L_9 = Collision_get_contacts_m912(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 0);
		Vector3_t4  L_10 = ContactPoint_get_normal_m914(((ContactPoint_t246 *)(ContactPoint_t246 *)SZArrayLdElema(L_9, 0)), /*hidden argument*/NULL);
		Vector3_t4  L_11 = Vector3_Project_m915(NULL /*static, unused*/, L_7, L_10, /*hidden argument*/NULL);
		V_1 = L_11;
		float L_12 = Vector3_get_magnitude_m647((&V_1), /*hidden argument*/NULL);
		__this->___U3CvelocityAlongCollisionNormalU3E__0_1 = L_12;
		float L_13 = (__this->___U3CvelocityAlongCollisionNormalU3E__0_1);
		Explosive_t147 * L_14 = (__this->___U3CU3Ef__this_5);
		NullCheck(L_14);
		float L_15 = (L_14->___detonationImpactVelocity_3);
		if ((((float)L_13) > ((float)L_15)))
		{
			goto IL_009e;
		}
	}
	{
		Explosive_t147 * L_16 = (__this->___U3CU3Ef__this_5);
		NullCheck(L_16);
		bool L_17 = (L_16->___m_Exploded_7);
		if (!L_17)
		{
			goto IL_0137;
		}
	}

IL_009e:
	{
		Explosive_t147 * L_18 = (__this->___U3CU3Ef__this_5);
		NullCheck(L_18);
		bool L_19 = (L_18->___m_Exploded_7);
		if (L_19)
		{
			goto IL_0137;
		}
	}
	{
		Explosive_t147 * L_20 = (__this->___U3CU3Ef__this_5);
		NullCheck(L_20);
		Transform_t1 * L_21 = (L_20->___explosionPrefab_2);
		Collision_t146 * L_22 = (__this->___col_0);
		NullCheck(L_22);
		ContactPointU5BU5D_t245* L_23 = Collision_get_contacts_m912(L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 0);
		Vector3_t4  L_24 = ContactPoint_get_point_m916(((ContactPoint_t246 *)(ContactPoint_t246 *)SZArrayLdElema(L_23, 0)), /*hidden argument*/NULL);
		Collision_t146 * L_25 = (__this->___col_0);
		NullCheck(L_25);
		ContactPointU5BU5D_t245* L_26 = Collision_get_contacts_m912(L_25, /*hidden argument*/NULL);
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, 0);
		Vector3_t4  L_27 = ContactPoint_get_normal_m914(((ContactPoint_t246 *)(ContactPoint_t246 *)SZArrayLdElema(L_26, 0)), /*hidden argument*/NULL);
		Quaternion_t19  L_28 = Quaternion_LookRotation_m917(NULL /*static, unused*/, L_27, /*hidden argument*/NULL);
		Object_Instantiate_m899(NULL /*static, unused*/, L_21, L_24, L_28, /*hidden argument*/NULL);
		Explosive_t147 * L_29 = (__this->___U3CU3Ef__this_5);
		NullCheck(L_29);
		L_29->___m_Exploded_7 = 1;
		Explosive_t147 * L_30 = (__this->___U3CU3Ef__this_5);
		NullCheck(L_30);
		Component_SendMessage_m918(L_30, (String_t*) &_stringLiteral174, /*hidden argument*/NULL);
		Explosive_t147 * L_31 = (__this->___U3CU3Ef__this_5);
		NullCheck(L_31);
		bool L_32 = (L_31->___reset_5);
		if (!L_32)
		{
			goto IL_0137;
		}
	}
	{
		Explosive_t147 * L_33 = (__this->___U3CU3Ef__this_5);
		NullCheck(L_33);
		ObjectResetter_t149 * L_34 = (L_33->___m_ObjectResetter_8);
		Explosive_t147 * L_35 = (__this->___U3CU3Ef__this_5);
		NullCheck(L_35);
		float L_36 = (L_35->___resetTimeDelay_6);
		NullCheck(L_34);
		ObjectResetter_DelayedReset_m515(L_34, L_36, /*hidden argument*/NULL);
	}

IL_0137:
	{
		__this->___U24current_3 = NULL;
		__this->___U24PC_2 = 1;
		goto IL_0153;
	}

IL_014a:
	{
		__this->___U24PC_2 = (-1);
	}

IL_0151:
	{
		return 0;
	}

IL_0153:
	{
		return 1;
	}
	// Dead block : IL_0155: ldloc.2
}
// System.Void UnityStandardAssets.Effects.Explosive/<OnCollisionEnter>c__Iterator2::Dispose()
extern "C" void U3COnCollisionEnterU3Ec__Iterator2_Dispose_m419 (U3COnCollisionEnterU3Ec__Iterator2_t148 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_2 = (-1);
		return;
	}
}
// System.Void UnityStandardAssets.Effects.Explosive/<OnCollisionEnter>c__Iterator2::Reset()
extern TypeInfo* NotSupportedException_t244_il2cpp_TypeInfo_var;
extern "C" void U3COnCollisionEnterU3Ec__Iterator2_Reset_m420 (U3COnCollisionEnterU3Ec__Iterator2_t148 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t244_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t244 * L_0 = (NotSupportedException_t244 *)il2cpp_codegen_object_new (NotSupportedException_t244_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m905(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.Effects.Explosive
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Effects_Ex_4MethodDeclarations.h"

struct Component_t219;
struct ObjectResetter_t149;
// Declaration !!0 UnityEngine.Component::GetComponent<UnityStandardAssets.Utility.ObjectResetter>()
// !!0 UnityEngine.Component::GetComponent<UnityStandardAssets.Utility.ObjectResetter>()
#define Component_GetComponent_TisObjectResetter_t149_m919(__this, method) (( ObjectResetter_t149 * (*) (Component_t219 *, const MethodInfo*))Component_GetComponent_TisObject_t_m614_gshared)(__this, method)


// System.Void UnityStandardAssets.Effects.Explosive::.ctor()
extern "C" void Explosive__ctor_m421 (Explosive_t147 * __this, const MethodInfo* method)
{
	{
		__this->___detonationImpactVelocity_3 = (10.0f);
		__this->___sizeMultiplier_4 = (1.0f);
		__this->___reset_5 = 1;
		__this->___resetTimeDelay_6 = (10.0f);
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Effects.Explosive::Start()
extern const MethodInfo* Component_GetComponent_TisObjectResetter_t149_m919_MethodInfo_var;
extern "C" void Explosive_Start_m422 (Explosive_t147 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisObjectResetter_t149_m919_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483667);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectResetter_t149 * L_0 = Component_GetComponent_TisObjectResetter_t149_m919(__this, /*hidden argument*/Component_GetComponent_TisObjectResetter_t149_m919_MethodInfo_var);
		__this->___m_ObjectResetter_8 = L_0;
		return;
	}
}
// System.Collections.IEnumerator UnityStandardAssets.Effects.Explosive::OnCollisionEnter(UnityEngine.Collision)
extern TypeInfo* U3COnCollisionEnterU3Ec__Iterator2_t148_il2cpp_TypeInfo_var;
extern "C" Object_t * Explosive_OnCollisionEnter_m423 (Explosive_t147 * __this, Collision_t146 * ___col, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3COnCollisionEnterU3Ec__Iterator2_t148_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(73);
		s_Il2CppMethodIntialized = true;
	}
	U3COnCollisionEnterU3Ec__Iterator2_t148 * V_0 = {0};
	{
		U3COnCollisionEnterU3Ec__Iterator2_t148 * L_0 = (U3COnCollisionEnterU3Ec__Iterator2_t148 *)il2cpp_codegen_object_new (U3COnCollisionEnterU3Ec__Iterator2_t148_il2cpp_TypeInfo_var);
		U3COnCollisionEnterU3Ec__Iterator2__ctor_m415(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3COnCollisionEnterU3Ec__Iterator2_t148 * L_1 = V_0;
		Collision_t146 * L_2 = ___col;
		NullCheck(L_1);
		L_1->___col_0 = L_2;
		U3COnCollisionEnterU3Ec__Iterator2_t148 * L_3 = V_0;
		Collision_t146 * L_4 = ___col;
		NullCheck(L_3);
		L_3->___U3CU24U3Ecol_4 = L_4;
		U3COnCollisionEnterU3Ec__Iterator2_t148 * L_5 = V_0;
		NullCheck(L_5);
		L_5->___U3CU3Ef__this_5 = __this;
		U3COnCollisionEnterU3Ec__Iterator2_t148 * L_6 = V_0;
		return L_6;
	}
}
// System.Void UnityStandardAssets.Effects.Explosive::Reset()
extern "C" void Explosive_Reset_m424 (Explosive_t147 * __this, const MethodInfo* method)
{
	{
		__this->___m_Exploded_7 = 0;
		return;
	}
}
// UnityStandardAssets.Effects.ExtinguishableParticleSystem
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Effects_Ex_5.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.Effects.ExtinguishableParticleSystem
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Effects_Ex_5MethodDeclarations.h"

// UnityEngine.ParticleSystem
#include "UnityEngine_UnityEngine_ParticleSystem.h"
// UnityEngine.ParticleSystem
#include "UnityEngine_UnityEngine_ParticleSystemMethodDeclarations.h"
struct Component_t219;
struct ParticleSystemU5BU5D_t150;
struct Component_t219;
struct ObjectU5BU5D_t224;
// Declaration !!0[] UnityEngine.Component::GetComponentsInChildren<System.Object>()
// !!0[] UnityEngine.Component::GetComponentsInChildren<System.Object>()
extern "C" ObjectU5BU5D_t224* Component_GetComponentsInChildren_TisObject_t_m698_gshared (Component_t219 * __this, const MethodInfo* method);
#define Component_GetComponentsInChildren_TisObject_t_m698(__this, method) (( ObjectU5BU5D_t224* (*) (Component_t219 *, const MethodInfo*))Component_GetComponentsInChildren_TisObject_t_m698_gshared)(__this, method)
// Declaration !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.ParticleSystem>()
// !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.ParticleSystem>()
#define Component_GetComponentsInChildren_TisParticleSystem_t161_m920(__this, method) (( ParticleSystemU5BU5D_t150* (*) (Component_t219 *, const MethodInfo*))Component_GetComponentsInChildren_TisObject_t_m698_gshared)(__this, method)


// System.Void UnityStandardAssets.Effects.ExtinguishableParticleSystem::.ctor()
extern "C" void ExtinguishableParticleSystem__ctor_m425 (ExtinguishableParticleSystem_t151 * __this, const MethodInfo* method)
{
	{
		__this->___multiplier_2 = (1.0f);
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Effects.ExtinguishableParticleSystem::Start()
extern const MethodInfo* Component_GetComponentsInChildren_TisParticleSystem_t161_m920_MethodInfo_var;
extern "C" void ExtinguishableParticleSystem_Start_m426 (ExtinguishableParticleSystem_t151 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponentsInChildren_TisParticleSystem_t161_m920_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483668);
		s_Il2CppMethodIntialized = true;
	}
	{
		ParticleSystemU5BU5D_t150* L_0 = Component_GetComponentsInChildren_TisParticleSystem_t161_m920(__this, /*hidden argument*/Component_GetComponentsInChildren_TisParticleSystem_t161_m920_MethodInfo_var);
		__this->___m_Systems_3 = L_0;
		return;
	}
}
// System.Void UnityStandardAssets.Effects.ExtinguishableParticleSystem::Extinguish()
extern "C" void ExtinguishableParticleSystem_Extinguish_m427 (ExtinguishableParticleSystem_t151 * __this, const MethodInfo* method)
{
	ParticleSystem_t161 * V_0 = {0};
	ParticleSystemU5BU5D_t150* V_1 = {0};
	int32_t V_2 = 0;
	{
		ParticleSystemU5BU5D_t150* L_0 = (__this->___m_Systems_3);
		V_1 = L_0;
		V_2 = 0;
		goto IL_001d;
	}

IL_000e:
	{
		ParticleSystemU5BU5D_t150* L_1 = V_1;
		int32_t L_2 = V_2;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		V_0 = (*(ParticleSystem_t161 **)(ParticleSystem_t161 **)SZArrayLdElema(L_1, L_3));
		ParticleSystem_t161 * L_4 = V_0;
		NullCheck(L_4);
		ParticleSystem_set_enableEmission_m921(L_4, 0, /*hidden argument*/NULL);
		int32_t L_5 = V_2;
		V_2 = ((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_001d:
	{
		int32_t L_6 = V_2;
		ParticleSystemU5BU5D_t150* L_7 = V_1;
		NullCheck(L_7);
		if ((((int32_t)L_6) < ((int32_t)(((int32_t)(((Array_t *)L_7)->max_length))))))
		{
			goto IL_000e;
		}
	}
	{
		return;
	}
}
// UnityStandardAssets.Effects.FireLight
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Effects_Fi.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.Effects.FireLight
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Effects_FiMethodDeclarations.h"

// UnityEngine.Light
#include "UnityEngine_UnityEngine_Light.h"
// UnityEngine.Time
#include "UnityEngine_UnityEngine_TimeMethodDeclarations.h"
// UnityEngine.Light
#include "UnityEngine_UnityEngine_LightMethodDeclarations.h"
struct Component_t219;
struct Light_t152;
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.Light>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Light>()
#define Component_GetComponent_TisLight_t152_m922(__this, method) (( Light_t152 * (*) (Component_t219 *, const MethodInfo*))Component_GetComponent_TisObject_t_m614_gshared)(__this, method)


// System.Void UnityStandardAssets.Effects.FireLight::.ctor()
extern "C" void FireLight__ctor_m428 (FireLight_t153 * __this, const MethodInfo* method)
{
	{
		__this->___m_Burning_3 = 1;
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Effects.FireLight::Start()
extern const MethodInfo* Component_GetComponent_TisLight_t152_m922_MethodInfo_var;
extern "C" void FireLight_Start_m429 (FireLight_t153 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisLight_t152_m922_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483669);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = Random_get_value_m858(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___m_Rnd_2 = ((float)((float)L_0*(float)(100.0f)));
		Light_t152 * L_1 = Component_GetComponent_TisLight_t152_m922(__this, /*hidden argument*/Component_GetComponent_TisLight_t152_m922_MethodInfo_var);
		__this->___m_Light_4 = L_1;
		return;
	}
}
// System.Void UnityStandardAssets.Effects.FireLight::Update()
extern TypeInfo* Mathf_t218_il2cpp_TypeInfo_var;
extern "C" void FireLight_Update_m430 (FireLight_t153 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	{
		bool L_0 = (__this->___m_Burning_3);
		if (!L_0)
		{
			goto IL_011a;
		}
	}
	{
		Light_t152 * L_1 = (__this->___m_Light_4);
		float L_2 = (__this->___m_Rnd_2);
		float L_3 = Time_get_time_m668(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_4 = (__this->___m_Rnd_2);
		float L_5 = Time_get_time_m668(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_6 = Mathf_PerlinNoise_m669(NULL /*static, unused*/, ((float)((float)L_2+(float)L_3)), ((float)((float)((float)((float)L_4+(float)(1.0f)))+(float)((float)((float)L_5*(float)(1.0f))))), /*hidden argument*/NULL);
		NullCheck(L_1);
		Light_set_intensity_m923(L_1, ((float)((float)(2.0f)*(float)L_6)), /*hidden argument*/NULL);
		float L_7 = (__this->___m_Rnd_2);
		float L_8 = Time_get_time_m668(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_9 = (__this->___m_Rnd_2);
		float L_10 = Time_get_time_m668(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_11 = Mathf_PerlinNoise_m669(NULL /*static, unused*/, ((float)((float)L_7+(float)((float)((float)L_8*(float)(2.0f))))), ((float)((float)((float)((float)L_9+(float)(1.0f)))+(float)((float)((float)L_10*(float)(2.0f))))), /*hidden argument*/NULL);
		V_0 = ((float)((float)L_11-(float)(0.5f)));
		float L_12 = (__this->___m_Rnd_2);
		float L_13 = Time_get_time_m668(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_14 = (__this->___m_Rnd_2);
		float L_15 = Time_get_time_m668(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_16 = Mathf_PerlinNoise_m669(NULL /*static, unused*/, ((float)((float)((float)((float)L_12+(float)(2.0f)))+(float)((float)((float)L_13*(float)(2.0f))))), ((float)((float)((float)((float)L_14+(float)(3.0f)))+(float)((float)((float)L_15*(float)(2.0f))))), /*hidden argument*/NULL);
		V_1 = ((float)((float)L_16-(float)(0.5f)));
		float L_17 = (__this->___m_Rnd_2);
		float L_18 = Time_get_time_m668(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_19 = (__this->___m_Rnd_2);
		float L_20 = Time_get_time_m668(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_21 = Mathf_PerlinNoise_m669(NULL /*static, unused*/, ((float)((float)((float)((float)L_17+(float)(4.0f)))+(float)((float)((float)L_18*(float)(2.0f))))), ((float)((float)((float)((float)L_19+(float)(5.0f)))+(float)((float)((float)L_20*(float)(2.0f))))), /*hidden argument*/NULL);
		V_2 = ((float)((float)L_21-(float)(0.5f)));
		Transform_t1 * L_22 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		Vector3_t4  L_23 = Vector3_get_up_m642(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_24 = V_0;
		float L_25 = V_1;
		float L_26 = V_2;
		Vector3_t4  L_27 = {0};
		Vector3__ctor_m612(&L_27, L_24, L_25, L_26, /*hidden argument*/NULL);
		Vector3_t4  L_28 = Vector3_op_Multiply_m600(NULL /*static, unused*/, L_27, (1.0f), /*hidden argument*/NULL);
		Vector3_t4  L_29 = Vector3_op_Addition_m604(NULL /*static, unused*/, L_23, L_28, /*hidden argument*/NULL);
		NullCheck(L_22);
		Transform_set_localPosition_m696(L_22, L_29, /*hidden argument*/NULL);
	}

IL_011a:
	{
		return;
	}
}
// System.Void UnityStandardAssets.Effects.FireLight::Extinguish()
extern "C" void FireLight_Extinguish_m431 (FireLight_t153 * __this, const MethodInfo* method)
{
	{
		__this->___m_Burning_3 = 0;
		Light_t152 * L_0 = (__this->___m_Light_4);
		NullCheck(L_0);
		Behaviour_set_enabled_m766(L_0, 0, /*hidden argument*/NULL);
		return;
	}
}
// UnityStandardAssets.Effects.Hose
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Effects_Ho.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.Effects.Hose
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Effects_HoMethodDeclarations.h"

// UnityEngine.KeyCode
#include "UnityEngine_UnityEngine_KeyCode.h"
// UnityEngine.Renderer
#include "UnityEngine_UnityEngine_Renderer.h"
// UnityEngine.Input
#include "UnityEngine_UnityEngine_InputMethodDeclarations.h"
// UnityEngine.Renderer
#include "UnityEngine_UnityEngine_RendererMethodDeclarations.h"


// System.Void UnityStandardAssets.Effects.Hose::.ctor()
extern "C" void Hose__ctor_m432 (Hose_t155 * __this, const MethodInfo* method)
{
	{
		__this->___maxPower_2 = (20.0f);
		__this->___minPower_3 = (5.0f);
		__this->___changeSpeed_4 = (5.0f);
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Effects.Hose::Update()
extern TypeInfo* Input_t220_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t218_il2cpp_TypeInfo_var;
extern "C" void Hose_Update_m433 (Hose_t155 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t220_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Mathf_t218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	ParticleSystem_t161 * V_0 = {0};
	ParticleSystemU5BU5D_t150* V_1 = {0};
	int32_t V_2 = 0;
	float G_B2_0 = 0.0f;
	Hose_t155 * G_B2_1 = {0};
	float G_B1_0 = 0.0f;
	Hose_t155 * G_B1_1 = {0};
	float G_B3_0 = 0.0f;
	float G_B3_1 = 0.0f;
	Hose_t155 * G_B3_2 = {0};
	{
		float L_0 = (__this->___m_Power_7);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t220_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetMouseButton_m924(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		G_B1_0 = L_0;
		G_B1_1 = __this;
		if (!L_1)
		{
			G_B2_0 = L_0;
			G_B2_1 = __this;
			goto IL_001d;
		}
	}
	{
		float L_2 = (__this->___maxPower_2);
		G_B3_0 = L_2;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_0023;
	}

IL_001d:
	{
		float L_3 = (__this->___minPower_3);
		G_B3_0 = L_3;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_0023:
	{
		float L_4 = Time_get_deltaTime_m602(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_5 = (__this->___changeSpeed_4);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_6 = Mathf_Lerp_m610(NULL /*static, unused*/, G_B3_1, G_B3_0, ((float)((float)L_4*(float)L_5)), /*hidden argument*/NULL);
		NullCheck(G_B3_2);
		G_B3_2->___m_Power_7 = L_6;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t220_il2cpp_TypeInfo_var);
		bool L_7 = Input_GetKeyDown_m925(NULL /*static, unused*/, ((int32_t)49), /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_005e;
		}
	}
	{
		Renderer_t154 * L_8 = (__this->___systemRenderer_6);
		Renderer_t154 * L_9 = (__this->___systemRenderer_6);
		NullCheck(L_9);
		bool L_10 = Renderer_get_enabled_m926(L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		Renderer_set_enabled_m927(L_8, ((((int32_t)L_10) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
	}

IL_005e:
	{
		ParticleSystemU5BU5D_t150* L_11 = (__this->___hoseWaterSystems_5);
		V_1 = L_11;
		V_2 = 0;
		goto IL_009a;
	}

IL_006c:
	{
		ParticleSystemU5BU5D_t150* L_12 = V_1;
		int32_t L_13 = V_2;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		int32_t L_14 = L_13;
		V_0 = (*(ParticleSystem_t161 **)(ParticleSystem_t161 **)SZArrayLdElema(L_12, L_14));
		ParticleSystem_t161 * L_15 = V_0;
		float L_16 = (__this->___m_Power_7);
		NullCheck(L_15);
		ParticleSystem_set_startSpeed_m928(L_15, L_16, /*hidden argument*/NULL);
		ParticleSystem_t161 * L_17 = V_0;
		float L_18 = (__this->___m_Power_7);
		float L_19 = (__this->___minPower_3);
		NullCheck(L_17);
		ParticleSystem_set_enableEmission_m921(L_17, ((((float)L_18) > ((float)((float)((float)L_19*(float)(1.1f)))))? 1 : 0), /*hidden argument*/NULL);
		int32_t L_20 = V_2;
		V_2 = ((int32_t)((int32_t)L_20+(int32_t)1));
	}

IL_009a:
	{
		int32_t L_21 = V_2;
		ParticleSystemU5BU5D_t150* L_22 = V_1;
		NullCheck(L_22);
		if ((((int32_t)L_21) < ((int32_t)(((int32_t)(((Array_t *)L_22)->max_length))))))
		{
			goto IL_006c;
		}
	}
	{
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.Effects.ParticleSystemMultiplier
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Effects_PaMethodDeclarations.h"



// System.Void UnityStandardAssets.Effects.ParticleSystemMultiplier::.ctor()
extern "C" void ParticleSystemMultiplier__ctor_m434 (ParticleSystemMultiplier_t156 * __this, const MethodInfo* method)
{
	{
		__this->___multiplier_2 = (1.0f);
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Effects.ParticleSystemMultiplier::Start()
extern TypeInfo* Mathf_t218_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisParticleSystem_t161_m920_MethodInfo_var;
extern "C" void ParticleSystemMultiplier_Start_m435 (ParticleSystemMultiplier_t156 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		Component_GetComponentsInChildren_TisParticleSystem_t161_m920_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483668);
		s_Il2CppMethodIntialized = true;
	}
	ParticleSystemU5BU5D_t150* V_0 = {0};
	ParticleSystem_t161 * V_1 = {0};
	ParticleSystemU5BU5D_t150* V_2 = {0};
	int32_t V_3 = 0;
	{
		ParticleSystemU5BU5D_t150* L_0 = Component_GetComponentsInChildren_TisParticleSystem_t161_m920(__this, /*hidden argument*/Component_GetComponentsInChildren_TisParticleSystem_t161_m920_MethodInfo_var);
		V_0 = L_0;
		ParticleSystemU5BU5D_t150* L_1 = V_0;
		V_2 = L_1;
		V_3 = 0;
		goto IL_006c;
	}

IL_0010:
	{
		ParticleSystemU5BU5D_t150* L_2 = V_2;
		int32_t L_3 = V_3;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		V_1 = (*(ParticleSystem_t161 **)(ParticleSystem_t161 **)SZArrayLdElema(L_2, L_4));
		ParticleSystem_t161 * L_5 = V_1;
		ParticleSystem_t161 * L_6 = L_5;
		NullCheck(L_6);
		float L_7 = ParticleSystem_get_startSize_m929(L_6, /*hidden argument*/NULL);
		float L_8 = (__this->___multiplier_2);
		NullCheck(L_6);
		ParticleSystem_set_startSize_m930(L_6, ((float)((float)L_7*(float)L_8)), /*hidden argument*/NULL);
		ParticleSystem_t161 * L_9 = V_1;
		ParticleSystem_t161 * L_10 = L_9;
		NullCheck(L_10);
		float L_11 = ParticleSystem_get_startSpeed_m931(L_10, /*hidden argument*/NULL);
		float L_12 = (__this->___multiplier_2);
		NullCheck(L_10);
		ParticleSystem_set_startSpeed_m928(L_10, ((float)((float)L_11*(float)L_12)), /*hidden argument*/NULL);
		ParticleSystem_t161 * L_13 = V_1;
		ParticleSystem_t161 * L_14 = L_13;
		NullCheck(L_14);
		float L_15 = ParticleSystem_get_startLifetime_m932(L_14, /*hidden argument*/NULL);
		float L_16 = (__this->___multiplier_2);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_17 = Mathf_Lerp_m610(NULL /*static, unused*/, L_16, (1.0f), (0.5f), /*hidden argument*/NULL);
		NullCheck(L_14);
		ParticleSystem_set_startLifetime_m933(L_14, ((float)((float)L_15*(float)L_17)), /*hidden argument*/NULL);
		ParticleSystem_t161 * L_18 = V_1;
		NullCheck(L_18);
		ParticleSystem_Clear_m934(L_18, /*hidden argument*/NULL);
		ParticleSystem_t161 * L_19 = V_1;
		NullCheck(L_19);
		ParticleSystem_Play_m935(L_19, /*hidden argument*/NULL);
		int32_t L_20 = V_3;
		V_3 = ((int32_t)((int32_t)L_20+(int32_t)1));
	}

IL_006c:
	{
		int32_t L_21 = V_3;
		ParticleSystemU5BU5D_t150* L_22 = V_2;
		NullCheck(L_22);
		if ((((int32_t)L_21) < ((int32_t)(((int32_t)(((Array_t *)L_22)->max_length))))))
		{
			goto IL_0010;
		}
	}
	{
		return;
	}
}
// UnityStandardAssets.Effects.SmokeParticles
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Effects_Sm.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.Effects.SmokeParticles
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Effects_SmMethodDeclarations.h"

// UnityEngine.AudioSource
#include "UnityEngine_UnityEngine_AudioSource.h"
// UnityEngine.AudioClip
#include "UnityEngine_UnityEngine_AudioClip.h"
// UnityEngine.AudioSource
#include "UnityEngine_UnityEngine_AudioSourceMethodDeclarations.h"
struct Component_t219;
struct AudioSource_t247;
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.AudioSource>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.AudioSource>()
#define Component_GetComponent_TisAudioSource_t247_m936(__this, method) (( AudioSource_t247 * (*) (Component_t219 *, const MethodInfo*))Component_GetComponent_TisObject_t_m614_gshared)(__this, method)


// System.Void UnityStandardAssets.Effects.SmokeParticles::.ctor()
extern "C" void SmokeParticles__ctor_m436 (SmokeParticles_t158 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Effects.SmokeParticles::Start()
extern const MethodInfo* Component_GetComponent_TisAudioSource_t247_m936_MethodInfo_var;
extern "C" void SmokeParticles_Start_m437 (SmokeParticles_t158 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisAudioSource_t247_m936_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483670);
		s_Il2CppMethodIntialized = true;
	}
	{
		AudioSource_t247 * L_0 = Component_GetComponent_TisAudioSource_t247_m936(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t247_m936_MethodInfo_var);
		AudioClipU5BU5D_t157* L_1 = (__this->___extinguishSounds_2);
		AudioClipU5BU5D_t157* L_2 = (__this->___extinguishSounds_2);
		NullCheck(L_2);
		int32_t L_3 = Random_Range_m896(NULL /*static, unused*/, 0, (((int32_t)(((Array_t *)L_2)->max_length))), /*hidden argument*/NULL);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_3);
		int32_t L_4 = L_3;
		NullCheck(L_0);
		AudioSource_set_clip_m937(L_0, (*(AudioClip_t248 **)(AudioClip_t248 **)SZArrayLdElema(L_1, L_4)), /*hidden argument*/NULL);
		AudioSource_t247 * L_5 = Component_GetComponent_TisAudioSource_t247_m936(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t247_m936_MethodInfo_var);
		NullCheck(L_5);
		AudioSource_Play_m938(L_5, /*hidden argument*/NULL);
		return;
	}
}
// UnityStandardAssets.Effects.WaterHoseParticles
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Effects_Wa.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.Effects.WaterHoseParticles
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Effects_WaMethodDeclarations.h"

// UnityEngine.ParticleCollisionEvent
#include "UnityEngine_UnityEngine_ParticleCollisionEvent.h"
// UnityEngine.GameObject
#include "UnityEngine_UnityEngine_GameObject.h"
// UnityEngine.SendMessageOptions
#include "UnityEngine_UnityEngine_SendMessageOptions.h"
// UnityEngine.ParticlePhysicsExtensions
#include "UnityEngine_UnityEngine_ParticlePhysicsExtensionsMethodDeclarations.h"
// UnityEngine.ParticleCollisionEvent
#include "UnityEngine_UnityEngine_ParticleCollisionEventMethodDeclarations.h"
// UnityEngine.GameObject
#include "UnityEngine_UnityEngine_GameObjectMethodDeclarations.h"
struct Component_t219;
struct ParticleSystem_t161;
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.ParticleSystem>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.ParticleSystem>()
#define Component_GetComponent_TisParticleSystem_t161_m939(__this, method) (( ParticleSystem_t161 * (*) (Component_t219 *, const MethodInfo*))Component_GetComponent_TisObject_t_m614_gshared)(__this, method)


// System.Void UnityStandardAssets.Effects.WaterHoseParticles::.ctor()
extern TypeInfo* ParticleCollisionEventU5BU5D_t159_il2cpp_TypeInfo_var;
extern "C" void WaterHoseParticles__ctor_m438 (WaterHoseParticles_t162 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParticleCollisionEventU5BU5D_t159_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(77);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___force_3 = (1.0f);
		__this->___m_CollisionEvents_4 = ((ParticleCollisionEventU5BU5D_t159*)SZArrayNew(ParticleCollisionEventU5BU5D_t159_il2cpp_TypeInfo_var, ((int32_t)16)));
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Effects.WaterHoseParticles::Start()
extern const MethodInfo* Component_GetComponent_TisParticleSystem_t161_m939_MethodInfo_var;
extern "C" void WaterHoseParticles_Start_m439 (WaterHoseParticles_t162 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisParticleSystem_t161_m939_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483671);
		s_Il2CppMethodIntialized = true;
	}
	{
		ParticleSystem_t161 * L_0 = Component_GetComponent_TisParticleSystem_t161_m939(__this, /*hidden argument*/Component_GetComponent_TisParticleSystem_t161_m939_MethodInfo_var);
		__this->___m_ParticleSystem_5 = L_0;
		return;
	}
}
// System.Void UnityStandardAssets.Effects.WaterHoseParticles::OnParticleCollision(UnityEngine.GameObject)
extern TypeInfo* ParticleCollisionEventU5BU5D_t159_il2cpp_TypeInfo_var;
extern TypeInfo* WaterHoseParticles_t162_il2cpp_TypeInfo_var;
extern "C" void WaterHoseParticles_OnParticleCollision_m440 (WaterHoseParticles_t162 * __this, GameObject_t78 * ___other, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParticleCollisionEventU5BU5D_t159_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(77);
		WaterHoseParticles_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(79);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Collider_t138 * V_3 = {0};
	Vector3_t4  V_4 = {0};
	{
		ParticleSystem_t161 * L_0 = (__this->___m_ParticleSystem_5);
		int32_t L_1 = ParticlePhysicsExtensions_GetSafeCollisionEventSize_m940(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		ParticleCollisionEventU5BU5D_t159* L_2 = (__this->___m_CollisionEvents_4);
		NullCheck(L_2);
		int32_t L_3 = V_0;
		if ((((int32_t)(((int32_t)(((Array_t *)L_2)->max_length)))) >= ((int32_t)L_3)))
		{
			goto IL_0026;
		}
	}
	{
		int32_t L_4 = V_0;
		__this->___m_CollisionEvents_4 = ((ParticleCollisionEventU5BU5D_t159*)SZArrayNew(ParticleCollisionEventU5BU5D_t159_il2cpp_TypeInfo_var, L_4));
	}

IL_0026:
	{
		ParticleSystem_t161 * L_5 = (__this->___m_ParticleSystem_5);
		GameObject_t78 * L_6 = ___other;
		ParticleCollisionEventU5BU5D_t159* L_7 = (__this->___m_CollisionEvents_4);
		int32_t L_8 = ParticlePhysicsExtensions_GetCollisionEvents_m941(NULL /*static, unused*/, L_5, L_6, L_7, /*hidden argument*/NULL);
		V_1 = L_8;
		V_2 = 0;
		goto IL_00be;
	}

IL_0040:
	{
		float L_9 = Time_get_time_m668(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_10 = ((WaterHoseParticles_t162_StaticFields*)WaterHoseParticles_t162_il2cpp_TypeInfo_var->static_fields)->___lastSoundTime_2;
		if ((!(((float)L_9) > ((float)((float)((float)L_10+(float)(0.2f)))))))
		{
			goto IL_005f;
		}
	}
	{
		float L_11 = Time_get_time_m668(NULL /*static, unused*/, /*hidden argument*/NULL);
		((WaterHoseParticles_t162_StaticFields*)WaterHoseParticles_t162_il2cpp_TypeInfo_var->static_fields)->___lastSoundTime_2 = L_11;
	}

IL_005f:
	{
		ParticleCollisionEventU5BU5D_t159* L_12 = (__this->___m_CollisionEvents_4);
		int32_t L_13 = V_2;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		Collider_t138 * L_14 = ParticleCollisionEvent_get_collider_m942(((ParticleCollisionEvent_t160 *)(ParticleCollisionEvent_t160 *)SZArrayLdElema(L_12, L_13)), /*hidden argument*/NULL);
		V_3 = L_14;
		Collider_t138 * L_15 = V_3;
		NullCheck(L_15);
		Rigidbody_t14 * L_16 = Collider_get_attachedRigidbody_m687(L_15, /*hidden argument*/NULL);
		bool L_17 = Object_op_Inequality_m623(NULL /*static, unused*/, L_16, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_00ae;
		}
	}
	{
		ParticleCollisionEventU5BU5D_t159* L_18 = (__this->___m_CollisionEvents_4);
		int32_t L_19 = V_2;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, L_19);
		Vector3_t4  L_20 = ParticleCollisionEvent_get_velocity_m943(((ParticleCollisionEvent_t160 *)(ParticleCollisionEvent_t160 *)SZArrayLdElema(L_18, L_19)), /*hidden argument*/NULL);
		V_4 = L_20;
		Collider_t138 * L_21 = V_3;
		NullCheck(L_21);
		Rigidbody_t14 * L_22 = Collider_get_attachedRigidbody_m687(L_21, /*hidden argument*/NULL);
		Vector3_t4  L_23 = V_4;
		float L_24 = (__this->___force_3);
		Vector3_t4  L_25 = Vector3_op_Multiply_m600(NULL /*static, unused*/, L_23, L_24, /*hidden argument*/NULL);
		NullCheck(L_22);
		Rigidbody_AddForce_m944(L_22, L_25, 1, /*hidden argument*/NULL);
	}

IL_00ae:
	{
		GameObject_t78 * L_26 = ___other;
		NullCheck(L_26);
		GameObject_BroadcastMessage_m945(L_26, (String_t*) &_stringLiteral175, 1, /*hidden argument*/NULL);
		int32_t L_27 = V_2;
		V_2 = ((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_00be:
	{
		int32_t L_28 = V_2;
		int32_t L_29 = V_1;
		if ((((int32_t)L_28) < ((int32_t)L_29)))
		{
			goto IL_0040;
		}
	}
	{
		return;
	}
}
// UnityStandardAssets.Utility.ActivateTrigger/Mode
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Ac.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.Utility.ActivateTrigger/Mode
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_AcMethodDeclarations.h"



// UnityStandardAssets.Utility.ActivateTrigger
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Ac_0.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.Utility.ActivateTrigger
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Ac_0MethodDeclarations.h"

// UnityEngine.Behaviour
#include "UnityEngine_UnityEngine_Behaviour.h"
// UnityEngine.Animation
#include "UnityEngine_UnityEngine_Animation.h"
// UnityEngine.Animation
#include "UnityEngine_UnityEngine_AnimationMethodDeclarations.h"
struct GameObject_t78;
struct Animation_t249;
struct GameObject_t78;
struct Object_t;
// Declaration !!0 UnityEngine.GameObject::GetComponent<System.Object>()
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C" Object_t * GameObject_GetComponent_TisObject_t_m771_gshared (GameObject_t78 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisObject_t_m771(__this, method) (( Object_t * (*) (GameObject_t78 *, const MethodInfo*))GameObject_GetComponent_TisObject_t_m771_gshared)(__this, method)
// Declaration !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Animation>()
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Animation>()
#define GameObject_GetComponent_TisAnimation_t249_m946(__this, method) (( Animation_t249 * (*) (GameObject_t78 *, const MethodInfo*))GameObject_GetComponent_TisObject_t_m771_gshared)(__this, method)


// System.Void UnityStandardAssets.Utility.ActivateTrigger::.ctor()
extern "C" void ActivateTrigger__ctor_m441 (ActivateTrigger_t165 * __this, const MethodInfo* method)
{
	{
		__this->___action_2 = 2;
		__this->___triggerCount_5 = 1;
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Utility.ActivateTrigger::DoActivateTrigger()
extern TypeInfo* Behaviour_t250_il2cpp_TypeInfo_var;
extern TypeInfo* GameObject_t78_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimation_t249_m946_MethodInfo_var;
extern "C" void ActivateTrigger_DoActivateTrigger_m442 (ActivateTrigger_t165 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Behaviour_t250_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(81);
		GameObject_t78_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(42);
		GameObject_GetComponent_TisAnimation_t249_m946_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483672);
		s_Il2CppMethodIntialized = true;
	}
	Object_t164 * V_0 = {0};
	Behaviour_t250 * V_1 = {0};
	GameObject_t78 * V_2 = {0};
	int32_t V_3 = {0};
	Object_t164 * G_B4_0 = {0};
	Object_t164 * G_B3_0 = {0};
	{
		int32_t L_0 = (__this->___triggerCount_5);
		__this->___triggerCount_5 = ((int32_t)((int32_t)L_0-(int32_t)1));
		int32_t L_1 = (__this->___triggerCount_5);
		if (!L_1)
		{
			goto IL_0024;
		}
	}
	{
		bool L_2 = (__this->___repeatTrigger_6);
		if (!L_2)
		{
			goto IL_014e;
		}
	}

IL_0024:
	{
		Object_t164 * L_3 = (__this->___target_3);
		Object_t164 * L_4 = L_3;
		G_B3_0 = L_4;
		if (L_4)
		{
			G_B4_0 = L_4;
			goto IL_0037;
		}
	}
	{
		GameObject_t78 * L_5 = Component_get_gameObject_m622(__this, /*hidden argument*/NULL);
		G_B4_0 = ((Object_t164 *)(L_5));
	}

IL_0037:
	{
		V_0 = G_B4_0;
		Object_t164 * L_6 = V_0;
		V_1 = ((Behaviour_t250 *)IsInst(L_6, Behaviour_t250_il2cpp_TypeInfo_var));
		Object_t164 * L_7 = V_0;
		V_2 = ((GameObject_t78 *)IsInst(L_7, GameObject_t78_il2cpp_TypeInfo_var));
		Behaviour_t250 * L_8 = V_1;
		bool L_9 = Object_op_Inequality_m623(NULL /*static, unused*/, L_8, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0059;
		}
	}
	{
		Behaviour_t250 * L_10 = V_1;
		NullCheck(L_10);
		GameObject_t78 * L_11 = Component_get_gameObject_m622(L_10, /*hidden argument*/NULL);
		V_2 = L_11;
	}

IL_0059:
	{
		int32_t L_12 = (__this->___action_2);
		V_3 = L_12;
		int32_t L_13 = V_3;
		if (L_13 == 0)
		{
			goto IL_0083;
		}
		if (L_13 == 1)
		{
			goto IL_009f;
		}
		if (L_13 == 2)
		{
			goto IL_00e9;
		}
		if (L_13 == 3)
		{
			goto IL_0101;
		}
		if (L_13 == 4)
		{
			goto IL_0119;
		}
		if (L_13 == 5)
		{
			goto IL_0136;
		}
	}
	{
		goto IL_014e;
	}

IL_0083:
	{
		GameObject_t78 * L_14 = V_2;
		bool L_15 = Object_op_Inequality_m623(NULL /*static, unused*/, L_14, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_009a;
		}
	}
	{
		GameObject_t78 * L_16 = V_2;
		NullCheck(L_16);
		GameObject_BroadcastMessage_m947(L_16, (String_t*) &_stringLiteral176, /*hidden argument*/NULL);
	}

IL_009a:
	{
		goto IL_014e;
	}

IL_009f:
	{
		GameObject_t78 * L_17 = (__this->___source_4);
		bool L_18 = Object_op_Inequality_m623(NULL /*static, unused*/, L_17, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_00e4;
		}
	}
	{
		GameObject_t78 * L_19 = V_2;
		bool L_20 = Object_op_Inequality_m623(NULL /*static, unused*/, L_19, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_00e4;
		}
	}
	{
		GameObject_t78 * L_21 = (__this->___source_4);
		GameObject_t78 * L_22 = V_2;
		NullCheck(L_22);
		Transform_t1 * L_23 = GameObject_get_transform_m609(L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		Vector3_t4  L_24 = Transform_get_position_m593(L_23, /*hidden argument*/NULL);
		GameObject_t78 * L_25 = V_2;
		NullCheck(L_25);
		Transform_t1 * L_26 = GameObject_get_transform_m609(L_25, /*hidden argument*/NULL);
		NullCheck(L_26);
		Quaternion_t19  L_27 = Transform_get_rotation_m656(L_26, /*hidden argument*/NULL);
		Object_Instantiate_m899(NULL /*static, unused*/, L_21, L_24, L_27, /*hidden argument*/NULL);
		GameObject_t78 * L_28 = V_2;
		Object_DestroyObject_m948(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
	}

IL_00e4:
	{
		goto IL_014e;
	}

IL_00e9:
	{
		GameObject_t78 * L_29 = V_2;
		bool L_30 = Object_op_Inequality_m623(NULL /*static, unused*/, L_29, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_30)
		{
			goto IL_00fc;
		}
	}
	{
		GameObject_t78 * L_31 = V_2;
		NullCheck(L_31);
		GameObject_SetActive_m713(L_31, 1, /*hidden argument*/NULL);
	}

IL_00fc:
	{
		goto IL_014e;
	}

IL_0101:
	{
		Behaviour_t250 * L_32 = V_1;
		bool L_33 = Object_op_Inequality_m623(NULL /*static, unused*/, L_32, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_33)
		{
			goto IL_0114;
		}
	}
	{
		Behaviour_t250 * L_34 = V_1;
		NullCheck(L_34);
		Behaviour_set_enabled_m766(L_34, 1, /*hidden argument*/NULL);
	}

IL_0114:
	{
		goto IL_014e;
	}

IL_0119:
	{
		GameObject_t78 * L_35 = V_2;
		bool L_36 = Object_op_Inequality_m623(NULL /*static, unused*/, L_35, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_36)
		{
			goto IL_0131;
		}
	}
	{
		GameObject_t78 * L_37 = V_2;
		NullCheck(L_37);
		Animation_t249 * L_38 = GameObject_GetComponent_TisAnimation_t249_m946(L_37, /*hidden argument*/GameObject_GetComponent_TisAnimation_t249_m946_MethodInfo_var);
		NullCheck(L_38);
		Animation_Play_m949(L_38, /*hidden argument*/NULL);
	}

IL_0131:
	{
		goto IL_014e;
	}

IL_0136:
	{
		GameObject_t78 * L_39 = V_2;
		bool L_40 = Object_op_Inequality_m623(NULL /*static, unused*/, L_39, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_40)
		{
			goto IL_0149;
		}
	}
	{
		GameObject_t78 * L_41 = V_2;
		NullCheck(L_41);
		GameObject_SetActive_m713(L_41, 0, /*hidden argument*/NULL);
	}

IL_0149:
	{
		goto IL_014e;
	}

IL_014e:
	{
		return;
	}
}
// System.Void UnityStandardAssets.Utility.ActivateTrigger::OnTriggerEnter(UnityEngine.Collider)
extern "C" void ActivateTrigger_OnTriggerEnter_m443 (ActivateTrigger_t165 * __this, Collider_t138 * ___other, const MethodInfo* method)
{
	{
		ActivateTrigger_DoActivateTrigger_m442(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementDefinition
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Au.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementDefinition
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_AuMethodDeclarations.h"



// System.Void UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementDefinition::.ctor()
extern "C" void ReplacementDefinition__ctor_m444 (ReplacementDefinition_t166 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m677(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementList
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Au_0.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementList
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Au_0MethodDeclarations.h"

#include "Assembly-CSharp-firstpass_ArrayTypes.h"


// System.Void UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementList::.ctor()
extern TypeInfo* ReplacementDefinitionU5BU5D_t167_il2cpp_TypeInfo_var;
extern "C" void ReplacementList__ctor_m445 (ReplacementList_t168 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReplacementDefinitionU5BU5D_t167_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___items_0 = ((ReplacementDefinitionU5BU5D_t167*)SZArrayNew(ReplacementDefinitionU5BU5D_t167_il2cpp_TypeInfo_var, 0));
		Object__ctor_m677(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityStandardAssets.Utility.AutoMobileShaderSwitch
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Au_1.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.Utility.AutoMobileShaderSwitch
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Au_1MethodDeclarations.h"

// System.Collections.Generic.List`1<UnityEngine.Material>
#include "mscorlib_System_Collections_Generic_List_1_gen_1.h"
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
// UnityEngine.Debug
#include "UnityEngine_UnityEngine_DebugMethodDeclarations.h"
// System.Collections.Generic.List`1<UnityEngine.Material>
#include "mscorlib_System_Collections_Generic_List_1_gen_1MethodDeclarations.h"
struct Object_t164;
struct RendererU5BU5D_t223;
struct Object_t164;
struct ObjectU5BU5D_t224;
// Declaration !!0[] UnityEngine.Object::FindObjectsOfType<System.Object>()
// !!0[] UnityEngine.Object::FindObjectsOfType<System.Object>()
extern "C" ObjectU5BU5D_t224* Object_FindObjectsOfType_TisObject_t_m951_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define Object_FindObjectsOfType_TisObject_t_m951(__this /* static, unused */, method) (( ObjectU5BU5D_t224* (*) (Object_t * /* static, unused */, const MethodInfo*))Object_FindObjectsOfType_TisObject_t_m951_gshared)(__this /* static, unused */, method)
// Declaration !!0[] UnityEngine.Object::FindObjectsOfType<UnityEngine.Renderer>()
// !!0[] UnityEngine.Object::FindObjectsOfType<UnityEngine.Renderer>()
#define Object_FindObjectsOfType_TisRenderer_t154_m950(__this /* static, unused */, method) (( RendererU5BU5D_t223* (*) (Object_t * /* static, unused */, const MethodInfo*))Object_FindObjectsOfType_TisObject_t_m951_gshared)(__this /* static, unused */, method)
struct Object_t164;
struct Material_t55;
struct Object_t164;
struct Object_t;
// Declaration !!0 UnityEngine.Object::Instantiate<System.Object>(!!0)
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0)
extern "C" Object_t * Object_Instantiate_TisObject_t_m953_gshared (Object_t * __this /* static, unused */, Object_t * p0, const MethodInfo* method);
#define Object_Instantiate_TisObject_t_m953(__this /* static, unused */, p0, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Object_Instantiate_TisObject_t_m953_gshared)(__this /* static, unused */, p0, method)
// Declaration !!0 UnityEngine.Object::Instantiate<UnityEngine.Material>(!!0)
// !!0 UnityEngine.Object::Instantiate<UnityEngine.Material>(!!0)
#define Object_Instantiate_TisMaterial_t55_m952(__this /* static, unused */, p0, method) (( Material_t55 * (*) (Object_t * /* static, unused */, Material_t55 *, const MethodInfo*))Object_Instantiate_TisObject_t_m953_gshared)(__this /* static, unused */, p0, method)


// System.Void UnityStandardAssets.Utility.AutoMobileShaderSwitch::.ctor()
extern "C" void AutoMobileShaderSwitch__ctor_m446 (AutoMobileShaderSwitch_t169 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Utility.AutoMobileShaderSwitch::OnEnable()
extern TypeInfo* Int32_t253_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t251_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t224_il2cpp_TypeInfo_var;
extern TypeInfo* StringU5BU5D_t243_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_FindObjectsOfType_TisRenderer_t154_m950_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m955_MethodInfo_var;
extern const MethodInfo* Object_Instantiate_TisMaterial_t55_m952_MethodInfo_var;
extern "C" void AutoMobileShaderSwitch_OnEnable_m447 (AutoMobileShaderSwitch_t169 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32_t253_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(54);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		List_1_t251_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(84);
		ObjectU5BU5D_t224_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		StringU5BU5D_t243_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(58);
		Object_FindObjectsOfType_TisRenderer_t154_m950_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483673);
		List_1__ctor_m955_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483674);
		Object_Instantiate_TisMaterial_t55_m952_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483675);
		s_Il2CppMethodIntialized = true;
	}
	RendererU5BU5D_t223* V_0 = {0};
	List_1_t251 * V_1 = {0};
	List_1_t251 * V_2 = {0};
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	ReplacementDefinition_t166 * V_5 = {0};
	ReplacementDefinitionU5BU5D_t167* V_6 = {0};
	int32_t V_7 = 0;
	Renderer_t154 * V_8 = {0};
	RendererU5BU5D_t223* V_9 = {0};
	int32_t V_10 = 0;
	MaterialU5BU5D_t252* V_11 = {0};
	int32_t V_12 = 0;
	Material_t55 * V_13 = {0};
	Material_t55 * V_14 = {0};
	int32_t V_15 = 0;
	{
		RendererU5BU5D_t223* L_0 = Object_FindObjectsOfType_TisRenderer_t154_m950(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectsOfType_TisRenderer_t154_m950_MethodInfo_var);
		V_0 = L_0;
		RendererU5BU5D_t223* L_1 = V_0;
		NullCheck(L_1);
		int32_t L_2 = (((int32_t)(((Array_t *)L_1)->max_length)));
		Object_t * L_3 = Box(Int32_t253_il2cpp_TypeInfo_var, &L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m954(NULL /*static, unused*/, L_3, (String_t*) &_stringLiteral177, /*hidden argument*/NULL);
		Debug_Log_m857(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		List_1_t251 * L_5 = (List_1_t251 *)il2cpp_codegen_object_new (List_1_t251_il2cpp_TypeInfo_var);
		List_1__ctor_m955(L_5, /*hidden argument*/List_1__ctor_m955_MethodInfo_var);
		V_1 = L_5;
		List_1_t251 * L_6 = (List_1_t251 *)il2cpp_codegen_object_new (List_1_t251_il2cpp_TypeInfo_var);
		List_1__ctor_m955(L_6, /*hidden argument*/List_1__ctor_m955_MethodInfo_var);
		V_2 = L_6;
		V_3 = 0;
		V_4 = 0;
		ReplacementList_t168 * L_7 = (__this->___m_ReplacementList_2);
		NullCheck(L_7);
		ReplacementDefinitionU5BU5D_t167* L_8 = (L_7->___items_0);
		V_6 = L_8;
		V_7 = 0;
		goto IL_0180;
	}

IL_0043:
	{
		ReplacementDefinitionU5BU5D_t167* L_9 = V_6;
		int32_t L_10 = V_7;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		V_5 = (*(ReplacementDefinition_t166 **)(ReplacementDefinition_t166 **)SZArrayLdElema(L_9, L_11));
		RendererU5BU5D_t223* L_12 = V_0;
		V_9 = L_12;
		V_10 = 0;
		goto IL_016f;
	}

IL_0055:
	{
		RendererU5BU5D_t223* L_13 = V_9;
		int32_t L_14 = V_10;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		int32_t L_15 = L_14;
		V_8 = (*(Renderer_t154 **)(Renderer_t154 **)SZArrayLdElema(L_13, L_15));
		V_11 = (MaterialU5BU5D_t252*)NULL;
		V_12 = 0;
		goto IL_0149;
	}

IL_0067:
	{
		Renderer_t154 * L_16 = V_8;
		NullCheck(L_16);
		MaterialU5BU5D_t252* L_17 = Renderer_get_sharedMaterials_m956(L_16, /*hidden argument*/NULL);
		int32_t L_18 = V_12;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		int32_t L_19 = L_18;
		V_13 = (*(Material_t55 **)(Material_t55 **)SZArrayLdElema(L_17, L_19));
		Material_t55 * L_20 = V_13;
		NullCheck(L_20);
		Shader_t54 * L_21 = Material_get_shader_m767(L_20, /*hidden argument*/NULL);
		ReplacementDefinition_t166 * L_22 = V_5;
		NullCheck(L_22);
		Shader_t54 * L_23 = (L_22->___original_0);
		bool L_24 = Object_op_Equality_m640(NULL /*static, unused*/, L_21, L_23, /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_0143;
		}
	}
	{
		MaterialU5BU5D_t252* L_25 = V_11;
		if (L_25)
		{
			goto IL_009b;
		}
	}
	{
		Renderer_t154 * L_26 = V_8;
		NullCheck(L_26);
		MaterialU5BU5D_t252* L_27 = Renderer_get_materials_m957(L_26, /*hidden argument*/NULL);
		V_11 = L_27;
	}

IL_009b:
	{
		List_1_t251 * L_28 = V_1;
		Material_t55 * L_29 = V_13;
		NullCheck(L_28);
		bool L_30 = (bool)VirtFuncInvoker1< bool, Material_t55 * >::Invoke(24 /* System.Boolean System.Collections.Generic.List`1<UnityEngine.Material>::Contains(!0) */, L_28, L_29);
		if (L_30)
		{
			goto IL_00d3;
		}
	}
	{
		List_1_t251 * L_31 = V_1;
		Material_t55 * L_32 = V_13;
		NullCheck(L_31);
		VirtActionInvoker1< Material_t55 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.Material>::Add(!0) */, L_31, L_32);
		Material_t55 * L_33 = V_13;
		Material_t55 * L_34 = Object_Instantiate_TisMaterial_t55_m952(NULL /*static, unused*/, L_33, /*hidden argument*/Object_Instantiate_TisMaterial_t55_m952_MethodInfo_var);
		V_14 = L_34;
		Material_t55 * L_35 = V_14;
		ReplacementDefinition_t166 * L_36 = V_5;
		NullCheck(L_36);
		Shader_t54 * L_37 = (L_36->___replacement_1);
		NullCheck(L_35);
		Material_set_shader_m958(L_35, L_37, /*hidden argument*/NULL);
		List_1_t251 * L_38 = V_2;
		Material_t55 * L_39 = V_14;
		NullCheck(L_38);
		VirtActionInvoker1< Material_t55 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.Material>::Add(!0) */, L_38, L_39);
		int32_t L_40 = V_3;
		V_3 = ((int32_t)((int32_t)L_40+(int32_t)1));
	}

IL_00d3:
	{
		ObjectU5BU5D_t224* L_41 = ((ObjectU5BU5D_t224*)SZArrayNew(ObjectU5BU5D_t224_il2cpp_TypeInfo_var, 6));
		NullCheck(L_41);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_41, 0);
		ArrayElementTypeCheck (L_41, (String_t*) &_stringLiteral178);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_41, 0)) = (Object_t *)(String_t*) &_stringLiteral178;
		ObjectU5BU5D_t224* L_42 = L_41;
		Renderer_t154 * L_43 = V_8;
		NullCheck(L_43);
		GameObject_t78 * L_44 = Component_get_gameObject_m622(L_43, /*hidden argument*/NULL);
		NullCheck(L_44);
		String_t* L_45 = Object_get_name_m798(L_44, /*hidden argument*/NULL);
		NullCheck(L_42);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_42, 1);
		ArrayElementTypeCheck (L_42, L_45);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_42, 1)) = (Object_t *)L_45;
		ObjectU5BU5D_t224* L_46 = L_42;
		NullCheck(L_46);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_46, 2);
		ArrayElementTypeCheck (L_46, (String_t*) &_stringLiteral179);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_46, 2)) = (Object_t *)(String_t*) &_stringLiteral179;
		ObjectU5BU5D_t224* L_47 = L_46;
		int32_t L_48 = V_12;
		int32_t L_49 = L_48;
		Object_t * L_50 = Box(Int32_t253_il2cpp_TypeInfo_var, &L_49);
		NullCheck(L_47);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_47, 3);
		ArrayElementTypeCheck (L_47, L_50);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_47, 3)) = (Object_t *)L_50;
		ObjectU5BU5D_t224* L_51 = L_47;
		NullCheck(L_51);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_51, 4);
		ArrayElementTypeCheck (L_51, (String_t*) &_stringLiteral180);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_51, 4)) = (Object_t *)(String_t*) &_stringLiteral180;
		ObjectU5BU5D_t224* L_52 = L_51;
		List_1_t251 * L_53 = V_2;
		List_1_t251 * L_54 = V_1;
		Material_t55 * L_55 = V_13;
		NullCheck(L_54);
		int32_t L_56 = (int32_t)VirtFuncInvoker1< int32_t, Material_t55 * >::Invoke(28 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.Material>::IndexOf(!0) */, L_54, L_55);
		NullCheck(L_53);
		Material_t55 * L_57 = (Material_t55 *)VirtFuncInvoker1< Material_t55 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Material>::get_Item(System.Int32) */, L_53, L_56);
		NullCheck(L_57);
		String_t* L_58 = Object_get_name_m798(L_57, /*hidden argument*/NULL);
		NullCheck(L_52);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_52, 5);
		ArrayElementTypeCheck (L_52, L_58);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_52, 5)) = (Object_t *)L_58;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_59 = String_Concat_m959(NULL /*static, unused*/, L_52, /*hidden argument*/NULL);
		Debug_Log_m857(NULL /*static, unused*/, L_59, /*hidden argument*/NULL);
		MaterialU5BU5D_t252* L_60 = V_11;
		int32_t L_61 = V_12;
		List_1_t251 * L_62 = V_2;
		List_1_t251 * L_63 = V_1;
		Material_t55 * L_64 = V_13;
		NullCheck(L_63);
		int32_t L_65 = (int32_t)VirtFuncInvoker1< int32_t, Material_t55 * >::Invoke(28 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.Material>::IndexOf(!0) */, L_63, L_64);
		NullCheck(L_62);
		Material_t55 * L_66 = (Material_t55 *)VirtFuncInvoker1< Material_t55 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Material>::get_Item(System.Int32) */, L_62, L_65);
		NullCheck(L_60);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_60, L_61);
		ArrayElementTypeCheck (L_60, L_66);
		*((Material_t55 **)(Material_t55 **)SZArrayLdElema(L_60, L_61)) = (Material_t55 *)L_66;
		int32_t L_67 = V_4;
		V_4 = ((int32_t)((int32_t)L_67+(int32_t)1));
	}

IL_0143:
	{
		int32_t L_68 = V_12;
		V_12 = ((int32_t)((int32_t)L_68+(int32_t)1));
	}

IL_0149:
	{
		int32_t L_69 = V_12;
		Renderer_t154 * L_70 = V_8;
		NullCheck(L_70);
		MaterialU5BU5D_t252* L_71 = Renderer_get_sharedMaterials_m956(L_70, /*hidden argument*/NULL);
		NullCheck(L_71);
		if ((((int32_t)L_69) < ((int32_t)(((int32_t)(((Array_t *)L_71)->max_length))))))
		{
			goto IL_0067;
		}
	}
	{
		MaterialU5BU5D_t252* L_72 = V_11;
		if (!L_72)
		{
			goto IL_0169;
		}
	}
	{
		Renderer_t154 * L_73 = V_8;
		MaterialU5BU5D_t252* L_74 = V_11;
		NullCheck(L_73);
		Renderer_set_materials_m960(L_73, L_74, /*hidden argument*/NULL);
	}

IL_0169:
	{
		int32_t L_75 = V_10;
		V_10 = ((int32_t)((int32_t)L_75+(int32_t)1));
	}

IL_016f:
	{
		int32_t L_76 = V_10;
		RendererU5BU5D_t223* L_77 = V_9;
		NullCheck(L_77);
		if ((((int32_t)L_76) < ((int32_t)(((int32_t)(((Array_t *)L_77)->max_length))))))
		{
			goto IL_0055;
		}
	}
	{
		int32_t L_78 = V_7;
		V_7 = ((int32_t)((int32_t)L_78+(int32_t)1));
	}

IL_0180:
	{
		int32_t L_79 = V_7;
		ReplacementDefinitionU5BU5D_t167* L_80 = V_6;
		NullCheck(L_80);
		if ((((int32_t)L_79) < ((int32_t)(((int32_t)(((Array_t *)L_80)->max_length))))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_81 = V_4;
		int32_t L_82 = L_81;
		Object_t * L_83 = Box(Int32_t253_il2cpp_TypeInfo_var, &L_82);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_84 = String_Concat_m954(NULL /*static, unused*/, L_83, (String_t*) &_stringLiteral181, /*hidden argument*/NULL);
		Debug_Log_m857(NULL /*static, unused*/, L_84, /*hidden argument*/NULL);
		int32_t L_85 = V_3;
		int32_t L_86 = L_85;
		Object_t * L_87 = Box(Int32_t253_il2cpp_TypeInfo_var, &L_86);
		String_t* L_88 = String_Concat_m954(NULL /*static, unused*/, L_87, (String_t*) &_stringLiteral182, /*hidden argument*/NULL);
		Debug_Log_m857(NULL /*static, unused*/, L_88, /*hidden argument*/NULL);
		V_15 = 0;
		goto IL_023e;
	}

IL_01be:
	{
		StringU5BU5D_t243* L_89 = ((StringU5BU5D_t243*)SZArrayNew(StringU5BU5D_t243_il2cpp_TypeInfo_var, 8));
		List_1_t251 * L_90 = V_1;
		int32_t L_91 = V_15;
		NullCheck(L_90);
		Material_t55 * L_92 = (Material_t55 *)VirtFuncInvoker1< Material_t55 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Material>::get_Item(System.Int32) */, L_90, L_91);
		NullCheck(L_92);
		String_t* L_93 = Object_get_name_m798(L_92, /*hidden argument*/NULL);
		NullCheck(L_89);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_89, 0);
		ArrayElementTypeCheck (L_89, L_93);
		*((String_t**)(String_t**)SZArrayLdElema(L_89, 0)) = (String_t*)L_93;
		StringU5BU5D_t243* L_94 = L_89;
		NullCheck(L_94);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_94, 1);
		ArrayElementTypeCheck (L_94, (String_t*) &_stringLiteral183);
		*((String_t**)(String_t**)SZArrayLdElema(L_94, 1)) = (String_t*)(String_t*) &_stringLiteral183;
		StringU5BU5D_t243* L_95 = L_94;
		List_1_t251 * L_96 = V_1;
		int32_t L_97 = V_15;
		NullCheck(L_96);
		Material_t55 * L_98 = (Material_t55 *)VirtFuncInvoker1< Material_t55 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Material>::get_Item(System.Int32) */, L_96, L_97);
		NullCheck(L_98);
		Shader_t54 * L_99 = Material_get_shader_m767(L_98, /*hidden argument*/NULL);
		NullCheck(L_99);
		String_t* L_100 = Object_get_name_m798(L_99, /*hidden argument*/NULL);
		NullCheck(L_95);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_95, 2);
		ArrayElementTypeCheck (L_95, L_100);
		*((String_t**)(String_t**)SZArrayLdElema(L_95, 2)) = (String_t*)L_100;
		StringU5BU5D_t243* L_101 = L_95;
		NullCheck(L_101);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_101, 3);
		ArrayElementTypeCheck (L_101, (String_t*) &_stringLiteral184);
		*((String_t**)(String_t**)SZArrayLdElema(L_101, 3)) = (String_t*)(String_t*) &_stringLiteral184;
		StringU5BU5D_t243* L_102 = L_101;
		List_1_t251 * L_103 = V_2;
		int32_t L_104 = V_15;
		NullCheck(L_103);
		Material_t55 * L_105 = (Material_t55 *)VirtFuncInvoker1< Material_t55 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Material>::get_Item(System.Int32) */, L_103, L_104);
		NullCheck(L_105);
		String_t* L_106 = Object_get_name_m798(L_105, /*hidden argument*/NULL);
		NullCheck(L_102);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_102, 4);
		ArrayElementTypeCheck (L_102, L_106);
		*((String_t**)(String_t**)SZArrayLdElema(L_102, 4)) = (String_t*)L_106;
		StringU5BU5D_t243* L_107 = L_102;
		NullCheck(L_107);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_107, 5);
		ArrayElementTypeCheck (L_107, (String_t*) &_stringLiteral183);
		*((String_t**)(String_t**)SZArrayLdElema(L_107, 5)) = (String_t*)(String_t*) &_stringLiteral183;
		StringU5BU5D_t243* L_108 = L_107;
		List_1_t251 * L_109 = V_2;
		int32_t L_110 = V_15;
		NullCheck(L_109);
		Material_t55 * L_111 = (Material_t55 *)VirtFuncInvoker1< Material_t55 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Material>::get_Item(System.Int32) */, L_109, L_110);
		NullCheck(L_111);
		Shader_t54 * L_112 = Material_get_shader_m767(L_111, /*hidden argument*/NULL);
		NullCheck(L_112);
		String_t* L_113 = Object_get_name_m798(L_112, /*hidden argument*/NULL);
		NullCheck(L_108);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_108, 6);
		ArrayElementTypeCheck (L_108, L_113);
		*((String_t**)(String_t**)SZArrayLdElema(L_108, 6)) = (String_t*)L_113;
		StringU5BU5D_t243* L_114 = L_108;
		NullCheck(L_114);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_114, 7);
		ArrayElementTypeCheck (L_114, (String_t*) &_stringLiteral185);
		*((String_t**)(String_t**)SZArrayLdElema(L_114, 7)) = (String_t*)(String_t*) &_stringLiteral185;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_115 = String_Concat_m861(NULL /*static, unused*/, L_114, /*hidden argument*/NULL);
		Debug_Log_m857(NULL /*static, unused*/, L_115, /*hidden argument*/NULL);
		int32_t L_116 = V_15;
		V_15 = ((int32_t)((int32_t)L_116+(int32_t)1));
	}

IL_023e:
	{
		int32_t L_117 = V_15;
		List_1_t251 * L_118 = V_1;
		NullCheck(L_118);
		int32_t L_119 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.Material>::get_Count() */, L_118);
		if ((((int32_t)L_117) < ((int32_t)L_119)))
		{
			goto IL_01be;
		}
	}
	{
		return;
	}
}
// UnityStandardAssets.Utility.AutoMoveAndRotate/Vector3andSpace
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Au_2.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.Utility.AutoMoveAndRotate/Vector3andSpace
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Au_2MethodDeclarations.h"

// UnityEngine.Space
#include "UnityEngine_UnityEngine_Space.h"


// System.Void UnityStandardAssets.Utility.AutoMoveAndRotate/Vector3andSpace::.ctor()
extern "C" void Vector3andSpace__ctor_m448 (Vector3andSpace_t170 * __this, const MethodInfo* method)
{
	{
		__this->___space_1 = 1;
		Object__ctor_m677(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityStandardAssets.Utility.AutoMoveAndRotate
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Au_3.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.Utility.AutoMoveAndRotate
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Au_3MethodDeclarations.h"



// System.Void UnityStandardAssets.Utility.AutoMoveAndRotate::.ctor()
extern "C" void AutoMoveAndRotate__ctor_m449 (AutoMoveAndRotate_t171 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Utility.AutoMoveAndRotate::Start()
extern "C" void AutoMoveAndRotate_Start_m450 (AutoMoveAndRotate_t171 * __this, const MethodInfo* method)
{
	{
		float L_0 = Time_get_realtimeSinceStartup_m961(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___m_LastRealTime_5 = L_0;
		return;
	}
}
// System.Void UnityStandardAssets.Utility.AutoMoveAndRotate::Update()
extern "C" void AutoMoveAndRotate_Update_m451 (AutoMoveAndRotate_t171 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = Time_get_deltaTime_m602(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1 = (__this->___ignoreTimescale_4);
		if (!L_1)
		{
			goto IL_0029;
		}
	}
	{
		float L_2 = Time_get_realtimeSinceStartup_m961(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_3 = (__this->___m_LastRealTime_5);
		V_0 = ((float)((float)L_2-(float)L_3));
		float L_4 = Time_get_realtimeSinceStartup_m961(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___m_LastRealTime_5 = L_4;
	}

IL_0029:
	{
		Transform_t1 * L_5 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		Vector3andSpace_t170 * L_6 = (__this->___moveUnitsPerSecond_2);
		NullCheck(L_6);
		Vector3_t4  L_7 = (L_6->___value_0);
		float L_8 = V_0;
		Vector3_t4  L_9 = Vector3_op_Multiply_m600(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		Vector3andSpace_t170 * L_10 = (__this->___moveUnitsPerSecond_2);
		NullCheck(L_10);
		int32_t L_11 = (L_10->___space_1);
		NullCheck(L_5);
		Transform_Translate_m962(L_5, L_9, L_11, /*hidden argument*/NULL);
		Transform_t1 * L_12 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		Vector3andSpace_t170 * L_13 = (__this->___rotateDegreesPerSecond_3);
		NullCheck(L_13);
		Vector3_t4  L_14 = (L_13->___value_0);
		float L_15 = V_0;
		Vector3_t4  L_16 = Vector3_op_Multiply_m600(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
		Vector3andSpace_t170 * L_17 = (__this->___moveUnitsPerSecond_2);
		NullCheck(L_17);
		int32_t L_18 = (L_17->___space_1);
		NullCheck(L_12);
		Transform_Rotate_m963(L_12, L_16, L_18, /*hidden argument*/NULL);
		return;
	}
}
// UnityStandardAssets.Utility.CameraRefocus
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Ca.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.Utility.CameraRefocus
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_CaMethodDeclarations.h"

// UnityEngine.Camera
#include "UnityEngine_UnityEngine_Camera.h"


// System.Void UnityStandardAssets.Utility.CameraRefocus::.ctor(UnityEngine.Camera,UnityEngine.Transform,UnityEngine.Vector3)
extern "C" void CameraRefocus__ctor_m452 (CameraRefocus_t172 * __this, Camera_t27 * ___camera, Transform_t1 * ___parent, Vector3_t4  ___origCameraPos, const MethodInfo* method)
{
	{
		Object__ctor_m677(__this, /*hidden argument*/NULL);
		Vector3_t4  L_0 = ___origCameraPos;
		__this->___m_OrigCameraPos_3 = L_0;
		Camera_t27 * L_1 = ___camera;
		__this->___Camera_0 = L_1;
		Transform_t1 * L_2 = ___parent;
		__this->___Parent_2 = L_2;
		return;
	}
}
// System.Void UnityStandardAssets.Utility.CameraRefocus::ChangeCamera(UnityEngine.Camera)
extern "C" void CameraRefocus_ChangeCamera_m453 (CameraRefocus_t172 * __this, Camera_t27 * ___camera, const MethodInfo* method)
{
	{
		Camera_t27 * L_0 = ___camera;
		__this->___Camera_0 = L_0;
		return;
	}
}
// System.Void UnityStandardAssets.Utility.CameraRefocus::ChangeParent(UnityEngine.Transform)
extern "C" void CameraRefocus_ChangeParent_m454 (CameraRefocus_t172 * __this, Transform_t1 * ___parent, const MethodInfo* method)
{
	{
		Transform_t1 * L_0 = ___parent;
		__this->___Parent_2 = L_0;
		return;
	}
}
// System.Void UnityStandardAssets.Utility.CameraRefocus::GetFocusPoint()
extern "C" void CameraRefocus_GetFocusPoint_m455 (CameraRefocus_t172 * __this, const MethodInfo* method)
{
	RaycastHit_t24  V_0 = {0};
	{
		Transform_t1 * L_0 = (__this->___Parent_2);
		NullCheck(L_0);
		Transform_t1 * L_1 = Component_get_transform_m594(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t4  L_2 = Transform_get_position_m593(L_1, /*hidden argument*/NULL);
		Vector3_t4  L_3 = (__this->___m_OrigCameraPos_3);
		Vector3_t4  L_4 = Vector3_op_Addition_m604(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		Transform_t1 * L_5 = (__this->___Parent_2);
		NullCheck(L_5);
		Transform_t1 * L_6 = Component_get_transform_m594(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t4  L_7 = Transform_get_forward_m643(L_6, /*hidden argument*/NULL);
		bool L_8 = Physics_Raycast_m964(NULL /*static, unused*/, L_4, L_7, (&V_0), (100.0f), /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0051;
		}
	}
	{
		Vector3_t4  L_9 = RaycastHit_get_point_m693((&V_0), /*hidden argument*/NULL);
		__this->___Lookatpoint_1 = L_9;
		__this->___m_Refocus_4 = 1;
		return;
	}

IL_0051:
	{
		__this->___m_Refocus_4 = 0;
		return;
	}
}
// System.Void UnityStandardAssets.Utility.CameraRefocus::SetFocusPoint()
extern "C" void CameraRefocus_SetFocusPoint_m456 (CameraRefocus_t172 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_Refocus_4);
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		Camera_t27 * L_1 = (__this->___Camera_0);
		NullCheck(L_1);
		Transform_t1 * L_2 = Component_get_transform_m594(L_1, /*hidden argument*/NULL);
		Vector3_t4  L_3 = (__this->___Lookatpoint_1);
		NullCheck(L_2);
		Transform_LookAt_m965(L_2, L_3, /*hidden argument*/NULL);
	}

IL_0021:
	{
		return;
	}
}
// UnityStandardAssets.Utility.CurveControlledBob
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Cu.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.Utility.CurveControlledBob
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_CuMethodDeclarations.h"

// UnityEngine.Keyframe
#include "UnityEngine_UnityEngine_Keyframe.h"
// UnityEngine.AnimationCurve
#include "UnityEngine_UnityEngine_AnimationCurve.h"
// UnityEngine.Keyframe
#include "UnityEngine_UnityEngine_KeyframeMethodDeclarations.h"
// UnityEngine.AnimationCurve
#include "UnityEngine_UnityEngine_AnimationCurveMethodDeclarations.h"


// System.Void UnityStandardAssets.Utility.CurveControlledBob::.ctor()
extern TypeInfo* KeyframeU5BU5D_t239_il2cpp_TypeInfo_var;
extern TypeInfo* AnimationCurve_t82_il2cpp_TypeInfo_var;
extern "C" void CurveControlledBob__ctor_m457 (CurveControlledBob_t173 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		KeyframeU5BU5D_t239_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		AnimationCurve_t82_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___HorizontalBobRange_0 = (0.33f);
		__this->___VerticalBobRange_1 = (0.33f);
		KeyframeU5BU5D_t239* L_0 = ((KeyframeU5BU5D_t239*)SZArrayNew(KeyframeU5BU5D_t239_il2cpp_TypeInfo_var, 5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		Keyframe_t240  L_1 = {0};
		Keyframe__ctor_m803(&L_1, (0.0f), (0.0f), /*hidden argument*/NULL);
		*((Keyframe_t240 *)(Keyframe_t240 *)SZArrayLdElema(L_0, 0)) = L_1;
		KeyframeU5BU5D_t239* L_2 = L_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		Keyframe_t240  L_3 = {0};
		Keyframe__ctor_m803(&L_3, (0.5f), (1.0f), /*hidden argument*/NULL);
		*((Keyframe_t240 *)(Keyframe_t240 *)SZArrayLdElema(L_2, 1)) = L_3;
		KeyframeU5BU5D_t239* L_4 = L_2;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		Keyframe_t240  L_5 = {0};
		Keyframe__ctor_m803(&L_5, (1.0f), (0.0f), /*hidden argument*/NULL);
		*((Keyframe_t240 *)(Keyframe_t240 *)SZArrayLdElema(L_4, 2)) = L_5;
		KeyframeU5BU5D_t239* L_6 = L_4;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 3);
		Keyframe_t240  L_7 = {0};
		Keyframe__ctor_m803(&L_7, (1.5f), (-1.0f), /*hidden argument*/NULL);
		*((Keyframe_t240 *)(Keyframe_t240 *)SZArrayLdElema(L_6, 3)) = L_7;
		KeyframeU5BU5D_t239* L_8 = L_6;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 4);
		Keyframe_t240  L_9 = {0};
		Keyframe__ctor_m803(&L_9, (2.0f), (0.0f), /*hidden argument*/NULL);
		*((Keyframe_t240 *)(Keyframe_t240 *)SZArrayLdElema(L_8, 4)) = L_9;
		AnimationCurve_t82 * L_10 = (AnimationCurve_t82 *)il2cpp_codegen_object_new (AnimationCurve_t82_il2cpp_TypeInfo_var);
		AnimationCurve__ctor_m804(L_10, L_8, /*hidden argument*/NULL);
		__this->___Bobcurve_2 = L_10;
		__this->___VerticaltoHorizontalRatio_3 = (1.0f);
		Object__ctor_m677(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Utility.CurveControlledBob::Setup(UnityEngine.Camera,System.Single)
extern "C" void CurveControlledBob_Setup_m458 (CurveControlledBob_t173 * __this, Camera_t27 * ___camera, float ___bobBaseInterval, const MethodInfo* method)
{
	Keyframe_t240  V_0 = {0};
	{
		float L_0 = ___bobBaseInterval;
		__this->___m_BobBaseInterval_6 = L_0;
		Camera_t27 * L_1 = ___camera;
		NullCheck(L_1);
		Transform_t1 * L_2 = Component_get_transform_m594(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t4  L_3 = Transform_get_localPosition_m680(L_2, /*hidden argument*/NULL);
		__this->___m_OriginalCameraPosition_7 = L_3;
		AnimationCurve_t82 * L_4 = (__this->___Bobcurve_2);
		AnimationCurve_t82 * L_5 = (__this->___Bobcurve_2);
		NullCheck(L_5);
		int32_t L_6 = AnimationCurve_get_length_m878(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		Keyframe_t240  L_7 = AnimationCurve_get_Item_m879(L_4, ((int32_t)((int32_t)L_6-(int32_t)1)), /*hidden argument*/NULL);
		V_0 = L_7;
		float L_8 = Keyframe_get_time_m880((&V_0), /*hidden argument*/NULL);
		__this->___m_Time_8 = L_8;
		return;
	}
}
// UnityEngine.Vector3 UnityStandardAssets.Utility.CurveControlledBob::DoHeadBob(System.Single)
extern "C" Vector3_t4  CurveControlledBob_DoHeadBob_m459 (CurveControlledBob_t173 * __this, float ___speed, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		Vector3_t4 * L_0 = &(__this->___m_OriginalCameraPosition_7);
		float L_1 = (L_0->___x_1);
		AnimationCurve_t82 * L_2 = (__this->___Bobcurve_2);
		float L_3 = (__this->___m_CyclePositionX_4);
		NullCheck(L_2);
		float L_4 = AnimationCurve_Evaluate_m806(L_2, L_3, /*hidden argument*/NULL);
		float L_5 = (__this->___HorizontalBobRange_0);
		V_0 = ((float)((float)L_1+(float)((float)((float)L_4*(float)L_5))));
		Vector3_t4 * L_6 = &(__this->___m_OriginalCameraPosition_7);
		float L_7 = (L_6->___y_2);
		AnimationCurve_t82 * L_8 = (__this->___Bobcurve_2);
		float L_9 = (__this->___m_CyclePositionY_5);
		NullCheck(L_8);
		float L_10 = AnimationCurve_Evaluate_m806(L_8, L_9, /*hidden argument*/NULL);
		float L_11 = (__this->___VerticalBobRange_1);
		V_1 = ((float)((float)L_7+(float)((float)((float)L_10*(float)L_11))));
		float L_12 = (__this->___m_CyclePositionX_4);
		float L_13 = ___speed;
		float L_14 = Time_get_deltaTime_m602(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_15 = (__this->___m_BobBaseInterval_6);
		__this->___m_CyclePositionX_4 = ((float)((float)L_12+(float)((float)((float)((float)((float)L_13*(float)L_14))/(float)L_15))));
		float L_16 = (__this->___m_CyclePositionY_5);
		float L_17 = ___speed;
		float L_18 = Time_get_deltaTime_m602(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_19 = (__this->___m_BobBaseInterval_6);
		float L_20 = (__this->___VerticaltoHorizontalRatio_3);
		__this->___m_CyclePositionY_5 = ((float)((float)L_16+(float)((float)((float)((float)((float)((float)((float)L_17*(float)L_18))/(float)L_19))*(float)L_20))));
		float L_21 = (__this->___m_CyclePositionX_4);
		float L_22 = (__this->___m_Time_8);
		if ((!(((float)L_21) > ((float)L_22))))
		{
			goto IL_00ab;
		}
	}
	{
		float L_23 = (__this->___m_CyclePositionX_4);
		float L_24 = (__this->___m_Time_8);
		__this->___m_CyclePositionX_4 = ((float)((float)L_23-(float)L_24));
	}

IL_00ab:
	{
		float L_25 = (__this->___m_CyclePositionY_5);
		float L_26 = (__this->___m_Time_8);
		if ((!(((float)L_25) > ((float)L_26))))
		{
			goto IL_00cf;
		}
	}
	{
		float L_27 = (__this->___m_CyclePositionY_5);
		float L_28 = (__this->___m_Time_8);
		__this->___m_CyclePositionY_5 = ((float)((float)L_27-(float)L_28));
	}

IL_00cf:
	{
		float L_29 = V_0;
		float L_30 = V_1;
		Vector3_t4  L_31 = {0};
		Vector3__ctor_m612(&L_31, L_29, L_30, (0.0f), /*hidden argument*/NULL);
		return L_31;
	}
}
// UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator3
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Dr.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator3
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_DrMethodDeclarations.h"

// UnityStandardAssets.Utility.DragRigidbody
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Dr_0.h"
// UnityEngine.SpringJoint
#include "UnityEngine_UnityEngine_SpringJoint.h"
// UnityEngine.Joint
#include "UnityEngine_UnityEngine_JointMethodDeclarations.h"
// UnityStandardAssets.Utility.DragRigidbody
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Dr_0MethodDeclarations.h"
// UnityEngine.Camera
#include "UnityEngine_UnityEngine_CameraMethodDeclarations.h"


// System.Void UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator3::.ctor()
extern "C" void U3CDragObjectU3Ec__Iterator3__ctor_m460 (U3CDragObjectU3Ec__Iterator3_t175 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m677(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator3::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CDragObjectU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m461 (U3CDragObjectU3Ec__Iterator3_t175 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_6);
		return L_0;
	}
}
// System.Object UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CDragObjectU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m462 (U3CDragObjectU3Ec__Iterator3_t175 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_6);
		return L_0;
	}
}
// System.Boolean UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator3::MoveNext()
extern TypeInfo* Input_t220_il2cpp_TypeInfo_var;
extern "C" bool U3CDragObjectU3Ec__Iterator3_MoveNext_m463 (U3CDragObjectU3Ec__Iterator3_t175 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t220_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_5);
		V_0 = L_0;
		__this->___U24PC_5 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_00f0;
		}
	}
	{
		goto IL_0163;
	}

IL_0021:
	{
		DragRigidbody_t174 * L_2 = (__this->___U3CU3Ef__this_8);
		NullCheck(L_2);
		SpringJoint_t176 * L_3 = (L_2->___m_SpringJoint_8);
		NullCheck(L_3);
		Rigidbody_t14 * L_4 = Joint_get_connectedBody_m966(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		float L_5 = Rigidbody_get_drag_m967(L_4, /*hidden argument*/NULL);
		__this->___U3ColdDragU3E__0_0 = L_5;
		DragRigidbody_t174 * L_6 = (__this->___U3CU3Ef__this_8);
		NullCheck(L_6);
		SpringJoint_t176 * L_7 = (L_6->___m_SpringJoint_8);
		NullCheck(L_7);
		Rigidbody_t14 * L_8 = Joint_get_connectedBody_m966(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		float L_9 = Rigidbody_get_angularDrag_m968(L_8, /*hidden argument*/NULL);
		__this->___U3ColdAngularDragU3E__1_1 = L_9;
		DragRigidbody_t174 * L_10 = (__this->___U3CU3Ef__this_8);
		NullCheck(L_10);
		SpringJoint_t176 * L_11 = (L_10->___m_SpringJoint_8);
		NullCheck(L_11);
		Rigidbody_t14 * L_12 = Joint_get_connectedBody_m966(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Rigidbody_set_drag_m969(L_12, (10.0f), /*hidden argument*/NULL);
		DragRigidbody_t174 * L_13 = (__this->___U3CU3Ef__this_8);
		NullCheck(L_13);
		SpringJoint_t176 * L_14 = (L_13->___m_SpringJoint_8);
		NullCheck(L_14);
		Rigidbody_t14 * L_15 = Joint_get_connectedBody_m966(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		Rigidbody_set_angularDrag_m970(L_15, (5.0f), /*hidden argument*/NULL);
		DragRigidbody_t174 * L_16 = (__this->___U3CU3Ef__this_8);
		NullCheck(L_16);
		Camera_t27 * L_17 = DragRigidbody_FindCamera_m469(L_16, /*hidden argument*/NULL);
		__this->___U3CmainCameraU3E__2_2 = L_17;
		goto IL_00f0;
	}

IL_00a1:
	{
		Camera_t27 * L_18 = (__this->___U3CmainCameraU3E__2_2);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t220_il2cpp_TypeInfo_var);
		Vector3_t4  L_19 = Input_get_mousePosition_m720(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_18);
		Ray_t26  L_20 = Camera_ScreenPointToRay_m971(L_18, L_19, /*hidden argument*/NULL);
		__this->___U3CrayU3E__3_3 = L_20;
		DragRigidbody_t174 * L_21 = (__this->___U3CU3Ef__this_8);
		NullCheck(L_21);
		SpringJoint_t176 * L_22 = (L_21->___m_SpringJoint_8);
		NullCheck(L_22);
		Transform_t1 * L_23 = Component_get_transform_m594(L_22, /*hidden argument*/NULL);
		Ray_t26 * L_24 = &(__this->___U3CrayU3E__3_3);
		float L_25 = (__this->___distance_4);
		Vector3_t4  L_26 = Ray_GetPoint_m972(L_24, L_25, /*hidden argument*/NULL);
		NullCheck(L_23);
		Transform_set_position_m607(L_23, L_26, /*hidden argument*/NULL);
		__this->___U24current_6 = NULL;
		__this->___U24PC_5 = 1;
		goto IL_0165;
	}

IL_00f0:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t220_il2cpp_TypeInfo_var);
		bool L_27 = Input_GetMouseButton_m924(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (L_27)
		{
			goto IL_00a1;
		}
	}
	{
		DragRigidbody_t174 * L_28 = (__this->___U3CU3Ef__this_8);
		NullCheck(L_28);
		SpringJoint_t176 * L_29 = (L_28->___m_SpringJoint_8);
		NullCheck(L_29);
		Rigidbody_t14 * L_30 = Joint_get_connectedBody_m966(L_29, /*hidden argument*/NULL);
		bool L_31 = Object_op_Implicit_m629(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		if (!L_31)
		{
			goto IL_015c;
		}
	}
	{
		DragRigidbody_t174 * L_32 = (__this->___U3CU3Ef__this_8);
		NullCheck(L_32);
		SpringJoint_t176 * L_33 = (L_32->___m_SpringJoint_8);
		NullCheck(L_33);
		Rigidbody_t14 * L_34 = Joint_get_connectedBody_m966(L_33, /*hidden argument*/NULL);
		float L_35 = (__this->___U3ColdDragU3E__0_0);
		NullCheck(L_34);
		Rigidbody_set_drag_m969(L_34, L_35, /*hidden argument*/NULL);
		DragRigidbody_t174 * L_36 = (__this->___U3CU3Ef__this_8);
		NullCheck(L_36);
		SpringJoint_t176 * L_37 = (L_36->___m_SpringJoint_8);
		NullCheck(L_37);
		Rigidbody_t14 * L_38 = Joint_get_connectedBody_m966(L_37, /*hidden argument*/NULL);
		float L_39 = (__this->___U3ColdAngularDragU3E__1_1);
		NullCheck(L_38);
		Rigidbody_set_angularDrag_m970(L_38, L_39, /*hidden argument*/NULL);
		DragRigidbody_t174 * L_40 = (__this->___U3CU3Ef__this_8);
		NullCheck(L_40);
		SpringJoint_t176 * L_41 = (L_40->___m_SpringJoint_8);
		NullCheck(L_41);
		Joint_set_connectedBody_m973(L_41, (Rigidbody_t14 *)NULL, /*hidden argument*/NULL);
	}

IL_015c:
	{
		__this->___U24PC_5 = (-1);
	}

IL_0163:
	{
		return 0;
	}

IL_0165:
	{
		return 1;
	}
	// Dead block : IL_0167: ldloc.1
}
// System.Void UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator3::Dispose()
extern "C" void U3CDragObjectU3Ec__Iterator3_Dispose_m464 (U3CDragObjectU3Ec__Iterator3_t175 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_5 = (-1);
		return;
	}
}
// System.Void UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator3::Reset()
extern TypeInfo* NotSupportedException_t244_il2cpp_TypeInfo_var;
extern "C" void U3CDragObjectU3Ec__Iterator3_Reset_m465 (U3CDragObjectU3Ec__Iterator3_t175 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t244_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t244 * L_0 = (NotSupportedException_t244 *)il2cpp_codegen_object_new (NotSupportedException_t244_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m905(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Coroutine
#include "UnityEngine_UnityEngine_Coroutine.h"
// UnityEngine.SpringJoint
#include "UnityEngine_UnityEngine_SpringJointMethodDeclarations.h"
struct GameObject_t78;
struct Rigidbody_t14;
struct GameObject_t78;
struct Object_t;
// Declaration !!0 UnityEngine.GameObject::AddComponent<System.Object>()
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C" Object_t * GameObject_AddComponent_TisObject_t_m975_gshared (GameObject_t78 * __this, const MethodInfo* method);
#define GameObject_AddComponent_TisObject_t_m975(__this, method) (( Object_t * (*) (GameObject_t78 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m975_gshared)(__this, method)
// Declaration !!0 UnityEngine.GameObject::AddComponent<UnityEngine.Rigidbody>()
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.Rigidbody>()
#define GameObject_AddComponent_TisRigidbody_t14_m974(__this, method) (( Rigidbody_t14 * (*) (GameObject_t78 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m975_gshared)(__this, method)
struct GameObject_t78;
struct SpringJoint_t176;
// Declaration !!0 UnityEngine.GameObject::AddComponent<UnityEngine.SpringJoint>()
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.SpringJoint>()
#define GameObject_AddComponent_TisSpringJoint_t176_m976(__this, method) (( SpringJoint_t176 * (*) (GameObject_t78 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m975_gshared)(__this, method)
struct Component_t219;
struct Camera_t27;
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.Camera>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Camera>()
#define Component_GetComponent_TisCamera_t27_m744(__this, method) (( Camera_t27 * (*) (Component_t219 *, const MethodInfo*))Component_GetComponent_TisObject_t_m614_gshared)(__this, method)


// System.Void UnityStandardAssets.Utility.DragRigidbody::.ctor()
extern "C" void DragRigidbody__ctor_m466 (DragRigidbody_t174 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Utility.DragRigidbody::Update()
extern TypeInfo* Input_t220_il2cpp_TypeInfo_var;
extern TypeInfo* RaycastHit_t24_il2cpp_TypeInfo_var;
extern TypeInfo* GameObject_t78_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t254_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisRigidbody_t14_m974_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisSpringJoint_t176_m976_MethodInfo_var;
extern "C" void DragRigidbody_Update_m467 (DragRigidbody_t174 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t220_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		RaycastHit_t24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		GameObject_t78_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(42);
		Single_t254_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		GameObject_AddComponent_TisRigidbody_t14_m974_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483676);
		GameObject_AddComponent_TisSpringJoint_t176_m976_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483677);
		s_Il2CppMethodIntialized = true;
	}
	Camera_t27 * V_0 = {0};
	RaycastHit_t24  V_1 = {0};
	GameObject_t78 * V_2 = {0};
	Rigidbody_t14 * V_3 = {0};
	Ray_t26  V_4 = {0};
	Ray_t26  V_5 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t220_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetMouseButtonDown_m977(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Camera_t27 * L_1 = DragRigidbody_FindCamera_m469(__this, /*hidden argument*/NULL);
		V_0 = L_1;
		Initobj (RaycastHit_t24_il2cpp_TypeInfo_var, (&V_1));
		Camera_t27 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t220_il2cpp_TypeInfo_var);
		Vector3_t4  L_3 = Input_get_mousePosition_m720(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		Ray_t26  L_4 = Camera_ScreenPointToRay_m971(L_2, L_3, /*hidden argument*/NULL);
		V_4 = L_4;
		Vector3_t4  L_5 = Ray_get_origin_m684((&V_4), /*hidden argument*/NULL);
		Camera_t27 * L_6 = V_0;
		Vector3_t4  L_7 = Input_get_mousePosition_m720(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		Ray_t26  L_8 = Camera_ScreenPointToRay_m971(L_6, L_7, /*hidden argument*/NULL);
		V_5 = L_8;
		Vector3_t4  L_9 = Ray_get_direction_m978((&V_5), /*hidden argument*/NULL);
		bool L_10 = Physics_Raycast_m979(NULL /*static, unused*/, L_5, L_9, (&V_1), (100.0f), ((int32_t)-5), /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_0057;
		}
	}
	{
		return;
	}

IL_0057:
	{
		Rigidbody_t14 * L_11 = RaycastHit_get_rigidbody_m980((&V_1), /*hidden argument*/NULL);
		bool L_12 = Object_op_Implicit_m629(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0079;
		}
	}
	{
		Rigidbody_t14 * L_13 = RaycastHit_get_rigidbody_m980((&V_1), /*hidden argument*/NULL);
		NullCheck(L_13);
		bool L_14 = Rigidbody_get_isKinematic_m981(L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_007a;
		}
	}

IL_0079:
	{
		return;
	}

IL_007a:
	{
		SpringJoint_t176 * L_15 = (__this->___m_SpringJoint_8);
		bool L_16 = Object_op_Implicit_m629(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		if (L_16)
		{
			goto IL_00af;
		}
	}
	{
		GameObject_t78 * L_17 = (GameObject_t78 *)il2cpp_codegen_object_new (GameObject_t78_il2cpp_TypeInfo_var);
		GameObject__ctor_m982(L_17, (String_t*) &_stringLiteral186, /*hidden argument*/NULL);
		V_2 = L_17;
		GameObject_t78 * L_18 = V_2;
		NullCheck(L_18);
		Rigidbody_t14 * L_19 = GameObject_AddComponent_TisRigidbody_t14_m974(L_18, /*hidden argument*/GameObject_AddComponent_TisRigidbody_t14_m974_MethodInfo_var);
		V_3 = L_19;
		GameObject_t78 * L_20 = V_2;
		NullCheck(L_20);
		SpringJoint_t176 * L_21 = GameObject_AddComponent_TisSpringJoint_t176_m976(L_20, /*hidden argument*/GameObject_AddComponent_TisSpringJoint_t176_m976_MethodInfo_var);
		__this->___m_SpringJoint_8 = L_21;
		Rigidbody_t14 * L_22 = V_3;
		NullCheck(L_22);
		Rigidbody_set_isKinematic_m983(L_22, 1, /*hidden argument*/NULL);
	}

IL_00af:
	{
		SpringJoint_t176 * L_23 = (__this->___m_SpringJoint_8);
		NullCheck(L_23);
		Transform_t1 * L_24 = Component_get_transform_m594(L_23, /*hidden argument*/NULL);
		Vector3_t4  L_25 = RaycastHit_get_point_m693((&V_1), /*hidden argument*/NULL);
		NullCheck(L_24);
		Transform_set_position_m607(L_24, L_25, /*hidden argument*/NULL);
		SpringJoint_t176 * L_26 = (__this->___m_SpringJoint_8);
		Vector3_t4  L_27 = Vector3_get_zero_m601(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_26);
		Joint_set_anchor_m984(L_26, L_27, /*hidden argument*/NULL);
		SpringJoint_t176 * L_28 = (__this->___m_SpringJoint_8);
		NullCheck(L_28);
		SpringJoint_set_spring_m985(L_28, (50.0f), /*hidden argument*/NULL);
		SpringJoint_t176 * L_29 = (__this->___m_SpringJoint_8);
		NullCheck(L_29);
		SpringJoint_set_damper_m986(L_29, (5.0f), /*hidden argument*/NULL);
		SpringJoint_t176 * L_30 = (__this->___m_SpringJoint_8);
		NullCheck(L_30);
		SpringJoint_set_maxDistance_m987(L_30, (0.2f), /*hidden argument*/NULL);
		SpringJoint_t176 * L_31 = (__this->___m_SpringJoint_8);
		Rigidbody_t14 * L_32 = RaycastHit_get_rigidbody_m980((&V_1), /*hidden argument*/NULL);
		NullCheck(L_31);
		Joint_set_connectedBody_m973(L_31, L_32, /*hidden argument*/NULL);
		float L_33 = RaycastHit_get_distance_m678((&V_1), /*hidden argument*/NULL);
		float L_34 = L_33;
		Object_t * L_35 = Box(Single_t254_il2cpp_TypeInfo_var, &L_34);
		MonoBehaviour_StartCoroutine_m988(__this, (String_t*) &_stringLiteral187, L_35, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator UnityStandardAssets.Utility.DragRigidbody::DragObject(System.Single)
extern TypeInfo* U3CDragObjectU3Ec__Iterator3_t175_il2cpp_TypeInfo_var;
extern "C" Object_t * DragRigidbody_DragObject_m468 (DragRigidbody_t174 * __this, float ___distance, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CDragObjectU3Ec__Iterator3_t175_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(87);
		s_Il2CppMethodIntialized = true;
	}
	U3CDragObjectU3Ec__Iterator3_t175 * V_0 = {0};
	{
		U3CDragObjectU3Ec__Iterator3_t175 * L_0 = (U3CDragObjectU3Ec__Iterator3_t175 *)il2cpp_codegen_object_new (U3CDragObjectU3Ec__Iterator3_t175_il2cpp_TypeInfo_var);
		U3CDragObjectU3Ec__Iterator3__ctor_m460(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CDragObjectU3Ec__Iterator3_t175 * L_1 = V_0;
		float L_2 = ___distance;
		NullCheck(L_1);
		L_1->___distance_4 = L_2;
		U3CDragObjectU3Ec__Iterator3_t175 * L_3 = V_0;
		float L_4 = ___distance;
		NullCheck(L_3);
		L_3->___U3CU24U3Edistance_7 = L_4;
		U3CDragObjectU3Ec__Iterator3_t175 * L_5 = V_0;
		NullCheck(L_5);
		L_5->___U3CU3Ef__this_8 = __this;
		U3CDragObjectU3Ec__Iterator3_t175 * L_6 = V_0;
		return L_6;
	}
}
// UnityEngine.Camera UnityStandardAssets.Utility.DragRigidbody::FindCamera()
extern const MethodInfo* Component_GetComponent_TisCamera_t27_m744_MethodInfo_var;
extern "C" Camera_t27 * DragRigidbody_FindCamera_m469 (DragRigidbody_t174 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisCamera_t27_m744_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483659);
		s_Il2CppMethodIntialized = true;
	}
	{
		Camera_t27 * L_0 = Component_GetComponent_TisCamera_t27_m744(__this, /*hidden argument*/Component_GetComponent_TisCamera_t27_m744_MethodInfo_var);
		bool L_1 = Object_op_Implicit_m629(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		Camera_t27 * L_2 = Component_GetComponent_TisCamera_t27_m744(__this, /*hidden argument*/Component_GetComponent_TisCamera_t27_m744_MethodInfo_var);
		return L_2;
	}

IL_0017:
	{
		Camera_t27 * L_3 = Camera_get_main_m989(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityStandardAssets.Utility.DynamicShadowSettings
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Dy.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.Utility.DynamicShadowSettings
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_DyMethodDeclarations.h"

// UnityEngine.QualitySettings
#include "UnityEngine_UnityEngine_QualitySettingsMethodDeclarations.h"


// System.Void UnityStandardAssets.Utility.DynamicShadowSettings::.ctor()
extern "C" void DynamicShadowSettings__ctor_m470 (DynamicShadowSettings_t177 * __this, const MethodInfo* method)
{
	{
		__this->___minHeight_3 = (10.0f);
		__this->___minShadowDistance_4 = (80.0f);
		__this->___minShadowBias_5 = (1.0f);
		__this->___maxHeight_6 = (1000.0f);
		__this->___maxShadowDistance_7 = (10000.0f);
		__this->___maxShadowBias_8 = (0.1f);
		__this->___adaptTime_9 = (1.0f);
		__this->___m_OriginalStrength_12 = (1.0f);
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Utility.DynamicShadowSettings::Start()
extern "C" void DynamicShadowSettings_Start_m471 (DynamicShadowSettings_t177 * __this, const MethodInfo* method)
{
	{
		Light_t152 * L_0 = (__this->___sunLight_2);
		NullCheck(L_0);
		float L_1 = Light_get_shadowStrength_m990(L_0, /*hidden argument*/NULL);
		__this->___m_OriginalStrength_12 = L_1;
		return;
	}
}
// System.Void UnityStandardAssets.Utility.DynamicShadowSettings::Update()
extern TypeInfo* Mathf_t218_il2cpp_TypeInfo_var;
extern "C" void DynamicShadowSettings_Update_m472 (DynamicShadowSettings_t177 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	Ray_t26  V_0 = {0};
	RaycastHit_t24  V_1 = {0};
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	Vector3_t4  V_4 = {0};
	{
		Camera_t27 * L_0 = Camera_get_main_m989(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t1 * L_1 = Component_get_transform_m594(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t4  L_2 = Transform_get_position_m593(L_1, /*hidden argument*/NULL);
		Vector3_t4  L_3 = Vector3_get_up_m642(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4  L_4 = Vector3_op_UnaryNegation_m682(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		Ray__ctor_m900((&V_0), L_2, L_4, /*hidden argument*/NULL);
		Transform_t1 * L_5 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Vector3_t4  L_6 = Transform_get_position_m593(L_5, /*hidden argument*/NULL);
		V_4 = L_6;
		float L_7 = ((&V_4)->___y_2);
		V_2 = L_7;
		Ray_t26  L_8 = V_0;
		bool L_9 = Physics_Raycast_m991(NULL /*static, unused*/, L_8, (&V_1), /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_004a;
		}
	}
	{
		float L_10 = RaycastHit_get_distance_m678((&V_1), /*hidden argument*/NULL);
		V_2 = L_10;
	}

IL_004a:
	{
		float L_11 = V_2;
		float L_12 = (__this->___m_SmoothHeight_10);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_13 = fabsf(((float)((float)L_11-(float)L_12)));
		if ((!(((float)L_13) > ((float)(1.0f)))))
		{
			goto IL_007f;
		}
	}
	{
		float L_14 = (__this->___m_SmoothHeight_10);
		float L_15 = V_2;
		float* L_16 = &(__this->___m_ChangeSpeed_11);
		float L_17 = (__this->___adaptTime_9);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_18 = Mathf_SmoothDamp_m649(NULL /*static, unused*/, L_14, L_15, L_16, L_17, /*hidden argument*/NULL);
		__this->___m_SmoothHeight_10 = L_18;
	}

IL_007f:
	{
		float L_19 = (__this->___minHeight_3);
		float L_20 = (__this->___maxHeight_6);
		float L_21 = (__this->___m_SmoothHeight_10);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_22 = Mathf_InverseLerp_m651(NULL /*static, unused*/, L_19, L_20, L_21, /*hidden argument*/NULL);
		V_3 = L_22;
		float L_23 = (__this->___minShadowDistance_4);
		float L_24 = (__this->___maxShadowDistance_7);
		float L_25 = V_3;
		float L_26 = Mathf_Lerp_m610(NULL /*static, unused*/, L_23, L_24, L_25, /*hidden argument*/NULL);
		QualitySettings_set_shadowDistance_m992(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		Light_t152 * L_27 = (__this->___sunLight_2);
		float L_28 = (__this->___minShadowBias_5);
		float L_29 = (__this->___maxShadowBias_8);
		float L_30 = V_3;
		float L_31 = V_3;
		float L_32 = Mathf_Lerp_m610(NULL /*static, unused*/, L_28, L_29, ((float)((float)(1.0f)-(float)((float)((float)((float)((float)(1.0f)-(float)L_30))*(float)((float)((float)(1.0f)-(float)L_31)))))), /*hidden argument*/NULL);
		NullCheck(L_27);
		Light_set_shadowBias_m993(L_27, L_32, /*hidden argument*/NULL);
		Light_t152 * L_33 = (__this->___sunLight_2);
		float L_34 = (__this->___m_OriginalStrength_12);
		float L_35 = V_3;
		float L_36 = Mathf_Lerp_m610(NULL /*static, unused*/, L_34, (0.0f), L_35, /*hidden argument*/NULL);
		NullCheck(L_33);
		Light_set_shadowStrength_m994(L_33, L_36, /*hidden argument*/NULL);
		return;
	}
}
// UnityStandardAssets.Utility.FOVKick/<FOVKickUp>c__Iterator4
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_FO.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.Utility.FOVKick/<FOVKickUp>c__Iterator4
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_FOMethodDeclarations.h"

// UnityStandardAssets.Utility.FOVKick
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_FO_1.h"
// UnityEngine.WaitForEndOfFrame
#include "UnityEngine_UnityEngine_WaitForEndOfFrame.h"
// UnityEngine.WaitForEndOfFrame
#include "UnityEngine_UnityEngine_WaitForEndOfFrameMethodDeclarations.h"


// System.Void UnityStandardAssets.Utility.FOVKick/<FOVKickUp>c__Iterator4::.ctor()
extern "C" void U3CFOVKickUpU3Ec__Iterator4__ctor_m473 (U3CFOVKickUpU3Ec__Iterator4_t179 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m677(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object UnityStandardAssets.Utility.FOVKick/<FOVKickUp>c__Iterator4::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CFOVKickUpU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m474 (U3CFOVKickUpU3Ec__Iterator4_t179 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_2);
		return L_0;
	}
}
// System.Object UnityStandardAssets.Utility.FOVKick/<FOVKickUp>c__Iterator4::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CFOVKickUpU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m475 (U3CFOVKickUpU3Ec__Iterator4_t179 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_2);
		return L_0;
	}
}
// System.Boolean UnityStandardAssets.Utility.FOVKick/<FOVKickUp>c__Iterator4::MoveNext()
extern TypeInfo* Mathf_t218_il2cpp_TypeInfo_var;
extern TypeInfo* WaitForEndOfFrame_t255_il2cpp_TypeInfo_var;
extern "C" bool U3CFOVKickUpU3Ec__Iterator4_MoveNext_m476 (U3CFOVKickUpU3Ec__Iterator4_t179 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		WaitForEndOfFrame_t255_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(88);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_1);
		V_0 = L_0;
		__this->___U24PC_1 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_00cc;
		}
	}
	{
		goto IL_00e9;
	}

IL_0021:
	{
		FOVKick_t178 * L_2 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_2);
		Camera_t27 * L_3 = (L_2->___Camera_0);
		NullCheck(L_3);
		float L_4 = Camera_get_fieldOfView_m699(L_3, /*hidden argument*/NULL);
		FOVKick_t178 * L_5 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_5);
		float L_6 = (L_5->___originalFov_1);
		FOVKick_t178 * L_7 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_7);
		float L_8 = (L_7->___FOVIncrease_2);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_9 = fabsf(((float)((float)((float)((float)L_4-(float)L_6))/(float)L_8)));
		__this->___U3CtU3E__0_0 = L_9;
		goto IL_00cc;
	}

IL_0059:
	{
		FOVKick_t178 * L_10 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_10);
		Camera_t27 * L_11 = (L_10->___Camera_0);
		FOVKick_t178 * L_12 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_12);
		float L_13 = (L_12->___originalFov_1);
		FOVKick_t178 * L_14 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_14);
		AnimationCurve_t82 * L_15 = (L_14->___IncreaseCurve_5);
		float L_16 = (__this->___U3CtU3E__0_0);
		FOVKick_t178 * L_17 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_17);
		float L_18 = (L_17->___TimeToIncrease_3);
		NullCheck(L_15);
		float L_19 = AnimationCurve_Evaluate_m806(L_15, ((float)((float)L_16/(float)L_18)), /*hidden argument*/NULL);
		FOVKick_t178 * L_20 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_20);
		float L_21 = (L_20->___FOVIncrease_2);
		NullCheck(L_11);
		Camera_set_fieldOfView_m700(L_11, ((float)((float)L_13+(float)((float)((float)L_19*(float)L_21)))), /*hidden argument*/NULL);
		float L_22 = (__this->___U3CtU3E__0_0);
		float L_23 = Time_get_deltaTime_m602(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___U3CtU3E__0_0 = ((float)((float)L_22+(float)L_23));
		WaitForEndOfFrame_t255 * L_24 = (WaitForEndOfFrame_t255 *)il2cpp_codegen_object_new (WaitForEndOfFrame_t255_il2cpp_TypeInfo_var);
		WaitForEndOfFrame__ctor_m995(L_24, /*hidden argument*/NULL);
		__this->___U24current_2 = L_24;
		__this->___U24PC_1 = 1;
		goto IL_00eb;
	}

IL_00cc:
	{
		float L_25 = (__this->___U3CtU3E__0_0);
		FOVKick_t178 * L_26 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_26);
		float L_27 = (L_26->___TimeToIncrease_3);
		if ((((float)L_25) < ((float)L_27)))
		{
			goto IL_0059;
		}
	}
	{
		__this->___U24PC_1 = (-1);
	}

IL_00e9:
	{
		return 0;
	}

IL_00eb:
	{
		return 1;
	}
	// Dead block : IL_00ed: ldloc.1
}
// System.Void UnityStandardAssets.Utility.FOVKick/<FOVKickUp>c__Iterator4::Dispose()
extern "C" void U3CFOVKickUpU3Ec__Iterator4_Dispose_m477 (U3CFOVKickUpU3Ec__Iterator4_t179 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_1 = (-1);
		return;
	}
}
// System.Void UnityStandardAssets.Utility.FOVKick/<FOVKickUp>c__Iterator4::Reset()
extern TypeInfo* NotSupportedException_t244_il2cpp_TypeInfo_var;
extern "C" void U3CFOVKickUpU3Ec__Iterator4_Reset_m478 (U3CFOVKickUpU3Ec__Iterator4_t179 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t244_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t244 * L_0 = (NotSupportedException_t244 *)il2cpp_codegen_object_new (NotSupportedException_t244_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m905(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// UnityStandardAssets.Utility.FOVKick/<FOVKickDown>c__Iterator5
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_FO_0.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.Utility.FOVKick/<FOVKickDown>c__Iterator5
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_FO_0MethodDeclarations.h"



// System.Void UnityStandardAssets.Utility.FOVKick/<FOVKickDown>c__Iterator5::.ctor()
extern "C" void U3CFOVKickDownU3Ec__Iterator5__ctor_m479 (U3CFOVKickDownU3Ec__Iterator5_t180 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m677(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object UnityStandardAssets.Utility.FOVKick/<FOVKickDown>c__Iterator5::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CFOVKickDownU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m480 (U3CFOVKickDownU3Ec__Iterator5_t180 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_2);
		return L_0;
	}
}
// System.Object UnityStandardAssets.Utility.FOVKick/<FOVKickDown>c__Iterator5::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CFOVKickDownU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m481 (U3CFOVKickDownU3Ec__Iterator5_t180 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_2);
		return L_0;
	}
}
// System.Boolean UnityStandardAssets.Utility.FOVKick/<FOVKickDown>c__Iterator5::MoveNext()
extern TypeInfo* Mathf_t218_il2cpp_TypeInfo_var;
extern TypeInfo* WaitForEndOfFrame_t255_il2cpp_TypeInfo_var;
extern "C" bool U3CFOVKickDownU3Ec__Iterator5_MoveNext_m482 (U3CFOVKickDownU3Ec__Iterator5_t180 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		WaitForEndOfFrame_t255_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(88);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_1);
		V_0 = L_0;
		__this->___U24PC_1 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_00cc;
		}
	}
	{
		goto IL_00fe;
	}

IL_0021:
	{
		FOVKick_t178 * L_2 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_2);
		Camera_t27 * L_3 = (L_2->___Camera_0);
		NullCheck(L_3);
		float L_4 = Camera_get_fieldOfView_m699(L_3, /*hidden argument*/NULL);
		FOVKick_t178 * L_5 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_5);
		float L_6 = (L_5->___originalFov_1);
		FOVKick_t178 * L_7 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_7);
		float L_8 = (L_7->___FOVIncrease_2);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_9 = fabsf(((float)((float)((float)((float)L_4-(float)L_6))/(float)L_8)));
		__this->___U3CtU3E__0_0 = L_9;
		goto IL_00cc;
	}

IL_0059:
	{
		FOVKick_t178 * L_10 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_10);
		Camera_t27 * L_11 = (L_10->___Camera_0);
		FOVKick_t178 * L_12 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_12);
		float L_13 = (L_12->___originalFov_1);
		FOVKick_t178 * L_14 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_14);
		AnimationCurve_t82 * L_15 = (L_14->___IncreaseCurve_5);
		float L_16 = (__this->___U3CtU3E__0_0);
		FOVKick_t178 * L_17 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_17);
		float L_18 = (L_17->___TimeToDecrease_4);
		NullCheck(L_15);
		float L_19 = AnimationCurve_Evaluate_m806(L_15, ((float)((float)L_16/(float)L_18)), /*hidden argument*/NULL);
		FOVKick_t178 * L_20 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_20);
		float L_21 = (L_20->___FOVIncrease_2);
		NullCheck(L_11);
		Camera_set_fieldOfView_m700(L_11, ((float)((float)L_13+(float)((float)((float)L_19*(float)L_21)))), /*hidden argument*/NULL);
		float L_22 = (__this->___U3CtU3E__0_0);
		float L_23 = Time_get_deltaTime_m602(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___U3CtU3E__0_0 = ((float)((float)L_22-(float)L_23));
		WaitForEndOfFrame_t255 * L_24 = (WaitForEndOfFrame_t255 *)il2cpp_codegen_object_new (WaitForEndOfFrame_t255_il2cpp_TypeInfo_var);
		WaitForEndOfFrame__ctor_m995(L_24, /*hidden argument*/NULL);
		__this->___U24current_2 = L_24;
		__this->___U24PC_1 = 1;
		goto IL_0100;
	}

IL_00cc:
	{
		float L_25 = (__this->___U3CtU3E__0_0);
		if ((((float)L_25) > ((float)(0.0f))))
		{
			goto IL_0059;
		}
	}
	{
		FOVKick_t178 * L_26 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_26);
		Camera_t27 * L_27 = (L_26->___Camera_0);
		FOVKick_t178 * L_28 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_28);
		float L_29 = (L_28->___originalFov_1);
		NullCheck(L_27);
		Camera_set_fieldOfView_m700(L_27, L_29, /*hidden argument*/NULL);
		__this->___U24PC_1 = (-1);
	}

IL_00fe:
	{
		return 0;
	}

IL_0100:
	{
		return 1;
	}
	// Dead block : IL_0102: ldloc.1
}
// System.Void UnityStandardAssets.Utility.FOVKick/<FOVKickDown>c__Iterator5::Dispose()
extern "C" void U3CFOVKickDownU3Ec__Iterator5_Dispose_m483 (U3CFOVKickDownU3Ec__Iterator5_t180 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_1 = (-1);
		return;
	}
}
// System.Void UnityStandardAssets.Utility.FOVKick/<FOVKickDown>c__Iterator5::Reset()
extern TypeInfo* NotSupportedException_t244_il2cpp_TypeInfo_var;
extern "C" void U3CFOVKickDownU3Ec__Iterator5_Reset_m484 (U3CFOVKickDownU3Ec__Iterator5_t180 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t244_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t244 * L_0 = (NotSupportedException_t244 *)il2cpp_codegen_object_new (NotSupportedException_t244_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m905(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.Utility.FOVKick
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_FO_1MethodDeclarations.h"

// System.Exception
#include "mscorlib_System_Exception.h"
// System.Exception
#include "mscorlib_System_ExceptionMethodDeclarations.h"


// System.Void UnityStandardAssets.Utility.FOVKick::.ctor()
extern "C" void FOVKick__ctor_m485 (FOVKick_t178 * __this, const MethodInfo* method)
{
	{
		__this->___FOVIncrease_2 = (3.0f);
		__this->___TimeToIncrease_3 = (1.0f);
		__this->___TimeToDecrease_4 = (1.0f);
		Object__ctor_m677(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Utility.FOVKick::Setup(UnityEngine.Camera)
extern "C" void FOVKick_Setup_m486 (FOVKick_t178 * __this, Camera_t27 * ___camera, const MethodInfo* method)
{
	{
		Camera_t27 * L_0 = ___camera;
		FOVKick_CheckStatus_m487(__this, L_0, /*hidden argument*/NULL);
		Camera_t27 * L_1 = ___camera;
		__this->___Camera_0 = L_1;
		Camera_t27 * L_2 = ___camera;
		NullCheck(L_2);
		float L_3 = Camera_get_fieldOfView_m699(L_2, /*hidden argument*/NULL);
		__this->___originalFov_1 = L_3;
		return;
	}
}
// System.Void UnityStandardAssets.Utility.FOVKick::CheckStatus(UnityEngine.Camera)
extern TypeInfo* Exception_t232_il2cpp_TypeInfo_var;
extern "C" void FOVKick_CheckStatus_m487 (FOVKick_t178 * __this, Camera_t27 * ___camera, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Exception_t232_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(31);
		s_Il2CppMethodIntialized = true;
	}
	{
		Camera_t27 * L_0 = ___camera;
		bool L_1 = Object_op_Equality_m640(NULL /*static, unused*/, L_0, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		Exception_t232 * L_2 = (Exception_t232 *)il2cpp_codegen_object_new (Exception_t232_il2cpp_TypeInfo_var);
		Exception__ctor_m719(L_2, (String_t*) &_stringLiteral188, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0017:
	{
		AnimationCurve_t82 * L_3 = (__this->___IncreaseCurve_5);
		if (L_3)
		{
			goto IL_002d;
		}
	}
	{
		Exception_t232 * L_4 = (Exception_t232 *)il2cpp_codegen_object_new (Exception_t232_il2cpp_TypeInfo_var);
		Exception__ctor_m719(L_4, (String_t*) &_stringLiteral189, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_4);
	}

IL_002d:
	{
		return;
	}
}
// System.Void UnityStandardAssets.Utility.FOVKick::ChangeCamera(UnityEngine.Camera)
extern "C" void FOVKick_ChangeCamera_m488 (FOVKick_t178 * __this, Camera_t27 * ___camera, const MethodInfo* method)
{
	{
		Camera_t27 * L_0 = ___camera;
		__this->___Camera_0 = L_0;
		return;
	}
}
// System.Collections.IEnumerator UnityStandardAssets.Utility.FOVKick::FOVKickUp()
extern TypeInfo* U3CFOVKickUpU3Ec__Iterator4_t179_il2cpp_TypeInfo_var;
extern "C" Object_t * FOVKick_FOVKickUp_m489 (FOVKick_t178 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CFOVKickUpU3Ec__Iterator4_t179_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(89);
		s_Il2CppMethodIntialized = true;
	}
	U3CFOVKickUpU3Ec__Iterator4_t179 * V_0 = {0};
	{
		U3CFOVKickUpU3Ec__Iterator4_t179 * L_0 = (U3CFOVKickUpU3Ec__Iterator4_t179 *)il2cpp_codegen_object_new (U3CFOVKickUpU3Ec__Iterator4_t179_il2cpp_TypeInfo_var);
		U3CFOVKickUpU3Ec__Iterator4__ctor_m473(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CFOVKickUpU3Ec__Iterator4_t179 * L_1 = V_0;
		NullCheck(L_1);
		L_1->___U3CU3Ef__this_3 = __this;
		U3CFOVKickUpU3Ec__Iterator4_t179 * L_2 = V_0;
		return L_2;
	}
}
// System.Collections.IEnumerator UnityStandardAssets.Utility.FOVKick::FOVKickDown()
extern TypeInfo* U3CFOVKickDownU3Ec__Iterator5_t180_il2cpp_TypeInfo_var;
extern "C" Object_t * FOVKick_FOVKickDown_m490 (FOVKick_t178 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CFOVKickDownU3Ec__Iterator5_t180_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(90);
		s_Il2CppMethodIntialized = true;
	}
	U3CFOVKickDownU3Ec__Iterator5_t180 * V_0 = {0};
	{
		U3CFOVKickDownU3Ec__Iterator5_t180 * L_0 = (U3CFOVKickDownU3Ec__Iterator5_t180 *)il2cpp_codegen_object_new (U3CFOVKickDownU3Ec__Iterator5_t180_il2cpp_TypeInfo_var);
		U3CFOVKickDownU3Ec__Iterator5__ctor_m479(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CFOVKickDownU3Ec__Iterator5_t180 * L_1 = V_0;
		NullCheck(L_1);
		L_1->___U3CU3Ef__this_3 = __this;
		U3CFOVKickDownU3Ec__Iterator5_t180 * L_2 = V_0;
		return L_2;
	}
}
// UnityStandardAssets.Utility.FPSCounter
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_FP.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.Utility.FPSCounter
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_FPMethodDeclarations.h"

// UnityEngine.GUIText
#include "UnityEngine_UnityEngine_GUIText.h"
// UnityEngine.GUIText
#include "UnityEngine_UnityEngine_GUITextMethodDeclarations.h"
struct Component_t219;
struct GUIText_t181;
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.GUIText>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.GUIText>()
#define Component_GetComponent_TisGUIText_t181_m996(__this, method) (( GUIText_t181 * (*) (Component_t219 *, const MethodInfo*))Component_GetComponent_TisObject_t_m614_gshared)(__this, method)


// System.Void UnityStandardAssets.Utility.FPSCounter::.ctor()
extern "C" void FPSCounter__ctor_m491 (FPSCounter_t182 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Utility.FPSCounter::Start()
extern const MethodInfo* Component_GetComponent_TisGUIText_t181_m996_MethodInfo_var;
extern "C" void FPSCounter_Start_m492 (FPSCounter_t182 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisGUIText_t181_m996_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483678);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = Time_get_realtimeSinceStartup_m961(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___m_FpsNextPeriod_5 = ((float)((float)L_0+(float)(0.5f)));
		GUIText_t181 * L_1 = Component_GetComponent_TisGUIText_t181_m996(__this, /*hidden argument*/Component_GetComponent_TisGUIText_t181_m996_MethodInfo_var);
		__this->___m_GuiText_7 = L_1;
		return;
	}
}
// System.Void UnityStandardAssets.Utility.FPSCounter::Update()
extern TypeInfo* Int32_t253_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void FPSCounter_Update_m493 (FPSCounter_t182 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32_t253_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(54);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (__this->___m_FpsAccumulator_4);
		__this->___m_FpsAccumulator_4 = ((int32_t)((int32_t)L_0+(int32_t)1));
		float L_1 = Time_get_realtimeSinceStartup_m961(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_2 = (__this->___m_FpsNextPeriod_5);
		if ((!(((float)L_1) > ((float)L_2))))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_3 = (__this->___m_FpsAccumulator_4);
		__this->___m_CurrentFps_6 = (((int32_t)((float)((float)(((float)L_3))/(float)(0.5f)))));
		__this->___m_FpsAccumulator_4 = 0;
		float L_4 = (__this->___m_FpsNextPeriod_5);
		__this->___m_FpsNextPeriod_5 = ((float)((float)L_4+(float)(0.5f)));
		GUIText_t181 * L_5 = (__this->___m_GuiText_7);
		int32_t L_6 = (__this->___m_CurrentFps_6);
		int32_t L_7 = L_6;
		Object_t * L_8 = Box(Int32_t253_il2cpp_TypeInfo_var, &L_7);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Format_m997(NULL /*static, unused*/, (String_t*) &_stringLiteral190, L_8, /*hidden argument*/NULL);
		NullCheck(L_5);
		GUIText_set_text_m998(L_5, L_9, /*hidden argument*/NULL);
	}

IL_006b:
	{
		return;
	}
}
// UnityStandardAssets.Utility.FollowTarget
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Fo_2.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.Utility.FollowTarget
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Fo_2MethodDeclarations.h"



// System.Void UnityStandardAssets.Utility.FollowTarget::.ctor()
extern "C" void FollowTarget__ctor_m494 (FollowTarget_t183 * __this, const MethodInfo* method)
{
	{
		Vector3_t4  L_0 = {0};
		Vector3__ctor_m612(&L_0, (0.0f), (7.5f), (0.0f), /*hidden argument*/NULL);
		__this->___offset_3 = L_0;
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Utility.FollowTarget::LateUpdate()
extern "C" void FollowTarget_LateUpdate_m495 (FollowTarget_t183 * __this, const MethodInfo* method)
{
	{
		Transform_t1 * L_0 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		Transform_t1 * L_1 = (__this->___target_2);
		NullCheck(L_1);
		Vector3_t4  L_2 = Transform_get_position_m593(L_1, /*hidden argument*/NULL);
		Vector3_t4  L_3 = (__this->___offset_3);
		Vector3_t4  L_4 = Vector3_op_Addition_m604(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_set_position_m607(L_0, L_4, /*hidden argument*/NULL);
		return;
	}
}
// ForcedReset
#include "AssemblyU2DCSharpU2Dfirstpass_ForcedReset.h"
#ifndef _MSC_VER
#else
#endif
// ForcedReset
#include "AssemblyU2DCSharpU2Dfirstpass_ForcedResetMethodDeclarations.h"

// UnityEngine.AsyncOperation
#include "UnityEngine_UnityEngine_AsyncOperation.h"
// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_4MethodDeclarations.h"
// UnityEngine.Application
#include "UnityEngine_UnityEngine_ApplicationMethodDeclarations.h"


// System.Void ForcedReset::.ctor()
extern "C" void ForcedReset__ctor_m496 (ForcedReset_t184 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ForcedReset::Update()
extern TypeInfo* CrossPlatformInputManager_t35_il2cpp_TypeInfo_var;
extern "C" void ForcedReset_Update_m497 (ForcedReset_t184 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t35_il2cpp_TypeInfo_var);
		bool L_0 = CrossPlatformInputManager_GetButtonDown_m104(NULL /*static, unused*/, (String_t*) &_stringLiteral191, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		String_t* L_1 = Application_get_loadedLevelName_m637(NULL /*static, unused*/, /*hidden argument*/NULL);
		Application_LoadLevelAsync_m999(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return;
	}
}
// UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>c__Iterator6
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Le.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>c__Iterator6
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_LeMethodDeclarations.h"

// UnityStandardAssets.Utility.LerpControlledBob
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Le_0.h"
// UnityEngine.WaitForFixedUpdate
#include "UnityEngine_UnityEngine_WaitForFixedUpdate.h"
// UnityEngine.WaitForFixedUpdate
#include "UnityEngine_UnityEngine_WaitForFixedUpdateMethodDeclarations.h"


// System.Void UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>c__Iterator6::.ctor()
extern "C" void U3CDoBobCycleU3Ec__Iterator6__ctor_m498 (U3CDoBobCycleU3Ec__Iterator6_t186 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m677(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>c__Iterator6::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CDoBobCycleU3Ec__Iterator6_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m499 (U3CDoBobCycleU3Ec__Iterator6_t186 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_2);
		return L_0;
	}
}
// System.Object UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>c__Iterator6::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CDoBobCycleU3Ec__Iterator6_System_Collections_IEnumerator_get_Current_m500 (U3CDoBobCycleU3Ec__Iterator6_t186 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_2);
		return L_0;
	}
}
// System.Boolean UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>c__Iterator6::MoveNext()
extern TypeInfo* Mathf_t218_il2cpp_TypeInfo_var;
extern TypeInfo* WaitForFixedUpdate_t256_il2cpp_TypeInfo_var;
extern "C" bool U3CDoBobCycleU3Ec__Iterator6_MoveNext_m501 (U3CDoBobCycleU3Ec__Iterator6_t186 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		WaitForFixedUpdate_t256_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(92);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_1);
		V_0 = L_0;
		__this->___U24PC_1 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0025;
		}
		if (L_1 == 1)
		{
			goto IL_0090;
		}
		if (L_1 == 2)
		{
			goto IL_0111;
		}
	}
	{
		goto IL_013e;
	}

IL_0025:
	{
		__this->___U3CtU3E__0_0 = (0.0f);
		goto IL_0090;
	}

IL_0035:
	{
		LerpControlledBob_t185 * L_2 = (__this->___U3CU3Ef__this_3);
		LerpControlledBob_t185 * L_3 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_3);
		float L_4 = (L_3->___BobAmount_1);
		float L_5 = (__this->___U3CtU3E__0_0);
		LerpControlledBob_t185 * L_6 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_6);
		float L_7 = (L_6->___BobDuration_0);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_8 = Mathf_Lerp_m610(NULL /*static, unused*/, (0.0f), L_4, ((float)((float)L_5/(float)L_7)), /*hidden argument*/NULL);
		NullCheck(L_2);
		L_2->___m_Offset_2 = L_8;
		float L_9 = (__this->___U3CtU3E__0_0);
		float L_10 = Time_get_deltaTime_m602(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___U3CtU3E__0_0 = ((float)((float)L_9+(float)L_10));
		WaitForFixedUpdate_t256 * L_11 = (WaitForFixedUpdate_t256 *)il2cpp_codegen_object_new (WaitForFixedUpdate_t256_il2cpp_TypeInfo_var);
		WaitForFixedUpdate__ctor_m1000(L_11, /*hidden argument*/NULL);
		__this->___U24current_2 = L_11;
		__this->___U24PC_1 = 1;
		goto IL_0140;
	}

IL_0090:
	{
		float L_12 = (__this->___U3CtU3E__0_0);
		LerpControlledBob_t185 * L_13 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_13);
		float L_14 = (L_13->___BobDuration_0);
		if ((((float)L_12) < ((float)L_14)))
		{
			goto IL_0035;
		}
	}
	{
		__this->___U3CtU3E__0_0 = (0.0f);
		goto IL_0111;
	}

IL_00b6:
	{
		LerpControlledBob_t185 * L_15 = (__this->___U3CU3Ef__this_3);
		LerpControlledBob_t185 * L_16 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_16);
		float L_17 = (L_16->___BobAmount_1);
		float L_18 = (__this->___U3CtU3E__0_0);
		LerpControlledBob_t185 * L_19 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_19);
		float L_20 = (L_19->___BobDuration_0);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_21 = Mathf_Lerp_m610(NULL /*static, unused*/, L_17, (0.0f), ((float)((float)L_18/(float)L_20)), /*hidden argument*/NULL);
		NullCheck(L_15);
		L_15->___m_Offset_2 = L_21;
		float L_22 = (__this->___U3CtU3E__0_0);
		float L_23 = Time_get_deltaTime_m602(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___U3CtU3E__0_0 = ((float)((float)L_22+(float)L_23));
		WaitForFixedUpdate_t256 * L_24 = (WaitForFixedUpdate_t256 *)il2cpp_codegen_object_new (WaitForFixedUpdate_t256_il2cpp_TypeInfo_var);
		WaitForFixedUpdate__ctor_m1000(L_24, /*hidden argument*/NULL);
		__this->___U24current_2 = L_24;
		__this->___U24PC_1 = 2;
		goto IL_0140;
	}

IL_0111:
	{
		float L_25 = (__this->___U3CtU3E__0_0);
		LerpControlledBob_t185 * L_26 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_26);
		float L_27 = (L_26->___BobDuration_0);
		if ((((float)L_25) < ((float)L_27)))
		{
			goto IL_00b6;
		}
	}
	{
		LerpControlledBob_t185 * L_28 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_28);
		L_28->___m_Offset_2 = (0.0f);
		__this->___U24PC_1 = (-1);
	}

IL_013e:
	{
		return 0;
	}

IL_0140:
	{
		return 1;
	}
	// Dead block : IL_0142: ldloc.1
}
// System.Void UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>c__Iterator6::Dispose()
extern "C" void U3CDoBobCycleU3Ec__Iterator6_Dispose_m502 (U3CDoBobCycleU3Ec__Iterator6_t186 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_1 = (-1);
		return;
	}
}
// System.Void UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>c__Iterator6::Reset()
extern TypeInfo* NotSupportedException_t244_il2cpp_TypeInfo_var;
extern "C" void U3CDoBobCycleU3Ec__Iterator6_Reset_m503 (U3CDoBobCycleU3Ec__Iterator6_t186 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t244_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t244 * L_0 = (NotSupportedException_t244 *)il2cpp_codegen_object_new (NotSupportedException_t244_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m905(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.Utility.LerpControlledBob
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Le_0MethodDeclarations.h"



// System.Void UnityStandardAssets.Utility.LerpControlledBob::.ctor()
extern "C" void LerpControlledBob__ctor_m504 (LerpControlledBob_t185 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m677(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityStandardAssets.Utility.LerpControlledBob::Offset()
extern "C" float LerpControlledBob_Offset_m505 (LerpControlledBob_t185 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Offset_2);
		return L_0;
	}
}
// System.Collections.IEnumerator UnityStandardAssets.Utility.LerpControlledBob::DoBobCycle()
extern TypeInfo* U3CDoBobCycleU3Ec__Iterator6_t186_il2cpp_TypeInfo_var;
extern "C" Object_t * LerpControlledBob_DoBobCycle_m506 (LerpControlledBob_t185 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CDoBobCycleU3Ec__Iterator6_t186_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(93);
		s_Il2CppMethodIntialized = true;
	}
	U3CDoBobCycleU3Ec__Iterator6_t186 * V_0 = {0};
	{
		U3CDoBobCycleU3Ec__Iterator6_t186 * L_0 = (U3CDoBobCycleU3Ec__Iterator6_t186 *)il2cpp_codegen_object_new (U3CDoBobCycleU3Ec__Iterator6_t186_il2cpp_TypeInfo_var);
		U3CDoBobCycleU3Ec__Iterator6__ctor_m498(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CDoBobCycleU3Ec__Iterator6_t186 * L_1 = V_0;
		NullCheck(L_1);
		L_1->___U3CU3Ef__this_3 = __this;
		U3CDoBobCycleU3Ec__Iterator6_t186 * L_2 = V_0;
		return L_2;
	}
}
// UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator7
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Ob.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator7
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_ObMethodDeclarations.h"

// UnityEngine.WaitForSeconds
#include "UnityEngine_UnityEngine_WaitForSeconds.h"
// System.Collections.Generic.List`1<UnityEngine.Transform>
#include "mscorlib_System_Collections_Generic_List_1_gen_2.h"
// UnityEngine.WaitForSeconds
#include "UnityEngine_UnityEngine_WaitForSecondsMethodDeclarations.h"
// System.Collections.Generic.List`1<UnityEngine.Transform>
#include "mscorlib_System_Collections_Generic_List_1_gen_2MethodDeclarations.h"
struct Component_t219;
struct TransformU5BU5D_t141;
// Declaration !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.Transform>()
// !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.Transform>()
#define Component_GetComponentsInChildren_TisTransform_t1_m1001(__this, method) (( TransformU5BU5D_t141* (*) (Component_t219 *, const MethodInfo*))Component_GetComponentsInChildren_TisObject_t_m698_gshared)(__this, method)


// System.Void UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator7::.ctor()
extern "C" void U3CResetCoroutineU3Ec__Iterator7__ctor_m507 (U3CResetCoroutineU3Ec__Iterator7_t187 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m677(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator7::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CResetCoroutineU3Ec__Iterator7_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m508 (U3CResetCoroutineU3Ec__Iterator7_t187 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_5);
		return L_0;
	}
}
// System.Object UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator7::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CResetCoroutineU3Ec__Iterator7_System_Collections_IEnumerator_get_Current_m509 (U3CResetCoroutineU3Ec__Iterator7_t187 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_5);
		return L_0;
	}
}
// System.Boolean UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator7::MoveNext()
extern TypeInfo* WaitForSeconds_t257_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisTransform_t1_m1001_MethodInfo_var;
extern "C" bool U3CResetCoroutineU3Ec__Iterator7_MoveNext_m510 (U3CResetCoroutineU3Ec__Iterator7_t187 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WaitForSeconds_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(94);
		Component_GetComponentsInChildren_TisTransform_t1_m1001_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483679);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_4);
		V_0 = L_0;
		__this->___U24PC_4 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_003e;
		}
	}
	{
		goto IL_0142;
	}

IL_0021:
	{
		float L_2 = (__this->___delay_0);
		WaitForSeconds_t257 * L_3 = (WaitForSeconds_t257 *)il2cpp_codegen_object_new (WaitForSeconds_t257_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1002(L_3, L_2, /*hidden argument*/NULL);
		__this->___U24current_5 = L_3;
		__this->___U24PC_4 = 1;
		goto IL_0144;
	}

IL_003e:
	{
		ObjectResetter_t149 * L_4 = (__this->___U3CU3Ef__this_7);
		NullCheck(L_4);
		TransformU5BU5D_t141* L_5 = Component_GetComponentsInChildren_TisTransform_t1_m1001(L_4, /*hidden argument*/Component_GetComponentsInChildren_TisTransform_t1_m1001_MethodInfo_var);
		__this->___U3CU24s_23U3E__0_1 = L_5;
		__this->___U3CU24s_24U3E__1_2 = 0;
		goto IL_00a3;
	}

IL_005b:
	{
		TransformU5BU5D_t141* L_6 = (__this->___U3CU24s_23U3E__0_1);
		int32_t L_7 = (__this->___U3CU24s_24U3E__1_2);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		__this->___U3CtU3E__2_3 = (*(Transform_t1 **)(Transform_t1 **)SZArrayLdElema(L_6, L_8));
		ObjectResetter_t149 * L_9 = (__this->___U3CU3Ef__this_7);
		NullCheck(L_9);
		List_1_t188 * L_10 = (L_9->___originalStructure_4);
		Transform_t1 * L_11 = (__this->___U3CtU3E__2_3);
		NullCheck(L_10);
		bool L_12 = (bool)VirtFuncInvoker1< bool, Transform_t1 * >::Invoke(24 /* System.Boolean System.Collections.Generic.List`1<UnityEngine.Transform>::Contains(!0) */, L_10, L_11);
		if (L_12)
		{
			goto IL_0095;
		}
	}
	{
		Transform_t1 * L_13 = (__this->___U3CtU3E__2_3);
		NullCheck(L_13);
		Transform_set_parent_m596(L_13, (Transform_t1 *)NULL, /*hidden argument*/NULL);
	}

IL_0095:
	{
		int32_t L_14 = (__this->___U3CU24s_24U3E__1_2);
		__this->___U3CU24s_24U3E__1_2 = ((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_00a3:
	{
		int32_t L_15 = (__this->___U3CU24s_24U3E__1_2);
		TransformU5BU5D_t141* L_16 = (__this->___U3CU24s_23U3E__0_1);
		NullCheck(L_16);
		if ((((int32_t)L_15) < ((int32_t)(((int32_t)(((Array_t *)L_16)->max_length))))))
		{
			goto IL_005b;
		}
	}
	{
		ObjectResetter_t149 * L_17 = (__this->___U3CU3Ef__this_7);
		NullCheck(L_17);
		Transform_t1 * L_18 = Component_get_transform_m594(L_17, /*hidden argument*/NULL);
		ObjectResetter_t149 * L_19 = (__this->___U3CU3Ef__this_7);
		NullCheck(L_19);
		Vector3_t4  L_20 = (L_19->___originalPosition_2);
		NullCheck(L_18);
		Transform_set_position_m607(L_18, L_20, /*hidden argument*/NULL);
		ObjectResetter_t149 * L_21 = (__this->___U3CU3Ef__this_7);
		NullCheck(L_21);
		Transform_t1 * L_22 = Component_get_transform_m594(L_21, /*hidden argument*/NULL);
		ObjectResetter_t149 * L_23 = (__this->___U3CU3Ef__this_7);
		NullCheck(L_23);
		Quaternion_t19  L_24 = (L_23->___originalRotation_3);
		NullCheck(L_22);
		Transform_set_rotation_m658(L_22, L_24, /*hidden argument*/NULL);
		ObjectResetter_t149 * L_25 = (__this->___U3CU3Ef__this_7);
		NullCheck(L_25);
		Rigidbody_t14 * L_26 = (L_25->___Rigidbody_5);
		bool L_27 = Object_op_Implicit_m629(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_012b;
		}
	}
	{
		ObjectResetter_t149 * L_28 = (__this->___U3CU3Ef__this_7);
		NullCheck(L_28);
		Rigidbody_t14 * L_29 = (L_28->___Rigidbody_5);
		Vector3_t4  L_30 = Vector3_get_zero_m601(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_29);
		Rigidbody_set_velocity_m1003(L_29, L_30, /*hidden argument*/NULL);
		ObjectResetter_t149 * L_31 = (__this->___U3CU3Ef__this_7);
		NullCheck(L_31);
		Rigidbody_t14 * L_32 = (L_31->___Rigidbody_5);
		Vector3_t4  L_33 = Vector3_get_zero_m601(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_32);
		Rigidbody_set_angularVelocity_m1004(L_32, L_33, /*hidden argument*/NULL);
	}

IL_012b:
	{
		ObjectResetter_t149 * L_34 = (__this->___U3CU3Ef__this_7);
		NullCheck(L_34);
		Component_SendMessage_m918(L_34, (String_t*) &_stringLiteral192, /*hidden argument*/NULL);
		__this->___U24PC_4 = (-1);
	}

IL_0142:
	{
		return 0;
	}

IL_0144:
	{
		return 1;
	}
	// Dead block : IL_0146: ldloc.1
}
// System.Void UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator7::Dispose()
extern "C" void U3CResetCoroutineU3Ec__Iterator7_Dispose_m511 (U3CResetCoroutineU3Ec__Iterator7_t187 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_4 = (-1);
		return;
	}
}
// System.Void UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator7::Reset()
extern TypeInfo* NotSupportedException_t244_il2cpp_TypeInfo_var;
extern "C" void U3CResetCoroutineU3Ec__Iterator7_Reset_m512 (U3CResetCoroutineU3Ec__Iterator7_t187 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t244_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t244 * L_0 = (NotSupportedException_t244 *)il2cpp_codegen_object_new (NotSupportedException_t244_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m905(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
#ifndef _MSC_VER
#else
#endif

struct Component_t219;
struct Rigidbody_t14;
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody>()
#define Component_GetComponent_TisRigidbody_t14_m639(__this, method) (( Rigidbody_t14 * (*) (Component_t219 *, const MethodInfo*))Component_GetComponent_TisObject_t_m614_gshared)(__this, method)


// System.Void UnityStandardAssets.Utility.ObjectResetter::.ctor()
extern "C" void ObjectResetter__ctor_m513 (ObjectResetter_t149 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Utility.ObjectResetter::Start()
extern TypeInfo* List_1_t188_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisTransform_t1_m1001_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m1005_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisRigidbody_t14_m639_MethodInfo_var;
extern "C" void ObjectResetter_Start_m514 (ObjectResetter_t149 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t188_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(95);
		Component_GetComponentsInChildren_TisTransform_t1_m1001_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483679);
		List_1__ctor_m1005_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483680);
		Component_GetComponent_TisRigidbody_t14_m639_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483652);
		s_Il2CppMethodIntialized = true;
	}
	{
		TransformU5BU5D_t141* L_0 = Component_GetComponentsInChildren_TisTransform_t1_m1001(__this, /*hidden argument*/Component_GetComponentsInChildren_TisTransform_t1_m1001_MethodInfo_var);
		List_1_t188 * L_1 = (List_1_t188 *)il2cpp_codegen_object_new (List_1_t188_il2cpp_TypeInfo_var);
		List_1__ctor_m1005(L_1, (Object_t*)(Object_t*)L_0, /*hidden argument*/List_1__ctor_m1005_MethodInfo_var);
		__this->___originalStructure_4 = L_1;
		Transform_t1 * L_2 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t4  L_3 = Transform_get_position_m593(L_2, /*hidden argument*/NULL);
		__this->___originalPosition_2 = L_3;
		Transform_t1 * L_4 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Quaternion_t19  L_5 = Transform_get_rotation_m656(L_4, /*hidden argument*/NULL);
		__this->___originalRotation_3 = L_5;
		Rigidbody_t14 * L_6 = Component_GetComponent_TisRigidbody_t14_m639(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t14_m639_MethodInfo_var);
		__this->___Rigidbody_5 = L_6;
		return;
	}
}
// System.Void UnityStandardAssets.Utility.ObjectResetter::DelayedReset(System.Single)
extern "C" void ObjectResetter_DelayedReset_m515 (ObjectResetter_t149 * __this, float ___delay, const MethodInfo* method)
{
	{
		float L_0 = ___delay;
		Object_t * L_1 = ObjectResetter_ResetCoroutine_m516(__this, L_0, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m1006(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator UnityStandardAssets.Utility.ObjectResetter::ResetCoroutine(System.Single)
extern TypeInfo* U3CResetCoroutineU3Ec__Iterator7_t187_il2cpp_TypeInfo_var;
extern "C" Object_t * ObjectResetter_ResetCoroutine_m516 (ObjectResetter_t149 * __this, float ___delay, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CResetCoroutineU3Ec__Iterator7_t187_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(96);
		s_Il2CppMethodIntialized = true;
	}
	U3CResetCoroutineU3Ec__Iterator7_t187 * V_0 = {0};
	{
		U3CResetCoroutineU3Ec__Iterator7_t187 * L_0 = (U3CResetCoroutineU3Ec__Iterator7_t187 *)il2cpp_codegen_object_new (U3CResetCoroutineU3Ec__Iterator7_t187_il2cpp_TypeInfo_var);
		U3CResetCoroutineU3Ec__Iterator7__ctor_m507(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CResetCoroutineU3Ec__Iterator7_t187 * L_1 = V_0;
		float L_2 = ___delay;
		NullCheck(L_1);
		L_1->___delay_0 = L_2;
		U3CResetCoroutineU3Ec__Iterator7_t187 * L_3 = V_0;
		float L_4 = ___delay;
		NullCheck(L_3);
		L_3->___U3CU24U3Edelay_6 = L_4;
		U3CResetCoroutineU3Ec__Iterator7_t187 * L_5 = V_0;
		NullCheck(L_5);
		L_5->___U3CU3Ef__this_7 = __this;
		U3CResetCoroutineU3Ec__Iterator7_t187 * L_6 = V_0;
		return L_6;
	}
}
// UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator8
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Pa.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator8
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_PaMethodDeclarations.h"

// UnityStandardAssets.Utility.ParticleSystemDestroyer
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Pa_0.h"


// System.Void UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator8::.ctor()
extern "C" void U3CStartU3Ec__Iterator8__ctor_m517 (U3CStartU3Ec__Iterator8_t190 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m677(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator8::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CStartU3Ec__Iterator8_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m518 (U3CStartU3Ec__Iterator8_t190 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_9);
		return L_0;
	}
}
// System.Object UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator8::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CStartU3Ec__Iterator8_System_Collections_IEnumerator_get_Current_m519 (U3CStartU3Ec__Iterator8_t190 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_9);
		return L_0;
	}
}
// System.Boolean UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator8::MoveNext()
extern TypeInfo* Mathf_t218_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* WaitForSeconds_t257_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisParticleSystem_t161_m920_MethodInfo_var;
extern "C" bool U3CStartU3Ec__Iterator8_MoveNext_m520 (U3CStartU3Ec__Iterator8_t190 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		WaitForSeconds_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(94);
		Component_GetComponentsInChildren_TisParticleSystem_t161_m920_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483668);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_8);
		V_0 = L_0;
		__this->___U24PC_8 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0025;
		}
		if (L_1 == 1)
		{
			goto IL_00e7;
		}
		if (L_1 == 2)
		{
			goto IL_01ac;
		}
	}
	{
		goto IL_01c3;
	}

IL_0025:
	{
		ParticleSystemDestroyer_t189 * L_2 = (__this->___U3CU3Ef__this_10);
		NullCheck(L_2);
		ParticleSystemU5BU5D_t150* L_3 = Component_GetComponentsInChildren_TisParticleSystem_t161_m920(L_2, /*hidden argument*/Component_GetComponentsInChildren_TisParticleSystem_t161_m920_MethodInfo_var);
		__this->___U3CsystemsU3E__0_0 = L_3;
		ParticleSystemU5BU5D_t150* L_4 = (__this->___U3CsystemsU3E__0_0);
		__this->___U3CU24s_25U3E__1_1 = L_4;
		__this->___U3CU24s_26U3E__2_2 = 0;
		goto IL_0095;
	}

IL_004e:
	{
		ParticleSystemU5BU5D_t150* L_5 = (__this->___U3CU24s_25U3E__1_1);
		int32_t L_6 = (__this->___U3CU24s_26U3E__2_2);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		__this->___U3CsystemU3E__3_3 = (*(ParticleSystem_t161 **)(ParticleSystem_t161 **)SZArrayLdElema(L_5, L_7));
		ParticleSystemDestroyer_t189 * L_8 = (__this->___U3CU3Ef__this_10);
		ParticleSystem_t161 * L_9 = (__this->___U3CsystemU3E__3_3);
		NullCheck(L_9);
		float L_10 = ParticleSystem_get_startLifetime_m932(L_9, /*hidden argument*/NULL);
		ParticleSystemDestroyer_t189 * L_11 = (__this->___U3CU3Ef__this_10);
		NullCheck(L_11);
		float L_12 = (L_11->___m_MaxLifetime_4);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_13 = Mathf_Max_m782(NULL /*static, unused*/, L_10, L_12, /*hidden argument*/NULL);
		NullCheck(L_8);
		L_8->___m_MaxLifetime_4 = L_13;
		int32_t L_14 = (__this->___U3CU24s_26U3E__2_2);
		__this->___U3CU24s_26U3E__2_2 = ((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_0095:
	{
		int32_t L_15 = (__this->___U3CU24s_26U3E__2_2);
		ParticleSystemU5BU5D_t150* L_16 = (__this->___U3CU24s_25U3E__1_1);
		NullCheck(L_16);
		if ((((int32_t)L_15) < ((int32_t)(((int32_t)(((Array_t *)L_16)->max_length))))))
		{
			goto IL_004e;
		}
	}
	{
		float L_17 = Time_get_time_m668(NULL /*static, unused*/, /*hidden argument*/NULL);
		ParticleSystemDestroyer_t189 * L_18 = (__this->___U3CU3Ef__this_10);
		NullCheck(L_18);
		float L_19 = (L_18->___minDuration_2);
		ParticleSystemDestroyer_t189 * L_20 = (__this->___U3CU3Ef__this_10);
		NullCheck(L_20);
		float L_21 = (L_20->___maxDuration_3);
		float L_22 = Random_Range_m856(NULL /*static, unused*/, L_19, L_21, /*hidden argument*/NULL);
		__this->___U3CstopTimeU3E__4_4 = ((float)((float)L_17+(float)L_22));
		goto IL_00e7;
	}

IL_00d4:
	{
		__this->___U24current_9 = NULL;
		__this->___U24PC_8 = 1;
		goto IL_01c5;
	}

IL_00e7:
	{
		float L_23 = Time_get_time_m668(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_24 = (__this->___U3CstopTimeU3E__4_4);
		if ((((float)L_23) < ((float)L_24)))
		{
			goto IL_00d4;
		}
	}
	{
		ParticleSystemDestroyer_t189 * L_25 = (__this->___U3CU3Ef__this_10);
		NullCheck(L_25);
		bool L_26 = (L_25->___m_EarlyStop_5);
		if (L_26)
		{
			goto IL_00d4;
		}
	}
	{
		ParticleSystemDestroyer_t189 * L_27 = (__this->___U3CU3Ef__this_10);
		NullCheck(L_27);
		String_t* L_28 = Object_get_name_m798(L_27, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_29 = String_Concat_m860(NULL /*static, unused*/, (String_t*) &_stringLiteral193, L_28, /*hidden argument*/NULL);
		Debug_Log_m857(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		ParticleSystemU5BU5D_t150* L_30 = (__this->___U3CsystemsU3E__0_0);
		__this->___U3CU24s_27U3E__5_5 = L_30;
		__this->___U3CU24s_28U3E__6_6 = 0;
		goto IL_0166;
	}

IL_0139:
	{
		ParticleSystemU5BU5D_t150* L_31 = (__this->___U3CU24s_27U3E__5_5);
		int32_t L_32 = (__this->___U3CU24s_28U3E__6_6);
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, L_32);
		int32_t L_33 = L_32;
		__this->___U3CsystemU3E__7_7 = (*(ParticleSystem_t161 **)(ParticleSystem_t161 **)SZArrayLdElema(L_31, L_33));
		ParticleSystem_t161 * L_34 = (__this->___U3CsystemU3E__7_7);
		NullCheck(L_34);
		ParticleSystem_set_enableEmission_m921(L_34, 0, /*hidden argument*/NULL);
		int32_t L_35 = (__this->___U3CU24s_28U3E__6_6);
		__this->___U3CU24s_28U3E__6_6 = ((int32_t)((int32_t)L_35+(int32_t)1));
	}

IL_0166:
	{
		int32_t L_36 = (__this->___U3CU24s_28U3E__6_6);
		ParticleSystemU5BU5D_t150* L_37 = (__this->___U3CU24s_27U3E__5_5);
		NullCheck(L_37);
		if ((((int32_t)L_36) < ((int32_t)(((int32_t)(((Array_t *)L_37)->max_length))))))
		{
			goto IL_0139;
		}
	}
	{
		ParticleSystemDestroyer_t189 * L_38 = (__this->___U3CU3Ef__this_10);
		NullCheck(L_38);
		Component_BroadcastMessage_m1007(L_38, (String_t*) &_stringLiteral175, 1, /*hidden argument*/NULL);
		ParticleSystemDestroyer_t189 * L_39 = (__this->___U3CU3Ef__this_10);
		NullCheck(L_39);
		float L_40 = (L_39->___m_MaxLifetime_4);
		WaitForSeconds_t257 * L_41 = (WaitForSeconds_t257 *)il2cpp_codegen_object_new (WaitForSeconds_t257_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1002(L_41, L_40, /*hidden argument*/NULL);
		__this->___U24current_9 = L_41;
		__this->___U24PC_8 = 2;
		goto IL_01c5;
	}

IL_01ac:
	{
		ParticleSystemDestroyer_t189 * L_42 = (__this->___U3CU3Ef__this_10);
		NullCheck(L_42);
		GameObject_t78 * L_43 = Component_get_gameObject_m622(L_42, /*hidden argument*/NULL);
		Object_Destroy_m1008(NULL /*static, unused*/, L_43, /*hidden argument*/NULL);
		__this->___U24PC_8 = (-1);
	}

IL_01c3:
	{
		return 0;
	}

IL_01c5:
	{
		return 1;
	}
	// Dead block : IL_01c7: ldloc.1
}
// System.Void UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator8::Dispose()
extern "C" void U3CStartU3Ec__Iterator8_Dispose_m521 (U3CStartU3Ec__Iterator8_t190 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_8 = (-1);
		return;
	}
}
// System.Void UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator8::Reset()
extern TypeInfo* NotSupportedException_t244_il2cpp_TypeInfo_var;
extern "C" void U3CStartU3Ec__Iterator8_Reset_m522 (U3CStartU3Ec__Iterator8_t190 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t244_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t244 * L_0 = (NotSupportedException_t244 *)il2cpp_codegen_object_new (NotSupportedException_t244_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m905(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.Utility.ParticleSystemDestroyer
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Pa_0MethodDeclarations.h"



// System.Void UnityStandardAssets.Utility.ParticleSystemDestroyer::.ctor()
extern "C" void ParticleSystemDestroyer__ctor_m523 (ParticleSystemDestroyer_t189 * __this, const MethodInfo* method)
{
	{
		__this->___minDuration_2 = (8.0f);
		__this->___maxDuration_3 = (10.0f);
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator UnityStandardAssets.Utility.ParticleSystemDestroyer::Start()
extern TypeInfo* U3CStartU3Ec__Iterator8_t190_il2cpp_TypeInfo_var;
extern "C" Object_t * ParticleSystemDestroyer_Start_m524 (ParticleSystemDestroyer_t189 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CStartU3Ec__Iterator8_t190_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(97);
		s_Il2CppMethodIntialized = true;
	}
	U3CStartU3Ec__Iterator8_t190 * V_0 = {0};
	{
		U3CStartU3Ec__Iterator8_t190 * L_0 = (U3CStartU3Ec__Iterator8_t190 *)il2cpp_codegen_object_new (U3CStartU3Ec__Iterator8_t190_il2cpp_TypeInfo_var);
		U3CStartU3Ec__Iterator8__ctor_m517(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CStartU3Ec__Iterator8_t190 * L_1 = V_0;
		NullCheck(L_1);
		L_1->___U3CU3Ef__this_10 = __this;
		U3CStartU3Ec__Iterator8_t190 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityStandardAssets.Utility.ParticleSystemDestroyer::Stop()
extern "C" void ParticleSystemDestroyer_Stop_m525 (ParticleSystemDestroyer_t189 * __this, const MethodInfo* method)
{
	{
		__this->___m_EarlyStop_5 = 1;
		return;
	}
}
// UnityStandardAssets.Utility.PlatformSpecificContent/BuildTargetGroup
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Pl.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.Utility.PlatformSpecificContent/BuildTargetGroup
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_PlMethodDeclarations.h"



// UnityStandardAssets.Utility.PlatformSpecificContent
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Pl_0.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.Utility.PlatformSpecificContent
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Pl_0MethodDeclarations.h"

// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"


// System.Void UnityStandardAssets.Utility.PlatformSpecificContent::.ctor()
extern TypeInfo* GameObjectU5BU5D_t192_il2cpp_TypeInfo_var;
extern TypeInfo* MonoBehaviourU5BU5D_t193_il2cpp_TypeInfo_var;
extern "C" void PlatformSpecificContent__ctor_m526 (PlatformSpecificContent_t194 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObjectU5BU5D_t192_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(98);
		MonoBehaviourU5BU5D_t193_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(99);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___m_Content_3 = ((GameObjectU5BU5D_t192*)SZArrayNew(GameObjectU5BU5D_t192_il2cpp_TypeInfo_var, 0));
		__this->___m_MonoBehaviours_4 = ((MonoBehaviourU5BU5D_t193*)SZArrayNew(MonoBehaviourU5BU5D_t193_il2cpp_TypeInfo_var, 0));
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Utility.PlatformSpecificContent::OnEnable()
extern "C" void PlatformSpecificContent_OnEnable_m527 (PlatformSpecificContent_t194 * __this, const MethodInfo* method)
{
	{
		PlatformSpecificContent_CheckEnableContent_m528(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Utility.PlatformSpecificContent::CheckEnableContent()
extern "C" void PlatformSpecificContent_CheckEnableContent_m528 (PlatformSpecificContent_t194 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_BuildTargetGroup_2);
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0018;
		}
	}
	{
		PlatformSpecificContent_EnableContent_m529(__this, 1, /*hidden argument*/NULL);
		goto IL_001f;
	}

IL_0018:
	{
		PlatformSpecificContent_EnableContent_m529(__this, 0, /*hidden argument*/NULL);
	}

IL_001f:
	{
		return;
	}
}
// System.Void UnityStandardAssets.Utility.PlatformSpecificContent::EnableContent(System.Boolean)
extern TypeInfo* IEnumerator_t217_il2cpp_TypeInfo_var;
extern TypeInfo* Transform_t1_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t233_il2cpp_TypeInfo_var;
extern "C" void PlatformSpecificContent_EnableContent_m529 (PlatformSpecificContent_t194 * __this, bool ___enabled, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IEnumerator_t217_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(27);
		Transform_t1_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(28);
		IDisposable_t233_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t78 * V_0 = {0};
	GameObjectU5BU5D_t192* V_1 = {0};
	int32_t V_2 = 0;
	Transform_t1 * V_3 = {0};
	Object_t * V_4 = {0};
	MonoBehaviour_t3 * V_5 = {0};
	MonoBehaviourU5BU5D_t193* V_6 = {0};
	int32_t V_7 = 0;
	Object_t * V_8 = {0};
	Exception_t232 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t232 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		GameObjectU5BU5D_t192* L_0 = (__this->___m_Content_3);
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)(((Array_t *)L_0)->max_length)))) <= ((int32_t)0)))
		{
			goto IL_0040;
		}
	}
	{
		GameObjectU5BU5D_t192* L_1 = (__this->___m_Content_3);
		V_1 = L_1;
		V_2 = 0;
		goto IL_0037;
	}

IL_001c:
	{
		GameObjectU5BU5D_t192* L_2 = V_1;
		int32_t L_3 = V_2;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		V_0 = (*(GameObject_t78 **)(GameObject_t78 **)SZArrayLdElema(L_2, L_4));
		GameObject_t78 * L_5 = V_0;
		bool L_6 = Object_op_Inequality_m623(NULL /*static, unused*/, L_5, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0033;
		}
	}
	{
		GameObject_t78 * L_7 = V_0;
		bool L_8 = ___enabled;
		NullCheck(L_7);
		GameObject_SetActive_m713(L_7, L_8, /*hidden argument*/NULL);
	}

IL_0033:
	{
		int32_t L_9 = V_2;
		V_2 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0037:
	{
		int32_t L_10 = V_2;
		GameObjectU5BU5D_t192* L_11 = V_1;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)(((Array_t *)L_11)->max_length))))))
		{
			goto IL_001c;
		}
	}

IL_0040:
	{
		bool L_12 = (__this->___m_ChildrenOfThisObject_5);
		if (!L_12)
		{
			goto IL_009d;
		}
	}
	{
		Transform_t1 * L_13 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_13);
		Object_t * L_14 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(4 /* System.Collections.IEnumerator UnityEngine.Transform::GetEnumerator() */, L_13);
		V_4 = L_14;
	}

IL_0058:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0076;
		}

IL_005d:
		{
			Object_t * L_15 = V_4;
			NullCheck(L_15);
			Object_t * L_16 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t217_il2cpp_TypeInfo_var, L_15);
			V_3 = ((Transform_t1 *)Castclass(L_16, Transform_t1_il2cpp_TypeInfo_var));
			Transform_t1 * L_17 = V_3;
			NullCheck(L_17);
			GameObject_t78 * L_18 = Component_get_gameObject_m622(L_17, /*hidden argument*/NULL);
			bool L_19 = ___enabled;
			NullCheck(L_18);
			GameObject_SetActive_m713(L_18, L_19, /*hidden argument*/NULL);
		}

IL_0076:
		{
			Object_t * L_20 = V_4;
			NullCheck(L_20);
			bool L_21 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t217_il2cpp_TypeInfo_var, L_20);
			if (L_21)
			{
				goto IL_005d;
			}
		}

IL_0082:
		{
			IL2CPP_LEAVE(0x9D, FINALLY_0087);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t232 *)e.ex;
		goto FINALLY_0087;
	}

FINALLY_0087:
	{ // begin finally (depth: 1)
		{
			Object_t * L_22 = V_4;
			V_8 = ((Object_t *)IsInst(L_22, IDisposable_t233_il2cpp_TypeInfo_var));
			Object_t * L_23 = V_8;
			if (L_23)
			{
				goto IL_0095;
			}
		}

IL_0094:
		{
			IL2CPP_END_FINALLY(135)
		}

IL_0095:
		{
			Object_t * L_24 = V_8;
			NullCheck(L_24);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t233_il2cpp_TypeInfo_var, L_24);
			IL2CPP_END_FINALLY(135)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(135)
	{
		IL2CPP_JUMP_TBL(0x9D, IL_009d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t232 *)
	}

IL_009d:
	{
		MonoBehaviourU5BU5D_t193* L_25 = (__this->___m_MonoBehaviours_4);
		NullCheck(L_25);
		if ((((int32_t)(((int32_t)(((Array_t *)L_25)->max_length)))) <= ((int32_t)0)))
		{
			goto IL_00db;
		}
	}
	{
		MonoBehaviourU5BU5D_t193* L_26 = (__this->___m_MonoBehaviours_4);
		V_6 = L_26;
		V_7 = 0;
		goto IL_00d0;
	}

IL_00bb:
	{
		MonoBehaviourU5BU5D_t193* L_27 = V_6;
		int32_t L_28 = V_7;
		NullCheck(L_27);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_27, L_28);
		int32_t L_29 = L_28;
		V_5 = (*(MonoBehaviour_t3 **)(MonoBehaviour_t3 **)SZArrayLdElema(L_27, L_29));
		MonoBehaviour_t3 * L_30 = V_5;
		bool L_31 = ___enabled;
		NullCheck(L_30);
		Behaviour_set_enabled_m766(L_30, L_31, /*hidden argument*/NULL);
		int32_t L_32 = V_7;
		V_7 = ((int32_t)((int32_t)L_32+(int32_t)1));
	}

IL_00d0:
	{
		int32_t L_33 = V_7;
		MonoBehaviourU5BU5D_t193* L_34 = V_6;
		NullCheck(L_34);
		if ((((int32_t)L_33) < ((int32_t)(((int32_t)(((Array_t *)L_34)->max_length))))))
		{
			goto IL_00bb;
		}
	}

IL_00db:
	{
		return;
	}
}
// UnityStandardAssets.Utility.SimpleActivatorMenu
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Si.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.Utility.SimpleActivatorMenu
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_SiMethodDeclarations.h"



// System.Void UnityStandardAssets.Utility.SimpleActivatorMenu::.ctor()
extern "C" void SimpleActivatorMenu__ctor_m530 (SimpleActivatorMenu_t195 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Utility.SimpleActivatorMenu::OnEnable()
extern "C" void SimpleActivatorMenu_OnEnable_m531 (SimpleActivatorMenu_t195 * __this, const MethodInfo* method)
{
	{
		__this->___m_CurrentActiveObject_4 = 0;
		GUIText_t181 * L_0 = (__this->___camSwitchButton_2);
		GameObjectU5BU5D_t192* L_1 = (__this->___objects_3);
		int32_t L_2 = (__this->___m_CurrentActiveObject_4);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		NullCheck((*(GameObject_t78 **)(GameObject_t78 **)SZArrayLdElema(L_1, L_3)));
		String_t* L_4 = Object_get_name_m798((*(GameObject_t78 **)(GameObject_t78 **)SZArrayLdElema(L_1, L_3)), /*hidden argument*/NULL);
		NullCheck(L_0);
		GUIText_set_text_m998(L_0, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Utility.SimpleActivatorMenu::NextCamera()
extern "C" void SimpleActivatorMenu_NextCamera_m532 (SimpleActivatorMenu_t195 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = (__this->___m_CurrentActiveObject_4);
		GameObjectU5BU5D_t192* L_1 = (__this->___objects_3);
		NullCheck(L_1);
		if ((((int32_t)((int32_t)((int32_t)L_0+(int32_t)1))) < ((int32_t)(((int32_t)(((Array_t *)L_1)->max_length))))))
		{
			goto IL_001b;
		}
	}
	{
		G_B3_0 = 0;
		goto IL_0023;
	}

IL_001b:
	{
		int32_t L_2 = (__this->___m_CurrentActiveObject_4);
		G_B3_0 = ((int32_t)((int32_t)L_2+(int32_t)1));
	}

IL_0023:
	{
		V_0 = G_B3_0;
		V_1 = 0;
		goto IL_0040;
	}

IL_002b:
	{
		GameObjectU5BU5D_t192* L_3 = (__this->___objects_3);
		int32_t L_4 = V_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		int32_t L_6 = V_1;
		int32_t L_7 = V_0;
		NullCheck((*(GameObject_t78 **)(GameObject_t78 **)SZArrayLdElema(L_3, L_5)));
		GameObject_SetActive_m713((*(GameObject_t78 **)(GameObject_t78 **)SZArrayLdElema(L_3, L_5)), ((((int32_t)L_6) == ((int32_t)L_7))? 1 : 0), /*hidden argument*/NULL);
		int32_t L_8 = V_1;
		V_1 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0040:
	{
		int32_t L_9 = V_1;
		GameObjectU5BU5D_t192* L_10 = (__this->___objects_3);
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)(((Array_t *)L_10)->max_length))))))
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_11 = V_0;
		__this->___m_CurrentActiveObject_4 = L_11;
		GUIText_t181 * L_12 = (__this->___camSwitchButton_2);
		GameObjectU5BU5D_t192* L_13 = (__this->___objects_3);
		int32_t L_14 = (__this->___m_CurrentActiveObject_4);
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		int32_t L_15 = L_14;
		NullCheck((*(GameObject_t78 **)(GameObject_t78 **)SZArrayLdElema(L_13, L_15)));
		String_t* L_16 = Object_get_name_m798((*(GameObject_t78 **)(GameObject_t78 **)SZArrayLdElema(L_13, L_15)), /*hidden argument*/NULL);
		NullCheck(L_12);
		GUIText_set_text_m998(L_12, L_16, /*hidden argument*/NULL);
		return;
	}
}
// UnityStandardAssets.Utility.SimpleMouseRotator
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Si_0.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.Utility.SimpleMouseRotator
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Si_0MethodDeclarations.h"

// UnityEngine.Screen
#include "UnityEngine_UnityEngine_ScreenMethodDeclarations.h"


// System.Void UnityStandardAssets.Utility.SimpleMouseRotator::.ctor()
extern "C" void SimpleMouseRotator__ctor_m533 (SimpleMouseRotator_t196 * __this, const MethodInfo* method)
{
	{
		Vector3_t4  L_0 = {0};
		Vector3__ctor_m673(&L_0, (70.0f), (70.0f), /*hidden argument*/NULL);
		Vector2_t6  L_1 = Vector2_op_Implicit_m619(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->___rotationRange_2 = L_1;
		__this->___rotationSpeed_3 = (10.0f);
		__this->___dampingTime_4 = (0.2f);
		__this->___autoZeroVerticalOnMobile_5 = 1;
		__this->___relative_7 = 1;
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Utility.SimpleMouseRotator::Start()
extern "C" void SimpleMouseRotator_Start_m534 (SimpleMouseRotator_t196 * __this, const MethodInfo* method)
{
	{
		Transform_t1 * L_0 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Quaternion_t19  L_1 = Transform_get_localRotation_m662(L_0, /*hidden argument*/NULL);
		__this->___m_OriginalRotation_11 = L_1;
		return;
	}
}
// System.Void UnityStandardAssets.Utility.SimpleMouseRotator::Update()
extern TypeInfo* CrossPlatformInputManager_t35_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t218_il2cpp_TypeInfo_var;
extern TypeInfo* Input_t220_il2cpp_TypeInfo_var;
extern "C" void SimpleMouseRotator_Update_m535 (SimpleMouseRotator_t196 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		Mathf_t218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		Input_t220_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	Vector3_t4  V_2 = {0};
	Vector3_t4  V_3 = {0};
	{
		Transform_t1 * L_0 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		Quaternion_t19  L_1 = (__this->___m_OriginalRotation_11);
		NullCheck(L_0);
		Transform_set_localRotation_m667(L_0, L_1, /*hidden argument*/NULL);
		bool L_2 = (__this->___relative_7);
		if (!L_2)
		{
			goto IL_0293;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t35_il2cpp_TypeInfo_var);
		float L_3 = CrossPlatformInputManager_GetAxis_m100(NULL /*static, unused*/, (String_t*) &_stringLiteral10, /*hidden argument*/NULL);
		V_0 = L_3;
		float L_4 = CrossPlatformInputManager_GetAxis_m100(NULL /*static, unused*/, (String_t*) &_stringLiteral11, /*hidden argument*/NULL);
		V_1 = L_4;
		Vector3_t4 * L_5 = &(__this->___m_TargetAngles_8);
		float L_6 = (L_5->___y_2);
		if ((!(((float)L_6) > ((float)(180.0f)))))
		{
			goto IL_0075;
		}
	}
	{
		Vector3_t4 * L_7 = &(__this->___m_TargetAngles_8);
		Vector3_t4 * L_8 = L_7;
		float L_9 = (L_8->___y_2);
		L_8->___y_2 = ((float)((float)L_9-(float)(360.0f)));
		Vector3_t4 * L_10 = &(__this->___m_FollowAngles_9);
		Vector3_t4 * L_11 = L_10;
		float L_12 = (L_11->___y_2);
		L_11->___y_2 = ((float)((float)L_12-(float)(360.0f)));
	}

IL_0075:
	{
		Vector3_t4 * L_13 = &(__this->___m_TargetAngles_8);
		float L_14 = (L_13->___x_1);
		if ((!(((float)L_14) > ((float)(180.0f)))))
		{
			goto IL_00b8;
		}
	}
	{
		Vector3_t4 * L_15 = &(__this->___m_TargetAngles_8);
		Vector3_t4 * L_16 = L_15;
		float L_17 = (L_16->___x_1);
		L_16->___x_1 = ((float)((float)L_17-(float)(360.0f)));
		Vector3_t4 * L_18 = &(__this->___m_FollowAngles_9);
		Vector3_t4 * L_19 = L_18;
		float L_20 = (L_19->___x_1);
		L_19->___x_1 = ((float)((float)L_20-(float)(360.0f)));
	}

IL_00b8:
	{
		Vector3_t4 * L_21 = &(__this->___m_TargetAngles_8);
		float L_22 = (L_21->___y_2);
		if ((!(((float)L_22) < ((float)(-180.0f)))))
		{
			goto IL_00fb;
		}
	}
	{
		Vector3_t4 * L_23 = &(__this->___m_TargetAngles_8);
		Vector3_t4 * L_24 = L_23;
		float L_25 = (L_24->___y_2);
		L_24->___y_2 = ((float)((float)L_25+(float)(360.0f)));
		Vector3_t4 * L_26 = &(__this->___m_FollowAngles_9);
		Vector3_t4 * L_27 = L_26;
		float L_28 = (L_27->___y_2);
		L_27->___y_2 = ((float)((float)L_28+(float)(360.0f)));
	}

IL_00fb:
	{
		Vector3_t4 * L_29 = &(__this->___m_TargetAngles_8);
		float L_30 = (L_29->___x_1);
		if ((!(((float)L_30) < ((float)(-180.0f)))))
		{
			goto IL_013e;
		}
	}
	{
		Vector3_t4 * L_31 = &(__this->___m_TargetAngles_8);
		Vector3_t4 * L_32 = L_31;
		float L_33 = (L_32->___x_1);
		L_32->___x_1 = ((float)((float)L_33+(float)(360.0f)));
		Vector3_t4 * L_34 = &(__this->___m_FollowAngles_9);
		Vector3_t4 * L_35 = L_34;
		float L_36 = (L_35->___x_1);
		L_35->___x_1 = ((float)((float)L_36+(float)(360.0f)));
	}

IL_013e:
	{
		bool L_37 = (__this->___autoZeroHorizontalOnMobile_6);
		if (!L_37)
		{
			goto IL_018e;
		}
	}
	{
		Vector3_t4 * L_38 = &(__this->___m_TargetAngles_8);
		Vector2_t6 * L_39 = &(__this->___rotationRange_2);
		float L_40 = (L_39->___y_2);
		Vector2_t6 * L_41 = &(__this->___rotationRange_2);
		float L_42 = (L_41->___y_2);
		float L_43 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_44 = Mathf_Lerp_m610(NULL /*static, unused*/, ((float)((float)((-L_40))*(float)(0.5f))), ((float)((float)L_42*(float)(0.5f))), ((float)((float)((float)((float)L_43*(float)(0.5f)))+(float)(0.5f))), /*hidden argument*/NULL);
		L_38->___y_2 = L_44;
		goto IL_01a8;
	}

IL_018e:
	{
		Vector3_t4 * L_45 = &(__this->___m_TargetAngles_8);
		Vector3_t4 * L_46 = L_45;
		float L_47 = (L_46->___y_2);
		float L_48 = V_0;
		float L_49 = (__this->___rotationSpeed_3);
		L_46->___y_2 = ((float)((float)L_47+(float)((float)((float)L_48*(float)L_49))));
	}

IL_01a8:
	{
		bool L_50 = (__this->___autoZeroVerticalOnMobile_5);
		if (!L_50)
		{
			goto IL_01f8;
		}
	}
	{
		Vector3_t4 * L_51 = &(__this->___m_TargetAngles_8);
		Vector2_t6 * L_52 = &(__this->___rotationRange_2);
		float L_53 = (L_52->___x_1);
		Vector2_t6 * L_54 = &(__this->___rotationRange_2);
		float L_55 = (L_54->___x_1);
		float L_56 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_57 = Mathf_Lerp_m610(NULL /*static, unused*/, ((float)((float)((-L_53))*(float)(0.5f))), ((float)((float)L_55*(float)(0.5f))), ((float)((float)((float)((float)L_56*(float)(0.5f)))+(float)(0.5f))), /*hidden argument*/NULL);
		L_51->___x_1 = L_57;
		goto IL_0212;
	}

IL_01f8:
	{
		Vector3_t4 * L_58 = &(__this->___m_TargetAngles_8);
		Vector3_t4 * L_59 = L_58;
		float L_60 = (L_59->___x_1);
		float L_61 = V_1;
		float L_62 = (__this->___rotationSpeed_3);
		L_59->___x_1 = ((float)((float)L_60+(float)((float)((float)L_61*(float)L_62))));
	}

IL_0212:
	{
		Vector3_t4 * L_63 = &(__this->___m_TargetAngles_8);
		Vector3_t4 * L_64 = &(__this->___m_TargetAngles_8);
		float L_65 = (L_64->___y_2);
		Vector2_t6 * L_66 = &(__this->___rotationRange_2);
		float L_67 = (L_66->___y_2);
		Vector2_t6 * L_68 = &(__this->___rotationRange_2);
		float L_69 = (L_68->___y_2);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_70 = Mathf_Clamp_m611(NULL /*static, unused*/, L_65, ((float)((float)((-L_67))*(float)(0.5f))), ((float)((float)L_69*(float)(0.5f))), /*hidden argument*/NULL);
		L_63->___y_2 = L_70;
		Vector3_t4 * L_71 = &(__this->___m_TargetAngles_8);
		Vector3_t4 * L_72 = &(__this->___m_TargetAngles_8);
		float L_73 = (L_72->___x_1);
		Vector2_t6 * L_74 = &(__this->___rotationRange_2);
		float L_75 = (L_74->___x_1);
		Vector2_t6 * L_76 = &(__this->___rotationRange_2);
		float L_77 = (L_76->___x_1);
		float L_78 = Mathf_Clamp_m611(NULL /*static, unused*/, L_73, ((float)((float)((-L_75))*(float)(0.5f))), ((float)((float)L_77*(float)(0.5f))), /*hidden argument*/NULL);
		L_71->___x_1 = L_78;
		goto IL_0325;
	}

IL_0293:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t220_il2cpp_TypeInfo_var);
		Vector3_t4  L_79 = Input_get_mousePosition_m720(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = L_79;
		float L_80 = ((&V_2)->___x_1);
		V_0 = L_80;
		Vector3_t4  L_81 = Input_get_mousePosition_m720(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_3 = L_81;
		float L_82 = ((&V_3)->___y_2);
		V_1 = L_82;
		Vector3_t4 * L_83 = &(__this->___m_TargetAngles_8);
		Vector2_t6 * L_84 = &(__this->___rotationRange_2);
		float L_85 = (L_84->___y_2);
		Vector2_t6 * L_86 = &(__this->___rotationRange_2);
		float L_87 = (L_86->___y_2);
		float L_88 = V_0;
		int32_t L_89 = Screen_get_width_m723(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_90 = Mathf_Lerp_m610(NULL /*static, unused*/, ((float)((float)((-L_85))*(float)(0.5f))), ((float)((float)L_87*(float)(0.5f))), ((float)((float)L_88/(float)(((float)L_89)))), /*hidden argument*/NULL);
		L_83->___y_2 = L_90;
		Vector3_t4 * L_91 = &(__this->___m_TargetAngles_8);
		Vector2_t6 * L_92 = &(__this->___rotationRange_2);
		float L_93 = (L_92->___x_1);
		Vector2_t6 * L_94 = &(__this->___rotationRange_2);
		float L_95 = (L_94->___x_1);
		float L_96 = V_1;
		int32_t L_97 = Screen_get_height_m859(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_98 = Mathf_Lerp_m610(NULL /*static, unused*/, ((float)((float)((-L_93))*(float)(0.5f))), ((float)((float)L_95*(float)(0.5f))), ((float)((float)L_96/(float)(((float)L_97)))), /*hidden argument*/NULL);
		L_91->___x_1 = L_98;
	}

IL_0325:
	{
		Vector3_t4  L_99 = (__this->___m_FollowAngles_9);
		Vector3_t4  L_100 = (__this->___m_TargetAngles_8);
		Vector3_t4 * L_101 = &(__this->___m_FollowVelocity_10);
		float L_102 = (__this->___dampingTime_4);
		Vector3_t4  L_103 = Vector3_SmoothDamp_m606(NULL /*static, unused*/, L_99, L_100, L_101, L_102, /*hidden argument*/NULL);
		__this->___m_FollowAngles_9 = L_103;
		Transform_t1 * L_104 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		Quaternion_t19  L_105 = (__this->___m_OriginalRotation_11);
		Vector3_t4 * L_106 = &(__this->___m_FollowAngles_9);
		float L_107 = (L_106->___x_1);
		Vector3_t4 * L_108 = &(__this->___m_FollowAngles_9);
		float L_109 = (L_108->___y_2);
		Quaternion_t19  L_110 = Quaternion_Euler_m665(NULL /*static, unused*/, ((-L_107)), L_109, (0.0f), /*hidden argument*/NULL);
		Quaternion_t19  L_111 = Quaternion_op_Multiply_m672(NULL /*static, unused*/, L_105, L_110, /*hidden argument*/NULL);
		NullCheck(L_104);
		Transform_set_localRotation_m667(L_104, L_111, /*hidden argument*/NULL);
		return;
	}
}
// UnityStandardAssets.Utility.SmoothFollow
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Sm.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.Utility.SmoothFollow
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_SmMethodDeclarations.h"



// System.Void UnityStandardAssets.Utility.SmoothFollow::.ctor()
extern "C" void SmoothFollow__ctor_m536 (SmoothFollow_t197 * __this, const MethodInfo* method)
{
	{
		__this->___distance_3 = (10.0f);
		__this->___height_4 = (5.0f);
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Utility.SmoothFollow::Start()
extern "C" void SmoothFollow_Start_m537 (SmoothFollow_t197 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityStandardAssets.Utility.SmoothFollow::LateUpdate()
extern TypeInfo* Mathf_t218_il2cpp_TypeInfo_var;
extern "C" void SmoothFollow_LateUpdate_m538 (SmoothFollow_t197 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	Quaternion_t19  V_4 = {0};
	Vector3_t4  V_5 = {0};
	Vector3_t4  V_6 = {0};
	Vector3_t4  V_7 = {0};
	Vector3_t4  V_8 = {0};
	Vector3_t4  V_9 = {0};
	Vector3_t4  V_10 = {0};
	{
		Transform_t1 * L_0 = (__this->___target_2);
		bool L_1 = Object_op_Implicit_m629(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		Transform_t1 * L_2 = (__this->___target_2);
		NullCheck(L_2);
		Vector3_t4  L_3 = Transform_get_eulerAngles_m1009(L_2, /*hidden argument*/NULL);
		V_5 = L_3;
		float L_4 = ((&V_5)->___y_2);
		V_0 = L_4;
		Transform_t1 * L_5 = (__this->___target_2);
		NullCheck(L_5);
		Vector3_t4  L_6 = Transform_get_position_m593(L_5, /*hidden argument*/NULL);
		V_6 = L_6;
		float L_7 = ((&V_6)->___y_2);
		float L_8 = (__this->___height_4);
		V_1 = ((float)((float)L_7+(float)L_8));
		Transform_t1 * L_9 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		Vector3_t4  L_10 = Transform_get_eulerAngles_m1009(L_9, /*hidden argument*/NULL);
		V_7 = L_10;
		float L_11 = ((&V_7)->___y_2);
		V_2 = L_11;
		Transform_t1 * L_12 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t4  L_13 = Transform_get_position_m593(L_12, /*hidden argument*/NULL);
		V_8 = L_13;
		float L_14 = ((&V_8)->___y_2);
		V_3 = L_14;
		float L_15 = V_2;
		float L_16 = V_0;
		float L_17 = (__this->___rotationDamping_5);
		float L_18 = Time_get_deltaTime_m602(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_19 = Mathf_LerpAngle_m1010(NULL /*static, unused*/, L_15, L_16, ((float)((float)L_17*(float)L_18)), /*hidden argument*/NULL);
		V_2 = L_19;
		float L_20 = V_3;
		float L_21 = V_1;
		float L_22 = (__this->___heightDamping_6);
		float L_23 = Time_get_deltaTime_m602(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_24 = Mathf_Lerp_m610(NULL /*static, unused*/, L_20, L_21, ((float)((float)L_22*(float)L_23)), /*hidden argument*/NULL);
		V_3 = L_24;
		float L_25 = V_2;
		Quaternion_t19  L_26 = Quaternion_Euler_m665(NULL /*static, unused*/, (0.0f), L_25, (0.0f), /*hidden argument*/NULL);
		V_4 = L_26;
		Transform_t1 * L_27 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		Transform_t1 * L_28 = (__this->___target_2);
		NullCheck(L_28);
		Vector3_t4  L_29 = Transform_get_position_m593(L_28, /*hidden argument*/NULL);
		NullCheck(L_27);
		Transform_set_position_m607(L_27, L_29, /*hidden argument*/NULL);
		Transform_t1 * L_30 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		Transform_t1 * L_31 = L_30;
		NullCheck(L_31);
		Vector3_t4  L_32 = Transform_get_position_m593(L_31, /*hidden argument*/NULL);
		Quaternion_t19  L_33 = V_4;
		Vector3_t4  L_34 = Vector3_get_forward_m605(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4  L_35 = Quaternion_op_Multiply_m891(NULL /*static, unused*/, L_33, L_34, /*hidden argument*/NULL);
		float L_36 = (__this->___distance_3);
		Vector3_t4  L_37 = Vector3_op_Multiply_m600(NULL /*static, unused*/, L_35, L_36, /*hidden argument*/NULL);
		Vector3_t4  L_38 = Vector3_op_Subtraction_m595(NULL /*static, unused*/, L_32, L_37, /*hidden argument*/NULL);
		NullCheck(L_31);
		Transform_set_position_m607(L_31, L_38, /*hidden argument*/NULL);
		Transform_t1 * L_39 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		Transform_t1 * L_40 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_40);
		Vector3_t4  L_41 = Transform_get_position_m593(L_40, /*hidden argument*/NULL);
		V_9 = L_41;
		float L_42 = ((&V_9)->___x_1);
		float L_43 = V_3;
		Transform_t1 * L_44 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_44);
		Vector3_t4  L_45 = Transform_get_position_m593(L_44, /*hidden argument*/NULL);
		V_10 = L_45;
		float L_46 = ((&V_10)->___z_3);
		Vector3_t4  L_47 = {0};
		Vector3__ctor_m612(&L_47, L_42, L_43, L_46, /*hidden argument*/NULL);
		NullCheck(L_39);
		Transform_set_position_m607(L_39, L_47, /*hidden argument*/NULL);
		Transform_t1 * L_48 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		Transform_t1 * L_49 = (__this->___target_2);
		NullCheck(L_48);
		Transform_LookAt_m1011(L_48, L_49, /*hidden argument*/NULL);
		return;
	}
}
// UnityStandardAssets.Utility.TimedObjectActivator/Action
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Ti.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.Utility.TimedObjectActivator/Action
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_TiMethodDeclarations.h"



// UnityStandardAssets.Utility.TimedObjectActivator/Entry
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Ti_0.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.Utility.TimedObjectActivator/Entry
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Ti_0MethodDeclarations.h"



// System.Void UnityStandardAssets.Utility.TimedObjectActivator/Entry::.ctor()
extern "C" void Entry__ctor_m539 (Entry_t199 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m677(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityStandardAssets.Utility.TimedObjectActivator/Entries
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Ti_1.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.Utility.TimedObjectActivator/Entries
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Ti_1MethodDeclarations.h"



// System.Void UnityStandardAssets.Utility.TimedObjectActivator/Entries::.ctor()
extern "C" void Entries__ctor_m540 (Entries_t201 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m677(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityStandardAssets.Utility.TimedObjectActivator/<Activate>c__Iterator9
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Ti_2.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.Utility.TimedObjectActivator/<Activate>c__Iterator9
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Ti_2MethodDeclarations.h"



// System.Void UnityStandardAssets.Utility.TimedObjectActivator/<Activate>c__Iterator9::.ctor()
extern "C" void U3CActivateU3Ec__Iterator9__ctor_m541 (U3CActivateU3Ec__Iterator9_t202 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m677(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object UnityStandardAssets.Utility.TimedObjectActivator/<Activate>c__Iterator9::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CActivateU3Ec__Iterator9_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m542 (U3CActivateU3Ec__Iterator9_t202 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_2);
		return L_0;
	}
}
// System.Object UnityStandardAssets.Utility.TimedObjectActivator/<Activate>c__Iterator9::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CActivateU3Ec__Iterator9_System_Collections_IEnumerator_get_Current_m543 (U3CActivateU3Ec__Iterator9_t202 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_2);
		return L_0;
	}
}
// System.Boolean UnityStandardAssets.Utility.TimedObjectActivator/<Activate>c__Iterator9::MoveNext()
extern TypeInfo* WaitForSeconds_t257_il2cpp_TypeInfo_var;
extern "C" bool U3CActivateU3Ec__Iterator9_MoveNext_m544 (U3CActivateU3Ec__Iterator9_t202 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WaitForSeconds_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(94);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_1);
		V_0 = L_0;
		__this->___U24PC_1 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0043;
		}
	}
	{
		goto IL_005b;
	}

IL_0021:
	{
		Entry_t199 * L_2 = (__this->___entry_0);
		NullCheck(L_2);
		float L_3 = (L_2->___delay_2);
		WaitForSeconds_t257 * L_4 = (WaitForSeconds_t257 *)il2cpp_codegen_object_new (WaitForSeconds_t257_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1002(L_4, L_3, /*hidden argument*/NULL);
		__this->___U24current_2 = L_4;
		__this->___U24PC_1 = 1;
		goto IL_005d;
	}

IL_0043:
	{
		Entry_t199 * L_5 = (__this->___entry_0);
		NullCheck(L_5);
		GameObject_t78 * L_6 = (L_5->___target_0);
		NullCheck(L_6);
		GameObject_SetActive_m713(L_6, 1, /*hidden argument*/NULL);
		__this->___U24PC_1 = (-1);
	}

IL_005b:
	{
		return 0;
	}

IL_005d:
	{
		return 1;
	}
	// Dead block : IL_005f: ldloc.1
}
// System.Void UnityStandardAssets.Utility.TimedObjectActivator/<Activate>c__Iterator9::Dispose()
extern "C" void U3CActivateU3Ec__Iterator9_Dispose_m545 (U3CActivateU3Ec__Iterator9_t202 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_1 = (-1);
		return;
	}
}
// System.Void UnityStandardAssets.Utility.TimedObjectActivator/<Activate>c__Iterator9::Reset()
extern TypeInfo* NotSupportedException_t244_il2cpp_TypeInfo_var;
extern "C" void U3CActivateU3Ec__Iterator9_Reset_m546 (U3CActivateU3Ec__Iterator9_t202 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t244_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t244 * L_0 = (NotSupportedException_t244 *)il2cpp_codegen_object_new (NotSupportedException_t244_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m905(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// UnityStandardAssets.Utility.TimedObjectActivator/<Deactivate>c__IteratorA
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Ti_3.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.Utility.TimedObjectActivator/<Deactivate>c__IteratorA
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Ti_3MethodDeclarations.h"



// System.Void UnityStandardAssets.Utility.TimedObjectActivator/<Deactivate>c__IteratorA::.ctor()
extern "C" void U3CDeactivateU3Ec__IteratorA__ctor_m547 (U3CDeactivateU3Ec__IteratorA_t203 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m677(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object UnityStandardAssets.Utility.TimedObjectActivator/<Deactivate>c__IteratorA::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CDeactivateU3Ec__IteratorA_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m548 (U3CDeactivateU3Ec__IteratorA_t203 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_2);
		return L_0;
	}
}
// System.Object UnityStandardAssets.Utility.TimedObjectActivator/<Deactivate>c__IteratorA::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CDeactivateU3Ec__IteratorA_System_Collections_IEnumerator_get_Current_m549 (U3CDeactivateU3Ec__IteratorA_t203 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_2);
		return L_0;
	}
}
// System.Boolean UnityStandardAssets.Utility.TimedObjectActivator/<Deactivate>c__IteratorA::MoveNext()
extern TypeInfo* WaitForSeconds_t257_il2cpp_TypeInfo_var;
extern "C" bool U3CDeactivateU3Ec__IteratorA_MoveNext_m550 (U3CDeactivateU3Ec__IteratorA_t203 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WaitForSeconds_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(94);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_1);
		V_0 = L_0;
		__this->___U24PC_1 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0043;
		}
	}
	{
		goto IL_005b;
	}

IL_0021:
	{
		Entry_t199 * L_2 = (__this->___entry_0);
		NullCheck(L_2);
		float L_3 = (L_2->___delay_2);
		WaitForSeconds_t257 * L_4 = (WaitForSeconds_t257 *)il2cpp_codegen_object_new (WaitForSeconds_t257_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1002(L_4, L_3, /*hidden argument*/NULL);
		__this->___U24current_2 = L_4;
		__this->___U24PC_1 = 1;
		goto IL_005d;
	}

IL_0043:
	{
		Entry_t199 * L_5 = (__this->___entry_0);
		NullCheck(L_5);
		GameObject_t78 * L_6 = (L_5->___target_0);
		NullCheck(L_6);
		GameObject_SetActive_m713(L_6, 0, /*hidden argument*/NULL);
		__this->___U24PC_1 = (-1);
	}

IL_005b:
	{
		return 0;
	}

IL_005d:
	{
		return 1;
	}
	// Dead block : IL_005f: ldloc.1
}
// System.Void UnityStandardAssets.Utility.TimedObjectActivator/<Deactivate>c__IteratorA::Dispose()
extern "C" void U3CDeactivateU3Ec__IteratorA_Dispose_m551 (U3CDeactivateU3Ec__IteratorA_t203 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_1 = (-1);
		return;
	}
}
// System.Void UnityStandardAssets.Utility.TimedObjectActivator/<Deactivate>c__IteratorA::Reset()
extern TypeInfo* NotSupportedException_t244_il2cpp_TypeInfo_var;
extern "C" void U3CDeactivateU3Ec__IteratorA_Reset_m552 (U3CDeactivateU3Ec__IteratorA_t203 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t244_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t244 * L_0 = (NotSupportedException_t244 *)il2cpp_codegen_object_new (NotSupportedException_t244_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m905(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>c__IteratorB
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Ti_4.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>c__IteratorB
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Ti_4MethodDeclarations.h"



// System.Void UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>c__IteratorB::.ctor()
extern "C" void U3CReloadLevelU3Ec__IteratorB__ctor_m553 (U3CReloadLevelU3Ec__IteratorB_t204 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m677(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>c__IteratorB::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CReloadLevelU3Ec__IteratorB_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m554 (U3CReloadLevelU3Ec__IteratorB_t204 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_2);
		return L_0;
	}
}
// System.Object UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>c__IteratorB::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CReloadLevelU3Ec__IteratorB_System_Collections_IEnumerator_get_Current_m555 (U3CReloadLevelU3Ec__IteratorB_t204 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_2);
		return L_0;
	}
}
// System.Boolean UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>c__IteratorB::MoveNext()
extern TypeInfo* WaitForSeconds_t257_il2cpp_TypeInfo_var;
extern "C" bool U3CReloadLevelU3Ec__IteratorB_MoveNext_m556 (U3CReloadLevelU3Ec__IteratorB_t204 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WaitForSeconds_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(94);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_1);
		V_0 = L_0;
		__this->___U24PC_1 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0043;
		}
	}
	{
		goto IL_0054;
	}

IL_0021:
	{
		Entry_t199 * L_2 = (__this->___entry_0);
		NullCheck(L_2);
		float L_3 = (L_2->___delay_2);
		WaitForSeconds_t257 * L_4 = (WaitForSeconds_t257 *)il2cpp_codegen_object_new (WaitForSeconds_t257_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1002(L_4, L_3, /*hidden argument*/NULL);
		__this->___U24current_2 = L_4;
		__this->___U24PC_1 = 1;
		goto IL_0056;
	}

IL_0043:
	{
		int32_t L_5 = Application_get_loadedLevel_m1012(NULL /*static, unused*/, /*hidden argument*/NULL);
		Application_LoadLevel_m1013(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		__this->___U24PC_1 = (-1);
	}

IL_0054:
	{
		return 0;
	}

IL_0056:
	{
		return 1;
	}
	// Dead block : IL_0058: ldloc.1
}
// System.Void UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>c__IteratorB::Dispose()
extern "C" void U3CReloadLevelU3Ec__IteratorB_Dispose_m557 (U3CReloadLevelU3Ec__IteratorB_t204 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_1 = (-1);
		return;
	}
}
// System.Void UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>c__IteratorB::Reset()
extern TypeInfo* NotSupportedException_t244_il2cpp_TypeInfo_var;
extern "C" void U3CReloadLevelU3Ec__IteratorB_Reset_m558 (U3CReloadLevelU3Ec__IteratorB_t204 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t244_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t244 * L_0 = (NotSupportedException_t244 *)il2cpp_codegen_object_new (NotSupportedException_t244_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m905(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// UnityStandardAssets.Utility.TimedObjectActivator
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Ti_5.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.Utility.TimedObjectActivator
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Ti_5MethodDeclarations.h"



// System.Void UnityStandardAssets.Utility.TimedObjectActivator::.ctor()
extern TypeInfo* Entries_t201_il2cpp_TypeInfo_var;
extern "C" void TimedObjectActivator__ctor_m559 (TimedObjectActivator_t205 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Entries_t201_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(101);
		s_Il2CppMethodIntialized = true;
	}
	{
		Entries_t201 * L_0 = (Entries_t201 *)il2cpp_codegen_object_new (Entries_t201_il2cpp_TypeInfo_var);
		Entries__ctor_m540(L_0, /*hidden argument*/NULL);
		__this->___entries_2 = L_0;
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Utility.TimedObjectActivator::Awake()
extern "C" void TimedObjectActivator_Awake_m560 (TimedObjectActivator_t205 * __this, const MethodInfo* method)
{
	Entry_t199 * V_0 = {0};
	EntryU5BU5D_t200* V_1 = {0};
	int32_t V_2 = 0;
	int32_t V_3 = {0};
	{
		Entries_t201 * L_0 = (__this->___entries_2);
		NullCheck(L_0);
		EntryU5BU5D_t200* L_1 = (L_0->___entries_0);
		V_1 = L_1;
		V_2 = 0;
		goto IL_008c;
	}

IL_0013:
	{
		EntryU5BU5D_t200* L_2 = V_1;
		int32_t L_3 = V_2;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		V_0 = (*(Entry_t199 **)(Entry_t199 **)SZArrayLdElema(L_2, L_4));
		Entry_t199 * L_5 = V_0;
		NullCheck(L_5);
		int32_t L_6 = (L_5->___action_1);
		V_3 = L_6;
		int32_t L_7 = V_3;
		if (L_7 == 0)
		{
			goto IL_0039;
		}
		if (L_7 == 1)
		{
			goto IL_004c;
		}
		if (L_7 == 2)
		{
			goto IL_005f;
		}
		if (L_7 == 3)
		{
			goto IL_0075;
		}
	}
	{
		goto IL_0088;
	}

IL_0039:
	{
		Entry_t199 * L_8 = V_0;
		Object_t * L_9 = TimedObjectActivator_Activate_m561(__this, L_8, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m1006(__this, L_9, /*hidden argument*/NULL);
		goto IL_0088;
	}

IL_004c:
	{
		Entry_t199 * L_10 = V_0;
		Object_t * L_11 = TimedObjectActivator_Deactivate_m562(__this, L_10, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m1006(__this, L_11, /*hidden argument*/NULL);
		goto IL_0088;
	}

IL_005f:
	{
		Entry_t199 * L_12 = V_0;
		NullCheck(L_12);
		GameObject_t78 * L_13 = (L_12->___target_0);
		Entry_t199 * L_14 = V_0;
		NullCheck(L_14);
		float L_15 = (L_14->___delay_2);
		Object_Destroy_m1014(NULL /*static, unused*/, L_13, L_15, /*hidden argument*/NULL);
		goto IL_0088;
	}

IL_0075:
	{
		Entry_t199 * L_16 = V_0;
		Object_t * L_17 = TimedObjectActivator_ReloadLevel_m563(__this, L_16, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m1006(__this, L_17, /*hidden argument*/NULL);
		goto IL_0088;
	}

IL_0088:
	{
		int32_t L_18 = V_2;
		V_2 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_008c:
	{
		int32_t L_19 = V_2;
		EntryU5BU5D_t200* L_20 = V_1;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)(((Array_t *)L_20)->max_length))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Collections.IEnumerator UnityStandardAssets.Utility.TimedObjectActivator::Activate(UnityStandardAssets.Utility.TimedObjectActivator/Entry)
extern TypeInfo* U3CActivateU3Ec__Iterator9_t202_il2cpp_TypeInfo_var;
extern "C" Object_t * TimedObjectActivator_Activate_m561 (TimedObjectActivator_t205 * __this, Entry_t199 * ___entry, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CActivateU3Ec__Iterator9_t202_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(102);
		s_Il2CppMethodIntialized = true;
	}
	U3CActivateU3Ec__Iterator9_t202 * V_0 = {0};
	{
		U3CActivateU3Ec__Iterator9_t202 * L_0 = (U3CActivateU3Ec__Iterator9_t202 *)il2cpp_codegen_object_new (U3CActivateU3Ec__Iterator9_t202_il2cpp_TypeInfo_var);
		U3CActivateU3Ec__Iterator9__ctor_m541(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CActivateU3Ec__Iterator9_t202 * L_1 = V_0;
		Entry_t199 * L_2 = ___entry;
		NullCheck(L_1);
		L_1->___entry_0 = L_2;
		U3CActivateU3Ec__Iterator9_t202 * L_3 = V_0;
		Entry_t199 * L_4 = ___entry;
		NullCheck(L_3);
		L_3->___U3CU24U3Eentry_3 = L_4;
		U3CActivateU3Ec__Iterator9_t202 * L_5 = V_0;
		return L_5;
	}
}
// System.Collections.IEnumerator UnityStandardAssets.Utility.TimedObjectActivator::Deactivate(UnityStandardAssets.Utility.TimedObjectActivator/Entry)
extern TypeInfo* U3CDeactivateU3Ec__IteratorA_t203_il2cpp_TypeInfo_var;
extern "C" Object_t * TimedObjectActivator_Deactivate_m562 (TimedObjectActivator_t205 * __this, Entry_t199 * ___entry, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CDeactivateU3Ec__IteratorA_t203_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(103);
		s_Il2CppMethodIntialized = true;
	}
	U3CDeactivateU3Ec__IteratorA_t203 * V_0 = {0};
	{
		U3CDeactivateU3Ec__IteratorA_t203 * L_0 = (U3CDeactivateU3Ec__IteratorA_t203 *)il2cpp_codegen_object_new (U3CDeactivateU3Ec__IteratorA_t203_il2cpp_TypeInfo_var);
		U3CDeactivateU3Ec__IteratorA__ctor_m547(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CDeactivateU3Ec__IteratorA_t203 * L_1 = V_0;
		Entry_t199 * L_2 = ___entry;
		NullCheck(L_1);
		L_1->___entry_0 = L_2;
		U3CDeactivateU3Ec__IteratorA_t203 * L_3 = V_0;
		Entry_t199 * L_4 = ___entry;
		NullCheck(L_3);
		L_3->___U3CU24U3Eentry_3 = L_4;
		U3CDeactivateU3Ec__IteratorA_t203 * L_5 = V_0;
		return L_5;
	}
}
// System.Collections.IEnumerator UnityStandardAssets.Utility.TimedObjectActivator::ReloadLevel(UnityStandardAssets.Utility.TimedObjectActivator/Entry)
extern TypeInfo* U3CReloadLevelU3Ec__IteratorB_t204_il2cpp_TypeInfo_var;
extern "C" Object_t * TimedObjectActivator_ReloadLevel_m563 (TimedObjectActivator_t205 * __this, Entry_t199 * ___entry, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CReloadLevelU3Ec__IteratorB_t204_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(104);
		s_Il2CppMethodIntialized = true;
	}
	U3CReloadLevelU3Ec__IteratorB_t204 * V_0 = {0};
	{
		U3CReloadLevelU3Ec__IteratorB_t204 * L_0 = (U3CReloadLevelU3Ec__IteratorB_t204 *)il2cpp_codegen_object_new (U3CReloadLevelU3Ec__IteratorB_t204_il2cpp_TypeInfo_var);
		U3CReloadLevelU3Ec__IteratorB__ctor_m553(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CReloadLevelU3Ec__IteratorB_t204 * L_1 = V_0;
		Entry_t199 * L_2 = ___entry;
		NullCheck(L_1);
		L_1->___entry_0 = L_2;
		U3CReloadLevelU3Ec__IteratorB_t204 * L_3 = V_0;
		Entry_t199 * L_4 = ___entry;
		NullCheck(L_3);
		L_3->___U3CU24U3Eentry_3 = L_4;
		U3CReloadLevelU3Ec__IteratorB_t204 * L_5 = V_0;
		return L_5;
	}
}
// UnityStandardAssets.Utility.TimedObjectDestructor
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Ti_6.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.Utility.TimedObjectDestructor
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Ti_6MethodDeclarations.h"



// System.Void UnityStandardAssets.Utility.TimedObjectDestructor::.ctor()
extern "C" void TimedObjectDestructor__ctor_m564 (TimedObjectDestructor_t206 * __this, const MethodInfo* method)
{
	{
		__this->___m_TimeOut_2 = (1.0f);
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Utility.TimedObjectDestructor::Awake()
extern "C" void TimedObjectDestructor_Awake_m565 (TimedObjectDestructor_t206 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_TimeOut_2);
		MonoBehaviour_Invoke_m1015(__this, (String_t*) &_stringLiteral194, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Utility.TimedObjectDestructor::DestroyNow()
extern "C" void TimedObjectDestructor_DestroyNow_m566 (TimedObjectDestructor_t206 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_DetachChildren_3);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Transform_t1 * L_1 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_DetachChildren_m1016(L_1, /*hidden argument*/NULL);
	}

IL_0016:
	{
		GameObject_t78 * L_2 = Component_get_gameObject_m622(__this, /*hidden argument*/NULL);
		Object_DestroyObject_m948(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// UnityStandardAssets.Utility.WaypointCircuit/WaypointList
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Wa.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.Utility.WaypointCircuit/WaypointList
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_WaMethodDeclarations.h"



// System.Void UnityStandardAssets.Utility.WaypointCircuit/WaypointList::.ctor()
extern TypeInfo* TransformU5BU5D_t141_il2cpp_TypeInfo_var;
extern "C" void WaypointList__ctor_m567 (WaypointList_t208 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TransformU5BU5D_t141_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(105);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___items_1 = ((TransformU5BU5D_t141*)SZArrayNew(TransformU5BU5D_t141_il2cpp_TypeInfo_var, 0));
		Object__ctor_m677(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityStandardAssets.Utility.WaypointCircuit/RoutePoint
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Wa_0.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.Utility.WaypointCircuit/RoutePoint
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Wa_0MethodDeclarations.h"



// System.Void UnityStandardAssets.Utility.WaypointCircuit/RoutePoint::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" void RoutePoint__ctor_m568 (RoutePoint_t209 * __this, Vector3_t4  ___position, Vector3_t4  ___direction, const MethodInfo* method)
{
	{
		Vector3_t4  L_0 = ___position;
		__this->___position_0 = L_0;
		Vector3_t4  L_1 = ___direction;
		__this->___direction_1 = L_1;
		return;
	}
}
// UnityStandardAssets.Utility.WaypointCircuit
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Wa_1.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.Utility.WaypointCircuit
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Wa_1MethodDeclarations.h"



// System.Void UnityStandardAssets.Utility.WaypointCircuit::.ctor()
extern TypeInfo* WaypointList_t208_il2cpp_TypeInfo_var;
extern "C" void WaypointCircuit__ctor_m569 (WaypointCircuit_t207 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WaypointList_t208_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(106);
		s_Il2CppMethodIntialized = true;
	}
	{
		WaypointList_t208 * L_0 = (WaypointList_t208 *)il2cpp_codegen_object_new (WaypointList_t208_il2cpp_TypeInfo_var);
		WaypointList__ctor_m567(L_0, /*hidden argument*/NULL);
		__this->___waypointList_2 = L_0;
		__this->___smoothRoute_3 = 1;
		__this->___editorVisualisationSubsteps_7 = (100.0f);
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityStandardAssets.Utility.WaypointCircuit::get_Length()
extern "C" float WaypointCircuit_get_Length_m570 (WaypointCircuit_t207 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___U3CLengthU3Ek__BackingField_17);
		return L_0;
	}
}
// System.Void UnityStandardAssets.Utility.WaypointCircuit::set_Length(System.Single)
extern "C" void WaypointCircuit_set_Length_m571 (WaypointCircuit_t207 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___U3CLengthU3Ek__BackingField_17 = L_0;
		return;
	}
}
// UnityEngine.Transform[] UnityStandardAssets.Utility.WaypointCircuit::get_Waypoints()
extern "C" TransformU5BU5D_t141* WaypointCircuit_get_Waypoints_m572 (WaypointCircuit_t207 * __this, const MethodInfo* method)
{
	{
		WaypointList_t208 * L_0 = (__this->___waypointList_2);
		NullCheck(L_0);
		TransformU5BU5D_t141* L_1 = (L_0->___items_1);
		return L_1;
	}
}
// System.Void UnityStandardAssets.Utility.WaypointCircuit::Awake()
extern "C" void WaypointCircuit_Awake_m573 (WaypointCircuit_t207 * __this, const MethodInfo* method)
{
	{
		TransformU5BU5D_t141* L_0 = WaypointCircuit_get_Waypoints_m572(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)(((Array_t *)L_0)->max_length)))) <= ((int32_t)1)))
		{
			goto IL_0014;
		}
	}
	{
		WaypointCircuit_CachePositionsAndDistances_m577(__this, /*hidden argument*/NULL);
	}

IL_0014:
	{
		TransformU5BU5D_t141* L_1 = WaypointCircuit_get_Waypoints_m572(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		__this->___numPoints_4 = (((int32_t)(((Array_t *)L_1)->max_length)));
		return;
	}
}
// UnityStandardAssets.Utility.WaypointCircuit/RoutePoint UnityStandardAssets.Utility.WaypointCircuit::GetRoutePoint(System.Single)
extern "C" RoutePoint_t209  WaypointCircuit_GetRoutePoint_m574 (WaypointCircuit_t207 * __this, float ___dist, const MethodInfo* method)
{
	Vector3_t4  V_0 = {0};
	Vector3_t4  V_1 = {0};
	Vector3_t4  V_2 = {0};
	{
		float L_0 = ___dist;
		Vector3_t4  L_1 = WaypointCircuit_GetRoutePosition_m575(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = ___dist;
		Vector3_t4  L_3 = WaypointCircuit_GetRoutePosition_m575(__this, ((float)((float)L_2+(float)(0.1f))), /*hidden argument*/NULL);
		V_1 = L_3;
		Vector3_t4  L_4 = V_1;
		Vector3_t4  L_5 = V_0;
		Vector3_t4  L_6 = Vector3_op_Subtraction_m595(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		V_2 = L_6;
		Vector3_t4  L_7 = V_0;
		Vector3_t4  L_8 = Vector3_get_normalized_m648((&V_2), /*hidden argument*/NULL);
		RoutePoint_t209  L_9 = {0};
		RoutePoint__ctor_m568(&L_9, L_7, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// UnityEngine.Vector3 UnityStandardAssets.Utility.WaypointCircuit::GetRoutePosition(System.Single)
extern TypeInfo* Mathf_t218_il2cpp_TypeInfo_var;
extern "C" Vector3_t4  WaypointCircuit_GetRoutePosition_m575 (WaypointCircuit_t207 * __this, float ___dist, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		float L_0 = WaypointCircuit_get_Length_m570(__this, /*hidden argument*/NULL);
		if ((!(((float)L_0) == ((float)(0.0f)))))
		{
			goto IL_0029;
		}
	}
	{
		SingleU5BU5D_t211* L_1 = (__this->___distances_6);
		SingleU5BU5D_t211* L_2 = (__this->___distances_6);
		NullCheck(L_2);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, ((int32_t)((int32_t)(((int32_t)(((Array_t *)L_2)->max_length)))-(int32_t)1)));
		int32_t L_3 = ((int32_t)((int32_t)(((int32_t)(((Array_t *)L_2)->max_length)))-(int32_t)1));
		WaypointCircuit_set_Length_m571(__this, (*(float*)(float*)SZArrayLdElema(L_1, L_3)), /*hidden argument*/NULL);
	}

IL_0029:
	{
		float L_4 = ___dist;
		float L_5 = WaypointCircuit_get_Length_m570(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_6 = Mathf_Repeat_m1017(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		___dist = L_6;
		goto IL_0040;
	}

IL_003c:
	{
		int32_t L_7 = V_0;
		V_0 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0040:
	{
		SingleU5BU5D_t211* L_8 = (__this->___distances_6);
		int32_t L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		int32_t L_10 = L_9;
		float L_11 = ___dist;
		if ((((float)(*(float*)(float*)SZArrayLdElema(L_8, L_10))) < ((float)L_11)))
		{
			goto IL_003c;
		}
	}
	{
		int32_t L_12 = V_0;
		int32_t L_13 = (__this->___numPoints_4);
		int32_t L_14 = (__this->___numPoints_4);
		__this->___p1n_9 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_12-(int32_t)1))+(int32_t)L_13))%(int32_t)L_14));
		int32_t L_15 = V_0;
		__this->___p2n_10 = L_15;
		SingleU5BU5D_t211* L_16 = (__this->___distances_6);
		int32_t L_17 = (__this->___p1n_9);
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_17);
		int32_t L_18 = L_17;
		SingleU5BU5D_t211* L_19 = (__this->___distances_6);
		int32_t L_20 = (__this->___p2n_10);
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, L_20);
		int32_t L_21 = L_20;
		float L_22 = ___dist;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_23 = Mathf_InverseLerp_m651(NULL /*static, unused*/, (*(float*)(float*)SZArrayLdElema(L_16, L_18)), (*(float*)(float*)SZArrayLdElema(L_19, L_21)), L_22, /*hidden argument*/NULL);
		__this->___i_12 = L_23;
		bool L_24 = (__this->___smoothRoute_3);
		if (!L_24)
		{
			goto IL_016c;
		}
	}
	{
		int32_t L_25 = V_0;
		int32_t L_26 = (__this->___numPoints_4);
		int32_t L_27 = (__this->___numPoints_4);
		__this->___p0n_8 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_25-(int32_t)2))+(int32_t)L_26))%(int32_t)L_27));
		int32_t L_28 = V_0;
		int32_t L_29 = (__this->___numPoints_4);
		__this->___p3n_11 = ((int32_t)((int32_t)((int32_t)((int32_t)L_28+(int32_t)1))%(int32_t)L_29));
		int32_t L_30 = (__this->___p2n_10);
		int32_t L_31 = (__this->___numPoints_4);
		__this->___p2n_10 = ((int32_t)((int32_t)L_30%(int32_t)L_31));
		Vector3U5BU5D_t210* L_32 = (__this->___points_5);
		int32_t L_33 = (__this->___p0n_8);
		NullCheck(L_32);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_32, L_33);
		__this->___P0_13 = (*(Vector3_t4 *)((Vector3_t4 *)(Vector3_t4 *)SZArrayLdElema(L_32, L_33)));
		Vector3U5BU5D_t210* L_34 = (__this->___points_5);
		int32_t L_35 = (__this->___p1n_9);
		NullCheck(L_34);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_34, L_35);
		__this->___P1_14 = (*(Vector3_t4 *)((Vector3_t4 *)(Vector3_t4 *)SZArrayLdElema(L_34, L_35)));
		Vector3U5BU5D_t210* L_36 = (__this->___points_5);
		int32_t L_37 = (__this->___p2n_10);
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, L_37);
		__this->___P2_15 = (*(Vector3_t4 *)((Vector3_t4 *)(Vector3_t4 *)SZArrayLdElema(L_36, L_37)));
		Vector3U5BU5D_t210* L_38 = (__this->___points_5);
		int32_t L_39 = (__this->___p3n_11);
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, L_39);
		__this->___P3_16 = (*(Vector3_t4 *)((Vector3_t4 *)(Vector3_t4 *)SZArrayLdElema(L_38, L_39)));
		Vector3_t4  L_40 = (__this->___P0_13);
		Vector3_t4  L_41 = (__this->___P1_14);
		Vector3_t4  L_42 = (__this->___P2_15);
		Vector3_t4  L_43 = (__this->___P3_16);
		float L_44 = (__this->___i_12);
		Vector3_t4  L_45 = WaypointCircuit_CatmullRom_m576(__this, L_40, L_41, L_42, L_43, L_44, /*hidden argument*/NULL);
		return L_45;
	}

IL_016c:
	{
		int32_t L_46 = V_0;
		int32_t L_47 = (__this->___numPoints_4);
		int32_t L_48 = (__this->___numPoints_4);
		__this->___p1n_9 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_46-(int32_t)1))+(int32_t)L_47))%(int32_t)L_48));
		int32_t L_49 = V_0;
		__this->___p2n_10 = L_49;
		Vector3U5BU5D_t210* L_50 = (__this->___points_5);
		int32_t L_51 = (__this->___p1n_9);
		NullCheck(L_50);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_50, L_51);
		Vector3U5BU5D_t210* L_52 = (__this->___points_5);
		int32_t L_53 = (__this->___p2n_10);
		NullCheck(L_52);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_52, L_53);
		float L_54 = (__this->___i_12);
		Vector3_t4  L_55 = Vector3_Lerp_m652(NULL /*static, unused*/, (*(Vector3_t4 *)((Vector3_t4 *)(Vector3_t4 *)SZArrayLdElema(L_50, L_51))), (*(Vector3_t4 *)((Vector3_t4 *)(Vector3_t4 *)SZArrayLdElema(L_52, L_53))), L_54, /*hidden argument*/NULL);
		return L_55;
	}
}
// UnityEngine.Vector3 UnityStandardAssets.Utility.WaypointCircuit::CatmullRom(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C" Vector3_t4  WaypointCircuit_CatmullRom_m576 (WaypointCircuit_t207 * __this, Vector3_t4  ___p0, Vector3_t4  ___p1, Vector3_t4  ___p2, Vector3_t4  ___p3, float ___i, const MethodInfo* method)
{
	{
		Vector3_t4  L_0 = ___p1;
		Vector3_t4  L_1 = Vector3_op_Multiply_m598(NULL /*static, unused*/, (2.0f), L_0, /*hidden argument*/NULL);
		Vector3_t4  L_2 = ___p0;
		Vector3_t4  L_3 = Vector3_op_UnaryNegation_m682(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		Vector3_t4  L_4 = ___p2;
		Vector3_t4  L_5 = Vector3_op_Addition_m604(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		float L_6 = ___i;
		Vector3_t4  L_7 = Vector3_op_Multiply_m600(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		Vector3_t4  L_8 = Vector3_op_Addition_m604(NULL /*static, unused*/, L_1, L_7, /*hidden argument*/NULL);
		Vector3_t4  L_9 = ___p0;
		Vector3_t4  L_10 = Vector3_op_Multiply_m598(NULL /*static, unused*/, (2.0f), L_9, /*hidden argument*/NULL);
		Vector3_t4  L_11 = ___p1;
		Vector3_t4  L_12 = Vector3_op_Multiply_m598(NULL /*static, unused*/, (5.0f), L_11, /*hidden argument*/NULL);
		Vector3_t4  L_13 = Vector3_op_Subtraction_m595(NULL /*static, unused*/, L_10, L_12, /*hidden argument*/NULL);
		Vector3_t4  L_14 = ___p2;
		Vector3_t4  L_15 = Vector3_op_Multiply_m598(NULL /*static, unused*/, (4.0f), L_14, /*hidden argument*/NULL);
		Vector3_t4  L_16 = Vector3_op_Addition_m604(NULL /*static, unused*/, L_13, L_15, /*hidden argument*/NULL);
		Vector3_t4  L_17 = ___p3;
		Vector3_t4  L_18 = Vector3_op_Subtraction_m595(NULL /*static, unused*/, L_16, L_17, /*hidden argument*/NULL);
		float L_19 = ___i;
		Vector3_t4  L_20 = Vector3_op_Multiply_m600(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
		float L_21 = ___i;
		Vector3_t4  L_22 = Vector3_op_Multiply_m600(NULL /*static, unused*/, L_20, L_21, /*hidden argument*/NULL);
		Vector3_t4  L_23 = Vector3_op_Addition_m604(NULL /*static, unused*/, L_8, L_22, /*hidden argument*/NULL);
		Vector3_t4  L_24 = ___p0;
		Vector3_t4  L_25 = Vector3_op_UnaryNegation_m682(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		Vector3_t4  L_26 = ___p1;
		Vector3_t4  L_27 = Vector3_op_Multiply_m598(NULL /*static, unused*/, (3.0f), L_26, /*hidden argument*/NULL);
		Vector3_t4  L_28 = Vector3_op_Addition_m604(NULL /*static, unused*/, L_25, L_27, /*hidden argument*/NULL);
		Vector3_t4  L_29 = ___p2;
		Vector3_t4  L_30 = Vector3_op_Multiply_m598(NULL /*static, unused*/, (3.0f), L_29, /*hidden argument*/NULL);
		Vector3_t4  L_31 = Vector3_op_Subtraction_m595(NULL /*static, unused*/, L_28, L_30, /*hidden argument*/NULL);
		Vector3_t4  L_32 = ___p3;
		Vector3_t4  L_33 = Vector3_op_Addition_m604(NULL /*static, unused*/, L_31, L_32, /*hidden argument*/NULL);
		float L_34 = ___i;
		Vector3_t4  L_35 = Vector3_op_Multiply_m600(NULL /*static, unused*/, L_33, L_34, /*hidden argument*/NULL);
		float L_36 = ___i;
		Vector3_t4  L_37 = Vector3_op_Multiply_m600(NULL /*static, unused*/, L_35, L_36, /*hidden argument*/NULL);
		float L_38 = ___i;
		Vector3_t4  L_39 = Vector3_op_Multiply_m600(NULL /*static, unused*/, L_37, L_38, /*hidden argument*/NULL);
		Vector3_t4  L_40 = Vector3_op_Addition_m604(NULL /*static, unused*/, L_23, L_39, /*hidden argument*/NULL);
		Vector3_t4  L_41 = Vector3_op_Multiply_m598(NULL /*static, unused*/, (0.5f), L_40, /*hidden argument*/NULL);
		return L_41;
	}
}
// System.Void UnityStandardAssets.Utility.WaypointCircuit::CachePositionsAndDistances()
extern TypeInfo* Vector3U5BU5D_t210_il2cpp_TypeInfo_var;
extern TypeInfo* SingleU5BU5D_t211_il2cpp_TypeInfo_var;
extern "C" void WaypointCircuit_CachePositionsAndDistances_m577 (WaypointCircuit_t207 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Vector3U5BU5D_t210_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(61);
		SingleU5BU5D_t211_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(18);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	Transform_t1 * V_2 = {0};
	Transform_t1 * V_3 = {0};
	Vector3_t4  V_4 = {0};
	Vector3_t4  V_5 = {0};
	Vector3_t4  V_6 = {0};
	{
		TransformU5BU5D_t141* L_0 = WaypointCircuit_get_Waypoints_m572(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		__this->___points_5 = ((Vector3U5BU5D_t210*)SZArrayNew(Vector3U5BU5D_t210_il2cpp_TypeInfo_var, ((int32_t)((int32_t)(((int32_t)(((Array_t *)L_0)->max_length)))+(int32_t)1))));
		TransformU5BU5D_t141* L_1 = WaypointCircuit_get_Waypoints_m572(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		__this->___distances_6 = ((SingleU5BU5D_t211*)SZArrayNew(SingleU5BU5D_t211_il2cpp_TypeInfo_var, ((int32_t)((int32_t)(((int32_t)(((Array_t *)L_1)->max_length)))+(int32_t)1))));
		V_0 = (0.0f);
		V_1 = 0;
		goto IL_00ce;
	}

IL_0037:
	{
		TransformU5BU5D_t141* L_2 = WaypointCircuit_get_Waypoints_m572(__this, /*hidden argument*/NULL);
		int32_t L_3 = V_1;
		TransformU5BU5D_t141* L_4 = WaypointCircuit_get_Waypoints_m572(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, ((int32_t)((int32_t)L_3%(int32_t)(((int32_t)(((Array_t *)L_4)->max_length))))));
		int32_t L_5 = ((int32_t)((int32_t)L_3%(int32_t)(((int32_t)(((Array_t *)L_4)->max_length)))));
		V_2 = (*(Transform_t1 **)(Transform_t1 **)SZArrayLdElema(L_2, L_5));
		TransformU5BU5D_t141* L_6 = WaypointCircuit_get_Waypoints_m572(__this, /*hidden argument*/NULL);
		int32_t L_7 = V_1;
		TransformU5BU5D_t141* L_8 = WaypointCircuit_get_Waypoints_m572(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, ((int32_t)((int32_t)((int32_t)((int32_t)L_7+(int32_t)1))%(int32_t)(((int32_t)(((Array_t *)L_8)->max_length))))));
		int32_t L_9 = ((int32_t)((int32_t)((int32_t)((int32_t)L_7+(int32_t)1))%(int32_t)(((int32_t)(((Array_t *)L_8)->max_length)))));
		V_3 = (*(Transform_t1 **)(Transform_t1 **)SZArrayLdElema(L_6, L_9));
		Transform_t1 * L_10 = V_2;
		bool L_11 = Object_op_Inequality_m623(NULL /*static, unused*/, L_10, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_00ca;
		}
	}
	{
		Transform_t1 * L_12 = V_3;
		bool L_13 = Object_op_Inequality_m623(NULL /*static, unused*/, L_12, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_00ca;
		}
	}
	{
		Transform_t1 * L_14 = V_2;
		NullCheck(L_14);
		Vector3_t4  L_15 = Transform_get_position_m593(L_14, /*hidden argument*/NULL);
		V_4 = L_15;
		Transform_t1 * L_16 = V_3;
		NullCheck(L_16);
		Vector3_t4  L_17 = Transform_get_position_m593(L_16, /*hidden argument*/NULL);
		V_5 = L_17;
		Vector3U5BU5D_t210* L_18 = (__this->___points_5);
		int32_t L_19 = V_1;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, L_19);
		TransformU5BU5D_t141* L_20 = WaypointCircuit_get_Waypoints_m572(__this, /*hidden argument*/NULL);
		int32_t L_21 = V_1;
		TransformU5BU5D_t141* L_22 = WaypointCircuit_get_Waypoints_m572(__this, /*hidden argument*/NULL);
		NullCheck(L_22);
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, ((int32_t)((int32_t)L_21%(int32_t)(((int32_t)(((Array_t *)L_22)->max_length))))));
		int32_t L_23 = ((int32_t)((int32_t)L_21%(int32_t)(((int32_t)(((Array_t *)L_22)->max_length)))));
		NullCheck((*(Transform_t1 **)(Transform_t1 **)SZArrayLdElema(L_20, L_23)));
		Vector3_t4  L_24 = Transform_get_position_m593((*(Transform_t1 **)(Transform_t1 **)SZArrayLdElema(L_20, L_23)), /*hidden argument*/NULL);
		*((Vector3_t4 *)(Vector3_t4 *)SZArrayLdElema(L_18, L_19)) = L_24;
		SingleU5BU5D_t211* L_25 = (__this->___distances_6);
		int32_t L_26 = V_1;
		float L_27 = V_0;
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, L_26);
		*((float*)(float*)SZArrayLdElema(L_25, L_26)) = (float)L_27;
		float L_28 = V_0;
		Vector3_t4  L_29 = V_4;
		Vector3_t4  L_30 = V_5;
		Vector3_t4  L_31 = Vector3_op_Subtraction_m595(NULL /*static, unused*/, L_29, L_30, /*hidden argument*/NULL);
		V_6 = L_31;
		float L_32 = Vector3_get_magnitude_m647((&V_6), /*hidden argument*/NULL);
		V_0 = ((float)((float)L_28+(float)L_32));
	}

IL_00ca:
	{
		int32_t L_33 = V_1;
		V_1 = ((int32_t)((int32_t)L_33+(int32_t)1));
	}

IL_00ce:
	{
		int32_t L_34 = V_1;
		Vector3U5BU5D_t210* L_35 = (__this->___points_5);
		NullCheck(L_35);
		if ((((int32_t)L_34) < ((int32_t)(((int32_t)(((Array_t *)L_35)->max_length))))))
		{
			goto IL_0037;
		}
	}
	{
		return;
	}
}
// System.Void UnityStandardAssets.Utility.WaypointCircuit::OnDrawGizmos()
extern "C" void WaypointCircuit_OnDrawGizmos_m578 (WaypointCircuit_t207 * __this, const MethodInfo* method)
{
	{
		WaypointCircuit_DrawGizmos_m580(__this, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Utility.WaypointCircuit::OnDrawGizmosSelected()
extern "C" void WaypointCircuit_OnDrawGizmosSelected_m579 (WaypointCircuit_t207 * __this, const MethodInfo* method)
{
	{
		WaypointCircuit_DrawGizmos_m580(__this, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Utility.WaypointCircuit::DrawGizmos(System.Boolean)
extern "C" void WaypointCircuit_DrawGizmos_m580 (WaypointCircuit_t207 * __this, bool ___selected, const MethodInfo* method)
{
	Vector3_t4  V_0 = {0};
	float V_1 = 0.0f;
	Vector3_t4  V_2 = {0};
	int32_t V_3 = 0;
	Vector3_t4  V_4 = {0};
	Color_t65  G_B4_0 = {0};
	{
		WaypointList_t208 * L_0 = (__this->___waypointList_2);
		NullCheck(L_0);
		L_0->___circuit_0 = __this;
		TransformU5BU5D_t141* L_1 = WaypointCircuit_get_Waypoints_m572(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		if ((((int32_t)(((int32_t)(((Array_t *)L_1)->max_length)))) <= ((int32_t)1)))
		{
			goto IL_0120;
		}
	}
	{
		TransformU5BU5D_t141* L_2 = WaypointCircuit_get_Waypoints_m572(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		__this->___numPoints_4 = (((int32_t)(((Array_t *)L_2)->max_length)));
		WaypointCircuit_CachePositionsAndDistances_m577(__this, /*hidden argument*/NULL);
		SingleU5BU5D_t211* L_3 = (__this->___distances_6);
		SingleU5BU5D_t211* L_4 = (__this->___distances_6);
		NullCheck(L_4);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, ((int32_t)((int32_t)(((int32_t)(((Array_t *)L_4)->max_length)))-(int32_t)1)));
		int32_t L_5 = ((int32_t)((int32_t)(((int32_t)(((Array_t *)L_4)->max_length)))-(int32_t)1));
		WaypointCircuit_set_Length_m571(__this, (*(float*)(float*)SZArrayLdElema(L_3, L_5)), /*hidden argument*/NULL);
		bool L_6 = ___selected;
		if (!L_6)
		{
			goto IL_0055;
		}
	}
	{
		Color_t65  L_7 = Color_get_yellow_m1018(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B4_0 = L_7;
		goto IL_006e;
	}

IL_0055:
	{
		Color_t65  L_8 = {0};
		Color__ctor_m746(&L_8, (1.0f), (1.0f), (0.0f), (0.5f), /*hidden argument*/NULL);
		G_B4_0 = L_8;
	}

IL_006e:
	{
		Gizmos_set_color_m890(NULL /*static, unused*/, G_B4_0, /*hidden argument*/NULL);
		TransformU5BU5D_t141* L_9 = WaypointCircuit_get_Waypoints_m572(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 0);
		int32_t L_10 = 0;
		NullCheck((*(Transform_t1 **)(Transform_t1 **)SZArrayLdElema(L_9, L_10)));
		Vector3_t4  L_11 = Transform_get_position_m593((*(Transform_t1 **)(Transform_t1 **)SZArrayLdElema(L_9, L_10)), /*hidden argument*/NULL);
		V_0 = L_11;
		bool L_12 = (__this->___smoothRoute_3);
		if (!L_12)
		{
			goto IL_00e2;
		}
	}
	{
		V_1 = (0.0f);
		goto IL_00be;
	}

IL_0097:
	{
		float L_13 = V_1;
		Vector3_t4  L_14 = WaypointCircuit_GetRoutePosition_m575(__this, ((float)((float)L_13+(float)(1.0f))), /*hidden argument*/NULL);
		V_2 = L_14;
		Vector3_t4  L_15 = V_0;
		Vector3_t4  L_16 = V_2;
		Gizmos_DrawLine_m894(NULL /*static, unused*/, L_15, L_16, /*hidden argument*/NULL);
		Vector3_t4  L_17 = V_2;
		V_0 = L_17;
		float L_18 = V_1;
		float L_19 = WaypointCircuit_get_Length_m570(__this, /*hidden argument*/NULL);
		float L_20 = (__this->___editorVisualisationSubsteps_7);
		V_1 = ((float)((float)L_18+(float)((float)((float)L_19/(float)L_20))));
	}

IL_00be:
	{
		float L_21 = V_1;
		float L_22 = WaypointCircuit_get_Length_m570(__this, /*hidden argument*/NULL);
		if ((((float)L_21) < ((float)L_22)))
		{
			goto IL_0097;
		}
	}
	{
		Vector3_t4  L_23 = V_0;
		TransformU5BU5D_t141* L_24 = WaypointCircuit_get_Waypoints_m572(__this, /*hidden argument*/NULL);
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 0);
		int32_t L_25 = 0;
		NullCheck((*(Transform_t1 **)(Transform_t1 **)SZArrayLdElema(L_24, L_25)));
		Vector3_t4  L_26 = Transform_get_position_m593((*(Transform_t1 **)(Transform_t1 **)SZArrayLdElema(L_24, L_25)), /*hidden argument*/NULL);
		Gizmos_DrawLine_m894(NULL /*static, unused*/, L_23, L_26, /*hidden argument*/NULL);
		goto IL_0120;
	}

IL_00e2:
	{
		V_3 = 0;
		goto IL_0112;
	}

IL_00e9:
	{
		TransformU5BU5D_t141* L_27 = WaypointCircuit_get_Waypoints_m572(__this, /*hidden argument*/NULL);
		int32_t L_28 = V_3;
		TransformU5BU5D_t141* L_29 = WaypointCircuit_get_Waypoints_m572(__this, /*hidden argument*/NULL);
		NullCheck(L_29);
		NullCheck(L_27);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_27, ((int32_t)((int32_t)((int32_t)((int32_t)L_28+(int32_t)1))%(int32_t)(((int32_t)(((Array_t *)L_29)->max_length))))));
		int32_t L_30 = ((int32_t)((int32_t)((int32_t)((int32_t)L_28+(int32_t)1))%(int32_t)(((int32_t)(((Array_t *)L_29)->max_length)))));
		NullCheck((*(Transform_t1 **)(Transform_t1 **)SZArrayLdElema(L_27, L_30)));
		Vector3_t4  L_31 = Transform_get_position_m593((*(Transform_t1 **)(Transform_t1 **)SZArrayLdElema(L_27, L_30)), /*hidden argument*/NULL);
		V_4 = L_31;
		Vector3_t4  L_32 = V_0;
		Vector3_t4  L_33 = V_4;
		Gizmos_DrawLine_m894(NULL /*static, unused*/, L_32, L_33, /*hidden argument*/NULL);
		Vector3_t4  L_34 = V_4;
		V_0 = L_34;
		int32_t L_35 = V_3;
		V_3 = ((int32_t)((int32_t)L_35+(int32_t)1));
	}

IL_0112:
	{
		int32_t L_36 = V_3;
		TransformU5BU5D_t141* L_37 = WaypointCircuit_get_Waypoints_m572(__this, /*hidden argument*/NULL);
		NullCheck(L_37);
		if ((((int32_t)L_36) < ((int32_t)(((int32_t)(((Array_t *)L_37)->max_length))))))
		{
			goto IL_00e9;
		}
	}

IL_0120:
	{
		return;
	}
}
// UnityStandardAssets.Utility.WaypointProgressTracker/ProgressStyle
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Wa_2.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.Utility.WaypointProgressTracker/ProgressStyle
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Wa_2MethodDeclarations.h"



// UnityStandardAssets.Utility.WaypointProgressTracker
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Wa_3.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.Utility.WaypointProgressTracker
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Wa_3MethodDeclarations.h"



// System.Void UnityStandardAssets.Utility.WaypointProgressTracker::.ctor()
extern "C" void WaypointProgressTracker__ctor_m581 (WaypointProgressTracker_t213 * __this, const MethodInfo* method)
{
	{
		__this->___lookAheadForTargetOffset_3 = (5.0f);
		__this->___lookAheadForTargetFactor_4 = (0.1f);
		__this->___lookAheadForSpeedOffset_5 = (10.0f);
		__this->___lookAheadForSpeedFactor_6 = (0.2f);
		__this->___pointToPointThreshold_8 = (4.0f);
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityStandardAssets.Utility.WaypointCircuit/RoutePoint UnityStandardAssets.Utility.WaypointProgressTracker::get_targetPoint()
extern "C" RoutePoint_t209  WaypointProgressTracker_get_targetPoint_m582 (WaypointProgressTracker_t213 * __this, const MethodInfo* method)
{
	{
		RoutePoint_t209  L_0 = (__this->___U3CtargetPointU3Ek__BackingField_14);
		return L_0;
	}
}
// System.Void UnityStandardAssets.Utility.WaypointProgressTracker::set_targetPoint(UnityStandardAssets.Utility.WaypointCircuit/RoutePoint)
extern "C" void WaypointProgressTracker_set_targetPoint_m583 (WaypointProgressTracker_t213 * __this, RoutePoint_t209  ___value, const MethodInfo* method)
{
	{
		RoutePoint_t209  L_0 = ___value;
		__this->___U3CtargetPointU3Ek__BackingField_14 = L_0;
		return;
	}
}
// UnityStandardAssets.Utility.WaypointCircuit/RoutePoint UnityStandardAssets.Utility.WaypointProgressTracker::get_speedPoint()
extern "C" RoutePoint_t209  WaypointProgressTracker_get_speedPoint_m584 (WaypointProgressTracker_t213 * __this, const MethodInfo* method)
{
	{
		RoutePoint_t209  L_0 = (__this->___U3CspeedPointU3Ek__BackingField_15);
		return L_0;
	}
}
// System.Void UnityStandardAssets.Utility.WaypointProgressTracker::set_speedPoint(UnityStandardAssets.Utility.WaypointCircuit/RoutePoint)
extern "C" void WaypointProgressTracker_set_speedPoint_m585 (WaypointProgressTracker_t213 * __this, RoutePoint_t209  ___value, const MethodInfo* method)
{
	{
		RoutePoint_t209  L_0 = ___value;
		__this->___U3CspeedPointU3Ek__BackingField_15 = L_0;
		return;
	}
}
// UnityStandardAssets.Utility.WaypointCircuit/RoutePoint UnityStandardAssets.Utility.WaypointProgressTracker::get_progressPoint()
extern "C" RoutePoint_t209  WaypointProgressTracker_get_progressPoint_m586 (WaypointProgressTracker_t213 * __this, const MethodInfo* method)
{
	{
		RoutePoint_t209  L_0 = (__this->___U3CprogressPointU3Ek__BackingField_16);
		return L_0;
	}
}
// System.Void UnityStandardAssets.Utility.WaypointProgressTracker::set_progressPoint(UnityStandardAssets.Utility.WaypointCircuit/RoutePoint)
extern "C" void WaypointProgressTracker_set_progressPoint_m587 (WaypointProgressTracker_t213 * __this, RoutePoint_t209  ___value, const MethodInfo* method)
{
	{
		RoutePoint_t209  L_0 = ___value;
		__this->___U3CprogressPointU3Ek__BackingField_16 = L_0;
		return;
	}
}
// System.Void UnityStandardAssets.Utility.WaypointProgressTracker::Start()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* GameObject_t78_il2cpp_TypeInfo_var;
extern "C" void WaypointProgressTracker_Start_m588 (WaypointProgressTracker_t213 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		GameObject_t78_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(42);
		s_Il2CppMethodIntialized = true;
	}
	{
		Transform_t1 * L_0 = (__this->___target_9);
		bool L_1 = Object_op_Equality_m640(NULL /*static, unused*/, L_0, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0031;
		}
	}
	{
		String_t* L_2 = Object_get_name_m798(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m860(NULL /*static, unused*/, L_2, (String_t*) &_stringLiteral195, /*hidden argument*/NULL);
		GameObject_t78 * L_4 = (GameObject_t78 *)il2cpp_codegen_object_new (GameObject_t78_il2cpp_TypeInfo_var);
		GameObject__ctor_m982(L_4, L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_t1 * L_5 = GameObject_get_transform_m609(L_4, /*hidden argument*/NULL);
		__this->___target_9 = L_5;
	}

IL_0031:
	{
		WaypointProgressTracker_Reset_m589(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Utility.WaypointProgressTracker::Reset()
extern "C" void WaypointProgressTracker_Reset_m589 (WaypointProgressTracker_t213 * __this, const MethodInfo* method)
{
	{
		__this->___progressDistance_10 = (0.0f);
		__this->___progressNum_11 = 0;
		int32_t L_0 = (__this->___progressStyle_7);
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0062;
		}
	}
	{
		Transform_t1 * L_1 = (__this->___target_9);
		WaypointCircuit_t207 * L_2 = (__this->___circuit_2);
		NullCheck(L_2);
		TransformU5BU5D_t141* L_3 = WaypointCircuit_get_Waypoints_m572(L_2, /*hidden argument*/NULL);
		int32_t L_4 = (__this->___progressNum_11);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		NullCheck((*(Transform_t1 **)(Transform_t1 **)SZArrayLdElema(L_3, L_5)));
		Vector3_t4  L_6 = Transform_get_position_m593((*(Transform_t1 **)(Transform_t1 **)SZArrayLdElema(L_3, L_5)), /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_set_position_m607(L_1, L_6, /*hidden argument*/NULL);
		Transform_t1 * L_7 = (__this->___target_9);
		WaypointCircuit_t207 * L_8 = (__this->___circuit_2);
		NullCheck(L_8);
		TransformU5BU5D_t141* L_9 = WaypointCircuit_get_Waypoints_m572(L_8, /*hidden argument*/NULL);
		int32_t L_10 = (__this->___progressNum_11);
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		NullCheck((*(Transform_t1 **)(Transform_t1 **)SZArrayLdElema(L_9, L_11)));
		Quaternion_t19  L_12 = Transform_get_rotation_m656((*(Transform_t1 **)(Transform_t1 **)SZArrayLdElema(L_9, L_11)), /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_set_rotation_m658(L_7, L_12, /*hidden argument*/NULL);
	}

IL_0062:
	{
		return;
	}
}
// System.Void UnityStandardAssets.Utility.WaypointProgressTracker::Update()
extern TypeInfo* Mathf_t218_il2cpp_TypeInfo_var;
extern "C" void WaypointProgressTracker_Update_m590 (WaypointProgressTracker_t213 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t4  V_0 = {0};
	Vector3_t4  V_1 = {0};
	Vector3_t4  V_2 = {0};
	Vector3_t4  V_3 = {0};
	RoutePoint_t209  V_4 = {0};
	RoutePoint_t209  V_5 = {0};
	RoutePoint_t209  V_6 = {0};
	RoutePoint_t209  V_7 = {0};
	RoutePoint_t209  V_8 = {0};
	RoutePoint_t209  V_9 = {0};
	{
		int32_t L_0 = (__this->___progressStyle_7);
		if (L_0)
		{
			goto IL_0153;
		}
	}
	{
		float L_1 = Time_get_deltaTime_m602(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((float)L_1) > ((float)(0.0f)))))
		{
			goto IL_0054;
		}
	}
	{
		float L_2 = (__this->___speed_13);
		Vector3_t4  L_3 = (__this->___lastPosition_12);
		Transform_t1 * L_4 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t4  L_5 = Transform_get_position_m593(L_4, /*hidden argument*/NULL);
		Vector3_t4  L_6 = Vector3_op_Subtraction_m595(NULL /*static, unused*/, L_3, L_5, /*hidden argument*/NULL);
		V_3 = L_6;
		float L_7 = Vector3_get_magnitude_m647((&V_3), /*hidden argument*/NULL);
		float L_8 = Time_get_deltaTime_m602(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_9 = Time_get_deltaTime_m602(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_10 = Mathf_Lerp_m610(NULL /*static, unused*/, L_2, ((float)((float)L_7/(float)L_8)), L_9, /*hidden argument*/NULL);
		__this->___speed_13 = L_10;
	}

IL_0054:
	{
		Transform_t1 * L_11 = (__this->___target_9);
		WaypointCircuit_t207 * L_12 = (__this->___circuit_2);
		float L_13 = (__this->___progressDistance_10);
		float L_14 = (__this->___lookAheadForTargetOffset_3);
		float L_15 = (__this->___lookAheadForTargetFactor_4);
		float L_16 = (__this->___speed_13);
		NullCheck(L_12);
		RoutePoint_t209  L_17 = WaypointCircuit_GetRoutePoint_m574(L_12, ((float)((float)((float)((float)L_13+(float)L_14))+(float)((float)((float)L_15*(float)L_16)))), /*hidden argument*/NULL);
		V_4 = L_17;
		Vector3_t4  L_18 = ((&V_4)->___position_0);
		NullCheck(L_11);
		Transform_set_position_m607(L_11, L_18, /*hidden argument*/NULL);
		Transform_t1 * L_19 = (__this->___target_9);
		WaypointCircuit_t207 * L_20 = (__this->___circuit_2);
		float L_21 = (__this->___progressDistance_10);
		float L_22 = (__this->___lookAheadForSpeedOffset_5);
		float L_23 = (__this->___lookAheadForSpeedFactor_6);
		float L_24 = (__this->___speed_13);
		NullCheck(L_20);
		RoutePoint_t209  L_25 = WaypointCircuit_GetRoutePoint_m574(L_20, ((float)((float)((float)((float)L_21+(float)L_22))+(float)((float)((float)L_23*(float)L_24)))), /*hidden argument*/NULL);
		V_5 = L_25;
		Vector3_t4  L_26 = ((&V_5)->___direction_1);
		Quaternion_t19  L_27 = Quaternion_LookRotation_m917(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		NullCheck(L_19);
		Transform_set_rotation_m658(L_19, L_27, /*hidden argument*/NULL);
		WaypointCircuit_t207 * L_28 = (__this->___circuit_2);
		float L_29 = (__this->___progressDistance_10);
		NullCheck(L_28);
		RoutePoint_t209  L_30 = WaypointCircuit_GetRoutePoint_m574(L_28, L_29, /*hidden argument*/NULL);
		WaypointProgressTracker_set_progressPoint_m587(__this, L_30, /*hidden argument*/NULL);
		RoutePoint_t209  L_31 = WaypointProgressTracker_get_progressPoint_m586(__this, /*hidden argument*/NULL);
		V_6 = L_31;
		Vector3_t4  L_32 = ((&V_6)->___position_0);
		Transform_t1 * L_33 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_33);
		Vector3_t4  L_34 = Transform_get_position_m593(L_33, /*hidden argument*/NULL);
		Vector3_t4  L_35 = Vector3_op_Subtraction_m595(NULL /*static, unused*/, L_32, L_34, /*hidden argument*/NULL);
		V_0 = L_35;
		Vector3_t4  L_36 = V_0;
		RoutePoint_t209  L_37 = WaypointProgressTracker_get_progressPoint_m586(__this, /*hidden argument*/NULL);
		V_7 = L_37;
		Vector3_t4  L_38 = ((&V_7)->___direction_1);
		float L_39 = Vector3_Dot_m791(NULL /*static, unused*/, L_36, L_38, /*hidden argument*/NULL);
		if ((!(((float)L_39) < ((float)(0.0f)))))
		{
			goto IL_013d;
		}
	}
	{
		float L_40 = (__this->___progressDistance_10);
		float L_41 = Vector3_get_magnitude_m647((&V_0), /*hidden argument*/NULL);
		__this->___progressDistance_10 = ((float)((float)L_40+(float)((float)((float)L_41*(float)(0.5f)))));
	}

IL_013d:
	{
		Transform_t1 * L_42 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_42);
		Vector3_t4  L_43 = Transform_get_position_m593(L_42, /*hidden argument*/NULL);
		__this->___lastPosition_12 = L_43;
		goto IL_025c;
	}

IL_0153:
	{
		Transform_t1 * L_44 = (__this->___target_9);
		NullCheck(L_44);
		Vector3_t4  L_45 = Transform_get_position_m593(L_44, /*hidden argument*/NULL);
		Transform_t1 * L_46 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_46);
		Vector3_t4  L_47 = Transform_get_position_m593(L_46, /*hidden argument*/NULL);
		Vector3_t4  L_48 = Vector3_op_Subtraction_m595(NULL /*static, unused*/, L_45, L_47, /*hidden argument*/NULL);
		V_1 = L_48;
		float L_49 = Vector3_get_magnitude_m647((&V_1), /*hidden argument*/NULL);
		float L_50 = (__this->___pointToPointThreshold_8);
		if ((!(((float)L_49) < ((float)L_50))))
		{
			goto IL_019d;
		}
	}
	{
		int32_t L_51 = (__this->___progressNum_11);
		WaypointCircuit_t207 * L_52 = (__this->___circuit_2);
		NullCheck(L_52);
		TransformU5BU5D_t141* L_53 = WaypointCircuit_get_Waypoints_m572(L_52, /*hidden argument*/NULL);
		NullCheck(L_53);
		__this->___progressNum_11 = ((int32_t)((int32_t)((int32_t)((int32_t)L_51+(int32_t)1))%(int32_t)(((int32_t)(((Array_t *)L_53)->max_length)))));
	}

IL_019d:
	{
		Transform_t1 * L_54 = (__this->___target_9);
		WaypointCircuit_t207 * L_55 = (__this->___circuit_2);
		NullCheck(L_55);
		TransformU5BU5D_t141* L_56 = WaypointCircuit_get_Waypoints_m572(L_55, /*hidden argument*/NULL);
		int32_t L_57 = (__this->___progressNum_11);
		NullCheck(L_56);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_56, L_57);
		int32_t L_58 = L_57;
		NullCheck((*(Transform_t1 **)(Transform_t1 **)SZArrayLdElema(L_56, L_58)));
		Vector3_t4  L_59 = Transform_get_position_m593((*(Transform_t1 **)(Transform_t1 **)SZArrayLdElema(L_56, L_58)), /*hidden argument*/NULL);
		NullCheck(L_54);
		Transform_set_position_m607(L_54, L_59, /*hidden argument*/NULL);
		Transform_t1 * L_60 = (__this->___target_9);
		WaypointCircuit_t207 * L_61 = (__this->___circuit_2);
		NullCheck(L_61);
		TransformU5BU5D_t141* L_62 = WaypointCircuit_get_Waypoints_m572(L_61, /*hidden argument*/NULL);
		int32_t L_63 = (__this->___progressNum_11);
		NullCheck(L_62);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_62, L_63);
		int32_t L_64 = L_63;
		NullCheck((*(Transform_t1 **)(Transform_t1 **)SZArrayLdElema(L_62, L_64)));
		Quaternion_t19  L_65 = Transform_get_rotation_m656((*(Transform_t1 **)(Transform_t1 **)SZArrayLdElema(L_62, L_64)), /*hidden argument*/NULL);
		NullCheck(L_60);
		Transform_set_rotation_m658(L_60, L_65, /*hidden argument*/NULL);
		WaypointCircuit_t207 * L_66 = (__this->___circuit_2);
		float L_67 = (__this->___progressDistance_10);
		NullCheck(L_66);
		RoutePoint_t209  L_68 = WaypointCircuit_GetRoutePoint_m574(L_66, L_67, /*hidden argument*/NULL);
		WaypointProgressTracker_set_progressPoint_m587(__this, L_68, /*hidden argument*/NULL);
		RoutePoint_t209  L_69 = WaypointProgressTracker_get_progressPoint_m586(__this, /*hidden argument*/NULL);
		V_8 = L_69;
		Vector3_t4  L_70 = ((&V_8)->___position_0);
		Transform_t1 * L_71 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_71);
		Vector3_t4  L_72 = Transform_get_position_m593(L_71, /*hidden argument*/NULL);
		Vector3_t4  L_73 = Vector3_op_Subtraction_m595(NULL /*static, unused*/, L_70, L_72, /*hidden argument*/NULL);
		V_2 = L_73;
		Vector3_t4  L_74 = V_2;
		RoutePoint_t209  L_75 = WaypointProgressTracker_get_progressPoint_m586(__this, /*hidden argument*/NULL);
		V_9 = L_75;
		Vector3_t4  L_76 = ((&V_9)->___direction_1);
		float L_77 = Vector3_Dot_m791(NULL /*static, unused*/, L_74, L_76, /*hidden argument*/NULL);
		if ((!(((float)L_77) < ((float)(0.0f)))))
		{
			goto IL_024b;
		}
	}
	{
		float L_78 = (__this->___progressDistance_10);
		float L_79 = Vector3_get_magnitude_m647((&V_2), /*hidden argument*/NULL);
		__this->___progressDistance_10 = ((float)((float)L_78+(float)L_79));
	}

IL_024b:
	{
		Transform_t1 * L_80 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_80);
		Vector3_t4  L_81 = Transform_get_position_m593(L_80, /*hidden argument*/NULL);
		__this->___lastPosition_12 = L_81;
	}

IL_025c:
	{
		return;
	}
}
// System.Void UnityStandardAssets.Utility.WaypointProgressTracker::OnDrawGizmos()
extern "C" void WaypointProgressTracker_OnDrawGizmos_m591 (WaypointProgressTracker_t213 * __this, const MethodInfo* method)
{
	{
		bool L_0 = Application_get_isPlaying_m645(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_007f;
		}
	}
	{
		Color_t65  L_1 = Color_get_green_m1019(NULL /*static, unused*/, /*hidden argument*/NULL);
		Gizmos_set_color_m890(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		Transform_t1 * L_2 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t4  L_3 = Transform_get_position_m593(L_2, /*hidden argument*/NULL);
		Transform_t1 * L_4 = (__this->___target_9);
		NullCheck(L_4);
		Vector3_t4  L_5 = Transform_get_position_m593(L_4, /*hidden argument*/NULL);
		Gizmos_DrawLine_m894(NULL /*static, unused*/, L_3, L_5, /*hidden argument*/NULL);
		WaypointCircuit_t207 * L_6 = (__this->___circuit_2);
		float L_7 = (__this->___progressDistance_10);
		NullCheck(L_6);
		Vector3_t4  L_8 = WaypointCircuit_GetRoutePosition_m575(L_6, L_7, /*hidden argument*/NULL);
		Gizmos_DrawWireSphere_m1020(NULL /*static, unused*/, L_8, (1.0f), /*hidden argument*/NULL);
		Color_t65  L_9 = Color_get_yellow_m1018(NULL /*static, unused*/, /*hidden argument*/NULL);
		Gizmos_set_color_m890(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		Transform_t1 * L_10 = (__this->___target_9);
		NullCheck(L_10);
		Vector3_t4  L_11 = Transform_get_position_m593(L_10, /*hidden argument*/NULL);
		Transform_t1 * L_12 = (__this->___target_9);
		NullCheck(L_12);
		Vector3_t4  L_13 = Transform_get_position_m593(L_12, /*hidden argument*/NULL);
		Transform_t1 * L_14 = (__this->___target_9);
		NullCheck(L_14);
		Vector3_t4  L_15 = Transform_get_forward_m643(L_14, /*hidden argument*/NULL);
		Vector3_t4  L_16 = Vector3_op_Addition_m604(NULL /*static, unused*/, L_13, L_15, /*hidden argument*/NULL);
		Gizmos_DrawLine_m894(NULL /*static, unused*/, L_11, L_16, /*hidden argument*/NULL);
	}

IL_007f:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
