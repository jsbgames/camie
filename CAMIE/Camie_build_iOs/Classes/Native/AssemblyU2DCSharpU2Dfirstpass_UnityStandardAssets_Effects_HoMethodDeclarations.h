﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets.Effects.Hose
struct Hose_t155;

// System.Void UnityStandardAssets.Effects.Hose::.ctor()
extern "C" void Hose__ctor_m432 (Hose_t155 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Effects.Hose::Update()
extern "C" void Hose_Update_m433 (Hose_t155 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
