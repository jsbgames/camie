﻿#pragma once
#include <stdint.h>
// System.Text.Encoding
#include "mscorlib_System_Text_Encoding.h"
// System.Text.UTF8Encoding
struct  UTF8Encoding_t2155  : public Encoding_t1022
{
	// System.Boolean System.Text.UTF8Encoding::emitIdentifier
	bool ___emitIdentifier_28;
};
