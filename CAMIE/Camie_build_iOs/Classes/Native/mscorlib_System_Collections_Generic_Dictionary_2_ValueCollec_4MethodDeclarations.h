﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>
struct Enumerator_t1054;
// System.Object
struct Object_t;
// UnityEngine.GUIStyle
struct GUIStyle_t302;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>
struct Dictionary_2_t831;

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_7MethodDeclarations.h"
#define Enumerator__ctor_m19271(__this, ___host, method) (( void (*) (Enumerator_t1054 *, Dictionary_2_t831 *, const MethodInfo*))Enumerator__ctor_m13721_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m19272(__this, method) (( Object_t * (*) (Enumerator_t1054 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m13722_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>::Dispose()
#define Enumerator_Dispose_m19273(__this, method) (( void (*) (Enumerator_t1054 *, const MethodInfo*))Enumerator_Dispose_m13723_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>::MoveNext()
#define Enumerator_MoveNext_m19274(__this, method) (( bool (*) (Enumerator_t1054 *, const MethodInfo*))Enumerator_MoveNext_m13724_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>::get_Current()
#define Enumerator_get_Current_m19275(__this, method) (( GUIStyle_t302 * (*) (Enumerator_t1054 *, const MethodInfo*))Enumerator_get_Current_m13725_gshared)(__this, method)
