﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "stringLiterals.h"
// <Module>
#include "AssemblyU2DCSharpU2Dfirstpass_U3CModuleU3E.h"
// Metadata Definition <Module>
extern TypeInfo U3CModuleU3E_t0_il2cpp_TypeInfo;
// <Module>
#include "AssemblyU2DCSharpU2Dfirstpass_U3CModuleU3EMethodDeclarations.h"
static const MethodInfo* U3CModuleU3E_t0_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType U3CModuleU3E_t0_0_0_0;
extern const Il2CppType U3CModuleU3E_t0_1_0_0;
struct U3CModuleU3E_t0;
const Il2CppTypeDefinitionMetadata U3CModuleU3E_t0_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U3CModuleU3E_t0_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "<Module>"/* name */
	, ""/* namespaze */
	, U3CModuleU3E_t0_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U3CModuleU3E_t0_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U3CModuleU3E_t0_0_0_0/* byval_arg */
	, &U3CModuleU3E_t0_1_0_0/* this_arg */
	, &U3CModuleU3E_t0_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CModuleU3E_t0)/* instance_size */
	, sizeof (U3CModuleU3E_t0)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets._2D.Camera2DFollow
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets__2D_Camera.h"
// Metadata Definition UnityStandardAssets._2D.Camera2DFollow
extern TypeInfo Camera2DFollow_t2_il2cpp_TypeInfo;
// UnityStandardAssets._2D.Camera2DFollow
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets__2D_CameraMethodDeclarations.h"
extern const Il2CppType Void_t272_0_0_0;
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets._2D.Camera2DFollow::.ctor()
extern const MethodInfo Camera2DFollow__ctor_m0_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Camera2DFollow__ctor_m0/* method */
	, &Camera2DFollow_t2_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets._2D.Camera2DFollow::Start()
extern const MethodInfo Camera2DFollow_Start_m1_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&Camera2DFollow_Start_m1/* method */
	, &Camera2DFollow_t2_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets._2D.Camera2DFollow::Update()
extern const MethodInfo Camera2DFollow_Update_m2_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&Camera2DFollow_Update_m2/* method */
	, &Camera2DFollow_t2_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Camera2DFollow_t2_MethodInfos[] =
{
	&Camera2DFollow__ctor_m0_MethodInfo,
	&Camera2DFollow_Start_m1_MethodInfo,
	&Camera2DFollow_Update_m2_MethodInfo,
	NULL
};
extern const MethodInfo Object_Equals_m1047_MethodInfo;
extern const MethodInfo Object_Finalize_m1048_MethodInfo;
extern const MethodInfo Object_GetHashCode_m1049_MethodInfo;
extern const MethodInfo Object_ToString_m1050_MethodInfo;
static const Il2CppMethodReference Camera2DFollow_t2_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool Camera2DFollow_t2_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType Camera2DFollow_t2_0_0_0;
extern const Il2CppType Camera2DFollow_t2_1_0_0;
extern const Il2CppType MonoBehaviour_t3_0_0_0;
struct Camera2DFollow_t2;
const Il2CppTypeDefinitionMetadata Camera2DFollow_t2_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, Camera2DFollow_t2_VTable/* vtableMethods */
	, Camera2DFollow_t2_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 0/* fieldStart */

};
TypeInfo Camera2DFollow_t2_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "Camera2DFollow"/* name */
	, "UnityStandardAssets._2D"/* namespaze */
	, Camera2DFollow_t2_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Camera2DFollow_t2_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Camera2DFollow_t2_0_0_0/* byval_arg */
	, &Camera2DFollow_t2_1_0_0/* this_arg */
	, &Camera2DFollow_t2_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Camera2DFollow_t2)/* instance_size */
	, sizeof (Camera2DFollow_t2)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 9/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets._2D.CameraFollow
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets__2D_Camera_0.h"
// Metadata Definition UnityStandardAssets._2D.CameraFollow
extern TypeInfo CameraFollow_t5_il2cpp_TypeInfo;
// UnityStandardAssets._2D.CameraFollow
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets__2D_Camera_0MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets._2D.CameraFollow::.ctor()
extern const MethodInfo CameraFollow__ctor_m3_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CameraFollow__ctor_m3/* method */
	, &CameraFollow_t5_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets._2D.CameraFollow::Awake()
extern const MethodInfo CameraFollow_Awake_m4_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&CameraFollow_Awake_m4/* method */
	, &CameraFollow_t5_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityStandardAssets._2D.CameraFollow::CheckXMargin()
extern const MethodInfo CameraFollow_CheckXMargin_m5_MethodInfo = 
{
	"CheckXMargin"/* name */
	, (methodPointerType)&CameraFollow_CheckXMargin_m5/* method */
	, &CameraFollow_t5_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityStandardAssets._2D.CameraFollow::CheckYMargin()
extern const MethodInfo CameraFollow_CheckYMargin_m6_MethodInfo = 
{
	"CheckYMargin"/* name */
	, (methodPointerType)&CameraFollow_CheckYMargin_m6/* method */
	, &CameraFollow_t5_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 7/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets._2D.CameraFollow::Update()
extern const MethodInfo CameraFollow_Update_m7_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&CameraFollow_Update_m7/* method */
	, &CameraFollow_t5_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 8/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets._2D.CameraFollow::TrackPlayer()
extern const MethodInfo CameraFollow_TrackPlayer_m8_MethodInfo = 
{
	"TrackPlayer"/* name */
	, (methodPointerType)&CameraFollow_TrackPlayer_m8/* method */
	, &CameraFollow_t5_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 9/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CameraFollow_t5_MethodInfos[] =
{
	&CameraFollow__ctor_m3_MethodInfo,
	&CameraFollow_Awake_m4_MethodInfo,
	&CameraFollow_CheckXMargin_m5_MethodInfo,
	&CameraFollow_CheckYMargin_m6_MethodInfo,
	&CameraFollow_Update_m7_MethodInfo,
	&CameraFollow_TrackPlayer_m8_MethodInfo,
	NULL
};
static const Il2CppMethodReference CameraFollow_t5_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool CameraFollow_t5_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType CameraFollow_t5_0_0_0;
extern const Il2CppType CameraFollow_t5_1_0_0;
struct CameraFollow_t5;
const Il2CppTypeDefinitionMetadata CameraFollow_t5_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, CameraFollow_t5_VTable/* vtableMethods */
	, CameraFollow_t5_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 9/* fieldStart */

};
TypeInfo CameraFollow_t5_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFollow"/* name */
	, "UnityStandardAssets._2D"/* namespaze */
	, CameraFollow_t5_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CameraFollow_t5_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CameraFollow_t5_0_0_0/* byval_arg */
	, &CameraFollow_t5_1_0_0/* this_arg */
	, &CameraFollow_t5_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFollow_t5)/* instance_size */
	, sizeof (CameraFollow_t5)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets._2D.Platformer2DUserControl
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets__2D_Platfo.h"
// Metadata Definition UnityStandardAssets._2D.Platformer2DUserControl
extern TypeInfo Platformer2DUserControl_t8_il2cpp_TypeInfo;
// UnityStandardAssets._2D.Platformer2DUserControl
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets__2D_PlatfoMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets._2D.Platformer2DUserControl::.ctor()
extern const MethodInfo Platformer2DUserControl__ctor_m9_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Platformer2DUserControl__ctor_m9/* method */
	, &Platformer2DUserControl_t8_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 10/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets._2D.Platformer2DUserControl::Awake()
extern const MethodInfo Platformer2DUserControl_Awake_m10_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&Platformer2DUserControl_Awake_m10/* method */
	, &Platformer2DUserControl_t8_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 11/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets._2D.Platformer2DUserControl::Update()
extern const MethodInfo Platformer2DUserControl_Update_m11_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&Platformer2DUserControl_Update_m11/* method */
	, &Platformer2DUserControl_t8_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 12/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets._2D.Platformer2DUserControl::FixedUpdate()
extern const MethodInfo Platformer2DUserControl_FixedUpdate_m12_MethodInfo = 
{
	"FixedUpdate"/* name */
	, (methodPointerType)&Platformer2DUserControl_FixedUpdate_m12/* method */
	, &Platformer2DUserControl_t8_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 13/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Platformer2DUserControl_t8_MethodInfos[] =
{
	&Platformer2DUserControl__ctor_m9_MethodInfo,
	&Platformer2DUserControl_Awake_m10_MethodInfo,
	&Platformer2DUserControl_Update_m11_MethodInfo,
	&Platformer2DUserControl_FixedUpdate_m12_MethodInfo,
	NULL
};
static const Il2CppMethodReference Platformer2DUserControl_t8_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool Platformer2DUserControl_t8_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType Platformer2DUserControl_t8_0_0_0;
extern const Il2CppType Platformer2DUserControl_t8_1_0_0;
struct Platformer2DUserControl_t8;
const Il2CppTypeDefinitionMetadata Platformer2DUserControl_t8_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, Platformer2DUserControl_t8_VTable/* vtableMethods */
	, Platformer2DUserControl_t8_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 16/* fieldStart */

};
TypeInfo Platformer2DUserControl_t8_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "Platformer2DUserControl"/* name */
	, "UnityStandardAssets._2D"/* namespaze */
	, Platformer2DUserControl_t8_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Platformer2DUserControl_t8_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2/* custom_attributes_cache */
	, &Platformer2DUserControl_t8_0_0_0/* byval_arg */
	, &Platformer2DUserControl_t8_1_0_0/* this_arg */
	, &Platformer2DUserControl_t8_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Platformer2DUserControl_t8)/* instance_size */
	, sizeof (Platformer2DUserControl_t8)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets._2D.PlatformerCharacter2D
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets__2D_Platfo_0.h"
// Metadata Definition UnityStandardAssets._2D.PlatformerCharacter2D
extern TypeInfo PlatformerCharacter2D_t7_il2cpp_TypeInfo;
// UnityStandardAssets._2D.PlatformerCharacter2D
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets__2D_Platfo_0MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets._2D.PlatformerCharacter2D::.ctor()
extern const MethodInfo PlatformerCharacter2D__ctor_m13_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PlatformerCharacter2D__ctor_m13/* method */
	, &PlatformerCharacter2D_t7_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 14/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets._2D.PlatformerCharacter2D::Awake()
extern const MethodInfo PlatformerCharacter2D_Awake_m14_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&PlatformerCharacter2D_Awake_m14/* method */
	, &PlatformerCharacter2D_t7_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 15/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets._2D.PlatformerCharacter2D::FixedUpdate()
extern const MethodInfo PlatformerCharacter2D_FixedUpdate_m15_MethodInfo = 
{
	"FixedUpdate"/* name */
	, (methodPointerType)&PlatformerCharacter2D_FixedUpdate_m15/* method */
	, &PlatformerCharacter2D_t7_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 16/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
extern const Il2CppType Single_t254_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo PlatformerCharacter2D_t7_PlatformerCharacter2D_Move_m16_ParameterInfos[] = 
{
	{"move", 0, 134217729, 0, &Single_t254_0_0_0},
	{"crouch", 1, 134217730, 0, &Boolean_t273_0_0_0},
	{"jump", 2, 134217731, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Single_t254_SByte_t274_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets._2D.PlatformerCharacter2D::Move(System.Single,System.Boolean,System.Boolean)
extern const MethodInfo PlatformerCharacter2D_Move_m16_MethodInfo = 
{
	"Move"/* name */
	, (methodPointerType)&PlatformerCharacter2D_Move_m16/* method */
	, &PlatformerCharacter2D_t7_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Single_t254_SByte_t274_SByte_t274/* invoker_method */
	, PlatformerCharacter2D_t7_PlatformerCharacter2D_Move_m16_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 17/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets._2D.PlatformerCharacter2D::Flip()
extern const MethodInfo PlatformerCharacter2D_Flip_m17_MethodInfo = 
{
	"Flip"/* name */
	, (methodPointerType)&PlatformerCharacter2D_Flip_m17/* method */
	, &PlatformerCharacter2D_t7_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 18/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* PlatformerCharacter2D_t7_MethodInfos[] =
{
	&PlatformerCharacter2D__ctor_m13_MethodInfo,
	&PlatformerCharacter2D_Awake_m14_MethodInfo,
	&PlatformerCharacter2D_FixedUpdate_m15_MethodInfo,
	&PlatformerCharacter2D_Move_m16_MethodInfo,
	&PlatformerCharacter2D_Flip_m17_MethodInfo,
	NULL
};
static const Il2CppMethodReference PlatformerCharacter2D_t7_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool PlatformerCharacter2D_t7_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType PlatformerCharacter2D_t7_0_0_0;
extern const Il2CppType PlatformerCharacter2D_t7_1_0_0;
struct PlatformerCharacter2D_t7;
const Il2CppTypeDefinitionMetadata PlatformerCharacter2D_t7_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, PlatformerCharacter2D_t7_VTable/* vtableMethods */
	, PlatformerCharacter2D_t7_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 18/* fieldStart */

};
TypeInfo PlatformerCharacter2D_t7_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "PlatformerCharacter2D"/* name */
	, "UnityStandardAssets._2D"/* namespaze */
	, PlatformerCharacter2D_t7_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &PlatformerCharacter2D_t7_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PlatformerCharacter2D_t7_0_0_0/* byval_arg */
	, &PlatformerCharacter2D_t7_1_0_0/* this_arg */
	, &PlatformerCharacter2D_t7_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PlatformerCharacter2D_t7)/* instance_size */
	, sizeof (PlatformerCharacter2D_t7)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 13/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets._2D.Restarter
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets__2D_Restar.h"
// Metadata Definition UnityStandardAssets._2D.Restarter
extern TypeInfo Restarter_t12_il2cpp_TypeInfo;
// UnityStandardAssets._2D.Restarter
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets__2D_RestarMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets._2D.Restarter::.ctor()
extern const MethodInfo Restarter__ctor_m18_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Restarter__ctor_m18/* method */
	, &Restarter_t12_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 19/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Collider2D_t214_0_0_0;
extern const Il2CppType Collider2D_t214_0_0_0;
static const ParameterInfo Restarter_t12_Restarter_OnTriggerEnter2D_m19_ParameterInfos[] = 
{
	{"other", 0, 134217732, 0, &Collider2D_t214_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets._2D.Restarter::OnTriggerEnter2D(UnityEngine.Collider2D)
extern const MethodInfo Restarter_OnTriggerEnter2D_m19_MethodInfo = 
{
	"OnTriggerEnter2D"/* name */
	, (methodPointerType)&Restarter_OnTriggerEnter2D_m19/* method */
	, &Restarter_t12_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, Restarter_t12_Restarter_OnTriggerEnter2D_m19_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 20/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Restarter_t12_MethodInfos[] =
{
	&Restarter__ctor_m18_MethodInfo,
	&Restarter_OnTriggerEnter2D_m19_MethodInfo,
	NULL
};
static const Il2CppMethodReference Restarter_t12_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool Restarter_t12_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType Restarter_t12_0_0_0;
extern const Il2CppType Restarter_t12_1_0_0;
struct Restarter_t12;
const Il2CppTypeDefinitionMetadata Restarter_t12_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, Restarter_t12_VTable/* vtableMethods */
	, Restarter_t12_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo Restarter_t12_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "Restarter"/* name */
	, "UnityStandardAssets._2D"/* namespaze */
	, Restarter_t12_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Restarter_t12_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Restarter_t12_0_0_0/* byval_arg */
	, &Restarter_t12_1_0_0/* this_arg */
	, &Restarter_t12_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Restarter_t12)/* instance_size */
	, sizeof (Restarter_t12)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.Cameras.AbstractTargetFollower/UpdateType
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Cameras_Ab.h"
// Metadata Definition UnityStandardAssets.Cameras.AbstractTargetFollower/UpdateType
extern TypeInfo UpdateType_t13_il2cpp_TypeInfo;
// UnityStandardAssets.Cameras.AbstractTargetFollower/UpdateType
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Cameras_AbMethodDeclarations.h"
static const MethodInfo* UpdateType_t13_MethodInfos[] =
{
	NULL
};
extern const MethodInfo Enum_Equals_m1051_MethodInfo;
extern const MethodInfo Enum_GetHashCode_m1052_MethodInfo;
extern const MethodInfo Enum_ToString_m1053_MethodInfo;
extern const MethodInfo Enum_ToString_m1054_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToBoolean_m1055_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToByte_m1056_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToChar_m1057_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDateTime_m1058_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDecimal_m1059_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDouble_m1060_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt16_m1061_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt32_m1062_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt64_m1063_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToSByte_m1064_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToSingle_m1065_MethodInfo;
extern const MethodInfo Enum_ToString_m1066_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToType_m1067_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt16_m1068_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt32_m1069_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt64_m1070_MethodInfo;
extern const MethodInfo Enum_CompareTo_m1071_MethodInfo;
extern const MethodInfo Enum_GetTypeCode_m1072_MethodInfo;
static const Il2CppMethodReference UpdateType_t13_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool UpdateType_t13_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IFormattable_t275_0_0_0;
extern const Il2CppType IConvertible_t276_0_0_0;
extern const Il2CppType IComparable_t277_0_0_0;
static Il2CppInterfaceOffsetPair UpdateType_t13_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType UpdateType_t13_0_0_0;
extern const Il2CppType UpdateType_t13_1_0_0;
extern const Il2CppType Enum_t278_0_0_0;
extern TypeInfo AbstractTargetFollower_t15_il2cpp_TypeInfo;
extern const Il2CppType AbstractTargetFollower_t15_0_0_0;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t253_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata UpdateType_t13_DefinitionMetadata = 
{
	&AbstractTargetFollower_t15_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UpdateType_t13_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, UpdateType_t13_VTable/* vtableMethods */
	, UpdateType_t13_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 31/* fieldStart */

};
TypeInfo UpdateType_t13_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "UpdateType"/* name */
	, ""/* namespaze */
	, UpdateType_t13_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UpdateType_t13_0_0_0/* byval_arg */
	, &UpdateType_t13_1_0_0/* this_arg */
	, &UpdateType_t13_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UpdateType_t13)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UpdateType_t13)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityStandardAssets.Cameras.AbstractTargetFollower
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Cameras_Ab_0.h"
// Metadata Definition UnityStandardAssets.Cameras.AbstractTargetFollower
// UnityStandardAssets.Cameras.AbstractTargetFollower
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Cameras_Ab_0MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::.ctor()
extern const MethodInfo AbstractTargetFollower__ctor_m20_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AbstractTargetFollower__ctor_m20/* method */
	, &AbstractTargetFollower_t15_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 21/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::Start()
extern const MethodInfo AbstractTargetFollower_Start_m21_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&AbstractTargetFollower_Start_m21/* method */
	, &AbstractTargetFollower_t15_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 22/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::FixedUpdate()
extern const MethodInfo AbstractTargetFollower_FixedUpdate_m22_MethodInfo = 
{
	"FixedUpdate"/* name */
	, (methodPointerType)&AbstractTargetFollower_FixedUpdate_m22/* method */
	, &AbstractTargetFollower_t15_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 23/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::LateUpdate()
extern const MethodInfo AbstractTargetFollower_LateUpdate_m23_MethodInfo = 
{
	"LateUpdate"/* name */
	, (methodPointerType)&AbstractTargetFollower_LateUpdate_m23/* method */
	, &AbstractTargetFollower_t15_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 24/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::ManualUpdate()
extern const MethodInfo AbstractTargetFollower_ManualUpdate_m24_MethodInfo = 
{
	"ManualUpdate"/* name */
	, (methodPointerType)&AbstractTargetFollower_ManualUpdate_m24/* method */
	, &AbstractTargetFollower_t15_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 25/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo AbstractTargetFollower_t15_AbstractTargetFollower_FollowTarget_m1026_ParameterInfos[] = 
{
	{"deltaTime", 0, 134217733, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::FollowTarget(System.Single)
extern const MethodInfo AbstractTargetFollower_FollowTarget_m1026_MethodInfo = 
{
	"FollowTarget"/* name */
	, NULL/* method */
	, &AbstractTargetFollower_t15_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Single_t254/* invoker_method */
	, AbstractTargetFollower_t15_AbstractTargetFollower_FollowTarget_m1026_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1476/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 26/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::FindAndTargetPlayer()
extern const MethodInfo AbstractTargetFollower_FindAndTargetPlayer_m25_MethodInfo = 
{
	"FindAndTargetPlayer"/* name */
	, (methodPointerType)&AbstractTargetFollower_FindAndTargetPlayer_m25/* method */
	, &AbstractTargetFollower_t15_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 27/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Transform_t1_0_0_0;
extern const Il2CppType Transform_t1_0_0_0;
static const ParameterInfo AbstractTargetFollower_t15_AbstractTargetFollower_SetTarget_m26_ParameterInfos[] = 
{
	{"newTransform", 0, 134217734, 0, &Transform_t1_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::SetTarget(UnityEngine.Transform)
extern const MethodInfo AbstractTargetFollower_SetTarget_m26_MethodInfo = 
{
	"SetTarget"/* name */
	, (methodPointerType)&AbstractTargetFollower_SetTarget_m26/* method */
	, &AbstractTargetFollower_t15_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, AbstractTargetFollower_t15_AbstractTargetFollower_SetTarget_m26_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 28/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Transform UnityStandardAssets.Cameras.AbstractTargetFollower::get_Target()
extern const MethodInfo AbstractTargetFollower_get_Target_m27_MethodInfo = 
{
	"get_Target"/* name */
	, (methodPointerType)&AbstractTargetFollower_get_Target_m27/* method */
	, &AbstractTargetFollower_t15_il2cpp_TypeInfo/* declaring_type */
	, &Transform_t1_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 29/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AbstractTargetFollower_t15_MethodInfos[] =
{
	&AbstractTargetFollower__ctor_m20_MethodInfo,
	&AbstractTargetFollower_Start_m21_MethodInfo,
	&AbstractTargetFollower_FixedUpdate_m22_MethodInfo,
	&AbstractTargetFollower_LateUpdate_m23_MethodInfo,
	&AbstractTargetFollower_ManualUpdate_m24_MethodInfo,
	&AbstractTargetFollower_FollowTarget_m1026_MethodInfo,
	&AbstractTargetFollower_FindAndTargetPlayer_m25_MethodInfo,
	&AbstractTargetFollower_SetTarget_m26_MethodInfo,
	&AbstractTargetFollower_get_Target_m27_MethodInfo,
	NULL
};
extern const MethodInfo AbstractTargetFollower_get_Target_m27_MethodInfo;
static const PropertyInfo AbstractTargetFollower_t15____Target_PropertyInfo = 
{
	&AbstractTargetFollower_t15_il2cpp_TypeInfo/* parent */
	, "Target"/* name */
	, &AbstractTargetFollower_get_Target_m27_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* AbstractTargetFollower_t15_PropertyInfos[] =
{
	&AbstractTargetFollower_t15____Target_PropertyInfo,
	NULL
};
static const Il2CppType* AbstractTargetFollower_t15_il2cpp_TypeInfo__nestedTypes[1] =
{
	&UpdateType_t13_0_0_0,
};
extern const MethodInfo AbstractTargetFollower_Start_m21_MethodInfo;
extern const MethodInfo AbstractTargetFollower_SetTarget_m26_MethodInfo;
static const Il2CppMethodReference AbstractTargetFollower_t15_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	&AbstractTargetFollower_Start_m21_MethodInfo,
	NULL,
	&AbstractTargetFollower_SetTarget_m26_MethodInfo,
};
static bool AbstractTargetFollower_t15_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType AbstractTargetFollower_t15_1_0_0;
struct AbstractTargetFollower_t15;
const Il2CppTypeDefinitionMetadata AbstractTargetFollower_t15_DefinitionMetadata = 
{
	NULL/* declaringType */
	, AbstractTargetFollower_t15_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, AbstractTargetFollower_t15_VTable/* vtableMethods */
	, AbstractTargetFollower_t15_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 35/* fieldStart */

};
TypeInfo AbstractTargetFollower_t15_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "AbstractTargetFollower"/* name */
	, "UnityStandardAssets.Cameras"/* namespaze */
	, AbstractTargetFollower_t15_MethodInfos/* methods */
	, AbstractTargetFollower_t15_PropertyInfos/* properties */
	, NULL/* events */
	, &AbstractTargetFollower_t15_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AbstractTargetFollower_t15_0_0_0/* byval_arg */
	, &AbstractTargetFollower_t15_1_0_0/* this_arg */
	, &AbstractTargetFollower_t15_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AbstractTargetFollower_t15)/* instance_size */
	, sizeof (AbstractTargetFollower_t15)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 1/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 7/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.Cameras.AutoCam
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Cameras_Au.h"
// Metadata Definition UnityStandardAssets.Cameras.AutoCam
extern TypeInfo AutoCam_t16_il2cpp_TypeInfo;
// UnityStandardAssets.Cameras.AutoCam
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Cameras_AuMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Cameras.AutoCam::.ctor()
extern const MethodInfo AutoCam__ctor_m28_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AutoCam__ctor_m28/* method */
	, &AutoCam_t16_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 30/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo AutoCam_t16_AutoCam_FollowTarget_m29_ParameterInfos[] = 
{
	{"deltaTime", 0, 134217735, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Cameras.AutoCam::FollowTarget(System.Single)
extern const MethodInfo AutoCam_FollowTarget_m29_MethodInfo = 
{
	"FollowTarget"/* name */
	, (methodPointerType)&AutoCam_FollowTarget_m29/* method */
	, &AutoCam_t16_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Single_t254/* invoker_method */
	, AutoCam_t16_AutoCam_FollowTarget_m29_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 31/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AutoCam_t16_MethodInfos[] =
{
	&AutoCam__ctor_m28_MethodInfo,
	&AutoCam_FollowTarget_m29_MethodInfo,
	NULL
};
extern const MethodInfo AutoCam_FollowTarget_m29_MethodInfo;
extern const MethodInfo PivotBasedCameraRig_Awake_m42_MethodInfo;
static const Il2CppMethodReference AutoCam_t16_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	&AbstractTargetFollower_Start_m21_MethodInfo,
	&AutoCam_FollowTarget_m29_MethodInfo,
	&AbstractTargetFollower_SetTarget_m26_MethodInfo,
	&PivotBasedCameraRig_Awake_m42_MethodInfo,
};
static bool AutoCam_t16_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType AutoCam_t16_0_0_0;
extern const Il2CppType AutoCam_t16_1_0_0;
extern const Il2CppType PivotBasedCameraRig_t17_0_0_0;
struct AutoCam_t16;
const Il2CppTypeDefinitionMetadata AutoCam_t16_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &PivotBasedCameraRig_t17_0_0_0/* parent */
	, AutoCam_t16_VTable/* vtableMethods */
	, AutoCam_t16_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 39/* fieldStart */

};
TypeInfo AutoCam_t16_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "AutoCam"/* name */
	, "UnityStandardAssets.Cameras"/* namespaze */
	, AutoCam_t16_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &AutoCam_t16_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 11/* custom_attributes_cache */
	, &AutoCam_t16_0_0_0/* byval_arg */
	, &AutoCam_t16_1_0_0/* this_arg */
	, &AutoCam_t16_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AutoCam_t16)/* instance_size */
	, sizeof (AutoCam_t16)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.Cameras.FreeLookCam
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Cameras_Fr.h"
// Metadata Definition UnityStandardAssets.Cameras.FreeLookCam
extern TypeInfo FreeLookCam_t18_il2cpp_TypeInfo;
// UnityStandardAssets.Cameras.FreeLookCam
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Cameras_FrMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Cameras.FreeLookCam::.ctor()
extern const MethodInfo FreeLookCam__ctor_m30_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&FreeLookCam__ctor_m30/* method */
	, &FreeLookCam_t18_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 32/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Cameras.FreeLookCam::Awake()
extern const MethodInfo FreeLookCam_Awake_m31_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&FreeLookCam_Awake_m31/* method */
	, &FreeLookCam_t18_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 33/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Cameras.FreeLookCam::Update()
extern const MethodInfo FreeLookCam_Update_m32_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&FreeLookCam_Update_m32/* method */
	, &FreeLookCam_t18_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 34/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Cameras.FreeLookCam::OnDisable()
extern const MethodInfo FreeLookCam_OnDisable_m33_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&FreeLookCam_OnDisable_m33/* method */
	, &FreeLookCam_t18_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 35/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo FreeLookCam_t18_FreeLookCam_FollowTarget_m34_ParameterInfos[] = 
{
	{"deltaTime", 0, 134217736, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Cameras.FreeLookCam::FollowTarget(System.Single)
extern const MethodInfo FreeLookCam_FollowTarget_m34_MethodInfo = 
{
	"FollowTarget"/* name */
	, (methodPointerType)&FreeLookCam_FollowTarget_m34/* method */
	, &FreeLookCam_t18_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Single_t254/* invoker_method */
	, FreeLookCam_t18_FreeLookCam_FollowTarget_m34_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 36/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Cameras.FreeLookCam::HandleRotationMovement()
extern const MethodInfo FreeLookCam_HandleRotationMovement_m35_MethodInfo = 
{
	"HandleRotationMovement"/* name */
	, (methodPointerType)&FreeLookCam_HandleRotationMovement_m35/* method */
	, &FreeLookCam_t18_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 37/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* FreeLookCam_t18_MethodInfos[] =
{
	&FreeLookCam__ctor_m30_MethodInfo,
	&FreeLookCam_Awake_m31_MethodInfo,
	&FreeLookCam_Update_m32_MethodInfo,
	&FreeLookCam_OnDisable_m33_MethodInfo,
	&FreeLookCam_FollowTarget_m34_MethodInfo,
	&FreeLookCam_HandleRotationMovement_m35_MethodInfo,
	NULL
};
extern const MethodInfo FreeLookCam_FollowTarget_m34_MethodInfo;
extern const MethodInfo FreeLookCam_Awake_m31_MethodInfo;
static const Il2CppMethodReference FreeLookCam_t18_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	&AbstractTargetFollower_Start_m21_MethodInfo,
	&FreeLookCam_FollowTarget_m34_MethodInfo,
	&AbstractTargetFollower_SetTarget_m26_MethodInfo,
	&FreeLookCam_Awake_m31_MethodInfo,
};
static bool FreeLookCam_t18_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType FreeLookCam_t18_0_0_0;
extern const Il2CppType FreeLookCam_t18_1_0_0;
struct FreeLookCam_t18;
const Il2CppTypeDefinitionMetadata FreeLookCam_t18_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &PivotBasedCameraRig_t17_0_0_0/* parent */
	, FreeLookCam_t18_VTable/* vtableMethods */
	, FreeLookCam_t18_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 51/* fieldStart */

};
TypeInfo FreeLookCam_t18_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "FreeLookCam"/* name */
	, "UnityStandardAssets.Cameras"/* namespaze */
	, FreeLookCam_t18_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &FreeLookCam_t18_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &FreeLookCam_t18_0_0_0/* byval_arg */
	, &FreeLookCam_t18_1_0_0/* this_arg */
	, &FreeLookCam_t18_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FreeLookCam_t18)/* instance_size */
	, sizeof (FreeLookCam_t18)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 13/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.Cameras.HandHeldCam
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Cameras_Ha.h"
// Metadata Definition UnityStandardAssets.Cameras.HandHeldCam
extern TypeInfo HandHeldCam_t20_il2cpp_TypeInfo;
// UnityStandardAssets.Cameras.HandHeldCam
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Cameras_HaMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Cameras.HandHeldCam::.ctor()
extern const MethodInfo HandHeldCam__ctor_m36_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&HandHeldCam__ctor_m36/* method */
	, &HandHeldCam_t20_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 38/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo HandHeldCam_t20_HandHeldCam_FollowTarget_m37_ParameterInfos[] = 
{
	{"deltaTime", 0, 134217737, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Cameras.HandHeldCam::FollowTarget(System.Single)
extern const MethodInfo HandHeldCam_FollowTarget_m37_MethodInfo = 
{
	"FollowTarget"/* name */
	, (methodPointerType)&HandHeldCam_FollowTarget_m37/* method */
	, &HandHeldCam_t20_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Single_t254/* invoker_method */
	, HandHeldCam_t20_HandHeldCam_FollowTarget_m37_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 39/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* HandHeldCam_t20_MethodInfos[] =
{
	&HandHeldCam__ctor_m36_MethodInfo,
	&HandHeldCam_FollowTarget_m37_MethodInfo,
	NULL
};
extern const MethodInfo LookatTarget_Start_m39_MethodInfo;
extern const MethodInfo HandHeldCam_FollowTarget_m37_MethodInfo;
static const Il2CppMethodReference HandHeldCam_t20_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	&LookatTarget_Start_m39_MethodInfo,
	&HandHeldCam_FollowTarget_m37_MethodInfo,
	&AbstractTargetFollower_SetTarget_m26_MethodInfo,
};
static bool HandHeldCam_t20_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType HandHeldCam_t20_0_0_0;
extern const Il2CppType HandHeldCam_t20_1_0_0;
extern const Il2CppType LookatTarget_t21_0_0_0;
struct HandHeldCam_t20;
const Il2CppTypeDefinitionMetadata HandHeldCam_t20_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &LookatTarget_t21_0_0_0/* parent */
	, HandHeldCam_t20_VTable/* vtableMethods */
	, HandHeldCam_t20_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 64/* fieldStart */

};
TypeInfo HandHeldCam_t20_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "HandHeldCam"/* name */
	, "UnityStandardAssets.Cameras"/* namespaze */
	, HandHeldCam_t20_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &HandHeldCam_t20_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &HandHeldCam_t20_0_0_0/* byval_arg */
	, &HandHeldCam_t20_1_0_0/* this_arg */
	, &HandHeldCam_t20_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HandHeldCam_t20)/* instance_size */
	, sizeof (HandHeldCam_t20)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.Cameras.LookatTarget
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Cameras_Lo.h"
// Metadata Definition UnityStandardAssets.Cameras.LookatTarget
extern TypeInfo LookatTarget_t21_il2cpp_TypeInfo;
// UnityStandardAssets.Cameras.LookatTarget
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Cameras_LoMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Cameras.LookatTarget::.ctor()
extern const MethodInfo LookatTarget__ctor_m38_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&LookatTarget__ctor_m38/* method */
	, &LookatTarget_t21_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 40/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Cameras.LookatTarget::Start()
extern const MethodInfo LookatTarget_Start_m39_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&LookatTarget_Start_m39/* method */
	, &LookatTarget_t21_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 41/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo LookatTarget_t21_LookatTarget_FollowTarget_m40_ParameterInfos[] = 
{
	{"deltaTime", 0, 134217738, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Cameras.LookatTarget::FollowTarget(System.Single)
extern const MethodInfo LookatTarget_FollowTarget_m40_MethodInfo = 
{
	"FollowTarget"/* name */
	, (methodPointerType)&LookatTarget_FollowTarget_m40/* method */
	, &LookatTarget_t21_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Single_t254/* invoker_method */
	, LookatTarget_t21_LookatTarget_FollowTarget_m40_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 42/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* LookatTarget_t21_MethodInfos[] =
{
	&LookatTarget__ctor_m38_MethodInfo,
	&LookatTarget_Start_m39_MethodInfo,
	&LookatTarget_FollowTarget_m40_MethodInfo,
	NULL
};
extern const MethodInfo LookatTarget_FollowTarget_m40_MethodInfo;
static const Il2CppMethodReference LookatTarget_t21_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	&LookatTarget_Start_m39_MethodInfo,
	&LookatTarget_FollowTarget_m40_MethodInfo,
	&AbstractTargetFollower_SetTarget_m26_MethodInfo,
};
static bool LookatTarget_t21_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType LookatTarget_t21_1_0_0;
struct LookatTarget_t21;
const Il2CppTypeDefinitionMetadata LookatTarget_t21_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &AbstractTargetFollower_t15_0_0_0/* parent */
	, LookatTarget_t21_VTable/* vtableMethods */
	, LookatTarget_t21_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 68/* fieldStart */

};
TypeInfo LookatTarget_t21_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "LookatTarget"/* name */
	, "UnityStandardAssets.Cameras"/* namespaze */
	, LookatTarget_t21_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &LookatTarget_t21_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &LookatTarget_t21_0_0_0/* byval_arg */
	, &LookatTarget_t21_1_0_0/* this_arg */
	, &LookatTarget_t21_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LookatTarget_t21)/* instance_size */
	, sizeof (LookatTarget_t21)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.Cameras.PivotBasedCameraRig
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Cameras_Pi.h"
// Metadata Definition UnityStandardAssets.Cameras.PivotBasedCameraRig
extern TypeInfo PivotBasedCameraRig_t17_il2cpp_TypeInfo;
// UnityStandardAssets.Cameras.PivotBasedCameraRig
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Cameras_PiMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Cameras.PivotBasedCameraRig::.ctor()
extern const MethodInfo PivotBasedCameraRig__ctor_m41_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PivotBasedCameraRig__ctor_m41/* method */
	, &PivotBasedCameraRig_t17_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 43/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Cameras.PivotBasedCameraRig::Awake()
extern const MethodInfo PivotBasedCameraRig_Awake_m42_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&PivotBasedCameraRig_Awake_m42/* method */
	, &PivotBasedCameraRig_t17_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 44/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* PivotBasedCameraRig_t17_MethodInfos[] =
{
	&PivotBasedCameraRig__ctor_m41_MethodInfo,
	&PivotBasedCameraRig_Awake_m42_MethodInfo,
	NULL
};
static const Il2CppMethodReference PivotBasedCameraRig_t17_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	&AbstractTargetFollower_Start_m21_MethodInfo,
	NULL,
	&AbstractTargetFollower_SetTarget_m26_MethodInfo,
	&PivotBasedCameraRig_Awake_m42_MethodInfo,
};
static bool PivotBasedCameraRig_t17_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType PivotBasedCameraRig_t17_1_0_0;
struct PivotBasedCameraRig_t17;
const Il2CppTypeDefinitionMetadata PivotBasedCameraRig_t17_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &AbstractTargetFollower_t15_0_0_0/* parent */
	, PivotBasedCameraRig_t17_VTable/* vtableMethods */
	, PivotBasedCameraRig_t17_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 73/* fieldStart */

};
TypeInfo PivotBasedCameraRig_t17_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "PivotBasedCameraRig"/* name */
	, "UnityStandardAssets.Cameras"/* namespaze */
	, PivotBasedCameraRig_t17_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &PivotBasedCameraRig_t17_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PivotBasedCameraRig_t17_0_0_0/* byval_arg */
	, &PivotBasedCameraRig_t17_1_0_0/* this_arg */
	, &PivotBasedCameraRig_t17_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PivotBasedCameraRig_t17)/* instance_size */
	, sizeof (PivotBasedCameraRig_t17)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.Cameras.ProtectCameraFromWallClip/RayHitComparer
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Cameras_Pr.h"
// Metadata Definition UnityStandardAssets.Cameras.ProtectCameraFromWallClip/RayHitComparer
extern TypeInfo RayHitComparer_t22_il2cpp_TypeInfo;
// UnityStandardAssets.Cameras.ProtectCameraFromWallClip/RayHitComparer
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Cameras_PrMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Cameras.ProtectCameraFromWallClip/RayHitComparer::.ctor()
extern const MethodInfo RayHitComparer__ctor_m43_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RayHitComparer__ctor_m43/* method */
	, &RayHitComparer_t22_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 50/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo RayHitComparer_t22_RayHitComparer_Compare_m44_ParameterInfos[] = 
{
	{"x", 0, 134217740, 0, &Object_t_0_0_0},
	{"y", 1, 134217741, 0, &Object_t_0_0_0},
};
extern const Il2CppType Int32_t253_0_0_0;
extern void* RuntimeInvoker_Int32_t253_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityStandardAssets.Cameras.ProtectCameraFromWallClip/RayHitComparer::Compare(System.Object,System.Object)
extern const MethodInfo RayHitComparer_Compare_m44_MethodInfo = 
{
	"Compare"/* name */
	, (methodPointerType)&RayHitComparer_Compare_m44/* method */
	, &RayHitComparer_t22_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253_Object_t_Object_t/* invoker_method */
	, RayHitComparer_t22_RayHitComparer_Compare_m44_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 51/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* RayHitComparer_t22_MethodInfos[] =
{
	&RayHitComparer__ctor_m43_MethodInfo,
	&RayHitComparer_Compare_m44_MethodInfo,
	NULL
};
extern const MethodInfo Object_Equals_m1073_MethodInfo;
extern const MethodInfo Object_GetHashCode_m1074_MethodInfo;
extern const MethodInfo Object_ToString_m1075_MethodInfo;
extern const MethodInfo RayHitComparer_Compare_m44_MethodInfo;
static const Il2CppMethodReference RayHitComparer_t22_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&RayHitComparer_Compare_m44_MethodInfo,
};
static bool RayHitComparer_t22_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IComparer_t279_0_0_0;
static const Il2CppType* RayHitComparer_t22_InterfacesTypeInfos[] = 
{
	&IComparer_t279_0_0_0,
};
static Il2CppInterfaceOffsetPair RayHitComparer_t22_InterfacesOffsets[] = 
{
	{ &IComparer_t279_0_0_0, 4},
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType RayHitComparer_t22_0_0_0;
extern const Il2CppType RayHitComparer_t22_1_0_0;
extern TypeInfo ProtectCameraFromWallClip_t25_il2cpp_TypeInfo;
extern const Il2CppType ProtectCameraFromWallClip_t25_0_0_0;
struct RayHitComparer_t22;
const Il2CppTypeDefinitionMetadata RayHitComparer_t22_DefinitionMetadata = 
{
	&ProtectCameraFromWallClip_t25_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, RayHitComparer_t22_InterfacesTypeInfos/* implementedInterfaces */
	, RayHitComparer_t22_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, RayHitComparer_t22_VTable/* vtableMethods */
	, RayHitComparer_t22_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo RayHitComparer_t22_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "RayHitComparer"/* name */
	, ""/* namespaze */
	, RayHitComparer_t22_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &RayHitComparer_t22_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RayHitComparer_t22_0_0_0/* byval_arg */
	, &RayHitComparer_t22_1_0_0/* this_arg */
	, &RayHitComparer_t22_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RayHitComparer_t22)/* instance_size */
	, sizeof (RayHitComparer_t22)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048578/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityStandardAssets.Cameras.ProtectCameraFromWallClip
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Cameras_Pr_0.h"
// Metadata Definition UnityStandardAssets.Cameras.ProtectCameraFromWallClip
// UnityStandardAssets.Cameras.ProtectCameraFromWallClip
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Cameras_Pr_0MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Cameras.ProtectCameraFromWallClip::.ctor()
extern const MethodInfo ProtectCameraFromWallClip__ctor_m45_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ProtectCameraFromWallClip__ctor_m45/* method */
	, &ProtectCameraFromWallClip_t25_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 45/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityStandardAssets.Cameras.ProtectCameraFromWallClip::get_protecting()
extern const MethodInfo ProtectCameraFromWallClip_get_protecting_m46_MethodInfo = 
{
	"get_protecting"/* name */
	, (methodPointerType)&ProtectCameraFromWallClip_get_protecting_m46/* method */
	, &ProtectCameraFromWallClip_t25_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 34/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 46/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo ProtectCameraFromWallClip_t25_ProtectCameraFromWallClip_set_protecting_m47_ParameterInfos[] = 
{
	{"value", 0, 134217739, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Cameras.ProtectCameraFromWallClip::set_protecting(System.Boolean)
extern const MethodInfo ProtectCameraFromWallClip_set_protecting_m47_MethodInfo = 
{
	"set_protecting"/* name */
	, (methodPointerType)&ProtectCameraFromWallClip_set_protecting_m47/* method */
	, &ProtectCameraFromWallClip_t25_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274/* invoker_method */
	, ProtectCameraFromWallClip_t25_ProtectCameraFromWallClip_set_protecting_m47_ParameterInfos/* parameters */
	, 35/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 47/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Cameras.ProtectCameraFromWallClip::Start()
extern const MethodInfo ProtectCameraFromWallClip_Start_m48_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&ProtectCameraFromWallClip_Start_m48/* method */
	, &ProtectCameraFromWallClip_t25_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 48/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Cameras.ProtectCameraFromWallClip::LateUpdate()
extern const MethodInfo ProtectCameraFromWallClip_LateUpdate_m49_MethodInfo = 
{
	"LateUpdate"/* name */
	, (methodPointerType)&ProtectCameraFromWallClip_LateUpdate_m49/* method */
	, &ProtectCameraFromWallClip_t25_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 49/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ProtectCameraFromWallClip_t25_MethodInfos[] =
{
	&ProtectCameraFromWallClip__ctor_m45_MethodInfo,
	&ProtectCameraFromWallClip_get_protecting_m46_MethodInfo,
	&ProtectCameraFromWallClip_set_protecting_m47_MethodInfo,
	&ProtectCameraFromWallClip_Start_m48_MethodInfo,
	&ProtectCameraFromWallClip_LateUpdate_m49_MethodInfo,
	NULL
};
extern const MethodInfo ProtectCameraFromWallClip_get_protecting_m46_MethodInfo;
extern const MethodInfo ProtectCameraFromWallClip_set_protecting_m47_MethodInfo;
static const PropertyInfo ProtectCameraFromWallClip_t25____protecting_PropertyInfo = 
{
	&ProtectCameraFromWallClip_t25_il2cpp_TypeInfo/* parent */
	, "protecting"/* name */
	, &ProtectCameraFromWallClip_get_protecting_m46_MethodInfo/* get */
	, &ProtectCameraFromWallClip_set_protecting_m47_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ProtectCameraFromWallClip_t25_PropertyInfos[] =
{
	&ProtectCameraFromWallClip_t25____protecting_PropertyInfo,
	NULL
};
static const Il2CppType* ProtectCameraFromWallClip_t25_il2cpp_TypeInfo__nestedTypes[1] =
{
	&RayHitComparer_t22_0_0_0,
};
static const Il2CppMethodReference ProtectCameraFromWallClip_t25_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool ProtectCameraFromWallClip_t25_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType ProtectCameraFromWallClip_t25_1_0_0;
struct ProtectCameraFromWallClip_t25;
const Il2CppTypeDefinitionMetadata ProtectCameraFromWallClip_t25_DefinitionMetadata = 
{
	NULL/* declaringType */
	, ProtectCameraFromWallClip_t25_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, ProtectCameraFromWallClip_t25_VTable/* vtableMethods */
	, ProtectCameraFromWallClip_t25_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 76/* fieldStart */

};
TypeInfo ProtectCameraFromWallClip_t25_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "ProtectCameraFromWallClip"/* name */
	, "UnityStandardAssets.Cameras"/* namespaze */
	, ProtectCameraFromWallClip_t25_MethodInfos/* methods */
	, ProtectCameraFromWallClip_t25_PropertyInfos/* properties */
	, NULL/* events */
	, &ProtectCameraFromWallClip_t25_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ProtectCameraFromWallClip_t25_0_0_0/* byval_arg */
	, &ProtectCameraFromWallClip_t25_1_0_0/* this_arg */
	, &ProtectCameraFromWallClip_t25_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ProtectCameraFromWallClip_t25)/* instance_size */
	, sizeof (ProtectCameraFromWallClip_t25)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 15/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.Cameras.TargetFieldOfView
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Cameras_Ta.h"
// Metadata Definition UnityStandardAssets.Cameras.TargetFieldOfView
extern TypeInfo TargetFieldOfView_t28_il2cpp_TypeInfo;
// UnityStandardAssets.Cameras.TargetFieldOfView
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Cameras_TaMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Cameras.TargetFieldOfView::.ctor()
extern const MethodInfo TargetFieldOfView__ctor_m50_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TargetFieldOfView__ctor_m50/* method */
	, &TargetFieldOfView_t28_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 52/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Cameras.TargetFieldOfView::Start()
extern const MethodInfo TargetFieldOfView_Start_m51_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&TargetFieldOfView_Start_m51/* method */
	, &TargetFieldOfView_t28_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 53/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo TargetFieldOfView_t28_TargetFieldOfView_FollowTarget_m52_ParameterInfos[] = 
{
	{"deltaTime", 0, 134217742, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Cameras.TargetFieldOfView::FollowTarget(System.Single)
extern const MethodInfo TargetFieldOfView_FollowTarget_m52_MethodInfo = 
{
	"FollowTarget"/* name */
	, (methodPointerType)&TargetFieldOfView_FollowTarget_m52/* method */
	, &TargetFieldOfView_t28_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Single_t254/* invoker_method */
	, TargetFieldOfView_t28_TargetFieldOfView_FollowTarget_m52_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 54/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Transform_t1_0_0_0;
static const ParameterInfo TargetFieldOfView_t28_TargetFieldOfView_SetTarget_m53_ParameterInfos[] = 
{
	{"newTransform", 0, 134217743, 0, &Transform_t1_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Cameras.TargetFieldOfView::SetTarget(UnityEngine.Transform)
extern const MethodInfo TargetFieldOfView_SetTarget_m53_MethodInfo = 
{
	"SetTarget"/* name */
	, (methodPointerType)&TargetFieldOfView_SetTarget_m53/* method */
	, &TargetFieldOfView_t28_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, TargetFieldOfView_t28_TargetFieldOfView_SetTarget_m53_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 55/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Transform_t1_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo TargetFieldOfView_t28_TargetFieldOfView_MaxBoundsExtent_m54_ParameterInfos[] = 
{
	{"obj", 0, 134217744, 0, &Transform_t1_0_0_0},
	{"includeEffects", 1, 134217745, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Single_t254_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityStandardAssets.Cameras.TargetFieldOfView::MaxBoundsExtent(UnityEngine.Transform,System.Boolean)
extern const MethodInfo TargetFieldOfView_MaxBoundsExtent_m54_MethodInfo = 
{
	"MaxBoundsExtent"/* name */
	, (methodPointerType)&TargetFieldOfView_MaxBoundsExtent_m54/* method */
	, &TargetFieldOfView_t28_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254_Object_t_SByte_t274/* invoker_method */
	, TargetFieldOfView_t28_TargetFieldOfView_MaxBoundsExtent_m54_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 56/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TargetFieldOfView_t28_MethodInfos[] =
{
	&TargetFieldOfView__ctor_m50_MethodInfo,
	&TargetFieldOfView_Start_m51_MethodInfo,
	&TargetFieldOfView_FollowTarget_m52_MethodInfo,
	&TargetFieldOfView_SetTarget_m53_MethodInfo,
	&TargetFieldOfView_MaxBoundsExtent_m54_MethodInfo,
	NULL
};
extern const MethodInfo TargetFieldOfView_Start_m51_MethodInfo;
extern const MethodInfo TargetFieldOfView_FollowTarget_m52_MethodInfo;
extern const MethodInfo TargetFieldOfView_SetTarget_m53_MethodInfo;
static const Il2CppMethodReference TargetFieldOfView_t28_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	&TargetFieldOfView_Start_m51_MethodInfo,
	&TargetFieldOfView_FollowTarget_m52_MethodInfo,
	&TargetFieldOfView_SetTarget_m53_MethodInfo,
};
static bool TargetFieldOfView_t28_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType TargetFieldOfView_t28_0_0_0;
extern const Il2CppType TargetFieldOfView_t28_1_0_0;
struct TargetFieldOfView_t28;
const Il2CppTypeDefinitionMetadata TargetFieldOfView_t28_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &AbstractTargetFollower_t15_0_0_0/* parent */
	, TargetFieldOfView_t28_VTable/* vtableMethods */
	, TargetFieldOfView_t28_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 91/* fieldStart */

};
TypeInfo TargetFieldOfView_t28_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "TargetFieldOfView"/* name */
	, "UnityStandardAssets.Cameras"/* namespaze */
	, TargetFieldOfView_t28_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TargetFieldOfView_t28_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TargetFieldOfView_t28_0_0_0/* byval_arg */
	, &TargetFieldOfView_t28_1_0_0/* this_arg */
	, &TargetFieldOfView_t28_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TargetFieldOfView_t28)/* instance_size */
	, sizeof (TargetFieldOfView_t28)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.CrossPlatformInput.AxisTouchButton
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf.h"
// Metadata Definition UnityStandardAssets.CrossPlatformInput.AxisTouchButton
extern TypeInfo AxisTouchButton_t29_il2cpp_TypeInfo;
// UnityStandardAssets.CrossPlatformInput.AxisTouchButton
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatfMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.AxisTouchButton::.ctor()
extern const MethodInfo AxisTouchButton__ctor_m55_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AxisTouchButton__ctor_m55/* method */
	, &AxisTouchButton_t29_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 57/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.AxisTouchButton::OnEnable()
extern const MethodInfo AxisTouchButton_OnEnable_m56_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&AxisTouchButton_OnEnable_m56/* method */
	, &AxisTouchButton_t29_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 58/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.AxisTouchButton::FindPairedButton()
extern const MethodInfo AxisTouchButton_FindPairedButton_m57_MethodInfo = 
{
	"FindPairedButton"/* name */
	, (methodPointerType)&AxisTouchButton_FindPairedButton_m57/* method */
	, &AxisTouchButton_t29_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 59/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.AxisTouchButton::OnDisable()
extern const MethodInfo AxisTouchButton_OnDisable_m58_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&AxisTouchButton_OnDisable_m58/* method */
	, &AxisTouchButton_t29_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 60/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t215_0_0_0;
extern const Il2CppType PointerEventData_t215_0_0_0;
static const ParameterInfo AxisTouchButton_t29_AxisTouchButton_OnPointerDown_m59_ParameterInfos[] = 
{
	{"data", 0, 134217746, 0, &PointerEventData_t215_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.AxisTouchButton::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern const MethodInfo AxisTouchButton_OnPointerDown_m59_MethodInfo = 
{
	"OnPointerDown"/* name */
	, (methodPointerType)&AxisTouchButton_OnPointerDown_m59/* method */
	, &AxisTouchButton_t29_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, AxisTouchButton_t29_AxisTouchButton_OnPointerDown_m59_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 61/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t215_0_0_0;
static const ParameterInfo AxisTouchButton_t29_AxisTouchButton_OnPointerUp_m60_ParameterInfos[] = 
{
	{"data", 0, 134217747, 0, &PointerEventData_t215_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.AxisTouchButton::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern const MethodInfo AxisTouchButton_OnPointerUp_m60_MethodInfo = 
{
	"OnPointerUp"/* name */
	, (methodPointerType)&AxisTouchButton_OnPointerUp_m60/* method */
	, &AxisTouchButton_t29_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, AxisTouchButton_t29_AxisTouchButton_OnPointerUp_m60_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 62/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AxisTouchButton_t29_MethodInfos[] =
{
	&AxisTouchButton__ctor_m55_MethodInfo,
	&AxisTouchButton_OnEnable_m56_MethodInfo,
	&AxisTouchButton_FindPairedButton_m57_MethodInfo,
	&AxisTouchButton_OnDisable_m58_MethodInfo,
	&AxisTouchButton_OnPointerDown_m59_MethodInfo,
	&AxisTouchButton_OnPointerUp_m60_MethodInfo,
	NULL
};
extern const MethodInfo AxisTouchButton_OnPointerDown_m59_MethodInfo;
extern const MethodInfo AxisTouchButton_OnPointerUp_m60_MethodInfo;
static const Il2CppMethodReference AxisTouchButton_t29_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	&AxisTouchButton_OnPointerDown_m59_MethodInfo,
	&AxisTouchButton_OnPointerUp_m60_MethodInfo,
};
static bool AxisTouchButton_t29_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IPointerDownHandler_t280_0_0_0;
extern const Il2CppType IEventSystemHandler_t281_0_0_0;
extern const Il2CppType IPointerUpHandler_t282_0_0_0;
static const Il2CppType* AxisTouchButton_t29_InterfacesTypeInfos[] = 
{
	&IPointerDownHandler_t280_0_0_0,
	&IEventSystemHandler_t281_0_0_0,
	&IPointerUpHandler_t282_0_0_0,
};
static Il2CppInterfaceOffsetPair AxisTouchButton_t29_InterfacesOffsets[] = 
{
	{ &IPointerDownHandler_t280_0_0_0, 4},
	{ &IEventSystemHandler_t281_0_0_0, 5},
	{ &IPointerUpHandler_t282_0_0_0, 5},
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType AxisTouchButton_t29_0_0_0;
extern const Il2CppType AxisTouchButton_t29_1_0_0;
struct AxisTouchButton_t29;
const Il2CppTypeDefinitionMetadata AxisTouchButton_t29_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, AxisTouchButton_t29_InterfacesTypeInfos/* implementedInterfaces */
	, AxisTouchButton_t29_InterfacesOffsets/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, AxisTouchButton_t29_VTable/* vtableMethods */
	, AxisTouchButton_t29_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 98/* fieldStart */

};
TypeInfo AxisTouchButton_t29_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "AxisTouchButton"/* name */
	, "UnityStandardAssets.CrossPlatformInput"/* namespaze */
	, AxisTouchButton_t29_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &AxisTouchButton_t29_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AxisTouchButton_t29_0_0_0/* byval_arg */
	, &AxisTouchButton_t29_1_0_0/* this_arg */
	, &AxisTouchButton_t29_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AxisTouchButton_t29)/* instance_size */
	, sizeof (AxisTouchButton_t29)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityStandardAssets.CrossPlatformInput.ButtonHandler
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_0.h"
// Metadata Definition UnityStandardAssets.CrossPlatformInput.ButtonHandler
extern TypeInfo ButtonHandler_t31_il2cpp_TypeInfo;
// UnityStandardAssets.CrossPlatformInput.ButtonHandler
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_0MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.ButtonHandler::.ctor()
extern const MethodInfo ButtonHandler__ctor_m61_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ButtonHandler__ctor_m61/* method */
	, &ButtonHandler_t31_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 63/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.ButtonHandler::OnEnable()
extern const MethodInfo ButtonHandler_OnEnable_m62_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&ButtonHandler_OnEnable_m62/* method */
	, &ButtonHandler_t31_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 64/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.ButtonHandler::SetDownState()
extern const MethodInfo ButtonHandler_SetDownState_m63_MethodInfo = 
{
	"SetDownState"/* name */
	, (methodPointerType)&ButtonHandler_SetDownState_m63/* method */
	, &ButtonHandler_t31_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 65/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.ButtonHandler::SetUpState()
extern const MethodInfo ButtonHandler_SetUpState_m64_MethodInfo = 
{
	"SetUpState"/* name */
	, (methodPointerType)&ButtonHandler_SetUpState_m64/* method */
	, &ButtonHandler_t31_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 66/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.ButtonHandler::SetAxisPositiveState()
extern const MethodInfo ButtonHandler_SetAxisPositiveState_m65_MethodInfo = 
{
	"SetAxisPositiveState"/* name */
	, (methodPointerType)&ButtonHandler_SetAxisPositiveState_m65/* method */
	, &ButtonHandler_t31_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 67/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.ButtonHandler::SetAxisNeutralState()
extern const MethodInfo ButtonHandler_SetAxisNeutralState_m66_MethodInfo = 
{
	"SetAxisNeutralState"/* name */
	, (methodPointerType)&ButtonHandler_SetAxisNeutralState_m66/* method */
	, &ButtonHandler_t31_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 68/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.ButtonHandler::SetAxisNegativeState()
extern const MethodInfo ButtonHandler_SetAxisNegativeState_m67_MethodInfo = 
{
	"SetAxisNegativeState"/* name */
	, (methodPointerType)&ButtonHandler_SetAxisNegativeState_m67/* method */
	, &ButtonHandler_t31_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 69/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.ButtonHandler::Update()
extern const MethodInfo ButtonHandler_Update_m68_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&ButtonHandler_Update_m68/* method */
	, &ButtonHandler_t31_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 70/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ButtonHandler_t31_MethodInfos[] =
{
	&ButtonHandler__ctor_m61_MethodInfo,
	&ButtonHandler_OnEnable_m62_MethodInfo,
	&ButtonHandler_SetDownState_m63_MethodInfo,
	&ButtonHandler_SetUpState_m64_MethodInfo,
	&ButtonHandler_SetAxisPositiveState_m65_MethodInfo,
	&ButtonHandler_SetAxisNeutralState_m66_MethodInfo,
	&ButtonHandler_SetAxisNegativeState_m67_MethodInfo,
	&ButtonHandler_Update_m68_MethodInfo,
	NULL
};
static const Il2CppMethodReference ButtonHandler_t31_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool ButtonHandler_t31_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType ButtonHandler_t31_0_0_0;
extern const Il2CppType ButtonHandler_t31_1_0_0;
struct ButtonHandler_t31;
const Il2CppTypeDefinitionMetadata ButtonHandler_t31_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, ButtonHandler_t31_VTable/* vtableMethods */
	, ButtonHandler_t31_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 104/* fieldStart */

};
TypeInfo ButtonHandler_t31_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "ButtonHandler"/* name */
	, "UnityStandardAssets.CrossPlatformInput"/* namespaze */
	, ButtonHandler_t31_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ButtonHandler_t31_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ButtonHandler_t31_0_0_0/* byval_arg */
	, &ButtonHandler_t31_1_0_0/* this_arg */
	, &ButtonHandler_t31_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ButtonHandler_t31)/* instance_size */
	, sizeof (ButtonHandler_t31)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/ActiveInputMethod
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_1.h"
// Metadata Definition UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/ActiveInputMethod
extern TypeInfo ActiveInputMethod_t32_il2cpp_TypeInfo;
// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/ActiveInputMethod
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_1MethodDeclarations.h"
static const MethodInfo* ActiveInputMethod_t32_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference ActiveInputMethod_t32_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool ActiveInputMethod_t32_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ActiveInputMethod_t32_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType ActiveInputMethod_t32_0_0_0;
extern const Il2CppType ActiveInputMethod_t32_1_0_0;
extern TypeInfo CrossPlatformInputManager_t35_il2cpp_TypeInfo;
extern const Il2CppType CrossPlatformInputManager_t35_0_0_0;
const Il2CppTypeDefinitionMetadata ActiveInputMethod_t32_DefinitionMetadata = 
{
	&CrossPlatformInputManager_t35_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ActiveInputMethod_t32_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, ActiveInputMethod_t32_VTable/* vtableMethods */
	, ActiveInputMethod_t32_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 105/* fieldStart */

};
TypeInfo ActiveInputMethod_t32_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "ActiveInputMethod"/* name */
	, ""/* namespaze */
	, ActiveInputMethod_t32_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ActiveInputMethod_t32_0_0_0/* byval_arg */
	, &ActiveInputMethod_t32_1_0_0/* this_arg */
	, &ActiveInputMethod_t32_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ActiveInputMethod_t32)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ActiveInputMethod_t32)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_2.h"
// Metadata Definition UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis
extern TypeInfo VirtualAxis_t30_il2cpp_TypeInfo;
// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_2MethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo VirtualAxis_t30_VirtualAxis__ctor_m69_ParameterInfos[] = 
{
	{"name", 0, 134217773, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis::.ctor(System.String)
extern const MethodInfo VirtualAxis__ctor_m69_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&VirtualAxis__ctor_m69/* method */
	, &VirtualAxis_t30_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, VirtualAxis_t30_VirtualAxis__ctor_m69_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 96/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo VirtualAxis_t30_VirtualAxis__ctor_m70_ParameterInfos[] = 
{
	{"name", 0, 134217774, 0, &String_t_0_0_0},
	{"matchToInputSettings", 1, 134217775, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis::.ctor(System.String,System.Boolean)
extern const MethodInfo VirtualAxis__ctor_m70_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&VirtualAxis__ctor_m70/* method */
	, &VirtualAxis_t30_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_SByte_t274/* invoker_method */
	, VirtualAxis_t30_VirtualAxis__ctor_m70_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 97/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis::get_name()
extern const MethodInfo VirtualAxis_get_name_m71_MethodInfo = 
{
	"get_name"/* name */
	, (methodPointerType)&VirtualAxis_get_name_m71/* method */
	, &VirtualAxis_t30_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 41/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 98/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo VirtualAxis_t30_VirtualAxis_set_name_m72_ParameterInfos[] = 
{
	{"value", 0, 134217776, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis::set_name(System.String)
extern const MethodInfo VirtualAxis_set_name_m72_MethodInfo = 
{
	"set_name"/* name */
	, (methodPointerType)&VirtualAxis_set_name_m72/* method */
	, &VirtualAxis_t30_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, VirtualAxis_t30_VirtualAxis_set_name_m72_ParameterInfos/* parameters */
	, 42/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 99/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis::get_matchWithInputManager()
extern const MethodInfo VirtualAxis_get_matchWithInputManager_m73_MethodInfo = 
{
	"get_matchWithInputManager"/* name */
	, (methodPointerType)&VirtualAxis_get_matchWithInputManager_m73/* method */
	, &VirtualAxis_t30_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 43/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 100/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo VirtualAxis_t30_VirtualAxis_set_matchWithInputManager_m74_ParameterInfos[] = 
{
	{"value", 0, 134217777, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis::set_matchWithInputManager(System.Boolean)
extern const MethodInfo VirtualAxis_set_matchWithInputManager_m74_MethodInfo = 
{
	"set_matchWithInputManager"/* name */
	, (methodPointerType)&VirtualAxis_set_matchWithInputManager_m74/* method */
	, &VirtualAxis_t30_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274/* invoker_method */
	, VirtualAxis_t30_VirtualAxis_set_matchWithInputManager_m74_ParameterInfos/* parameters */
	, 44/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 101/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis::Remove()
extern const MethodInfo VirtualAxis_Remove_m75_MethodInfo = 
{
	"Remove"/* name */
	, (methodPointerType)&VirtualAxis_Remove_m75/* method */
	, &VirtualAxis_t30_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 102/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo VirtualAxis_t30_VirtualAxis_Update_m76_ParameterInfos[] = 
{
	{"value", 0, 134217778, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis::Update(System.Single)
extern const MethodInfo VirtualAxis_Update_m76_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&VirtualAxis_Update_m76/* method */
	, &VirtualAxis_t30_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Single_t254/* invoker_method */
	, VirtualAxis_t30_VirtualAxis_Update_m76_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 103/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis::get_GetValue()
extern const MethodInfo VirtualAxis_get_GetValue_m77_MethodInfo = 
{
	"get_GetValue"/* name */
	, (methodPointerType)&VirtualAxis_get_GetValue_m77/* method */
	, &VirtualAxis_t30_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 104/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis::get_GetValueRaw()
extern const MethodInfo VirtualAxis_get_GetValueRaw_m78_MethodInfo = 
{
	"get_GetValueRaw"/* name */
	, (methodPointerType)&VirtualAxis_get_GetValueRaw_m78/* method */
	, &VirtualAxis_t30_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 105/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* VirtualAxis_t30_MethodInfos[] =
{
	&VirtualAxis__ctor_m69_MethodInfo,
	&VirtualAxis__ctor_m70_MethodInfo,
	&VirtualAxis_get_name_m71_MethodInfo,
	&VirtualAxis_set_name_m72_MethodInfo,
	&VirtualAxis_get_matchWithInputManager_m73_MethodInfo,
	&VirtualAxis_set_matchWithInputManager_m74_MethodInfo,
	&VirtualAxis_Remove_m75_MethodInfo,
	&VirtualAxis_Update_m76_MethodInfo,
	&VirtualAxis_get_GetValue_m77_MethodInfo,
	&VirtualAxis_get_GetValueRaw_m78_MethodInfo,
	NULL
};
extern const MethodInfo VirtualAxis_get_name_m71_MethodInfo;
extern const MethodInfo VirtualAxis_set_name_m72_MethodInfo;
static const PropertyInfo VirtualAxis_t30____name_PropertyInfo = 
{
	&VirtualAxis_t30_il2cpp_TypeInfo/* parent */
	, "name"/* name */
	, &VirtualAxis_get_name_m71_MethodInfo/* get */
	, &VirtualAxis_set_name_m72_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo VirtualAxis_get_matchWithInputManager_m73_MethodInfo;
extern const MethodInfo VirtualAxis_set_matchWithInputManager_m74_MethodInfo;
static const PropertyInfo VirtualAxis_t30____matchWithInputManager_PropertyInfo = 
{
	&VirtualAxis_t30_il2cpp_TypeInfo/* parent */
	, "matchWithInputManager"/* name */
	, &VirtualAxis_get_matchWithInputManager_m73_MethodInfo/* get */
	, &VirtualAxis_set_matchWithInputManager_m74_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo VirtualAxis_get_GetValue_m77_MethodInfo;
static const PropertyInfo VirtualAxis_t30____GetValue_PropertyInfo = 
{
	&VirtualAxis_t30_il2cpp_TypeInfo/* parent */
	, "GetValue"/* name */
	, &VirtualAxis_get_GetValue_m77_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo VirtualAxis_get_GetValueRaw_m78_MethodInfo;
static const PropertyInfo VirtualAxis_t30____GetValueRaw_PropertyInfo = 
{
	&VirtualAxis_t30_il2cpp_TypeInfo/* parent */
	, "GetValueRaw"/* name */
	, &VirtualAxis_get_GetValueRaw_m78_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* VirtualAxis_t30_PropertyInfos[] =
{
	&VirtualAxis_t30____name_PropertyInfo,
	&VirtualAxis_t30____matchWithInputManager_PropertyInfo,
	&VirtualAxis_t30____GetValue_PropertyInfo,
	&VirtualAxis_t30____GetValueRaw_PropertyInfo,
	NULL
};
static const Il2CppMethodReference VirtualAxis_t30_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool VirtualAxis_t30_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType VirtualAxis_t30_0_0_0;
extern const Il2CppType VirtualAxis_t30_1_0_0;
struct VirtualAxis_t30;
const Il2CppTypeDefinitionMetadata VirtualAxis_t30_DefinitionMetadata = 
{
	&CrossPlatformInputManager_t35_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, VirtualAxis_t30_VTable/* vtableMethods */
	, VirtualAxis_t30_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 108/* fieldStart */

};
TypeInfo VirtualAxis_t30_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "VirtualAxis"/* name */
	, ""/* namespaze */
	, VirtualAxis_t30_MethodInfos/* methods */
	, VirtualAxis_t30_PropertyInfos/* properties */
	, NULL/* events */
	, &VirtualAxis_t30_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &VirtualAxis_t30_0_0_0/* byval_arg */
	, &VirtualAxis_t30_1_0_0/* this_arg */
	, &VirtualAxis_t30_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (VirtualAxis_t30)/* instance_size */
	, sizeof (VirtualAxis_t30)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048578/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 4/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_3.h"
// Metadata Definition UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton
extern TypeInfo VirtualButton_t33_il2cpp_TypeInfo;
// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_3MethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo VirtualButton_t33_VirtualButton__ctor_m79_ParameterInfos[] = 
{
	{"name", 0, 134217779, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::.ctor(System.String)
extern const MethodInfo VirtualButton__ctor_m79_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&VirtualButton__ctor_m79/* method */
	, &VirtualButton_t33_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, VirtualButton_t33_VirtualButton__ctor_m79_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 106/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo VirtualButton_t33_VirtualButton__ctor_m80_ParameterInfos[] = 
{
	{"name", 0, 134217780, 0, &String_t_0_0_0},
	{"matchToInputSettings", 1, 134217781, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::.ctor(System.String,System.Boolean)
extern const MethodInfo VirtualButton__ctor_m80_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&VirtualButton__ctor_m80/* method */
	, &VirtualButton_t33_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_SByte_t274/* invoker_method */
	, VirtualButton_t33_VirtualButton__ctor_m80_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 107/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::get_name()
extern const MethodInfo VirtualButton_get_name_m81_MethodInfo = 
{
	"get_name"/* name */
	, (methodPointerType)&VirtualButton_get_name_m81/* method */
	, &VirtualButton_t33_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 47/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 108/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo VirtualButton_t33_VirtualButton_set_name_m82_ParameterInfos[] = 
{
	{"value", 0, 134217782, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::set_name(System.String)
extern const MethodInfo VirtualButton_set_name_m82_MethodInfo = 
{
	"set_name"/* name */
	, (methodPointerType)&VirtualButton_set_name_m82/* method */
	, &VirtualButton_t33_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, VirtualButton_t33_VirtualButton_set_name_m82_ParameterInfos/* parameters */
	, 48/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 109/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::get_matchWithInputManager()
extern const MethodInfo VirtualButton_get_matchWithInputManager_m83_MethodInfo = 
{
	"get_matchWithInputManager"/* name */
	, (methodPointerType)&VirtualButton_get_matchWithInputManager_m83/* method */
	, &VirtualButton_t33_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 49/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 110/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo VirtualButton_t33_VirtualButton_set_matchWithInputManager_m84_ParameterInfos[] = 
{
	{"value", 0, 134217783, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::set_matchWithInputManager(System.Boolean)
extern const MethodInfo VirtualButton_set_matchWithInputManager_m84_MethodInfo = 
{
	"set_matchWithInputManager"/* name */
	, (methodPointerType)&VirtualButton_set_matchWithInputManager_m84/* method */
	, &VirtualButton_t33_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274/* invoker_method */
	, VirtualButton_t33_VirtualButton_set_matchWithInputManager_m84_ParameterInfos/* parameters */
	, 50/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 111/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::Pressed()
extern const MethodInfo VirtualButton_Pressed_m85_MethodInfo = 
{
	"Pressed"/* name */
	, (methodPointerType)&VirtualButton_Pressed_m85/* method */
	, &VirtualButton_t33_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 112/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::Released()
extern const MethodInfo VirtualButton_Released_m86_MethodInfo = 
{
	"Released"/* name */
	, (methodPointerType)&VirtualButton_Released_m86/* method */
	, &VirtualButton_t33_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 113/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::Remove()
extern const MethodInfo VirtualButton_Remove_m87_MethodInfo = 
{
	"Remove"/* name */
	, (methodPointerType)&VirtualButton_Remove_m87/* method */
	, &VirtualButton_t33_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 114/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::get_GetButton()
extern const MethodInfo VirtualButton_get_GetButton_m88_MethodInfo = 
{
	"get_GetButton"/* name */
	, (methodPointerType)&VirtualButton_get_GetButton_m88/* method */
	, &VirtualButton_t33_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 115/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::get_GetButtonDown()
extern const MethodInfo VirtualButton_get_GetButtonDown_m89_MethodInfo = 
{
	"get_GetButtonDown"/* name */
	, (methodPointerType)&VirtualButton_get_GetButtonDown_m89/* method */
	, &VirtualButton_t33_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 116/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::get_GetButtonUp()
extern const MethodInfo VirtualButton_get_GetButtonUp_m90_MethodInfo = 
{
	"get_GetButtonUp"/* name */
	, (methodPointerType)&VirtualButton_get_GetButtonUp_m90/* method */
	, &VirtualButton_t33_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 117/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* VirtualButton_t33_MethodInfos[] =
{
	&VirtualButton__ctor_m79_MethodInfo,
	&VirtualButton__ctor_m80_MethodInfo,
	&VirtualButton_get_name_m81_MethodInfo,
	&VirtualButton_set_name_m82_MethodInfo,
	&VirtualButton_get_matchWithInputManager_m83_MethodInfo,
	&VirtualButton_set_matchWithInputManager_m84_MethodInfo,
	&VirtualButton_Pressed_m85_MethodInfo,
	&VirtualButton_Released_m86_MethodInfo,
	&VirtualButton_Remove_m87_MethodInfo,
	&VirtualButton_get_GetButton_m88_MethodInfo,
	&VirtualButton_get_GetButtonDown_m89_MethodInfo,
	&VirtualButton_get_GetButtonUp_m90_MethodInfo,
	NULL
};
extern const MethodInfo VirtualButton_get_name_m81_MethodInfo;
extern const MethodInfo VirtualButton_set_name_m82_MethodInfo;
static const PropertyInfo VirtualButton_t33____name_PropertyInfo = 
{
	&VirtualButton_t33_il2cpp_TypeInfo/* parent */
	, "name"/* name */
	, &VirtualButton_get_name_m81_MethodInfo/* get */
	, &VirtualButton_set_name_m82_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo VirtualButton_get_matchWithInputManager_m83_MethodInfo;
extern const MethodInfo VirtualButton_set_matchWithInputManager_m84_MethodInfo;
static const PropertyInfo VirtualButton_t33____matchWithInputManager_PropertyInfo = 
{
	&VirtualButton_t33_il2cpp_TypeInfo/* parent */
	, "matchWithInputManager"/* name */
	, &VirtualButton_get_matchWithInputManager_m83_MethodInfo/* get */
	, &VirtualButton_set_matchWithInputManager_m84_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo VirtualButton_get_GetButton_m88_MethodInfo;
static const PropertyInfo VirtualButton_t33____GetButton_PropertyInfo = 
{
	&VirtualButton_t33_il2cpp_TypeInfo/* parent */
	, "GetButton"/* name */
	, &VirtualButton_get_GetButton_m88_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo VirtualButton_get_GetButtonDown_m89_MethodInfo;
static const PropertyInfo VirtualButton_t33____GetButtonDown_PropertyInfo = 
{
	&VirtualButton_t33_il2cpp_TypeInfo/* parent */
	, "GetButtonDown"/* name */
	, &VirtualButton_get_GetButtonDown_m89_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo VirtualButton_get_GetButtonUp_m90_MethodInfo;
static const PropertyInfo VirtualButton_t33____GetButtonUp_PropertyInfo = 
{
	&VirtualButton_t33_il2cpp_TypeInfo/* parent */
	, "GetButtonUp"/* name */
	, &VirtualButton_get_GetButtonUp_m90_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* VirtualButton_t33_PropertyInfos[] =
{
	&VirtualButton_t33____name_PropertyInfo,
	&VirtualButton_t33____matchWithInputManager_PropertyInfo,
	&VirtualButton_t33____GetButton_PropertyInfo,
	&VirtualButton_t33____GetButtonDown_PropertyInfo,
	&VirtualButton_t33____GetButtonUp_PropertyInfo,
	NULL
};
static const Il2CppMethodReference VirtualButton_t33_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool VirtualButton_t33_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType VirtualButton_t33_0_0_0;
extern const Il2CppType VirtualButton_t33_1_0_0;
struct VirtualButton_t33;
const Il2CppTypeDefinitionMetadata VirtualButton_t33_DefinitionMetadata = 
{
	&CrossPlatformInputManager_t35_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, VirtualButton_t33_VTable/* vtableMethods */
	, VirtualButton_t33_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 111/* fieldStart */

};
TypeInfo VirtualButton_t33_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "VirtualButton"/* name */
	, ""/* namespaze */
	, VirtualButton_t33_MethodInfos/* methods */
	, VirtualButton_t33_PropertyInfos/* properties */
	, NULL/* events */
	, &VirtualButton_t33_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &VirtualButton_t33_0_0_0/* byval_arg */
	, &VirtualButton_t33_1_0_0/* this_arg */
	, &VirtualButton_t33_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (VirtualButton_t33)/* instance_size */
	, sizeof (VirtualButton_t33)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048578/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 12/* method_count */
	, 5/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_4.h"
// Metadata Definition UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager
// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_4MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::.cctor()
extern const MethodInfo CrossPlatformInputManager__cctor_m91_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&CrossPlatformInputManager__cctor_m91/* method */
	, &CrossPlatformInputManager_t35_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 71/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ActiveInputMethod_t32_0_0_0;
static const ParameterInfo CrossPlatformInputManager_t35_CrossPlatformInputManager_SwitchActiveInputMethod_m92_ParameterInfos[] = 
{
	{"activeInputMethod", 0, 134217748, 0, &ActiveInputMethod_t32_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::SwitchActiveInputMethod(UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/ActiveInputMethod)
extern const MethodInfo CrossPlatformInputManager_SwitchActiveInputMethod_m92_MethodInfo = 
{
	"SwitchActiveInputMethod"/* name */
	, (methodPointerType)&CrossPlatformInputManager_SwitchActiveInputMethod_m92/* method */
	, &CrossPlatformInputManager_t35_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, CrossPlatformInputManager_t35_CrossPlatformInputManager_SwitchActiveInputMethod_m92_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 72/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo CrossPlatformInputManager_t35_CrossPlatformInputManager_AxisExists_m93_ParameterInfos[] = 
{
	{"name", 0, 134217749, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::AxisExists(System.String)
extern const MethodInfo CrossPlatformInputManager_AxisExists_m93_MethodInfo = 
{
	"AxisExists"/* name */
	, (methodPointerType)&CrossPlatformInputManager_AxisExists_m93/* method */
	, &CrossPlatformInputManager_t35_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, CrossPlatformInputManager_t35_CrossPlatformInputManager_AxisExists_m93_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 73/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo CrossPlatformInputManager_t35_CrossPlatformInputManager_ButtonExists_m94_ParameterInfos[] = 
{
	{"name", 0, 134217750, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::ButtonExists(System.String)
extern const MethodInfo CrossPlatformInputManager_ButtonExists_m94_MethodInfo = 
{
	"ButtonExists"/* name */
	, (methodPointerType)&CrossPlatformInputManager_ButtonExists_m94/* method */
	, &CrossPlatformInputManager_t35_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, CrossPlatformInputManager_t35_CrossPlatformInputManager_ButtonExists_m94_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 74/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType VirtualAxis_t30_0_0_0;
static const ParameterInfo CrossPlatformInputManager_t35_CrossPlatformInputManager_RegisterVirtualAxis_m95_ParameterInfos[] = 
{
	{"axis", 0, 134217751, 0, &VirtualAxis_t30_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::RegisterVirtualAxis(UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis)
extern const MethodInfo CrossPlatformInputManager_RegisterVirtualAxis_m95_MethodInfo = 
{
	"RegisterVirtualAxis"/* name */
	, (methodPointerType)&CrossPlatformInputManager_RegisterVirtualAxis_m95/* method */
	, &CrossPlatformInputManager_t35_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, CrossPlatformInputManager_t35_CrossPlatformInputManager_RegisterVirtualAxis_m95_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 75/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType VirtualButton_t33_0_0_0;
static const ParameterInfo CrossPlatformInputManager_t35_CrossPlatformInputManager_RegisterVirtualButton_m96_ParameterInfos[] = 
{
	{"button", 0, 134217752, 0, &VirtualButton_t33_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::RegisterVirtualButton(UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton)
extern const MethodInfo CrossPlatformInputManager_RegisterVirtualButton_m96_MethodInfo = 
{
	"RegisterVirtualButton"/* name */
	, (methodPointerType)&CrossPlatformInputManager_RegisterVirtualButton_m96/* method */
	, &CrossPlatformInputManager_t35_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, CrossPlatformInputManager_t35_CrossPlatformInputManager_RegisterVirtualButton_m96_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 76/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo CrossPlatformInputManager_t35_CrossPlatformInputManager_UnRegisterVirtualAxis_m97_ParameterInfos[] = 
{
	{"name", 0, 134217753, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::UnRegisterVirtualAxis(System.String)
extern const MethodInfo CrossPlatformInputManager_UnRegisterVirtualAxis_m97_MethodInfo = 
{
	"UnRegisterVirtualAxis"/* name */
	, (methodPointerType)&CrossPlatformInputManager_UnRegisterVirtualAxis_m97/* method */
	, &CrossPlatformInputManager_t35_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, CrossPlatformInputManager_t35_CrossPlatformInputManager_UnRegisterVirtualAxis_m97_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 77/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo CrossPlatformInputManager_t35_CrossPlatformInputManager_UnRegisterVirtualButton_m98_ParameterInfos[] = 
{
	{"name", 0, 134217754, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::UnRegisterVirtualButton(System.String)
extern const MethodInfo CrossPlatformInputManager_UnRegisterVirtualButton_m98_MethodInfo = 
{
	"UnRegisterVirtualButton"/* name */
	, (methodPointerType)&CrossPlatformInputManager_UnRegisterVirtualButton_m98/* method */
	, &CrossPlatformInputManager_t35_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, CrossPlatformInputManager_t35_CrossPlatformInputManager_UnRegisterVirtualButton_m98_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 78/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo CrossPlatformInputManager_t35_CrossPlatformInputManager_VirtualAxisReference_m99_ParameterInfos[] = 
{
	{"name", 0, 134217755, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::VirtualAxisReference(System.String)
extern const MethodInfo CrossPlatformInputManager_VirtualAxisReference_m99_MethodInfo = 
{
	"VirtualAxisReference"/* name */
	, (methodPointerType)&CrossPlatformInputManager_VirtualAxisReference_m99/* method */
	, &CrossPlatformInputManager_t35_il2cpp_TypeInfo/* declaring_type */
	, &VirtualAxis_t30_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, CrossPlatformInputManager_t35_CrossPlatformInputManager_VirtualAxisReference_m99_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 79/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo CrossPlatformInputManager_t35_CrossPlatformInputManager_GetAxis_m100_ParameterInfos[] = 
{
	{"name", 0, 134217756, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Single_t254_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Single UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::GetAxis(System.String)
extern const MethodInfo CrossPlatformInputManager_GetAxis_m100_MethodInfo = 
{
	"GetAxis"/* name */
	, (methodPointerType)&CrossPlatformInputManager_GetAxis_m100/* method */
	, &CrossPlatformInputManager_t35_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254_Object_t/* invoker_method */
	, CrossPlatformInputManager_t35_CrossPlatformInputManager_GetAxis_m100_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 80/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo CrossPlatformInputManager_t35_CrossPlatformInputManager_GetAxisRaw_m101_ParameterInfos[] = 
{
	{"name", 0, 134217757, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Single_t254_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Single UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::GetAxisRaw(System.String)
extern const MethodInfo CrossPlatformInputManager_GetAxisRaw_m101_MethodInfo = 
{
	"GetAxisRaw"/* name */
	, (methodPointerType)&CrossPlatformInputManager_GetAxisRaw_m101/* method */
	, &CrossPlatformInputManager_t35_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254_Object_t/* invoker_method */
	, CrossPlatformInputManager_t35_CrossPlatformInputManager_GetAxisRaw_m101_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 81/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo CrossPlatformInputManager_t35_CrossPlatformInputManager_GetAxis_m102_ParameterInfos[] = 
{
	{"name", 0, 134217758, 0, &String_t_0_0_0},
	{"raw", 1, 134217759, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Single_t254_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::GetAxis(System.String,System.Boolean)
extern const MethodInfo CrossPlatformInputManager_GetAxis_m102_MethodInfo = 
{
	"GetAxis"/* name */
	, (methodPointerType)&CrossPlatformInputManager_GetAxis_m102/* method */
	, &CrossPlatformInputManager_t35_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254_Object_t_SByte_t274/* invoker_method */
	, CrossPlatformInputManager_t35_CrossPlatformInputManager_GetAxis_m102_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 82/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo CrossPlatformInputManager_t35_CrossPlatformInputManager_GetButton_m103_ParameterInfos[] = 
{
	{"name", 0, 134217760, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::GetButton(System.String)
extern const MethodInfo CrossPlatformInputManager_GetButton_m103_MethodInfo = 
{
	"GetButton"/* name */
	, (methodPointerType)&CrossPlatformInputManager_GetButton_m103/* method */
	, &CrossPlatformInputManager_t35_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, CrossPlatformInputManager_t35_CrossPlatformInputManager_GetButton_m103_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 83/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo CrossPlatformInputManager_t35_CrossPlatformInputManager_GetButtonDown_m104_ParameterInfos[] = 
{
	{"name", 0, 134217761, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::GetButtonDown(System.String)
extern const MethodInfo CrossPlatformInputManager_GetButtonDown_m104_MethodInfo = 
{
	"GetButtonDown"/* name */
	, (methodPointerType)&CrossPlatformInputManager_GetButtonDown_m104/* method */
	, &CrossPlatformInputManager_t35_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, CrossPlatformInputManager_t35_CrossPlatformInputManager_GetButtonDown_m104_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 84/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo CrossPlatformInputManager_t35_CrossPlatformInputManager_GetButtonUp_m105_ParameterInfos[] = 
{
	{"name", 0, 134217762, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::GetButtonUp(System.String)
extern const MethodInfo CrossPlatformInputManager_GetButtonUp_m105_MethodInfo = 
{
	"GetButtonUp"/* name */
	, (methodPointerType)&CrossPlatformInputManager_GetButtonUp_m105/* method */
	, &CrossPlatformInputManager_t35_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, CrossPlatformInputManager_t35_CrossPlatformInputManager_GetButtonUp_m105_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 85/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo CrossPlatformInputManager_t35_CrossPlatformInputManager_SetButtonDown_m106_ParameterInfos[] = 
{
	{"name", 0, 134217763, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::SetButtonDown(System.String)
extern const MethodInfo CrossPlatformInputManager_SetButtonDown_m106_MethodInfo = 
{
	"SetButtonDown"/* name */
	, (methodPointerType)&CrossPlatformInputManager_SetButtonDown_m106/* method */
	, &CrossPlatformInputManager_t35_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, CrossPlatformInputManager_t35_CrossPlatformInputManager_SetButtonDown_m106_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 86/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo CrossPlatformInputManager_t35_CrossPlatformInputManager_SetButtonUp_m107_ParameterInfos[] = 
{
	{"name", 0, 134217764, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::SetButtonUp(System.String)
extern const MethodInfo CrossPlatformInputManager_SetButtonUp_m107_MethodInfo = 
{
	"SetButtonUp"/* name */
	, (methodPointerType)&CrossPlatformInputManager_SetButtonUp_m107/* method */
	, &CrossPlatformInputManager_t35_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, CrossPlatformInputManager_t35_CrossPlatformInputManager_SetButtonUp_m107_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 87/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo CrossPlatformInputManager_t35_CrossPlatformInputManager_SetAxisPositive_m108_ParameterInfos[] = 
{
	{"name", 0, 134217765, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::SetAxisPositive(System.String)
extern const MethodInfo CrossPlatformInputManager_SetAxisPositive_m108_MethodInfo = 
{
	"SetAxisPositive"/* name */
	, (methodPointerType)&CrossPlatformInputManager_SetAxisPositive_m108/* method */
	, &CrossPlatformInputManager_t35_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, CrossPlatformInputManager_t35_CrossPlatformInputManager_SetAxisPositive_m108_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 88/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo CrossPlatformInputManager_t35_CrossPlatformInputManager_SetAxisNegative_m109_ParameterInfos[] = 
{
	{"name", 0, 134217766, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::SetAxisNegative(System.String)
extern const MethodInfo CrossPlatformInputManager_SetAxisNegative_m109_MethodInfo = 
{
	"SetAxisNegative"/* name */
	, (methodPointerType)&CrossPlatformInputManager_SetAxisNegative_m109/* method */
	, &CrossPlatformInputManager_t35_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, CrossPlatformInputManager_t35_CrossPlatformInputManager_SetAxisNegative_m109_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 89/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo CrossPlatformInputManager_t35_CrossPlatformInputManager_SetAxisZero_m110_ParameterInfos[] = 
{
	{"name", 0, 134217767, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::SetAxisZero(System.String)
extern const MethodInfo CrossPlatformInputManager_SetAxisZero_m110_MethodInfo = 
{
	"SetAxisZero"/* name */
	, (methodPointerType)&CrossPlatformInputManager_SetAxisZero_m110/* method */
	, &CrossPlatformInputManager_t35_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, CrossPlatformInputManager_t35_CrossPlatformInputManager_SetAxisZero_m110_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 90/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo CrossPlatformInputManager_t35_CrossPlatformInputManager_SetAxis_m111_ParameterInfos[] = 
{
	{"name", 0, 134217768, 0, &String_t_0_0_0},
	{"value", 1, 134217769, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::SetAxis(System.String,System.Single)
extern const MethodInfo CrossPlatformInputManager_SetAxis_m111_MethodInfo = 
{
	"SetAxis"/* name */
	, (methodPointerType)&CrossPlatformInputManager_SetAxis_m111/* method */
	, &CrossPlatformInputManager_t35_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Single_t254/* invoker_method */
	, CrossPlatformInputManager_t35_CrossPlatformInputManager_SetAxis_m111_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 91/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector3_t4_0_0_0;
extern void* RuntimeInvoker_Vector3_t4 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::get_mousePosition()
extern const MethodInfo CrossPlatformInputManager_get_mousePosition_m112_MethodInfo = 
{
	"get_mousePosition"/* name */
	, (methodPointerType)&CrossPlatformInputManager_get_mousePosition_m112/* method */
	, &CrossPlatformInputManager_t35_il2cpp_TypeInfo/* declaring_type */
	, &Vector3_t4_0_0_0/* return_type */
	, RuntimeInvoker_Vector3_t4/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 92/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo CrossPlatformInputManager_t35_CrossPlatformInputManager_SetVirtualMousePositionX_m113_ParameterInfos[] = 
{
	{"f", 0, 134217770, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::SetVirtualMousePositionX(System.Single)
extern const MethodInfo CrossPlatformInputManager_SetVirtualMousePositionX_m113_MethodInfo = 
{
	"SetVirtualMousePositionX"/* name */
	, (methodPointerType)&CrossPlatformInputManager_SetVirtualMousePositionX_m113/* method */
	, &CrossPlatformInputManager_t35_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Single_t254/* invoker_method */
	, CrossPlatformInputManager_t35_CrossPlatformInputManager_SetVirtualMousePositionX_m113_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 93/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo CrossPlatformInputManager_t35_CrossPlatformInputManager_SetVirtualMousePositionY_m114_ParameterInfos[] = 
{
	{"f", 0, 134217771, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::SetVirtualMousePositionY(System.Single)
extern const MethodInfo CrossPlatformInputManager_SetVirtualMousePositionY_m114_MethodInfo = 
{
	"SetVirtualMousePositionY"/* name */
	, (methodPointerType)&CrossPlatformInputManager_SetVirtualMousePositionY_m114/* method */
	, &CrossPlatformInputManager_t35_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Single_t254/* invoker_method */
	, CrossPlatformInputManager_t35_CrossPlatformInputManager_SetVirtualMousePositionY_m114_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 94/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo CrossPlatformInputManager_t35_CrossPlatformInputManager_SetVirtualMousePositionZ_m115_ParameterInfos[] = 
{
	{"f", 0, 134217772, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::SetVirtualMousePositionZ(System.Single)
extern const MethodInfo CrossPlatformInputManager_SetVirtualMousePositionZ_m115_MethodInfo = 
{
	"SetVirtualMousePositionZ"/* name */
	, (methodPointerType)&CrossPlatformInputManager_SetVirtualMousePositionZ_m115/* method */
	, &CrossPlatformInputManager_t35_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Single_t254/* invoker_method */
	, CrossPlatformInputManager_t35_CrossPlatformInputManager_SetVirtualMousePositionZ_m115_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 95/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CrossPlatformInputManager_t35_MethodInfos[] =
{
	&CrossPlatformInputManager__cctor_m91_MethodInfo,
	&CrossPlatformInputManager_SwitchActiveInputMethod_m92_MethodInfo,
	&CrossPlatformInputManager_AxisExists_m93_MethodInfo,
	&CrossPlatformInputManager_ButtonExists_m94_MethodInfo,
	&CrossPlatformInputManager_RegisterVirtualAxis_m95_MethodInfo,
	&CrossPlatformInputManager_RegisterVirtualButton_m96_MethodInfo,
	&CrossPlatformInputManager_UnRegisterVirtualAxis_m97_MethodInfo,
	&CrossPlatformInputManager_UnRegisterVirtualButton_m98_MethodInfo,
	&CrossPlatformInputManager_VirtualAxisReference_m99_MethodInfo,
	&CrossPlatformInputManager_GetAxis_m100_MethodInfo,
	&CrossPlatformInputManager_GetAxisRaw_m101_MethodInfo,
	&CrossPlatformInputManager_GetAxis_m102_MethodInfo,
	&CrossPlatformInputManager_GetButton_m103_MethodInfo,
	&CrossPlatformInputManager_GetButtonDown_m104_MethodInfo,
	&CrossPlatformInputManager_GetButtonUp_m105_MethodInfo,
	&CrossPlatformInputManager_SetButtonDown_m106_MethodInfo,
	&CrossPlatformInputManager_SetButtonUp_m107_MethodInfo,
	&CrossPlatformInputManager_SetAxisPositive_m108_MethodInfo,
	&CrossPlatformInputManager_SetAxisNegative_m109_MethodInfo,
	&CrossPlatformInputManager_SetAxisZero_m110_MethodInfo,
	&CrossPlatformInputManager_SetAxis_m111_MethodInfo,
	&CrossPlatformInputManager_get_mousePosition_m112_MethodInfo,
	&CrossPlatformInputManager_SetVirtualMousePositionX_m113_MethodInfo,
	&CrossPlatformInputManager_SetVirtualMousePositionY_m114_MethodInfo,
	&CrossPlatformInputManager_SetVirtualMousePositionZ_m115_MethodInfo,
	NULL
};
extern const MethodInfo CrossPlatformInputManager_get_mousePosition_m112_MethodInfo;
static const PropertyInfo CrossPlatformInputManager_t35____mousePosition_PropertyInfo = 
{
	&CrossPlatformInputManager_t35_il2cpp_TypeInfo/* parent */
	, "mousePosition"/* name */
	, &CrossPlatformInputManager_get_mousePosition_m112_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* CrossPlatformInputManager_t35_PropertyInfos[] =
{
	&CrossPlatformInputManager_t35____mousePosition_PropertyInfo,
	NULL
};
static const Il2CppType* CrossPlatformInputManager_t35_il2cpp_TypeInfo__nestedTypes[3] =
{
	&ActiveInputMethod_t32_0_0_0,
	&VirtualAxis_t30_0_0_0,
	&VirtualButton_t33_0_0_0,
};
static const Il2CppMethodReference CrossPlatformInputManager_t35_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool CrossPlatformInputManager_t35_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType CrossPlatformInputManager_t35_1_0_0;
struct CrossPlatformInputManager_t35;
const Il2CppTypeDefinitionMetadata CrossPlatformInputManager_t35_DefinitionMetadata = 
{
	NULL/* declaringType */
	, CrossPlatformInputManager_t35_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CrossPlatformInputManager_t35_VTable/* vtableMethods */
	, CrossPlatformInputManager_t35_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 116/* fieldStart */

};
TypeInfo CrossPlatformInputManager_t35_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "CrossPlatformInputManager"/* name */
	, "UnityStandardAssets.CrossPlatformInput"/* namespaze */
	, CrossPlatformInputManager_t35_MethodInfos/* methods */
	, CrossPlatformInputManager_t35_PropertyInfos/* properties */
	, NULL/* events */
	, &CrossPlatformInputManager_t35_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CrossPlatformInputManager_t35_0_0_0/* byval_arg */
	, &CrossPlatformInputManager_t35_1_0_0/* this_arg */
	, &CrossPlatformInputManager_t35_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CrossPlatformInputManager_t35)/* instance_size */
	, sizeof (CrossPlatformInputManager_t35)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CrossPlatformInputManager_t35_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 385/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 25/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 3/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.CrossPlatformInput.InputAxisScrollbar
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_5.h"
// Metadata Definition UnityStandardAssets.CrossPlatformInput.InputAxisScrollbar
extern TypeInfo InputAxisScrollbar_t36_il2cpp_TypeInfo;
// UnityStandardAssets.CrossPlatformInput.InputAxisScrollbar
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_5MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.InputAxisScrollbar::.ctor()
extern const MethodInfo InputAxisScrollbar__ctor_m116_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InputAxisScrollbar__ctor_m116/* method */
	, &InputAxisScrollbar_t36_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 118/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.InputAxisScrollbar::Update()
extern const MethodInfo InputAxisScrollbar_Update_m117_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&InputAxisScrollbar_Update_m117/* method */
	, &InputAxisScrollbar_t36_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 119/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo InputAxisScrollbar_t36_InputAxisScrollbar_HandleInput_m118_ParameterInfos[] = 
{
	{"value", 0, 134217784, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.InputAxisScrollbar::HandleInput(System.Single)
extern const MethodInfo InputAxisScrollbar_HandleInput_m118_MethodInfo = 
{
	"HandleInput"/* name */
	, (methodPointerType)&InputAxisScrollbar_HandleInput_m118/* method */
	, &InputAxisScrollbar_t36_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Single_t254/* invoker_method */
	, InputAxisScrollbar_t36_InputAxisScrollbar_HandleInput_m118_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 120/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* InputAxisScrollbar_t36_MethodInfos[] =
{
	&InputAxisScrollbar__ctor_m116_MethodInfo,
	&InputAxisScrollbar_Update_m117_MethodInfo,
	&InputAxisScrollbar_HandleInput_m118_MethodInfo,
	NULL
};
static const Il2CppMethodReference InputAxisScrollbar_t36_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool InputAxisScrollbar_t36_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType InputAxisScrollbar_t36_0_0_0;
extern const Il2CppType InputAxisScrollbar_t36_1_0_0;
struct InputAxisScrollbar_t36;
const Il2CppTypeDefinitionMetadata InputAxisScrollbar_t36_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, InputAxisScrollbar_t36_VTable/* vtableMethods */
	, InputAxisScrollbar_t36_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 119/* fieldStart */

};
TypeInfo InputAxisScrollbar_t36_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "InputAxisScrollbar"/* name */
	, "UnityStandardAssets.CrossPlatformInput"/* namespaze */
	, InputAxisScrollbar_t36_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &InputAxisScrollbar_t36_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &InputAxisScrollbar_t36_0_0_0/* byval_arg */
	, &InputAxisScrollbar_t36_1_0_0/* this_arg */
	, &InputAxisScrollbar_t36_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InputAxisScrollbar_t36)/* instance_size */
	, sizeof (InputAxisScrollbar_t36)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.CrossPlatformInput.Joystick/AxisOption
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_6.h"
// Metadata Definition UnityStandardAssets.CrossPlatformInput.Joystick/AxisOption
extern TypeInfo AxisOption_t37_il2cpp_TypeInfo;
// UnityStandardAssets.CrossPlatformInput.Joystick/AxisOption
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_6MethodDeclarations.h"
static const MethodInfo* AxisOption_t37_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference AxisOption_t37_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool AxisOption_t37_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair AxisOption_t37_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType AxisOption_t37_0_0_0;
extern const Il2CppType AxisOption_t37_1_0_0;
extern TypeInfo Joystick_t38_il2cpp_TypeInfo;
extern const Il2CppType Joystick_t38_0_0_0;
const Il2CppTypeDefinitionMetadata AxisOption_t37_DefinitionMetadata = 
{
	&Joystick_t38_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AxisOption_t37_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, AxisOption_t37_VTable/* vtableMethods */
	, AxisOption_t37_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 120/* fieldStart */

};
TypeInfo AxisOption_t37_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "AxisOption"/* name */
	, ""/* namespaze */
	, AxisOption_t37_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AxisOption_t37_0_0_0/* byval_arg */
	, &AxisOption_t37_1_0_0/* this_arg */
	, &AxisOption_t37_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AxisOption_t37)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (AxisOption_t37)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityStandardAssets.CrossPlatformInput.Joystick
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_7.h"
// Metadata Definition UnityStandardAssets.CrossPlatformInput.Joystick
// UnityStandardAssets.CrossPlatformInput.Joystick
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_7MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.Joystick::.ctor()
extern const MethodInfo Joystick__ctor_m119_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Joystick__ctor_m119/* method */
	, &Joystick_t38_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 121/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.Joystick::OnEnable()
extern const MethodInfo Joystick_OnEnable_m120_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&Joystick_OnEnable_m120/* method */
	, &Joystick_t38_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 122/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector3_t4_0_0_0;
static const ParameterInfo Joystick_t38_Joystick_UpdateVirtualAxes_m121_ParameterInfos[] = 
{
	{"value", 0, 134217785, 0, &Vector3_t4_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Vector3_t4 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.Joystick::UpdateVirtualAxes(UnityEngine.Vector3)
extern const MethodInfo Joystick_UpdateVirtualAxes_m121_MethodInfo = 
{
	"UpdateVirtualAxes"/* name */
	, (methodPointerType)&Joystick_UpdateVirtualAxes_m121/* method */
	, &Joystick_t38_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Vector3_t4/* invoker_method */
	, Joystick_t38_Joystick_UpdateVirtualAxes_m121_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 123/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.Joystick::CreateVirtualAxes()
extern const MethodInfo Joystick_CreateVirtualAxes_m122_MethodInfo = 
{
	"CreateVirtualAxes"/* name */
	, (methodPointerType)&Joystick_CreateVirtualAxes_m122/* method */
	, &Joystick_t38_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 124/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t215_0_0_0;
static const ParameterInfo Joystick_t38_Joystick_OnDrag_m123_ParameterInfos[] = 
{
	{"data", 0, 134217786, 0, &PointerEventData_t215_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.Joystick::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern const MethodInfo Joystick_OnDrag_m123_MethodInfo = 
{
	"OnDrag"/* name */
	, (methodPointerType)&Joystick_OnDrag_m123/* method */
	, &Joystick_t38_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, Joystick_t38_Joystick_OnDrag_m123_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 125/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t215_0_0_0;
static const ParameterInfo Joystick_t38_Joystick_OnPointerUp_m124_ParameterInfos[] = 
{
	{"data", 0, 134217787, 0, &PointerEventData_t215_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.Joystick::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern const MethodInfo Joystick_OnPointerUp_m124_MethodInfo = 
{
	"OnPointerUp"/* name */
	, (methodPointerType)&Joystick_OnPointerUp_m124/* method */
	, &Joystick_t38_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, Joystick_t38_Joystick_OnPointerUp_m124_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 126/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t215_0_0_0;
static const ParameterInfo Joystick_t38_Joystick_OnPointerDown_m125_ParameterInfos[] = 
{
	{"data", 0, 134217788, 0, &PointerEventData_t215_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.Joystick::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern const MethodInfo Joystick_OnPointerDown_m125_MethodInfo = 
{
	"OnPointerDown"/* name */
	, (methodPointerType)&Joystick_OnPointerDown_m125/* method */
	, &Joystick_t38_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, Joystick_t38_Joystick_OnPointerDown_m125_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 127/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.Joystick::OnDisable()
extern const MethodInfo Joystick_OnDisable_m126_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&Joystick_OnDisable_m126/* method */
	, &Joystick_t38_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 128/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Joystick_t38_MethodInfos[] =
{
	&Joystick__ctor_m119_MethodInfo,
	&Joystick_OnEnable_m120_MethodInfo,
	&Joystick_UpdateVirtualAxes_m121_MethodInfo,
	&Joystick_CreateVirtualAxes_m122_MethodInfo,
	&Joystick_OnDrag_m123_MethodInfo,
	&Joystick_OnPointerUp_m124_MethodInfo,
	&Joystick_OnPointerDown_m125_MethodInfo,
	&Joystick_OnDisable_m126_MethodInfo,
	NULL
};
static const Il2CppType* Joystick_t38_il2cpp_TypeInfo__nestedTypes[1] =
{
	&AxisOption_t37_0_0_0,
};
extern const MethodInfo Joystick_OnPointerDown_m125_MethodInfo;
extern const MethodInfo Joystick_OnPointerUp_m124_MethodInfo;
extern const MethodInfo Joystick_OnDrag_m123_MethodInfo;
static const Il2CppMethodReference Joystick_t38_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	&Joystick_OnPointerDown_m125_MethodInfo,
	&Joystick_OnPointerUp_m124_MethodInfo,
	&Joystick_OnDrag_m123_MethodInfo,
};
static bool Joystick_t38_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IDragHandler_t283_0_0_0;
static const Il2CppType* Joystick_t38_InterfacesTypeInfos[] = 
{
	&IPointerDownHandler_t280_0_0_0,
	&IEventSystemHandler_t281_0_0_0,
	&IPointerUpHandler_t282_0_0_0,
	&IDragHandler_t283_0_0_0,
};
static Il2CppInterfaceOffsetPair Joystick_t38_InterfacesOffsets[] = 
{
	{ &IPointerDownHandler_t280_0_0_0, 4},
	{ &IEventSystemHandler_t281_0_0_0, 5},
	{ &IPointerUpHandler_t282_0_0_0, 5},
	{ &IDragHandler_t283_0_0_0, 6},
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType Joystick_t38_1_0_0;
struct Joystick_t38;
const Il2CppTypeDefinitionMetadata Joystick_t38_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Joystick_t38_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, Joystick_t38_InterfacesTypeInfos/* implementedInterfaces */
	, Joystick_t38_InterfacesOffsets/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, Joystick_t38_VTable/* vtableMethods */
	, Joystick_t38_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 124/* fieldStart */

};
TypeInfo Joystick_t38_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "Joystick"/* name */
	, "UnityStandardAssets.CrossPlatformInput"/* namespaze */
	, Joystick_t38_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Joystick_t38_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Joystick_t38_0_0_0/* byval_arg */
	, &Joystick_t38_1_0_0/* this_arg */
	, &Joystick_t38_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Joystick_t38)/* instance_size */
	, sizeof (Joystick_t38)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 0/* property_count */
	, 9/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 7/* vtable_count */
	, 4/* interfaces_count */
	, 4/* interface_offsets_count */

};
// UnityStandardAssets.CrossPlatformInput.MobileControlRig
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_8.h"
// Metadata Definition UnityStandardAssets.CrossPlatformInput.MobileControlRig
extern TypeInfo MobileControlRig_t39_il2cpp_TypeInfo;
// UnityStandardAssets.CrossPlatformInput.MobileControlRig
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_8MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.MobileControlRig::.ctor()
extern const MethodInfo MobileControlRig__ctor_m127_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MobileControlRig__ctor_m127/* method */
	, &MobileControlRig_t39_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 129/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.MobileControlRig::OnEnable()
extern const MethodInfo MobileControlRig_OnEnable_m128_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&MobileControlRig_OnEnable_m128/* method */
	, &MobileControlRig_t39_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 130/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.MobileControlRig::CheckEnableControlRig()
extern const MethodInfo MobileControlRig_CheckEnableControlRig_m129_MethodInfo = 
{
	"CheckEnableControlRig"/* name */
	, (methodPointerType)&MobileControlRig_CheckEnableControlRig_m129/* method */
	, &MobileControlRig_t39_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 131/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo MobileControlRig_t39_MobileControlRig_EnableControlRig_m130_ParameterInfos[] = 
{
	{"enabled", 0, 134217789, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.MobileControlRig::EnableControlRig(System.Boolean)
extern const MethodInfo MobileControlRig_EnableControlRig_m130_MethodInfo = 
{
	"EnableControlRig"/* name */
	, (methodPointerType)&MobileControlRig_EnableControlRig_m130/* method */
	, &MobileControlRig_t39_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274/* invoker_method */
	, MobileControlRig_t39_MobileControlRig_EnableControlRig_m130_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 132/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MobileControlRig_t39_MethodInfos[] =
{
	&MobileControlRig__ctor_m127_MethodInfo,
	&MobileControlRig_OnEnable_m128_MethodInfo,
	&MobileControlRig_CheckEnableControlRig_m129_MethodInfo,
	&MobileControlRig_EnableControlRig_m130_MethodInfo,
	NULL
};
static const Il2CppMethodReference MobileControlRig_t39_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool MobileControlRig_t39_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType MobileControlRig_t39_0_0_0;
extern const Il2CppType MobileControlRig_t39_1_0_0;
struct MobileControlRig_t39;
const Il2CppTypeDefinitionMetadata MobileControlRig_t39_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, MobileControlRig_t39_VTable/* vtableMethods */
	, MobileControlRig_t39_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo MobileControlRig_t39_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "MobileControlRig"/* name */
	, "UnityStandardAssets.CrossPlatformInput"/* namespaze */
	, MobileControlRig_t39_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &MobileControlRig_t39_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 51/* custom_attributes_cache */
	, &MobileControlRig_t39_0_0_0/* byval_arg */
	, &MobileControlRig_t39_1_0_0/* this_arg */
	, &MobileControlRig_t39_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MobileControlRig_t39)/* instance_size */
	, sizeof (MobileControlRig_t39)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_9.h"
// Metadata Definition UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput
extern TypeInfo MobileInput_t40_il2cpp_TypeInfo;
// UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_9MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::.ctor()
extern const MethodInfo MobileInput__ctor_m131_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MobileInput__ctor_m131/* method */
	, &MobileInput_t40_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 133/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo MobileInput_t40_MobileInput_AddButton_m132_ParameterInfos[] = 
{
	{"name", 0, 134217790, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::AddButton(System.String)
extern const MethodInfo MobileInput_AddButton_m132_MethodInfo = 
{
	"AddButton"/* name */
	, (methodPointerType)&MobileInput_AddButton_m132/* method */
	, &MobileInput_t40_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, MobileInput_t40_MobileInput_AddButton_m132_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 134/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo MobileInput_t40_MobileInput_AddAxes_m133_ParameterInfos[] = 
{
	{"name", 0, 134217791, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::AddAxes(System.String)
extern const MethodInfo MobileInput_AddAxes_m133_MethodInfo = 
{
	"AddAxes"/* name */
	, (methodPointerType)&MobileInput_AddAxes_m133/* method */
	, &MobileInput_t40_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, MobileInput_t40_MobileInput_AddAxes_m133_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 135/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo MobileInput_t40_MobileInput_GetAxis_m134_ParameterInfos[] = 
{
	{"name", 0, 134217792, 0, &String_t_0_0_0},
	{"raw", 1, 134217793, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Single_t254_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::GetAxis(System.String,System.Boolean)
extern const MethodInfo MobileInput_GetAxis_m134_MethodInfo = 
{
	"GetAxis"/* name */
	, (methodPointerType)&MobileInput_GetAxis_m134/* method */
	, &MobileInput_t40_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254_Object_t_SByte_t274/* invoker_method */
	, MobileInput_t40_MobileInput_GetAxis_m134_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 136/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo MobileInput_t40_MobileInput_SetButtonDown_m135_ParameterInfos[] = 
{
	{"name", 0, 134217794, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::SetButtonDown(System.String)
extern const MethodInfo MobileInput_SetButtonDown_m135_MethodInfo = 
{
	"SetButtonDown"/* name */
	, (methodPointerType)&MobileInput_SetButtonDown_m135/* method */
	, &MobileInput_t40_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, MobileInput_t40_MobileInput_SetButtonDown_m135_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 137/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo MobileInput_t40_MobileInput_SetButtonUp_m136_ParameterInfos[] = 
{
	{"name", 0, 134217795, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::SetButtonUp(System.String)
extern const MethodInfo MobileInput_SetButtonUp_m136_MethodInfo = 
{
	"SetButtonUp"/* name */
	, (methodPointerType)&MobileInput_SetButtonUp_m136/* method */
	, &MobileInput_t40_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, MobileInput_t40_MobileInput_SetButtonUp_m136_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 138/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo MobileInput_t40_MobileInput_SetAxisPositive_m137_ParameterInfos[] = 
{
	{"name", 0, 134217796, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::SetAxisPositive(System.String)
extern const MethodInfo MobileInput_SetAxisPositive_m137_MethodInfo = 
{
	"SetAxisPositive"/* name */
	, (methodPointerType)&MobileInput_SetAxisPositive_m137/* method */
	, &MobileInput_t40_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, MobileInput_t40_MobileInput_SetAxisPositive_m137_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 139/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo MobileInput_t40_MobileInput_SetAxisNegative_m138_ParameterInfos[] = 
{
	{"name", 0, 134217797, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::SetAxisNegative(System.String)
extern const MethodInfo MobileInput_SetAxisNegative_m138_MethodInfo = 
{
	"SetAxisNegative"/* name */
	, (methodPointerType)&MobileInput_SetAxisNegative_m138/* method */
	, &MobileInput_t40_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, MobileInput_t40_MobileInput_SetAxisNegative_m138_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 140/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo MobileInput_t40_MobileInput_SetAxisZero_m139_ParameterInfos[] = 
{
	{"name", 0, 134217798, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::SetAxisZero(System.String)
extern const MethodInfo MobileInput_SetAxisZero_m139_MethodInfo = 
{
	"SetAxisZero"/* name */
	, (methodPointerType)&MobileInput_SetAxisZero_m139/* method */
	, &MobileInput_t40_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, MobileInput_t40_MobileInput_SetAxisZero_m139_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 141/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo MobileInput_t40_MobileInput_SetAxis_m140_ParameterInfos[] = 
{
	{"name", 0, 134217799, 0, &String_t_0_0_0},
	{"value", 1, 134217800, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::SetAxis(System.String,System.Single)
extern const MethodInfo MobileInput_SetAxis_m140_MethodInfo = 
{
	"SetAxis"/* name */
	, (methodPointerType)&MobileInput_SetAxis_m140/* method */
	, &MobileInput_t40_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Single_t254/* invoker_method */
	, MobileInput_t40_MobileInput_SetAxis_m140_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 142/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo MobileInput_t40_MobileInput_GetButtonDown_m141_ParameterInfos[] = 
{
	{"name", 0, 134217801, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::GetButtonDown(System.String)
extern const MethodInfo MobileInput_GetButtonDown_m141_MethodInfo = 
{
	"GetButtonDown"/* name */
	, (methodPointerType)&MobileInput_GetButtonDown_m141/* method */
	, &MobileInput_t40_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, MobileInput_t40_MobileInput_GetButtonDown_m141_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 143/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo MobileInput_t40_MobileInput_GetButtonUp_m142_ParameterInfos[] = 
{
	{"name", 0, 134217802, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::GetButtonUp(System.String)
extern const MethodInfo MobileInput_GetButtonUp_m142_MethodInfo = 
{
	"GetButtonUp"/* name */
	, (methodPointerType)&MobileInput_GetButtonUp_m142/* method */
	, &MobileInput_t40_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, MobileInput_t40_MobileInput_GetButtonUp_m142_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 144/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo MobileInput_t40_MobileInput_GetButton_m143_ParameterInfos[] = 
{
	{"name", 0, 134217803, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::GetButton(System.String)
extern const MethodInfo MobileInput_GetButton_m143_MethodInfo = 
{
	"GetButton"/* name */
	, (methodPointerType)&MobileInput_GetButton_m143/* method */
	, &MobileInput_t40_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, MobileInput_t40_MobileInput_GetButton_m143_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 145/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Vector3_t4 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::MousePosition()
extern const MethodInfo MobileInput_MousePosition_m144_MethodInfo = 
{
	"MousePosition"/* name */
	, (methodPointerType)&MobileInput_MousePosition_m144/* method */
	, &MobileInput_t40_il2cpp_TypeInfo/* declaring_type */
	, &Vector3_t4_0_0_0/* return_type */
	, RuntimeInvoker_Vector3_t4/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 146/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MobileInput_t40_MethodInfos[] =
{
	&MobileInput__ctor_m131_MethodInfo,
	&MobileInput_AddButton_m132_MethodInfo,
	&MobileInput_AddAxes_m133_MethodInfo,
	&MobileInput_GetAxis_m134_MethodInfo,
	&MobileInput_SetButtonDown_m135_MethodInfo,
	&MobileInput_SetButtonUp_m136_MethodInfo,
	&MobileInput_SetAxisPositive_m137_MethodInfo,
	&MobileInput_SetAxisNegative_m138_MethodInfo,
	&MobileInput_SetAxisZero_m139_MethodInfo,
	&MobileInput_SetAxis_m140_MethodInfo,
	&MobileInput_GetButtonDown_m141_MethodInfo,
	&MobileInput_GetButtonUp_m142_MethodInfo,
	&MobileInput_GetButton_m143_MethodInfo,
	&MobileInput_MousePosition_m144_MethodInfo,
	NULL
};
extern const MethodInfo MobileInput_GetAxis_m134_MethodInfo;
extern const MethodInfo MobileInput_GetButton_m143_MethodInfo;
extern const MethodInfo MobileInput_GetButtonDown_m141_MethodInfo;
extern const MethodInfo MobileInput_GetButtonUp_m142_MethodInfo;
extern const MethodInfo MobileInput_SetButtonDown_m135_MethodInfo;
extern const MethodInfo MobileInput_SetButtonUp_m136_MethodInfo;
extern const MethodInfo MobileInput_SetAxisPositive_m137_MethodInfo;
extern const MethodInfo MobileInput_SetAxisNegative_m138_MethodInfo;
extern const MethodInfo MobileInput_SetAxisZero_m139_MethodInfo;
extern const MethodInfo MobileInput_SetAxis_m140_MethodInfo;
extern const MethodInfo MobileInput_MousePosition_m144_MethodInfo;
static const Il2CppMethodReference MobileInput_t40_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&MobileInput_GetAxis_m134_MethodInfo,
	&MobileInput_GetButton_m143_MethodInfo,
	&MobileInput_GetButtonDown_m141_MethodInfo,
	&MobileInput_GetButtonUp_m142_MethodInfo,
	&MobileInput_SetButtonDown_m135_MethodInfo,
	&MobileInput_SetButtonUp_m136_MethodInfo,
	&MobileInput_SetAxisPositive_m137_MethodInfo,
	&MobileInput_SetAxisNegative_m138_MethodInfo,
	&MobileInput_SetAxisZero_m139_MethodInfo,
	&MobileInput_SetAxis_m140_MethodInfo,
	&MobileInput_MousePosition_m144_MethodInfo,
};
static bool MobileInput_t40_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType MobileInput_t40_0_0_0;
extern const Il2CppType MobileInput_t40_1_0_0;
extern const Il2CppType VirtualInput_t34_0_0_0;
struct MobileInput_t40;
const Il2CppTypeDefinitionMetadata MobileInput_t40_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &VirtualInput_t34_0_0_0/* parent */
	, MobileInput_t40_VTable/* vtableMethods */
	, MobileInput_t40_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo MobileInput_t40_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "MobileInput"/* name */
	, "UnityStandardAssets.CrossPlatformInput.PlatformSpecific"/* namespaze */
	, MobileInput_t40_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &MobileInput_t40_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MobileInput_t40_0_0_0/* byval_arg */
	, &MobileInput_t40_1_0_0/* this_arg */
	, &MobileInput_t40_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MobileInput_t40)/* instance_size */
	, sizeof (MobileInput_t40)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 14/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 15/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_11.h"
// Metadata Definition UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput
extern TypeInfo StandaloneInput_t41_il2cpp_TypeInfo;
// UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_11MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::.ctor()
extern const MethodInfo StandaloneInput__ctor_m145_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&StandaloneInput__ctor_m145/* method */
	, &StandaloneInput_t41_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 147/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo StandaloneInput_t41_StandaloneInput_GetAxis_m146_ParameterInfos[] = 
{
	{"name", 0, 134217804, 0, &String_t_0_0_0},
	{"raw", 1, 134217805, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Single_t254_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::GetAxis(System.String,System.Boolean)
extern const MethodInfo StandaloneInput_GetAxis_m146_MethodInfo = 
{
	"GetAxis"/* name */
	, (methodPointerType)&StandaloneInput_GetAxis_m146/* method */
	, &StandaloneInput_t41_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254_Object_t_SByte_t274/* invoker_method */
	, StandaloneInput_t41_StandaloneInput_GetAxis_m146_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 148/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo StandaloneInput_t41_StandaloneInput_GetButton_m147_ParameterInfos[] = 
{
	{"name", 0, 134217806, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::GetButton(System.String)
extern const MethodInfo StandaloneInput_GetButton_m147_MethodInfo = 
{
	"GetButton"/* name */
	, (methodPointerType)&StandaloneInput_GetButton_m147/* method */
	, &StandaloneInput_t41_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, StandaloneInput_t41_StandaloneInput_GetButton_m147_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 149/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo StandaloneInput_t41_StandaloneInput_GetButtonDown_m148_ParameterInfos[] = 
{
	{"name", 0, 134217807, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::GetButtonDown(System.String)
extern const MethodInfo StandaloneInput_GetButtonDown_m148_MethodInfo = 
{
	"GetButtonDown"/* name */
	, (methodPointerType)&StandaloneInput_GetButtonDown_m148/* method */
	, &StandaloneInput_t41_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, StandaloneInput_t41_StandaloneInput_GetButtonDown_m148_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 150/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo StandaloneInput_t41_StandaloneInput_GetButtonUp_m149_ParameterInfos[] = 
{
	{"name", 0, 134217808, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::GetButtonUp(System.String)
extern const MethodInfo StandaloneInput_GetButtonUp_m149_MethodInfo = 
{
	"GetButtonUp"/* name */
	, (methodPointerType)&StandaloneInput_GetButtonUp_m149/* method */
	, &StandaloneInput_t41_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, StandaloneInput_t41_StandaloneInput_GetButtonUp_m149_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 151/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo StandaloneInput_t41_StandaloneInput_SetButtonDown_m150_ParameterInfos[] = 
{
	{"name", 0, 134217809, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::SetButtonDown(System.String)
extern const MethodInfo StandaloneInput_SetButtonDown_m150_MethodInfo = 
{
	"SetButtonDown"/* name */
	, (methodPointerType)&StandaloneInput_SetButtonDown_m150/* method */
	, &StandaloneInput_t41_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, StandaloneInput_t41_StandaloneInput_SetButtonDown_m150_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 152/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo StandaloneInput_t41_StandaloneInput_SetButtonUp_m151_ParameterInfos[] = 
{
	{"name", 0, 134217810, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::SetButtonUp(System.String)
extern const MethodInfo StandaloneInput_SetButtonUp_m151_MethodInfo = 
{
	"SetButtonUp"/* name */
	, (methodPointerType)&StandaloneInput_SetButtonUp_m151/* method */
	, &StandaloneInput_t41_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, StandaloneInput_t41_StandaloneInput_SetButtonUp_m151_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 153/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo StandaloneInput_t41_StandaloneInput_SetAxisPositive_m152_ParameterInfos[] = 
{
	{"name", 0, 134217811, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::SetAxisPositive(System.String)
extern const MethodInfo StandaloneInput_SetAxisPositive_m152_MethodInfo = 
{
	"SetAxisPositive"/* name */
	, (methodPointerType)&StandaloneInput_SetAxisPositive_m152/* method */
	, &StandaloneInput_t41_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, StandaloneInput_t41_StandaloneInput_SetAxisPositive_m152_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 154/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo StandaloneInput_t41_StandaloneInput_SetAxisNegative_m153_ParameterInfos[] = 
{
	{"name", 0, 134217812, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::SetAxisNegative(System.String)
extern const MethodInfo StandaloneInput_SetAxisNegative_m153_MethodInfo = 
{
	"SetAxisNegative"/* name */
	, (methodPointerType)&StandaloneInput_SetAxisNegative_m153/* method */
	, &StandaloneInput_t41_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, StandaloneInput_t41_StandaloneInput_SetAxisNegative_m153_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 155/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo StandaloneInput_t41_StandaloneInput_SetAxisZero_m154_ParameterInfos[] = 
{
	{"name", 0, 134217813, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::SetAxisZero(System.String)
extern const MethodInfo StandaloneInput_SetAxisZero_m154_MethodInfo = 
{
	"SetAxisZero"/* name */
	, (methodPointerType)&StandaloneInput_SetAxisZero_m154/* method */
	, &StandaloneInput_t41_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, StandaloneInput_t41_StandaloneInput_SetAxisZero_m154_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 156/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo StandaloneInput_t41_StandaloneInput_SetAxis_m155_ParameterInfos[] = 
{
	{"name", 0, 134217814, 0, &String_t_0_0_0},
	{"value", 1, 134217815, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::SetAxis(System.String,System.Single)
extern const MethodInfo StandaloneInput_SetAxis_m155_MethodInfo = 
{
	"SetAxis"/* name */
	, (methodPointerType)&StandaloneInput_SetAxis_m155/* method */
	, &StandaloneInput_t41_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Single_t254/* invoker_method */
	, StandaloneInput_t41_StandaloneInput_SetAxis_m155_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 157/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Vector3_t4 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::MousePosition()
extern const MethodInfo StandaloneInput_MousePosition_m156_MethodInfo = 
{
	"MousePosition"/* name */
	, (methodPointerType)&StandaloneInput_MousePosition_m156/* method */
	, &StandaloneInput_t41_il2cpp_TypeInfo/* declaring_type */
	, &Vector3_t4_0_0_0/* return_type */
	, RuntimeInvoker_Vector3_t4/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 158/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* StandaloneInput_t41_MethodInfos[] =
{
	&StandaloneInput__ctor_m145_MethodInfo,
	&StandaloneInput_GetAxis_m146_MethodInfo,
	&StandaloneInput_GetButton_m147_MethodInfo,
	&StandaloneInput_GetButtonDown_m148_MethodInfo,
	&StandaloneInput_GetButtonUp_m149_MethodInfo,
	&StandaloneInput_SetButtonDown_m150_MethodInfo,
	&StandaloneInput_SetButtonUp_m151_MethodInfo,
	&StandaloneInput_SetAxisPositive_m152_MethodInfo,
	&StandaloneInput_SetAxisNegative_m153_MethodInfo,
	&StandaloneInput_SetAxisZero_m154_MethodInfo,
	&StandaloneInput_SetAxis_m155_MethodInfo,
	&StandaloneInput_MousePosition_m156_MethodInfo,
	NULL
};
extern const MethodInfo StandaloneInput_GetAxis_m146_MethodInfo;
extern const MethodInfo StandaloneInput_GetButton_m147_MethodInfo;
extern const MethodInfo StandaloneInput_GetButtonDown_m148_MethodInfo;
extern const MethodInfo StandaloneInput_GetButtonUp_m149_MethodInfo;
extern const MethodInfo StandaloneInput_SetButtonDown_m150_MethodInfo;
extern const MethodInfo StandaloneInput_SetButtonUp_m151_MethodInfo;
extern const MethodInfo StandaloneInput_SetAxisPositive_m152_MethodInfo;
extern const MethodInfo StandaloneInput_SetAxisNegative_m153_MethodInfo;
extern const MethodInfo StandaloneInput_SetAxisZero_m154_MethodInfo;
extern const MethodInfo StandaloneInput_SetAxis_m155_MethodInfo;
extern const MethodInfo StandaloneInput_MousePosition_m156_MethodInfo;
static const Il2CppMethodReference StandaloneInput_t41_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&StandaloneInput_GetAxis_m146_MethodInfo,
	&StandaloneInput_GetButton_m147_MethodInfo,
	&StandaloneInput_GetButtonDown_m148_MethodInfo,
	&StandaloneInput_GetButtonUp_m149_MethodInfo,
	&StandaloneInput_SetButtonDown_m150_MethodInfo,
	&StandaloneInput_SetButtonUp_m151_MethodInfo,
	&StandaloneInput_SetAxisPositive_m152_MethodInfo,
	&StandaloneInput_SetAxisNegative_m153_MethodInfo,
	&StandaloneInput_SetAxisZero_m154_MethodInfo,
	&StandaloneInput_SetAxis_m155_MethodInfo,
	&StandaloneInput_MousePosition_m156_MethodInfo,
};
static bool StandaloneInput_t41_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType StandaloneInput_t41_0_0_0;
extern const Il2CppType StandaloneInput_t41_1_0_0;
struct StandaloneInput_t41;
const Il2CppTypeDefinitionMetadata StandaloneInput_t41_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &VirtualInput_t34_0_0_0/* parent */
	, StandaloneInput_t41_VTable/* vtableMethods */
	, StandaloneInput_t41_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo StandaloneInput_t41_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "StandaloneInput"/* name */
	, "UnityStandardAssets.CrossPlatformInput.PlatformSpecific"/* namespaze */
	, StandaloneInput_t41_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &StandaloneInput_t41_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &StandaloneInput_t41_0_0_0/* byval_arg */
	, &StandaloneInput_t41_1_0_0/* this_arg */
	, &StandaloneInput_t41_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StandaloneInput_t41)/* instance_size */
	, sizeof (StandaloneInput_t41)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 12/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 15/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisOptions
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_12.h"
// Metadata Definition UnityStandardAssets.CrossPlatformInput.TiltInput/AxisOptions
extern TypeInfo AxisOptions_t42_il2cpp_TypeInfo;
// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisOptions
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_12MethodDeclarations.h"
static const MethodInfo* AxisOptions_t42_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference AxisOptions_t42_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool AxisOptions_t42_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair AxisOptions_t42_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType AxisOptions_t42_0_0_0;
extern const Il2CppType AxisOptions_t42_1_0_0;
extern TypeInfo TiltInput_t45_il2cpp_TypeInfo;
extern const Il2CppType TiltInput_t45_0_0_0;
const Il2CppTypeDefinitionMetadata AxisOptions_t42_DefinitionMetadata = 
{
	&TiltInput_t45_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AxisOptions_t42_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, AxisOptions_t42_VTable/* vtableMethods */
	, AxisOptions_t42_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 133/* fieldStart */

};
TypeInfo AxisOptions_t42_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "AxisOptions"/* name */
	, ""/* namespaze */
	, AxisOptions_t42_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AxisOptions_t42_0_0_0/* byval_arg */
	, &AxisOptions_t42_1_0_0/* this_arg */
	, &AxisOptions_t42_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AxisOptions_t42)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (AxisOptions_t42)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping/MappingType
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_13.h"
// Metadata Definition UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping/MappingType
extern TypeInfo MappingType_t43_il2cpp_TypeInfo;
// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping/MappingType
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_13MethodDeclarations.h"
static const MethodInfo* MappingType_t43_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference MappingType_t43_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool MappingType_t43_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair MappingType_t43_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType MappingType_t43_0_0_0;
extern const Il2CppType MappingType_t43_1_0_0;
extern TypeInfo AxisMapping_t44_il2cpp_TypeInfo;
extern const Il2CppType AxisMapping_t44_0_0_0;
const Il2CppTypeDefinitionMetadata MappingType_t43_DefinitionMetadata = 
{
	&AxisMapping_t44_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MappingType_t43_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, MappingType_t43_VTable/* vtableMethods */
	, MappingType_t43_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 136/* fieldStart */

};
TypeInfo MappingType_t43_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "MappingType"/* name */
	, ""/* namespaze */
	, MappingType_t43_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MappingType_t43_0_0_0/* byval_arg */
	, &MappingType_t43_1_0_0/* this_arg */
	, &MappingType_t43_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MappingType_t43)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (MappingType_t43)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_14.h"
// Metadata Definition UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping
// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_14MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping::.ctor()
extern const MethodInfo AxisMapping__ctor_m157_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AxisMapping__ctor_m157/* method */
	, &AxisMapping_t44_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 163/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AxisMapping_t44_MethodInfos[] =
{
	&AxisMapping__ctor_m157_MethodInfo,
	NULL
};
static const Il2CppType* AxisMapping_t44_il2cpp_TypeInfo__nestedTypes[1] =
{
	&MappingType_t43_0_0_0,
};
static const Il2CppMethodReference AxisMapping_t44_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool AxisMapping_t44_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType AxisMapping_t44_1_0_0;
struct AxisMapping_t44;
const Il2CppTypeDefinitionMetadata AxisMapping_t44_DefinitionMetadata = 
{
	&TiltInput_t45_0_0_0/* declaringType */
	, AxisMapping_t44_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, AxisMapping_t44_VTable/* vtableMethods */
	, AxisMapping_t44_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 141/* fieldStart */

};
TypeInfo AxisMapping_t44_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "AxisMapping"/* name */
	, ""/* namespaze */
	, AxisMapping_t44_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &AxisMapping_t44_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AxisMapping_t44_0_0_0/* byval_arg */
	, &AxisMapping_t44_1_0_0/* this_arg */
	, &AxisMapping_t44_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AxisMapping_t44)/* instance_size */
	, sizeof (AxisMapping_t44)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056770/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.CrossPlatformInput.TiltInput
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_15.h"
// Metadata Definition UnityStandardAssets.CrossPlatformInput.TiltInput
// UnityStandardAssets.CrossPlatformInput.TiltInput
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_15MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.TiltInput::.ctor()
extern const MethodInfo TiltInput__ctor_m158_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TiltInput__ctor_m158/* method */
	, &TiltInput_t45_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 159/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.TiltInput::OnEnable()
extern const MethodInfo TiltInput_OnEnable_m159_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&TiltInput_OnEnable_m159/* method */
	, &TiltInput_t45_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 160/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.TiltInput::Update()
extern const MethodInfo TiltInput_Update_m160_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&TiltInput_Update_m160/* method */
	, &TiltInput_t45_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 161/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.TiltInput::OnDisable()
extern const MethodInfo TiltInput_OnDisable_m161_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&TiltInput_OnDisable_m161/* method */
	, &TiltInput_t45_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 162/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TiltInput_t45_MethodInfos[] =
{
	&TiltInput__ctor_m158_MethodInfo,
	&TiltInput_OnEnable_m159_MethodInfo,
	&TiltInput_Update_m160_MethodInfo,
	&TiltInput_OnDisable_m161_MethodInfo,
	NULL
};
static const Il2CppType* TiltInput_t45_il2cpp_TypeInfo__nestedTypes[2] =
{
	&AxisOptions_t42_0_0_0,
	&AxisMapping_t44_0_0_0,
};
static const Il2CppMethodReference TiltInput_t45_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool TiltInput_t45_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType TiltInput_t45_1_0_0;
struct TiltInput_t45;
const Il2CppTypeDefinitionMetadata TiltInput_t45_DefinitionMetadata = 
{
	NULL/* declaringType */
	, TiltInput_t45_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, TiltInput_t45_VTable/* vtableMethods */
	, TiltInput_t45_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 143/* fieldStart */

};
TypeInfo TiltInput_t45_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "TiltInput"/* name */
	, "UnityStandardAssets.CrossPlatformInput"/* namespaze */
	, TiltInput_t45_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TiltInput_t45_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TiltInput_t45_0_0_0/* byval_arg */
	, &TiltInput_t45_1_0_0/* this_arg */
	, &TiltInput_t45_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TiltInput_t45)/* instance_size */
	, sizeof (TiltInput_t45)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.CrossPlatformInput.TouchPad/AxisOption
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_16.h"
// Metadata Definition UnityStandardAssets.CrossPlatformInput.TouchPad/AxisOption
extern TypeInfo AxisOption_t46_il2cpp_TypeInfo;
// UnityStandardAssets.CrossPlatformInput.TouchPad/AxisOption
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_16MethodDeclarations.h"
static const MethodInfo* AxisOption_t46_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference AxisOption_t46_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool AxisOption_t46_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair AxisOption_t46_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType AxisOption_t46_0_0_0;
extern const Il2CppType AxisOption_t46_1_0_0;
extern TypeInfo TouchPad_t49_il2cpp_TypeInfo;
extern const Il2CppType TouchPad_t49_0_0_0;
const Il2CppTypeDefinitionMetadata AxisOption_t46_DefinitionMetadata = 
{
	&TouchPad_t49_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AxisOption_t46_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, AxisOption_t46_VTable/* vtableMethods */
	, AxisOption_t46_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 148/* fieldStart */

};
TypeInfo AxisOption_t46_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "AxisOption"/* name */
	, ""/* namespaze */
	, AxisOption_t46_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AxisOption_t46_0_0_0/* byval_arg */
	, &AxisOption_t46_1_0_0/* this_arg */
	, &AxisOption_t46_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AxisOption_t46)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (AxisOption_t46)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityStandardAssets.CrossPlatformInput.TouchPad/ControlStyle
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_17.h"
// Metadata Definition UnityStandardAssets.CrossPlatformInput.TouchPad/ControlStyle
extern TypeInfo ControlStyle_t47_il2cpp_TypeInfo;
// UnityStandardAssets.CrossPlatformInput.TouchPad/ControlStyle
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_17MethodDeclarations.h"
static const MethodInfo* ControlStyle_t47_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference ControlStyle_t47_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool ControlStyle_t47_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ControlStyle_t47_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType ControlStyle_t47_0_0_0;
extern const Il2CppType ControlStyle_t47_1_0_0;
const Il2CppTypeDefinitionMetadata ControlStyle_t47_DefinitionMetadata = 
{
	&TouchPad_t49_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ControlStyle_t47_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, ControlStyle_t47_VTable/* vtableMethods */
	, ControlStyle_t47_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 152/* fieldStart */

};
TypeInfo ControlStyle_t47_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "ControlStyle"/* name */
	, ""/* namespaze */
	, ControlStyle_t47_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ControlStyle_t47_0_0_0/* byval_arg */
	, &ControlStyle_t47_1_0_0/* this_arg */
	, &ControlStyle_t47_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ControlStyle_t47)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ControlStyle_t47)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityStandardAssets.CrossPlatformInput.TouchPad
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_18.h"
// Metadata Definition UnityStandardAssets.CrossPlatformInput.TouchPad
// UnityStandardAssets.CrossPlatformInput.TouchPad
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_18MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.TouchPad::.ctor()
extern const MethodInfo TouchPad__ctor_m162_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TouchPad__ctor_m162/* method */
	, &TouchPad_t49_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 164/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.TouchPad::OnEnable()
extern const MethodInfo TouchPad_OnEnable_m163_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&TouchPad_OnEnable_m163/* method */
	, &TouchPad_t49_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 165/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.TouchPad::CreateVirtualAxes()
extern const MethodInfo TouchPad_CreateVirtualAxes_m164_MethodInfo = 
{
	"CreateVirtualAxes"/* name */
	, (methodPointerType)&TouchPad_CreateVirtualAxes_m164/* method */
	, &TouchPad_t49_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 166/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector3_t4_0_0_0;
static const ParameterInfo TouchPad_t49_TouchPad_UpdateVirtualAxes_m165_ParameterInfos[] = 
{
	{"value", 0, 134217816, 0, &Vector3_t4_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Vector3_t4 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.TouchPad::UpdateVirtualAxes(UnityEngine.Vector3)
extern const MethodInfo TouchPad_UpdateVirtualAxes_m165_MethodInfo = 
{
	"UpdateVirtualAxes"/* name */
	, (methodPointerType)&TouchPad_UpdateVirtualAxes_m165/* method */
	, &TouchPad_t49_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Vector3_t4/* invoker_method */
	, TouchPad_t49_TouchPad_UpdateVirtualAxes_m165_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 167/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t215_0_0_0;
static const ParameterInfo TouchPad_t49_TouchPad_OnPointerDown_m166_ParameterInfos[] = 
{
	{"data", 0, 134217817, 0, &PointerEventData_t215_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.TouchPad::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern const MethodInfo TouchPad_OnPointerDown_m166_MethodInfo = 
{
	"OnPointerDown"/* name */
	, (methodPointerType)&TouchPad_OnPointerDown_m166/* method */
	, &TouchPad_t49_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, TouchPad_t49_TouchPad_OnPointerDown_m166_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 168/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.TouchPad::Update()
extern const MethodInfo TouchPad_Update_m167_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&TouchPad_Update_m167/* method */
	, &TouchPad_t49_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 169/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t215_0_0_0;
static const ParameterInfo TouchPad_t49_TouchPad_OnPointerUp_m168_ParameterInfos[] = 
{
	{"data", 0, 134217818, 0, &PointerEventData_t215_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.TouchPad::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern const MethodInfo TouchPad_OnPointerUp_m168_MethodInfo = 
{
	"OnPointerUp"/* name */
	, (methodPointerType)&TouchPad_OnPointerUp_m168/* method */
	, &TouchPad_t49_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, TouchPad_t49_TouchPad_OnPointerUp_m168_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 170/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.TouchPad::OnDisable()
extern const MethodInfo TouchPad_OnDisable_m169_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&TouchPad_OnDisable_m169/* method */
	, &TouchPad_t49_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 171/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TouchPad_t49_MethodInfos[] =
{
	&TouchPad__ctor_m162_MethodInfo,
	&TouchPad_OnEnable_m163_MethodInfo,
	&TouchPad_CreateVirtualAxes_m164_MethodInfo,
	&TouchPad_UpdateVirtualAxes_m165_MethodInfo,
	&TouchPad_OnPointerDown_m166_MethodInfo,
	&TouchPad_Update_m167_MethodInfo,
	&TouchPad_OnPointerUp_m168_MethodInfo,
	&TouchPad_OnDisable_m169_MethodInfo,
	NULL
};
static const Il2CppType* TouchPad_t49_il2cpp_TypeInfo__nestedTypes[2] =
{
	&AxisOption_t46_0_0_0,
	&ControlStyle_t47_0_0_0,
};
extern const MethodInfo TouchPad_OnPointerDown_m166_MethodInfo;
extern const MethodInfo TouchPad_OnPointerUp_m168_MethodInfo;
static const Il2CppMethodReference TouchPad_t49_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	&TouchPad_OnPointerDown_m166_MethodInfo,
	&TouchPad_OnPointerUp_m168_MethodInfo,
};
static bool TouchPad_t49_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* TouchPad_t49_InterfacesTypeInfos[] = 
{
	&IPointerDownHandler_t280_0_0_0,
	&IEventSystemHandler_t281_0_0_0,
	&IPointerUpHandler_t282_0_0_0,
};
static Il2CppInterfaceOffsetPair TouchPad_t49_InterfacesOffsets[] = 
{
	{ &IPointerDownHandler_t280_0_0_0, 4},
	{ &IEventSystemHandler_t281_0_0_0, 5},
	{ &IPointerUpHandler_t282_0_0_0, 5},
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType TouchPad_t49_1_0_0;
struct TouchPad_t49;
const Il2CppTypeDefinitionMetadata TouchPad_t49_DefinitionMetadata = 
{
	NULL/* declaringType */
	, TouchPad_t49_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, TouchPad_t49_InterfacesTypeInfos/* implementedInterfaces */
	, TouchPad_t49_InterfacesOffsets/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, TouchPad_t49_VTable/* vtableMethods */
	, TouchPad_t49_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 156/* fieldStart */

};
TypeInfo TouchPad_t49_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "TouchPad"/* name */
	, "UnityStandardAssets.CrossPlatformInput"/* namespaze */
	, TouchPad_t49_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TouchPad_t49_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 52/* custom_attributes_cache */
	, &TouchPad_t49_0_0_0/* byval_arg */
	, &TouchPad_t49_1_0_0/* this_arg */
	, &TouchPad_t49_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TouchPad_t49)/* instance_size */
	, sizeof (TouchPad_t49)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 0/* property_count */
	, 18/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 6/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityStandardAssets.CrossPlatformInput.VirtualInput
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_10.h"
// Metadata Definition UnityStandardAssets.CrossPlatformInput.VirtualInput
extern TypeInfo VirtualInput_t34_il2cpp_TypeInfo;
// UnityStandardAssets.CrossPlatformInput.VirtualInput
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_10MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::.ctor()
extern const MethodInfo VirtualInput__ctor_m170_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&VirtualInput__ctor_m170/* method */
	, &VirtualInput_t34_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 172/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Vector3_t4 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.VirtualInput::get_virtualMousePosition()
extern const MethodInfo VirtualInput_get_virtualMousePosition_m171_MethodInfo = 
{
	"get_virtualMousePosition"/* name */
	, (methodPointerType)&VirtualInput_get_virtualMousePosition_m171/* method */
	, &VirtualInput_t34_il2cpp_TypeInfo/* declaring_type */
	, &Vector3_t4_0_0_0/* return_type */
	, RuntimeInvoker_Vector3_t4/* invoker_method */
	, NULL/* parameters */
	, 54/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 173/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector3_t4_0_0_0;
static const ParameterInfo VirtualInput_t34_VirtualInput_set_virtualMousePosition_m172_ParameterInfos[] = 
{
	{"value", 0, 134217819, 0, &Vector3_t4_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Vector3_t4 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::set_virtualMousePosition(UnityEngine.Vector3)
extern const MethodInfo VirtualInput_set_virtualMousePosition_m172_MethodInfo = 
{
	"set_virtualMousePosition"/* name */
	, (methodPointerType)&VirtualInput_set_virtualMousePosition_m172/* method */
	, &VirtualInput_t34_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Vector3_t4/* invoker_method */
	, VirtualInput_t34_VirtualInput_set_virtualMousePosition_m172_ParameterInfos/* parameters */
	, 55/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 174/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo VirtualInput_t34_VirtualInput_AxisExists_m173_ParameterInfos[] = 
{
	{"name", 0, 134217820, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityStandardAssets.CrossPlatformInput.VirtualInput::AxisExists(System.String)
extern const MethodInfo VirtualInput_AxisExists_m173_MethodInfo = 
{
	"AxisExists"/* name */
	, (methodPointerType)&VirtualInput_AxisExists_m173/* method */
	, &VirtualInput_t34_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, VirtualInput_t34_VirtualInput_AxisExists_m173_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 175/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo VirtualInput_t34_VirtualInput_ButtonExists_m174_ParameterInfos[] = 
{
	{"name", 0, 134217821, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityStandardAssets.CrossPlatformInput.VirtualInput::ButtonExists(System.String)
extern const MethodInfo VirtualInput_ButtonExists_m174_MethodInfo = 
{
	"ButtonExists"/* name */
	, (methodPointerType)&VirtualInput_ButtonExists_m174/* method */
	, &VirtualInput_t34_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, VirtualInput_t34_VirtualInput_ButtonExists_m174_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 176/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType VirtualAxis_t30_0_0_0;
static const ParameterInfo VirtualInput_t34_VirtualInput_RegisterVirtualAxis_m175_ParameterInfos[] = 
{
	{"axis", 0, 134217822, 0, &VirtualAxis_t30_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::RegisterVirtualAxis(UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis)
extern const MethodInfo VirtualInput_RegisterVirtualAxis_m175_MethodInfo = 
{
	"RegisterVirtualAxis"/* name */
	, (methodPointerType)&VirtualInput_RegisterVirtualAxis_m175/* method */
	, &VirtualInput_t34_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, VirtualInput_t34_VirtualInput_RegisterVirtualAxis_m175_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 177/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType VirtualButton_t33_0_0_0;
static const ParameterInfo VirtualInput_t34_VirtualInput_RegisterVirtualButton_m176_ParameterInfos[] = 
{
	{"button", 0, 134217823, 0, &VirtualButton_t33_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::RegisterVirtualButton(UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton)
extern const MethodInfo VirtualInput_RegisterVirtualButton_m176_MethodInfo = 
{
	"RegisterVirtualButton"/* name */
	, (methodPointerType)&VirtualInput_RegisterVirtualButton_m176/* method */
	, &VirtualInput_t34_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, VirtualInput_t34_VirtualInput_RegisterVirtualButton_m176_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 178/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo VirtualInput_t34_VirtualInput_UnRegisterVirtualAxis_m177_ParameterInfos[] = 
{
	{"name", 0, 134217824, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::UnRegisterVirtualAxis(System.String)
extern const MethodInfo VirtualInput_UnRegisterVirtualAxis_m177_MethodInfo = 
{
	"UnRegisterVirtualAxis"/* name */
	, (methodPointerType)&VirtualInput_UnRegisterVirtualAxis_m177/* method */
	, &VirtualInput_t34_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, VirtualInput_t34_VirtualInput_UnRegisterVirtualAxis_m177_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 179/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo VirtualInput_t34_VirtualInput_UnRegisterVirtualButton_m178_ParameterInfos[] = 
{
	{"name", 0, 134217825, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::UnRegisterVirtualButton(System.String)
extern const MethodInfo VirtualInput_UnRegisterVirtualButton_m178_MethodInfo = 
{
	"UnRegisterVirtualButton"/* name */
	, (methodPointerType)&VirtualInput_UnRegisterVirtualButton_m178/* method */
	, &VirtualInput_t34_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, VirtualInput_t34_VirtualInput_UnRegisterVirtualButton_m178_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 180/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo VirtualInput_t34_VirtualInput_VirtualAxisReference_m179_ParameterInfos[] = 
{
	{"name", 0, 134217826, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis UnityStandardAssets.CrossPlatformInput.VirtualInput::VirtualAxisReference(System.String)
extern const MethodInfo VirtualInput_VirtualAxisReference_m179_MethodInfo = 
{
	"VirtualAxisReference"/* name */
	, (methodPointerType)&VirtualInput_VirtualAxisReference_m179/* method */
	, &VirtualInput_t34_il2cpp_TypeInfo/* declaring_type */
	, &VirtualAxis_t30_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, VirtualInput_t34_VirtualInput_VirtualAxisReference_m179_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 181/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo VirtualInput_t34_VirtualInput_SetVirtualMousePositionX_m180_ParameterInfos[] = 
{
	{"f", 0, 134217827, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::SetVirtualMousePositionX(System.Single)
extern const MethodInfo VirtualInput_SetVirtualMousePositionX_m180_MethodInfo = 
{
	"SetVirtualMousePositionX"/* name */
	, (methodPointerType)&VirtualInput_SetVirtualMousePositionX_m180/* method */
	, &VirtualInput_t34_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Single_t254/* invoker_method */
	, VirtualInput_t34_VirtualInput_SetVirtualMousePositionX_m180_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 182/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo VirtualInput_t34_VirtualInput_SetVirtualMousePositionY_m181_ParameterInfos[] = 
{
	{"f", 0, 134217828, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::SetVirtualMousePositionY(System.Single)
extern const MethodInfo VirtualInput_SetVirtualMousePositionY_m181_MethodInfo = 
{
	"SetVirtualMousePositionY"/* name */
	, (methodPointerType)&VirtualInput_SetVirtualMousePositionY_m181/* method */
	, &VirtualInput_t34_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Single_t254/* invoker_method */
	, VirtualInput_t34_VirtualInput_SetVirtualMousePositionY_m181_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 183/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo VirtualInput_t34_VirtualInput_SetVirtualMousePositionZ_m182_ParameterInfos[] = 
{
	{"f", 0, 134217829, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::SetVirtualMousePositionZ(System.Single)
extern const MethodInfo VirtualInput_SetVirtualMousePositionZ_m182_MethodInfo = 
{
	"SetVirtualMousePositionZ"/* name */
	, (methodPointerType)&VirtualInput_SetVirtualMousePositionZ_m182/* method */
	, &VirtualInput_t34_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Single_t254/* invoker_method */
	, VirtualInput_t34_VirtualInput_SetVirtualMousePositionZ_m182_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 184/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo VirtualInput_t34_VirtualInput_GetAxis_m1029_ParameterInfos[] = 
{
	{"name", 0, 134217830, 0, &String_t_0_0_0},
	{"raw", 1, 134217831, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Single_t254_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityStandardAssets.CrossPlatformInput.VirtualInput::GetAxis(System.String,System.Boolean)
extern const MethodInfo VirtualInput_GetAxis_m1029_MethodInfo = 
{
	"GetAxis"/* name */
	, NULL/* method */
	, &VirtualInput_t34_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254_Object_t_SByte_t274/* invoker_method */
	, VirtualInput_t34_VirtualInput_GetAxis_m1029_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 185/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo VirtualInput_t34_VirtualInput_GetButton_m1030_ParameterInfos[] = 
{
	{"name", 0, 134217832, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityStandardAssets.CrossPlatformInput.VirtualInput::GetButton(System.String)
extern const MethodInfo VirtualInput_GetButton_m1030_MethodInfo = 
{
	"GetButton"/* name */
	, NULL/* method */
	, &VirtualInput_t34_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, VirtualInput_t34_VirtualInput_GetButton_m1030_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 186/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo VirtualInput_t34_VirtualInput_GetButtonDown_m1031_ParameterInfos[] = 
{
	{"name", 0, 134217833, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityStandardAssets.CrossPlatformInput.VirtualInput::GetButtonDown(System.String)
extern const MethodInfo VirtualInput_GetButtonDown_m1031_MethodInfo = 
{
	"GetButtonDown"/* name */
	, NULL/* method */
	, &VirtualInput_t34_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, VirtualInput_t34_VirtualInput_GetButtonDown_m1031_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 187/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo VirtualInput_t34_VirtualInput_GetButtonUp_m1032_ParameterInfos[] = 
{
	{"name", 0, 134217834, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityStandardAssets.CrossPlatformInput.VirtualInput::GetButtonUp(System.String)
extern const MethodInfo VirtualInput_GetButtonUp_m1032_MethodInfo = 
{
	"GetButtonUp"/* name */
	, NULL/* method */
	, &VirtualInput_t34_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, VirtualInput_t34_VirtualInput_GetButtonUp_m1032_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 188/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo VirtualInput_t34_VirtualInput_SetButtonDown_m1033_ParameterInfos[] = 
{
	{"name", 0, 134217835, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::SetButtonDown(System.String)
extern const MethodInfo VirtualInput_SetButtonDown_m1033_MethodInfo = 
{
	"SetButtonDown"/* name */
	, NULL/* method */
	, &VirtualInput_t34_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, VirtualInput_t34_VirtualInput_SetButtonDown_m1033_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 189/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo VirtualInput_t34_VirtualInput_SetButtonUp_m1034_ParameterInfos[] = 
{
	{"name", 0, 134217836, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::SetButtonUp(System.String)
extern const MethodInfo VirtualInput_SetButtonUp_m1034_MethodInfo = 
{
	"SetButtonUp"/* name */
	, NULL/* method */
	, &VirtualInput_t34_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, VirtualInput_t34_VirtualInput_SetButtonUp_m1034_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 190/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo VirtualInput_t34_VirtualInput_SetAxisPositive_m1035_ParameterInfos[] = 
{
	{"name", 0, 134217837, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::SetAxisPositive(System.String)
extern const MethodInfo VirtualInput_SetAxisPositive_m1035_MethodInfo = 
{
	"SetAxisPositive"/* name */
	, NULL/* method */
	, &VirtualInput_t34_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, VirtualInput_t34_VirtualInput_SetAxisPositive_m1035_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 191/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo VirtualInput_t34_VirtualInput_SetAxisNegative_m1036_ParameterInfos[] = 
{
	{"name", 0, 134217838, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::SetAxisNegative(System.String)
extern const MethodInfo VirtualInput_SetAxisNegative_m1036_MethodInfo = 
{
	"SetAxisNegative"/* name */
	, NULL/* method */
	, &VirtualInput_t34_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, VirtualInput_t34_VirtualInput_SetAxisNegative_m1036_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 192/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo VirtualInput_t34_VirtualInput_SetAxisZero_m1037_ParameterInfos[] = 
{
	{"name", 0, 134217839, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::SetAxisZero(System.String)
extern const MethodInfo VirtualInput_SetAxisZero_m1037_MethodInfo = 
{
	"SetAxisZero"/* name */
	, NULL/* method */
	, &VirtualInput_t34_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, VirtualInput_t34_VirtualInput_SetAxisZero_m1037_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 193/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo VirtualInput_t34_VirtualInput_SetAxis_m1038_ParameterInfos[] = 
{
	{"name", 0, 134217840, 0, &String_t_0_0_0},
	{"value", 1, 134217841, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::SetAxis(System.String,System.Single)
extern const MethodInfo VirtualInput_SetAxis_m1038_MethodInfo = 
{
	"SetAxis"/* name */
	, NULL/* method */
	, &VirtualInput_t34_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Single_t254/* invoker_method */
	, VirtualInput_t34_VirtualInput_SetAxis_m1038_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 194/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Vector3_t4 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.VirtualInput::MousePosition()
extern const MethodInfo VirtualInput_MousePosition_m1039_MethodInfo = 
{
	"MousePosition"/* name */
	, NULL/* method */
	, &VirtualInput_t34_il2cpp_TypeInfo/* declaring_type */
	, &Vector3_t4_0_0_0/* return_type */
	, RuntimeInvoker_Vector3_t4/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 195/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* VirtualInput_t34_MethodInfos[] =
{
	&VirtualInput__ctor_m170_MethodInfo,
	&VirtualInput_get_virtualMousePosition_m171_MethodInfo,
	&VirtualInput_set_virtualMousePosition_m172_MethodInfo,
	&VirtualInput_AxisExists_m173_MethodInfo,
	&VirtualInput_ButtonExists_m174_MethodInfo,
	&VirtualInput_RegisterVirtualAxis_m175_MethodInfo,
	&VirtualInput_RegisterVirtualButton_m176_MethodInfo,
	&VirtualInput_UnRegisterVirtualAxis_m177_MethodInfo,
	&VirtualInput_UnRegisterVirtualButton_m178_MethodInfo,
	&VirtualInput_VirtualAxisReference_m179_MethodInfo,
	&VirtualInput_SetVirtualMousePositionX_m180_MethodInfo,
	&VirtualInput_SetVirtualMousePositionY_m181_MethodInfo,
	&VirtualInput_SetVirtualMousePositionZ_m182_MethodInfo,
	&VirtualInput_GetAxis_m1029_MethodInfo,
	&VirtualInput_GetButton_m1030_MethodInfo,
	&VirtualInput_GetButtonDown_m1031_MethodInfo,
	&VirtualInput_GetButtonUp_m1032_MethodInfo,
	&VirtualInput_SetButtonDown_m1033_MethodInfo,
	&VirtualInput_SetButtonUp_m1034_MethodInfo,
	&VirtualInput_SetAxisPositive_m1035_MethodInfo,
	&VirtualInput_SetAxisNegative_m1036_MethodInfo,
	&VirtualInput_SetAxisZero_m1037_MethodInfo,
	&VirtualInput_SetAxis_m1038_MethodInfo,
	&VirtualInput_MousePosition_m1039_MethodInfo,
	NULL
};
extern const MethodInfo VirtualInput_get_virtualMousePosition_m171_MethodInfo;
extern const MethodInfo VirtualInput_set_virtualMousePosition_m172_MethodInfo;
static const PropertyInfo VirtualInput_t34____virtualMousePosition_PropertyInfo = 
{
	&VirtualInput_t34_il2cpp_TypeInfo/* parent */
	, "virtualMousePosition"/* name */
	, &VirtualInput_get_virtualMousePosition_m171_MethodInfo/* get */
	, &VirtualInput_set_virtualMousePosition_m172_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* VirtualInput_t34_PropertyInfos[] =
{
	&VirtualInput_t34____virtualMousePosition_PropertyInfo,
	NULL
};
static const Il2CppMethodReference VirtualInput_t34_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static bool VirtualInput_t34_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType VirtualInput_t34_1_0_0;
struct VirtualInput_t34;
const Il2CppTypeDefinitionMetadata VirtualInput_t34_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, VirtualInput_t34_VTable/* vtableMethods */
	, VirtualInput_t34_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 174/* fieldStart */

};
TypeInfo VirtualInput_t34_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "VirtualInput"/* name */
	, "UnityStandardAssets.CrossPlatformInput"/* namespaze */
	, VirtualInput_t34_MethodInfos/* methods */
	, VirtualInput_t34_PropertyInfos/* properties */
	, NULL/* events */
	, &VirtualInput_t34_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &VirtualInput_t34_0_0_0/* byval_arg */
	, &VirtualInput_t34_1_0_0/* this_arg */
	, &VirtualInput_t34_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (VirtualInput_t34)/* instance_size */
	, sizeof (VirtualInput_t34)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 24/* method_count */
	, 1/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 15/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.ImageEffects.AAMode
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec.h"
// Metadata Definition UnityStandardAssets.ImageEffects.AAMode
extern TypeInfo AAMode_t53_il2cpp_TypeInfo;
// UnityStandardAssets.ImageEffects.AAMode
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffecMethodDeclarations.h"
static const MethodInfo* AAMode_t53_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference AAMode_t53_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool AAMode_t53_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair AAMode_t53_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType AAMode_t53_0_0_0;
extern const Il2CppType AAMode_t53_1_0_0;
const Il2CppTypeDefinitionMetadata AAMode_t53_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AAMode_t53_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, AAMode_t53_VTable/* vtableMethods */
	, AAMode_t53_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 178/* fieldStart */

};
TypeInfo AAMode_t53_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "AAMode"/* name */
	, "UnityStandardAssets.ImageEffects"/* namespaze */
	, AAMode_t53_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AAMode_t53_0_0_0/* byval_arg */
	, &AAMode_t53_1_0_0/* this_arg */
	, &AAMode_t53_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AAMode_t53)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (AAMode_t53)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityStandardAssets.ImageEffects.Antialiasing
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_0.h"
// Metadata Definition UnityStandardAssets.ImageEffects.Antialiasing
extern TypeInfo Antialiasing_t56_il2cpp_TypeInfo;
// UnityStandardAssets.ImageEffects.Antialiasing
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_0MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.Antialiasing::.ctor()
extern const MethodInfo Antialiasing__ctor_m183_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Antialiasing__ctor_m183/* method */
	, &Antialiasing_t56_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 196/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Material_t55_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Material UnityStandardAssets.ImageEffects.Antialiasing::CurrentAAMaterial()
extern const MethodInfo Antialiasing_CurrentAAMaterial_m184_MethodInfo = 
{
	"CurrentAAMaterial"/* name */
	, (methodPointerType)&Antialiasing_CurrentAAMaterial_m184/* method */
	, &Antialiasing_t56_il2cpp_TypeInfo/* declaring_type */
	, &Material_t55_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 197/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityStandardAssets.ImageEffects.Antialiasing::CheckResources()
extern const MethodInfo Antialiasing_CheckResources_m185_MethodInfo = 
{
	"CheckResources"/* name */
	, (methodPointerType)&Antialiasing_CheckResources_m185/* method */
	, &Antialiasing_t56_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 198/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
static const ParameterInfo Antialiasing_t56_Antialiasing_OnRenderImage_m186_ParameterInfos[] = 
{
	{"source", 0, 134217842, 0, &RenderTexture_t101_0_0_0},
	{"destination", 1, 134217843, 0, &RenderTexture_t101_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.Antialiasing::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern const MethodInfo Antialiasing_OnRenderImage_m186_MethodInfo = 
{
	"OnRenderImage"/* name */
	, (methodPointerType)&Antialiasing_OnRenderImage_m186/* method */
	, &Antialiasing_t56_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, Antialiasing_t56_Antialiasing_OnRenderImage_m186_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 199/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Antialiasing_t56_MethodInfos[] =
{
	&Antialiasing__ctor_m183_MethodInfo,
	&Antialiasing_CurrentAAMaterial_m184_MethodInfo,
	&Antialiasing_CheckResources_m185_MethodInfo,
	&Antialiasing_OnRenderImage_m186_MethodInfo,
	NULL
};
extern const MethodInfo Antialiasing_CheckResources_m185_MethodInfo;
static const Il2CppMethodReference Antialiasing_t56_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	&Antialiasing_CheckResources_m185_MethodInfo,
};
static bool Antialiasing_t56_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType Antialiasing_t56_0_0_0;
extern const Il2CppType Antialiasing_t56_1_0_0;
extern const Il2CppType PostEffectsBase_t57_0_0_0;
struct Antialiasing_t56;
const Il2CppTypeDefinitionMetadata Antialiasing_t56_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &PostEffectsBase_t57_0_0_0/* parent */
	, Antialiasing_t56_VTable/* vtableMethods */
	, Antialiasing_t56_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 186/* fieldStart */

};
TypeInfo Antialiasing_t56_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "Antialiasing"/* name */
	, "UnityStandardAssets.ImageEffects"/* namespaze */
	, Antialiasing_t56_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Antialiasing_t56_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 56/* custom_attributes_cache */
	, &Antialiasing_t56_0_0_0/* byval_arg */
	, &Antialiasing_t56_1_0_0/* this_arg */
	, &Antialiasing_t56_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Antialiasing_t56)/* instance_size */
	, sizeof (Antialiasing_t56)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 22/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.ImageEffects.Bloom/LensFlareStyle
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_2.h"
// Metadata Definition UnityStandardAssets.ImageEffects.Bloom/LensFlareStyle
extern TypeInfo LensFlareStyle_t58_il2cpp_TypeInfo;
// UnityStandardAssets.ImageEffects.Bloom/LensFlareStyle
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_2MethodDeclarations.h"
static const MethodInfo* LensFlareStyle_t58_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference LensFlareStyle_t58_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool LensFlareStyle_t58_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair LensFlareStyle_t58_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType LensFlareStyle_t58_0_0_0;
extern const Il2CppType LensFlareStyle_t58_1_0_0;
extern TypeInfo Bloom_t64_il2cpp_TypeInfo;
extern const Il2CppType Bloom_t64_0_0_0;
const Il2CppTypeDefinitionMetadata LensFlareStyle_t58_DefinitionMetadata = 
{
	&Bloom_t64_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, LensFlareStyle_t58_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, LensFlareStyle_t58_VTable/* vtableMethods */
	, LensFlareStyle_t58_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 208/* fieldStart */

};
TypeInfo LensFlareStyle_t58_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "LensFlareStyle"/* name */
	, ""/* namespaze */
	, LensFlareStyle_t58_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &LensFlareStyle_t58_0_0_0/* byval_arg */
	, &LensFlareStyle_t58_1_0_0/* this_arg */
	, &LensFlareStyle_t58_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LensFlareStyle_t58)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (LensFlareStyle_t58)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityStandardAssets.ImageEffects.Bloom/TweakMode
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_3.h"
// Metadata Definition UnityStandardAssets.ImageEffects.Bloom/TweakMode
extern TypeInfo TweakMode_t59_il2cpp_TypeInfo;
// UnityStandardAssets.ImageEffects.Bloom/TweakMode
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_3MethodDeclarations.h"
static const MethodInfo* TweakMode_t59_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference TweakMode_t59_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool TweakMode_t59_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TweakMode_t59_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType TweakMode_t59_0_0_0;
extern const Il2CppType TweakMode_t59_1_0_0;
const Il2CppTypeDefinitionMetadata TweakMode_t59_DefinitionMetadata = 
{
	&Bloom_t64_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TweakMode_t59_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, TweakMode_t59_VTable/* vtableMethods */
	, TweakMode_t59_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 212/* fieldStart */

};
TypeInfo TweakMode_t59_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "TweakMode"/* name */
	, ""/* namespaze */
	, TweakMode_t59_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TweakMode_t59_0_0_0/* byval_arg */
	, &TweakMode_t59_1_0_0/* this_arg */
	, &TweakMode_t59_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TweakMode_t59)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TweakMode_t59)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityStandardAssets.ImageEffects.Bloom/HDRBloomMode
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_4.h"
// Metadata Definition UnityStandardAssets.ImageEffects.Bloom/HDRBloomMode
extern TypeInfo HDRBloomMode_t60_il2cpp_TypeInfo;
// UnityStandardAssets.ImageEffects.Bloom/HDRBloomMode
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_4MethodDeclarations.h"
static const MethodInfo* HDRBloomMode_t60_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference HDRBloomMode_t60_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool HDRBloomMode_t60_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair HDRBloomMode_t60_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType HDRBloomMode_t60_0_0_0;
extern const Il2CppType HDRBloomMode_t60_1_0_0;
const Il2CppTypeDefinitionMetadata HDRBloomMode_t60_DefinitionMetadata = 
{
	&Bloom_t64_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, HDRBloomMode_t60_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, HDRBloomMode_t60_VTable/* vtableMethods */
	, HDRBloomMode_t60_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 215/* fieldStart */

};
TypeInfo HDRBloomMode_t60_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "HDRBloomMode"/* name */
	, ""/* namespaze */
	, HDRBloomMode_t60_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &HDRBloomMode_t60_0_0_0/* byval_arg */
	, &HDRBloomMode_t60_1_0_0/* this_arg */
	, &HDRBloomMode_t60_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HDRBloomMode_t60)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (HDRBloomMode_t60)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityStandardAssets.ImageEffects.Bloom/BloomScreenBlendMode
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_5.h"
// Metadata Definition UnityStandardAssets.ImageEffects.Bloom/BloomScreenBlendMode
extern TypeInfo BloomScreenBlendMode_t61_il2cpp_TypeInfo;
// UnityStandardAssets.ImageEffects.Bloom/BloomScreenBlendMode
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_5MethodDeclarations.h"
static const MethodInfo* BloomScreenBlendMode_t61_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference BloomScreenBlendMode_t61_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool BloomScreenBlendMode_t61_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair BloomScreenBlendMode_t61_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType BloomScreenBlendMode_t61_0_0_0;
extern const Il2CppType BloomScreenBlendMode_t61_1_0_0;
const Il2CppTypeDefinitionMetadata BloomScreenBlendMode_t61_DefinitionMetadata = 
{
	&Bloom_t64_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, BloomScreenBlendMode_t61_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, BloomScreenBlendMode_t61_VTable/* vtableMethods */
	, BloomScreenBlendMode_t61_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 219/* fieldStart */

};
TypeInfo BloomScreenBlendMode_t61_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "BloomScreenBlendMode"/* name */
	, ""/* namespaze */
	, BloomScreenBlendMode_t61_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BloomScreenBlendMode_t61_0_0_0/* byval_arg */
	, &BloomScreenBlendMode_t61_1_0_0/* this_arg */
	, &BloomScreenBlendMode_t61_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BloomScreenBlendMode_t61)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (BloomScreenBlendMode_t61)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityStandardAssets.ImageEffects.Bloom/BloomQuality
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_6.h"
// Metadata Definition UnityStandardAssets.ImageEffects.Bloom/BloomQuality
extern TypeInfo BloomQuality_t62_il2cpp_TypeInfo;
// UnityStandardAssets.ImageEffects.Bloom/BloomQuality
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_6MethodDeclarations.h"
static const MethodInfo* BloomQuality_t62_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference BloomQuality_t62_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool BloomQuality_t62_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair BloomQuality_t62_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType BloomQuality_t62_0_0_0;
extern const Il2CppType BloomQuality_t62_1_0_0;
const Il2CppTypeDefinitionMetadata BloomQuality_t62_DefinitionMetadata = 
{
	&Bloom_t64_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, BloomQuality_t62_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, BloomQuality_t62_VTable/* vtableMethods */
	, BloomQuality_t62_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 222/* fieldStart */

};
TypeInfo BloomQuality_t62_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "BloomQuality"/* name */
	, ""/* namespaze */
	, BloomQuality_t62_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BloomQuality_t62_0_0_0/* byval_arg */
	, &BloomQuality_t62_1_0_0/* this_arg */
	, &BloomQuality_t62_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BloomQuality_t62)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (BloomQuality_t62)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityStandardAssets.ImageEffects.Bloom
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_7.h"
// Metadata Definition UnityStandardAssets.ImageEffects.Bloom
// UnityStandardAssets.ImageEffects.Bloom
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_7MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.Bloom::.ctor()
extern const MethodInfo Bloom__ctor_m187_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Bloom__ctor_m187/* method */
	, &Bloom_t64_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 200/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityStandardAssets.ImageEffects.Bloom::CheckResources()
extern const MethodInfo Bloom_CheckResources_m188_MethodInfo = 
{
	"CheckResources"/* name */
	, (methodPointerType)&Bloom_CheckResources_m188/* method */
	, &Bloom_t64_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 201/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
static const ParameterInfo Bloom_t64_Bloom_OnRenderImage_m189_ParameterInfos[] = 
{
	{"source", 0, 134217844, 0, &RenderTexture_t101_0_0_0},
	{"destination", 1, 134217845, 0, &RenderTexture_t101_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.Bloom::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern const MethodInfo Bloom_OnRenderImage_m189_MethodInfo = 
{
	"OnRenderImage"/* name */
	, (methodPointerType)&Bloom_OnRenderImage_m189/* method */
	, &Bloom_t64_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, Bloom_t64_Bloom_OnRenderImage_m189_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 202/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
static const ParameterInfo Bloom_t64_Bloom_AddTo_m190_ParameterInfos[] = 
{
	{"intensity_", 0, 134217846, 0, &Single_t254_0_0_0},
	{"from", 1, 134217847, 0, &RenderTexture_t101_0_0_0},
	{"to", 2, 134217848, 0, &RenderTexture_t101_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Single_t254_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.Bloom::AddTo(System.Single,UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern const MethodInfo Bloom_AddTo_m190_MethodInfo = 
{
	"AddTo"/* name */
	, (methodPointerType)&Bloom_AddTo_m190/* method */
	, &Bloom_t64_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Single_t254_Object_t_Object_t/* invoker_method */
	, Bloom_t64_Bloom_AddTo_m190_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 203/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
static const ParameterInfo Bloom_t64_Bloom_BlendFlares_m191_ParameterInfos[] = 
{
	{"from", 0, 134217849, 0, &RenderTexture_t101_0_0_0},
	{"to", 1, 134217850, 0, &RenderTexture_t101_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.Bloom::BlendFlares(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern const MethodInfo Bloom_BlendFlares_m191_MethodInfo = 
{
	"BlendFlares"/* name */
	, (methodPointerType)&Bloom_BlendFlares_m191/* method */
	, &Bloom_t64_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, Bloom_t64_Bloom_BlendFlares_m191_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 204/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
static const ParameterInfo Bloom_t64_Bloom_BrightFilter_m192_ParameterInfos[] = 
{
	{"thresh", 0, 134217851, 0, &Single_t254_0_0_0},
	{"from", 1, 134217852, 0, &RenderTexture_t101_0_0_0},
	{"to", 2, 134217853, 0, &RenderTexture_t101_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Single_t254_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.Bloom::BrightFilter(System.Single,UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern const MethodInfo Bloom_BrightFilter_m192_MethodInfo = 
{
	"BrightFilter"/* name */
	, (methodPointerType)&Bloom_BrightFilter_m192/* method */
	, &Bloom_t64_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Single_t254_Object_t_Object_t/* invoker_method */
	, Bloom_t64_Bloom_BrightFilter_m192_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 205/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Color_t65_0_0_0;
extern const Il2CppType Color_t65_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
static const ParameterInfo Bloom_t64_Bloom_BrightFilter_m193_ParameterInfos[] = 
{
	{"threshColor", 0, 134217854, 0, &Color_t65_0_0_0},
	{"from", 1, 134217855, 0, &RenderTexture_t101_0_0_0},
	{"to", 2, 134217856, 0, &RenderTexture_t101_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Color_t65_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.Bloom::BrightFilter(UnityEngine.Color,UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern const MethodInfo Bloom_BrightFilter_m193_MethodInfo = 
{
	"BrightFilter"/* name */
	, (methodPointerType)&Bloom_BrightFilter_m193/* method */
	, &Bloom_t64_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Color_t65_Object_t_Object_t/* invoker_method */
	, Bloom_t64_Bloom_BrightFilter_m193_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 206/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
static const ParameterInfo Bloom_t64_Bloom_Vignette_m194_ParameterInfos[] = 
{
	{"amount", 0, 134217857, 0, &Single_t254_0_0_0},
	{"from", 1, 134217858, 0, &RenderTexture_t101_0_0_0},
	{"to", 2, 134217859, 0, &RenderTexture_t101_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Single_t254_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.Bloom::Vignette(System.Single,UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern const MethodInfo Bloom_Vignette_m194_MethodInfo = 
{
	"Vignette"/* name */
	, (methodPointerType)&Bloom_Vignette_m194/* method */
	, &Bloom_t64_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Single_t254_Object_t_Object_t/* invoker_method */
	, Bloom_t64_Bloom_Vignette_m194_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 207/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Bloom_t64_MethodInfos[] =
{
	&Bloom__ctor_m187_MethodInfo,
	&Bloom_CheckResources_m188_MethodInfo,
	&Bloom_OnRenderImage_m189_MethodInfo,
	&Bloom_AddTo_m190_MethodInfo,
	&Bloom_BlendFlares_m191_MethodInfo,
	&Bloom_BrightFilter_m192_MethodInfo,
	&Bloom_BrightFilter_m193_MethodInfo,
	&Bloom_Vignette_m194_MethodInfo,
	NULL
};
static const Il2CppType* Bloom_t64_il2cpp_TypeInfo__nestedTypes[5] =
{
	&LensFlareStyle_t58_0_0_0,
	&TweakMode_t59_0_0_0,
	&HDRBloomMode_t60_0_0_0,
	&BloomScreenBlendMode_t61_0_0_0,
	&BloomQuality_t62_0_0_0,
};
extern const MethodInfo Bloom_CheckResources_m188_MethodInfo;
static const Il2CppMethodReference Bloom_t64_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	&Bloom_CheckResources_m188_MethodInfo,
};
static bool Bloom_t64_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType Bloom_t64_1_0_0;
struct Bloom_t64;
const Il2CppTypeDefinitionMetadata Bloom_t64_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Bloom_t64_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &PostEffectsBase_t57_0_0_0/* parent */
	, Bloom_t64_VTable/* vtableMethods */
	, Bloom_t64_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 225/* fieldStart */

};
TypeInfo Bloom_t64_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "Bloom"/* name */
	, "UnityStandardAssets.ImageEffects"/* namespaze */
	, Bloom_t64_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Bloom_t64_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 57/* custom_attributes_cache */
	, &Bloom_t64_0_0_0/* byval_arg */
	, &Bloom_t64_1_0_0/* this_arg */
	, &Bloom_t64_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Bloom_t64)/* instance_size */
	, sizeof (Bloom_t64)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 0/* property_count */
	, 30/* field_count */
	, 0/* event_count */
	, 5/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.ImageEffects.LensflareStyle34
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_8.h"
// Metadata Definition UnityStandardAssets.ImageEffects.LensflareStyle34
extern TypeInfo LensflareStyle34_t66_il2cpp_TypeInfo;
// UnityStandardAssets.ImageEffects.LensflareStyle34
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_8MethodDeclarations.h"
static const MethodInfo* LensflareStyle34_t66_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference LensflareStyle34_t66_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool LensflareStyle34_t66_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair LensflareStyle34_t66_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType LensflareStyle34_t66_0_0_0;
extern const Il2CppType LensflareStyle34_t66_1_0_0;
const Il2CppTypeDefinitionMetadata LensflareStyle34_t66_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, LensflareStyle34_t66_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, LensflareStyle34_t66_VTable/* vtableMethods */
	, LensflareStyle34_t66_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 255/* fieldStart */

};
TypeInfo LensflareStyle34_t66_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "LensflareStyle34"/* name */
	, "UnityStandardAssets.ImageEffects"/* namespaze */
	, LensflareStyle34_t66_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &LensflareStyle34_t66_0_0_0/* byval_arg */
	, &LensflareStyle34_t66_1_0_0/* this_arg */
	, &LensflareStyle34_t66_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LensflareStyle34_t66)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (LensflareStyle34_t66)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityStandardAssets.ImageEffects.TweakMode34
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_9.h"
// Metadata Definition UnityStandardAssets.ImageEffects.TweakMode34
extern TypeInfo TweakMode34_t67_il2cpp_TypeInfo;
// UnityStandardAssets.ImageEffects.TweakMode34
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_9MethodDeclarations.h"
static const MethodInfo* TweakMode34_t67_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference TweakMode34_t67_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool TweakMode34_t67_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TweakMode34_t67_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType TweakMode34_t67_0_0_0;
extern const Il2CppType TweakMode34_t67_1_0_0;
const Il2CppTypeDefinitionMetadata TweakMode34_t67_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TweakMode34_t67_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, TweakMode34_t67_VTable/* vtableMethods */
	, TweakMode34_t67_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 259/* fieldStart */

};
TypeInfo TweakMode34_t67_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "TweakMode34"/* name */
	, "UnityStandardAssets.ImageEffects"/* namespaze */
	, TweakMode34_t67_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TweakMode34_t67_0_0_0/* byval_arg */
	, &TweakMode34_t67_1_0_0/* this_arg */
	, &TweakMode34_t67_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TweakMode34_t67)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TweakMode34_t67)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityStandardAssets.ImageEffects.HDRBloomMode
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_10.h"
// Metadata Definition UnityStandardAssets.ImageEffects.HDRBloomMode
extern TypeInfo HDRBloomMode_t68_il2cpp_TypeInfo;
// UnityStandardAssets.ImageEffects.HDRBloomMode
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_10MethodDeclarations.h"
static const MethodInfo* HDRBloomMode_t68_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference HDRBloomMode_t68_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool HDRBloomMode_t68_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair HDRBloomMode_t68_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType HDRBloomMode_t68_0_0_0;
extern const Il2CppType HDRBloomMode_t68_1_0_0;
const Il2CppTypeDefinitionMetadata HDRBloomMode_t68_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, HDRBloomMode_t68_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, HDRBloomMode_t68_VTable/* vtableMethods */
	, HDRBloomMode_t68_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 262/* fieldStart */

};
TypeInfo HDRBloomMode_t68_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "HDRBloomMode"/* name */
	, "UnityStandardAssets.ImageEffects"/* namespaze */
	, HDRBloomMode_t68_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &HDRBloomMode_t68_0_0_0/* byval_arg */
	, &HDRBloomMode_t68_1_0_0/* this_arg */
	, &HDRBloomMode_t68_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HDRBloomMode_t68)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (HDRBloomMode_t68)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityStandardAssets.ImageEffects.BloomScreenBlendMode
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_11.h"
// Metadata Definition UnityStandardAssets.ImageEffects.BloomScreenBlendMode
extern TypeInfo BloomScreenBlendMode_t69_il2cpp_TypeInfo;
// UnityStandardAssets.ImageEffects.BloomScreenBlendMode
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_11MethodDeclarations.h"
static const MethodInfo* BloomScreenBlendMode_t69_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference BloomScreenBlendMode_t69_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool BloomScreenBlendMode_t69_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair BloomScreenBlendMode_t69_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType BloomScreenBlendMode_t69_0_0_0;
extern const Il2CppType BloomScreenBlendMode_t69_1_0_0;
const Il2CppTypeDefinitionMetadata BloomScreenBlendMode_t69_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, BloomScreenBlendMode_t69_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, BloomScreenBlendMode_t69_VTable/* vtableMethods */
	, BloomScreenBlendMode_t69_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 266/* fieldStart */

};
TypeInfo BloomScreenBlendMode_t69_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "BloomScreenBlendMode"/* name */
	, "UnityStandardAssets.ImageEffects"/* namespaze */
	, BloomScreenBlendMode_t69_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BloomScreenBlendMode_t69_0_0_0/* byval_arg */
	, &BloomScreenBlendMode_t69_1_0_0/* this_arg */
	, &BloomScreenBlendMode_t69_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BloomScreenBlendMode_t69)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (BloomScreenBlendMode_t69)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityStandardAssets.ImageEffects.BloomAndFlares
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_12.h"
// Metadata Definition UnityStandardAssets.ImageEffects.BloomAndFlares
extern TypeInfo BloomAndFlares_t70_il2cpp_TypeInfo;
// UnityStandardAssets.ImageEffects.BloomAndFlares
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_12MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.BloomAndFlares::.ctor()
extern const MethodInfo BloomAndFlares__ctor_m195_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&BloomAndFlares__ctor_m195/* method */
	, &BloomAndFlares_t70_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 208/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityStandardAssets.ImageEffects.BloomAndFlares::CheckResources()
extern const MethodInfo BloomAndFlares_CheckResources_m196_MethodInfo = 
{
	"CheckResources"/* name */
	, (methodPointerType)&BloomAndFlares_CheckResources_m196/* method */
	, &BloomAndFlares_t70_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 209/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
static const ParameterInfo BloomAndFlares_t70_BloomAndFlares_OnRenderImage_m197_ParameterInfos[] = 
{
	{"source", 0, 134217860, 0, &RenderTexture_t101_0_0_0},
	{"destination", 1, 134217861, 0, &RenderTexture_t101_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.BloomAndFlares::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern const MethodInfo BloomAndFlares_OnRenderImage_m197_MethodInfo = 
{
	"OnRenderImage"/* name */
	, (methodPointerType)&BloomAndFlares_OnRenderImage_m197/* method */
	, &BloomAndFlares_t70_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, BloomAndFlares_t70_BloomAndFlares_OnRenderImage_m197_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 210/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
static const ParameterInfo BloomAndFlares_t70_BloomAndFlares_AddTo_m198_ParameterInfos[] = 
{
	{"intensity_", 0, 134217862, 0, &Single_t254_0_0_0},
	{"from", 1, 134217863, 0, &RenderTexture_t101_0_0_0},
	{"to", 2, 134217864, 0, &RenderTexture_t101_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Single_t254_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.BloomAndFlares::AddTo(System.Single,UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern const MethodInfo BloomAndFlares_AddTo_m198_MethodInfo = 
{
	"AddTo"/* name */
	, (methodPointerType)&BloomAndFlares_AddTo_m198/* method */
	, &BloomAndFlares_t70_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Single_t254_Object_t_Object_t/* invoker_method */
	, BloomAndFlares_t70_BloomAndFlares_AddTo_m198_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 211/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
static const ParameterInfo BloomAndFlares_t70_BloomAndFlares_BlendFlares_m199_ParameterInfos[] = 
{
	{"from", 0, 134217865, 0, &RenderTexture_t101_0_0_0},
	{"to", 1, 134217866, 0, &RenderTexture_t101_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.BloomAndFlares::BlendFlares(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern const MethodInfo BloomAndFlares_BlendFlares_m199_MethodInfo = 
{
	"BlendFlares"/* name */
	, (methodPointerType)&BloomAndFlares_BlendFlares_m199/* method */
	, &BloomAndFlares_t70_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, BloomAndFlares_t70_BloomAndFlares_BlendFlares_m199_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 212/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
extern const Il2CppType Single_t254_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
static const ParameterInfo BloomAndFlares_t70_BloomAndFlares_BrightFilter_m200_ParameterInfos[] = 
{
	{"thresh", 0, 134217867, 0, &Single_t254_0_0_0},
	{"useAlphaAsMask", 1, 134217868, 0, &Single_t254_0_0_0},
	{"from", 2, 134217869, 0, &RenderTexture_t101_0_0_0},
	{"to", 3, 134217870, 0, &RenderTexture_t101_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Single_t254_Single_t254_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.BloomAndFlares::BrightFilter(System.Single,System.Single,UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern const MethodInfo BloomAndFlares_BrightFilter_m200_MethodInfo = 
{
	"BrightFilter"/* name */
	, (methodPointerType)&BloomAndFlares_BrightFilter_m200/* method */
	, &BloomAndFlares_t70_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Single_t254_Single_t254_Object_t_Object_t/* invoker_method */
	, BloomAndFlares_t70_BloomAndFlares_BrightFilter_m200_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 213/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
static const ParameterInfo BloomAndFlares_t70_BloomAndFlares_Vignette_m201_ParameterInfos[] = 
{
	{"amount", 0, 134217871, 0, &Single_t254_0_0_0},
	{"from", 1, 134217872, 0, &RenderTexture_t101_0_0_0},
	{"to", 2, 134217873, 0, &RenderTexture_t101_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Single_t254_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.BloomAndFlares::Vignette(System.Single,UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern const MethodInfo BloomAndFlares_Vignette_m201_MethodInfo = 
{
	"Vignette"/* name */
	, (methodPointerType)&BloomAndFlares_Vignette_m201/* method */
	, &BloomAndFlares_t70_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Single_t254_Object_t_Object_t/* invoker_method */
	, BloomAndFlares_t70_BloomAndFlares_Vignette_m201_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 214/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* BloomAndFlares_t70_MethodInfos[] =
{
	&BloomAndFlares__ctor_m195_MethodInfo,
	&BloomAndFlares_CheckResources_m196_MethodInfo,
	&BloomAndFlares_OnRenderImage_m197_MethodInfo,
	&BloomAndFlares_AddTo_m198_MethodInfo,
	&BloomAndFlares_BlendFlares_m199_MethodInfo,
	&BloomAndFlares_BrightFilter_m200_MethodInfo,
	&BloomAndFlares_Vignette_m201_MethodInfo,
	NULL
};
extern const MethodInfo BloomAndFlares_CheckResources_m196_MethodInfo;
static const Il2CppMethodReference BloomAndFlares_t70_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	&BloomAndFlares_CheckResources_m196_MethodInfo,
};
static bool BloomAndFlares_t70_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType BloomAndFlares_t70_0_0_0;
extern const Il2CppType BloomAndFlares_t70_1_0_0;
struct BloomAndFlares_t70;
const Il2CppTypeDefinitionMetadata BloomAndFlares_t70_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &PostEffectsBase_t57_0_0_0/* parent */
	, BloomAndFlares_t70_VTable/* vtableMethods */
	, BloomAndFlares_t70_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 269/* fieldStart */

};
TypeInfo BloomAndFlares_t70_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "BloomAndFlares"/* name */
	, "UnityStandardAssets.ImageEffects"/* namespaze */
	, BloomAndFlares_t70_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &BloomAndFlares_t70_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 58/* custom_attributes_cache */
	, &BloomAndFlares_t70_0_0_0/* byval_arg */
	, &BloomAndFlares_t70_1_0_0/* this_arg */
	, &BloomAndFlares_t70_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BloomAndFlares_t70)/* instance_size */
	, sizeof (BloomAndFlares_t70)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 34/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.ImageEffects.BloomOptimized/Resolution
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_13.h"
// Metadata Definition UnityStandardAssets.ImageEffects.BloomOptimized/Resolution
extern TypeInfo Resolution_t71_il2cpp_TypeInfo;
// UnityStandardAssets.ImageEffects.BloomOptimized/Resolution
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_13MethodDeclarations.h"
static const MethodInfo* Resolution_t71_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference Resolution_t71_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool Resolution_t71_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Resolution_t71_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType Resolution_t71_0_0_0;
extern const Il2CppType Resolution_t71_1_0_0;
extern TypeInfo BloomOptimized_t73_il2cpp_TypeInfo;
extern const Il2CppType BloomOptimized_t73_0_0_0;
const Il2CppTypeDefinitionMetadata Resolution_t71_DefinitionMetadata = 
{
	&BloomOptimized_t73_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Resolution_t71_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, Resolution_t71_VTable/* vtableMethods */
	, Resolution_t71_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 303/* fieldStart */

};
TypeInfo Resolution_t71_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "Resolution"/* name */
	, ""/* namespaze */
	, Resolution_t71_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Resolution_t71_0_0_0/* byval_arg */
	, &Resolution_t71_1_0_0/* this_arg */
	, &Resolution_t71_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Resolution_t71)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Resolution_t71)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityStandardAssets.ImageEffects.BloomOptimized/BlurType
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_14.h"
// Metadata Definition UnityStandardAssets.ImageEffects.BloomOptimized/BlurType
extern TypeInfo BlurType_t72_il2cpp_TypeInfo;
// UnityStandardAssets.ImageEffects.BloomOptimized/BlurType
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_14MethodDeclarations.h"
static const MethodInfo* BlurType_t72_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference BlurType_t72_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool BlurType_t72_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair BlurType_t72_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType BlurType_t72_0_0_0;
extern const Il2CppType BlurType_t72_1_0_0;
const Il2CppTypeDefinitionMetadata BlurType_t72_DefinitionMetadata = 
{
	&BloomOptimized_t73_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, BlurType_t72_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, BlurType_t72_VTable/* vtableMethods */
	, BlurType_t72_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 306/* fieldStart */

};
TypeInfo BlurType_t72_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "BlurType"/* name */
	, ""/* namespaze */
	, BlurType_t72_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BlurType_t72_0_0_0/* byval_arg */
	, &BlurType_t72_1_0_0/* this_arg */
	, &BlurType_t72_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BlurType_t72)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (BlurType_t72)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityStandardAssets.ImageEffects.BloomOptimized
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_15.h"
// Metadata Definition UnityStandardAssets.ImageEffects.BloomOptimized
// UnityStandardAssets.ImageEffects.BloomOptimized
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_15MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.BloomOptimized::.ctor()
extern const MethodInfo BloomOptimized__ctor_m202_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&BloomOptimized__ctor_m202/* method */
	, &BloomOptimized_t73_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 215/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityStandardAssets.ImageEffects.BloomOptimized::CheckResources()
extern const MethodInfo BloomOptimized_CheckResources_m203_MethodInfo = 
{
	"CheckResources"/* name */
	, (methodPointerType)&BloomOptimized_CheckResources_m203/* method */
	, &BloomOptimized_t73_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 216/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.BloomOptimized::OnDisable()
extern const MethodInfo BloomOptimized_OnDisable_m204_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&BloomOptimized_OnDisable_m204/* method */
	, &BloomOptimized_t73_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 217/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
static const ParameterInfo BloomOptimized_t73_BloomOptimized_OnRenderImage_m205_ParameterInfos[] = 
{
	{"source", 0, 134217874, 0, &RenderTexture_t101_0_0_0},
	{"destination", 1, 134217875, 0, &RenderTexture_t101_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.BloomOptimized::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern const MethodInfo BloomOptimized_OnRenderImage_m205_MethodInfo = 
{
	"OnRenderImage"/* name */
	, (methodPointerType)&BloomOptimized_OnRenderImage_m205/* method */
	, &BloomOptimized_t73_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, BloomOptimized_t73_BloomOptimized_OnRenderImage_m205_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 218/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* BloomOptimized_t73_MethodInfos[] =
{
	&BloomOptimized__ctor_m202_MethodInfo,
	&BloomOptimized_CheckResources_m203_MethodInfo,
	&BloomOptimized_OnDisable_m204_MethodInfo,
	&BloomOptimized_OnRenderImage_m205_MethodInfo,
	NULL
};
static const Il2CppType* BloomOptimized_t73_il2cpp_TypeInfo__nestedTypes[2] =
{
	&Resolution_t71_0_0_0,
	&BlurType_t72_0_0_0,
};
extern const MethodInfo BloomOptimized_CheckResources_m203_MethodInfo;
static const Il2CppMethodReference BloomOptimized_t73_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	&BloomOptimized_CheckResources_m203_MethodInfo,
};
static bool BloomOptimized_t73_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType BloomOptimized_t73_1_0_0;
struct BloomOptimized_t73;
const Il2CppTypeDefinitionMetadata BloomOptimized_t73_DefinitionMetadata = 
{
	NULL/* declaringType */
	, BloomOptimized_t73_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &PostEffectsBase_t57_0_0_0/* parent */
	, BloomOptimized_t73_VTable/* vtableMethods */
	, BloomOptimized_t73_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 309/* fieldStart */

};
TypeInfo BloomOptimized_t73_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "BloomOptimized"/* name */
	, "UnityStandardAssets.ImageEffects"/* namespaze */
	, BloomOptimized_t73_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &BloomOptimized_t73_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 59/* custom_attributes_cache */
	, &BloomOptimized_t73_0_0_0/* byval_arg */
	, &BloomOptimized_t73_1_0_0/* this_arg */
	, &BloomOptimized_t73_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BloomOptimized_t73)/* instance_size */
	, sizeof (BloomOptimized_t73)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.ImageEffects.Blur
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_16.h"
// Metadata Definition UnityStandardAssets.ImageEffects.Blur
extern TypeInfo Blur_t74_il2cpp_TypeInfo;
// UnityStandardAssets.ImageEffects.Blur
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_16MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.Blur::.ctor()
extern const MethodInfo Blur__ctor_m206_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Blur__ctor_m206/* method */
	, &Blur_t74_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 219/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.Blur::.cctor()
extern const MethodInfo Blur__cctor_m207_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&Blur__cctor_m207/* method */
	, &Blur_t74_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 220/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Material UnityStandardAssets.ImageEffects.Blur::get_material()
extern const MethodInfo Blur_get_material_m208_MethodInfo = 
{
	"get_material"/* name */
	, (methodPointerType)&Blur_get_material_m208/* method */
	, &Blur_t74_il2cpp_TypeInfo/* declaring_type */
	, &Material_t55_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2180/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 221/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.Blur::OnDisable()
extern const MethodInfo Blur_OnDisable_m209_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&Blur_OnDisable_m209/* method */
	, &Blur_t74_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 222/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.Blur::Start()
extern const MethodInfo Blur_Start_m210_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&Blur_Start_m210/* method */
	, &Blur_t74_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 223/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo Blur_t74_Blur_FourTapCone_m211_ParameterInfos[] = 
{
	{"source", 0, 134217876, 0, &RenderTexture_t101_0_0_0},
	{"dest", 1, 134217877, 0, &RenderTexture_t101_0_0_0},
	{"iteration", 2, 134217878, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.Blur::FourTapCone(UnityEngine.RenderTexture,UnityEngine.RenderTexture,System.Int32)
extern const MethodInfo Blur_FourTapCone_m211_MethodInfo = 
{
	"FourTapCone"/* name */
	, (methodPointerType)&Blur_FourTapCone_m211/* method */
	, &Blur_t74_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t_Int32_t253/* invoker_method */
	, Blur_t74_Blur_FourTapCone_m211_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 224/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
static const ParameterInfo Blur_t74_Blur_DownSample4x_m212_ParameterInfos[] = 
{
	{"source", 0, 134217879, 0, &RenderTexture_t101_0_0_0},
	{"dest", 1, 134217880, 0, &RenderTexture_t101_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.Blur::DownSample4x(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern const MethodInfo Blur_DownSample4x_m212_MethodInfo = 
{
	"DownSample4x"/* name */
	, (methodPointerType)&Blur_DownSample4x_m212/* method */
	, &Blur_t74_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, Blur_t74_Blur_DownSample4x_m212_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 225/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
static const ParameterInfo Blur_t74_Blur_OnRenderImage_m213_ParameterInfos[] = 
{
	{"source", 0, 134217881, 0, &RenderTexture_t101_0_0_0},
	{"destination", 1, 134217882, 0, &RenderTexture_t101_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.Blur::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern const MethodInfo Blur_OnRenderImage_m213_MethodInfo = 
{
	"OnRenderImage"/* name */
	, (methodPointerType)&Blur_OnRenderImage_m213/* method */
	, &Blur_t74_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, Blur_t74_Blur_OnRenderImage_m213_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 226/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Blur_t74_MethodInfos[] =
{
	&Blur__ctor_m206_MethodInfo,
	&Blur__cctor_m207_MethodInfo,
	&Blur_get_material_m208_MethodInfo,
	&Blur_OnDisable_m209_MethodInfo,
	&Blur_Start_m210_MethodInfo,
	&Blur_FourTapCone_m211_MethodInfo,
	&Blur_DownSample4x_m212_MethodInfo,
	&Blur_OnRenderImage_m213_MethodInfo,
	NULL
};
extern const MethodInfo Blur_get_material_m208_MethodInfo;
static const PropertyInfo Blur_t74____material_PropertyInfo = 
{
	&Blur_t74_il2cpp_TypeInfo/* parent */
	, "material"/* name */
	, &Blur_get_material_m208_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Blur_t74_PropertyInfos[] =
{
	&Blur_t74____material_PropertyInfo,
	NULL
};
static const Il2CppMethodReference Blur_t74_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool Blur_t74_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType Blur_t74_0_0_0;
extern const Il2CppType Blur_t74_1_0_0;
struct Blur_t74;
const Il2CppTypeDefinitionMetadata Blur_t74_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, Blur_t74_VTable/* vtableMethods */
	, Blur_t74_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 317/* fieldStart */

};
TypeInfo Blur_t74_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "Blur"/* name */
	, "UnityStandardAssets.ImageEffects"/* namespaze */
	, Blur_t74_MethodInfos/* methods */
	, Blur_t74_PropertyInfos/* properties */
	, NULL/* events */
	, &Blur_t74_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 64/* custom_attributes_cache */
	, &Blur_t74_0_0_0/* byval_arg */
	, &Blur_t74_1_0_0/* this_arg */
	, &Blur_t74_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Blur_t74)/* instance_size */
	, sizeof (Blur_t74)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Blur_t74_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 1/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.ImageEffects.BlurOptimized/BlurType
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_17.h"
// Metadata Definition UnityStandardAssets.ImageEffects.BlurOptimized/BlurType
extern TypeInfo BlurType_t75_il2cpp_TypeInfo;
// UnityStandardAssets.ImageEffects.BlurOptimized/BlurType
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_17MethodDeclarations.h"
static const MethodInfo* BlurType_t75_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference BlurType_t75_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool BlurType_t75_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair BlurType_t75_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType BlurType_t75_0_0_0;
extern const Il2CppType BlurType_t75_1_0_0;
extern TypeInfo BlurOptimized_t76_il2cpp_TypeInfo;
extern const Il2CppType BlurOptimized_t76_0_0_0;
const Il2CppTypeDefinitionMetadata BlurType_t75_DefinitionMetadata = 
{
	&BlurOptimized_t76_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, BlurType_t75_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, BlurType_t75_VTable/* vtableMethods */
	, BlurType_t75_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 321/* fieldStart */

};
TypeInfo BlurType_t75_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "BlurType"/* name */
	, ""/* namespaze */
	, BlurType_t75_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BlurType_t75_0_0_0/* byval_arg */
	, &BlurType_t75_1_0_0/* this_arg */
	, &BlurType_t75_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BlurType_t75)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (BlurType_t75)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityStandardAssets.ImageEffects.BlurOptimized
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_18.h"
// Metadata Definition UnityStandardAssets.ImageEffects.BlurOptimized
// UnityStandardAssets.ImageEffects.BlurOptimized
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_18MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.BlurOptimized::.ctor()
extern const MethodInfo BlurOptimized__ctor_m214_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&BlurOptimized__ctor_m214/* method */
	, &BlurOptimized_t76_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 227/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityStandardAssets.ImageEffects.BlurOptimized::CheckResources()
extern const MethodInfo BlurOptimized_CheckResources_m215_MethodInfo = 
{
	"CheckResources"/* name */
	, (methodPointerType)&BlurOptimized_CheckResources_m215/* method */
	, &BlurOptimized_t76_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 228/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.BlurOptimized::OnDisable()
extern const MethodInfo BlurOptimized_OnDisable_m216_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&BlurOptimized_OnDisable_m216/* method */
	, &BlurOptimized_t76_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 229/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
static const ParameterInfo BlurOptimized_t76_BlurOptimized_OnRenderImage_m217_ParameterInfos[] = 
{
	{"source", 0, 134217883, 0, &RenderTexture_t101_0_0_0},
	{"destination", 1, 134217884, 0, &RenderTexture_t101_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.BlurOptimized::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern const MethodInfo BlurOptimized_OnRenderImage_m217_MethodInfo = 
{
	"OnRenderImage"/* name */
	, (methodPointerType)&BlurOptimized_OnRenderImage_m217/* method */
	, &BlurOptimized_t76_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, BlurOptimized_t76_BlurOptimized_OnRenderImage_m217_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 230/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* BlurOptimized_t76_MethodInfos[] =
{
	&BlurOptimized__ctor_m214_MethodInfo,
	&BlurOptimized_CheckResources_m215_MethodInfo,
	&BlurOptimized_OnDisable_m216_MethodInfo,
	&BlurOptimized_OnRenderImage_m217_MethodInfo,
	NULL
};
static const Il2CppType* BlurOptimized_t76_il2cpp_TypeInfo__nestedTypes[1] =
{
	&BlurType_t75_0_0_0,
};
extern const MethodInfo BlurOptimized_CheckResources_m215_MethodInfo;
static const Il2CppMethodReference BlurOptimized_t76_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	&BlurOptimized_CheckResources_m215_MethodInfo,
};
static bool BlurOptimized_t76_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType BlurOptimized_t76_1_0_0;
struct BlurOptimized_t76;
const Il2CppTypeDefinitionMetadata BlurOptimized_t76_DefinitionMetadata = 
{
	NULL/* declaringType */
	, BlurOptimized_t76_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &PostEffectsBase_t57_0_0_0/* parent */
	, BlurOptimized_t76_VTable/* vtableMethods */
	, BlurOptimized_t76_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 324/* fieldStart */

};
TypeInfo BlurOptimized_t76_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "BlurOptimized"/* name */
	, "UnityStandardAssets.ImageEffects"/* namespaze */
	, BlurOptimized_t76_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &BlurOptimized_t76_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 65/* custom_attributes_cache */
	, &BlurOptimized_t76_0_0_0/* byval_arg */
	, &BlurOptimized_t76_1_0_0/* this_arg */
	, &BlurOptimized_t76_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BlurOptimized_t76)/* instance_size */
	, sizeof (BlurOptimized_t76)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.ImageEffects.CameraMotionBlur/MotionBlurFilter
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_19.h"
// Metadata Definition UnityStandardAssets.ImageEffects.CameraMotionBlur/MotionBlurFilter
extern TypeInfo MotionBlurFilter_t77_il2cpp_TypeInfo;
// UnityStandardAssets.ImageEffects.CameraMotionBlur/MotionBlurFilter
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_19MethodDeclarations.h"
static const MethodInfo* MotionBlurFilter_t77_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference MotionBlurFilter_t77_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool MotionBlurFilter_t77_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair MotionBlurFilter_t77_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType MotionBlurFilter_t77_0_0_0;
extern const Il2CppType MotionBlurFilter_t77_1_0_0;
extern TypeInfo CameraMotionBlur_t79_il2cpp_TypeInfo;
extern const Il2CppType CameraMotionBlur_t79_0_0_0;
const Il2CppTypeDefinitionMetadata MotionBlurFilter_t77_DefinitionMetadata = 
{
	&CameraMotionBlur_t79_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MotionBlurFilter_t77_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, MotionBlurFilter_t77_VTable/* vtableMethods */
	, MotionBlurFilter_t77_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 330/* fieldStart */

};
TypeInfo MotionBlurFilter_t77_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "MotionBlurFilter"/* name */
	, ""/* namespaze */
	, MotionBlurFilter_t77_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MotionBlurFilter_t77_0_0_0/* byval_arg */
	, &MotionBlurFilter_t77_1_0_0/* this_arg */
	, &MotionBlurFilter_t77_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MotionBlurFilter_t77)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (MotionBlurFilter_t77)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityStandardAssets.ImageEffects.CameraMotionBlur
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_20.h"
// Metadata Definition UnityStandardAssets.ImageEffects.CameraMotionBlur
// UnityStandardAssets.ImageEffects.CameraMotionBlur
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_20MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.CameraMotionBlur::.ctor()
extern const MethodInfo CameraMotionBlur__ctor_m218_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CameraMotionBlur__ctor_m218/* method */
	, &CameraMotionBlur_t79_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 231/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.CameraMotionBlur::.cctor()
extern const MethodInfo CameraMotionBlur__cctor_m219_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&CameraMotionBlur__cctor_m219/* method */
	, &CameraMotionBlur_t79_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 232/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.CameraMotionBlur::CalculateViewProjection()
extern const MethodInfo CameraMotionBlur_CalculateViewProjection_m220_MethodInfo = 
{
	"CalculateViewProjection"/* name */
	, (methodPointerType)&CameraMotionBlur_CalculateViewProjection_m220/* method */
	, &CameraMotionBlur_t79_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 233/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.CameraMotionBlur::Start()
extern const MethodInfo CameraMotionBlur_Start_m221_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&CameraMotionBlur_Start_m221/* method */
	, &CameraMotionBlur_t79_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 234/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.CameraMotionBlur::OnEnable()
extern const MethodInfo CameraMotionBlur_OnEnable_m222_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&CameraMotionBlur_OnEnable_m222/* method */
	, &CameraMotionBlur_t79_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 235/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.CameraMotionBlur::OnDisable()
extern const MethodInfo CameraMotionBlur_OnDisable_m223_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&CameraMotionBlur_OnDisable_m223/* method */
	, &CameraMotionBlur_t79_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 236/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityStandardAssets.ImageEffects.CameraMotionBlur::CheckResources()
extern const MethodInfo CameraMotionBlur_CheckResources_m224_MethodInfo = 
{
	"CheckResources"/* name */
	, (methodPointerType)&CameraMotionBlur_CheckResources_m224/* method */
	, &CameraMotionBlur_t79_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 237/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
static const ParameterInfo CameraMotionBlur_t79_CameraMotionBlur_OnRenderImage_m225_ParameterInfos[] = 
{
	{"source", 0, 134217885, 0, &RenderTexture_t101_0_0_0},
	{"destination", 1, 134217886, 0, &RenderTexture_t101_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.CameraMotionBlur::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern const MethodInfo CameraMotionBlur_OnRenderImage_m225_MethodInfo = 
{
	"OnRenderImage"/* name */
	, (methodPointerType)&CameraMotionBlur_OnRenderImage_m225/* method */
	, &CameraMotionBlur_t79_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, CameraMotionBlur_t79_CameraMotionBlur_OnRenderImage_m225_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 238/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.CameraMotionBlur::Remember()
extern const MethodInfo CameraMotionBlur_Remember_m226_MethodInfo = 
{
	"Remember"/* name */
	, (methodPointerType)&CameraMotionBlur_Remember_m226/* method */
	, &CameraMotionBlur_t79_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 239/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Camera_t27_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Camera UnityStandardAssets.ImageEffects.CameraMotionBlur::GetTmpCam()
extern const MethodInfo CameraMotionBlur_GetTmpCam_m227_MethodInfo = 
{
	"GetTmpCam"/* name */
	, (methodPointerType)&CameraMotionBlur_GetTmpCam_m227/* method */
	, &CameraMotionBlur_t79_il2cpp_TypeInfo/* declaring_type */
	, &Camera_t27_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 240/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.CameraMotionBlur::StartFrame()
extern const MethodInfo CameraMotionBlur_StartFrame_m228_MethodInfo = 
{
	"StartFrame"/* name */
	, (methodPointerType)&CameraMotionBlur_StartFrame_m228/* method */
	, &CameraMotionBlur_t79_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 241/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo CameraMotionBlur_t79_CameraMotionBlur_divRoundUp_m229_ParameterInfos[] = 
{
	{"x", 0, 134217887, 0, &Int32_t253_0_0_0},
	{"d", 1, 134217888, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Int32_t253_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityStandardAssets.ImageEffects.CameraMotionBlur::divRoundUp(System.Int32,System.Int32)
extern const MethodInfo CameraMotionBlur_divRoundUp_m229_MethodInfo = 
{
	"divRoundUp"/* name */
	, (methodPointerType)&CameraMotionBlur_divRoundUp_m229/* method */
	, &CameraMotionBlur_t79_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253_Int32_t253_Int32_t253/* invoker_method */
	, CameraMotionBlur_t79_CameraMotionBlur_divRoundUp_m229_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 242/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CameraMotionBlur_t79_MethodInfos[] =
{
	&CameraMotionBlur__ctor_m218_MethodInfo,
	&CameraMotionBlur__cctor_m219_MethodInfo,
	&CameraMotionBlur_CalculateViewProjection_m220_MethodInfo,
	&CameraMotionBlur_Start_m221_MethodInfo,
	&CameraMotionBlur_OnEnable_m222_MethodInfo,
	&CameraMotionBlur_OnDisable_m223_MethodInfo,
	&CameraMotionBlur_CheckResources_m224_MethodInfo,
	&CameraMotionBlur_OnRenderImage_m225_MethodInfo,
	&CameraMotionBlur_Remember_m226_MethodInfo,
	&CameraMotionBlur_GetTmpCam_m227_MethodInfo,
	&CameraMotionBlur_StartFrame_m228_MethodInfo,
	&CameraMotionBlur_divRoundUp_m229_MethodInfo,
	NULL
};
static const Il2CppType* CameraMotionBlur_t79_il2cpp_TypeInfo__nestedTypes[1] =
{
	&MotionBlurFilter_t77_0_0_0,
};
extern const MethodInfo CameraMotionBlur_CheckResources_m224_MethodInfo;
static const Il2CppMethodReference CameraMotionBlur_t79_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	&CameraMotionBlur_CheckResources_m224_MethodInfo,
};
static bool CameraMotionBlur_t79_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType CameraMotionBlur_t79_1_0_0;
struct CameraMotionBlur_t79;
const Il2CppTypeDefinitionMetadata CameraMotionBlur_t79_DefinitionMetadata = 
{
	NULL/* declaringType */
	, CameraMotionBlur_t79_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &PostEffectsBase_t57_0_0_0/* parent */
	, CameraMotionBlur_t79_VTable/* vtableMethods */
	, CameraMotionBlur_t79_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 336/* fieldStart */

};
TypeInfo CameraMotionBlur_t79_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraMotionBlur"/* name */
	, "UnityStandardAssets.ImageEffects"/* namespaze */
	, CameraMotionBlur_t79_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CameraMotionBlur_t79_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 69/* custom_attributes_cache */
	, &CameraMotionBlur_t79_0_0_0/* byval_arg */
	, &CameraMotionBlur_t79_1_0_0/* this_arg */
	, &CameraMotionBlur_t79_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraMotionBlur_t79)/* instance_size */
	, sizeof (CameraMotionBlur_t79)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraMotionBlur_t79_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 12/* method_count */
	, 0/* property_count */
	, 30/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.ImageEffects.ColorCorrectionCurves/ColorCorrectionMode
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_21.h"
// Metadata Definition UnityStandardAssets.ImageEffects.ColorCorrectionCurves/ColorCorrectionMode
extern TypeInfo ColorCorrectionMode_t81_il2cpp_TypeInfo;
// UnityStandardAssets.ImageEffects.ColorCorrectionCurves/ColorCorrectionMode
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_21MethodDeclarations.h"
static const MethodInfo* ColorCorrectionMode_t81_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference ColorCorrectionMode_t81_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool ColorCorrectionMode_t81_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ColorCorrectionMode_t81_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType ColorCorrectionMode_t81_0_0_0;
extern const Il2CppType ColorCorrectionMode_t81_1_0_0;
extern TypeInfo ColorCorrectionCurves_t83_il2cpp_TypeInfo;
extern const Il2CppType ColorCorrectionCurves_t83_0_0_0;
const Il2CppTypeDefinitionMetadata ColorCorrectionMode_t81_DefinitionMetadata = 
{
	&ColorCorrectionCurves_t83_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ColorCorrectionMode_t81_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, ColorCorrectionMode_t81_VTable/* vtableMethods */
	, ColorCorrectionMode_t81_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 366/* fieldStart */

};
TypeInfo ColorCorrectionMode_t81_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "ColorCorrectionMode"/* name */
	, ""/* namespaze */
	, ColorCorrectionMode_t81_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ColorCorrectionMode_t81_0_0_0/* byval_arg */
	, &ColorCorrectionMode_t81_1_0_0/* this_arg */
	, &ColorCorrectionMode_t81_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ColorCorrectionMode_t81)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ColorCorrectionMode_t81)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityStandardAssets.ImageEffects.ColorCorrectionCurves
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_22.h"
// Metadata Definition UnityStandardAssets.ImageEffects.ColorCorrectionCurves
// UnityStandardAssets.ImageEffects.ColorCorrectionCurves
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_22MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.ColorCorrectionCurves::.ctor()
extern const MethodInfo ColorCorrectionCurves__ctor_m230_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ColorCorrectionCurves__ctor_m230/* method */
	, &ColorCorrectionCurves_t83_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 243/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.ColorCorrectionCurves::Start()
extern const MethodInfo ColorCorrectionCurves_Start_m231_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&ColorCorrectionCurves_Start_m231/* method */
	, &ColorCorrectionCurves_t83_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 244/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.ColorCorrectionCurves::Awake()
extern const MethodInfo ColorCorrectionCurves_Awake_m232_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&ColorCorrectionCurves_Awake_m232/* method */
	, &ColorCorrectionCurves_t83_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 245/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityStandardAssets.ImageEffects.ColorCorrectionCurves::CheckResources()
extern const MethodInfo ColorCorrectionCurves_CheckResources_m233_MethodInfo = 
{
	"CheckResources"/* name */
	, (methodPointerType)&ColorCorrectionCurves_CheckResources_m233/* method */
	, &ColorCorrectionCurves_t83_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 246/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.ColorCorrectionCurves::UpdateParameters()
extern const MethodInfo ColorCorrectionCurves_UpdateParameters_m234_MethodInfo = 
{
	"UpdateParameters"/* name */
	, (methodPointerType)&ColorCorrectionCurves_UpdateParameters_m234/* method */
	, &ColorCorrectionCurves_t83_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 247/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.ColorCorrectionCurves::UpdateTextures()
extern const MethodInfo ColorCorrectionCurves_UpdateTextures_m235_MethodInfo = 
{
	"UpdateTextures"/* name */
	, (methodPointerType)&ColorCorrectionCurves_UpdateTextures_m235/* method */
	, &ColorCorrectionCurves_t83_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 248/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
static const ParameterInfo ColorCorrectionCurves_t83_ColorCorrectionCurves_OnRenderImage_m236_ParameterInfos[] = 
{
	{"source", 0, 134217889, 0, &RenderTexture_t101_0_0_0},
	{"destination", 1, 134217890, 0, &RenderTexture_t101_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.ColorCorrectionCurves::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern const MethodInfo ColorCorrectionCurves_OnRenderImage_m236_MethodInfo = 
{
	"OnRenderImage"/* name */
	, (methodPointerType)&ColorCorrectionCurves_OnRenderImage_m236/* method */
	, &ColorCorrectionCurves_t83_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, ColorCorrectionCurves_t83_ColorCorrectionCurves_OnRenderImage_m236_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 249/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ColorCorrectionCurves_t83_MethodInfos[] =
{
	&ColorCorrectionCurves__ctor_m230_MethodInfo,
	&ColorCorrectionCurves_Start_m231_MethodInfo,
	&ColorCorrectionCurves_Awake_m232_MethodInfo,
	&ColorCorrectionCurves_CheckResources_m233_MethodInfo,
	&ColorCorrectionCurves_UpdateParameters_m234_MethodInfo,
	&ColorCorrectionCurves_UpdateTextures_m235_MethodInfo,
	&ColorCorrectionCurves_OnRenderImage_m236_MethodInfo,
	NULL
};
static const Il2CppType* ColorCorrectionCurves_t83_il2cpp_TypeInfo__nestedTypes[1] =
{
	&ColorCorrectionMode_t81_0_0_0,
};
extern const MethodInfo ColorCorrectionCurves_CheckResources_m233_MethodInfo;
static const Il2CppMethodReference ColorCorrectionCurves_t83_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	&ColorCorrectionCurves_CheckResources_m233_MethodInfo,
};
static bool ColorCorrectionCurves_t83_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType ColorCorrectionCurves_t83_1_0_0;
struct ColorCorrectionCurves_t83;
const Il2CppTypeDefinitionMetadata ColorCorrectionCurves_t83_DefinitionMetadata = 
{
	NULL/* declaringType */
	, ColorCorrectionCurves_t83_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &PostEffectsBase_t57_0_0_0/* parent */
	, ColorCorrectionCurves_t83_VTable/* vtableMethods */
	, ColorCorrectionCurves_t83_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 369/* fieldStart */

};
TypeInfo ColorCorrectionCurves_t83_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "ColorCorrectionCurves"/* name */
	, "UnityStandardAssets.ImageEffects"/* namespaze */
	, ColorCorrectionCurves_t83_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ColorCorrectionCurves_t83_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 70/* custom_attributes_cache */
	, &ColorCorrectionCurves_t83_0_0_0/* byval_arg */
	, &ColorCorrectionCurves_t83_1_0_0/* this_arg */
	, &ColorCorrectionCurves_t83_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ColorCorrectionCurves_t83)/* instance_size */
	, sizeof (ColorCorrectionCurves_t83)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 24/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.ImageEffects.ColorCorrectionLookup
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_23.h"
// Metadata Definition UnityStandardAssets.ImageEffects.ColorCorrectionLookup
extern TypeInfo ColorCorrectionLookup_t85_il2cpp_TypeInfo;
// UnityStandardAssets.ImageEffects.ColorCorrectionLookup
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_23MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.ColorCorrectionLookup::.ctor()
extern const MethodInfo ColorCorrectionLookup__ctor_m237_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ColorCorrectionLookup__ctor_m237/* method */
	, &ColorCorrectionLookup_t85_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 250/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityStandardAssets.ImageEffects.ColorCorrectionLookup::CheckResources()
extern const MethodInfo ColorCorrectionLookup_CheckResources_m238_MethodInfo = 
{
	"CheckResources"/* name */
	, (methodPointerType)&ColorCorrectionLookup_CheckResources_m238/* method */
	, &ColorCorrectionLookup_t85_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 251/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.ColorCorrectionLookup::OnDisable()
extern const MethodInfo ColorCorrectionLookup_OnDisable_m239_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&ColorCorrectionLookup_OnDisable_m239/* method */
	, &ColorCorrectionLookup_t85_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 252/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.ColorCorrectionLookup::OnDestroy()
extern const MethodInfo ColorCorrectionLookup_OnDestroy_m240_MethodInfo = 
{
	"OnDestroy"/* name */
	, (methodPointerType)&ColorCorrectionLookup_OnDestroy_m240/* method */
	, &ColorCorrectionLookup_t85_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 253/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.ColorCorrectionLookup::SetIdentityLut()
extern const MethodInfo ColorCorrectionLookup_SetIdentityLut_m241_MethodInfo = 
{
	"SetIdentityLut"/* name */
	, (methodPointerType)&ColorCorrectionLookup_SetIdentityLut_m241/* method */
	, &ColorCorrectionLookup_t85_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 254/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Texture2D_t63_0_0_0;
extern const Il2CppType Texture2D_t63_0_0_0;
static const ParameterInfo ColorCorrectionLookup_t85_ColorCorrectionLookup_ValidDimensions_m242_ParameterInfos[] = 
{
	{"tex2d", 0, 134217891, 0, &Texture2D_t63_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityStandardAssets.ImageEffects.ColorCorrectionLookup::ValidDimensions(UnityEngine.Texture2D)
extern const MethodInfo ColorCorrectionLookup_ValidDimensions_m242_MethodInfo = 
{
	"ValidDimensions"/* name */
	, (methodPointerType)&ColorCorrectionLookup_ValidDimensions_m242/* method */
	, &ColorCorrectionLookup_t85_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, ColorCorrectionLookup_t85_ColorCorrectionLookup_ValidDimensions_m242_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 255/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Texture2D_t63_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo ColorCorrectionLookup_t85_ColorCorrectionLookup_Convert_m243_ParameterInfos[] = 
{
	{"temp2DTex", 0, 134217892, 0, &Texture2D_t63_0_0_0},
	{"path", 1, 134217893, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.ColorCorrectionLookup::Convert(UnityEngine.Texture2D,System.String)
extern const MethodInfo ColorCorrectionLookup_Convert_m243_MethodInfo = 
{
	"Convert"/* name */
	, (methodPointerType)&ColorCorrectionLookup_Convert_m243/* method */
	, &ColorCorrectionLookup_t85_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, ColorCorrectionLookup_t85_ColorCorrectionLookup_Convert_m243_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 256/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
static const ParameterInfo ColorCorrectionLookup_t85_ColorCorrectionLookup_OnRenderImage_m244_ParameterInfos[] = 
{
	{"source", 0, 134217894, 0, &RenderTexture_t101_0_0_0},
	{"destination", 1, 134217895, 0, &RenderTexture_t101_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.ColorCorrectionLookup::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern const MethodInfo ColorCorrectionLookup_OnRenderImage_m244_MethodInfo = 
{
	"OnRenderImage"/* name */
	, (methodPointerType)&ColorCorrectionLookup_OnRenderImage_m244/* method */
	, &ColorCorrectionLookup_t85_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, ColorCorrectionLookup_t85_ColorCorrectionLookup_OnRenderImage_m244_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 257/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ColorCorrectionLookup_t85_MethodInfos[] =
{
	&ColorCorrectionLookup__ctor_m237_MethodInfo,
	&ColorCorrectionLookup_CheckResources_m238_MethodInfo,
	&ColorCorrectionLookup_OnDisable_m239_MethodInfo,
	&ColorCorrectionLookup_OnDestroy_m240_MethodInfo,
	&ColorCorrectionLookup_SetIdentityLut_m241_MethodInfo,
	&ColorCorrectionLookup_ValidDimensions_m242_MethodInfo,
	&ColorCorrectionLookup_Convert_m243_MethodInfo,
	&ColorCorrectionLookup_OnRenderImage_m244_MethodInfo,
	NULL
};
extern const MethodInfo ColorCorrectionLookup_CheckResources_m238_MethodInfo;
static const Il2CppMethodReference ColorCorrectionLookup_t85_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	&ColorCorrectionLookup_CheckResources_m238_MethodInfo,
};
static bool ColorCorrectionLookup_t85_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType ColorCorrectionLookup_t85_0_0_0;
extern const Il2CppType ColorCorrectionLookup_t85_1_0_0;
struct ColorCorrectionLookup_t85;
const Il2CppTypeDefinitionMetadata ColorCorrectionLookup_t85_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &PostEffectsBase_t57_0_0_0/* parent */
	, ColorCorrectionLookup_t85_VTable/* vtableMethods */
	, ColorCorrectionLookup_t85_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 393/* fieldStart */

};
TypeInfo ColorCorrectionLookup_t85_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "ColorCorrectionLookup"/* name */
	, "UnityStandardAssets.ImageEffects"/* namespaze */
	, ColorCorrectionLookup_t85_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ColorCorrectionLookup_t85_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 71/* custom_attributes_cache */
	, &ColorCorrectionLookup_t85_0_0_0/* byval_arg */
	, &ColorCorrectionLookup_t85_1_0_0/* this_arg */
	, &ColorCorrectionLookup_t85_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ColorCorrectionLookup_t85)/* instance_size */
	, sizeof (ColorCorrectionLookup_t85)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.ImageEffects.ColorCorrectionRamp
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_24.h"
// Metadata Definition UnityStandardAssets.ImageEffects.ColorCorrectionRamp
extern TypeInfo ColorCorrectionRamp_t87_il2cpp_TypeInfo;
// UnityStandardAssets.ImageEffects.ColorCorrectionRamp
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_24MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.ColorCorrectionRamp::.ctor()
extern const MethodInfo ColorCorrectionRamp__ctor_m245_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ColorCorrectionRamp__ctor_m245/* method */
	, &ColorCorrectionRamp_t87_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 258/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
static const ParameterInfo ColorCorrectionRamp_t87_ColorCorrectionRamp_OnRenderImage_m246_ParameterInfos[] = 
{
	{"source", 0, 134217896, 0, &RenderTexture_t101_0_0_0},
	{"destination", 1, 134217897, 0, &RenderTexture_t101_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.ColorCorrectionRamp::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern const MethodInfo ColorCorrectionRamp_OnRenderImage_m246_MethodInfo = 
{
	"OnRenderImage"/* name */
	, (methodPointerType)&ColorCorrectionRamp_OnRenderImage_m246/* method */
	, &ColorCorrectionRamp_t87_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, ColorCorrectionRamp_t87_ColorCorrectionRamp_OnRenderImage_m246_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 259/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ColorCorrectionRamp_t87_MethodInfos[] =
{
	&ColorCorrectionRamp__ctor_m245_MethodInfo,
	&ColorCorrectionRamp_OnRenderImage_m246_MethodInfo,
	NULL
};
extern const MethodInfo ImageEffectBase_Start_m305_MethodInfo;
extern const MethodInfo ImageEffectBase_OnDisable_m307_MethodInfo;
static const Il2CppMethodReference ColorCorrectionRamp_t87_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	&ImageEffectBase_Start_m305_MethodInfo,
	&ImageEffectBase_OnDisable_m307_MethodInfo,
};
static bool ColorCorrectionRamp_t87_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType ColorCorrectionRamp_t87_0_0_0;
extern const Il2CppType ColorCorrectionRamp_t87_1_0_0;
extern const Il2CppType ImageEffectBase_t88_0_0_0;
struct ColorCorrectionRamp_t87;
const Il2CppTypeDefinitionMetadata ColorCorrectionRamp_t87_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ImageEffectBase_t88_0_0_0/* parent */
	, ColorCorrectionRamp_t87_VTable/* vtableMethods */
	, ColorCorrectionRamp_t87_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 397/* fieldStart */

};
TypeInfo ColorCorrectionRamp_t87_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "ColorCorrectionRamp"/* name */
	, "UnityStandardAssets.ImageEffects"/* namespaze */
	, ColorCorrectionRamp_t87_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ColorCorrectionRamp_t87_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 72/* custom_attributes_cache */
	, &ColorCorrectionRamp_t87_0_0_0/* byval_arg */
	, &ColorCorrectionRamp_t87_1_0_0/* this_arg */
	, &ColorCorrectionRamp_t87_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ColorCorrectionRamp_t87)/* instance_size */
	, sizeof (ColorCorrectionRamp_t87)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.ImageEffects.ContrastEnhance
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_26.h"
// Metadata Definition UnityStandardAssets.ImageEffects.ContrastEnhance
extern TypeInfo ContrastEnhance_t89_il2cpp_TypeInfo;
// UnityStandardAssets.ImageEffects.ContrastEnhance
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_26MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.ContrastEnhance::.ctor()
extern const MethodInfo ContrastEnhance__ctor_m247_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ContrastEnhance__ctor_m247/* method */
	, &ContrastEnhance_t89_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 260/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityStandardAssets.ImageEffects.ContrastEnhance::CheckResources()
extern const MethodInfo ContrastEnhance_CheckResources_m248_MethodInfo = 
{
	"CheckResources"/* name */
	, (methodPointerType)&ContrastEnhance_CheckResources_m248/* method */
	, &ContrastEnhance_t89_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 261/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
static const ParameterInfo ContrastEnhance_t89_ContrastEnhance_OnRenderImage_m249_ParameterInfos[] = 
{
	{"source", 0, 134217898, 0, &RenderTexture_t101_0_0_0},
	{"destination", 1, 134217899, 0, &RenderTexture_t101_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.ContrastEnhance::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern const MethodInfo ContrastEnhance_OnRenderImage_m249_MethodInfo = 
{
	"OnRenderImage"/* name */
	, (methodPointerType)&ContrastEnhance_OnRenderImage_m249/* method */
	, &ContrastEnhance_t89_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, ContrastEnhance_t89_ContrastEnhance_OnRenderImage_m249_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 262/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ContrastEnhance_t89_MethodInfos[] =
{
	&ContrastEnhance__ctor_m247_MethodInfo,
	&ContrastEnhance_CheckResources_m248_MethodInfo,
	&ContrastEnhance_OnRenderImage_m249_MethodInfo,
	NULL
};
extern const MethodInfo ContrastEnhance_CheckResources_m248_MethodInfo;
static const Il2CppMethodReference ContrastEnhance_t89_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	&ContrastEnhance_CheckResources_m248_MethodInfo,
};
static bool ContrastEnhance_t89_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType ContrastEnhance_t89_0_0_0;
extern const Il2CppType ContrastEnhance_t89_1_0_0;
struct ContrastEnhance_t89;
const Il2CppTypeDefinitionMetadata ContrastEnhance_t89_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &PostEffectsBase_t57_0_0_0/* parent */
	, ContrastEnhance_t89_VTable/* vtableMethods */
	, ContrastEnhance_t89_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 398/* fieldStart */

};
TypeInfo ContrastEnhance_t89_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "ContrastEnhance"/* name */
	, "UnityStandardAssets.ImageEffects"/* namespaze */
	, ContrastEnhance_t89_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ContrastEnhance_t89_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 73/* custom_attributes_cache */
	, &ContrastEnhance_t89_0_0_0/* byval_arg */
	, &ContrastEnhance_t89_1_0_0/* this_arg */
	, &ContrastEnhance_t89_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ContrastEnhance_t89)/* instance_size */
	, sizeof (ContrastEnhance_t89)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.ImageEffects.ContrastStretch
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_27.h"
// Metadata Definition UnityStandardAssets.ImageEffects.ContrastStretch
extern TypeInfo ContrastStretch_t91_il2cpp_TypeInfo;
// UnityStandardAssets.ImageEffects.ContrastStretch
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_27MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.ContrastStretch::.ctor()
extern const MethodInfo ContrastStretch__ctor_m250_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ContrastStretch__ctor_m250/* method */
	, &ContrastStretch_t91_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 263/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Material UnityStandardAssets.ImageEffects.ContrastStretch::get_materialLum()
extern const MethodInfo ContrastStretch_get_materialLum_m251_MethodInfo = 
{
	"get_materialLum"/* name */
	, (methodPointerType)&ContrastStretch_get_materialLum_m251/* method */
	, &ContrastStretch_t91_il2cpp_TypeInfo/* declaring_type */
	, &Material_t55_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2180/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 264/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Material UnityStandardAssets.ImageEffects.ContrastStretch::get_materialReduce()
extern const MethodInfo ContrastStretch_get_materialReduce_m252_MethodInfo = 
{
	"get_materialReduce"/* name */
	, (methodPointerType)&ContrastStretch_get_materialReduce_m252/* method */
	, &ContrastStretch_t91_il2cpp_TypeInfo/* declaring_type */
	, &Material_t55_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2180/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 265/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Material UnityStandardAssets.ImageEffects.ContrastStretch::get_materialAdapt()
extern const MethodInfo ContrastStretch_get_materialAdapt_m253_MethodInfo = 
{
	"get_materialAdapt"/* name */
	, (methodPointerType)&ContrastStretch_get_materialAdapt_m253/* method */
	, &ContrastStretch_t91_il2cpp_TypeInfo/* declaring_type */
	, &Material_t55_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2180/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 266/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Material UnityStandardAssets.ImageEffects.ContrastStretch::get_materialApply()
extern const MethodInfo ContrastStretch_get_materialApply_m254_MethodInfo = 
{
	"get_materialApply"/* name */
	, (methodPointerType)&ContrastStretch_get_materialApply_m254/* method */
	, &ContrastStretch_t91_il2cpp_TypeInfo/* declaring_type */
	, &Material_t55_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2180/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 267/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.ContrastStretch::Start()
extern const MethodInfo ContrastStretch_Start_m255_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&ContrastStretch_Start_m255/* method */
	, &ContrastStretch_t91_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 268/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.ContrastStretch::OnEnable()
extern const MethodInfo ContrastStretch_OnEnable_m256_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&ContrastStretch_OnEnable_m256/* method */
	, &ContrastStretch_t91_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 269/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.ContrastStretch::OnDisable()
extern const MethodInfo ContrastStretch_OnDisable_m257_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&ContrastStretch_OnDisable_m257/* method */
	, &ContrastStretch_t91_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 270/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
static const ParameterInfo ContrastStretch_t91_ContrastStretch_OnRenderImage_m258_ParameterInfos[] = 
{
	{"source", 0, 134217900, 0, &RenderTexture_t101_0_0_0},
	{"destination", 1, 134217901, 0, &RenderTexture_t101_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.ContrastStretch::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern const MethodInfo ContrastStretch_OnRenderImage_m258_MethodInfo = 
{
	"OnRenderImage"/* name */
	, (methodPointerType)&ContrastStretch_OnRenderImage_m258/* method */
	, &ContrastStretch_t91_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, ContrastStretch_t91_ContrastStretch_OnRenderImage_m258_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 271/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Texture_t86_0_0_0;
extern const Il2CppType Texture_t86_0_0_0;
static const ParameterInfo ContrastStretch_t91_ContrastStretch_CalculateAdaptation_m259_ParameterInfos[] = 
{
	{"curTexture", 0, 134217902, 0, &Texture_t86_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.ContrastStretch::CalculateAdaptation(UnityEngine.Texture)
extern const MethodInfo ContrastStretch_CalculateAdaptation_m259_MethodInfo = 
{
	"CalculateAdaptation"/* name */
	, (methodPointerType)&ContrastStretch_CalculateAdaptation_m259/* method */
	, &ContrastStretch_t91_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, ContrastStretch_t91_ContrastStretch_CalculateAdaptation_m259_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 272/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ContrastStretch_t91_MethodInfos[] =
{
	&ContrastStretch__ctor_m250_MethodInfo,
	&ContrastStretch_get_materialLum_m251_MethodInfo,
	&ContrastStretch_get_materialReduce_m252_MethodInfo,
	&ContrastStretch_get_materialAdapt_m253_MethodInfo,
	&ContrastStretch_get_materialApply_m254_MethodInfo,
	&ContrastStretch_Start_m255_MethodInfo,
	&ContrastStretch_OnEnable_m256_MethodInfo,
	&ContrastStretch_OnDisable_m257_MethodInfo,
	&ContrastStretch_OnRenderImage_m258_MethodInfo,
	&ContrastStretch_CalculateAdaptation_m259_MethodInfo,
	NULL
};
extern const MethodInfo ContrastStretch_get_materialLum_m251_MethodInfo;
static const PropertyInfo ContrastStretch_t91____materialLum_PropertyInfo = 
{
	&ContrastStretch_t91_il2cpp_TypeInfo/* parent */
	, "materialLum"/* name */
	, &ContrastStretch_get_materialLum_m251_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ContrastStretch_get_materialReduce_m252_MethodInfo;
static const PropertyInfo ContrastStretch_t91____materialReduce_PropertyInfo = 
{
	&ContrastStretch_t91_il2cpp_TypeInfo/* parent */
	, "materialReduce"/* name */
	, &ContrastStretch_get_materialReduce_m252_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ContrastStretch_get_materialAdapt_m253_MethodInfo;
static const PropertyInfo ContrastStretch_t91____materialAdapt_PropertyInfo = 
{
	&ContrastStretch_t91_il2cpp_TypeInfo/* parent */
	, "materialAdapt"/* name */
	, &ContrastStretch_get_materialAdapt_m253_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ContrastStretch_get_materialApply_m254_MethodInfo;
static const PropertyInfo ContrastStretch_t91____materialApply_PropertyInfo = 
{
	&ContrastStretch_t91_il2cpp_TypeInfo/* parent */
	, "materialApply"/* name */
	, &ContrastStretch_get_materialApply_m254_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ContrastStretch_t91_PropertyInfos[] =
{
	&ContrastStretch_t91____materialLum_PropertyInfo,
	&ContrastStretch_t91____materialReduce_PropertyInfo,
	&ContrastStretch_t91____materialAdapt_PropertyInfo,
	&ContrastStretch_t91____materialApply_PropertyInfo,
	NULL
};
static const Il2CppMethodReference ContrastStretch_t91_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool ContrastStretch_t91_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType ContrastStretch_t91_0_0_0;
extern const Il2CppType ContrastStretch_t91_1_0_0;
struct ContrastStretch_t91;
const Il2CppTypeDefinitionMetadata ContrastStretch_t91_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, ContrastStretch_t91_VTable/* vtableMethods */
	, ContrastStretch_t91_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 405/* fieldStart */

};
TypeInfo ContrastStretch_t91_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "ContrastStretch"/* name */
	, "UnityStandardAssets.ImageEffects"/* namespaze */
	, ContrastStretch_t91_MethodInfos/* methods */
	, ContrastStretch_t91_PropertyInfos/* properties */
	, NULL/* events */
	, &ContrastStretch_t91_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 74/* custom_attributes_cache */
	, &ContrastStretch_t91_0_0_0/* byval_arg */
	, &ContrastStretch_t91_1_0_0/* this_arg */
	, &ContrastStretch_t91_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ContrastStretch_t91)/* instance_size */
	, sizeof (ContrastStretch_t91)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 4/* property_count */
	, 13/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.ImageEffects.CreaseShading
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_28.h"
// Metadata Definition UnityStandardAssets.ImageEffects.CreaseShading
extern TypeInfo CreaseShading_t92_il2cpp_TypeInfo;
// UnityStandardAssets.ImageEffects.CreaseShading
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_28MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.CreaseShading::.ctor()
extern const MethodInfo CreaseShading__ctor_m260_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CreaseShading__ctor_m260/* method */
	, &CreaseShading_t92_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 273/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityStandardAssets.ImageEffects.CreaseShading::CheckResources()
extern const MethodInfo CreaseShading_CheckResources_m261_MethodInfo = 
{
	"CheckResources"/* name */
	, (methodPointerType)&CreaseShading_CheckResources_m261/* method */
	, &CreaseShading_t92_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 274/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
static const ParameterInfo CreaseShading_t92_CreaseShading_OnRenderImage_m262_ParameterInfos[] = 
{
	{"source", 0, 134217903, 0, &RenderTexture_t101_0_0_0},
	{"destination", 1, 134217904, 0, &RenderTexture_t101_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.CreaseShading::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern const MethodInfo CreaseShading_OnRenderImage_m262_MethodInfo = 
{
	"OnRenderImage"/* name */
	, (methodPointerType)&CreaseShading_OnRenderImage_m262/* method */
	, &CreaseShading_t92_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, CreaseShading_t92_CreaseShading_OnRenderImage_m262_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 275/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CreaseShading_t92_MethodInfos[] =
{
	&CreaseShading__ctor_m260_MethodInfo,
	&CreaseShading_CheckResources_m261_MethodInfo,
	&CreaseShading_OnRenderImage_m262_MethodInfo,
	NULL
};
extern const MethodInfo CreaseShading_CheckResources_m261_MethodInfo;
static const Il2CppMethodReference CreaseShading_t92_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	&CreaseShading_CheckResources_m261_MethodInfo,
};
static bool CreaseShading_t92_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType CreaseShading_t92_0_0_0;
extern const Il2CppType CreaseShading_t92_1_0_0;
struct CreaseShading_t92;
const Il2CppTypeDefinitionMetadata CreaseShading_t92_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &PostEffectsBase_t57_0_0_0/* parent */
	, CreaseShading_t92_VTable/* vtableMethods */
	, CreaseShading_t92_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 418/* fieldStart */

};
TypeInfo CreaseShading_t92_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "CreaseShading"/* name */
	, "UnityStandardAssets.ImageEffects"/* namespaze */
	, CreaseShading_t92_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CreaseShading_t92_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 75/* custom_attributes_cache */
	, &CreaseShading_t92_0_0_0/* byval_arg */
	, &CreaseShading_t92_1_0_0/* this_arg */
	, &CreaseShading_t92_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CreaseShading_t92)/* instance_size */
	, sizeof (CreaseShading_t92)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 9/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.ImageEffects.DepthOfField/BlurType
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_29.h"
// Metadata Definition UnityStandardAssets.ImageEffects.DepthOfField/BlurType
extern TypeInfo BlurType_t93_il2cpp_TypeInfo;
// UnityStandardAssets.ImageEffects.DepthOfField/BlurType
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_29MethodDeclarations.h"
static const MethodInfo* BlurType_t93_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference BlurType_t93_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool BlurType_t93_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair BlurType_t93_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType BlurType_t93_0_0_0;
extern const Il2CppType BlurType_t93_1_0_0;
extern TypeInfo DepthOfField_t96_il2cpp_TypeInfo;
extern const Il2CppType DepthOfField_t96_0_0_0;
const Il2CppTypeDefinitionMetadata BlurType_t93_DefinitionMetadata = 
{
	&DepthOfField_t96_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, BlurType_t93_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, BlurType_t93_VTable/* vtableMethods */
	, BlurType_t93_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 427/* fieldStart */

};
TypeInfo BlurType_t93_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "BlurType"/* name */
	, ""/* namespaze */
	, BlurType_t93_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BlurType_t93_0_0_0/* byval_arg */
	, &BlurType_t93_1_0_0/* this_arg */
	, &BlurType_t93_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BlurType_t93)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (BlurType_t93)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityStandardAssets.ImageEffects.DepthOfField/BlurSampleCount
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_30.h"
// Metadata Definition UnityStandardAssets.ImageEffects.DepthOfField/BlurSampleCount
extern TypeInfo BlurSampleCount_t94_il2cpp_TypeInfo;
// UnityStandardAssets.ImageEffects.DepthOfField/BlurSampleCount
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_30MethodDeclarations.h"
static const MethodInfo* BlurSampleCount_t94_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference BlurSampleCount_t94_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool BlurSampleCount_t94_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair BlurSampleCount_t94_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType BlurSampleCount_t94_0_0_0;
extern const Il2CppType BlurSampleCount_t94_1_0_0;
const Il2CppTypeDefinitionMetadata BlurSampleCount_t94_DefinitionMetadata = 
{
	&DepthOfField_t96_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, BlurSampleCount_t94_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, BlurSampleCount_t94_VTable/* vtableMethods */
	, BlurSampleCount_t94_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 430/* fieldStart */

};
TypeInfo BlurSampleCount_t94_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "BlurSampleCount"/* name */
	, ""/* namespaze */
	, BlurSampleCount_t94_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BlurSampleCount_t94_0_0_0/* byval_arg */
	, &BlurSampleCount_t94_1_0_0/* this_arg */
	, &BlurSampleCount_t94_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BlurSampleCount_t94)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (BlurSampleCount_t94)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityStandardAssets.ImageEffects.DepthOfField
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_31.h"
// Metadata Definition UnityStandardAssets.ImageEffects.DepthOfField
// UnityStandardAssets.ImageEffects.DepthOfField
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_31MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.DepthOfField::.ctor()
extern const MethodInfo DepthOfField__ctor_m263_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DepthOfField__ctor_m263/* method */
	, &DepthOfField_t96_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 276/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityStandardAssets.ImageEffects.DepthOfField::CheckResources()
extern const MethodInfo DepthOfField_CheckResources_m264_MethodInfo = 
{
	"CheckResources"/* name */
	, (methodPointerType)&DepthOfField_CheckResources_m264/* method */
	, &DepthOfField_t96_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 277/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.DepthOfField::OnEnable()
extern const MethodInfo DepthOfField_OnEnable_m265_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&DepthOfField_OnEnable_m265/* method */
	, &DepthOfField_t96_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 278/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.DepthOfField::OnDisable()
extern const MethodInfo DepthOfField_OnDisable_m266_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&DepthOfField_OnDisable_m266/* method */
	, &DepthOfField_t96_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 279/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.DepthOfField::ReleaseComputeResources()
extern const MethodInfo DepthOfField_ReleaseComputeResources_m267_MethodInfo = 
{
	"ReleaseComputeResources"/* name */
	, (methodPointerType)&DepthOfField_ReleaseComputeResources_m267/* method */
	, &DepthOfField_t96_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 280/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.DepthOfField::CreateComputeResources()
extern const MethodInfo DepthOfField_CreateComputeResources_m268_MethodInfo = 
{
	"CreateComputeResources"/* name */
	, (methodPointerType)&DepthOfField_CreateComputeResources_m268/* method */
	, &DepthOfField_t96_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 281/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo DepthOfField_t96_DepthOfField_FocalDistance01_m269_ParameterInfos[] = 
{
	{"worldDist", 0, 134217905, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Single_t254_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityStandardAssets.ImageEffects.DepthOfField::FocalDistance01(System.Single)
extern const MethodInfo DepthOfField_FocalDistance01_m269_MethodInfo = 
{
	"FocalDistance01"/* name */
	, (methodPointerType)&DepthOfField_FocalDistance01_m269/* method */
	, &DepthOfField_t96_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254_Single_t254/* invoker_method */
	, DepthOfField_t96_DepthOfField_FocalDistance01_m269_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 282/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo DepthOfField_t96_DepthOfField_WriteCoc_m270_ParameterInfos[] = 
{
	{"fromTo", 0, 134217906, 0, &RenderTexture_t101_0_0_0},
	{"fgDilate", 1, 134217907, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.DepthOfField::WriteCoc(UnityEngine.RenderTexture,System.Boolean)
extern const MethodInfo DepthOfField_WriteCoc_m270_MethodInfo = 
{
	"WriteCoc"/* name */
	, (methodPointerType)&DepthOfField_WriteCoc_m270/* method */
	, &DepthOfField_t96_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_SByte_t274/* invoker_method */
	, DepthOfField_t96_DepthOfField_WriteCoc_m270_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 283/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
static const ParameterInfo DepthOfField_t96_DepthOfField_OnRenderImage_m271_ParameterInfos[] = 
{
	{"source", 0, 134217908, 0, &RenderTexture_t101_0_0_0},
	{"destination", 1, 134217909, 0, &RenderTexture_t101_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.DepthOfField::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern const MethodInfo DepthOfField_OnRenderImage_m271_MethodInfo = 
{
	"OnRenderImage"/* name */
	, (methodPointerType)&DepthOfField_OnRenderImage_m271/* method */
	, &DepthOfField_t96_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, DepthOfField_t96_DepthOfField_OnRenderImage_m271_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 284/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* DepthOfField_t96_MethodInfos[] =
{
	&DepthOfField__ctor_m263_MethodInfo,
	&DepthOfField_CheckResources_m264_MethodInfo,
	&DepthOfField_OnEnable_m265_MethodInfo,
	&DepthOfField_OnDisable_m266_MethodInfo,
	&DepthOfField_ReleaseComputeResources_m267_MethodInfo,
	&DepthOfField_CreateComputeResources_m268_MethodInfo,
	&DepthOfField_FocalDistance01_m269_MethodInfo,
	&DepthOfField_WriteCoc_m270_MethodInfo,
	&DepthOfField_OnRenderImage_m271_MethodInfo,
	NULL
};
static const Il2CppType* DepthOfField_t96_il2cpp_TypeInfo__nestedTypes[2] =
{
	&BlurType_t93_0_0_0,
	&BlurSampleCount_t94_0_0_0,
};
extern const MethodInfo DepthOfField_CheckResources_m264_MethodInfo;
static const Il2CppMethodReference DepthOfField_t96_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	&DepthOfField_CheckResources_m264_MethodInfo,
};
static bool DepthOfField_t96_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType DepthOfField_t96_1_0_0;
struct DepthOfField_t96;
const Il2CppTypeDefinitionMetadata DepthOfField_t96_DefinitionMetadata = 
{
	NULL/* declaringType */
	, DepthOfField_t96_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &PostEffectsBase_t57_0_0_0/* parent */
	, DepthOfField_t96_VTable/* vtableMethods */
	, DepthOfField_t96_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 434/* fieldStart */

};
TypeInfo DepthOfField_t96_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "DepthOfField"/* name */
	, "UnityStandardAssets.ImageEffects"/* namespaze */
	, DepthOfField_t96_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &DepthOfField_t96_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 76/* custom_attributes_cache */
	, &DepthOfField_t96_0_0_0/* byval_arg */
	, &DepthOfField_t96_1_0_0/* this_arg */
	, &DepthOfField_t96_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DepthOfField_t96)/* instance_size */
	, sizeof (DepthOfField_t96)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 0/* property_count */
	, 24/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated/Dof34QualitySetting
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_32.h"
// Metadata Definition UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated/Dof34QualitySetting
extern TypeInfo Dof34QualitySetting_t97_il2cpp_TypeInfo;
// UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated/Dof34QualitySetting
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_32MethodDeclarations.h"
static const MethodInfo* Dof34QualitySetting_t97_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference Dof34QualitySetting_t97_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool Dof34QualitySetting_t97_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Dof34QualitySetting_t97_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType Dof34QualitySetting_t97_0_0_0;
extern const Il2CppType Dof34QualitySetting_t97_1_0_0;
extern TypeInfo DepthOfFieldDeprecated_t102_il2cpp_TypeInfo;
extern const Il2CppType DepthOfFieldDeprecated_t102_0_0_0;
const Il2CppTypeDefinitionMetadata Dof34QualitySetting_t97_DefinitionMetadata = 
{
	&DepthOfFieldDeprecated_t102_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Dof34QualitySetting_t97_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, Dof34QualitySetting_t97_VTable/* vtableMethods */
	, Dof34QualitySetting_t97_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 458/* fieldStart */

};
TypeInfo Dof34QualitySetting_t97_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "Dof34QualitySetting"/* name */
	, ""/* namespaze */
	, Dof34QualitySetting_t97_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Dof34QualitySetting_t97_0_0_0/* byval_arg */
	, &Dof34QualitySetting_t97_1_0_0/* this_arg */
	, &Dof34QualitySetting_t97_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Dof34QualitySetting_t97)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Dof34QualitySetting_t97)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated/DofResolution
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_33.h"
// Metadata Definition UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated/DofResolution
extern TypeInfo DofResolution_t98_il2cpp_TypeInfo;
// UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated/DofResolution
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_33MethodDeclarations.h"
static const MethodInfo* DofResolution_t98_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference DofResolution_t98_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool DofResolution_t98_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair DofResolution_t98_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType DofResolution_t98_0_0_0;
extern const Il2CppType DofResolution_t98_1_0_0;
const Il2CppTypeDefinitionMetadata DofResolution_t98_DefinitionMetadata = 
{
	&DepthOfFieldDeprecated_t102_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DofResolution_t98_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, DofResolution_t98_VTable/* vtableMethods */
	, DofResolution_t98_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 461/* fieldStart */

};
TypeInfo DofResolution_t98_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "DofResolution"/* name */
	, ""/* namespaze */
	, DofResolution_t98_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DofResolution_t98_0_0_0/* byval_arg */
	, &DofResolution_t98_1_0_0/* this_arg */
	, &DofResolution_t98_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DofResolution_t98)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (DofResolution_t98)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated/DofBlurriness
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_34.h"
// Metadata Definition UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated/DofBlurriness
extern TypeInfo DofBlurriness_t99_il2cpp_TypeInfo;
// UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated/DofBlurriness
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_34MethodDeclarations.h"
static const MethodInfo* DofBlurriness_t99_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference DofBlurriness_t99_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool DofBlurriness_t99_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair DofBlurriness_t99_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType DofBlurriness_t99_0_0_0;
extern const Il2CppType DofBlurriness_t99_1_0_0;
const Il2CppTypeDefinitionMetadata DofBlurriness_t99_DefinitionMetadata = 
{
	&DepthOfFieldDeprecated_t102_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DofBlurriness_t99_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, DofBlurriness_t99_VTable/* vtableMethods */
	, DofBlurriness_t99_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 465/* fieldStart */

};
TypeInfo DofBlurriness_t99_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "DofBlurriness"/* name */
	, ""/* namespaze */
	, DofBlurriness_t99_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DofBlurriness_t99_0_0_0/* byval_arg */
	, &DofBlurriness_t99_1_0_0/* this_arg */
	, &DofBlurriness_t99_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DofBlurriness_t99)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (DofBlurriness_t99)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated/BokehDestination
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_35.h"
// Metadata Definition UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated/BokehDestination
extern TypeInfo BokehDestination_t100_il2cpp_TypeInfo;
// UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated/BokehDestination
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_35MethodDeclarations.h"
static const MethodInfo* BokehDestination_t100_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference BokehDestination_t100_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool BokehDestination_t100_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair BokehDestination_t100_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType BokehDestination_t100_0_0_0;
extern const Il2CppType BokehDestination_t100_1_0_0;
const Il2CppTypeDefinitionMetadata BokehDestination_t100_DefinitionMetadata = 
{
	&DepthOfFieldDeprecated_t102_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, BokehDestination_t100_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, BokehDestination_t100_VTable/* vtableMethods */
	, BokehDestination_t100_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 469/* fieldStart */

};
TypeInfo BokehDestination_t100_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "BokehDestination"/* name */
	, ""/* namespaze */
	, BokehDestination_t100_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BokehDestination_t100_0_0_0/* byval_arg */
	, &BokehDestination_t100_1_0_0/* this_arg */
	, &BokehDestination_t100_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BokehDestination_t100)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (BokehDestination_t100)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_36.h"
// Metadata Definition UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated
// UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_36MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::.ctor()
extern const MethodInfo DepthOfFieldDeprecated__ctor_m272_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DepthOfFieldDeprecated__ctor_m272/* method */
	, &DepthOfFieldDeprecated_t102_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 285/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::.cctor()
extern const MethodInfo DepthOfFieldDeprecated__cctor_m273_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&DepthOfFieldDeprecated__cctor_m273/* method */
	, &DepthOfFieldDeprecated_t102_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 286/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::CreateMaterials()
extern const MethodInfo DepthOfFieldDeprecated_CreateMaterials_m274_MethodInfo = 
{
	"CreateMaterials"/* name */
	, (methodPointerType)&DepthOfFieldDeprecated_CreateMaterials_m274/* method */
	, &DepthOfFieldDeprecated_t102_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 287/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::CheckResources()
extern const MethodInfo DepthOfFieldDeprecated_CheckResources_m275_MethodInfo = 
{
	"CheckResources"/* name */
	, (methodPointerType)&DepthOfFieldDeprecated_CheckResources_m275/* method */
	, &DepthOfFieldDeprecated_t102_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 288/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::OnDisable()
extern const MethodInfo DepthOfFieldDeprecated_OnDisable_m276_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&DepthOfFieldDeprecated_OnDisable_m276/* method */
	, &DepthOfFieldDeprecated_t102_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 289/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::OnEnable()
extern const MethodInfo DepthOfFieldDeprecated_OnEnable_m277_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&DepthOfFieldDeprecated_OnEnable_m277/* method */
	, &DepthOfFieldDeprecated_t102_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 290/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo DepthOfFieldDeprecated_t102_DepthOfFieldDeprecated_FocalDistance01_m278_ParameterInfos[] = 
{
	{"worldDist", 0, 134217910, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Single_t254_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::FocalDistance01(System.Single)
extern const MethodInfo DepthOfFieldDeprecated_FocalDistance01_m278_MethodInfo = 
{
	"FocalDistance01"/* name */
	, (methodPointerType)&DepthOfFieldDeprecated_FocalDistance01_m278/* method */
	, &DepthOfFieldDeprecated_t102_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254_Single_t254/* invoker_method */
	, DepthOfFieldDeprecated_t102_DepthOfFieldDeprecated_FocalDistance01_m278_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 291/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::GetDividerBasedOnQuality()
extern const MethodInfo DepthOfFieldDeprecated_GetDividerBasedOnQuality_m279_MethodInfo = 
{
	"GetDividerBasedOnQuality"/* name */
	, (methodPointerType)&DepthOfFieldDeprecated_GetDividerBasedOnQuality_m279/* method */
	, &DepthOfFieldDeprecated_t102_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 292/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo DepthOfFieldDeprecated_t102_DepthOfFieldDeprecated_GetLowResolutionDividerBasedOnQuality_m280_ParameterInfos[] = 
{
	{"baseDivider", 0, 134217911, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::GetLowResolutionDividerBasedOnQuality(System.Int32)
extern const MethodInfo DepthOfFieldDeprecated_GetLowResolutionDividerBasedOnQuality_m280_MethodInfo = 
{
	"GetLowResolutionDividerBasedOnQuality"/* name */
	, (methodPointerType)&DepthOfFieldDeprecated_GetLowResolutionDividerBasedOnQuality_m280/* method */
	, &DepthOfFieldDeprecated_t102_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253_Int32_t253/* invoker_method */
	, DepthOfFieldDeprecated_t102_DepthOfFieldDeprecated_GetLowResolutionDividerBasedOnQuality_m280_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 293/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
static const ParameterInfo DepthOfFieldDeprecated_t102_DepthOfFieldDeprecated_OnRenderImage_m281_ParameterInfos[] = 
{
	{"source", 0, 134217912, 0, &RenderTexture_t101_0_0_0},
	{"destination", 1, 134217913, 0, &RenderTexture_t101_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern const MethodInfo DepthOfFieldDeprecated_OnRenderImage_m281_MethodInfo = 
{
	"OnRenderImage"/* name */
	, (methodPointerType)&DepthOfFieldDeprecated_OnRenderImage_m281/* method */
	, &DepthOfFieldDeprecated_t102_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, DepthOfFieldDeprecated_t102_DepthOfFieldDeprecated_OnRenderImage_m281_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 294/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType DofBlurriness_t99_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo DepthOfFieldDeprecated_t102_DepthOfFieldDeprecated_Blur_m282_ParameterInfos[] = 
{
	{"from", 0, 134217914, 0, &RenderTexture_t101_0_0_0},
	{"to", 1, 134217915, 0, &RenderTexture_t101_0_0_0},
	{"iterations", 2, 134217916, 0, &DofBlurriness_t99_0_0_0},
	{"blurPass", 3, 134217917, 0, &Int32_t253_0_0_0},
	{"spread", 4, 134217918, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t_Int32_t253_Int32_t253_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::Blur(UnityEngine.RenderTexture,UnityEngine.RenderTexture,UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated/DofBlurriness,System.Int32,System.Single)
extern const MethodInfo DepthOfFieldDeprecated_Blur_m282_MethodInfo = 
{
	"Blur"/* name */
	, (methodPointerType)&DepthOfFieldDeprecated_Blur_m282/* method */
	, &DepthOfFieldDeprecated_t102_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t_Int32_t253_Int32_t253_Single_t254/* invoker_method */
	, DepthOfFieldDeprecated_t102_DepthOfFieldDeprecated_Blur_m282_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 295/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType DofBlurriness_t99_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo DepthOfFieldDeprecated_t102_DepthOfFieldDeprecated_BlurFg_m283_ParameterInfos[] = 
{
	{"from", 0, 134217919, 0, &RenderTexture_t101_0_0_0},
	{"to", 1, 134217920, 0, &RenderTexture_t101_0_0_0},
	{"iterations", 2, 134217921, 0, &DofBlurriness_t99_0_0_0},
	{"blurPass", 3, 134217922, 0, &Int32_t253_0_0_0},
	{"spread", 4, 134217923, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t_Int32_t253_Int32_t253_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::BlurFg(UnityEngine.RenderTexture,UnityEngine.RenderTexture,UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated/DofBlurriness,System.Int32,System.Single)
extern const MethodInfo DepthOfFieldDeprecated_BlurFg_m283_MethodInfo = 
{
	"BlurFg"/* name */
	, (methodPointerType)&DepthOfFieldDeprecated_BlurFg_m283/* method */
	, &DepthOfFieldDeprecated_t102_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t_Int32_t253_Int32_t253_Single_t254/* invoker_method */
	, DepthOfFieldDeprecated_t102_DepthOfFieldDeprecated_BlurFg_m283_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 296/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Single_t254_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
static const ParameterInfo DepthOfFieldDeprecated_t102_DepthOfFieldDeprecated_BlurHex_m284_ParameterInfos[] = 
{
	{"from", 0, 134217924, 0, &RenderTexture_t101_0_0_0},
	{"to", 1, 134217925, 0, &RenderTexture_t101_0_0_0},
	{"blurPass", 2, 134217926, 0, &Int32_t253_0_0_0},
	{"spread", 3, 134217927, 0, &Single_t254_0_0_0},
	{"tmp", 4, 134217928, 0, &RenderTexture_t101_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t_Int32_t253_Single_t254_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::BlurHex(UnityEngine.RenderTexture,UnityEngine.RenderTexture,System.Int32,System.Single,UnityEngine.RenderTexture)
extern const MethodInfo DepthOfFieldDeprecated_BlurHex_m284_MethodInfo = 
{
	"BlurHex"/* name */
	, (methodPointerType)&DepthOfFieldDeprecated_BlurHex_m284/* method */
	, &DepthOfFieldDeprecated_t102_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t_Int32_t253_Single_t254_Object_t/* invoker_method */
	, DepthOfFieldDeprecated_t102_DepthOfFieldDeprecated_BlurHex_m284_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 297/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
static const ParameterInfo DepthOfFieldDeprecated_t102_DepthOfFieldDeprecated_Downsample_m285_ParameterInfos[] = 
{
	{"from", 0, 134217929, 0, &RenderTexture_t101_0_0_0},
	{"to", 1, 134217930, 0, &RenderTexture_t101_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::Downsample(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern const MethodInfo DepthOfFieldDeprecated_Downsample_m285_MethodInfo = 
{
	"Downsample"/* name */
	, (methodPointerType)&DepthOfFieldDeprecated_Downsample_m285/* method */
	, &DepthOfFieldDeprecated_t102_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, DepthOfFieldDeprecated_t102_DepthOfFieldDeprecated_Downsample_m285_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 298/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
static const ParameterInfo DepthOfFieldDeprecated_t102_DepthOfFieldDeprecated_AddBokeh_m286_ParameterInfos[] = 
{
	{"bokehInfo", 0, 134217931, 0, &RenderTexture_t101_0_0_0},
	{"tempTex", 1, 134217932, 0, &RenderTexture_t101_0_0_0},
	{"finalTarget", 2, 134217933, 0, &RenderTexture_t101_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::AddBokeh(UnityEngine.RenderTexture,UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern const MethodInfo DepthOfFieldDeprecated_AddBokeh_m286_MethodInfo = 
{
	"AddBokeh"/* name */
	, (methodPointerType)&DepthOfFieldDeprecated_AddBokeh_m286/* method */
	, &DepthOfFieldDeprecated_t102_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t/* invoker_method */
	, DepthOfFieldDeprecated_t102_DepthOfFieldDeprecated_AddBokeh_m286_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 299/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::ReleaseTextures()
extern const MethodInfo DepthOfFieldDeprecated_ReleaseTextures_m287_MethodInfo = 
{
	"ReleaseTextures"/* name */
	, (methodPointerType)&DepthOfFieldDeprecated_ReleaseTextures_m287/* method */
	, &DepthOfFieldDeprecated_t102_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 300/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo DepthOfFieldDeprecated_t102_DepthOfFieldDeprecated_AllocateTextures_m288_ParameterInfos[] = 
{
	{"blurForeground", 0, 134217934, 0, &Boolean_t273_0_0_0},
	{"source", 1, 134217935, 0, &RenderTexture_t101_0_0_0},
	{"divider", 2, 134217936, 0, &Int32_t253_0_0_0},
	{"lowTexDivider", 3, 134217937, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274_Object_t_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::AllocateTextures(System.Boolean,UnityEngine.RenderTexture,System.Int32,System.Int32)
extern const MethodInfo DepthOfFieldDeprecated_AllocateTextures_m288_MethodInfo = 
{
	"AllocateTextures"/* name */
	, (methodPointerType)&DepthOfFieldDeprecated_AllocateTextures_m288/* method */
	, &DepthOfFieldDeprecated_t102_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274_Object_t_Int32_t253_Int32_t253/* invoker_method */
	, DepthOfFieldDeprecated_t102_DepthOfFieldDeprecated_AllocateTextures_m288_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 301/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* DepthOfFieldDeprecated_t102_MethodInfos[] =
{
	&DepthOfFieldDeprecated__ctor_m272_MethodInfo,
	&DepthOfFieldDeprecated__cctor_m273_MethodInfo,
	&DepthOfFieldDeprecated_CreateMaterials_m274_MethodInfo,
	&DepthOfFieldDeprecated_CheckResources_m275_MethodInfo,
	&DepthOfFieldDeprecated_OnDisable_m276_MethodInfo,
	&DepthOfFieldDeprecated_OnEnable_m277_MethodInfo,
	&DepthOfFieldDeprecated_FocalDistance01_m278_MethodInfo,
	&DepthOfFieldDeprecated_GetDividerBasedOnQuality_m279_MethodInfo,
	&DepthOfFieldDeprecated_GetLowResolutionDividerBasedOnQuality_m280_MethodInfo,
	&DepthOfFieldDeprecated_OnRenderImage_m281_MethodInfo,
	&DepthOfFieldDeprecated_Blur_m282_MethodInfo,
	&DepthOfFieldDeprecated_BlurFg_m283_MethodInfo,
	&DepthOfFieldDeprecated_BlurHex_m284_MethodInfo,
	&DepthOfFieldDeprecated_Downsample_m285_MethodInfo,
	&DepthOfFieldDeprecated_AddBokeh_m286_MethodInfo,
	&DepthOfFieldDeprecated_ReleaseTextures_m287_MethodInfo,
	&DepthOfFieldDeprecated_AllocateTextures_m288_MethodInfo,
	NULL
};
static const Il2CppType* DepthOfFieldDeprecated_t102_il2cpp_TypeInfo__nestedTypes[4] =
{
	&Dof34QualitySetting_t97_0_0_0,
	&DofResolution_t98_0_0_0,
	&DofBlurriness_t99_0_0_0,
	&BokehDestination_t100_0_0_0,
};
extern const MethodInfo DepthOfFieldDeprecated_CheckResources_m275_MethodInfo;
static const Il2CppMethodReference DepthOfFieldDeprecated_t102_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	&DepthOfFieldDeprecated_CheckResources_m275_MethodInfo,
};
static bool DepthOfFieldDeprecated_t102_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType DepthOfFieldDeprecated_t102_1_0_0;
struct DepthOfFieldDeprecated_t102;
const Il2CppTypeDefinitionMetadata DepthOfFieldDeprecated_t102_DefinitionMetadata = 
{
	NULL/* declaringType */
	, DepthOfFieldDeprecated_t102_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &PostEffectsBase_t57_0_0_0/* parent */
	, DepthOfFieldDeprecated_t102_VTable/* vtableMethods */
	, DepthOfFieldDeprecated_t102_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 473/* fieldStart */

};
TypeInfo DepthOfFieldDeprecated_t102_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "DepthOfFieldDeprecated"/* name */
	, "UnityStandardAssets.ImageEffects"/* namespaze */
	, DepthOfFieldDeprecated_t102_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &DepthOfFieldDeprecated_t102_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 77/* custom_attributes_cache */
	, &DepthOfFieldDeprecated_t102_0_0_0/* byval_arg */
	, &DepthOfFieldDeprecated_t102_1_0_0/* this_arg */
	, &DepthOfFieldDeprecated_t102_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DepthOfFieldDeprecated_t102)/* instance_size */
	, sizeof (DepthOfFieldDeprecated_t102)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(DepthOfFieldDeprecated_t102_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 17/* method_count */
	, 0/* property_count */
	, 43/* field_count */
	, 0/* event_count */
	, 4/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.ImageEffects.EdgeDetection/EdgeDetectMode
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_37.h"
// Metadata Definition UnityStandardAssets.ImageEffects.EdgeDetection/EdgeDetectMode
extern TypeInfo EdgeDetectMode_t103_il2cpp_TypeInfo;
// UnityStandardAssets.ImageEffects.EdgeDetection/EdgeDetectMode
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_37MethodDeclarations.h"
static const MethodInfo* EdgeDetectMode_t103_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference EdgeDetectMode_t103_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool EdgeDetectMode_t103_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair EdgeDetectMode_t103_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType EdgeDetectMode_t103_0_0_0;
extern const Il2CppType EdgeDetectMode_t103_1_0_0;
extern TypeInfo EdgeDetection_t104_il2cpp_TypeInfo;
extern const Il2CppType EdgeDetection_t104_0_0_0;
const Il2CppTypeDefinitionMetadata EdgeDetectMode_t103_DefinitionMetadata = 
{
	&EdgeDetection_t104_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, EdgeDetectMode_t103_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, EdgeDetectMode_t103_VTable/* vtableMethods */
	, EdgeDetectMode_t103_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 516/* fieldStart */

};
TypeInfo EdgeDetectMode_t103_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "EdgeDetectMode"/* name */
	, ""/* namespaze */
	, EdgeDetectMode_t103_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &EdgeDetectMode_t103_0_0_0/* byval_arg */
	, &EdgeDetectMode_t103_1_0_0/* this_arg */
	, &EdgeDetectMode_t103_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EdgeDetectMode_t103)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (EdgeDetectMode_t103)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityStandardAssets.ImageEffects.EdgeDetection
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_38.h"
// Metadata Definition UnityStandardAssets.ImageEffects.EdgeDetection
// UnityStandardAssets.ImageEffects.EdgeDetection
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_38MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.EdgeDetection::.ctor()
extern const MethodInfo EdgeDetection__ctor_m289_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&EdgeDetection__ctor_m289/* method */
	, &EdgeDetection_t104_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 302/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityStandardAssets.ImageEffects.EdgeDetection::CheckResources()
extern const MethodInfo EdgeDetection_CheckResources_m290_MethodInfo = 
{
	"CheckResources"/* name */
	, (methodPointerType)&EdgeDetection_CheckResources_m290/* method */
	, &EdgeDetection_t104_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 303/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.EdgeDetection::Start()
extern const MethodInfo EdgeDetection_Start_m291_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&EdgeDetection_Start_m291/* method */
	, &EdgeDetection_t104_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 304/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.EdgeDetection::SetCameraFlag()
extern const MethodInfo EdgeDetection_SetCameraFlag_m292_MethodInfo = 
{
	"SetCameraFlag"/* name */
	, (methodPointerType)&EdgeDetection_SetCameraFlag_m292/* method */
	, &EdgeDetection_t104_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 305/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.EdgeDetection::OnEnable()
extern const MethodInfo EdgeDetection_OnEnable_m293_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&EdgeDetection_OnEnable_m293/* method */
	, &EdgeDetection_t104_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 306/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
static const ParameterInfo EdgeDetection_t104_EdgeDetection_OnRenderImage_m294_ParameterInfos[] = 
{
	{"source", 0, 134217938, 0, &RenderTexture_t101_0_0_0},
	{"destination", 1, 134217939, 0, &RenderTexture_t101_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.EdgeDetection::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern const MethodInfo EdgeDetection_OnRenderImage_m294_MethodInfo = 
{
	"OnRenderImage"/* name */
	, (methodPointerType)&EdgeDetection_OnRenderImage_m294/* method */
	, &EdgeDetection_t104_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, EdgeDetection_t104_EdgeDetection_OnRenderImage_m294_ParameterInfos/* parameters */
	, 79/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 307/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* EdgeDetection_t104_MethodInfos[] =
{
	&EdgeDetection__ctor_m289_MethodInfo,
	&EdgeDetection_CheckResources_m290_MethodInfo,
	&EdgeDetection_Start_m291_MethodInfo,
	&EdgeDetection_SetCameraFlag_m292_MethodInfo,
	&EdgeDetection_OnEnable_m293_MethodInfo,
	&EdgeDetection_OnRenderImage_m294_MethodInfo,
	NULL
};
static const Il2CppType* EdgeDetection_t104_il2cpp_TypeInfo__nestedTypes[1] =
{
	&EdgeDetectMode_t103_0_0_0,
};
extern const MethodInfo EdgeDetection_CheckResources_m290_MethodInfo;
static const Il2CppMethodReference EdgeDetection_t104_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	&EdgeDetection_CheckResources_m290_MethodInfo,
};
static bool EdgeDetection_t104_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType EdgeDetection_t104_1_0_0;
struct EdgeDetection_t104;
const Il2CppTypeDefinitionMetadata EdgeDetection_t104_DefinitionMetadata = 
{
	NULL/* declaringType */
	, EdgeDetection_t104_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &PostEffectsBase_t57_0_0_0/* parent */
	, EdgeDetection_t104_VTable/* vtableMethods */
	, EdgeDetection_t104_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 522/* fieldStart */

};
TypeInfo EdgeDetection_t104_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "EdgeDetection"/* name */
	, "UnityStandardAssets.ImageEffects"/* namespaze */
	, EdgeDetection_t104_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &EdgeDetection_t104_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 78/* custom_attributes_cache */
	, &EdgeDetection_t104_0_0_0/* byval_arg */
	, &EdgeDetection_t104_1_0_0/* this_arg */
	, &EdgeDetection_t104_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EdgeDetection_t104)/* instance_size */
	, sizeof (EdgeDetection_t104)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.ImageEffects.Fisheye
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_39.h"
// Metadata Definition UnityStandardAssets.ImageEffects.Fisheye
extern TypeInfo Fisheye_t105_il2cpp_TypeInfo;
// UnityStandardAssets.ImageEffects.Fisheye
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_39MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.Fisheye::.ctor()
extern const MethodInfo Fisheye__ctor_m295_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Fisheye__ctor_m295/* method */
	, &Fisheye_t105_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 308/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityStandardAssets.ImageEffects.Fisheye::CheckResources()
extern const MethodInfo Fisheye_CheckResources_m296_MethodInfo = 
{
	"CheckResources"/* name */
	, (methodPointerType)&Fisheye_CheckResources_m296/* method */
	, &Fisheye_t105_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 309/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
static const ParameterInfo Fisheye_t105_Fisheye_OnRenderImage_m297_ParameterInfos[] = 
{
	{"source", 0, 134217940, 0, &RenderTexture_t101_0_0_0},
	{"destination", 1, 134217941, 0, &RenderTexture_t101_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.Fisheye::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern const MethodInfo Fisheye_OnRenderImage_m297_MethodInfo = 
{
	"OnRenderImage"/* name */
	, (methodPointerType)&Fisheye_OnRenderImage_m297/* method */
	, &Fisheye_t105_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, Fisheye_t105_Fisheye_OnRenderImage_m297_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 310/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Fisheye_t105_MethodInfos[] =
{
	&Fisheye__ctor_m295_MethodInfo,
	&Fisheye_CheckResources_m296_MethodInfo,
	&Fisheye_OnRenderImage_m297_MethodInfo,
	NULL
};
extern const MethodInfo Fisheye_CheckResources_m296_MethodInfo;
static const Il2CppMethodReference Fisheye_t105_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	&Fisheye_CheckResources_m296_MethodInfo,
};
static bool Fisheye_t105_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType Fisheye_t105_0_0_0;
extern const Il2CppType Fisheye_t105_1_0_0;
struct Fisheye_t105;
const Il2CppTypeDefinitionMetadata Fisheye_t105_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &PostEffectsBase_t57_0_0_0/* parent */
	, Fisheye_t105_VTable/* vtableMethods */
	, Fisheye_t105_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 533/* fieldStart */

};
TypeInfo Fisheye_t105_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "Fisheye"/* name */
	, "UnityStandardAssets.ImageEffects"/* namespaze */
	, Fisheye_t105_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Fisheye_t105_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 80/* custom_attributes_cache */
	, &Fisheye_t105_0_0_0/* byval_arg */
	, &Fisheye_t105_1_0_0/* this_arg */
	, &Fisheye_t105_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Fisheye_t105)/* instance_size */
	, sizeof (Fisheye_t105)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.ImageEffects.GlobalFog
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_40.h"
// Metadata Definition UnityStandardAssets.ImageEffects.GlobalFog
extern TypeInfo GlobalFog_t106_il2cpp_TypeInfo;
// UnityStandardAssets.ImageEffects.GlobalFog
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_40MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.GlobalFog::.ctor()
extern const MethodInfo GlobalFog__ctor_m298_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&GlobalFog__ctor_m298/* method */
	, &GlobalFog_t106_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 311/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityStandardAssets.ImageEffects.GlobalFog::CheckResources()
extern const MethodInfo GlobalFog_CheckResources_m299_MethodInfo = 
{
	"CheckResources"/* name */
	, (methodPointerType)&GlobalFog_CheckResources_m299/* method */
	, &GlobalFog_t106_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 312/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
static const ParameterInfo GlobalFog_t106_GlobalFog_OnRenderImage_m300_ParameterInfos[] = 
{
	{"source", 0, 134217942, 0, &RenderTexture_t101_0_0_0},
	{"destination", 1, 134217943, 0, &RenderTexture_t101_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.GlobalFog::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern const MethodInfo GlobalFog_OnRenderImage_m300_MethodInfo = 
{
	"OnRenderImage"/* name */
	, (methodPointerType)&GlobalFog_OnRenderImage_m300/* method */
	, &GlobalFog_t106_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, GlobalFog_t106_GlobalFog_OnRenderImage_m300_ParameterInfos/* parameters */
	, 88/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 313/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType Material_t55_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo GlobalFog_t106_GlobalFog_CustomGraphicsBlit_m301_ParameterInfos[] = 
{
	{"source", 0, 134217944, 0, &RenderTexture_t101_0_0_0},
	{"dest", 1, 134217945, 0, &RenderTexture_t101_0_0_0},
	{"fxMaterial", 2, 134217946, 0, &Material_t55_0_0_0},
	{"passNr", 3, 134217947, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.GlobalFog::CustomGraphicsBlit(UnityEngine.RenderTexture,UnityEngine.RenderTexture,UnityEngine.Material,System.Int32)
extern const MethodInfo GlobalFog_CustomGraphicsBlit_m301_MethodInfo = 
{
	"CustomGraphicsBlit"/* name */
	, (methodPointerType)&GlobalFog_CustomGraphicsBlit_m301/* method */
	, &GlobalFog_t106_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t_Int32_t253/* invoker_method */
	, GlobalFog_t106_GlobalFog_CustomGraphicsBlit_m301_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 314/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* GlobalFog_t106_MethodInfos[] =
{
	&GlobalFog__ctor_m298_MethodInfo,
	&GlobalFog_CheckResources_m299_MethodInfo,
	&GlobalFog_OnRenderImage_m300_MethodInfo,
	&GlobalFog_CustomGraphicsBlit_m301_MethodInfo,
	NULL
};
extern const MethodInfo GlobalFog_CheckResources_m299_MethodInfo;
static const Il2CppMethodReference GlobalFog_t106_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	&GlobalFog_CheckResources_m299_MethodInfo,
};
static bool GlobalFog_t106_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType GlobalFog_t106_0_0_0;
extern const Il2CppType GlobalFog_t106_1_0_0;
struct GlobalFog_t106;
const Il2CppTypeDefinitionMetadata GlobalFog_t106_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &PostEffectsBase_t57_0_0_0/* parent */
	, GlobalFog_t106_VTable/* vtableMethods */
	, GlobalFog_t106_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 537/* fieldStart */

};
TypeInfo GlobalFog_t106_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "GlobalFog"/* name */
	, "UnityStandardAssets.ImageEffects"/* namespaze */
	, GlobalFog_t106_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &GlobalFog_t106_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 81/* custom_attributes_cache */
	, &GlobalFog_t106_0_0_0/* byval_arg */
	, &GlobalFog_t106_1_0_0/* this_arg */
	, &GlobalFog_t106_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GlobalFog_t106)/* instance_size */
	, sizeof (GlobalFog_t106)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.ImageEffects.Grayscale
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_41.h"
// Metadata Definition UnityStandardAssets.ImageEffects.Grayscale
extern TypeInfo Grayscale_t107_il2cpp_TypeInfo;
// UnityStandardAssets.ImageEffects.Grayscale
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_41MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.Grayscale::.ctor()
extern const MethodInfo Grayscale__ctor_m302_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Grayscale__ctor_m302/* method */
	, &Grayscale_t107_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 315/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
static const ParameterInfo Grayscale_t107_Grayscale_OnRenderImage_m303_ParameterInfos[] = 
{
	{"source", 0, 134217948, 0, &RenderTexture_t101_0_0_0},
	{"destination", 1, 134217949, 0, &RenderTexture_t101_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.Grayscale::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern const MethodInfo Grayscale_OnRenderImage_m303_MethodInfo = 
{
	"OnRenderImage"/* name */
	, (methodPointerType)&Grayscale_OnRenderImage_m303/* method */
	, &Grayscale_t107_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, Grayscale_t107_Grayscale_OnRenderImage_m303_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 316/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Grayscale_t107_MethodInfos[] =
{
	&Grayscale__ctor_m302_MethodInfo,
	&Grayscale_OnRenderImage_m303_MethodInfo,
	NULL
};
static const Il2CppMethodReference Grayscale_t107_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	&ImageEffectBase_Start_m305_MethodInfo,
	&ImageEffectBase_OnDisable_m307_MethodInfo,
};
static bool Grayscale_t107_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType Grayscale_t107_0_0_0;
extern const Il2CppType Grayscale_t107_1_0_0;
struct Grayscale_t107;
const Il2CppTypeDefinitionMetadata Grayscale_t107_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ImageEffectBase_t88_0_0_0/* parent */
	, Grayscale_t107_VTable/* vtableMethods */
	, Grayscale_t107_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 545/* fieldStart */

};
TypeInfo Grayscale_t107_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "Grayscale"/* name */
	, "UnityStandardAssets.ImageEffects"/* namespaze */
	, Grayscale_t107_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Grayscale_t107_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 89/* custom_attributes_cache */
	, &Grayscale_t107_0_0_0/* byval_arg */
	, &Grayscale_t107_1_0_0/* this_arg */
	, &Grayscale_t107_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Grayscale_t107)/* instance_size */
	, sizeof (Grayscale_t107)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.ImageEffects.ImageEffectBase
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_25.h"
// Metadata Definition UnityStandardAssets.ImageEffects.ImageEffectBase
extern TypeInfo ImageEffectBase_t88_il2cpp_TypeInfo;
// UnityStandardAssets.ImageEffects.ImageEffectBase
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_25MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.ImageEffectBase::.ctor()
extern const MethodInfo ImageEffectBase__ctor_m304_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ImageEffectBase__ctor_m304/* method */
	, &ImageEffectBase_t88_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 317/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.ImageEffectBase::Start()
extern const MethodInfo ImageEffectBase_Start_m305_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&ImageEffectBase_Start_m305/* method */
	, &ImageEffectBase_t88_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 318/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Material UnityStandardAssets.ImageEffects.ImageEffectBase::get_material()
extern const MethodInfo ImageEffectBase_get_material_m306_MethodInfo = 
{
	"get_material"/* name */
	, (methodPointerType)&ImageEffectBase_get_material_m306/* method */
	, &ImageEffectBase_t88_il2cpp_TypeInfo/* declaring_type */
	, &Material_t55_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2180/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 319/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.ImageEffectBase::OnDisable()
extern const MethodInfo ImageEffectBase_OnDisable_m307_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&ImageEffectBase_OnDisable_m307/* method */
	, &ImageEffectBase_t88_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 320/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ImageEffectBase_t88_MethodInfos[] =
{
	&ImageEffectBase__ctor_m304_MethodInfo,
	&ImageEffectBase_Start_m305_MethodInfo,
	&ImageEffectBase_get_material_m306_MethodInfo,
	&ImageEffectBase_OnDisable_m307_MethodInfo,
	NULL
};
extern const MethodInfo ImageEffectBase_get_material_m306_MethodInfo;
static const PropertyInfo ImageEffectBase_t88____material_PropertyInfo = 
{
	&ImageEffectBase_t88_il2cpp_TypeInfo/* parent */
	, "material"/* name */
	, &ImageEffectBase_get_material_m306_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ImageEffectBase_t88_PropertyInfos[] =
{
	&ImageEffectBase_t88____material_PropertyInfo,
	NULL
};
static const Il2CppMethodReference ImageEffectBase_t88_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	&ImageEffectBase_Start_m305_MethodInfo,
	&ImageEffectBase_OnDisable_m307_MethodInfo,
};
static bool ImageEffectBase_t88_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType ImageEffectBase_t88_1_0_0;
struct ImageEffectBase_t88;
const Il2CppTypeDefinitionMetadata ImageEffectBase_t88_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, ImageEffectBase_t88_VTable/* vtableMethods */
	, ImageEffectBase_t88_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 547/* fieldStart */

};
TypeInfo ImageEffectBase_t88_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "ImageEffectBase"/* name */
	, "UnityStandardAssets.ImageEffects"/* namespaze */
	, ImageEffectBase_t88_MethodInfos/* methods */
	, ImageEffectBase_t88_PropertyInfos/* properties */
	, NULL/* events */
	, &ImageEffectBase_t88_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 90/* custom_attributes_cache */
	, &ImageEffectBase_t88_0_0_0/* byval_arg */
	, &ImageEffectBase_t88_1_0_0/* this_arg */
	, &ImageEffectBase_t88_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ImageEffectBase_t88)/* instance_size */
	, sizeof (ImageEffectBase_t88)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.ImageEffects.ImageEffects
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_42.h"
// Metadata Definition UnityStandardAssets.ImageEffects.ImageEffects
extern TypeInfo ImageEffects_t108_il2cpp_TypeInfo;
// UnityStandardAssets.ImageEffects.ImageEffects
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_42MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.ImageEffects::.ctor()
extern const MethodInfo ImageEffects__ctor_m308_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ImageEffects__ctor_m308/* method */
	, &ImageEffects_t108_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 321/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Material_t55_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType Single_t254_0_0_0;
extern const Il2CppType Vector2_t6_0_0_0;
extern const Il2CppType Vector2_t6_0_0_0;
extern const Il2CppType Vector2_t6_0_0_0;
static const ParameterInfo ImageEffects_t108_ImageEffects_RenderDistortion_m309_ParameterInfos[] = 
{
	{"material", 0, 134217950, 0, &Material_t55_0_0_0},
	{"source", 1, 134217951, 0, &RenderTexture_t101_0_0_0},
	{"destination", 2, 134217952, 0, &RenderTexture_t101_0_0_0},
	{"angle", 3, 134217953, 0, &Single_t254_0_0_0},
	{"center", 4, 134217954, 0, &Vector2_t6_0_0_0},
	{"radius", 5, 134217955, 0, &Vector2_t6_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t_Single_t254_Vector2_t6_Vector2_t6 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.ImageEffects::RenderDistortion(UnityEngine.Material,UnityEngine.RenderTexture,UnityEngine.RenderTexture,System.Single,UnityEngine.Vector2,UnityEngine.Vector2)
extern const MethodInfo ImageEffects_RenderDistortion_m309_MethodInfo = 
{
	"RenderDistortion"/* name */
	, (methodPointerType)&ImageEffects_RenderDistortion_m309/* method */
	, &ImageEffects_t108_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t_Single_t254_Vector2_t6_Vector2_t6/* invoker_method */
	, ImageEffects_t108_ImageEffects_RenderDistortion_m309_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 322/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
static const ParameterInfo ImageEffects_t108_ImageEffects_Blit_m310_ParameterInfos[] = 
{
	{"source", 0, 134217956, 0, &RenderTexture_t101_0_0_0},
	{"dest", 1, 134217957, 0, &RenderTexture_t101_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.ImageEffects::Blit(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern const MethodInfo ImageEffects_Blit_m310_MethodInfo = 
{
	"Blit"/* name */
	, (methodPointerType)&ImageEffects_Blit_m310/* method */
	, &ImageEffects_t108_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, ImageEffects_t108_ImageEffects_Blit_m310_ParameterInfos/* parameters */
	, 92/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 323/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Material_t55_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
static const ParameterInfo ImageEffects_t108_ImageEffects_BlitWithMaterial_m311_ParameterInfos[] = 
{
	{"material", 0, 134217958, 0, &Material_t55_0_0_0},
	{"source", 1, 134217959, 0, &RenderTexture_t101_0_0_0},
	{"dest", 2, 134217960, 0, &RenderTexture_t101_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.ImageEffects::BlitWithMaterial(UnityEngine.Material,UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern const MethodInfo ImageEffects_BlitWithMaterial_m311_MethodInfo = 
{
	"BlitWithMaterial"/* name */
	, (methodPointerType)&ImageEffects_BlitWithMaterial_m311/* method */
	, &ImageEffects_t108_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t/* invoker_method */
	, ImageEffects_t108_ImageEffects_BlitWithMaterial_m311_ParameterInfos/* parameters */
	, 93/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 324/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ImageEffects_t108_MethodInfos[] =
{
	&ImageEffects__ctor_m308_MethodInfo,
	&ImageEffects_RenderDistortion_m309_MethodInfo,
	&ImageEffects_Blit_m310_MethodInfo,
	&ImageEffects_BlitWithMaterial_m311_MethodInfo,
	NULL
};
static const Il2CppMethodReference ImageEffects_t108_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool ImageEffects_t108_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType ImageEffects_t108_0_0_0;
extern const Il2CppType ImageEffects_t108_1_0_0;
struct ImageEffects_t108;
const Il2CppTypeDefinitionMetadata ImageEffects_t108_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ImageEffects_t108_VTable/* vtableMethods */
	, ImageEffects_t108_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ImageEffects_t108_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "ImageEffects"/* name */
	, "UnityStandardAssets.ImageEffects"/* namespaze */
	, ImageEffects_t108_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ImageEffects_t108_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 91/* custom_attributes_cache */
	, &ImageEffects_t108_0_0_0/* byval_arg */
	, &ImageEffects_t108_1_0_0/* this_arg */
	, &ImageEffects_t108_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ImageEffects_t108)/* instance_size */
	, sizeof (ImageEffects_t108)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.ImageEffects.MotionBlur
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_43.h"
// Metadata Definition UnityStandardAssets.ImageEffects.MotionBlur
extern TypeInfo MotionBlur_t109_il2cpp_TypeInfo;
// UnityStandardAssets.ImageEffects.MotionBlur
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_43MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.MotionBlur::.ctor()
extern const MethodInfo MotionBlur__ctor_m312_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MotionBlur__ctor_m312/* method */
	, &MotionBlur_t109_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 325/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.MotionBlur::Start()
extern const MethodInfo MotionBlur_Start_m313_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&MotionBlur_Start_m313/* method */
	, &MotionBlur_t109_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 326/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.MotionBlur::OnDisable()
extern const MethodInfo MotionBlur_OnDisable_m314_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&MotionBlur_OnDisable_m314/* method */
	, &MotionBlur_t109_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 327/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
static const ParameterInfo MotionBlur_t109_MotionBlur_OnRenderImage_m315_ParameterInfos[] = 
{
	{"source", 0, 134217961, 0, &RenderTexture_t101_0_0_0},
	{"destination", 1, 134217962, 0, &RenderTexture_t101_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.MotionBlur::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern const MethodInfo MotionBlur_OnRenderImage_m315_MethodInfo = 
{
	"OnRenderImage"/* name */
	, (methodPointerType)&MotionBlur_OnRenderImage_m315/* method */
	, &MotionBlur_t109_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, MotionBlur_t109_MotionBlur_OnRenderImage_m315_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 328/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MotionBlur_t109_MethodInfos[] =
{
	&MotionBlur__ctor_m312_MethodInfo,
	&MotionBlur_Start_m313_MethodInfo,
	&MotionBlur_OnDisable_m314_MethodInfo,
	&MotionBlur_OnRenderImage_m315_MethodInfo,
	NULL
};
extern const MethodInfo MotionBlur_Start_m313_MethodInfo;
extern const MethodInfo MotionBlur_OnDisable_m314_MethodInfo;
static const Il2CppMethodReference MotionBlur_t109_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	&MotionBlur_Start_m313_MethodInfo,
	&MotionBlur_OnDisable_m314_MethodInfo,
};
static bool MotionBlur_t109_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType MotionBlur_t109_0_0_0;
extern const Il2CppType MotionBlur_t109_1_0_0;
struct MotionBlur_t109;
const Il2CppTypeDefinitionMetadata MotionBlur_t109_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ImageEffectBase_t88_0_0_0/* parent */
	, MotionBlur_t109_VTable/* vtableMethods */
	, MotionBlur_t109_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 549/* fieldStart */

};
TypeInfo MotionBlur_t109_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "MotionBlur"/* name */
	, "UnityStandardAssets.ImageEffects"/* namespaze */
	, MotionBlur_t109_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &MotionBlur_t109_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 94/* custom_attributes_cache */
	, &MotionBlur_t109_0_0_0/* byval_arg */
	, &MotionBlur_t109_1_0_0/* this_arg */
	, &MotionBlur_t109_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MotionBlur_t109)/* instance_size */
	, sizeof (MotionBlur_t109)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.ImageEffects.NoiseAndGrain
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_44.h"
// Metadata Definition UnityStandardAssets.ImageEffects.NoiseAndGrain
extern TypeInfo NoiseAndGrain_t110_il2cpp_TypeInfo;
// UnityStandardAssets.ImageEffects.NoiseAndGrain
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_44MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.NoiseAndGrain::.ctor()
extern const MethodInfo NoiseAndGrain__ctor_m316_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&NoiseAndGrain__ctor_m316/* method */
	, &NoiseAndGrain_t110_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 329/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.NoiseAndGrain::.cctor()
extern const MethodInfo NoiseAndGrain__cctor_m317_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&NoiseAndGrain__cctor_m317/* method */
	, &NoiseAndGrain_t110_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 330/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityStandardAssets.ImageEffects.NoiseAndGrain::CheckResources()
extern const MethodInfo NoiseAndGrain_CheckResources_m318_MethodInfo = 
{
	"CheckResources"/* name */
	, (methodPointerType)&NoiseAndGrain_CheckResources_m318/* method */
	, &NoiseAndGrain_t110_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 331/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
static const ParameterInfo NoiseAndGrain_t110_NoiseAndGrain_OnRenderImage_m319_ParameterInfos[] = 
{
	{"source", 0, 134217963, 0, &RenderTexture_t101_0_0_0},
	{"destination", 1, 134217964, 0, &RenderTexture_t101_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.NoiseAndGrain::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern const MethodInfo NoiseAndGrain_OnRenderImage_m319_MethodInfo = 
{
	"OnRenderImage"/* name */
	, (methodPointerType)&NoiseAndGrain_OnRenderImage_m319/* method */
	, &NoiseAndGrain_t110_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, NoiseAndGrain_t110_NoiseAndGrain_OnRenderImage_m319_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 332/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType Material_t55_0_0_0;
extern const Il2CppType Texture2D_t63_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo NoiseAndGrain_t110_NoiseAndGrain_DrawNoiseQuadGrid_m320_ParameterInfos[] = 
{
	{"source", 0, 134217965, 0, &RenderTexture_t101_0_0_0},
	{"dest", 1, 134217966, 0, &RenderTexture_t101_0_0_0},
	{"fxMaterial", 2, 134217967, 0, &Material_t55_0_0_0},
	{"noise", 3, 134217968, 0, &Texture2D_t63_0_0_0},
	{"passNr", 4, 134217969, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.NoiseAndGrain::DrawNoiseQuadGrid(UnityEngine.RenderTexture,UnityEngine.RenderTexture,UnityEngine.Material,UnityEngine.Texture2D,System.Int32)
extern const MethodInfo NoiseAndGrain_DrawNoiseQuadGrid_m320_MethodInfo = 
{
	"DrawNoiseQuadGrid"/* name */
	, (methodPointerType)&NoiseAndGrain_DrawNoiseQuadGrid_m320/* method */
	, &NoiseAndGrain_t110_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t_Object_t_Int32_t253/* invoker_method */
	, NoiseAndGrain_t110_NoiseAndGrain_DrawNoiseQuadGrid_m320_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 333/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* NoiseAndGrain_t110_MethodInfos[] =
{
	&NoiseAndGrain__ctor_m316_MethodInfo,
	&NoiseAndGrain__cctor_m317_MethodInfo,
	&NoiseAndGrain_CheckResources_m318_MethodInfo,
	&NoiseAndGrain_OnRenderImage_m319_MethodInfo,
	&NoiseAndGrain_DrawNoiseQuadGrid_m320_MethodInfo,
	NULL
};
extern const MethodInfo NoiseAndGrain_CheckResources_m318_MethodInfo;
static const Il2CppMethodReference NoiseAndGrain_t110_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	&NoiseAndGrain_CheckResources_m318_MethodInfo,
};
static bool NoiseAndGrain_t110_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType NoiseAndGrain_t110_0_0_0;
extern const Il2CppType NoiseAndGrain_t110_1_0_0;
struct NoiseAndGrain_t110;
const Il2CppTypeDefinitionMetadata NoiseAndGrain_t110_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &PostEffectsBase_t57_0_0_0/* parent */
	, NoiseAndGrain_t110_VTable/* vtableMethods */
	, NoiseAndGrain_t110_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 552/* fieldStart */

};
TypeInfo NoiseAndGrain_t110_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "NoiseAndGrain"/* name */
	, "UnityStandardAssets.ImageEffects"/* namespaze */
	, NoiseAndGrain_t110_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &NoiseAndGrain_t110_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 95/* custom_attributes_cache */
	, &NoiseAndGrain_t110_0_0_0/* byval_arg */
	, &NoiseAndGrain_t110_1_0_0/* this_arg */
	, &NoiseAndGrain_t110_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NoiseAndGrain_t110)/* instance_size */
	, sizeof (NoiseAndGrain_t110)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(NoiseAndGrain_t110_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 18/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.ImageEffects.NoiseAndScratches
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_45.h"
// Metadata Definition UnityStandardAssets.ImageEffects.NoiseAndScratches
extern TypeInfo NoiseAndScratches_t111_il2cpp_TypeInfo;
// UnityStandardAssets.ImageEffects.NoiseAndScratches
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_45MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.NoiseAndScratches::.ctor()
extern const MethodInfo NoiseAndScratches__ctor_m321_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&NoiseAndScratches__ctor_m321/* method */
	, &NoiseAndScratches_t111_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 334/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.NoiseAndScratches::Start()
extern const MethodInfo NoiseAndScratches_Start_m322_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&NoiseAndScratches_Start_m322/* method */
	, &NoiseAndScratches_t111_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 335/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Material UnityStandardAssets.ImageEffects.NoiseAndScratches::get_material()
extern const MethodInfo NoiseAndScratches_get_material_m323_MethodInfo = 
{
	"get_material"/* name */
	, (methodPointerType)&NoiseAndScratches_get_material_m323/* method */
	, &NoiseAndScratches_t111_il2cpp_TypeInfo/* declaring_type */
	, &Material_t55_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2180/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 336/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.NoiseAndScratches::OnDisable()
extern const MethodInfo NoiseAndScratches_OnDisable_m324_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&NoiseAndScratches_OnDisable_m324/* method */
	, &NoiseAndScratches_t111_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 337/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.NoiseAndScratches::SanitizeParameters()
extern const MethodInfo NoiseAndScratches_SanitizeParameters_m325_MethodInfo = 
{
	"SanitizeParameters"/* name */
	, (methodPointerType)&NoiseAndScratches_SanitizeParameters_m325/* method */
	, &NoiseAndScratches_t111_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 338/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
static const ParameterInfo NoiseAndScratches_t111_NoiseAndScratches_OnRenderImage_m326_ParameterInfos[] = 
{
	{"source", 0, 134217970, 0, &RenderTexture_t101_0_0_0},
	{"destination", 1, 134217971, 0, &RenderTexture_t101_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.NoiseAndScratches::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern const MethodInfo NoiseAndScratches_OnRenderImage_m326_MethodInfo = 
{
	"OnRenderImage"/* name */
	, (methodPointerType)&NoiseAndScratches_OnRenderImage_m326/* method */
	, &NoiseAndScratches_t111_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, NoiseAndScratches_t111_NoiseAndScratches_OnRenderImage_m326_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 339/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* NoiseAndScratches_t111_MethodInfos[] =
{
	&NoiseAndScratches__ctor_m321_MethodInfo,
	&NoiseAndScratches_Start_m322_MethodInfo,
	&NoiseAndScratches_get_material_m323_MethodInfo,
	&NoiseAndScratches_OnDisable_m324_MethodInfo,
	&NoiseAndScratches_SanitizeParameters_m325_MethodInfo,
	&NoiseAndScratches_OnRenderImage_m326_MethodInfo,
	NULL
};
extern const MethodInfo NoiseAndScratches_get_material_m323_MethodInfo;
static const PropertyInfo NoiseAndScratches_t111____material_PropertyInfo = 
{
	&NoiseAndScratches_t111_il2cpp_TypeInfo/* parent */
	, "material"/* name */
	, &NoiseAndScratches_get_material_m323_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* NoiseAndScratches_t111_PropertyInfos[] =
{
	&NoiseAndScratches_t111____material_PropertyInfo,
	NULL
};
static const Il2CppMethodReference NoiseAndScratches_t111_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool NoiseAndScratches_t111_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType NoiseAndScratches_t111_0_0_0;
extern const Il2CppType NoiseAndScratches_t111_1_0_0;
struct NoiseAndScratches_t111;
const Il2CppTypeDefinitionMetadata NoiseAndScratches_t111_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, NoiseAndScratches_t111_VTable/* vtableMethods */
	, NoiseAndScratches_t111_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 570/* fieldStart */

};
TypeInfo NoiseAndScratches_t111_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "NoiseAndScratches"/* name */
	, "UnityStandardAssets.ImageEffects"/* namespaze */
	, NoiseAndScratches_t111_MethodInfos/* methods */
	, NoiseAndScratches_t111_PropertyInfos/* properties */
	, NULL/* events */
	, &NoiseAndScratches_t111_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 96/* custom_attributes_cache */
	, &NoiseAndScratches_t111_0_0_0/* byval_arg */
	, &NoiseAndScratches_t111_1_0_0/* this_arg */
	, &NoiseAndScratches_t111_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NoiseAndScratches_t111)/* instance_size */
	, sizeof (NoiseAndScratches_t111)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 18/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.ImageEffects.PostEffectsBase
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_1.h"
// Metadata Definition UnityStandardAssets.ImageEffects.PostEffectsBase
extern TypeInfo PostEffectsBase_t57_il2cpp_TypeInfo;
// UnityStandardAssets.ImageEffects.PostEffectsBase
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_1MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.PostEffectsBase::.ctor()
extern const MethodInfo PostEffectsBase__ctor_m327_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PostEffectsBase__ctor_m327/* method */
	, &PostEffectsBase_t57_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 340/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Shader_t54_0_0_0;
extern const Il2CppType Shader_t54_0_0_0;
extern const Il2CppType Material_t55_0_0_0;
static const ParameterInfo PostEffectsBase_t57_PostEffectsBase_CheckShaderAndCreateMaterial_m328_ParameterInfos[] = 
{
	{"s", 0, 134217972, 0, &Shader_t54_0_0_0},
	{"m2Create", 1, 134217973, 0, &Material_t55_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Material UnityStandardAssets.ImageEffects.PostEffectsBase::CheckShaderAndCreateMaterial(UnityEngine.Shader,UnityEngine.Material)
extern const MethodInfo PostEffectsBase_CheckShaderAndCreateMaterial_m328_MethodInfo = 
{
	"CheckShaderAndCreateMaterial"/* name */
	, (methodPointerType)&PostEffectsBase_CheckShaderAndCreateMaterial_m328/* method */
	, &PostEffectsBase_t57_il2cpp_TypeInfo/* declaring_type */
	, &Material_t55_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, PostEffectsBase_t57_PostEffectsBase_CheckShaderAndCreateMaterial_m328_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 341/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Shader_t54_0_0_0;
extern const Il2CppType Material_t55_0_0_0;
static const ParameterInfo PostEffectsBase_t57_PostEffectsBase_CreateMaterial_m329_ParameterInfos[] = 
{
	{"s", 0, 134217974, 0, &Shader_t54_0_0_0},
	{"m2Create", 1, 134217975, 0, &Material_t55_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Material UnityStandardAssets.ImageEffects.PostEffectsBase::CreateMaterial(UnityEngine.Shader,UnityEngine.Material)
extern const MethodInfo PostEffectsBase_CreateMaterial_m329_MethodInfo = 
{
	"CreateMaterial"/* name */
	, (methodPointerType)&PostEffectsBase_CreateMaterial_m329/* method */
	, &PostEffectsBase_t57_il2cpp_TypeInfo/* declaring_type */
	, &Material_t55_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, PostEffectsBase_t57_PostEffectsBase_CreateMaterial_m329_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 342/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.PostEffectsBase::OnEnable()
extern const MethodInfo PostEffectsBase_OnEnable_m330_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&PostEffectsBase_OnEnable_m330/* method */
	, &PostEffectsBase_t57_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 343/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::CheckSupport()
extern const MethodInfo PostEffectsBase_CheckSupport_m331_MethodInfo = 
{
	"CheckSupport"/* name */
	, (methodPointerType)&PostEffectsBase_CheckSupport_m331/* method */
	, &PostEffectsBase_t57_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 344/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::CheckResources()
extern const MethodInfo PostEffectsBase_CheckResources_m332_MethodInfo = 
{
	"CheckResources"/* name */
	, (methodPointerType)&PostEffectsBase_CheckResources_m332/* method */
	, &PostEffectsBase_t57_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 345/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.PostEffectsBase::Start()
extern const MethodInfo PostEffectsBase_Start_m333_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&PostEffectsBase_Start_m333/* method */
	, &PostEffectsBase_t57_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 346/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo PostEffectsBase_t57_PostEffectsBase_CheckSupport_m334_ParameterInfos[] = 
{
	{"needDepth", 0, 134217976, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::CheckSupport(System.Boolean)
extern const MethodInfo PostEffectsBase_CheckSupport_m334_MethodInfo = 
{
	"CheckSupport"/* name */
	, (methodPointerType)&PostEffectsBase_CheckSupport_m334/* method */
	, &PostEffectsBase_t57_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_SByte_t274/* invoker_method */
	, PostEffectsBase_t57_PostEffectsBase_CheckSupport_m334_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 347/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo PostEffectsBase_t57_PostEffectsBase_CheckSupport_m335_ParameterInfos[] = 
{
	{"needDepth", 0, 134217977, 0, &Boolean_t273_0_0_0},
	{"needHdr", 1, 134217978, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_SByte_t274_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::CheckSupport(System.Boolean,System.Boolean)
extern const MethodInfo PostEffectsBase_CheckSupport_m335_MethodInfo = 
{
	"CheckSupport"/* name */
	, (methodPointerType)&PostEffectsBase_CheckSupport_m335/* method */
	, &PostEffectsBase_t57_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_SByte_t274_SByte_t274/* invoker_method */
	, PostEffectsBase_t57_PostEffectsBase_CheckSupport_m335_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 348/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::Dx11Support()
extern const MethodInfo PostEffectsBase_Dx11Support_m336_MethodInfo = 
{
	"Dx11Support"/* name */
	, (methodPointerType)&PostEffectsBase_Dx11Support_m336/* method */
	, &PostEffectsBase_t57_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 349/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.PostEffectsBase::ReportAutoDisable()
extern const MethodInfo PostEffectsBase_ReportAutoDisable_m337_MethodInfo = 
{
	"ReportAutoDisable"/* name */
	, (methodPointerType)&PostEffectsBase_ReportAutoDisable_m337/* method */
	, &PostEffectsBase_t57_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 350/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Shader_t54_0_0_0;
static const ParameterInfo PostEffectsBase_t57_PostEffectsBase_CheckShader_m338_ParameterInfos[] = 
{
	{"s", 0, 134217979, 0, &Shader_t54_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::CheckShader(UnityEngine.Shader)
extern const MethodInfo PostEffectsBase_CheckShader_m338_MethodInfo = 
{
	"CheckShader"/* name */
	, (methodPointerType)&PostEffectsBase_CheckShader_m338/* method */
	, &PostEffectsBase_t57_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, PostEffectsBase_t57_PostEffectsBase_CheckShader_m338_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 351/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.PostEffectsBase::NotSupported()
extern const MethodInfo PostEffectsBase_NotSupported_m339_MethodInfo = 
{
	"NotSupported"/* name */
	, (methodPointerType)&PostEffectsBase_NotSupported_m339/* method */
	, &PostEffectsBase_t57_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 352/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType Material_t55_0_0_0;
static const ParameterInfo PostEffectsBase_t57_PostEffectsBase_DrawBorder_m340_ParameterInfos[] = 
{
	{"dest", 0, 134217980, 0, &RenderTexture_t101_0_0_0},
	{"material", 1, 134217981, 0, &Material_t55_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.PostEffectsBase::DrawBorder(UnityEngine.RenderTexture,UnityEngine.Material)
extern const MethodInfo PostEffectsBase_DrawBorder_m340_MethodInfo = 
{
	"DrawBorder"/* name */
	, (methodPointerType)&PostEffectsBase_DrawBorder_m340/* method */
	, &PostEffectsBase_t57_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, PostEffectsBase_t57_PostEffectsBase_DrawBorder_m340_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 353/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* PostEffectsBase_t57_MethodInfos[] =
{
	&PostEffectsBase__ctor_m327_MethodInfo,
	&PostEffectsBase_CheckShaderAndCreateMaterial_m328_MethodInfo,
	&PostEffectsBase_CreateMaterial_m329_MethodInfo,
	&PostEffectsBase_OnEnable_m330_MethodInfo,
	&PostEffectsBase_CheckSupport_m331_MethodInfo,
	&PostEffectsBase_CheckResources_m332_MethodInfo,
	&PostEffectsBase_Start_m333_MethodInfo,
	&PostEffectsBase_CheckSupport_m334_MethodInfo,
	&PostEffectsBase_CheckSupport_m335_MethodInfo,
	&PostEffectsBase_Dx11Support_m336_MethodInfo,
	&PostEffectsBase_ReportAutoDisable_m337_MethodInfo,
	&PostEffectsBase_CheckShader_m338_MethodInfo,
	&PostEffectsBase_NotSupported_m339_MethodInfo,
	&PostEffectsBase_DrawBorder_m340_MethodInfo,
	NULL
};
extern const MethodInfo PostEffectsBase_CheckResources_m332_MethodInfo;
static const Il2CppMethodReference PostEffectsBase_t57_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	&PostEffectsBase_CheckResources_m332_MethodInfo,
};
static bool PostEffectsBase_t57_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType PostEffectsBase_t57_1_0_0;
struct PostEffectsBase_t57;
const Il2CppTypeDefinitionMetadata PostEffectsBase_t57_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, PostEffectsBase_t57_VTable/* vtableMethods */
	, PostEffectsBase_t57_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 588/* fieldStart */

};
TypeInfo PostEffectsBase_t57_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "PostEffectsBase"/* name */
	, "UnityStandardAssets.ImageEffects"/* namespaze */
	, PostEffectsBase_t57_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &PostEffectsBase_t57_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 97/* custom_attributes_cache */
	, &PostEffectsBase_t57_0_0_0/* byval_arg */
	, &PostEffectsBase_t57_1_0_0/* this_arg */
	, &PostEffectsBase_t57_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PostEffectsBase_t57)/* instance_size */
	, sizeof (PostEffectsBase_t57)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 14/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.ImageEffects.PostEffectsHelper
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_46.h"
// Metadata Definition UnityStandardAssets.ImageEffects.PostEffectsHelper
extern TypeInfo PostEffectsHelper_t112_il2cpp_TypeInfo;
// UnityStandardAssets.ImageEffects.PostEffectsHelper
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_46MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.PostEffectsHelper::.ctor()
extern const MethodInfo PostEffectsHelper__ctor_m341_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PostEffectsHelper__ctor_m341/* method */
	, &PostEffectsHelper_t112_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 354/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
static const ParameterInfo PostEffectsHelper_t112_PostEffectsHelper_OnRenderImage_m342_ParameterInfos[] = 
{
	{"source", 0, 134217982, 0, &RenderTexture_t101_0_0_0},
	{"destination", 1, 134217983, 0, &RenderTexture_t101_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.PostEffectsHelper::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern const MethodInfo PostEffectsHelper_OnRenderImage_m342_MethodInfo = 
{
	"OnRenderImage"/* name */
	, (methodPointerType)&PostEffectsHelper_OnRenderImage_m342/* method */
	, &PostEffectsHelper_t112_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, PostEffectsHelper_t112_PostEffectsHelper_OnRenderImage_m342_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 355/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType Material_t55_0_0_0;
extern const Il2CppType Camera_t27_0_0_0;
static const ParameterInfo PostEffectsHelper_t112_PostEffectsHelper_DrawLowLevelPlaneAlignedWithCamera_m343_ParameterInfos[] = 
{
	{"dist", 0, 134217984, 0, &Single_t254_0_0_0},
	{"source", 1, 134217985, 0, &RenderTexture_t101_0_0_0},
	{"dest", 2, 134217986, 0, &RenderTexture_t101_0_0_0},
	{"material", 3, 134217987, 0, &Material_t55_0_0_0},
	{"cameraForProjectionMatrix", 4, 134217988, 0, &Camera_t27_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Single_t254_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.PostEffectsHelper::DrawLowLevelPlaneAlignedWithCamera(System.Single,UnityEngine.RenderTexture,UnityEngine.RenderTexture,UnityEngine.Material,UnityEngine.Camera)
extern const MethodInfo PostEffectsHelper_DrawLowLevelPlaneAlignedWithCamera_m343_MethodInfo = 
{
	"DrawLowLevelPlaneAlignedWithCamera"/* name */
	, (methodPointerType)&PostEffectsHelper_DrawLowLevelPlaneAlignedWithCamera_m343/* method */
	, &PostEffectsHelper_t112_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Single_t254_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, PostEffectsHelper_t112_PostEffectsHelper_DrawLowLevelPlaneAlignedWithCamera_m343_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 356/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType Material_t55_0_0_0;
static const ParameterInfo PostEffectsHelper_t112_PostEffectsHelper_DrawBorder_m344_ParameterInfos[] = 
{
	{"dest", 0, 134217989, 0, &RenderTexture_t101_0_0_0},
	{"material", 1, 134217990, 0, &Material_t55_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.PostEffectsHelper::DrawBorder(UnityEngine.RenderTexture,UnityEngine.Material)
extern const MethodInfo PostEffectsHelper_DrawBorder_m344_MethodInfo = 
{
	"DrawBorder"/* name */
	, (methodPointerType)&PostEffectsHelper_DrawBorder_m344/* method */
	, &PostEffectsHelper_t112_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, PostEffectsHelper_t112_PostEffectsHelper_DrawBorder_m344_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 357/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
extern const Il2CppType Single_t254_0_0_0;
extern const Il2CppType Single_t254_0_0_0;
extern const Il2CppType Single_t254_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType Material_t55_0_0_0;
static const ParameterInfo PostEffectsHelper_t112_PostEffectsHelper_DrawLowLevelQuad_m345_ParameterInfos[] = 
{
	{"x1", 0, 134217991, 0, &Single_t254_0_0_0},
	{"x2", 1, 134217992, 0, &Single_t254_0_0_0},
	{"y1", 2, 134217993, 0, &Single_t254_0_0_0},
	{"y2", 3, 134217994, 0, &Single_t254_0_0_0},
	{"source", 4, 134217995, 0, &RenderTexture_t101_0_0_0},
	{"dest", 5, 134217996, 0, &RenderTexture_t101_0_0_0},
	{"material", 6, 134217997, 0, &Material_t55_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Single_t254_Single_t254_Single_t254_Single_t254_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.PostEffectsHelper::DrawLowLevelQuad(System.Single,System.Single,System.Single,System.Single,UnityEngine.RenderTexture,UnityEngine.RenderTexture,UnityEngine.Material)
extern const MethodInfo PostEffectsHelper_DrawLowLevelQuad_m345_MethodInfo = 
{
	"DrawLowLevelQuad"/* name */
	, (methodPointerType)&PostEffectsHelper_DrawLowLevelQuad_m345/* method */
	, &PostEffectsHelper_t112_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Single_t254_Single_t254_Single_t254_Single_t254_Object_t_Object_t_Object_t/* invoker_method */
	, PostEffectsHelper_t112_PostEffectsHelper_DrawLowLevelQuad_m345_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 7/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 358/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* PostEffectsHelper_t112_MethodInfos[] =
{
	&PostEffectsHelper__ctor_m341_MethodInfo,
	&PostEffectsHelper_OnRenderImage_m342_MethodInfo,
	&PostEffectsHelper_DrawLowLevelPlaneAlignedWithCamera_m343_MethodInfo,
	&PostEffectsHelper_DrawBorder_m344_MethodInfo,
	&PostEffectsHelper_DrawLowLevelQuad_m345_MethodInfo,
	NULL
};
static const Il2CppMethodReference PostEffectsHelper_t112_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool PostEffectsHelper_t112_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType PostEffectsHelper_t112_0_0_0;
extern const Il2CppType PostEffectsHelper_t112_1_0_0;
struct PostEffectsHelper_t112;
const Il2CppTypeDefinitionMetadata PostEffectsHelper_t112_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, PostEffectsHelper_t112_VTable/* vtableMethods */
	, PostEffectsHelper_t112_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo PostEffectsHelper_t112_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "PostEffectsHelper"/* name */
	, "UnityStandardAssets.ImageEffects"/* namespaze */
	, PostEffectsHelper_t112_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &PostEffectsHelper_t112_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 98/* custom_attributes_cache */
	, &PostEffectsHelper_t112_0_0_0/* byval_arg */
	, &PostEffectsHelper_t112_1_0_0/* this_arg */
	, &PostEffectsHelper_t112_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PostEffectsHelper_t112)/* instance_size */
	, sizeof (PostEffectsHelper_t112)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.ImageEffects.Quads
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_47.h"
// Metadata Definition UnityStandardAssets.ImageEffects.Quads
extern TypeInfo Quads_t114_il2cpp_TypeInfo;
// UnityStandardAssets.ImageEffects.Quads
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_47MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.Quads::.ctor()
extern const MethodInfo Quads__ctor_m346_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Quads__ctor_m346/* method */
	, &Quads_t114_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 359/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.Quads::.cctor()
extern const MethodInfo Quads__cctor_m347_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&Quads__cctor_m347/* method */
	, &Quads_t114_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 360/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityStandardAssets.ImageEffects.Quads::HasMeshes()
extern const MethodInfo Quads_HasMeshes_m348_MethodInfo = 
{
	"HasMeshes"/* name */
	, (methodPointerType)&Quads_HasMeshes_m348/* method */
	, &Quads_t114_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 361/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.Quads::Cleanup()
extern const MethodInfo Quads_Cleanup_m349_MethodInfo = 
{
	"Cleanup"/* name */
	, (methodPointerType)&Quads_Cleanup_m349/* method */
	, &Quads_t114_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 362/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo Quads_t114_Quads_GetMeshes_m350_ParameterInfos[] = 
{
	{"totalWidth", 0, 134217998, 0, &Int32_t253_0_0_0},
	{"totalHeight", 1, 134217999, 0, &Int32_t253_0_0_0},
};
extern const Il2CppType MeshU5BU5D_t113_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Mesh[] UnityStandardAssets.ImageEffects.Quads::GetMeshes(System.Int32,System.Int32)
extern const MethodInfo Quads_GetMeshes_m350_MethodInfo = 
{
	"GetMeshes"/* name */
	, (methodPointerType)&Quads_GetMeshes_m350/* method */
	, &Quads_t114_il2cpp_TypeInfo/* declaring_type */
	, &MeshU5BU5D_t113_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t253_Int32_t253/* invoker_method */
	, Quads_t114_Quads_GetMeshes_m350_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 363/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo Quads_t114_Quads_GetMesh_m351_ParameterInfos[] = 
{
	{"triCount", 0, 134218000, 0, &Int32_t253_0_0_0},
	{"triOffset", 1, 134218001, 0, &Int32_t253_0_0_0},
	{"totalWidth", 2, 134218002, 0, &Int32_t253_0_0_0},
	{"totalHeight", 3, 134218003, 0, &Int32_t253_0_0_0},
};
extern const Il2CppType Mesh_t216_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t253_Int32_t253_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Mesh UnityStandardAssets.ImageEffects.Quads::GetMesh(System.Int32,System.Int32,System.Int32,System.Int32)
extern const MethodInfo Quads_GetMesh_m351_MethodInfo = 
{
	"GetMesh"/* name */
	, (methodPointerType)&Quads_GetMesh_m351/* method */
	, &Quads_t114_il2cpp_TypeInfo/* declaring_type */
	, &Mesh_t216_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t253_Int32_t253_Int32_t253_Int32_t253/* invoker_method */
	, Quads_t114_Quads_GetMesh_m351_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 364/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Quads_t114_MethodInfos[] =
{
	&Quads__ctor_m346_MethodInfo,
	&Quads__cctor_m347_MethodInfo,
	&Quads_HasMeshes_m348_MethodInfo,
	&Quads_Cleanup_m349_MethodInfo,
	&Quads_GetMeshes_m350_MethodInfo,
	&Quads_GetMesh_m351_MethodInfo,
	NULL
};
static const Il2CppMethodReference Quads_t114_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool Quads_t114_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType Quads_t114_0_0_0;
extern const Il2CppType Quads_t114_1_0_0;
struct Quads_t114;
const Il2CppTypeDefinitionMetadata Quads_t114_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Quads_t114_VTable/* vtableMethods */
	, Quads_t114_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 591/* fieldStart */

};
TypeInfo Quads_t114_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "Quads"/* name */
	, "UnityStandardAssets.ImageEffects"/* namespaze */
	, Quads_t114_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Quads_t114_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Quads_t114_0_0_0/* byval_arg */
	, &Quads_t114_1_0_0/* this_arg */
	, &Quads_t114_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Quads_t114)/* instance_size */
	, sizeof (Quads_t114)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Quads_t114_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.ImageEffects.ScreenOverlay/OverlayBlendMode
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_48.h"
// Metadata Definition UnityStandardAssets.ImageEffects.ScreenOverlay/OverlayBlendMode
extern TypeInfo OverlayBlendMode_t115_il2cpp_TypeInfo;
// UnityStandardAssets.ImageEffects.ScreenOverlay/OverlayBlendMode
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_48MethodDeclarations.h"
static const MethodInfo* OverlayBlendMode_t115_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference OverlayBlendMode_t115_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool OverlayBlendMode_t115_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair OverlayBlendMode_t115_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType OverlayBlendMode_t115_0_0_0;
extern const Il2CppType OverlayBlendMode_t115_1_0_0;
extern TypeInfo ScreenOverlay_t116_il2cpp_TypeInfo;
extern const Il2CppType ScreenOverlay_t116_0_0_0;
const Il2CppTypeDefinitionMetadata OverlayBlendMode_t115_DefinitionMetadata = 
{
	&ScreenOverlay_t116_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, OverlayBlendMode_t115_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, OverlayBlendMode_t115_VTable/* vtableMethods */
	, OverlayBlendMode_t115_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 593/* fieldStart */

};
TypeInfo OverlayBlendMode_t115_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "OverlayBlendMode"/* name */
	, ""/* namespaze */
	, OverlayBlendMode_t115_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &OverlayBlendMode_t115_0_0_0/* byval_arg */
	, &OverlayBlendMode_t115_1_0_0/* this_arg */
	, &OverlayBlendMode_t115_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OverlayBlendMode_t115)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (OverlayBlendMode_t115)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityStandardAssets.ImageEffects.ScreenOverlay
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_49.h"
// Metadata Definition UnityStandardAssets.ImageEffects.ScreenOverlay
// UnityStandardAssets.ImageEffects.ScreenOverlay
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_49MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.ScreenOverlay::.ctor()
extern const MethodInfo ScreenOverlay__ctor_m352_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ScreenOverlay__ctor_m352/* method */
	, &ScreenOverlay_t116_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 365/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityStandardAssets.ImageEffects.ScreenOverlay::CheckResources()
extern const MethodInfo ScreenOverlay_CheckResources_m353_MethodInfo = 
{
	"CheckResources"/* name */
	, (methodPointerType)&ScreenOverlay_CheckResources_m353/* method */
	, &ScreenOverlay_t116_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 366/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
static const ParameterInfo ScreenOverlay_t116_ScreenOverlay_OnRenderImage_m354_ParameterInfos[] = 
{
	{"source", 0, 134218004, 0, &RenderTexture_t101_0_0_0},
	{"destination", 1, 134218005, 0, &RenderTexture_t101_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.ScreenOverlay::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern const MethodInfo ScreenOverlay_OnRenderImage_m354_MethodInfo = 
{
	"OnRenderImage"/* name */
	, (methodPointerType)&ScreenOverlay_OnRenderImage_m354/* method */
	, &ScreenOverlay_t116_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, ScreenOverlay_t116_ScreenOverlay_OnRenderImage_m354_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 367/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ScreenOverlay_t116_MethodInfos[] =
{
	&ScreenOverlay__ctor_m352_MethodInfo,
	&ScreenOverlay_CheckResources_m353_MethodInfo,
	&ScreenOverlay_OnRenderImage_m354_MethodInfo,
	NULL
};
static const Il2CppType* ScreenOverlay_t116_il2cpp_TypeInfo__nestedTypes[1] =
{
	&OverlayBlendMode_t115_0_0_0,
};
extern const MethodInfo ScreenOverlay_CheckResources_m353_MethodInfo;
static const Il2CppMethodReference ScreenOverlay_t116_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	&ScreenOverlay_CheckResources_m353_MethodInfo,
};
static bool ScreenOverlay_t116_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType ScreenOverlay_t116_1_0_0;
struct ScreenOverlay_t116;
const Il2CppTypeDefinitionMetadata ScreenOverlay_t116_DefinitionMetadata = 
{
	NULL/* declaringType */
	, ScreenOverlay_t116_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &PostEffectsBase_t57_0_0_0/* parent */
	, ScreenOverlay_t116_VTable/* vtableMethods */
	, ScreenOverlay_t116_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 599/* fieldStart */

};
TypeInfo ScreenOverlay_t116_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "ScreenOverlay"/* name */
	, "UnityStandardAssets.ImageEffects"/* namespaze */
	, ScreenOverlay_t116_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ScreenOverlay_t116_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 99/* custom_attributes_cache */
	, &ScreenOverlay_t116_0_0_0/* byval_arg */
	, &ScreenOverlay_t116_1_0_0/* this_arg */
	, &ScreenOverlay_t116_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ScreenOverlay_t116)/* instance_size */
	, sizeof (ScreenOverlay_t116)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.ImageEffects.ScreenSpaceAmbientObscurance
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_50.h"
// Metadata Definition UnityStandardAssets.ImageEffects.ScreenSpaceAmbientObscurance
extern TypeInfo ScreenSpaceAmbientObscurance_t117_il2cpp_TypeInfo;
// UnityStandardAssets.ImageEffects.ScreenSpaceAmbientObscurance
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_50MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.ScreenSpaceAmbientObscurance::.ctor()
extern const MethodInfo ScreenSpaceAmbientObscurance__ctor_m355_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ScreenSpaceAmbientObscurance__ctor_m355/* method */
	, &ScreenSpaceAmbientObscurance_t117_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 368/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityStandardAssets.ImageEffects.ScreenSpaceAmbientObscurance::CheckResources()
extern const MethodInfo ScreenSpaceAmbientObscurance_CheckResources_m356_MethodInfo = 
{
	"CheckResources"/* name */
	, (methodPointerType)&ScreenSpaceAmbientObscurance_CheckResources_m356/* method */
	, &ScreenSpaceAmbientObscurance_t117_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 369/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.ScreenSpaceAmbientObscurance::OnDisable()
extern const MethodInfo ScreenSpaceAmbientObscurance_OnDisable_m357_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&ScreenSpaceAmbientObscurance_OnDisable_m357/* method */
	, &ScreenSpaceAmbientObscurance_t117_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 370/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
static const ParameterInfo ScreenSpaceAmbientObscurance_t117_ScreenSpaceAmbientObscurance_OnRenderImage_m358_ParameterInfos[] = 
{
	{"source", 0, 134218006, 0, &RenderTexture_t101_0_0_0},
	{"destination", 1, 134218007, 0, &RenderTexture_t101_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.ScreenSpaceAmbientObscurance::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern const MethodInfo ScreenSpaceAmbientObscurance_OnRenderImage_m358_MethodInfo = 
{
	"OnRenderImage"/* name */
	, (methodPointerType)&ScreenSpaceAmbientObscurance_OnRenderImage_m358/* method */
	, &ScreenSpaceAmbientObscurance_t117_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, ScreenSpaceAmbientObscurance_t117_ScreenSpaceAmbientObscurance_OnRenderImage_m358_ParameterInfos/* parameters */
	, 106/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 371/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ScreenSpaceAmbientObscurance_t117_MethodInfos[] =
{
	&ScreenSpaceAmbientObscurance__ctor_m355_MethodInfo,
	&ScreenSpaceAmbientObscurance_CheckResources_m356_MethodInfo,
	&ScreenSpaceAmbientObscurance_OnDisable_m357_MethodInfo,
	&ScreenSpaceAmbientObscurance_OnRenderImage_m358_MethodInfo,
	NULL
};
extern const MethodInfo ScreenSpaceAmbientObscurance_CheckResources_m356_MethodInfo;
static const Il2CppMethodReference ScreenSpaceAmbientObscurance_t117_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	&ScreenSpaceAmbientObscurance_CheckResources_m356_MethodInfo,
};
static bool ScreenSpaceAmbientObscurance_t117_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType ScreenSpaceAmbientObscurance_t117_0_0_0;
extern const Il2CppType ScreenSpaceAmbientObscurance_t117_1_0_0;
struct ScreenSpaceAmbientObscurance_t117;
const Il2CppTypeDefinitionMetadata ScreenSpaceAmbientObscurance_t117_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &PostEffectsBase_t57_0_0_0/* parent */
	, ScreenSpaceAmbientObscurance_t117_VTable/* vtableMethods */
	, ScreenSpaceAmbientObscurance_t117_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 604/* fieldStart */

};
TypeInfo ScreenSpaceAmbientObscurance_t117_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "ScreenSpaceAmbientObscurance"/* name */
	, "UnityStandardAssets.ImageEffects"/* namespaze */
	, ScreenSpaceAmbientObscurance_t117_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ScreenSpaceAmbientObscurance_t117_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 100/* custom_attributes_cache */
	, &ScreenSpaceAmbientObscurance_t117_0_0_0/* byval_arg */
	, &ScreenSpaceAmbientObscurance_t117_1_0_0/* this_arg */
	, &ScreenSpaceAmbientObscurance_t117_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ScreenSpaceAmbientObscurance_t117)/* instance_size */
	, sizeof (ScreenSpaceAmbientObscurance_t117)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion/SSAOSamples
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_51.h"
// Metadata Definition UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion/SSAOSamples
extern TypeInfo SSAOSamples_t118_il2cpp_TypeInfo;
// UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion/SSAOSamples
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_51MethodDeclarations.h"
static const MethodInfo* SSAOSamples_t118_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference SSAOSamples_t118_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool SSAOSamples_t118_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair SSAOSamples_t118_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType SSAOSamples_t118_0_0_0;
extern const Il2CppType SSAOSamples_t118_1_0_0;
extern TypeInfo ScreenSpaceAmbientOcclusion_t119_il2cpp_TypeInfo;
extern const Il2CppType ScreenSpaceAmbientOcclusion_t119_0_0_0;
const Il2CppTypeDefinitionMetadata SSAOSamples_t118_DefinitionMetadata = 
{
	&ScreenSpaceAmbientOcclusion_t119_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SSAOSamples_t118_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, SSAOSamples_t118_VTable/* vtableMethods */
	, SSAOSamples_t118_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 612/* fieldStart */

};
TypeInfo SSAOSamples_t118_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "SSAOSamples"/* name */
	, ""/* namespaze */
	, SSAOSamples_t118_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SSAOSamples_t118_0_0_0/* byval_arg */
	, &SSAOSamples_t118_1_0_0/* this_arg */
	, &SSAOSamples_t118_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SSAOSamples_t118)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (SSAOSamples_t118)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_52.h"
// Metadata Definition UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion
// UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_52MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::.ctor()
extern const MethodInfo ScreenSpaceAmbientOcclusion__ctor_m359_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ScreenSpaceAmbientOcclusion__ctor_m359/* method */
	, &ScreenSpaceAmbientOcclusion_t119_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 372/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Shader_t54_0_0_0;
static const ParameterInfo ScreenSpaceAmbientOcclusion_t119_ScreenSpaceAmbientOcclusion_CreateMaterial_m360_ParameterInfos[] = 
{
	{"shader", 0, 134218008, 0, &Shader_t54_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Material UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::CreateMaterial(UnityEngine.Shader)
extern const MethodInfo ScreenSpaceAmbientOcclusion_CreateMaterial_m360_MethodInfo = 
{
	"CreateMaterial"/* name */
	, (methodPointerType)&ScreenSpaceAmbientOcclusion_CreateMaterial_m360/* method */
	, &ScreenSpaceAmbientOcclusion_t119_il2cpp_TypeInfo/* declaring_type */
	, &Material_t55_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ScreenSpaceAmbientOcclusion_t119_ScreenSpaceAmbientOcclusion_CreateMaterial_m360_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 373/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Material_t55_0_0_0;
static const ParameterInfo ScreenSpaceAmbientOcclusion_t119_ScreenSpaceAmbientOcclusion_DestroyMaterial_m361_ParameterInfos[] = 
{
	{"mat", 0, 134218009, 0, &Material_t55_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::DestroyMaterial(UnityEngine.Material)
extern const MethodInfo ScreenSpaceAmbientOcclusion_DestroyMaterial_m361_MethodInfo = 
{
	"DestroyMaterial"/* name */
	, (methodPointerType)&ScreenSpaceAmbientOcclusion_DestroyMaterial_m361/* method */
	, &ScreenSpaceAmbientOcclusion_t119_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, ScreenSpaceAmbientOcclusion_t119_ScreenSpaceAmbientOcclusion_DestroyMaterial_m361_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 374/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::OnDisable()
extern const MethodInfo ScreenSpaceAmbientOcclusion_OnDisable_m362_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&ScreenSpaceAmbientOcclusion_OnDisable_m362/* method */
	, &ScreenSpaceAmbientOcclusion_t119_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 375/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::Start()
extern const MethodInfo ScreenSpaceAmbientOcclusion_Start_m363_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&ScreenSpaceAmbientOcclusion_Start_m363/* method */
	, &ScreenSpaceAmbientOcclusion_t119_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 376/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::OnEnable()
extern const MethodInfo ScreenSpaceAmbientOcclusion_OnEnable_m364_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&ScreenSpaceAmbientOcclusion_OnEnable_m364/* method */
	, &ScreenSpaceAmbientOcclusion_t119_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 377/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::CreateMaterials()
extern const MethodInfo ScreenSpaceAmbientOcclusion_CreateMaterials_m365_MethodInfo = 
{
	"CreateMaterials"/* name */
	, (methodPointerType)&ScreenSpaceAmbientOcclusion_CreateMaterials_m365/* method */
	, &ScreenSpaceAmbientOcclusion_t119_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 378/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
static const ParameterInfo ScreenSpaceAmbientOcclusion_t119_ScreenSpaceAmbientOcclusion_OnRenderImage_m366_ParameterInfos[] = 
{
	{"source", 0, 134218010, 0, &RenderTexture_t101_0_0_0},
	{"destination", 1, 134218011, 0, &RenderTexture_t101_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern const MethodInfo ScreenSpaceAmbientOcclusion_OnRenderImage_m366_MethodInfo = 
{
	"OnRenderImage"/* name */
	, (methodPointerType)&ScreenSpaceAmbientOcclusion_OnRenderImage_m366/* method */
	, &ScreenSpaceAmbientOcclusion_t119_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, ScreenSpaceAmbientOcclusion_t119_ScreenSpaceAmbientOcclusion_OnRenderImage_m366_ParameterInfos/* parameters */
	, 108/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 379/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ScreenSpaceAmbientOcclusion_t119_MethodInfos[] =
{
	&ScreenSpaceAmbientOcclusion__ctor_m359_MethodInfo,
	&ScreenSpaceAmbientOcclusion_CreateMaterial_m360_MethodInfo,
	&ScreenSpaceAmbientOcclusion_DestroyMaterial_m361_MethodInfo,
	&ScreenSpaceAmbientOcclusion_OnDisable_m362_MethodInfo,
	&ScreenSpaceAmbientOcclusion_Start_m363_MethodInfo,
	&ScreenSpaceAmbientOcclusion_OnEnable_m364_MethodInfo,
	&ScreenSpaceAmbientOcclusion_CreateMaterials_m365_MethodInfo,
	&ScreenSpaceAmbientOcclusion_OnRenderImage_m366_MethodInfo,
	NULL
};
static const Il2CppType* ScreenSpaceAmbientOcclusion_t119_il2cpp_TypeInfo__nestedTypes[1] =
{
	&SSAOSamples_t118_0_0_0,
};
static const Il2CppMethodReference ScreenSpaceAmbientOcclusion_t119_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool ScreenSpaceAmbientOcclusion_t119_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType ScreenSpaceAmbientOcclusion_t119_1_0_0;
struct ScreenSpaceAmbientOcclusion_t119;
const Il2CppTypeDefinitionMetadata ScreenSpaceAmbientOcclusion_t119_DefinitionMetadata = 
{
	NULL/* declaringType */
	, ScreenSpaceAmbientOcclusion_t119_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, ScreenSpaceAmbientOcclusion_t119_VTable/* vtableMethods */
	, ScreenSpaceAmbientOcclusion_t119_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 616/* fieldStart */

};
TypeInfo ScreenSpaceAmbientOcclusion_t119_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "ScreenSpaceAmbientOcclusion"/* name */
	, "UnityStandardAssets.ImageEffects"/* namespaze */
	, ScreenSpaceAmbientOcclusion_t119_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ScreenSpaceAmbientOcclusion_t119_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 107/* custom_attributes_cache */
	, &ScreenSpaceAmbientOcclusion_t119_0_0_0/* byval_arg */
	, &ScreenSpaceAmbientOcclusion_t119_1_0_0/* this_arg */
	, &ScreenSpaceAmbientOcclusion_t119_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ScreenSpaceAmbientOcclusion_t119)/* instance_size */
	, sizeof (ScreenSpaceAmbientOcclusion_t119)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 0/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.ImageEffects.SepiaTone
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_53.h"
// Metadata Definition UnityStandardAssets.ImageEffects.SepiaTone
extern TypeInfo SepiaTone_t120_il2cpp_TypeInfo;
// UnityStandardAssets.ImageEffects.SepiaTone
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_53MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.SepiaTone::.ctor()
extern const MethodInfo SepiaTone__ctor_m367_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SepiaTone__ctor_m367/* method */
	, &SepiaTone_t120_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 380/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
static const ParameterInfo SepiaTone_t120_SepiaTone_OnRenderImage_m368_ParameterInfos[] = 
{
	{"source", 0, 134218012, 0, &RenderTexture_t101_0_0_0},
	{"destination", 1, 134218013, 0, &RenderTexture_t101_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.SepiaTone::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern const MethodInfo SepiaTone_OnRenderImage_m368_MethodInfo = 
{
	"OnRenderImage"/* name */
	, (methodPointerType)&SepiaTone_OnRenderImage_m368/* method */
	, &SepiaTone_t120_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, SepiaTone_t120_SepiaTone_OnRenderImage_m368_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 381/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SepiaTone_t120_MethodInfos[] =
{
	&SepiaTone__ctor_m367_MethodInfo,
	&SepiaTone_OnRenderImage_m368_MethodInfo,
	NULL
};
static const Il2CppMethodReference SepiaTone_t120_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	&ImageEffectBase_Start_m305_MethodInfo,
	&ImageEffectBase_OnDisable_m307_MethodInfo,
};
static bool SepiaTone_t120_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType SepiaTone_t120_0_0_0;
extern const Il2CppType SepiaTone_t120_1_0_0;
struct SepiaTone_t120;
const Il2CppTypeDefinitionMetadata SepiaTone_t120_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ImageEffectBase_t88_0_0_0/* parent */
	, SepiaTone_t120_VTable/* vtableMethods */
	, SepiaTone_t120_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo SepiaTone_t120_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "SepiaTone"/* name */
	, "UnityStandardAssets.ImageEffects"/* namespaze */
	, SepiaTone_t120_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SepiaTone_t120_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 109/* custom_attributes_cache */
	, &SepiaTone_t120_0_0_0/* byval_arg */
	, &SepiaTone_t120_1_0_0/* this_arg */
	, &SepiaTone_t120_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SepiaTone_t120)/* instance_size */
	, sizeof (SepiaTone_t120)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.ImageEffects.SunShafts/SunShaftsResolution
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_54.h"
// Metadata Definition UnityStandardAssets.ImageEffects.SunShafts/SunShaftsResolution
extern TypeInfo SunShaftsResolution_t121_il2cpp_TypeInfo;
// UnityStandardAssets.ImageEffects.SunShafts/SunShaftsResolution
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_54MethodDeclarations.h"
static const MethodInfo* SunShaftsResolution_t121_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference SunShaftsResolution_t121_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool SunShaftsResolution_t121_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair SunShaftsResolution_t121_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType SunShaftsResolution_t121_0_0_0;
extern const Il2CppType SunShaftsResolution_t121_1_0_0;
extern TypeInfo SunShafts_t123_il2cpp_TypeInfo;
extern const Il2CppType SunShafts_t123_0_0_0;
const Il2CppTypeDefinitionMetadata SunShaftsResolution_t121_DefinitionMetadata = 
{
	&SunShafts_t123_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SunShaftsResolution_t121_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, SunShaftsResolution_t121_VTable/* vtableMethods */
	, SunShaftsResolution_t121_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 627/* fieldStart */

};
TypeInfo SunShaftsResolution_t121_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "SunShaftsResolution"/* name */
	, ""/* namespaze */
	, SunShaftsResolution_t121_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SunShaftsResolution_t121_0_0_0/* byval_arg */
	, &SunShaftsResolution_t121_1_0_0/* this_arg */
	, &SunShaftsResolution_t121_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SunShaftsResolution_t121)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (SunShaftsResolution_t121)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityStandardAssets.ImageEffects.SunShafts/ShaftsScreenBlendMode
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_55.h"
// Metadata Definition UnityStandardAssets.ImageEffects.SunShafts/ShaftsScreenBlendMode
extern TypeInfo ShaftsScreenBlendMode_t122_il2cpp_TypeInfo;
// UnityStandardAssets.ImageEffects.SunShafts/ShaftsScreenBlendMode
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_55MethodDeclarations.h"
static const MethodInfo* ShaftsScreenBlendMode_t122_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference ShaftsScreenBlendMode_t122_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool ShaftsScreenBlendMode_t122_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ShaftsScreenBlendMode_t122_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType ShaftsScreenBlendMode_t122_0_0_0;
extern const Il2CppType ShaftsScreenBlendMode_t122_1_0_0;
const Il2CppTypeDefinitionMetadata ShaftsScreenBlendMode_t122_DefinitionMetadata = 
{
	&SunShafts_t123_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ShaftsScreenBlendMode_t122_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, ShaftsScreenBlendMode_t122_VTable/* vtableMethods */
	, ShaftsScreenBlendMode_t122_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 631/* fieldStart */

};
TypeInfo ShaftsScreenBlendMode_t122_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "ShaftsScreenBlendMode"/* name */
	, ""/* namespaze */
	, ShaftsScreenBlendMode_t122_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ShaftsScreenBlendMode_t122_0_0_0/* byval_arg */
	, &ShaftsScreenBlendMode_t122_1_0_0/* this_arg */
	, &ShaftsScreenBlendMode_t122_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ShaftsScreenBlendMode_t122)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ShaftsScreenBlendMode_t122)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityStandardAssets.ImageEffects.SunShafts
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_56.h"
// Metadata Definition UnityStandardAssets.ImageEffects.SunShafts
// UnityStandardAssets.ImageEffects.SunShafts
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_56MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.SunShafts::.ctor()
extern const MethodInfo SunShafts__ctor_m369_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SunShafts__ctor_m369/* method */
	, &SunShafts_t123_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 382/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityStandardAssets.ImageEffects.SunShafts::CheckResources()
extern const MethodInfo SunShafts_CheckResources_m370_MethodInfo = 
{
	"CheckResources"/* name */
	, (methodPointerType)&SunShafts_CheckResources_m370/* method */
	, &SunShafts_t123_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 383/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
static const ParameterInfo SunShafts_t123_SunShafts_OnRenderImage_m371_ParameterInfos[] = 
{
	{"source", 0, 134218014, 0, &RenderTexture_t101_0_0_0},
	{"destination", 1, 134218015, 0, &RenderTexture_t101_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.SunShafts::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern const MethodInfo SunShafts_OnRenderImage_m371_MethodInfo = 
{
	"OnRenderImage"/* name */
	, (methodPointerType)&SunShafts_OnRenderImage_m371/* method */
	, &SunShafts_t123_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, SunShafts_t123_SunShafts_OnRenderImage_m371_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 384/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SunShafts_t123_MethodInfos[] =
{
	&SunShafts__ctor_m369_MethodInfo,
	&SunShafts_CheckResources_m370_MethodInfo,
	&SunShafts_OnRenderImage_m371_MethodInfo,
	NULL
};
static const Il2CppType* SunShafts_t123_il2cpp_TypeInfo__nestedTypes[2] =
{
	&SunShaftsResolution_t121_0_0_0,
	&ShaftsScreenBlendMode_t122_0_0_0,
};
extern const MethodInfo SunShafts_CheckResources_m370_MethodInfo;
static const Il2CppMethodReference SunShafts_t123_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	&SunShafts_CheckResources_m370_MethodInfo,
};
static bool SunShafts_t123_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType SunShafts_t123_1_0_0;
struct SunShafts_t123;
const Il2CppTypeDefinitionMetadata SunShafts_t123_DefinitionMetadata = 
{
	NULL/* declaringType */
	, SunShafts_t123_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &PostEffectsBase_t57_0_0_0/* parent */
	, SunShafts_t123_VTable/* vtableMethods */
	, SunShafts_t123_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 634/* fieldStart */

};
TypeInfo SunShafts_t123_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "SunShafts"/* name */
	, "UnityStandardAssets.ImageEffects"/* namespaze */
	, SunShafts_t123_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SunShafts_t123_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 110/* custom_attributes_cache */
	, &SunShafts_t123_0_0_0/* byval_arg */
	, &SunShafts_t123_1_0_0/* this_arg */
	, &SunShafts_t123_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SunShafts_t123)/* instance_size */
	, sizeof (SunShafts_t123)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 14/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.ImageEffects.TiltShift/TiltShiftMode
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_57.h"
// Metadata Definition UnityStandardAssets.ImageEffects.TiltShift/TiltShiftMode
extern TypeInfo TiltShiftMode_t124_il2cpp_TypeInfo;
// UnityStandardAssets.ImageEffects.TiltShift/TiltShiftMode
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_57MethodDeclarations.h"
static const MethodInfo* TiltShiftMode_t124_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference TiltShiftMode_t124_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool TiltShiftMode_t124_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TiltShiftMode_t124_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType TiltShiftMode_t124_0_0_0;
extern const Il2CppType TiltShiftMode_t124_1_0_0;
extern TypeInfo TiltShift_t126_il2cpp_TypeInfo;
extern const Il2CppType TiltShift_t126_0_0_0;
const Il2CppTypeDefinitionMetadata TiltShiftMode_t124_DefinitionMetadata = 
{
	&TiltShift_t126_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TiltShiftMode_t124_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, TiltShiftMode_t124_VTable/* vtableMethods */
	, TiltShiftMode_t124_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 648/* fieldStart */

};
TypeInfo TiltShiftMode_t124_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "TiltShiftMode"/* name */
	, ""/* namespaze */
	, TiltShiftMode_t124_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TiltShiftMode_t124_0_0_0/* byval_arg */
	, &TiltShiftMode_t124_1_0_0/* this_arg */
	, &TiltShiftMode_t124_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TiltShiftMode_t124)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TiltShiftMode_t124)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityStandardAssets.ImageEffects.TiltShift/TiltShiftQuality
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_58.h"
// Metadata Definition UnityStandardAssets.ImageEffects.TiltShift/TiltShiftQuality
extern TypeInfo TiltShiftQuality_t125_il2cpp_TypeInfo;
// UnityStandardAssets.ImageEffects.TiltShift/TiltShiftQuality
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_58MethodDeclarations.h"
static const MethodInfo* TiltShiftQuality_t125_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference TiltShiftQuality_t125_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool TiltShiftQuality_t125_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TiltShiftQuality_t125_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType TiltShiftQuality_t125_0_0_0;
extern const Il2CppType TiltShiftQuality_t125_1_0_0;
const Il2CppTypeDefinitionMetadata TiltShiftQuality_t125_DefinitionMetadata = 
{
	&TiltShift_t126_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TiltShiftQuality_t125_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, TiltShiftQuality_t125_VTable/* vtableMethods */
	, TiltShiftQuality_t125_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 651/* fieldStart */

};
TypeInfo TiltShiftQuality_t125_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "TiltShiftQuality"/* name */
	, ""/* namespaze */
	, TiltShiftQuality_t125_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TiltShiftQuality_t125_0_0_0/* byval_arg */
	, &TiltShiftQuality_t125_1_0_0/* this_arg */
	, &TiltShiftQuality_t125_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TiltShiftQuality_t125)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TiltShiftQuality_t125)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityStandardAssets.ImageEffects.TiltShift
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_59.h"
// Metadata Definition UnityStandardAssets.ImageEffects.TiltShift
// UnityStandardAssets.ImageEffects.TiltShift
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_59MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.TiltShift::.ctor()
extern const MethodInfo TiltShift__ctor_m372_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TiltShift__ctor_m372/* method */
	, &TiltShift_t126_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 385/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityStandardAssets.ImageEffects.TiltShift::CheckResources()
extern const MethodInfo TiltShift_CheckResources_m373_MethodInfo = 
{
	"CheckResources"/* name */
	, (methodPointerType)&TiltShift_CheckResources_m373/* method */
	, &TiltShift_t126_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 386/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
static const ParameterInfo TiltShift_t126_TiltShift_OnRenderImage_m374_ParameterInfos[] = 
{
	{"source", 0, 134218016, 0, &RenderTexture_t101_0_0_0},
	{"destination", 1, 134218017, 0, &RenderTexture_t101_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.TiltShift::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern const MethodInfo TiltShift_OnRenderImage_m374_MethodInfo = 
{
	"OnRenderImage"/* name */
	, (methodPointerType)&TiltShift_OnRenderImage_m374/* method */
	, &TiltShift_t126_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, TiltShift_t126_TiltShift_OnRenderImage_m374_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 387/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TiltShift_t126_MethodInfos[] =
{
	&TiltShift__ctor_m372_MethodInfo,
	&TiltShift_CheckResources_m373_MethodInfo,
	&TiltShift_OnRenderImage_m374_MethodInfo,
	NULL
};
static const Il2CppType* TiltShift_t126_il2cpp_TypeInfo__nestedTypes[2] =
{
	&TiltShiftMode_t124_0_0_0,
	&TiltShiftQuality_t125_0_0_0,
};
extern const MethodInfo TiltShift_CheckResources_m373_MethodInfo;
static const Il2CppMethodReference TiltShift_t126_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	&TiltShift_CheckResources_m373_MethodInfo,
};
static bool TiltShift_t126_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType TiltShift_t126_1_0_0;
struct TiltShift_t126;
const Il2CppTypeDefinitionMetadata TiltShift_t126_DefinitionMetadata = 
{
	NULL/* declaringType */
	, TiltShift_t126_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &PostEffectsBase_t57_0_0_0/* parent */
	, TiltShift_t126_VTable/* vtableMethods */
	, TiltShift_t126_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 655/* fieldStart */

};
TypeInfo TiltShift_t126_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "TiltShift"/* name */
	, "UnityStandardAssets.ImageEffects"/* namespaze */
	, TiltShift_t126_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TiltShift_t126_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 111/* custom_attributes_cache */
	, &TiltShift_t126_0_0_0/* byval_arg */
	, &TiltShift_t126_1_0_0/* this_arg */
	, &TiltShift_t126_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TiltShift_t126)/* instance_size */
	, sizeof (TiltShift_t126)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.ImageEffects.Tonemapping/TonemapperType
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_60.h"
// Metadata Definition UnityStandardAssets.ImageEffects.Tonemapping/TonemapperType
extern TypeInfo TonemapperType_t127_il2cpp_TypeInfo;
// UnityStandardAssets.ImageEffects.Tonemapping/TonemapperType
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_60MethodDeclarations.h"
static const MethodInfo* TonemapperType_t127_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference TonemapperType_t127_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool TonemapperType_t127_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TonemapperType_t127_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType TonemapperType_t127_0_0_0;
extern const Il2CppType TonemapperType_t127_1_0_0;
extern TypeInfo Tonemapping_t129_il2cpp_TypeInfo;
extern const Il2CppType Tonemapping_t129_0_0_0;
const Il2CppTypeDefinitionMetadata TonemapperType_t127_DefinitionMetadata = 
{
	&Tonemapping_t129_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TonemapperType_t127_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, TonemapperType_t127_VTable/* vtableMethods */
	, TonemapperType_t127_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 662/* fieldStart */

};
TypeInfo TonemapperType_t127_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "TonemapperType"/* name */
	, ""/* namespaze */
	, TonemapperType_t127_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TonemapperType_t127_0_0_0/* byval_arg */
	, &TonemapperType_t127_1_0_0/* this_arg */
	, &TonemapperType_t127_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TonemapperType_t127)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TonemapperType_t127)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityStandardAssets.ImageEffects.Tonemapping/AdaptiveTexSize
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_61.h"
// Metadata Definition UnityStandardAssets.ImageEffects.Tonemapping/AdaptiveTexSize
extern TypeInfo AdaptiveTexSize_t128_il2cpp_TypeInfo;
// UnityStandardAssets.ImageEffects.Tonemapping/AdaptiveTexSize
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_61MethodDeclarations.h"
static const MethodInfo* AdaptiveTexSize_t128_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference AdaptiveTexSize_t128_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool AdaptiveTexSize_t128_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair AdaptiveTexSize_t128_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType AdaptiveTexSize_t128_0_0_0;
extern const Il2CppType AdaptiveTexSize_t128_1_0_0;
const Il2CppTypeDefinitionMetadata AdaptiveTexSize_t128_DefinitionMetadata = 
{
	&Tonemapping_t129_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AdaptiveTexSize_t128_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, AdaptiveTexSize_t128_VTable/* vtableMethods */
	, AdaptiveTexSize_t128_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 670/* fieldStart */

};
TypeInfo AdaptiveTexSize_t128_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "AdaptiveTexSize"/* name */
	, ""/* namespaze */
	, AdaptiveTexSize_t128_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AdaptiveTexSize_t128_0_0_0/* byval_arg */
	, &AdaptiveTexSize_t128_1_0_0/* this_arg */
	, &AdaptiveTexSize_t128_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AdaptiveTexSize_t128)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (AdaptiveTexSize_t128)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityStandardAssets.ImageEffects.Tonemapping
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_62.h"
// Metadata Definition UnityStandardAssets.ImageEffects.Tonemapping
// UnityStandardAssets.ImageEffects.Tonemapping
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_62MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.Tonemapping::.ctor()
extern const MethodInfo Tonemapping__ctor_m375_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Tonemapping__ctor_m375/* method */
	, &Tonemapping_t129_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 388/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityStandardAssets.ImageEffects.Tonemapping::CheckResources()
extern const MethodInfo Tonemapping_CheckResources_m376_MethodInfo = 
{
	"CheckResources"/* name */
	, (methodPointerType)&Tonemapping_CheckResources_m376/* method */
	, &Tonemapping_t129_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 389/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityStandardAssets.ImageEffects.Tonemapping::UpdateCurve()
extern const MethodInfo Tonemapping_UpdateCurve_m377_MethodInfo = 
{
	"UpdateCurve"/* name */
	, (methodPointerType)&Tonemapping_UpdateCurve_m377/* method */
	, &Tonemapping_t129_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 390/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.Tonemapping::OnDisable()
extern const MethodInfo Tonemapping_OnDisable_m378_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&Tonemapping_OnDisable_m378/* method */
	, &Tonemapping_t129_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 391/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityStandardAssets.ImageEffects.Tonemapping::CreateInternalRenderTexture()
extern const MethodInfo Tonemapping_CreateInternalRenderTexture_m379_MethodInfo = 
{
	"CreateInternalRenderTexture"/* name */
	, (methodPointerType)&Tonemapping_CreateInternalRenderTexture_m379/* method */
	, &Tonemapping_t129_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 392/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
static const ParameterInfo Tonemapping_t129_Tonemapping_OnRenderImage_m380_ParameterInfos[] = 
{
	{"source", 0, 134218018, 0, &RenderTexture_t101_0_0_0},
	{"destination", 1, 134218019, 0, &RenderTexture_t101_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.Tonemapping::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern const MethodInfo Tonemapping_OnRenderImage_m380_MethodInfo = 
{
	"OnRenderImage"/* name */
	, (methodPointerType)&Tonemapping_OnRenderImage_m380/* method */
	, &Tonemapping_t129_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, Tonemapping_t129_Tonemapping_OnRenderImage_m380_ParameterInfos/* parameters */
	, 116/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 393/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Tonemapping_t129_MethodInfos[] =
{
	&Tonemapping__ctor_m375_MethodInfo,
	&Tonemapping_CheckResources_m376_MethodInfo,
	&Tonemapping_UpdateCurve_m377_MethodInfo,
	&Tonemapping_OnDisable_m378_MethodInfo,
	&Tonemapping_CreateInternalRenderTexture_m379_MethodInfo,
	&Tonemapping_OnRenderImage_m380_MethodInfo,
	NULL
};
static const Il2CppType* Tonemapping_t129_il2cpp_TypeInfo__nestedTypes[2] =
{
	&TonemapperType_t127_0_0_0,
	&AdaptiveTexSize_t128_0_0_0,
};
extern const MethodInfo Tonemapping_CheckResources_m376_MethodInfo;
static const Il2CppMethodReference Tonemapping_t129_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	&Tonemapping_CheckResources_m376_MethodInfo,
};
static bool Tonemapping_t129_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType Tonemapping_t129_1_0_0;
struct Tonemapping_t129;
const Il2CppTypeDefinitionMetadata Tonemapping_t129_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Tonemapping_t129_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &PostEffectsBase_t57_0_0_0/* parent */
	, Tonemapping_t129_VTable/* vtableMethods */
	, Tonemapping_t129_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 678/* fieldStart */

};
TypeInfo Tonemapping_t129_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "Tonemapping"/* name */
	, "UnityStandardAssets.ImageEffects"/* namespaze */
	, Tonemapping_t129_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Tonemapping_t129_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 115/* custom_attributes_cache */
	, &Tonemapping_t129_0_0_0/* byval_arg */
	, &Tonemapping_t129_1_0_0/* this_arg */
	, &Tonemapping_t129_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Tonemapping_t129)/* instance_size */
	, sizeof (Tonemapping_t129)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 13/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
