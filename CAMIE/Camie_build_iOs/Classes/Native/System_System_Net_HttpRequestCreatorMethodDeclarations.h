﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Net.HttpRequestCreator
struct HttpRequestCreator_t1291;
// System.Net.WebRequest
struct WebRequest_t1285;
// System.Uri
struct Uri_t926;

// System.Void System.Net.HttpRequestCreator::.ctor()
extern "C" void HttpRequestCreator__ctor_m5581 (HttpRequestCreator_t1291 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.WebRequest System.Net.HttpRequestCreator::Create(System.Uri)
extern "C" WebRequest_t1285 * HttpRequestCreator_Create_m5582 (HttpRequestCreator_t1291 * __this, Uri_t926 * ___uri, const MethodInfo* method) IL2CPP_METHOD_ATTR;
