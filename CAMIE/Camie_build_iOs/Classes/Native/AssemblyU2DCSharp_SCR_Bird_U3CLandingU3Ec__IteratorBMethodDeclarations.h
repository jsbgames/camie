﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SCR_Bird/<Landing>c__IteratorB
struct U3CLandingU3Ec__IteratorB_t391;
// System.Object
struct Object_t;

// System.Void SCR_Bird/<Landing>c__IteratorB::.ctor()
extern "C" void U3CLandingU3Ec__IteratorB__ctor_m1510 (U3CLandingU3Ec__IteratorB_t391 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SCR_Bird/<Landing>c__IteratorB::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CLandingU3Ec__IteratorB_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1511 (U3CLandingU3Ec__IteratorB_t391 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SCR_Bird/<Landing>c__IteratorB::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CLandingU3Ec__IteratorB_System_Collections_IEnumerator_get_Current_m1512 (U3CLandingU3Ec__IteratorB_t391 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SCR_Bird/<Landing>c__IteratorB::MoveNext()
extern "C" bool U3CLandingU3Ec__IteratorB_MoveNext_m1513 (U3CLandingU3Ec__IteratorB_t391 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Bird/<Landing>c__IteratorB::Dispose()
extern "C" void U3CLandingU3Ec__IteratorB_Dispose_m1514 (U3CLandingU3Ec__IteratorB_t391 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Bird/<Landing>c__IteratorB::Reset()
extern "C" void U3CLandingU3Ec__IteratorB_Reset_m1515 (U3CLandingU3Ec__IteratorB_t391 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
