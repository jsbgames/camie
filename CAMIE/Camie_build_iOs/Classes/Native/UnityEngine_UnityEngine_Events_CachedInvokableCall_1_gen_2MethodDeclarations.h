﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Events.CachedInvokableCall`1<System.Boolean>
struct CachedInvokableCall_1_t1106;
// UnityEngine.Object
struct Object_t164;
struct Object_t164_marshaled;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Object[]
struct ObjectU5BU5D_t224;

// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Boolean>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// UnityEngine.Events.CachedInvokableCall`1<System.Byte>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_4MethodDeclarations.h"
#define CachedInvokableCall_1__ctor_m5243(__this, ___target, ___theFunction, ___argument, method) (( void (*) (CachedInvokableCall_1_t1106 *, Object_t164 *, MethodInfo_t *, bool, const MethodInfo*))CachedInvokableCall_1__ctor_m21543_gshared)(__this, ___target, ___theFunction, ___argument, method)
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Boolean>::Invoke(System.Object[])
#define CachedInvokableCall_1_Invoke_m21544(__this, ___args, method) (( void (*) (CachedInvokableCall_1_t1106 *, ObjectU5BU5D_t224*, const MethodInfo*))CachedInvokableCall_1_Invoke_m21545_gshared)(__this, ___args, method)
