﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Security.Cryptography.PKCS8/PrivateKeyInfo
struct PrivateKeyInfo_t1727;
// System.Byte[]
struct ByteU5BU5D_t850;
// System.Security.Cryptography.RSA
struct RSA_t1432;
// System.Security.Cryptography.DSA
struct DSA_t1431;
// System.Security.Cryptography.DSAParameters
#include "mscorlib_System_Security_Cryptography_DSAParameters.h"

// System.Void Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::.ctor()
extern "C" void PrivateKeyInfo__ctor_m8711 (PrivateKeyInfo_t1727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::.ctor(System.Byte[])
extern "C" void PrivateKeyInfo__ctor_m8712 (PrivateKeyInfo_t1727 * __this, ByteU5BU5D_t850* ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::get_PrivateKey()
extern "C" ByteU5BU5D_t850* PrivateKeyInfo_get_PrivateKey_m8713 (PrivateKeyInfo_t1727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::Decode(System.Byte[])
extern "C" void PrivateKeyInfo_Decode_m8714 (PrivateKeyInfo_t1727 * __this, ByteU5BU5D_t850* ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::RemoveLeadingZero(System.Byte[])
extern "C" ByteU5BU5D_t850* PrivateKeyInfo_RemoveLeadingZero_m8715 (Object_t * __this /* static, unused */, ByteU5BU5D_t850* ___bigInt, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::Normalize(System.Byte[],System.Int32)
extern "C" ByteU5BU5D_t850* PrivateKeyInfo_Normalize_m8716 (Object_t * __this /* static, unused */, ByteU5BU5D_t850* ___bigInt, int32_t ___length, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.RSA Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::DecodeRSA(System.Byte[])
extern "C" RSA_t1432 * PrivateKeyInfo_DecodeRSA_m8717 (Object_t * __this /* static, unused */, ByteU5BU5D_t850* ___keypair, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.DSA Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::DecodeDSA(System.Byte[],System.Security.Cryptography.DSAParameters)
extern "C" DSA_t1431 * PrivateKeyInfo_DecodeDSA_m8718 (Object_t * __this /* static, unused */, ByteU5BU5D_t850* ___privateKey, DSAParameters_t1453  ___dsaParameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
