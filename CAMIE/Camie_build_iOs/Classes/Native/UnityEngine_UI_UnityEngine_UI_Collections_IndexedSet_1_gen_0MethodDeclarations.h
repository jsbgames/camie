﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>
struct IndexedSet_1_t679;
// UnityEngine.UI.Graphic
struct Graphic_t418;
// System.Collections.IEnumerator
struct IEnumerator_t217;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Graphic>
struct IEnumerator_1_t3673;
// UnityEngine.UI.Graphic[]
struct GraphicU5BU5D_t3115;
// System.Predicate`1<UnityEngine.UI.Graphic>
struct Predicate_1_t3117;
// System.Comparison`1<UnityEngine.UI.Graphic>
struct Comparison_1_t527;

// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::.ctor()
// UnityEngine.UI.Collections.IndexedSet`1<System.Object>
#include "UnityEngine_UI_UnityEngine_UI_Collections_IndexedSet_1_gen_1MethodDeclarations.h"
#define IndexedSet_1__ctor_m3218(__this, method) (( void (*) (IndexedSet_1_t679 *, const MethodInfo*))IndexedSet_1__ctor_m17276_gshared)(__this, method)
// System.Collections.IEnumerator UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::System.Collections.IEnumerable.GetEnumerator()
#define IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m18064(__this, method) (( Object_t * (*) (IndexedSet_1_t679 *, const MethodInfo*))IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m17278_gshared)(__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Add(T)
#define IndexedSet_1_Add_m18065(__this, ___item, method) (( void (*) (IndexedSet_1_t679 *, Graphic_t418 *, const MethodInfo*))IndexedSet_1_Add_m17280_gshared)(__this, ___item, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Remove(T)
#define IndexedSet_1_Remove_m18066(__this, ___item, method) (( bool (*) (IndexedSet_1_t679 *, Graphic_t418 *, const MethodInfo*))IndexedSet_1_Remove_m17282_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<T> UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::GetEnumerator()
#define IndexedSet_1_GetEnumerator_m18067(__this, method) (( Object_t* (*) (IndexedSet_1_t679 *, const MethodInfo*))IndexedSet_1_GetEnumerator_m17284_gshared)(__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Clear()
#define IndexedSet_1_Clear_m18068(__this, method) (( void (*) (IndexedSet_1_t679 *, const MethodInfo*))IndexedSet_1_Clear_m17286_gshared)(__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Contains(T)
#define IndexedSet_1_Contains_m18069(__this, ___item, method) (( bool (*) (IndexedSet_1_t679 *, Graphic_t418 *, const MethodInfo*))IndexedSet_1_Contains_m17288_gshared)(__this, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::CopyTo(T[],System.Int32)
#define IndexedSet_1_CopyTo_m18070(__this, ___array, ___arrayIndex, method) (( void (*) (IndexedSet_1_t679 *, GraphicU5BU5D_t3115*, int32_t, const MethodInfo*))IndexedSet_1_CopyTo_m17290_gshared)(__this, ___array, ___arrayIndex, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::get_Count()
#define IndexedSet_1_get_Count_m18071(__this, method) (( int32_t (*) (IndexedSet_1_t679 *, const MethodInfo*))IndexedSet_1_get_Count_m17292_gshared)(__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::get_IsReadOnly()
#define IndexedSet_1_get_IsReadOnly_m18072(__this, method) (( bool (*) (IndexedSet_1_t679 *, const MethodInfo*))IndexedSet_1_get_IsReadOnly_m17294_gshared)(__this, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::IndexOf(T)
#define IndexedSet_1_IndexOf_m18073(__this, ___item, method) (( int32_t (*) (IndexedSet_1_t679 *, Graphic_t418 *, const MethodInfo*))IndexedSet_1_IndexOf_m17296_gshared)(__this, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Insert(System.Int32,T)
#define IndexedSet_1_Insert_m18074(__this, ___index, ___item, method) (( void (*) (IndexedSet_1_t679 *, int32_t, Graphic_t418 *, const MethodInfo*))IndexedSet_1_Insert_m17298_gshared)(__this, ___index, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::RemoveAt(System.Int32)
#define IndexedSet_1_RemoveAt_m18075(__this, ___index, method) (( void (*) (IndexedSet_1_t679 *, int32_t, const MethodInfo*))IndexedSet_1_RemoveAt_m17300_gshared)(__this, ___index, method)
// T UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::get_Item(System.Int32)
#define IndexedSet_1_get_Item_m18076(__this, ___index, method) (( Graphic_t418 * (*) (IndexedSet_1_t679 *, int32_t, const MethodInfo*))IndexedSet_1_get_Item_m17302_gshared)(__this, ___index, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::set_Item(System.Int32,T)
#define IndexedSet_1_set_Item_m18077(__this, ___index, ___value, method) (( void (*) (IndexedSet_1_t679 *, int32_t, Graphic_t418 *, const MethodInfo*))IndexedSet_1_set_Item_m17304_gshared)(__this, ___index, ___value, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::RemoveAll(System.Predicate`1<T>)
#define IndexedSet_1_RemoveAll_m18078(__this, ___match, method) (( void (*) (IndexedSet_1_t679 *, Predicate_1_t3117 *, const MethodInfo*))IndexedSet_1_RemoveAll_m17305_gshared)(__this, ___match, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Sort(System.Comparison`1<T>)
#define IndexedSet_1_Sort_m18079(__this, ___sortLayoutFunction, method) (( void (*) (IndexedSet_1_t679 *, Comparison_1_t527 *, const MethodInfo*))IndexedSet_1_Sort_m17306_gshared)(__this, ___sortLayoutFunction, method)
