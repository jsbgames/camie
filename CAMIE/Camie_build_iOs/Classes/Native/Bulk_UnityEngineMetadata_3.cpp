﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "stringLiterals.h"
// UnityEngine.RangeAttribute
#include "UnityEngine_UnityEngine_RangeAttribute.h"
// Metadata Definition UnityEngine.RangeAttribute
extern TypeInfo RangeAttribute_t261_il2cpp_TypeInfo;
// UnityEngine.RangeAttribute
#include "UnityEngine_UnityEngine_RangeAttributeMethodDeclarations.h"
extern const Il2CppType Single_t254_0_0_0;
extern const Il2CppType Single_t254_0_0_0;
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo RangeAttribute_t261_RangeAttribute__ctor_m1025_ParameterInfos[] = 
{
	{"min", 0, 134220179, 0, &Single_t254_0_0_0},
	{"max", 1, 134220180, 0, &Single_t254_0_0_0},
};
extern const Il2CppType Void_t272_0_0_0;
extern void* RuntimeInvoker_Void_t272_Single_t254_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.RangeAttribute::.ctor(System.Single,System.Single)
extern const MethodInfo RangeAttribute__ctor_m1025_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RangeAttribute__ctor_m1025/* method */
	, &RangeAttribute_t261_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Single_t254_Single_t254/* invoker_method */
	, RangeAttribute_t261_RangeAttribute__ctor_m1025_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2117/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* RangeAttribute_t261_MethodInfos[] =
{
	&RangeAttribute__ctor_m1025_MethodInfo,
	NULL
};
extern const MethodInfo Attribute_Equals_m5384_MethodInfo;
extern const MethodInfo Object_Finalize_m1048_MethodInfo;
extern const MethodInfo Attribute_GetHashCode_m5253_MethodInfo;
extern const MethodInfo Object_ToString_m1075_MethodInfo;
static const Il2CppMethodReference RangeAttribute_t261_VTable[] =
{
	&Attribute_Equals_m5384_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Attribute_GetHashCode_m5253_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool RangeAttribute_t261_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern const Il2CppType _Attribute_t1145_0_0_0;
static Il2CppInterfaceOffsetPair RangeAttribute_t261_InterfacesOffsets[] = 
{
	{ &_Attribute_t1145_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType RangeAttribute_t261_0_0_0;
extern const Il2CppType RangeAttribute_t261_1_0_0;
extern const Il2CppType PropertyAttribute_t990_0_0_0;
struct RangeAttribute_t261;
const Il2CppTypeDefinitionMetadata RangeAttribute_t261_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RangeAttribute_t261_InterfacesOffsets/* interfaceOffsets */
	, &PropertyAttribute_t990_0_0_0/* parent */
	, RangeAttribute_t261_VTable/* vtableMethods */
	, RangeAttribute_t261_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1320/* fieldStart */

};
TypeInfo RangeAttribute_t261_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "RangeAttribute"/* name */
	, "UnityEngine"/* namespaze */
	, RangeAttribute_t261_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &RangeAttribute_t261_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1036/* custom_attributes_cache */
	, &RangeAttribute_t261_0_0_0/* byval_arg */
	, &RangeAttribute_t261_1_0_0/* this_arg */
	, &RangeAttribute_t261_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RangeAttribute_t261)/* instance_size */
	, sizeof (RangeAttribute_t261)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.TextAreaAttribute
#include "UnityEngine_UnityEngine_TextAreaAttribute.h"
// Metadata Definition UnityEngine.TextAreaAttribute
extern TypeInfo TextAreaAttribute_t728_il2cpp_TypeInfo;
// UnityEngine.TextAreaAttribute
#include "UnityEngine_UnityEngine_TextAreaAttributeMethodDeclarations.h"
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo TextAreaAttribute_t728_TextAreaAttribute__ctor_m3537_ParameterInfos[] = 
{
	{"minLines", 0, 134220181, 0, &Int32_t253_0_0_0},
	{"maxLines", 1, 134220182, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.TextAreaAttribute::.ctor(System.Int32,System.Int32)
extern const MethodInfo TextAreaAttribute__ctor_m3537_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TextAreaAttribute__ctor_m3537/* method */
	, &TextAreaAttribute_t728_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253_Int32_t253/* invoker_method */
	, TextAreaAttribute_t728_TextAreaAttribute__ctor_m3537_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2118/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TextAreaAttribute_t728_MethodInfos[] =
{
	&TextAreaAttribute__ctor_m3537_MethodInfo,
	NULL
};
static const Il2CppMethodReference TextAreaAttribute_t728_VTable[] =
{
	&Attribute_Equals_m5384_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Attribute_GetHashCode_m5253_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool TextAreaAttribute_t728_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TextAreaAttribute_t728_InterfacesOffsets[] = 
{
	{ &_Attribute_t1145_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TextAreaAttribute_t728_0_0_0;
extern const Il2CppType TextAreaAttribute_t728_1_0_0;
struct TextAreaAttribute_t728;
const Il2CppTypeDefinitionMetadata TextAreaAttribute_t728_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TextAreaAttribute_t728_InterfacesOffsets/* interfaceOffsets */
	, &PropertyAttribute_t990_0_0_0/* parent */
	, TextAreaAttribute_t728_VTable/* vtableMethods */
	, TextAreaAttribute_t728_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1322/* fieldStart */

};
TypeInfo TextAreaAttribute_t728_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TextAreaAttribute"/* name */
	, "UnityEngine"/* namespaze */
	, TextAreaAttribute_t728_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TextAreaAttribute_t728_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1037/* custom_attributes_cache */
	, &TextAreaAttribute_t728_0_0_0/* byval_arg */
	, &TextAreaAttribute_t728_1_0_0/* this_arg */
	, &TextAreaAttribute_t728_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TextAreaAttribute_t728)/* instance_size */
	, sizeof (TextAreaAttribute_t728)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SelectionBaseAttribute
#include "UnityEngine_UnityEngine_SelectionBaseAttribute.h"
// Metadata Definition UnityEngine.SelectionBaseAttribute
extern TypeInfo SelectionBaseAttribute_t727_il2cpp_TypeInfo;
// UnityEngine.SelectionBaseAttribute
#include "UnityEngine_UnityEngine_SelectionBaseAttributeMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SelectionBaseAttribute::.ctor()
extern const MethodInfo SelectionBaseAttribute__ctor_m3534_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SelectionBaseAttribute__ctor_m3534/* method */
	, &SelectionBaseAttribute_t727_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2119/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SelectionBaseAttribute_t727_MethodInfos[] =
{
	&SelectionBaseAttribute__ctor_m3534_MethodInfo,
	NULL
};
static const Il2CppMethodReference SelectionBaseAttribute_t727_VTable[] =
{
	&Attribute_Equals_m5384_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Attribute_GetHashCode_m5253_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool SelectionBaseAttribute_t727_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair SelectionBaseAttribute_t727_InterfacesOffsets[] = 
{
	{ &_Attribute_t1145_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType SelectionBaseAttribute_t727_0_0_0;
extern const Il2CppType SelectionBaseAttribute_t727_1_0_0;
extern const Il2CppType Attribute_t805_0_0_0;
struct SelectionBaseAttribute_t727;
const Il2CppTypeDefinitionMetadata SelectionBaseAttribute_t727_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SelectionBaseAttribute_t727_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t805_0_0_0/* parent */
	, SelectionBaseAttribute_t727_VTable/* vtableMethods */
	, SelectionBaseAttribute_t727_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo SelectionBaseAttribute_t727_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SelectionBaseAttribute"/* name */
	, "UnityEngine"/* namespaze */
	, SelectionBaseAttribute_t727_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SelectionBaseAttribute_t727_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1038/* custom_attributes_cache */
	, &SelectionBaseAttribute_t727_0_0_0/* byval_arg */
	, &SelectionBaseAttribute_t727_1_0_0/* this_arg */
	, &SelectionBaseAttribute_t727_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SelectionBaseAttribute_t727)/* instance_size */
	, sizeof (SelectionBaseAttribute_t727)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SliderState
#include "UnityEngine_UnityEngine_SliderState.h"
// Metadata Definition UnityEngine.SliderState
extern TypeInfo SliderState_t991_il2cpp_TypeInfo;
// UnityEngine.SliderState
#include "UnityEngine_UnityEngine_SliderStateMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SliderState::.ctor()
extern const MethodInfo SliderState__ctor_m4964_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SliderState__ctor_m4964/* method */
	, &SliderState_t991_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2120/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SliderState_t991_MethodInfos[] =
{
	&SliderState__ctor_m4964_MethodInfo,
	NULL
};
extern const MethodInfo Object_Equals_m1073_MethodInfo;
extern const MethodInfo Object_GetHashCode_m1074_MethodInfo;
static const Il2CppMethodReference SliderState_t991_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool SliderState_t991_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType SliderState_t991_0_0_0;
extern const Il2CppType SliderState_t991_1_0_0;
extern const Il2CppType Object_t_0_0_0;
struct SliderState_t991;
const Il2CppTypeDefinitionMetadata SliderState_t991_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SliderState_t991_VTable/* vtableMethods */
	, SliderState_t991_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1324/* fieldStart */

};
TypeInfo SliderState_t991_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SliderState"/* name */
	, "UnityEngine"/* namespaze */
	, SliderState_t991_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SliderState_t991_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SliderState_t991_0_0_0/* byval_arg */
	, &SliderState_t991_1_0_0/* this_arg */
	, &SliderState_t991_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SliderState_t991)/* instance_size */
	, sizeof (SliderState_t991)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.SliderHandler
#include "UnityEngine_UnityEngine_SliderHandler.h"
// Metadata Definition UnityEngine.SliderHandler
extern TypeInfo SliderHandler_t992_il2cpp_TypeInfo;
// UnityEngine.SliderHandler
#include "UnityEngine_UnityEngine_SliderHandlerMethodDeclarations.h"
extern const Il2CppType Rect_t304_0_0_0;
extern const Il2CppType Rect_t304_0_0_0;
extern const Il2CppType Single_t254_0_0_0;
extern const Il2CppType Single_t254_0_0_0;
extern const Il2CppType Single_t254_0_0_0;
extern const Il2CppType Single_t254_0_0_0;
extern const Il2CppType GUIStyle_t302_0_0_0;
extern const Il2CppType GUIStyle_t302_0_0_0;
extern const Il2CppType GUIStyle_t302_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo SliderHandler_t992_SliderHandler__ctor_m4965_ParameterInfos[] = 
{
	{"position", 0, 134220183, 0, &Rect_t304_0_0_0},
	{"currentValue", 1, 134220184, 0, &Single_t254_0_0_0},
	{"size", 2, 134220185, 0, &Single_t254_0_0_0},
	{"start", 3, 134220186, 0, &Single_t254_0_0_0},
	{"end", 4, 134220187, 0, &Single_t254_0_0_0},
	{"slider", 5, 134220188, 0, &GUIStyle_t302_0_0_0},
	{"thumb", 6, 134220189, 0, &GUIStyle_t302_0_0_0},
	{"horiz", 7, 134220190, 0, &Boolean_t273_0_0_0},
	{"id", 8, 134220191, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Rect_t304_Single_t254_Single_t254_Single_t254_Single_t254_Object_t_Object_t_SByte_t274_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SliderHandler::.ctor(UnityEngine.Rect,System.Single,System.Single,System.Single,System.Single,UnityEngine.GUIStyle,UnityEngine.GUIStyle,System.Boolean,System.Int32)
extern const MethodInfo SliderHandler__ctor_m4965_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SliderHandler__ctor_m4965/* method */
	, &SliderHandler_t992_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Rect_t304_Single_t254_Single_t254_Single_t254_Single_t254_Object_t_Object_t_SByte_t274_Int32_t253/* invoker_method */
	, SliderHandler_t992_SliderHandler__ctor_m4965_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 9/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2121/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.SliderHandler::Handle()
extern const MethodInfo SliderHandler_Handle_m4966_MethodInfo = 
{
	"Handle"/* name */
	, (methodPointerType)&SliderHandler_Handle_m4966/* method */
	, &SliderHandler_t992_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2122/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.SliderHandler::OnMouseDown()
extern const MethodInfo SliderHandler_OnMouseDown_m4967_MethodInfo = 
{
	"OnMouseDown"/* name */
	, (methodPointerType)&SliderHandler_OnMouseDown_m4967/* method */
	, &SliderHandler_t992_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2123/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.SliderHandler::OnMouseDrag()
extern const MethodInfo SliderHandler_OnMouseDrag_m4968_MethodInfo = 
{
	"OnMouseDrag"/* name */
	, (methodPointerType)&SliderHandler_OnMouseDrag_m4968/* method */
	, &SliderHandler_t992_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2124/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.SliderHandler::OnMouseUp()
extern const MethodInfo SliderHandler_OnMouseUp_m4969_MethodInfo = 
{
	"OnMouseUp"/* name */
	, (methodPointerType)&SliderHandler_OnMouseUp_m4969/* method */
	, &SliderHandler_t992_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2125/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.SliderHandler::OnRepaint()
extern const MethodInfo SliderHandler_OnRepaint_m4970_MethodInfo = 
{
	"OnRepaint"/* name */
	, (methodPointerType)&SliderHandler_OnRepaint_m4970/* method */
	, &SliderHandler_t992_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2126/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType EventType_t837_0_0_0;
extern void* RuntimeInvoker_EventType_t837 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.EventType UnityEngine.SliderHandler::CurrentEventType()
extern const MethodInfo SliderHandler_CurrentEventType_m4971_MethodInfo = 
{
	"CurrentEventType"/* name */
	, (methodPointerType)&SliderHandler_CurrentEventType_m4971/* method */
	, &SliderHandler_t992_il2cpp_TypeInfo/* declaring_type */
	, &EventType_t837_0_0_0/* return_type */
	, RuntimeInvoker_EventType_t837/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2127/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.SliderHandler::CurrentScrollTroughSide()
extern const MethodInfo SliderHandler_CurrentScrollTroughSide_m4972_MethodInfo = 
{
	"CurrentScrollTroughSide"/* name */
	, (methodPointerType)&SliderHandler_CurrentScrollTroughSide_m4972/* method */
	, &SliderHandler_t992_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2128/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.SliderHandler::IsEmptySlider()
extern const MethodInfo SliderHandler_IsEmptySlider_m4973_MethodInfo = 
{
	"IsEmptySlider"/* name */
	, (methodPointerType)&SliderHandler_IsEmptySlider_m4973/* method */
	, &SliderHandler_t992_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2129/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.SliderHandler::SupportsPageMovements()
extern const MethodInfo SliderHandler_SupportsPageMovements_m4974_MethodInfo = 
{
	"SupportsPageMovements"/* name */
	, (methodPointerType)&SliderHandler_SupportsPageMovements_m4974/* method */
	, &SliderHandler_t992_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2130/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.SliderHandler::PageMovementValue()
extern const MethodInfo SliderHandler_PageMovementValue_m4975_MethodInfo = 
{
	"PageMovementValue"/* name */
	, (methodPointerType)&SliderHandler_PageMovementValue_m4975/* method */
	, &SliderHandler_t992_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2131/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.SliderHandler::PageUpMovementBound()
extern const MethodInfo SliderHandler_PageUpMovementBound_m4976_MethodInfo = 
{
	"PageUpMovementBound"/* name */
	, (methodPointerType)&SliderHandler_PageUpMovementBound_m4976/* method */
	, &SliderHandler_t992_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2132/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Event_t560_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Event UnityEngine.SliderHandler::CurrentEvent()
extern const MethodInfo SliderHandler_CurrentEvent_m4977_MethodInfo = 
{
	"CurrentEvent"/* name */
	, (methodPointerType)&SliderHandler_CurrentEvent_m4977/* method */
	, &SliderHandler_t992_il2cpp_TypeInfo/* declaring_type */
	, &Event_t560_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2133/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.SliderHandler::ValueForCurrentMousePosition()
extern const MethodInfo SliderHandler_ValueForCurrentMousePosition_m4978_MethodInfo = 
{
	"ValueForCurrentMousePosition"/* name */
	, (methodPointerType)&SliderHandler_ValueForCurrentMousePosition_m4978/* method */
	, &SliderHandler_t992_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2134/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo SliderHandler_t992_SliderHandler_Clamp_m4979_ParameterInfos[] = 
{
	{"value", 0, 134220192, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Single_t254_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.SliderHandler::Clamp(System.Single)
extern const MethodInfo SliderHandler_Clamp_m4979_MethodInfo = 
{
	"Clamp"/* name */
	, (methodPointerType)&SliderHandler_Clamp_m4979/* method */
	, &SliderHandler_t992_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254_Single_t254/* invoker_method */
	, SliderHandler_t992_SliderHandler_Clamp_m4979_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2135/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Rect_t304 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Rect UnityEngine.SliderHandler::ThumbSelectionRect()
extern const MethodInfo SliderHandler_ThumbSelectionRect_m4980_MethodInfo = 
{
	"ThumbSelectionRect"/* name */
	, (methodPointerType)&SliderHandler_ThumbSelectionRect_m4980/* method */
	, &SliderHandler_t992_il2cpp_TypeInfo/* declaring_type */
	, &Rect_t304_0_0_0/* return_type */
	, RuntimeInvoker_Rect_t304/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2136/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo SliderHandler_t992_SliderHandler_StartDraggingWithValue_m4981_ParameterInfos[] = 
{
	{"dragStartValue", 0, 134220193, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SliderHandler::StartDraggingWithValue(System.Single)
extern const MethodInfo SliderHandler_StartDraggingWithValue_m4981_MethodInfo = 
{
	"StartDraggingWithValue"/* name */
	, (methodPointerType)&SliderHandler_StartDraggingWithValue_m4981/* method */
	, &SliderHandler_t992_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Single_t254/* invoker_method */
	, SliderHandler_t992_SliderHandler_StartDraggingWithValue_m4981_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2137/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.SliderState UnityEngine.SliderHandler::SliderState()
extern const MethodInfo SliderHandler_SliderState_m4982_MethodInfo = 
{
	"SliderState"/* name */
	, (methodPointerType)&SliderHandler_SliderState_m4982/* method */
	, &SliderHandler_t992_il2cpp_TypeInfo/* declaring_type */
	, &SliderState_t991_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2138/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Rect_t304 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Rect UnityEngine.SliderHandler::ThumbRect()
extern const MethodInfo SliderHandler_ThumbRect_m4983_MethodInfo = 
{
	"ThumbRect"/* name */
	, (methodPointerType)&SliderHandler_ThumbRect_m4983/* method */
	, &SliderHandler_t992_il2cpp_TypeInfo/* declaring_type */
	, &Rect_t304_0_0_0/* return_type */
	, RuntimeInvoker_Rect_t304/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2139/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Rect_t304 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Rect UnityEngine.SliderHandler::VerticalThumbRect()
extern const MethodInfo SliderHandler_VerticalThumbRect_m4984_MethodInfo = 
{
	"VerticalThumbRect"/* name */
	, (methodPointerType)&SliderHandler_VerticalThumbRect_m4984/* method */
	, &SliderHandler_t992_il2cpp_TypeInfo/* declaring_type */
	, &Rect_t304_0_0_0/* return_type */
	, RuntimeInvoker_Rect_t304/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2140/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Rect_t304 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Rect UnityEngine.SliderHandler::HorizontalThumbRect()
extern const MethodInfo SliderHandler_HorizontalThumbRect_m4985_MethodInfo = 
{
	"HorizontalThumbRect"/* name */
	, (methodPointerType)&SliderHandler_HorizontalThumbRect_m4985/* method */
	, &SliderHandler_t992_il2cpp_TypeInfo/* declaring_type */
	, &Rect_t304_0_0_0/* return_type */
	, RuntimeInvoker_Rect_t304/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2141/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.SliderHandler::ClampedCurrentValue()
extern const MethodInfo SliderHandler_ClampedCurrentValue_m4986_MethodInfo = 
{
	"ClampedCurrentValue"/* name */
	, (methodPointerType)&SliderHandler_ClampedCurrentValue_m4986/* method */
	, &SliderHandler_t992_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2142/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.SliderHandler::MousePosition()
extern const MethodInfo SliderHandler_MousePosition_m4987_MethodInfo = 
{
	"MousePosition"/* name */
	, (methodPointerType)&SliderHandler_MousePosition_m4987/* method */
	, &SliderHandler_t992_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2143/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.SliderHandler::ValuesPerPixel()
extern const MethodInfo SliderHandler_ValuesPerPixel_m4988_MethodInfo = 
{
	"ValuesPerPixel"/* name */
	, (methodPointerType)&SliderHandler_ValuesPerPixel_m4988/* method */
	, &SliderHandler_t992_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2144/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.SliderHandler::ThumbSize()
extern const MethodInfo SliderHandler_ThumbSize_m4989_MethodInfo = 
{
	"ThumbSize"/* name */
	, (methodPointerType)&SliderHandler_ThumbSize_m4989/* method */
	, &SliderHandler_t992_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2145/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.SliderHandler::MaxValue()
extern const MethodInfo SliderHandler_MaxValue_m4990_MethodInfo = 
{
	"MaxValue"/* name */
	, (methodPointerType)&SliderHandler_MaxValue_m4990/* method */
	, &SliderHandler_t992_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2146/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.SliderHandler::MinValue()
extern const MethodInfo SliderHandler_MinValue_m4991_MethodInfo = 
{
	"MinValue"/* name */
	, (methodPointerType)&SliderHandler_MinValue_m4991/* method */
	, &SliderHandler_t992_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2147/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SliderHandler_t992_MethodInfos[] =
{
	&SliderHandler__ctor_m4965_MethodInfo,
	&SliderHandler_Handle_m4966_MethodInfo,
	&SliderHandler_OnMouseDown_m4967_MethodInfo,
	&SliderHandler_OnMouseDrag_m4968_MethodInfo,
	&SliderHandler_OnMouseUp_m4969_MethodInfo,
	&SliderHandler_OnRepaint_m4970_MethodInfo,
	&SliderHandler_CurrentEventType_m4971_MethodInfo,
	&SliderHandler_CurrentScrollTroughSide_m4972_MethodInfo,
	&SliderHandler_IsEmptySlider_m4973_MethodInfo,
	&SliderHandler_SupportsPageMovements_m4974_MethodInfo,
	&SliderHandler_PageMovementValue_m4975_MethodInfo,
	&SliderHandler_PageUpMovementBound_m4976_MethodInfo,
	&SliderHandler_CurrentEvent_m4977_MethodInfo,
	&SliderHandler_ValueForCurrentMousePosition_m4978_MethodInfo,
	&SliderHandler_Clamp_m4979_MethodInfo,
	&SliderHandler_ThumbSelectionRect_m4980_MethodInfo,
	&SliderHandler_StartDraggingWithValue_m4981_MethodInfo,
	&SliderHandler_SliderState_m4982_MethodInfo,
	&SliderHandler_ThumbRect_m4983_MethodInfo,
	&SliderHandler_VerticalThumbRect_m4984_MethodInfo,
	&SliderHandler_HorizontalThumbRect_m4985_MethodInfo,
	&SliderHandler_ClampedCurrentValue_m4986_MethodInfo,
	&SliderHandler_MousePosition_m4987_MethodInfo,
	&SliderHandler_ValuesPerPixel_m4988_MethodInfo,
	&SliderHandler_ThumbSize_m4989_MethodInfo,
	&SliderHandler_MaxValue_m4990_MethodInfo,
	&SliderHandler_MinValue_m4991_MethodInfo,
	NULL
};
extern const MethodInfo ValueType_Equals_m1076_MethodInfo;
extern const MethodInfo ValueType_GetHashCode_m1077_MethodInfo;
extern const MethodInfo ValueType_ToString_m1078_MethodInfo;
static const Il2CppMethodReference SliderHandler_t992_VTable[] =
{
	&ValueType_Equals_m1076_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&ValueType_GetHashCode_m1077_MethodInfo,
	&ValueType_ToString_m1078_MethodInfo,
};
static bool SliderHandler_t992_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType SliderHandler_t992_0_0_0;
extern const Il2CppType SliderHandler_t992_1_0_0;
extern const Il2CppType ValueType_t285_0_0_0;
const Il2CppTypeDefinitionMetadata SliderHandler_t992_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t285_0_0_0/* parent */
	, SliderHandler_t992_VTable/* vtableMethods */
	, SliderHandler_t992_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1327/* fieldStart */

};
TypeInfo SliderHandler_t992_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SliderHandler"/* name */
	, "UnityEngine"/* namespaze */
	, SliderHandler_t992_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SliderHandler_t992_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SliderHandler_t992_0_0_0/* byval_arg */
	, &SliderHandler_t992_1_0_0/* this_arg */
	, &SliderHandler_t992_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SliderHandler_t992)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (SliderHandler_t992)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 27/* method_count */
	, 0/* property_count */
	, 9/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.StackTraceUtility
#include "UnityEngine_UnityEngine_StackTraceUtility.h"
// Metadata Definition UnityEngine.StackTraceUtility
extern TypeInfo StackTraceUtility_t993_il2cpp_TypeInfo;
// UnityEngine.StackTraceUtility
#include "UnityEngine_UnityEngine_StackTraceUtilityMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StackTraceUtility::.ctor()
extern const MethodInfo StackTraceUtility__ctor_m4992_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&StackTraceUtility__ctor_m4992/* method */
	, &StackTraceUtility_t993_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2148/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StackTraceUtility::.cctor()
extern const MethodInfo StackTraceUtility__cctor_m4993_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&StackTraceUtility__cctor_m4993/* method */
	, &StackTraceUtility_t993_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2149/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo StackTraceUtility_t993_StackTraceUtility_SetProjectFolder_m4994_ParameterInfos[] = 
{
	{"folder", 0, 134220194, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StackTraceUtility::SetProjectFolder(System.String)
extern const MethodInfo StackTraceUtility_SetProjectFolder_m4994_MethodInfo = 
{
	"SetProjectFolder"/* name */
	, (methodPointerType)&StackTraceUtility_SetProjectFolder_m4994/* method */
	, &StackTraceUtility_t993_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, StackTraceUtility_t993_StackTraceUtility_SetProjectFolder_m4994_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2150/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.StackTraceUtility::ExtractStackTrace()
extern const MethodInfo StackTraceUtility_ExtractStackTrace_m4995_MethodInfo = 
{
	"ExtractStackTrace"/* name */
	, (methodPointerType)&StackTraceUtility_ExtractStackTrace_m4995/* method */
	, &StackTraceUtility_t993_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 1039/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2151/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo StackTraceUtility_t993_StackTraceUtility_IsSystemStacktraceType_m4996_ParameterInfos[] = 
{
	{"name", 0, 134220195, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.StackTraceUtility::IsSystemStacktraceType(System.Object)
extern const MethodInfo StackTraceUtility_IsSystemStacktraceType_m4996_MethodInfo = 
{
	"IsSystemStacktraceType"/* name */
	, (methodPointerType)&StackTraceUtility_IsSystemStacktraceType_m4996/* method */
	, &StackTraceUtility_t993_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, StackTraceUtility_t993_StackTraceUtility_IsSystemStacktraceType_m4996_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2152/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo StackTraceUtility_t993_StackTraceUtility_ExtractStringFromException_m4997_ParameterInfos[] = 
{
	{"exception", 0, 134220196, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.StackTraceUtility::ExtractStringFromException(System.Object)
extern const MethodInfo StackTraceUtility_ExtractStringFromException_m4997_MethodInfo = 
{
	"ExtractStringFromException"/* name */
	, (methodPointerType)&StackTraceUtility_ExtractStringFromException_m4997/* method */
	, &StackTraceUtility_t993_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, StackTraceUtility_t993_StackTraceUtility_ExtractStringFromException_m4997_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2153/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType String_t_1_0_2;
extern const Il2CppType String_t_1_0_0;
extern const Il2CppType String_t_1_0_2;
static const ParameterInfo StackTraceUtility_t993_StackTraceUtility_ExtractStringFromExceptionInternal_m4998_ParameterInfos[] = 
{
	{"exceptiono", 0, 134220197, 0, &Object_t_0_0_0},
	{"message", 1, 134220198, 0, &String_t_1_0_2},
	{"stackTrace", 2, 134220199, 0, &String_t_1_0_2},
};
extern void* RuntimeInvoker_Void_t272_Object_t_StringU26_t1216_StringU26_t1216 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StackTraceUtility::ExtractStringFromExceptionInternal(System.Object,System.String&,System.String&)
extern const MethodInfo StackTraceUtility_ExtractStringFromExceptionInternal_m4998_MethodInfo = 
{
	"ExtractStringFromExceptionInternal"/* name */
	, (methodPointerType)&StackTraceUtility_ExtractStringFromExceptionInternal_m4998/* method */
	, &StackTraceUtility_t993_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_StringU26_t1216_StringU26_t1216/* invoker_method */
	, StackTraceUtility_t993_StackTraceUtility_ExtractStringFromExceptionInternal_m4998_ParameterInfos/* parameters */
	, 1040/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2154/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo StackTraceUtility_t993_StackTraceUtility_PostprocessStacktrace_m4999_ParameterInfos[] = 
{
	{"oldString", 0, 134220200, 0, &String_t_0_0_0},
	{"stripEngineInternalInformation", 1, 134220201, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.StackTraceUtility::PostprocessStacktrace(System.String,System.Boolean)
extern const MethodInfo StackTraceUtility_PostprocessStacktrace_m4999_MethodInfo = 
{
	"PostprocessStacktrace"/* name */
	, (methodPointerType)&StackTraceUtility_PostprocessStacktrace_m4999/* method */
	, &StackTraceUtility_t993_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_SByte_t274/* invoker_method */
	, StackTraceUtility_t993_StackTraceUtility_PostprocessStacktrace_m4999_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2155/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StackTrace_t1042_0_0_0;
extern const Il2CppType StackTrace_t1042_0_0_0;
static const ParameterInfo StackTraceUtility_t993_StackTraceUtility_ExtractFormattedStackTrace_m5000_ParameterInfos[] = 
{
	{"stackTrace", 0, 134220202, 0, &StackTrace_t1042_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.StackTraceUtility::ExtractFormattedStackTrace(System.Diagnostics.StackTrace)
extern const MethodInfo StackTraceUtility_ExtractFormattedStackTrace_m5000_MethodInfo = 
{
	"ExtractFormattedStackTrace"/* name */
	, (methodPointerType)&StackTraceUtility_ExtractFormattedStackTrace_m5000/* method */
	, &StackTraceUtility_t993_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, StackTraceUtility_t993_StackTraceUtility_ExtractFormattedStackTrace_m5000_ParameterInfos/* parameters */
	, 1041/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2156/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* StackTraceUtility_t993_MethodInfos[] =
{
	&StackTraceUtility__ctor_m4992_MethodInfo,
	&StackTraceUtility__cctor_m4993_MethodInfo,
	&StackTraceUtility_SetProjectFolder_m4994_MethodInfo,
	&StackTraceUtility_ExtractStackTrace_m4995_MethodInfo,
	&StackTraceUtility_IsSystemStacktraceType_m4996_MethodInfo,
	&StackTraceUtility_ExtractStringFromException_m4997_MethodInfo,
	&StackTraceUtility_ExtractStringFromExceptionInternal_m4998_MethodInfo,
	&StackTraceUtility_PostprocessStacktrace_m4999_MethodInfo,
	&StackTraceUtility_ExtractFormattedStackTrace_m5000_MethodInfo,
	NULL
};
static const Il2CppMethodReference StackTraceUtility_t993_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool StackTraceUtility_t993_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType StackTraceUtility_t993_0_0_0;
extern const Il2CppType StackTraceUtility_t993_1_0_0;
struct StackTraceUtility_t993;
const Il2CppTypeDefinitionMetadata StackTraceUtility_t993_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, StackTraceUtility_t993_VTable/* vtableMethods */
	, StackTraceUtility_t993_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1336/* fieldStart */

};
TypeInfo StackTraceUtility_t993_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "StackTraceUtility"/* name */
	, "UnityEngine"/* namespaze */
	, StackTraceUtility_t993_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &StackTraceUtility_t993_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &StackTraceUtility_t993_0_0_0/* byval_arg */
	, &StackTraceUtility_t993_1_0_0/* this_arg */
	, &StackTraceUtility_t993_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StackTraceUtility_t993)/* instance_size */
	, sizeof (StackTraceUtility_t993)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(StackTraceUtility_t993_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UnityException
#include "UnityEngine_UnityEngine_UnityException.h"
// Metadata Definition UnityEngine.UnityException
extern TypeInfo UnityException_t681_il2cpp_TypeInfo;
// UnityEngine.UnityException
#include "UnityEngine_UnityEngine_UnityExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UnityException::.ctor()
extern const MethodInfo UnityException__ctor_m5001_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityException__ctor_m5001/* method */
	, &UnityException_t681_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2157/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo UnityException_t681_UnityException__ctor_m5002_ParameterInfos[] = 
{
	{"message", 0, 134220203, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UnityException::.ctor(System.String)
extern const MethodInfo UnityException__ctor_m5002_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityException__ctor_m5002/* method */
	, &UnityException_t681_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, UnityException_t681_UnityException__ctor_m5002_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2158/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Exception_t232_0_0_0;
extern const Il2CppType Exception_t232_0_0_0;
static const ParameterInfo UnityException_t681_UnityException__ctor_m5003_ParameterInfos[] = 
{
	{"message", 0, 134220204, 0, &String_t_0_0_0},
	{"innerException", 1, 134220205, 0, &Exception_t232_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UnityException::.ctor(System.String,System.Exception)
extern const MethodInfo UnityException__ctor_m5003_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityException__ctor_m5003/* method */
	, &UnityException_t681_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, UnityException_t681_UnityException__ctor_m5003_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2159/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1043_0_0_0;
extern const Il2CppType SerializationInfo_t1043_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
static const ParameterInfo UnityException_t681_UnityException__ctor_m5004_ParameterInfos[] = 
{
	{"info", 0, 134220206, 0, &SerializationInfo_t1043_0_0_0},
	{"context", 1, 134220207, 0, &StreamingContext_t1044_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UnityException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo UnityException__ctor_m5004_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityException__ctor_m5004/* method */
	, &UnityException_t681_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044/* invoker_method */
	, UnityException_t681_UnityException__ctor_m5004_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2160/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UnityException_t681_MethodInfos[] =
{
	&UnityException__ctor_m5001_MethodInfo,
	&UnityException__ctor_m5002_MethodInfo,
	&UnityException__ctor_m5003_MethodInfo,
	&UnityException__ctor_m5004_MethodInfo,
	NULL
};
extern const MethodInfo Exception_ToString_m5385_MethodInfo;
extern const MethodInfo Exception_GetObjectData_m5386_MethodInfo;
extern const MethodInfo Exception_get_InnerException_m5387_MethodInfo;
extern const MethodInfo Exception_get_Message_m5388_MethodInfo;
extern const MethodInfo Exception_get_Source_m5389_MethodInfo;
extern const MethodInfo Exception_get_StackTrace_m5390_MethodInfo;
extern const MethodInfo Exception_GetType_m5391_MethodInfo;
static const Il2CppMethodReference UnityException_t681_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Exception_ToString_m5385_MethodInfo,
	&Exception_GetObjectData_m5386_MethodInfo,
	&Exception_get_InnerException_m5387_MethodInfo,
	&Exception_get_Message_m5388_MethodInfo,
	&Exception_get_Source_m5389_MethodInfo,
	&Exception_get_StackTrace_m5390_MethodInfo,
	&Exception_GetObjectData_m5386_MethodInfo,
	&Exception_GetType_m5391_MethodInfo,
};
static bool UnityException_t681_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ISerializable_t439_0_0_0;
extern const Il2CppType _Exception_t1147_0_0_0;
static Il2CppInterfaceOffsetPair UnityException_t681_InterfacesOffsets[] = 
{
	{ &ISerializable_t439_0_0_0, 4},
	{ &_Exception_t1147_0_0_0, 5},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityException_t681_0_0_0;
extern const Il2CppType UnityException_t681_1_0_0;
struct UnityException_t681;
const Il2CppTypeDefinitionMetadata UnityException_t681_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityException_t681_InterfacesOffsets/* interfaceOffsets */
	, &Exception_t232_0_0_0/* parent */
	, UnityException_t681_VTable/* vtableMethods */
	, UnityException_t681_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1337/* fieldStart */

};
TypeInfo UnityException_t681_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityException"/* name */
	, "UnityEngine"/* namespaze */
	, UnityException_t681_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UnityException_t681_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityException_t681_0_0_0/* byval_arg */
	, &UnityException_t681_1_0_0/* this_arg */
	, &UnityException_t681_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityException_t681)/* instance_size */
	, sizeof (UnityException_t681)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.SharedBetweenAnimatorsAttribute
#include "UnityEngine_UnityEngine_SharedBetweenAnimatorsAttribute.h"
// Metadata Definition UnityEngine.SharedBetweenAnimatorsAttribute
extern TypeInfo SharedBetweenAnimatorsAttribute_t994_il2cpp_TypeInfo;
// UnityEngine.SharedBetweenAnimatorsAttribute
#include "UnityEngine_UnityEngine_SharedBetweenAnimatorsAttributeMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SharedBetweenAnimatorsAttribute::.ctor()
extern const MethodInfo SharedBetweenAnimatorsAttribute__ctor_m5005_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SharedBetweenAnimatorsAttribute__ctor_m5005/* method */
	, &SharedBetweenAnimatorsAttribute_t994_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2161/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SharedBetweenAnimatorsAttribute_t994_MethodInfos[] =
{
	&SharedBetweenAnimatorsAttribute__ctor_m5005_MethodInfo,
	NULL
};
static const Il2CppMethodReference SharedBetweenAnimatorsAttribute_t994_VTable[] =
{
	&Attribute_Equals_m5384_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Attribute_GetHashCode_m5253_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool SharedBetweenAnimatorsAttribute_t994_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair SharedBetweenAnimatorsAttribute_t994_InterfacesOffsets[] = 
{
	{ &_Attribute_t1145_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType SharedBetweenAnimatorsAttribute_t994_0_0_0;
extern const Il2CppType SharedBetweenAnimatorsAttribute_t994_1_0_0;
struct SharedBetweenAnimatorsAttribute_t994;
const Il2CppTypeDefinitionMetadata SharedBetweenAnimatorsAttribute_t994_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SharedBetweenAnimatorsAttribute_t994_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t805_0_0_0/* parent */
	, SharedBetweenAnimatorsAttribute_t994_VTable/* vtableMethods */
	, SharedBetweenAnimatorsAttribute_t994_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo SharedBetweenAnimatorsAttribute_t994_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SharedBetweenAnimatorsAttribute"/* name */
	, "UnityEngine"/* namespaze */
	, SharedBetweenAnimatorsAttribute_t994_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SharedBetweenAnimatorsAttribute_t994_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1042/* custom_attributes_cache */
	, &SharedBetweenAnimatorsAttribute_t994_0_0_0/* byval_arg */
	, &SharedBetweenAnimatorsAttribute_t994_1_0_0/* this_arg */
	, &SharedBetweenAnimatorsAttribute_t994_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SharedBetweenAnimatorsAttribute_t994)/* instance_size */
	, sizeof (SharedBetweenAnimatorsAttribute_t994)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.StateMachineBehaviour
#include "UnityEngine_UnityEngine_StateMachineBehaviour.h"
// Metadata Definition UnityEngine.StateMachineBehaviour
extern TypeInfo StateMachineBehaviour_t995_il2cpp_TypeInfo;
// UnityEngine.StateMachineBehaviour
#include "UnityEngine_UnityEngine_StateMachineBehaviourMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StateMachineBehaviour::.ctor()
extern const MethodInfo StateMachineBehaviour__ctor_m5006_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&StateMachineBehaviour__ctor_m5006/* method */
	, &StateMachineBehaviour_t995_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2162/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Animator_t9_0_0_0;
extern const Il2CppType Animator_t9_0_0_0;
extern const Il2CppType AnimatorStateInfo_t887_0_0_0;
extern const Il2CppType AnimatorStateInfo_t887_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo StateMachineBehaviour_t995_StateMachineBehaviour_OnStateEnter_m5007_ParameterInfos[] = 
{
	{"animator", 0, 134220208, 0, &Animator_t9_0_0_0},
	{"stateInfo", 1, 134220209, 0, &AnimatorStateInfo_t887_0_0_0},
	{"layerIndex", 2, 134220210, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_AnimatorStateInfo_t887_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StateMachineBehaviour::OnStateEnter(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern const MethodInfo StateMachineBehaviour_OnStateEnter_m5007_MethodInfo = 
{
	"OnStateEnter"/* name */
	, (methodPointerType)&StateMachineBehaviour_OnStateEnter_m5007/* method */
	, &StateMachineBehaviour_t995_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_AnimatorStateInfo_t887_Int32_t253/* invoker_method */
	, StateMachineBehaviour_t995_StateMachineBehaviour_OnStateEnter_m5007_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2163/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Animator_t9_0_0_0;
extern const Il2CppType AnimatorStateInfo_t887_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo StateMachineBehaviour_t995_StateMachineBehaviour_OnStateUpdate_m5008_ParameterInfos[] = 
{
	{"animator", 0, 134220211, 0, &Animator_t9_0_0_0},
	{"stateInfo", 1, 134220212, 0, &AnimatorStateInfo_t887_0_0_0},
	{"layerIndex", 2, 134220213, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_AnimatorStateInfo_t887_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StateMachineBehaviour::OnStateUpdate(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern const MethodInfo StateMachineBehaviour_OnStateUpdate_m5008_MethodInfo = 
{
	"OnStateUpdate"/* name */
	, (methodPointerType)&StateMachineBehaviour_OnStateUpdate_m5008/* method */
	, &StateMachineBehaviour_t995_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_AnimatorStateInfo_t887_Int32_t253/* invoker_method */
	, StateMachineBehaviour_t995_StateMachineBehaviour_OnStateUpdate_m5008_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2164/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Animator_t9_0_0_0;
extern const Il2CppType AnimatorStateInfo_t887_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo StateMachineBehaviour_t995_StateMachineBehaviour_OnStateExit_m5009_ParameterInfos[] = 
{
	{"animator", 0, 134220214, 0, &Animator_t9_0_0_0},
	{"stateInfo", 1, 134220215, 0, &AnimatorStateInfo_t887_0_0_0},
	{"layerIndex", 2, 134220216, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_AnimatorStateInfo_t887_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StateMachineBehaviour::OnStateExit(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern const MethodInfo StateMachineBehaviour_OnStateExit_m5009_MethodInfo = 
{
	"OnStateExit"/* name */
	, (methodPointerType)&StateMachineBehaviour_OnStateExit_m5009/* method */
	, &StateMachineBehaviour_t995_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_AnimatorStateInfo_t887_Int32_t253/* invoker_method */
	, StateMachineBehaviour_t995_StateMachineBehaviour_OnStateExit_m5009_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2165/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Animator_t9_0_0_0;
extern const Il2CppType AnimatorStateInfo_t887_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo StateMachineBehaviour_t995_StateMachineBehaviour_OnStateMove_m5010_ParameterInfos[] = 
{
	{"animator", 0, 134220217, 0, &Animator_t9_0_0_0},
	{"stateInfo", 1, 134220218, 0, &AnimatorStateInfo_t887_0_0_0},
	{"layerIndex", 2, 134220219, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_AnimatorStateInfo_t887_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StateMachineBehaviour::OnStateMove(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern const MethodInfo StateMachineBehaviour_OnStateMove_m5010_MethodInfo = 
{
	"OnStateMove"/* name */
	, (methodPointerType)&StateMachineBehaviour_OnStateMove_m5010/* method */
	, &StateMachineBehaviour_t995_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_AnimatorStateInfo_t887_Int32_t253/* invoker_method */
	, StateMachineBehaviour_t995_StateMachineBehaviour_OnStateMove_m5010_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2166/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Animator_t9_0_0_0;
extern const Il2CppType AnimatorStateInfo_t887_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo StateMachineBehaviour_t995_StateMachineBehaviour_OnStateIK_m5011_ParameterInfos[] = 
{
	{"animator", 0, 134220220, 0, &Animator_t9_0_0_0},
	{"stateInfo", 1, 134220221, 0, &AnimatorStateInfo_t887_0_0_0},
	{"layerIndex", 2, 134220222, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_AnimatorStateInfo_t887_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StateMachineBehaviour::OnStateIK(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern const MethodInfo StateMachineBehaviour_OnStateIK_m5011_MethodInfo = 
{
	"OnStateIK"/* name */
	, (methodPointerType)&StateMachineBehaviour_OnStateIK_m5011/* method */
	, &StateMachineBehaviour_t995_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_AnimatorStateInfo_t887_Int32_t253/* invoker_method */
	, StateMachineBehaviour_t995_StateMachineBehaviour_OnStateIK_m5011_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2167/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Animator_t9_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo StateMachineBehaviour_t995_StateMachineBehaviour_OnStateMachineEnter_m5012_ParameterInfos[] = 
{
	{"animator", 0, 134220223, 0, &Animator_t9_0_0_0},
	{"stateMachinePathHash", 1, 134220224, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StateMachineBehaviour::OnStateMachineEnter(UnityEngine.Animator,System.Int32)
extern const MethodInfo StateMachineBehaviour_OnStateMachineEnter_m5012_MethodInfo = 
{
	"OnStateMachineEnter"/* name */
	, (methodPointerType)&StateMachineBehaviour_OnStateMachineEnter_m5012/* method */
	, &StateMachineBehaviour_t995_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Int32_t253/* invoker_method */
	, StateMachineBehaviour_t995_StateMachineBehaviour_OnStateMachineEnter_m5012_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2168/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Animator_t9_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo StateMachineBehaviour_t995_StateMachineBehaviour_OnStateMachineExit_m5013_ParameterInfos[] = 
{
	{"animator", 0, 134220225, 0, &Animator_t9_0_0_0},
	{"stateMachinePathHash", 1, 134220226, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StateMachineBehaviour::OnStateMachineExit(UnityEngine.Animator,System.Int32)
extern const MethodInfo StateMachineBehaviour_OnStateMachineExit_m5013_MethodInfo = 
{
	"OnStateMachineExit"/* name */
	, (methodPointerType)&StateMachineBehaviour_OnStateMachineExit_m5013/* method */
	, &StateMachineBehaviour_t995_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Int32_t253/* invoker_method */
	, StateMachineBehaviour_t995_StateMachineBehaviour_OnStateMachineExit_m5013_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2169/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* StateMachineBehaviour_t995_MethodInfos[] =
{
	&StateMachineBehaviour__ctor_m5006_MethodInfo,
	&StateMachineBehaviour_OnStateEnter_m5007_MethodInfo,
	&StateMachineBehaviour_OnStateUpdate_m5008_MethodInfo,
	&StateMachineBehaviour_OnStateExit_m5009_MethodInfo,
	&StateMachineBehaviour_OnStateMove_m5010_MethodInfo,
	&StateMachineBehaviour_OnStateIK_m5011_MethodInfo,
	&StateMachineBehaviour_OnStateMachineEnter_m5012_MethodInfo,
	&StateMachineBehaviour_OnStateMachineExit_m5013_MethodInfo,
	NULL
};
extern const MethodInfo Object_Equals_m1047_MethodInfo;
extern const MethodInfo Object_GetHashCode_m1049_MethodInfo;
extern const MethodInfo Object_ToString_m1050_MethodInfo;
extern const MethodInfo StateMachineBehaviour_OnStateEnter_m5007_MethodInfo;
extern const MethodInfo StateMachineBehaviour_OnStateUpdate_m5008_MethodInfo;
extern const MethodInfo StateMachineBehaviour_OnStateExit_m5009_MethodInfo;
extern const MethodInfo StateMachineBehaviour_OnStateMove_m5010_MethodInfo;
extern const MethodInfo StateMachineBehaviour_OnStateIK_m5011_MethodInfo;
extern const MethodInfo StateMachineBehaviour_OnStateMachineEnter_m5012_MethodInfo;
extern const MethodInfo StateMachineBehaviour_OnStateMachineExit_m5013_MethodInfo;
static const Il2CppMethodReference StateMachineBehaviour_t995_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	&StateMachineBehaviour_OnStateEnter_m5007_MethodInfo,
	&StateMachineBehaviour_OnStateUpdate_m5008_MethodInfo,
	&StateMachineBehaviour_OnStateExit_m5009_MethodInfo,
	&StateMachineBehaviour_OnStateMove_m5010_MethodInfo,
	&StateMachineBehaviour_OnStateIK_m5011_MethodInfo,
	&StateMachineBehaviour_OnStateMachineEnter_m5012_MethodInfo,
	&StateMachineBehaviour_OnStateMachineExit_m5013_MethodInfo,
};
static bool StateMachineBehaviour_t995_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType StateMachineBehaviour_t995_0_0_0;
extern const Il2CppType StateMachineBehaviour_t995_1_0_0;
extern const Il2CppType ScriptableObject_t784_0_0_0;
struct StateMachineBehaviour_t995;
const Il2CppTypeDefinitionMetadata StateMachineBehaviour_t995_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ScriptableObject_t784_0_0_0/* parent */
	, StateMachineBehaviour_t995_VTable/* vtableMethods */
	, StateMachineBehaviour_t995_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo StateMachineBehaviour_t995_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "StateMachineBehaviour"/* name */
	, "UnityEngine"/* namespaze */
	, StateMachineBehaviour_t995_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &StateMachineBehaviour_t995_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &StateMachineBehaviour_t995_0_0_0/* byval_arg */
	, &StateMachineBehaviour_t995_1_0_0/* this_arg */
	, &StateMachineBehaviour_t995_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StateMachineBehaviour_t995)/* instance_size */
	, sizeof (StateMachineBehaviour_t995)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.SystemClock
#include "UnityEngine_UnityEngine_SystemClock.h"
// Metadata Definition UnityEngine.SystemClock
extern TypeInfo SystemClock_t996_il2cpp_TypeInfo;
// UnityEngine.SystemClock
#include "UnityEngine_UnityEngine_SystemClockMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SystemClock::.cctor()
extern const MethodInfo SystemClock__cctor_m5014_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&SystemClock__cctor_m5014/* method */
	, &SystemClock_t996_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2170/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType DateTime_t406_0_0_0;
extern void* RuntimeInvoker_DateTime_t406 (const MethodInfo* method, void* obj, void** args);
// System.DateTime UnityEngine.SystemClock::get_now()
extern const MethodInfo SystemClock_get_now_m5015_MethodInfo = 
{
	"get_now"/* name */
	, (methodPointerType)&SystemClock_get_now_m5015/* method */
	, &SystemClock_t996_il2cpp_TypeInfo/* declaring_type */
	, &DateTime_t406_0_0_0/* return_type */
	, RuntimeInvoker_DateTime_t406/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2171/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SystemClock_t996_MethodInfos[] =
{
	&SystemClock__cctor_m5014_MethodInfo,
	&SystemClock_get_now_m5015_MethodInfo,
	NULL
};
extern const MethodInfo SystemClock_get_now_m5015_MethodInfo;
static const PropertyInfo SystemClock_t996____now_PropertyInfo = 
{
	&SystemClock_t996_il2cpp_TypeInfo/* parent */
	, "now"/* name */
	, &SystemClock_get_now_m5015_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* SystemClock_t996_PropertyInfos[] =
{
	&SystemClock_t996____now_PropertyInfo,
	NULL
};
static const Il2CppMethodReference SystemClock_t996_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool SystemClock_t996_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType SystemClock_t996_0_0_0;
extern const Il2CppType SystemClock_t996_1_0_0;
struct SystemClock_t996;
const Il2CppTypeDefinitionMetadata SystemClock_t996_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SystemClock_t996_VTable/* vtableMethods */
	, SystemClock_t996_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1339/* fieldStart */

};
TypeInfo SystemClock_t996_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SystemClock"/* name */
	, "UnityEngine"/* namespaze */
	, SystemClock_t996_MethodInfos/* methods */
	, SystemClock_t996_PropertyInfos/* properties */
	, NULL/* events */
	, &SystemClock_t996_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SystemClock_t996_0_0_0/* byval_arg */
	, &SystemClock_t996_1_0_0/* this_arg */
	, &SystemClock_t996_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SystemClock_t996)/* instance_size */
	, sizeof (SystemClock_t996)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(SystemClock_t996_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.TextEditor/DblClickSnapping
#include "UnityEngine_UnityEngine_TextEditor_DblClickSnapping.h"
// Metadata Definition UnityEngine.TextEditor/DblClickSnapping
extern TypeInfo DblClickSnapping_t997_il2cpp_TypeInfo;
// UnityEngine.TextEditor/DblClickSnapping
#include "UnityEngine_UnityEngine_TextEditor_DblClickSnappingMethodDeclarations.h"
static const MethodInfo* DblClickSnapping_t997_MethodInfos[] =
{
	NULL
};
extern const MethodInfo Enum_Equals_m1051_MethodInfo;
extern const MethodInfo Enum_GetHashCode_m1052_MethodInfo;
extern const MethodInfo Enum_ToString_m1053_MethodInfo;
extern const MethodInfo Enum_ToString_m1054_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToBoolean_m1055_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToByte_m1056_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToChar_m1057_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDateTime_m1058_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDecimal_m1059_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDouble_m1060_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt16_m1061_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt32_m1062_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt64_m1063_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToSByte_m1064_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToSingle_m1065_MethodInfo;
extern const MethodInfo Enum_ToString_m1066_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToType_m1067_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt16_m1068_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt32_m1069_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt64_m1070_MethodInfo;
extern const MethodInfo Enum_CompareTo_m1071_MethodInfo;
extern const MethodInfo Enum_GetTypeCode_m1072_MethodInfo;
static const Il2CppMethodReference DblClickSnapping_t997_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool DblClickSnapping_t997_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IFormattable_t275_0_0_0;
extern const Il2CppType IConvertible_t276_0_0_0;
extern const Il2CppType IComparable_t277_0_0_0;
static Il2CppInterfaceOffsetPair DblClickSnapping_t997_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType DblClickSnapping_t997_0_0_0;
extern const Il2CppType DblClickSnapping_t997_1_0_0;
extern const Il2CppType Enum_t278_0_0_0;
extern TypeInfo TextEditor_t685_il2cpp_TypeInfo;
extern const Il2CppType TextEditor_t685_0_0_0;
// System.Byte
#include "mscorlib_System_Byte.h"
extern TypeInfo Byte_t680_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata DblClickSnapping_t997_DefinitionMetadata = 
{
	&TextEditor_t685_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DblClickSnapping_t997_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, DblClickSnapping_t997_VTable/* vtableMethods */
	, DblClickSnapping_t997_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1340/* fieldStart */

};
TypeInfo DblClickSnapping_t997_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "DblClickSnapping"/* name */
	, ""/* namespaze */
	, DblClickSnapping_t997_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Byte_t680_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DblClickSnapping_t997_0_0_0/* byval_arg */
	, &DblClickSnapping_t997_1_0_0/* this_arg */
	, &DblClickSnapping_t997_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DblClickSnapping_t997)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (DblClickSnapping_t997)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint8_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.TextEditor/TextEditOp
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp.h"
// Metadata Definition UnityEngine.TextEditor/TextEditOp
extern TypeInfo TextEditOp_t998_il2cpp_TypeInfo;
// UnityEngine.TextEditor/TextEditOp
#include "UnityEngine_UnityEngine_TextEditor_TextEditOpMethodDeclarations.h"
static const MethodInfo* TextEditOp_t998_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference TextEditOp_t998_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool TextEditOp_t998_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TextEditOp_t998_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TextEditOp_t998_0_0_0;
extern const Il2CppType TextEditOp_t998_1_0_0;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t253_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata TextEditOp_t998_DefinitionMetadata = 
{
	&TextEditor_t685_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TextEditOp_t998_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, TextEditOp_t998_VTable/* vtableMethods */
	, TextEditOp_t998_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1343/* fieldStart */

};
TypeInfo TextEditOp_t998_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TextEditOp"/* name */
	, ""/* namespaze */
	, TextEditOp_t998_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TextEditOp_t998_0_0_0/* byval_arg */
	, &TextEditOp_t998_1_0_0/* this_arg */
	, &TextEditOp_t998_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TextEditOp_t998)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TextEditOp_t998)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 51/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.TextEditor
#include "UnityEngine_UnityEngine_TextEditor.h"
// Metadata Definition UnityEngine.TextEditor
// UnityEngine.TextEditor
#include "UnityEngine_UnityEngine_TextEditorMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.TextEditor::.ctor()
extern const MethodInfo TextEditor__ctor_m3284_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TextEditor__ctor_m3284/* method */
	, &TextEditor_t685_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2172/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.TextEditor::ClearCursorPos()
extern const MethodInfo TextEditor_ClearCursorPos_m5016_MethodInfo = 
{
	"ClearCursorPos"/* name */
	, (methodPointerType)&TextEditor_ClearCursorPos_m5016/* method */
	, &TextEditor_t685_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2173/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.TextEditor::OnFocus()
extern const MethodInfo TextEditor_OnFocus_m3288_MethodInfo = 
{
	"OnFocus"/* name */
	, (methodPointerType)&TextEditor_OnFocus_m3288/* method */
	, &TextEditor_t685_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2174/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.TextEditor::OnLostFocus()
extern const MethodInfo TextEditor_OnLostFocus_m5017_MethodInfo = 
{
	"OnLostFocus"/* name */
	, (methodPointerType)&TextEditor_OnLostFocus_m5017/* method */
	, &TextEditor_t685_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2175/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.TextEditor::SelectAll()
extern const MethodInfo TextEditor_SelectAll_m5018_MethodInfo = 
{
	"SelectAll"/* name */
	, (methodPointerType)&TextEditor_SelectAll_m5018/* method */
	, &TextEditor_t685_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2176/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.TextEditor::DeleteSelection()
extern const MethodInfo TextEditor_DeleteSelection_m5019_MethodInfo = 
{
	"DeleteSelection"/* name */
	, (methodPointerType)&TextEditor_DeleteSelection_m5019/* method */
	, &TextEditor_t685_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2177/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo TextEditor_t685_TextEditor_ReplaceSelection_m5020_ParameterInfos[] = 
{
	{"replace", 0, 134220227, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.TextEditor::ReplaceSelection(System.String)
extern const MethodInfo TextEditor_ReplaceSelection_m5020_MethodInfo = 
{
	"ReplaceSelection"/* name */
	, (methodPointerType)&TextEditor_ReplaceSelection_m5020/* method */
	, &TextEditor_t685_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, TextEditor_t685_TextEditor_ReplaceSelection_m5020_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2178/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.TextEditor::UpdateScrollOffsetIfNeeded()
extern const MethodInfo TextEditor_UpdateScrollOffsetIfNeeded_m5021_MethodInfo = 
{
	"UpdateScrollOffsetIfNeeded"/* name */
	, (methodPointerType)&TextEditor_UpdateScrollOffsetIfNeeded_m5021/* method */
	, &TextEditor_t685_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2179/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.TextEditor::UpdateScrollOffset()
extern const MethodInfo TextEditor_UpdateScrollOffset_m5022_MethodInfo = 
{
	"UpdateScrollOffset"/* name */
	, (methodPointerType)&TextEditor_UpdateScrollOffset_m5022/* method */
	, &TextEditor_t685_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2180/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.TextEditor::SaveBackup()
extern const MethodInfo TextEditor_SaveBackup_m5023_MethodInfo = 
{
	"SaveBackup"/* name */
	, (methodPointerType)&TextEditor_SaveBackup_m5023/* method */
	, &TextEditor_t685_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2181/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.TextEditor::Copy()
extern const MethodInfo TextEditor_Copy_m3289_MethodInfo = 
{
	"Copy"/* name */
	, (methodPointerType)&TextEditor_Copy_m3289/* method */
	, &TextEditor_t685_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2182/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo TextEditor_t685_TextEditor_ReplaceNewlinesWithSpaces_m5024_ParameterInfos[] = 
{
	{"value", 0, 134220228, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.TextEditor::ReplaceNewlinesWithSpaces(System.String)
extern const MethodInfo TextEditor_ReplaceNewlinesWithSpaces_m5024_MethodInfo = 
{
	"ReplaceNewlinesWithSpaces"/* name */
	, (methodPointerType)&TextEditor_ReplaceNewlinesWithSpaces_m5024/* method */
	, &TextEditor_t685_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, TextEditor_t685_TextEditor_ReplaceNewlinesWithSpaces_m5024_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2183/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.TextEditor::Paste()
extern const MethodInfo TextEditor_Paste_m3285_MethodInfo = 
{
	"Paste"/* name */
	, (methodPointerType)&TextEditor_Paste_m3285/* method */
	, &TextEditor_t685_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2184/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.TextEditor::ClampPos()
extern const MethodInfo TextEditor_ClampPos_m5025_MethodInfo = 
{
	"ClampPos"/* name */
	, (methodPointerType)&TextEditor_ClampPos_m5025/* method */
	, &TextEditor_t685_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2185/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TextEditor_t685_MethodInfos[] =
{
	&TextEditor__ctor_m3284_MethodInfo,
	&TextEditor_ClearCursorPos_m5016_MethodInfo,
	&TextEditor_OnFocus_m3288_MethodInfo,
	&TextEditor_OnLostFocus_m5017_MethodInfo,
	&TextEditor_SelectAll_m5018_MethodInfo,
	&TextEditor_DeleteSelection_m5019_MethodInfo,
	&TextEditor_ReplaceSelection_m5020_MethodInfo,
	&TextEditor_UpdateScrollOffsetIfNeeded_m5021_MethodInfo,
	&TextEditor_UpdateScrollOffset_m5022_MethodInfo,
	&TextEditor_SaveBackup_m5023_MethodInfo,
	&TextEditor_Copy_m3289_MethodInfo,
	&TextEditor_ReplaceNewlinesWithSpaces_m5024_MethodInfo,
	&TextEditor_Paste_m3285_MethodInfo,
	&TextEditor_ClampPos_m5025_MethodInfo,
	NULL
};
static const Il2CppType* TextEditor_t685_il2cpp_TypeInfo__nestedTypes[2] =
{
	&DblClickSnapping_t997_0_0_0,
	&TextEditOp_t998_0_0_0,
};
static const Il2CppMethodReference TextEditor_t685_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool TextEditor_t685_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TextEditor_t685_1_0_0;
struct TextEditor_t685;
const Il2CppTypeDefinitionMetadata TextEditor_t685_DefinitionMetadata = 
{
	NULL/* declaringType */
	, TextEditor_t685_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TextEditor_t685_VTable/* vtableMethods */
	, TextEditor_t685_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1394/* fieldStart */

};
TypeInfo TextEditor_t685_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TextEditor"/* name */
	, "UnityEngine"/* namespaze */
	, TextEditor_t685_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TextEditor_t685_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TextEditor_t685_0_0_0/* byval_arg */
	, &TextEditor_t685_1_0_0/* this_arg */
	, &TextEditor_t685_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TextEditor_t685)/* instance_size */
	, sizeof (TextEditor_t685)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(TextEditor_t685_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 14/* method_count */
	, 0/* property_count */
	, 24/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.TextGenerationSettings
#include "UnityEngine_UnityEngine_TextGenerationSettings.h"
// Metadata Definition UnityEngine.TextGenerationSettings
extern TypeInfo TextGenerationSettings_t649_il2cpp_TypeInfo;
// UnityEngine.TextGenerationSettings
#include "UnityEngine_UnityEngine_TextGenerationSettingsMethodDeclarations.h"
extern const Il2CppType Color_t65_0_0_0;
extern const Il2CppType Color_t65_0_0_0;
extern const Il2CppType Color_t65_0_0_0;
static const ParameterInfo TextGenerationSettings_t649_TextGenerationSettings_CompareColors_m5026_ParameterInfos[] = 
{
	{"left", 0, 134220229, 0, &Color_t65_0_0_0},
	{"right", 1, 134220230, 0, &Color_t65_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Color_t65_Color_t65 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.TextGenerationSettings::CompareColors(UnityEngine.Color,UnityEngine.Color)
extern const MethodInfo TextGenerationSettings_CompareColors_m5026_MethodInfo = 
{
	"CompareColors"/* name */
	, (methodPointerType)&TextGenerationSettings_CompareColors_m5026/* method */
	, &TextGenerationSettings_t649_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Color_t65_Color_t65/* invoker_method */
	, TextGenerationSettings_t649_TextGenerationSettings_CompareColors_m5026_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2186/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t6_0_0_0;
extern const Il2CppType Vector2_t6_0_0_0;
extern const Il2CppType Vector2_t6_0_0_0;
static const ParameterInfo TextGenerationSettings_t649_TextGenerationSettings_CompareVector2_m5027_ParameterInfos[] = 
{
	{"left", 0, 134220231, 0, &Vector2_t6_0_0_0},
	{"right", 1, 134220232, 0, &Vector2_t6_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Vector2_t6_Vector2_t6 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.TextGenerationSettings::CompareVector2(UnityEngine.Vector2,UnityEngine.Vector2)
extern const MethodInfo TextGenerationSettings_CompareVector2_m5027_MethodInfo = 
{
	"CompareVector2"/* name */
	, (methodPointerType)&TextGenerationSettings_CompareVector2_m5027/* method */
	, &TextGenerationSettings_t649_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Vector2_t6_Vector2_t6/* invoker_method */
	, TextGenerationSettings_t649_TextGenerationSettings_CompareVector2_m5027_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2187/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TextGenerationSettings_t649_0_0_0;
extern const Il2CppType TextGenerationSettings_t649_0_0_0;
static const ParameterInfo TextGenerationSettings_t649_TextGenerationSettings_Equals_m5028_ParameterInfos[] = 
{
	{"other", 0, 134220233, 0, &TextGenerationSettings_t649_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_TextGenerationSettings_t649 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.TextGenerationSettings::Equals(UnityEngine.TextGenerationSettings)
extern const MethodInfo TextGenerationSettings_Equals_m5028_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&TextGenerationSettings_Equals_m5028/* method */
	, &TextGenerationSettings_t649_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_TextGenerationSettings_t649/* invoker_method */
	, TextGenerationSettings_t649_TextGenerationSettings_Equals_m5028_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2188/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TextGenerationSettings_t649_MethodInfos[] =
{
	&TextGenerationSettings_CompareColors_m5026_MethodInfo,
	&TextGenerationSettings_CompareVector2_m5027_MethodInfo,
	&TextGenerationSettings_Equals_m5028_MethodInfo,
	NULL
};
static const Il2CppMethodReference TextGenerationSettings_t649_VTable[] =
{
	&ValueType_Equals_m1076_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&ValueType_GetHashCode_m1077_MethodInfo,
	&ValueType_ToString_m1078_MethodInfo,
};
static bool TextGenerationSettings_t649_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TextGenerationSettings_t649_1_0_0;
const Il2CppTypeDefinitionMetadata TextGenerationSettings_t649_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t285_0_0_0/* parent */
	, TextGenerationSettings_t649_VTable/* vtableMethods */
	, TextGenerationSettings_t649_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1418/* fieldStart */

};
TypeInfo TextGenerationSettings_t649_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TextGenerationSettings"/* name */
	, "UnityEngine"/* namespaze */
	, TextGenerationSettings_t649_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TextGenerationSettings_t649_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TextGenerationSettings_t649_0_0_0/* byval_arg */
	, &TextGenerationSettings_t649_1_0_0/* this_arg */
	, &TextGenerationSettings_t649_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TextGenerationSettings_t649)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TextGenerationSettings_t649)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 17/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.TrackedReference
#include "UnityEngine_UnityEngine_TrackedReference.h"
// Metadata Definition UnityEngine.TrackedReference
extern TypeInfo TrackedReference_t891_il2cpp_TypeInfo;
// UnityEngine.TrackedReference
#include "UnityEngine_UnityEngine_TrackedReferenceMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo TrackedReference_t891_TrackedReference_Equals_m5029_ParameterInfos[] = 
{
	{"o", 0, 134220234, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.TrackedReference::Equals(System.Object)
extern const MethodInfo TrackedReference_Equals_m5029_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&TrackedReference_Equals_m5029/* method */
	, &TrackedReference_t891_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, TrackedReference_t891_TrackedReference_Equals_m5029_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2189/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.TrackedReference::GetHashCode()
extern const MethodInfo TrackedReference_GetHashCode_m5030_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&TrackedReference_GetHashCode_m5030/* method */
	, &TrackedReference_t891_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2190/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TrackedReference_t891_0_0_0;
extern const Il2CppType TrackedReference_t891_0_0_0;
extern const Il2CppType TrackedReference_t891_0_0_0;
static const ParameterInfo TrackedReference_t891_TrackedReference_op_Equality_m5031_ParameterInfos[] = 
{
	{"x", 0, 134220235, 0, &TrackedReference_t891_0_0_0},
	{"y", 1, 134220236, 0, &TrackedReference_t891_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.TrackedReference::op_Equality(UnityEngine.TrackedReference,UnityEngine.TrackedReference)
extern const MethodInfo TrackedReference_op_Equality_m5031_MethodInfo = 
{
	"op_Equality"/* name */
	, (methodPointerType)&TrackedReference_op_Equality_m5031/* method */
	, &TrackedReference_t891_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t_Object_t/* invoker_method */
	, TrackedReference_t891_TrackedReference_op_Equality_m5031_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2191/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TrackedReference_t891_MethodInfos[] =
{
	&TrackedReference_Equals_m5029_MethodInfo,
	&TrackedReference_GetHashCode_m5030_MethodInfo,
	&TrackedReference_op_Equality_m5031_MethodInfo,
	NULL
};
extern const MethodInfo TrackedReference_Equals_m5029_MethodInfo;
extern const MethodInfo TrackedReference_GetHashCode_m5030_MethodInfo;
static const Il2CppMethodReference TrackedReference_t891_VTable[] =
{
	&TrackedReference_Equals_m5029_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&TrackedReference_GetHashCode_m5030_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool TrackedReference_t891_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TrackedReference_t891_1_0_0;
struct TrackedReference_t891;
const Il2CppTypeDefinitionMetadata TrackedReference_t891_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TrackedReference_t891_VTable/* vtableMethods */
	, TrackedReference_t891_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1435/* fieldStart */

};
TypeInfo TrackedReference_t891_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TrackedReference"/* name */
	, "UnityEngine"/* namespaze */
	, TrackedReference_t891_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TrackedReference_t891_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TrackedReference_t891_0_0_0/* byval_arg */
	, &TrackedReference_t891_1_0_0/* this_arg */
	, &TrackedReference_t891_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)TrackedReference_t891_marshal/* marshal_to_native_func */
	, (methodPointerType)TrackedReference_t891_marshal_back/* marshal_from_native_func */
	, (methodPointerType)TrackedReference_t891_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (TrackedReference_t891)/* instance_size */
	, sizeof (TrackedReference_t891)/* actualSize */
	, 0/* element_size */
	, sizeof(TrackedReference_t891_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048585/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.PersistentListenerMode
#include "UnityEngine_UnityEngine_Events_PersistentListenerMode.h"
// Metadata Definition UnityEngine.Events.PersistentListenerMode
extern TypeInfo PersistentListenerMode_t1000_il2cpp_TypeInfo;
// UnityEngine.Events.PersistentListenerMode
#include "UnityEngine_UnityEngine_Events_PersistentListenerModeMethodDeclarations.h"
static const MethodInfo* PersistentListenerMode_t1000_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference PersistentListenerMode_t1000_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool PersistentListenerMode_t1000_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair PersistentListenerMode_t1000_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType PersistentListenerMode_t1000_0_0_0;
extern const Il2CppType PersistentListenerMode_t1000_1_0_0;
const Il2CppTypeDefinitionMetadata PersistentListenerMode_t1000_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, PersistentListenerMode_t1000_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, PersistentListenerMode_t1000_VTable/* vtableMethods */
	, PersistentListenerMode_t1000_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1436/* fieldStart */

};
TypeInfo PersistentListenerMode_t1000_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "PersistentListenerMode"/* name */
	, "UnityEngine.Events"/* namespaze */
	, PersistentListenerMode_t1000_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PersistentListenerMode_t1000_0_0_0/* byval_arg */
	, &PersistentListenerMode_t1000_1_0_0/* this_arg */
	, &PersistentListenerMode_t1000_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PersistentListenerMode_t1000)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (PersistentListenerMode_t1000)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.Events.ArgumentCache
#include "UnityEngine_UnityEngine_Events_ArgumentCache.h"
// Metadata Definition UnityEngine.Events.ArgumentCache
extern TypeInfo ArgumentCache_t1001_il2cpp_TypeInfo;
// UnityEngine.Events.ArgumentCache
#include "UnityEngine_UnityEngine_Events_ArgumentCacheMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.ArgumentCache::.ctor()
extern const MethodInfo ArgumentCache__ctor_m5032_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ArgumentCache__ctor_m5032/* method */
	, &ArgumentCache_t1001_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2192/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t164_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Object UnityEngine.Events.ArgumentCache::get_unityObjectArgument()
extern const MethodInfo ArgumentCache_get_unityObjectArgument_m5033_MethodInfo = 
{
	"get_unityObjectArgument"/* name */
	, (methodPointerType)&ArgumentCache_get_unityObjectArgument_m5033/* method */
	, &ArgumentCache_t1001_il2cpp_TypeInfo/* declaring_type */
	, &Object_t164_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2193/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Events.ArgumentCache::get_unityObjectArgumentAssemblyTypeName()
extern const MethodInfo ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m5034_MethodInfo = 
{
	"get_unityObjectArgumentAssemblyTypeName"/* name */
	, (methodPointerType)&ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m5034/* method */
	, &ArgumentCache_t1001_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2194/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.Events.ArgumentCache::get_intArgument()
extern const MethodInfo ArgumentCache_get_intArgument_m5035_MethodInfo = 
{
	"get_intArgument"/* name */
	, (methodPointerType)&ArgumentCache_get_intArgument_m5035/* method */
	, &ArgumentCache_t1001_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2195/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.Events.ArgumentCache::get_floatArgument()
extern const MethodInfo ArgumentCache_get_floatArgument_m5036_MethodInfo = 
{
	"get_floatArgument"/* name */
	, (methodPointerType)&ArgumentCache_get_floatArgument_m5036/* method */
	, &ArgumentCache_t1001_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2196/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Events.ArgumentCache::get_stringArgument()
extern const MethodInfo ArgumentCache_get_stringArgument_m5037_MethodInfo = 
{
	"get_stringArgument"/* name */
	, (methodPointerType)&ArgumentCache_get_stringArgument_m5037/* method */
	, &ArgumentCache_t1001_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2197/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Events.ArgumentCache::get_boolArgument()
extern const MethodInfo ArgumentCache_get_boolArgument_m5038_MethodInfo = 
{
	"get_boolArgument"/* name */
	, (methodPointerType)&ArgumentCache_get_boolArgument_m5038/* method */
	, &ArgumentCache_t1001_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2198/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.ArgumentCache::TidyAssemblyTypeName()
extern const MethodInfo ArgumentCache_TidyAssemblyTypeName_m5039_MethodInfo = 
{
	"TidyAssemblyTypeName"/* name */
	, (methodPointerType)&ArgumentCache_TidyAssemblyTypeName_m5039/* method */
	, &ArgumentCache_t1001_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2199/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.ArgumentCache::OnBeforeSerialize()
extern const MethodInfo ArgumentCache_OnBeforeSerialize_m5040_MethodInfo = 
{
	"OnBeforeSerialize"/* name */
	, (methodPointerType)&ArgumentCache_OnBeforeSerialize_m5040/* method */
	, &ArgumentCache_t1001_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2200/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.ArgumentCache::OnAfterDeserialize()
extern const MethodInfo ArgumentCache_OnAfterDeserialize_m5041_MethodInfo = 
{
	"OnAfterDeserialize"/* name */
	, (methodPointerType)&ArgumentCache_OnAfterDeserialize_m5041/* method */
	, &ArgumentCache_t1001_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2201/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ArgumentCache_t1001_MethodInfos[] =
{
	&ArgumentCache__ctor_m5032_MethodInfo,
	&ArgumentCache_get_unityObjectArgument_m5033_MethodInfo,
	&ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m5034_MethodInfo,
	&ArgumentCache_get_intArgument_m5035_MethodInfo,
	&ArgumentCache_get_floatArgument_m5036_MethodInfo,
	&ArgumentCache_get_stringArgument_m5037_MethodInfo,
	&ArgumentCache_get_boolArgument_m5038_MethodInfo,
	&ArgumentCache_TidyAssemblyTypeName_m5039_MethodInfo,
	&ArgumentCache_OnBeforeSerialize_m5040_MethodInfo,
	&ArgumentCache_OnAfterDeserialize_m5041_MethodInfo,
	NULL
};
extern const MethodInfo ArgumentCache_get_unityObjectArgument_m5033_MethodInfo;
static const PropertyInfo ArgumentCache_t1001____unityObjectArgument_PropertyInfo = 
{
	&ArgumentCache_t1001_il2cpp_TypeInfo/* parent */
	, "unityObjectArgument"/* name */
	, &ArgumentCache_get_unityObjectArgument_m5033_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m5034_MethodInfo;
static const PropertyInfo ArgumentCache_t1001____unityObjectArgumentAssemblyTypeName_PropertyInfo = 
{
	&ArgumentCache_t1001_il2cpp_TypeInfo/* parent */
	, "unityObjectArgumentAssemblyTypeName"/* name */
	, &ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m5034_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ArgumentCache_get_intArgument_m5035_MethodInfo;
static const PropertyInfo ArgumentCache_t1001____intArgument_PropertyInfo = 
{
	&ArgumentCache_t1001_il2cpp_TypeInfo/* parent */
	, "intArgument"/* name */
	, &ArgumentCache_get_intArgument_m5035_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ArgumentCache_get_floatArgument_m5036_MethodInfo;
static const PropertyInfo ArgumentCache_t1001____floatArgument_PropertyInfo = 
{
	&ArgumentCache_t1001_il2cpp_TypeInfo/* parent */
	, "floatArgument"/* name */
	, &ArgumentCache_get_floatArgument_m5036_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ArgumentCache_get_stringArgument_m5037_MethodInfo;
static const PropertyInfo ArgumentCache_t1001____stringArgument_PropertyInfo = 
{
	&ArgumentCache_t1001_il2cpp_TypeInfo/* parent */
	, "stringArgument"/* name */
	, &ArgumentCache_get_stringArgument_m5037_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ArgumentCache_get_boolArgument_m5038_MethodInfo;
static const PropertyInfo ArgumentCache_t1001____boolArgument_PropertyInfo = 
{
	&ArgumentCache_t1001_il2cpp_TypeInfo/* parent */
	, "boolArgument"/* name */
	, &ArgumentCache_get_boolArgument_m5038_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ArgumentCache_t1001_PropertyInfos[] =
{
	&ArgumentCache_t1001____unityObjectArgument_PropertyInfo,
	&ArgumentCache_t1001____unityObjectArgumentAssemblyTypeName_PropertyInfo,
	&ArgumentCache_t1001____intArgument_PropertyInfo,
	&ArgumentCache_t1001____floatArgument_PropertyInfo,
	&ArgumentCache_t1001____stringArgument_PropertyInfo,
	&ArgumentCache_t1001____boolArgument_PropertyInfo,
	NULL
};
extern const MethodInfo ArgumentCache_OnBeforeSerialize_m5040_MethodInfo;
extern const MethodInfo ArgumentCache_OnAfterDeserialize_m5041_MethodInfo;
static const Il2CppMethodReference ArgumentCache_t1001_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&ArgumentCache_OnBeforeSerialize_m5040_MethodInfo,
	&ArgumentCache_OnAfterDeserialize_m5041_MethodInfo,
};
static bool ArgumentCache_t1001_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ISerializationCallbackReceiver_t731_0_0_0;
static const Il2CppType* ArgumentCache_t1001_InterfacesTypeInfos[] = 
{
	&ISerializationCallbackReceiver_t731_0_0_0,
};
static Il2CppInterfaceOffsetPair ArgumentCache_t1001_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t731_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ArgumentCache_t1001_0_0_0;
extern const Il2CppType ArgumentCache_t1001_1_0_0;
struct ArgumentCache_t1001;
const Il2CppTypeDefinitionMetadata ArgumentCache_t1001_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ArgumentCache_t1001_InterfacesTypeInfos/* implementedInterfaces */
	, ArgumentCache_t1001_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ArgumentCache_t1001_VTable/* vtableMethods */
	, ArgumentCache_t1001_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1444/* fieldStart */

};
TypeInfo ArgumentCache_t1001_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ArgumentCache"/* name */
	, "UnityEngine.Events"/* namespaze */
	, ArgumentCache_t1001_MethodInfos/* methods */
	, ArgumentCache_t1001_PropertyInfos/* properties */
	, NULL/* events */
	, &ArgumentCache_t1001_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ArgumentCache_t1001_0_0_0/* byval_arg */
	, &ArgumentCache_t1001_1_0_0/* this_arg */
	, &ArgumentCache_t1001_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ArgumentCache_t1001)/* instance_size */
	, sizeof (ArgumentCache_t1001)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 6/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// Metadata Definition UnityEngine.Events.BaseInvokableCall
extern TypeInfo BaseInvokableCall_t1002_il2cpp_TypeInfo;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCallMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.BaseInvokableCall::.ctor()
extern const MethodInfo BaseInvokableCall__ctor_m5042_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&BaseInvokableCall__ctor_m5042/* method */
	, &BaseInvokableCall_t1002_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2202/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo BaseInvokableCall_t1002_BaseInvokableCall__ctor_m5043_ParameterInfos[] = 
{
	{"target", 0, 134220237, 0, &Object_t_0_0_0},
	{"function", 1, 134220238, 0, &MethodInfo_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.BaseInvokableCall::.ctor(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo BaseInvokableCall__ctor_m5043_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&BaseInvokableCall__ctor_m5043/* method */
	, &BaseInvokableCall_t1002_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, BaseInvokableCall_t1002_BaseInvokableCall__ctor_m5043_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2203/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectU5BU5D_t224_0_0_0;
extern const Il2CppType ObjectU5BU5D_t224_0_0_0;
static const ParameterInfo BaseInvokableCall_t1002_BaseInvokableCall_Invoke_m5332_ParameterInfos[] = 
{
	{"args", 0, 134220239, 0, &ObjectU5BU5D_t224_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.BaseInvokableCall::Invoke(System.Object[])
extern const MethodInfo BaseInvokableCall_Invoke_m5332_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &BaseInvokableCall_t1002_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, BaseInvokableCall_t1002_BaseInvokableCall_Invoke_m5332_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2204/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo BaseInvokableCall_t1002_BaseInvokableCall_ThrowOnInvalidArg_m5333_ParameterInfos[] = 
{
	{"arg", 0, 134220240, 0, &Object_t_0_0_0},
};
extern const Il2CppGenericContainer BaseInvokableCall_ThrowOnInvalidArg_m5333_Il2CppGenericContainer;
extern TypeInfo BaseInvokableCall_ThrowOnInvalidArg_m5333_gp_T_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter BaseInvokableCall_ThrowOnInvalidArg_m5333_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &BaseInvokableCall_ThrowOnInvalidArg_m5333_Il2CppGenericContainer, NULL, "T", 0, 0 };
static const Il2CppGenericParameter* BaseInvokableCall_ThrowOnInvalidArg_m5333_Il2CppGenericParametersArray[1] = 
{
	&BaseInvokableCall_ThrowOnInvalidArg_m5333_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo BaseInvokableCall_ThrowOnInvalidArg_m5333_MethodInfo;
extern const Il2CppGenericContainer BaseInvokableCall_ThrowOnInvalidArg_m5333_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&BaseInvokableCall_ThrowOnInvalidArg_m5333_MethodInfo, 1, 1, BaseInvokableCall_ThrowOnInvalidArg_m5333_Il2CppGenericParametersArray };
extern const Il2CppType BaseInvokableCall_ThrowOnInvalidArg_m5333_gp_0_0_0_0;
static Il2CppRGCTXDefinition BaseInvokableCall_ThrowOnInvalidArg_m5333_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&BaseInvokableCall_ThrowOnInvalidArg_m5333_gp_0_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&BaseInvokableCall_ThrowOnInvalidArg_m5333_gp_0_0_0_0 }/* Type */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg(System.Object)
extern const MethodInfo BaseInvokableCall_ThrowOnInvalidArg_m5333_MethodInfo = 
{
	"ThrowOnInvalidArg"/* name */
	, NULL/* method */
	, &BaseInvokableCall_t1002_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, NULL/* invoker_method */
	, BaseInvokableCall_t1002_BaseInvokableCall_ThrowOnInvalidArg_m5333_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 148/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 2205/* token */
	, BaseInvokableCall_ThrowOnInvalidArg_m5333_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &BaseInvokableCall_ThrowOnInvalidArg_m5333_Il2CppGenericContainer/* genericContainer */

};
extern const Il2CppType Delegate_t675_0_0_0;
extern const Il2CppType Delegate_t675_0_0_0;
static const ParameterInfo BaseInvokableCall_t1002_BaseInvokableCall_AllowInvoke_m5044_ParameterInfos[] = 
{
	{"delegate", 0, 134220241, 0, &Delegate_t675_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Events.BaseInvokableCall::AllowInvoke(System.Delegate)
extern const MethodInfo BaseInvokableCall_AllowInvoke_m5044_MethodInfo = 
{
	"AllowInvoke"/* name */
	, (methodPointerType)&BaseInvokableCall_AllowInvoke_m5044/* method */
	, &BaseInvokableCall_t1002_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, BaseInvokableCall_t1002_BaseInvokableCall_AllowInvoke_m5044_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 148/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2206/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo BaseInvokableCall_t1002_BaseInvokableCall_Find_m5334_ParameterInfos[] = 
{
	{"targetObj", 0, 134220242, 0, &Object_t_0_0_0},
	{"method", 1, 134220243, 0, &MethodInfo_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Events.BaseInvokableCall::Find(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo BaseInvokableCall_Find_m5334_MethodInfo = 
{
	"Find"/* name */
	, NULL/* method */
	, &BaseInvokableCall_t1002_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t_Object_t/* invoker_method */
	, BaseInvokableCall_t1002_BaseInvokableCall_Find_m5334_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2207/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* BaseInvokableCall_t1002_MethodInfos[] =
{
	&BaseInvokableCall__ctor_m5042_MethodInfo,
	&BaseInvokableCall__ctor_m5043_MethodInfo,
	&BaseInvokableCall_Invoke_m5332_MethodInfo,
	&BaseInvokableCall_ThrowOnInvalidArg_m5333_MethodInfo,
	&BaseInvokableCall_AllowInvoke_m5044_MethodInfo,
	&BaseInvokableCall_Find_m5334_MethodInfo,
	NULL
};
static const Il2CppMethodReference BaseInvokableCall_t1002_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	NULL,
	NULL,
};
static bool BaseInvokableCall_t1002_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType BaseInvokableCall_t1002_0_0_0;
extern const Il2CppType BaseInvokableCall_t1002_1_0_0;
struct BaseInvokableCall_t1002;
const Il2CppTypeDefinitionMetadata BaseInvokableCall_t1002_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, BaseInvokableCall_t1002_VTable/* vtableMethods */
	, BaseInvokableCall_t1002_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo BaseInvokableCall_t1002_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "BaseInvokableCall"/* name */
	, "UnityEngine.Events"/* namespaze */
	, BaseInvokableCall_t1002_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &BaseInvokableCall_t1002_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BaseInvokableCall_t1002_0_0_0/* byval_arg */
	, &BaseInvokableCall_t1002_1_0_0/* this_arg */
	, &BaseInvokableCall_t1002_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BaseInvokableCall_t1002)/* instance_size */
	, sizeof (BaseInvokableCall_t1002)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048704/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.InvokableCall
#include "UnityEngine_UnityEngine_Events_InvokableCall.h"
// Metadata Definition UnityEngine.Events.InvokableCall
extern TypeInfo InvokableCall_t1003_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall
#include "UnityEngine_UnityEngine_Events_InvokableCallMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo InvokableCall_t1003_InvokableCall__ctor_m5045_ParameterInfos[] = 
{
	{"target", 0, 134220244, 0, &Object_t_0_0_0},
	{"theFunction", 1, 134220245, 0, &MethodInfo_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.InvokableCall::.ctor(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo InvokableCall__ctor_m5045_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall__ctor_m5045/* method */
	, &InvokableCall_t1003_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, InvokableCall_t1003_InvokableCall__ctor_m5045_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2208/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UnityAction_t416_0_0_0;
extern const Il2CppType UnityAction_t416_0_0_0;
static const ParameterInfo InvokableCall_t1003_InvokableCall__ctor_m5046_ParameterInfos[] = 
{
	{"action", 0, 134220246, 0, &UnityAction_t416_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.InvokableCall::.ctor(UnityEngine.Events.UnityAction)
extern const MethodInfo InvokableCall__ctor_m5046_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall__ctor_m5046/* method */
	, &InvokableCall_t1003_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, InvokableCall_t1003_InvokableCall__ctor_m5046_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2209/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectU5BU5D_t224_0_0_0;
static const ParameterInfo InvokableCall_t1003_InvokableCall_Invoke_m5047_ParameterInfos[] = 
{
	{"args", 0, 134220247, 0, &ObjectU5BU5D_t224_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.InvokableCall::Invoke(System.Object[])
extern const MethodInfo InvokableCall_Invoke_m5047_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_Invoke_m5047/* method */
	, &InvokableCall_t1003_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, InvokableCall_t1003_InvokableCall_Invoke_m5047_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2210/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo InvokableCall_t1003_InvokableCall_Find_m5048_ParameterInfos[] = 
{
	{"targetObj", 0, 134220248, 0, &Object_t_0_0_0},
	{"method", 1, 134220249, 0, &MethodInfo_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Events.InvokableCall::Find(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo InvokableCall_Find_m5048_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_Find_m5048/* method */
	, &InvokableCall_t1003_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t_Object_t/* invoker_method */
	, InvokableCall_t1003_InvokableCall_Find_m5048_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2211/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* InvokableCall_t1003_MethodInfos[] =
{
	&InvokableCall__ctor_m5045_MethodInfo,
	&InvokableCall__ctor_m5046_MethodInfo,
	&InvokableCall_Invoke_m5047_MethodInfo,
	&InvokableCall_Find_m5048_MethodInfo,
	NULL
};
extern const MethodInfo InvokableCall_Invoke_m5047_MethodInfo;
extern const MethodInfo InvokableCall_Find_m5048_MethodInfo;
static const Il2CppMethodReference InvokableCall_t1003_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&InvokableCall_Invoke_m5047_MethodInfo,
	&InvokableCall_Find_m5048_MethodInfo,
};
static bool InvokableCall_t1003_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType InvokableCall_t1003_0_0_0;
extern const Il2CppType InvokableCall_t1003_1_0_0;
struct InvokableCall_t1003;
const Il2CppTypeDefinitionMetadata InvokableCall_t1003_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &BaseInvokableCall_t1002_0_0_0/* parent */
	, InvokableCall_t1003_VTable/* vtableMethods */
	, InvokableCall_t1003_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1450/* fieldStart */

};
TypeInfo InvokableCall_t1003_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_t1003_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &InvokableCall_t1003_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &InvokableCall_t1003_0_0_0/* byval_arg */
	, &InvokableCall_t1003_1_0_0/* this_arg */
	, &InvokableCall_t1003_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_t1003)/* instance_size */
	, sizeof (InvokableCall_t1003)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.InvokableCall`1
extern TypeInfo InvokableCall_1_t1128_il2cpp_TypeInfo;
extern const Il2CppGenericContainer InvokableCall_1_t1128_Il2CppGenericContainer;
extern TypeInfo InvokableCall_1_t1128_gp_T1_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter InvokableCall_1_t1128_gp_T1_0_il2cpp_TypeInfo_GenericParamFull = { &InvokableCall_1_t1128_Il2CppGenericContainer, NULL, "T1", 0, 0 };
static const Il2CppGenericParameter* InvokableCall_1_t1128_Il2CppGenericParametersArray[1] = 
{
	&InvokableCall_1_t1128_gp_T1_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer InvokableCall_1_t1128_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&InvokableCall_1_t1128_il2cpp_TypeInfo, 1, 0, InvokableCall_1_t1128_Il2CppGenericParametersArray };
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo InvokableCall_1_t1128_InvokableCall_1__ctor_m5335_ParameterInfos[] = 
{
	{"target", 0, 134220250, 0, &Object_t_0_0_0},
	{"theFunction", 1, 134220251, 0, &MethodInfo_t_0_0_0},
};
// System.Void UnityEngine.Events.InvokableCall`1::.ctor(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo InvokableCall_1__ctor_m5335_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &InvokableCall_1_t1128_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_1_t1128_InvokableCall_1__ctor_m5335_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2212/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UnityAction_1_t1219_0_0_0;
extern const Il2CppType UnityAction_1_t1219_0_0_0;
static const ParameterInfo InvokableCall_1_t1128_InvokableCall_1__ctor_m5336_ParameterInfos[] = 
{
	{"callback", 0, 134220252, 0, &UnityAction_1_t1219_0_0_0},
};
// System.Void UnityEngine.Events.InvokableCall`1::.ctor(UnityEngine.Events.UnityAction`1<T1>)
extern const MethodInfo InvokableCall_1__ctor_m5336_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &InvokableCall_1_t1128_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_1_t1128_InvokableCall_1__ctor_m5336_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2213/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectU5BU5D_t224_0_0_0;
static const ParameterInfo InvokableCall_1_t1128_InvokableCall_1_Invoke_m5337_ParameterInfos[] = 
{
	{"args", 0, 134220253, 0, &ObjectU5BU5D_t224_0_0_0},
};
// System.Void UnityEngine.Events.InvokableCall`1::Invoke(System.Object[])
extern const MethodInfo InvokableCall_1_Invoke_m5337_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &InvokableCall_1_t1128_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_1_t1128_InvokableCall_1_Invoke_m5337_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2214/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo InvokableCall_1_t1128_InvokableCall_1_Find_m5338_ParameterInfos[] = 
{
	{"targetObj", 0, 134220254, 0, &Object_t_0_0_0},
	{"method", 1, 134220255, 0, &MethodInfo_t_0_0_0},
};
// System.Boolean UnityEngine.Events.InvokableCall`1::Find(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo InvokableCall_1_Find_m5338_MethodInfo = 
{
	"Find"/* name */
	, NULL/* method */
	, &InvokableCall_1_t1128_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_1_t1128_InvokableCall_1_Find_m5338_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2215/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* InvokableCall_1_t1128_MethodInfos[] =
{
	&InvokableCall_1__ctor_m5335_MethodInfo,
	&InvokableCall_1__ctor_m5336_MethodInfo,
	&InvokableCall_1_Invoke_m5337_MethodInfo,
	&InvokableCall_1_Find_m5338_MethodInfo,
	NULL
};
extern const MethodInfo InvokableCall_1_Invoke_m5337_MethodInfo;
extern const MethodInfo InvokableCall_1_Find_m5338_MethodInfo;
static const Il2CppMethodReference InvokableCall_1_t1128_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&InvokableCall_1_Invoke_m5337_MethodInfo,
	&InvokableCall_1_Find_m5338_MethodInfo,
};
static bool InvokableCall_1_t1128_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppGenericMethod BaseInvokableCall_ThrowOnInvalidArg_TisT1_t1218_m5442_GenericMethod;
extern const Il2CppType InvokableCall_1_t1128_gp_0_0_0_0;
extern const Il2CppGenericMethod UnityAction_1_Invoke_m5443_GenericMethod;
static Il2CppRGCTXDefinition InvokableCall_1_t1128_RGCTXData[6] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&UnityAction_1_t1219_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&UnityAction_1_t1219_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &BaseInvokableCall_ThrowOnInvalidArg_TisT1_t1218_m5442_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&InvokableCall_1_t1128_gp_0_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &UnityAction_1_Invoke_m5443_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType InvokableCall_1_t1128_0_0_0;
extern const Il2CppType InvokableCall_1_t1128_1_0_0;
struct InvokableCall_1_t1128;
const Il2CppTypeDefinitionMetadata InvokableCall_1_t1128_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &BaseInvokableCall_t1002_0_0_0/* parent */
	, InvokableCall_1_t1128_VTable/* vtableMethods */
	, InvokableCall_1_t1128_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, InvokableCall_1_t1128_RGCTXData/* rgctxDefinition */
	, 1451/* fieldStart */

};
TypeInfo InvokableCall_1_t1128_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t1128_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &InvokableCall_1_t1128_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &InvokableCall_1_t1128_0_0_0/* byval_arg */
	, &InvokableCall_1_t1128_1_0_0/* this_arg */
	, &InvokableCall_1_t1128_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &InvokableCall_1_t1128_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.InvokableCall`2
extern TypeInfo InvokableCall_2_t1129_il2cpp_TypeInfo;
extern const Il2CppGenericContainer InvokableCall_2_t1129_Il2CppGenericContainer;
extern TypeInfo InvokableCall_2_t1129_gp_T1_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter InvokableCall_2_t1129_gp_T1_0_il2cpp_TypeInfo_GenericParamFull = { &InvokableCall_2_t1129_Il2CppGenericContainer, NULL, "T1", 0, 0 };
extern TypeInfo InvokableCall_2_t1129_gp_T2_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter InvokableCall_2_t1129_gp_T2_1_il2cpp_TypeInfo_GenericParamFull = { &InvokableCall_2_t1129_Il2CppGenericContainer, NULL, "T2", 1, 0 };
static const Il2CppGenericParameter* InvokableCall_2_t1129_Il2CppGenericParametersArray[2] = 
{
	&InvokableCall_2_t1129_gp_T1_0_il2cpp_TypeInfo_GenericParamFull,
	&InvokableCall_2_t1129_gp_T2_1_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer InvokableCall_2_t1129_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&InvokableCall_2_t1129_il2cpp_TypeInfo, 2, 0, InvokableCall_2_t1129_Il2CppGenericParametersArray };
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo InvokableCall_2_t1129_InvokableCall_2__ctor_m5339_ParameterInfos[] = 
{
	{"target", 0, 134220256, 0, &Object_t_0_0_0},
	{"theFunction", 1, 134220257, 0, &MethodInfo_t_0_0_0},
};
// System.Void UnityEngine.Events.InvokableCall`2::.ctor(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo InvokableCall_2__ctor_m5339_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &InvokableCall_2_t1129_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_2_t1129_InvokableCall_2__ctor_m5339_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2216/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectU5BU5D_t224_0_0_0;
static const ParameterInfo InvokableCall_2_t1129_InvokableCall_2_Invoke_m5340_ParameterInfos[] = 
{
	{"args", 0, 134220258, 0, &ObjectU5BU5D_t224_0_0_0},
};
// System.Void UnityEngine.Events.InvokableCall`2::Invoke(System.Object[])
extern const MethodInfo InvokableCall_2_Invoke_m5340_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &InvokableCall_2_t1129_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_2_t1129_InvokableCall_2_Invoke_m5340_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2217/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo InvokableCall_2_t1129_InvokableCall_2_Find_m5341_ParameterInfos[] = 
{
	{"targetObj", 0, 134220259, 0, &Object_t_0_0_0},
	{"method", 1, 134220260, 0, &MethodInfo_t_0_0_0},
};
// System.Boolean UnityEngine.Events.InvokableCall`2::Find(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo InvokableCall_2_Find_m5341_MethodInfo = 
{
	"Find"/* name */
	, NULL/* method */
	, &InvokableCall_2_t1129_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_2_t1129_InvokableCall_2_Find_m5341_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2218/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* InvokableCall_2_t1129_MethodInfos[] =
{
	&InvokableCall_2__ctor_m5339_MethodInfo,
	&InvokableCall_2_Invoke_m5340_MethodInfo,
	&InvokableCall_2_Find_m5341_MethodInfo,
	NULL
};
extern const MethodInfo InvokableCall_2_Invoke_m5340_MethodInfo;
extern const MethodInfo InvokableCall_2_Find_m5341_MethodInfo;
static const Il2CppMethodReference InvokableCall_2_t1129_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&InvokableCall_2_Invoke_m5340_MethodInfo,
	&InvokableCall_2_Find_m5341_MethodInfo,
};
static bool InvokableCall_2_t1129_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType UnityAction_2_t1222_0_0_0;
extern const Il2CppGenericMethod BaseInvokableCall_ThrowOnInvalidArg_TisT1_t1220_m5444_GenericMethod;
extern const Il2CppGenericMethod BaseInvokableCall_ThrowOnInvalidArg_TisT2_t1221_m5445_GenericMethod;
extern const Il2CppType InvokableCall_2_t1129_gp_0_0_0_0;
extern const Il2CppType InvokableCall_2_t1129_gp_1_0_0_0;
extern const Il2CppGenericMethod UnityAction_2_Invoke_m5446_GenericMethod;
static Il2CppRGCTXDefinition InvokableCall_2_t1129_RGCTXData[8] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&UnityAction_2_t1222_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&UnityAction_2_t1222_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &BaseInvokableCall_ThrowOnInvalidArg_TisT1_t1220_m5444_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &BaseInvokableCall_ThrowOnInvalidArg_TisT2_t1221_m5445_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&InvokableCall_2_t1129_gp_0_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&InvokableCall_2_t1129_gp_1_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &UnityAction_2_Invoke_m5446_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType InvokableCall_2_t1129_0_0_0;
extern const Il2CppType InvokableCall_2_t1129_1_0_0;
struct InvokableCall_2_t1129;
const Il2CppTypeDefinitionMetadata InvokableCall_2_t1129_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &BaseInvokableCall_t1002_0_0_0/* parent */
	, InvokableCall_2_t1129_VTable/* vtableMethods */
	, InvokableCall_2_t1129_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, InvokableCall_2_t1129_RGCTXData/* rgctxDefinition */
	, 1452/* fieldStart */

};
TypeInfo InvokableCall_2_t1129_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`2"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_2_t1129_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &InvokableCall_2_t1129_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &InvokableCall_2_t1129_0_0_0/* byval_arg */
	, &InvokableCall_2_t1129_1_0_0/* this_arg */
	, &InvokableCall_2_t1129_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &InvokableCall_2_t1129_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.InvokableCall`3
extern TypeInfo InvokableCall_3_t1130_il2cpp_TypeInfo;
extern const Il2CppGenericContainer InvokableCall_3_t1130_Il2CppGenericContainer;
extern TypeInfo InvokableCall_3_t1130_gp_T1_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter InvokableCall_3_t1130_gp_T1_0_il2cpp_TypeInfo_GenericParamFull = { &InvokableCall_3_t1130_Il2CppGenericContainer, NULL, "T1", 0, 0 };
extern TypeInfo InvokableCall_3_t1130_gp_T2_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter InvokableCall_3_t1130_gp_T2_1_il2cpp_TypeInfo_GenericParamFull = { &InvokableCall_3_t1130_Il2CppGenericContainer, NULL, "T2", 1, 0 };
extern TypeInfo InvokableCall_3_t1130_gp_T3_2_il2cpp_TypeInfo;
extern const Il2CppGenericParameter InvokableCall_3_t1130_gp_T3_2_il2cpp_TypeInfo_GenericParamFull = { &InvokableCall_3_t1130_Il2CppGenericContainer, NULL, "T3", 2, 0 };
static const Il2CppGenericParameter* InvokableCall_3_t1130_Il2CppGenericParametersArray[3] = 
{
	&InvokableCall_3_t1130_gp_T1_0_il2cpp_TypeInfo_GenericParamFull,
	&InvokableCall_3_t1130_gp_T2_1_il2cpp_TypeInfo_GenericParamFull,
	&InvokableCall_3_t1130_gp_T3_2_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer InvokableCall_3_t1130_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&InvokableCall_3_t1130_il2cpp_TypeInfo, 3, 0, InvokableCall_3_t1130_Il2CppGenericParametersArray };
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo InvokableCall_3_t1130_InvokableCall_3__ctor_m5342_ParameterInfos[] = 
{
	{"target", 0, 134220261, 0, &Object_t_0_0_0},
	{"theFunction", 1, 134220262, 0, &MethodInfo_t_0_0_0},
};
// System.Void UnityEngine.Events.InvokableCall`3::.ctor(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo InvokableCall_3__ctor_m5342_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &InvokableCall_3_t1130_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_3_t1130_InvokableCall_3__ctor_m5342_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2219/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectU5BU5D_t224_0_0_0;
static const ParameterInfo InvokableCall_3_t1130_InvokableCall_3_Invoke_m5343_ParameterInfos[] = 
{
	{"args", 0, 134220263, 0, &ObjectU5BU5D_t224_0_0_0},
};
// System.Void UnityEngine.Events.InvokableCall`3::Invoke(System.Object[])
extern const MethodInfo InvokableCall_3_Invoke_m5343_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &InvokableCall_3_t1130_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_3_t1130_InvokableCall_3_Invoke_m5343_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2220/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo InvokableCall_3_t1130_InvokableCall_3_Find_m5344_ParameterInfos[] = 
{
	{"targetObj", 0, 134220264, 0, &Object_t_0_0_0},
	{"method", 1, 134220265, 0, &MethodInfo_t_0_0_0},
};
// System.Boolean UnityEngine.Events.InvokableCall`3::Find(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo InvokableCall_3_Find_m5344_MethodInfo = 
{
	"Find"/* name */
	, NULL/* method */
	, &InvokableCall_3_t1130_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_3_t1130_InvokableCall_3_Find_m5344_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2221/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* InvokableCall_3_t1130_MethodInfos[] =
{
	&InvokableCall_3__ctor_m5342_MethodInfo,
	&InvokableCall_3_Invoke_m5343_MethodInfo,
	&InvokableCall_3_Find_m5344_MethodInfo,
	NULL
};
extern const MethodInfo InvokableCall_3_Invoke_m5343_MethodInfo;
extern const MethodInfo InvokableCall_3_Find_m5344_MethodInfo;
static const Il2CppMethodReference InvokableCall_3_t1130_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&InvokableCall_3_Invoke_m5343_MethodInfo,
	&InvokableCall_3_Find_m5344_MethodInfo,
};
static bool InvokableCall_3_t1130_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType UnityAction_3_t1226_0_0_0;
extern const Il2CppGenericMethod BaseInvokableCall_ThrowOnInvalidArg_TisT1_t1223_m5447_GenericMethod;
extern const Il2CppGenericMethod BaseInvokableCall_ThrowOnInvalidArg_TisT2_t1224_m5448_GenericMethod;
extern const Il2CppGenericMethod BaseInvokableCall_ThrowOnInvalidArg_TisT3_t1225_m5449_GenericMethod;
extern const Il2CppType InvokableCall_3_t1130_gp_0_0_0_0;
extern const Il2CppType InvokableCall_3_t1130_gp_1_0_0_0;
extern const Il2CppType InvokableCall_3_t1130_gp_2_0_0_0;
extern const Il2CppGenericMethod UnityAction_3_Invoke_m5450_GenericMethod;
static Il2CppRGCTXDefinition InvokableCall_3_t1130_RGCTXData[10] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&UnityAction_3_t1226_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&UnityAction_3_t1226_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &BaseInvokableCall_ThrowOnInvalidArg_TisT1_t1223_m5447_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &BaseInvokableCall_ThrowOnInvalidArg_TisT2_t1224_m5448_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &BaseInvokableCall_ThrowOnInvalidArg_TisT3_t1225_m5449_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&InvokableCall_3_t1130_gp_0_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&InvokableCall_3_t1130_gp_1_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&InvokableCall_3_t1130_gp_2_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &UnityAction_3_Invoke_m5450_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType InvokableCall_3_t1130_0_0_0;
extern const Il2CppType InvokableCall_3_t1130_1_0_0;
struct InvokableCall_3_t1130;
const Il2CppTypeDefinitionMetadata InvokableCall_3_t1130_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &BaseInvokableCall_t1002_0_0_0/* parent */
	, InvokableCall_3_t1130_VTable/* vtableMethods */
	, InvokableCall_3_t1130_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, InvokableCall_3_t1130_RGCTXData/* rgctxDefinition */
	, 1453/* fieldStart */

};
TypeInfo InvokableCall_3_t1130_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`3"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_3_t1130_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &InvokableCall_3_t1130_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &InvokableCall_3_t1130_0_0_0/* byval_arg */
	, &InvokableCall_3_t1130_1_0_0/* this_arg */
	, &InvokableCall_3_t1130_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &InvokableCall_3_t1130_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.InvokableCall`4
extern TypeInfo InvokableCall_4_t1131_il2cpp_TypeInfo;
extern const Il2CppGenericContainer InvokableCall_4_t1131_Il2CppGenericContainer;
extern TypeInfo InvokableCall_4_t1131_gp_T1_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter InvokableCall_4_t1131_gp_T1_0_il2cpp_TypeInfo_GenericParamFull = { &InvokableCall_4_t1131_Il2CppGenericContainer, NULL, "T1", 0, 0 };
extern TypeInfo InvokableCall_4_t1131_gp_T2_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter InvokableCall_4_t1131_gp_T2_1_il2cpp_TypeInfo_GenericParamFull = { &InvokableCall_4_t1131_Il2CppGenericContainer, NULL, "T2", 1, 0 };
extern TypeInfo InvokableCall_4_t1131_gp_T3_2_il2cpp_TypeInfo;
extern const Il2CppGenericParameter InvokableCall_4_t1131_gp_T3_2_il2cpp_TypeInfo_GenericParamFull = { &InvokableCall_4_t1131_Il2CppGenericContainer, NULL, "T3", 2, 0 };
extern TypeInfo InvokableCall_4_t1131_gp_T4_3_il2cpp_TypeInfo;
extern const Il2CppGenericParameter InvokableCall_4_t1131_gp_T4_3_il2cpp_TypeInfo_GenericParamFull = { &InvokableCall_4_t1131_Il2CppGenericContainer, NULL, "T4", 3, 0 };
static const Il2CppGenericParameter* InvokableCall_4_t1131_Il2CppGenericParametersArray[4] = 
{
	&InvokableCall_4_t1131_gp_T1_0_il2cpp_TypeInfo_GenericParamFull,
	&InvokableCall_4_t1131_gp_T2_1_il2cpp_TypeInfo_GenericParamFull,
	&InvokableCall_4_t1131_gp_T3_2_il2cpp_TypeInfo_GenericParamFull,
	&InvokableCall_4_t1131_gp_T4_3_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer InvokableCall_4_t1131_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&InvokableCall_4_t1131_il2cpp_TypeInfo, 4, 0, InvokableCall_4_t1131_Il2CppGenericParametersArray };
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo InvokableCall_4_t1131_InvokableCall_4__ctor_m5345_ParameterInfos[] = 
{
	{"target", 0, 134220266, 0, &Object_t_0_0_0},
	{"theFunction", 1, 134220267, 0, &MethodInfo_t_0_0_0},
};
// System.Void UnityEngine.Events.InvokableCall`4::.ctor(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo InvokableCall_4__ctor_m5345_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &InvokableCall_4_t1131_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_4_t1131_InvokableCall_4__ctor_m5345_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2222/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectU5BU5D_t224_0_0_0;
static const ParameterInfo InvokableCall_4_t1131_InvokableCall_4_Invoke_m5346_ParameterInfos[] = 
{
	{"args", 0, 134220268, 0, &ObjectU5BU5D_t224_0_0_0},
};
// System.Void UnityEngine.Events.InvokableCall`4::Invoke(System.Object[])
extern const MethodInfo InvokableCall_4_Invoke_m5346_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &InvokableCall_4_t1131_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_4_t1131_InvokableCall_4_Invoke_m5346_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2223/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo InvokableCall_4_t1131_InvokableCall_4_Find_m5347_ParameterInfos[] = 
{
	{"targetObj", 0, 134220269, 0, &Object_t_0_0_0},
	{"method", 1, 134220270, 0, &MethodInfo_t_0_0_0},
};
// System.Boolean UnityEngine.Events.InvokableCall`4::Find(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo InvokableCall_4_Find_m5347_MethodInfo = 
{
	"Find"/* name */
	, NULL/* method */
	, &InvokableCall_4_t1131_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_4_t1131_InvokableCall_4_Find_m5347_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2224/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* InvokableCall_4_t1131_MethodInfos[] =
{
	&InvokableCall_4__ctor_m5345_MethodInfo,
	&InvokableCall_4_Invoke_m5346_MethodInfo,
	&InvokableCall_4_Find_m5347_MethodInfo,
	NULL
};
extern const MethodInfo InvokableCall_4_Invoke_m5346_MethodInfo;
extern const MethodInfo InvokableCall_4_Find_m5347_MethodInfo;
static const Il2CppMethodReference InvokableCall_4_t1131_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&InvokableCall_4_Invoke_m5346_MethodInfo,
	&InvokableCall_4_Find_m5347_MethodInfo,
};
static bool InvokableCall_4_t1131_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType UnityAction_4_t1231_0_0_0;
extern const Il2CppGenericMethod BaseInvokableCall_ThrowOnInvalidArg_TisT1_t1227_m5451_GenericMethod;
extern const Il2CppGenericMethod BaseInvokableCall_ThrowOnInvalidArg_TisT2_t1228_m5452_GenericMethod;
extern const Il2CppGenericMethod BaseInvokableCall_ThrowOnInvalidArg_TisT3_t1229_m5453_GenericMethod;
extern const Il2CppGenericMethod BaseInvokableCall_ThrowOnInvalidArg_TisT4_t1230_m5454_GenericMethod;
extern const Il2CppType InvokableCall_4_t1131_gp_0_0_0_0;
extern const Il2CppType InvokableCall_4_t1131_gp_1_0_0_0;
extern const Il2CppType InvokableCall_4_t1131_gp_2_0_0_0;
extern const Il2CppType InvokableCall_4_t1131_gp_3_0_0_0;
extern const Il2CppGenericMethod UnityAction_4_Invoke_m5455_GenericMethod;
static Il2CppRGCTXDefinition InvokableCall_4_t1131_RGCTXData[12] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&UnityAction_4_t1231_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&UnityAction_4_t1231_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &BaseInvokableCall_ThrowOnInvalidArg_TisT1_t1227_m5451_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &BaseInvokableCall_ThrowOnInvalidArg_TisT2_t1228_m5452_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &BaseInvokableCall_ThrowOnInvalidArg_TisT3_t1229_m5453_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &BaseInvokableCall_ThrowOnInvalidArg_TisT4_t1230_m5454_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&InvokableCall_4_t1131_gp_0_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&InvokableCall_4_t1131_gp_1_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&InvokableCall_4_t1131_gp_2_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&InvokableCall_4_t1131_gp_3_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &UnityAction_4_Invoke_m5455_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType InvokableCall_4_t1131_0_0_0;
extern const Il2CppType InvokableCall_4_t1131_1_0_0;
struct InvokableCall_4_t1131;
const Il2CppTypeDefinitionMetadata InvokableCall_4_t1131_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &BaseInvokableCall_t1002_0_0_0/* parent */
	, InvokableCall_4_t1131_VTable/* vtableMethods */
	, InvokableCall_4_t1131_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, InvokableCall_4_t1131_RGCTXData/* rgctxDefinition */
	, 1454/* fieldStart */

};
TypeInfo InvokableCall_4_t1131_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`4"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_4_t1131_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &InvokableCall_4_t1131_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &InvokableCall_4_t1131_0_0_0/* byval_arg */
	, &InvokableCall_4_t1131_1_0_0/* this_arg */
	, &InvokableCall_4_t1131_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &InvokableCall_4_t1131_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1
extern TypeInfo CachedInvokableCall_1_t1107_il2cpp_TypeInfo;
extern const Il2CppGenericContainer CachedInvokableCall_1_t1107_Il2CppGenericContainer;
extern TypeInfo CachedInvokableCall_1_t1107_gp_T_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter CachedInvokableCall_1_t1107_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &CachedInvokableCall_1_t1107_Il2CppGenericContainer, NULL, "T", 0, 0 };
static const Il2CppGenericParameter* CachedInvokableCall_1_t1107_Il2CppGenericParametersArray[1] = 
{
	&CachedInvokableCall_1_t1107_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer CachedInvokableCall_1_t1107_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&CachedInvokableCall_1_t1107_il2cpp_TypeInfo, 1, 0, CachedInvokableCall_1_t1107_Il2CppGenericParametersArray };
extern const Il2CppType Object_t164_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
extern const Il2CppType CachedInvokableCall_1_t1107_gp_0_0_0_0;
extern const Il2CppType CachedInvokableCall_1_t1107_gp_0_0_0_0;
static const ParameterInfo CachedInvokableCall_1_t1107_CachedInvokableCall_1__ctor_m5348_ParameterInfos[] = 
{
	{"target", 0, 134220271, 0, &Object_t164_0_0_0},
	{"theFunction", 1, 134220272, 0, &MethodInfo_t_0_0_0},
	{"argument", 2, 134220273, 0, &CachedInvokableCall_1_t1107_gp_0_0_0_0},
};
// System.Void UnityEngine.Events.CachedInvokableCall`1::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
extern const MethodInfo CachedInvokableCall_1__ctor_m5348_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &CachedInvokableCall_1_t1107_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, NULL/* invoker_method */
	, CachedInvokableCall_1_t1107_CachedInvokableCall_1__ctor_m5348_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2225/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectU5BU5D_t224_0_0_0;
static const ParameterInfo CachedInvokableCall_1_t1107_CachedInvokableCall_1_Invoke_m5349_ParameterInfos[] = 
{
	{"args", 0, 134220274, 0, &ObjectU5BU5D_t224_0_0_0},
};
// System.Void UnityEngine.Events.CachedInvokableCall`1::Invoke(System.Object[])
extern const MethodInfo CachedInvokableCall_1_Invoke_m5349_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &CachedInvokableCall_1_t1107_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, NULL/* invoker_method */
	, CachedInvokableCall_1_t1107_CachedInvokableCall_1_Invoke_m5349_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2226/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CachedInvokableCall_1_t1107_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m5348_MethodInfo,
	&CachedInvokableCall_1_Invoke_m5349_MethodInfo,
	NULL
};
extern const MethodInfo CachedInvokableCall_1_Invoke_m5349_MethodInfo;
extern const Il2CppGenericMethod InvokableCall_1_Find_m5456_GenericMethod;
static const Il2CppMethodReference CachedInvokableCall_1_t1107_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&CachedInvokableCall_1_Invoke_m5349_MethodInfo,
	&InvokableCall_1_Find_m5456_GenericMethod,
};
static bool CachedInvokableCall_1_t1107_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	true,
};
extern const Il2CppGenericMethod InvokableCall_1__ctor_m5457_GenericMethod;
extern const Il2CppGenericMethod InvokableCall_1_Invoke_m5458_GenericMethod;
static Il2CppRGCTXDefinition CachedInvokableCall_1_t1107_RGCTXData[4] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, &InvokableCall_1__ctor_m5457_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&CachedInvokableCall_1_t1107_gp_0_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &InvokableCall_1_Invoke_m5458_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType CachedInvokableCall_1_t1107_0_0_0;
extern const Il2CppType CachedInvokableCall_1_t1107_1_0_0;
extern const Il2CppType InvokableCall_1_t1233_0_0_0;
struct CachedInvokableCall_1_t1107;
const Il2CppTypeDefinitionMetadata CachedInvokableCall_1_t1107_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &InvokableCall_1_t1233_0_0_0/* parent */
	, CachedInvokableCall_1_t1107_VTable/* vtableMethods */
	, CachedInvokableCall_1_t1107_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, CachedInvokableCall_1_t1107_RGCTXData/* rgctxDefinition */
	, 1455/* fieldStart */

};
TypeInfo CachedInvokableCall_1_t1107_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t1107_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CachedInvokableCall_1_t1107_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CachedInvokableCall_1_t1107_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t1107_1_0_0/* this_arg */
	, &CachedInvokableCall_1_t1107_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &CachedInvokableCall_1_t1107_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.UnityEventCallState
#include "UnityEngine_UnityEngine_Events_UnityEventCallState.h"
// Metadata Definition UnityEngine.Events.UnityEventCallState
extern TypeInfo UnityEventCallState_t1004_il2cpp_TypeInfo;
// UnityEngine.Events.UnityEventCallState
#include "UnityEngine_UnityEngine_Events_UnityEventCallStateMethodDeclarations.h"
static const MethodInfo* UnityEventCallState_t1004_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference UnityEventCallState_t1004_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool UnityEventCallState_t1004_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UnityEventCallState_t1004_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityEventCallState_t1004_0_0_0;
extern const Il2CppType UnityEventCallState_t1004_1_0_0;
const Il2CppTypeDefinitionMetadata UnityEventCallState_t1004_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityEventCallState_t1004_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, UnityEventCallState_t1004_VTable/* vtableMethods */
	, UnityEventCallState_t1004_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1456/* fieldStart */

};
TypeInfo UnityEventCallState_t1004_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityEventCallState"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityEventCallState_t1004_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityEventCallState_t1004_0_0_0/* byval_arg */
	, &UnityEventCallState_t1004_1_0_0/* this_arg */
	, &UnityEventCallState_t1004_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityEventCallState_t1004)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UnityEventCallState_t1004)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.Events.PersistentCall
#include "UnityEngine_UnityEngine_Events_PersistentCall.h"
// Metadata Definition UnityEngine.Events.PersistentCall
extern TypeInfo PersistentCall_t1005_il2cpp_TypeInfo;
// UnityEngine.Events.PersistentCall
#include "UnityEngine_UnityEngine_Events_PersistentCallMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.PersistentCall::.ctor()
extern const MethodInfo PersistentCall__ctor_m5049_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PersistentCall__ctor_m5049/* method */
	, &PersistentCall_t1005_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2227/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Object UnityEngine.Events.PersistentCall::get_target()
extern const MethodInfo PersistentCall_get_target_m5050_MethodInfo = 
{
	"get_target"/* name */
	, (methodPointerType)&PersistentCall_get_target_m5050/* method */
	, &PersistentCall_t1005_il2cpp_TypeInfo/* declaring_type */
	, &Object_t164_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2228/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Events.PersistentCall::get_methodName()
extern const MethodInfo PersistentCall_get_methodName_m5051_MethodInfo = 
{
	"get_methodName"/* name */
	, (methodPointerType)&PersistentCall_get_methodName_m5051/* method */
	, &PersistentCall_t1005_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2229/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_PersistentListenerMode_t1000 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Events.PersistentListenerMode UnityEngine.Events.PersistentCall::get_mode()
extern const MethodInfo PersistentCall_get_mode_m5052_MethodInfo = 
{
	"get_mode"/* name */
	, (methodPointerType)&PersistentCall_get_mode_m5052/* method */
	, &PersistentCall_t1005_il2cpp_TypeInfo/* declaring_type */
	, &PersistentListenerMode_t1000_0_0_0/* return_type */
	, RuntimeInvoker_PersistentListenerMode_t1000/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2230/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Events.ArgumentCache UnityEngine.Events.PersistentCall::get_arguments()
extern const MethodInfo PersistentCall_get_arguments_m5053_MethodInfo = 
{
	"get_arguments"/* name */
	, (methodPointerType)&PersistentCall_get_arguments_m5053/* method */
	, &PersistentCall_t1005_il2cpp_TypeInfo/* declaring_type */
	, &ArgumentCache_t1001_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2231/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Events.PersistentCall::IsValid()
extern const MethodInfo PersistentCall_IsValid_m5054_MethodInfo = 
{
	"IsValid"/* name */
	, (methodPointerType)&PersistentCall_IsValid_m5054/* method */
	, &PersistentCall_t1005_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2232/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UnityEventBase_t1010_0_0_0;
extern const Il2CppType UnityEventBase_t1010_0_0_0;
static const ParameterInfo PersistentCall_t1005_PersistentCall_GetRuntimeCall_m5055_ParameterInfos[] = 
{
	{"theEvent", 0, 134220275, 0, &UnityEventBase_t1010_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.PersistentCall::GetRuntimeCall(UnityEngine.Events.UnityEventBase)
extern const MethodInfo PersistentCall_GetRuntimeCall_m5055_MethodInfo = 
{
	"GetRuntimeCall"/* name */
	, (methodPointerType)&PersistentCall_GetRuntimeCall_m5055/* method */
	, &PersistentCall_t1005_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t1002_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, PersistentCall_t1005_PersistentCall_GetRuntimeCall_m5055_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2233/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t164_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
extern const Il2CppType ArgumentCache_t1001_0_0_0;
static const ParameterInfo PersistentCall_t1005_PersistentCall_GetObjectCall_m5056_ParameterInfos[] = 
{
	{"target", 0, 134220276, 0, &Object_t164_0_0_0},
	{"method", 1, 134220277, 0, &MethodInfo_t_0_0_0},
	{"arguments", 2, 134220278, 0, &ArgumentCache_t1001_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.PersistentCall::GetObjectCall(UnityEngine.Object,System.Reflection.MethodInfo,UnityEngine.Events.ArgumentCache)
extern const MethodInfo PersistentCall_GetObjectCall_m5056_MethodInfo = 
{
	"GetObjectCall"/* name */
	, (methodPointerType)&PersistentCall_GetObjectCall_m5056/* method */
	, &PersistentCall_t1005_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t1002_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, PersistentCall_t1005_PersistentCall_GetObjectCall_m5056_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2234/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* PersistentCall_t1005_MethodInfos[] =
{
	&PersistentCall__ctor_m5049_MethodInfo,
	&PersistentCall_get_target_m5050_MethodInfo,
	&PersistentCall_get_methodName_m5051_MethodInfo,
	&PersistentCall_get_mode_m5052_MethodInfo,
	&PersistentCall_get_arguments_m5053_MethodInfo,
	&PersistentCall_IsValid_m5054_MethodInfo,
	&PersistentCall_GetRuntimeCall_m5055_MethodInfo,
	&PersistentCall_GetObjectCall_m5056_MethodInfo,
	NULL
};
extern const MethodInfo PersistentCall_get_target_m5050_MethodInfo;
static const PropertyInfo PersistentCall_t1005____target_PropertyInfo = 
{
	&PersistentCall_t1005_il2cpp_TypeInfo/* parent */
	, "target"/* name */
	, &PersistentCall_get_target_m5050_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo PersistentCall_get_methodName_m5051_MethodInfo;
static const PropertyInfo PersistentCall_t1005____methodName_PropertyInfo = 
{
	&PersistentCall_t1005_il2cpp_TypeInfo/* parent */
	, "methodName"/* name */
	, &PersistentCall_get_methodName_m5051_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo PersistentCall_get_mode_m5052_MethodInfo;
static const PropertyInfo PersistentCall_t1005____mode_PropertyInfo = 
{
	&PersistentCall_t1005_il2cpp_TypeInfo/* parent */
	, "mode"/* name */
	, &PersistentCall_get_mode_m5052_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo PersistentCall_get_arguments_m5053_MethodInfo;
static const PropertyInfo PersistentCall_t1005____arguments_PropertyInfo = 
{
	&PersistentCall_t1005_il2cpp_TypeInfo/* parent */
	, "arguments"/* name */
	, &PersistentCall_get_arguments_m5053_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* PersistentCall_t1005_PropertyInfos[] =
{
	&PersistentCall_t1005____target_PropertyInfo,
	&PersistentCall_t1005____methodName_PropertyInfo,
	&PersistentCall_t1005____mode_PropertyInfo,
	&PersistentCall_t1005____arguments_PropertyInfo,
	NULL
};
static const Il2CppMethodReference PersistentCall_t1005_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool PersistentCall_t1005_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType PersistentCall_t1005_0_0_0;
extern const Il2CppType PersistentCall_t1005_1_0_0;
struct PersistentCall_t1005;
const Il2CppTypeDefinitionMetadata PersistentCall_t1005_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, PersistentCall_t1005_VTable/* vtableMethods */
	, PersistentCall_t1005_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1460/* fieldStart */

};
TypeInfo PersistentCall_t1005_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "PersistentCall"/* name */
	, "UnityEngine.Events"/* namespaze */
	, PersistentCall_t1005_MethodInfos/* methods */
	, PersistentCall_t1005_PropertyInfos/* properties */
	, NULL/* events */
	, &PersistentCall_t1005_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PersistentCall_t1005_0_0_0/* byval_arg */
	, &PersistentCall_t1005_1_0_0/* this_arg */
	, &PersistentCall_t1005_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PersistentCall_t1005)/* instance_size */
	, sizeof (PersistentCall_t1005)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 4/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.PersistentCallGroup
#include "UnityEngine_UnityEngine_Events_PersistentCallGroup.h"
// Metadata Definition UnityEngine.Events.PersistentCallGroup
extern TypeInfo PersistentCallGroup_t1007_il2cpp_TypeInfo;
// UnityEngine.Events.PersistentCallGroup
#include "UnityEngine_UnityEngine_Events_PersistentCallGroupMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.PersistentCallGroup::.ctor()
extern const MethodInfo PersistentCallGroup__ctor_m5057_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PersistentCallGroup__ctor_m5057/* method */
	, &PersistentCallGroup_t1007_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2235/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType InvokableCallList_t1009_0_0_0;
extern const Il2CppType InvokableCallList_t1009_0_0_0;
extern const Il2CppType UnityEventBase_t1010_0_0_0;
static const ParameterInfo PersistentCallGroup_t1007_PersistentCallGroup_Initialize_m5058_ParameterInfos[] = 
{
	{"invokableList", 0, 134220279, 0, &InvokableCallList_t1009_0_0_0},
	{"unityEventBase", 1, 134220280, 0, &UnityEventBase_t1010_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.PersistentCallGroup::Initialize(UnityEngine.Events.InvokableCallList,UnityEngine.Events.UnityEventBase)
extern const MethodInfo PersistentCallGroup_Initialize_m5058_MethodInfo = 
{
	"Initialize"/* name */
	, (methodPointerType)&PersistentCallGroup_Initialize_m5058/* method */
	, &PersistentCallGroup_t1007_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, PersistentCallGroup_t1007_PersistentCallGroup_Initialize_m5058_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2236/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* PersistentCallGroup_t1007_MethodInfos[] =
{
	&PersistentCallGroup__ctor_m5057_MethodInfo,
	&PersistentCallGroup_Initialize_m5058_MethodInfo,
	NULL
};
static const Il2CppMethodReference PersistentCallGroup_t1007_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool PersistentCallGroup_t1007_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType PersistentCallGroup_t1007_0_0_0;
extern const Il2CppType PersistentCallGroup_t1007_1_0_0;
struct PersistentCallGroup_t1007;
const Il2CppTypeDefinitionMetadata PersistentCallGroup_t1007_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, PersistentCallGroup_t1007_VTable/* vtableMethods */
	, PersistentCallGroup_t1007_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1465/* fieldStart */

};
TypeInfo PersistentCallGroup_t1007_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "PersistentCallGroup"/* name */
	, "UnityEngine.Events"/* namespaze */
	, PersistentCallGroup_t1007_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &PersistentCallGroup_t1007_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PersistentCallGroup_t1007_0_0_0/* byval_arg */
	, &PersistentCallGroup_t1007_1_0_0/* this_arg */
	, &PersistentCallGroup_t1007_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PersistentCallGroup_t1007)/* instance_size */
	, sizeof (PersistentCallGroup_t1007)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.InvokableCallList
#include "UnityEngine_UnityEngine_Events_InvokableCallList.h"
// Metadata Definition UnityEngine.Events.InvokableCallList
extern TypeInfo InvokableCallList_t1009_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCallList
#include "UnityEngine_UnityEngine_Events_InvokableCallListMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.InvokableCallList::.ctor()
extern const MethodInfo InvokableCallList__ctor_m5059_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCallList__ctor_m5059/* method */
	, &InvokableCallList_t1009_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2237/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BaseInvokableCall_t1002_0_0_0;
static const ParameterInfo InvokableCallList_t1009_InvokableCallList_AddPersistentInvokableCall_m5060_ParameterInfos[] = 
{
	{"call", 0, 134220281, 0, &BaseInvokableCall_t1002_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.InvokableCallList::AddPersistentInvokableCall(UnityEngine.Events.BaseInvokableCall)
extern const MethodInfo InvokableCallList_AddPersistentInvokableCall_m5060_MethodInfo = 
{
	"AddPersistentInvokableCall"/* name */
	, (methodPointerType)&InvokableCallList_AddPersistentInvokableCall_m5060/* method */
	, &InvokableCallList_t1009_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, InvokableCallList_t1009_InvokableCallList_AddPersistentInvokableCall_m5060_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2238/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BaseInvokableCall_t1002_0_0_0;
static const ParameterInfo InvokableCallList_t1009_InvokableCallList_AddListener_m5061_ParameterInfos[] = 
{
	{"call", 0, 134220282, 0, &BaseInvokableCall_t1002_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.InvokableCallList::AddListener(UnityEngine.Events.BaseInvokableCall)
extern const MethodInfo InvokableCallList_AddListener_m5061_MethodInfo = 
{
	"AddListener"/* name */
	, (methodPointerType)&InvokableCallList_AddListener_m5061/* method */
	, &InvokableCallList_t1009_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, InvokableCallList_t1009_InvokableCallList_AddListener_m5061_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2239/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo InvokableCallList_t1009_InvokableCallList_RemoveListener_m5062_ParameterInfos[] = 
{
	{"targetObj", 0, 134220283, 0, &Object_t_0_0_0},
	{"method", 1, 134220284, 0, &MethodInfo_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.InvokableCallList::RemoveListener(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo InvokableCallList_RemoveListener_m5062_MethodInfo = 
{
	"RemoveListener"/* name */
	, (methodPointerType)&InvokableCallList_RemoveListener_m5062/* method */
	, &InvokableCallList_t1009_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, InvokableCallList_t1009_InvokableCallList_RemoveListener_m5062_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2240/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.InvokableCallList::ClearPersistent()
extern const MethodInfo InvokableCallList_ClearPersistent_m5063_MethodInfo = 
{
	"ClearPersistent"/* name */
	, (methodPointerType)&InvokableCallList_ClearPersistent_m5063/* method */
	, &InvokableCallList_t1009_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2241/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectU5BU5D_t224_0_0_0;
static const ParameterInfo InvokableCallList_t1009_InvokableCallList_Invoke_m5064_ParameterInfos[] = 
{
	{"parameters", 0, 134220285, 0, &ObjectU5BU5D_t224_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.InvokableCallList::Invoke(System.Object[])
extern const MethodInfo InvokableCallList_Invoke_m5064_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCallList_Invoke_m5064/* method */
	, &InvokableCallList_t1009_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, InvokableCallList_t1009_InvokableCallList_Invoke_m5064_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2242/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* InvokableCallList_t1009_MethodInfos[] =
{
	&InvokableCallList__ctor_m5059_MethodInfo,
	&InvokableCallList_AddPersistentInvokableCall_m5060_MethodInfo,
	&InvokableCallList_AddListener_m5061_MethodInfo,
	&InvokableCallList_RemoveListener_m5062_MethodInfo,
	&InvokableCallList_ClearPersistent_m5063_MethodInfo,
	&InvokableCallList_Invoke_m5064_MethodInfo,
	NULL
};
static const Il2CppMethodReference InvokableCallList_t1009_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool InvokableCallList_t1009_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType InvokableCallList_t1009_1_0_0;
struct InvokableCallList_t1009;
const Il2CppTypeDefinitionMetadata InvokableCallList_t1009_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, InvokableCallList_t1009_VTable/* vtableMethods */
	, InvokableCallList_t1009_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1466/* fieldStart */

};
TypeInfo InvokableCallList_t1009_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCallList"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCallList_t1009_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &InvokableCallList_t1009_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &InvokableCallList_t1009_0_0_0/* byval_arg */
	, &InvokableCallList_t1009_1_0_0/* this_arg */
	, &InvokableCallList_t1009_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCallList_t1009)/* instance_size */
	, sizeof (InvokableCallList_t1009)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.UnityEventBase
#include "UnityEngine_UnityEngine_Events_UnityEventBase.h"
// Metadata Definition UnityEngine.Events.UnityEventBase
extern TypeInfo UnityEventBase_t1010_il2cpp_TypeInfo;
// UnityEngine.Events.UnityEventBase
#include "UnityEngine_UnityEngine_Events_UnityEventBaseMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEventBase::.ctor()
extern const MethodInfo UnityEventBase__ctor_m5065_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityEventBase__ctor_m5065/* method */
	, &UnityEventBase_t1010_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2243/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEventBase::UnityEngine.ISerializationCallbackReceiver.OnBeforeSerialize()
extern const MethodInfo UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m3582_MethodInfo = 
{
	"UnityEngine.ISerializationCallbackReceiver.OnBeforeSerialize"/* name */
	, (methodPointerType)&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m3582/* method */
	, &UnityEventBase_t1010_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2244/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEventBase::UnityEngine.ISerializationCallbackReceiver.OnAfterDeserialize()
extern const MethodInfo UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m3583_MethodInfo = 
{
	"UnityEngine.ISerializationCallbackReceiver.OnAfterDeserialize"/* name */
	, (methodPointerType)&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m3583/* method */
	, &UnityEventBase_t1010_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2245/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo UnityEventBase_t1010_UnityEventBase_FindMethod_Impl_m5350_ParameterInfos[] = 
{
	{"name", 0, 134220286, 0, &String_t_0_0_0},
	{"targetObj", 1, 134220287, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::FindMethod_Impl(System.String,System.Object)
extern const MethodInfo UnityEventBase_FindMethod_Impl_m5350_MethodInfo = 
{
	"FindMethod_Impl"/* name */
	, NULL/* method */
	, &UnityEventBase_t1010_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, UnityEventBase_t1010_UnityEventBase_FindMethod_Impl_m5350_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1476/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2246/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo UnityEventBase_t1010_UnityEventBase_GetDelegate_m5351_ParameterInfos[] = 
{
	{"target", 0, 134220288, 0, &Object_t_0_0_0},
	{"theFunction", 1, 134220289, 0, &MethodInfo_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEventBase::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo UnityEventBase_GetDelegate_m5351_MethodInfo = 
{
	"GetDelegate"/* name */
	, NULL/* method */
	, &UnityEventBase_t1010_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t1002_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, UnityEventBase_t1010_UnityEventBase_GetDelegate_m5351_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1475/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2247/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PersistentCall_t1005_0_0_0;
static const ParameterInfo UnityEventBase_t1010_UnityEventBase_FindMethod_m5066_ParameterInfos[] = 
{
	{"call", 0, 134220290, 0, &PersistentCall_t1005_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::FindMethod(UnityEngine.Events.PersistentCall)
extern const MethodInfo UnityEventBase_FindMethod_m5066_MethodInfo = 
{
	"FindMethod"/* name */
	, (methodPointerType)&UnityEventBase_FindMethod_m5066/* method */
	, &UnityEventBase_t1010_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, UnityEventBase_t1010_UnityEventBase_FindMethod_m5066_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2248/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType PersistentListenerMode_t1000_0_0_0;
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo UnityEventBase_t1010_UnityEventBase_FindMethod_m5067_ParameterInfos[] = 
{
	{"name", 0, 134220291, 0, &String_t_0_0_0},
	{"listener", 1, 134220292, 0, &Object_t_0_0_0},
	{"mode", 2, 134220293, 0, &PersistentListenerMode_t1000_0_0_0},
	{"argumentType", 3, 134220294, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::FindMethod(System.String,System.Object,UnityEngine.Events.PersistentListenerMode,System.Type)
extern const MethodInfo UnityEventBase_FindMethod_m5067_MethodInfo = 
{
	"FindMethod"/* name */
	, (methodPointerType)&UnityEventBase_FindMethod_m5067/* method */
	, &UnityEventBase_t1010_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t253_Object_t/* invoker_method */
	, UnityEventBase_t1010_UnityEventBase_FindMethod_m5067_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2249/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEventBase::DirtyPersistentCalls()
extern const MethodInfo UnityEventBase_DirtyPersistentCalls_m5068_MethodInfo = 
{
	"DirtyPersistentCalls"/* name */
	, (methodPointerType)&UnityEventBase_DirtyPersistentCalls_m5068/* method */
	, &UnityEventBase_t1010_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2250/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEventBase::RebuildPersistentCallsIfNeeded()
extern const MethodInfo UnityEventBase_RebuildPersistentCallsIfNeeded_m5069_MethodInfo = 
{
	"RebuildPersistentCallsIfNeeded"/* name */
	, (methodPointerType)&UnityEventBase_RebuildPersistentCallsIfNeeded_m5069/* method */
	, &UnityEventBase_t1010_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2251/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BaseInvokableCall_t1002_0_0_0;
static const ParameterInfo UnityEventBase_t1010_UnityEventBase_AddCall_m5070_ParameterInfos[] = 
{
	{"call", 0, 134220295, 0, &BaseInvokableCall_t1002_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEventBase::AddCall(UnityEngine.Events.BaseInvokableCall)
extern const MethodInfo UnityEventBase_AddCall_m5070_MethodInfo = 
{
	"AddCall"/* name */
	, (methodPointerType)&UnityEventBase_AddCall_m5070/* method */
	, &UnityEventBase_t1010_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, UnityEventBase_t1010_UnityEventBase_AddCall_m5070_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2252/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo UnityEventBase_t1010_UnityEventBase_RemoveListener_m5071_ParameterInfos[] = 
{
	{"targetObj", 0, 134220296, 0, &Object_t_0_0_0},
	{"method", 1, 134220297, 0, &MethodInfo_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEventBase::RemoveListener(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo UnityEventBase_RemoveListener_m5071_MethodInfo = 
{
	"RemoveListener"/* name */
	, (methodPointerType)&UnityEventBase_RemoveListener_m5071/* method */
	, &UnityEventBase_t1010_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, UnityEventBase_t1010_UnityEventBase_RemoveListener_m5071_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2253/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectU5BU5D_t224_0_0_0;
static const ParameterInfo UnityEventBase_t1010_UnityEventBase_Invoke_m5072_ParameterInfos[] = 
{
	{"parameters", 0, 134220298, 0, &ObjectU5BU5D_t224_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEventBase::Invoke(System.Object[])
extern const MethodInfo UnityEventBase_Invoke_m5072_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityEventBase_Invoke_m5072/* method */
	, &UnityEventBase_t1010_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, UnityEventBase_t1010_UnityEventBase_Invoke_m5072_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2254/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Events.UnityEventBase::ToString()
extern const MethodInfo UnityEventBase_ToString_m3581_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&UnityEventBase_ToString_m3581/* method */
	, &UnityEventBase_t1010_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2255/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType TypeU5BU5D_t238_0_0_0;
extern const Il2CppType TypeU5BU5D_t238_0_0_0;
static const ParameterInfo UnityEventBase_t1010_UnityEventBase_GetValidMethodInfo_m5073_ParameterInfos[] = 
{
	{"obj", 0, 134220299, 0, &Object_t_0_0_0},
	{"functionName", 1, 134220300, 0, &String_t_0_0_0},
	{"argumentTypes", 2, 134220301, 0, &TypeU5BU5D_t238_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::GetValidMethodInfo(System.Object,System.String,System.Type[])
extern const MethodInfo UnityEventBase_GetValidMethodInfo_m5073_MethodInfo = 
{
	"GetValidMethodInfo"/* name */
	, (methodPointerType)&UnityEventBase_GetValidMethodInfo_m5073/* method */
	, &UnityEventBase_t1010_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityEventBase_t1010_UnityEventBase_GetValidMethodInfo_m5073_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2256/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UnityEventBase_t1010_MethodInfos[] =
{
	&UnityEventBase__ctor_m5065_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m3582_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m3583_MethodInfo,
	&UnityEventBase_FindMethod_Impl_m5350_MethodInfo,
	&UnityEventBase_GetDelegate_m5351_MethodInfo,
	&UnityEventBase_FindMethod_m5066_MethodInfo,
	&UnityEventBase_FindMethod_m5067_MethodInfo,
	&UnityEventBase_DirtyPersistentCalls_m5068_MethodInfo,
	&UnityEventBase_RebuildPersistentCallsIfNeeded_m5069_MethodInfo,
	&UnityEventBase_AddCall_m5070_MethodInfo,
	&UnityEventBase_RemoveListener_m5071_MethodInfo,
	&UnityEventBase_Invoke_m5072_MethodInfo,
	&UnityEventBase_ToString_m3581_MethodInfo,
	&UnityEventBase_GetValidMethodInfo_m5073_MethodInfo,
	NULL
};
extern const MethodInfo UnityEventBase_ToString_m3581_MethodInfo;
extern const MethodInfo UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m3582_MethodInfo;
extern const MethodInfo UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m3583_MethodInfo;
static const Il2CppMethodReference UnityEventBase_t1010_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&UnityEventBase_ToString_m3581_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m3582_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m3583_MethodInfo,
	NULL,
	NULL,
};
static bool UnityEventBase_t1010_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* UnityEventBase_t1010_InterfacesTypeInfos[] = 
{
	&ISerializationCallbackReceiver_t731_0_0_0,
};
static Il2CppInterfaceOffsetPair UnityEventBase_t1010_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t731_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityEventBase_t1010_1_0_0;
struct UnityEventBase_t1010;
const Il2CppTypeDefinitionMetadata UnityEventBase_t1010_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, UnityEventBase_t1010_InterfacesTypeInfos/* implementedInterfaces */
	, UnityEventBase_t1010_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, UnityEventBase_t1010_VTable/* vtableMethods */
	, UnityEventBase_t1010_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1469/* fieldStart */

};
TypeInfo UnityEventBase_t1010_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityEventBase"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityEventBase_t1010_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UnityEventBase_t1010_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityEventBase_t1010_0_0_0/* byval_arg */
	, &UnityEventBase_t1010_1_0_0/* this_arg */
	, &UnityEventBase_t1010_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityEventBase_t1010)/* instance_size */
	, sizeof (UnityEventBase_t1010)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 14/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.Events.UnityEvent
#include "UnityEngine_UnityEngine_Events_UnityEvent.h"
// Metadata Definition UnityEngine.Events.UnityEvent
extern TypeInfo UnityEvent_t508_il2cpp_TypeInfo;
// UnityEngine.Events.UnityEvent
#include "UnityEngine_UnityEngine_Events_UnityEventMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEvent::.ctor()
extern const MethodInfo UnityEvent__ctor_m3153_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityEvent__ctor_m3153/* method */
	, &UnityEvent_t508_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2257/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UnityAction_t416_0_0_0;
static const ParameterInfo UnityEvent_t508_UnityEvent_AddListener_m1726_ParameterInfos[] = 
{
	{"call", 0, 134220302, 0, &UnityAction_t416_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEvent::AddListener(UnityEngine.Events.UnityAction)
extern const MethodInfo UnityEvent_AddListener_m1726_MethodInfo = 
{
	"AddListener"/* name */
	, (methodPointerType)&UnityEvent_AddListener_m1726/* method */
	, &UnityEvent_t508_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, UnityEvent_t508_UnityEvent_AddListener_m1726_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2258/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UnityAction_t416_0_0_0;
static const ParameterInfo UnityEvent_t508_UnityEvent_RemoveListener_m1727_ParameterInfos[] = 
{
	{"call", 0, 134220303, 0, &UnityAction_t416_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEvent::RemoveListener(UnityEngine.Events.UnityAction)
extern const MethodInfo UnityEvent_RemoveListener_m1727_MethodInfo = 
{
	"RemoveListener"/* name */
	, (methodPointerType)&UnityEvent_RemoveListener_m1727/* method */
	, &UnityEvent_t508_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, UnityEvent_t508_UnityEvent_RemoveListener_m1727_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2259/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo UnityEvent_t508_UnityEvent_FindMethod_Impl_m3603_ParameterInfos[] = 
{
	{"name", 0, 134220304, 0, &String_t_0_0_0},
	{"targetObj", 1, 134220305, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent::FindMethod_Impl(System.String,System.Object)
extern const MethodInfo UnityEvent_FindMethod_Impl_m3603_MethodInfo = 
{
	"FindMethod_Impl"/* name */
	, (methodPointerType)&UnityEvent_FindMethod_Impl_m3603/* method */
	, &UnityEvent_t508_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, UnityEvent_t508_UnityEvent_FindMethod_Impl_m3603_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2260/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo UnityEvent_t508_UnityEvent_GetDelegate_m3604_ParameterInfos[] = 
{
	{"target", 0, 134220306, 0, &Object_t_0_0_0},
	{"theFunction", 1, 134220307, 0, &MethodInfo_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo UnityEvent_GetDelegate_m3604_MethodInfo = 
{
	"GetDelegate"/* name */
	, (methodPointerType)&UnityEvent_GetDelegate_m3604/* method */
	, &UnityEvent_t508_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t1002_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, UnityEvent_t508_UnityEvent_GetDelegate_m3604_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 195/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2261/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UnityAction_t416_0_0_0;
static const ParameterInfo UnityEvent_t508_UnityEvent_GetDelegate_m5074_ParameterInfos[] = 
{
	{"action", 0, 134220308, 0, &UnityAction_t416_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent::GetDelegate(UnityEngine.Events.UnityAction)
extern const MethodInfo UnityEvent_GetDelegate_m5074_MethodInfo = 
{
	"GetDelegate"/* name */
	, (methodPointerType)&UnityEvent_GetDelegate_m5074/* method */
	, &UnityEvent_t508_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t1002_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, UnityEvent_t508_UnityEvent_GetDelegate_m5074_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2262/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEvent::Invoke()
extern const MethodInfo UnityEvent_Invoke_m3155_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityEvent_Invoke_m3155/* method */
	, &UnityEvent_t508_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2263/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UnityEvent_t508_MethodInfos[] =
{
	&UnityEvent__ctor_m3153_MethodInfo,
	&UnityEvent_AddListener_m1726_MethodInfo,
	&UnityEvent_RemoveListener_m1727_MethodInfo,
	&UnityEvent_FindMethod_Impl_m3603_MethodInfo,
	&UnityEvent_GetDelegate_m3604_MethodInfo,
	&UnityEvent_GetDelegate_m5074_MethodInfo,
	&UnityEvent_Invoke_m3155_MethodInfo,
	NULL
};
extern const MethodInfo UnityEvent_FindMethod_Impl_m3603_MethodInfo;
extern const MethodInfo UnityEvent_GetDelegate_m3604_MethodInfo;
static const Il2CppMethodReference UnityEvent_t508_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&UnityEventBase_ToString_m3581_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m3582_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m3583_MethodInfo,
	&UnityEvent_FindMethod_Impl_m3603_MethodInfo,
	&UnityEvent_GetDelegate_m3604_MethodInfo,
};
static bool UnityEvent_t508_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UnityEvent_t508_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t731_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityEvent_t508_0_0_0;
extern const Il2CppType UnityEvent_t508_1_0_0;
struct UnityEvent_t508;
const Il2CppTypeDefinitionMetadata UnityEvent_t508_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityEvent_t508_InterfacesOffsets/* interfaceOffsets */
	, &UnityEventBase_t1010_0_0_0/* parent */
	, UnityEvent_t508_VTable/* vtableMethods */
	, UnityEvent_t508_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1473/* fieldStart */

};
TypeInfo UnityEvent_t508_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityEvent"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityEvent_t508_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UnityEvent_t508_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityEvent_t508_0_0_0/* byval_arg */
	, &UnityEvent_t508_1_0_0/* this_arg */
	, &UnityEvent_t508_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityEvent_t508)/* instance_size */
	, sizeof (UnityEvent_t508)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.UnityEvent`1
extern TypeInfo UnityEvent_1_t1132_il2cpp_TypeInfo;
extern const Il2CppGenericContainer UnityEvent_1_t1132_Il2CppGenericContainer;
extern TypeInfo UnityEvent_1_t1132_gp_T0_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityEvent_1_t1132_gp_T0_0_il2cpp_TypeInfo_GenericParamFull = { &UnityEvent_1_t1132_Il2CppGenericContainer, NULL, "T0", 0, 0 };
static const Il2CppGenericParameter* UnityEvent_1_t1132_Il2CppGenericParametersArray[1] = 
{
	&UnityEvent_1_t1132_gp_T0_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer UnityEvent_1_t1132_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&UnityEvent_1_t1132_il2cpp_TypeInfo, 1, 0, UnityEvent_1_t1132_Il2CppGenericParametersArray };
// System.Void UnityEngine.Events.UnityEvent`1::.ctor()
extern const MethodInfo UnityEvent_1__ctor_m5352_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &UnityEvent_1_t1132_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2264/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UnityAction_1_t1235_0_0_0;
extern const Il2CppType UnityAction_1_t1235_0_0_0;
static const ParameterInfo UnityEvent_1_t1132_UnityEvent_1_AddListener_m5353_ParameterInfos[] = 
{
	{"call", 0, 134220309, 0, &UnityAction_1_t1235_0_0_0},
};
// System.Void UnityEngine.Events.UnityEvent`1::AddListener(UnityEngine.Events.UnityAction`1<T0>)
extern const MethodInfo UnityEvent_1_AddListener_m5353_MethodInfo = 
{
	"AddListener"/* name */
	, NULL/* method */
	, &UnityEvent_1_t1132_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_1_t1132_UnityEvent_1_AddListener_m5353_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2265/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UnityAction_1_t1235_0_0_0;
static const ParameterInfo UnityEvent_1_t1132_UnityEvent_1_RemoveListener_m5354_ParameterInfos[] = 
{
	{"call", 0, 134220310, 0, &UnityAction_1_t1235_0_0_0},
};
// System.Void UnityEngine.Events.UnityEvent`1::RemoveListener(UnityEngine.Events.UnityAction`1<T0>)
extern const MethodInfo UnityEvent_1_RemoveListener_m5354_MethodInfo = 
{
	"RemoveListener"/* name */
	, NULL/* method */
	, &UnityEvent_1_t1132_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_1_t1132_UnityEvent_1_RemoveListener_m5354_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2266/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo UnityEvent_1_t1132_UnityEvent_1_FindMethod_Impl_m5355_ParameterInfos[] = 
{
	{"name", 0, 134220311, 0, &String_t_0_0_0},
	{"targetObj", 1, 134220312, 0, &Object_t_0_0_0},
};
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`1::FindMethod_Impl(System.String,System.Object)
extern const MethodInfo UnityEvent_1_FindMethod_Impl_m5355_MethodInfo = 
{
	"FindMethod_Impl"/* name */
	, NULL/* method */
	, &UnityEvent_1_t1132_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_1_t1132_UnityEvent_1_FindMethod_Impl_m5355_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2267/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo UnityEvent_1_t1132_UnityEvent_1_GetDelegate_m5356_ParameterInfos[] = 
{
	{"target", 0, 134220313, 0, &Object_t_0_0_0},
	{"theFunction", 1, 134220314, 0, &MethodInfo_t_0_0_0},
};
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo UnityEvent_1_GetDelegate_m5356_MethodInfo = 
{
	"GetDelegate"/* name */
	, NULL/* method */
	, &UnityEvent_1_t1132_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t1002_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_1_t1132_UnityEvent_1_GetDelegate_m5356_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 195/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2268/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UnityAction_1_t1235_0_0_0;
static const ParameterInfo UnityEvent_1_t1132_UnityEvent_1_GetDelegate_m5357_ParameterInfos[] = 
{
	{"action", 0, 134220315, 0, &UnityAction_1_t1235_0_0_0},
};
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1::GetDelegate(UnityEngine.Events.UnityAction`1<T0>)
extern const MethodInfo UnityEvent_1_GetDelegate_m5357_MethodInfo = 
{
	"GetDelegate"/* name */
	, NULL/* method */
	, &UnityEvent_1_t1132_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t1002_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_1_t1132_UnityEvent_1_GetDelegate_m5357_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2269/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UnityEvent_1_t1132_gp_0_0_0_0;
extern const Il2CppType UnityEvent_1_t1132_gp_0_0_0_0;
static const ParameterInfo UnityEvent_1_t1132_UnityEvent_1_Invoke_m5358_ParameterInfos[] = 
{
	{"arg0", 0, 134220316, 0, &UnityEvent_1_t1132_gp_0_0_0_0},
};
// System.Void UnityEngine.Events.UnityEvent`1::Invoke(T0)
extern const MethodInfo UnityEvent_1_Invoke_m5358_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &UnityEvent_1_t1132_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_1_t1132_UnityEvent_1_Invoke_m5358_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2270/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UnityEvent_1_t1132_MethodInfos[] =
{
	&UnityEvent_1__ctor_m5352_MethodInfo,
	&UnityEvent_1_AddListener_m5353_MethodInfo,
	&UnityEvent_1_RemoveListener_m5354_MethodInfo,
	&UnityEvent_1_FindMethod_Impl_m5355_MethodInfo,
	&UnityEvent_1_GetDelegate_m5356_MethodInfo,
	&UnityEvent_1_GetDelegate_m5357_MethodInfo,
	&UnityEvent_1_Invoke_m5358_MethodInfo,
	NULL
};
extern const MethodInfo UnityEvent_1_FindMethod_Impl_m5355_MethodInfo;
extern const MethodInfo UnityEvent_1_GetDelegate_m5356_MethodInfo;
static const Il2CppMethodReference UnityEvent_1_t1132_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&UnityEventBase_ToString_m3581_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m3582_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m3583_MethodInfo,
	&UnityEvent_1_FindMethod_Impl_m5355_MethodInfo,
	&UnityEvent_1_GetDelegate_m5356_MethodInfo,
};
static bool UnityEvent_1_t1132_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UnityEvent_1_t1132_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t731_0_0_0, 4},
};
extern const Il2CppGenericMethod UnityEvent_1_GetDelegate_m5459_GenericMethod;
extern const Il2CppType InvokableCall_1_t1236_0_0_0;
extern const Il2CppGenericMethod InvokableCall_1__ctor_m5460_GenericMethod;
extern const Il2CppGenericMethod InvokableCall_1__ctor_m5461_GenericMethod;
static Il2CppRGCTXDefinition UnityEvent_1_t1132_RGCTXData[7] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, &UnityEvent_1_GetDelegate_m5459_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&UnityEvent_1_t1132_gp_0_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&InvokableCall_1_t1236_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &InvokableCall_1__ctor_m5460_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &InvokableCall_1__ctor_m5461_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&UnityEvent_1_t1132_gp_0_0_0_0 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityEvent_1_t1132_0_0_0;
extern const Il2CppType UnityEvent_1_t1132_1_0_0;
struct UnityEvent_1_t1132;
const Il2CppTypeDefinitionMetadata UnityEvent_1_t1132_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityEvent_1_t1132_InterfacesOffsets/* interfaceOffsets */
	, &UnityEventBase_t1010_0_0_0/* parent */
	, UnityEvent_1_t1132_VTable/* vtableMethods */
	, UnityEvent_1_t1132_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, UnityEvent_1_t1132_RGCTXData/* rgctxDefinition */
	, 1474/* fieldStart */

};
TypeInfo UnityEvent_1_t1132_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityEvent`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityEvent_1_t1132_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UnityEvent_1_t1132_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityEvent_1_t1132_0_0_0/* byval_arg */
	, &UnityEvent_1_t1132_1_0_0/* this_arg */
	, &UnityEvent_1_t1132_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &UnityEvent_1_t1132_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.UnityEvent`2
extern TypeInfo UnityEvent_2_t1133_il2cpp_TypeInfo;
extern const Il2CppGenericContainer UnityEvent_2_t1133_Il2CppGenericContainer;
extern TypeInfo UnityEvent_2_t1133_gp_T0_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityEvent_2_t1133_gp_T0_0_il2cpp_TypeInfo_GenericParamFull = { &UnityEvent_2_t1133_Il2CppGenericContainer, NULL, "T0", 0, 0 };
extern TypeInfo UnityEvent_2_t1133_gp_T1_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityEvent_2_t1133_gp_T1_1_il2cpp_TypeInfo_GenericParamFull = { &UnityEvent_2_t1133_Il2CppGenericContainer, NULL, "T1", 1, 0 };
static const Il2CppGenericParameter* UnityEvent_2_t1133_Il2CppGenericParametersArray[2] = 
{
	&UnityEvent_2_t1133_gp_T0_0_il2cpp_TypeInfo_GenericParamFull,
	&UnityEvent_2_t1133_gp_T1_1_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer UnityEvent_2_t1133_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&UnityEvent_2_t1133_il2cpp_TypeInfo, 2, 0, UnityEvent_2_t1133_Il2CppGenericParametersArray };
// System.Void UnityEngine.Events.UnityEvent`2::.ctor()
extern const MethodInfo UnityEvent_2__ctor_m5359_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &UnityEvent_2_t1133_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2271/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo UnityEvent_2_t1133_UnityEvent_2_FindMethod_Impl_m5360_ParameterInfos[] = 
{
	{"name", 0, 134220317, 0, &String_t_0_0_0},
	{"targetObj", 1, 134220318, 0, &Object_t_0_0_0},
};
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`2::FindMethod_Impl(System.String,System.Object)
extern const MethodInfo UnityEvent_2_FindMethod_Impl_m5360_MethodInfo = 
{
	"FindMethod_Impl"/* name */
	, NULL/* method */
	, &UnityEvent_2_t1133_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_2_t1133_UnityEvent_2_FindMethod_Impl_m5360_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2272/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo UnityEvent_2_t1133_UnityEvent_2_GetDelegate_m5361_ParameterInfos[] = 
{
	{"target", 0, 134220319, 0, &Object_t_0_0_0},
	{"theFunction", 1, 134220320, 0, &MethodInfo_t_0_0_0},
};
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`2::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo UnityEvent_2_GetDelegate_m5361_MethodInfo = 
{
	"GetDelegate"/* name */
	, NULL/* method */
	, &UnityEvent_2_t1133_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t1002_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_2_t1133_UnityEvent_2_GetDelegate_m5361_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 195/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2273/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UnityEvent_2_t1133_MethodInfos[] =
{
	&UnityEvent_2__ctor_m5359_MethodInfo,
	&UnityEvent_2_FindMethod_Impl_m5360_MethodInfo,
	&UnityEvent_2_GetDelegate_m5361_MethodInfo,
	NULL
};
extern const MethodInfo UnityEvent_2_FindMethod_Impl_m5360_MethodInfo;
extern const MethodInfo UnityEvent_2_GetDelegate_m5361_MethodInfo;
static const Il2CppMethodReference UnityEvent_2_t1133_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&UnityEventBase_ToString_m3581_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m3582_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m3583_MethodInfo,
	&UnityEvent_2_FindMethod_Impl_m5360_MethodInfo,
	&UnityEvent_2_GetDelegate_m5361_MethodInfo,
};
static bool UnityEvent_2_t1133_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UnityEvent_2_t1133_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t731_0_0_0, 4},
};
extern const Il2CppType UnityEvent_2_t1133_gp_0_0_0_0;
extern const Il2CppType UnityEvent_2_t1133_gp_1_0_0_0;
extern const Il2CppType InvokableCall_2_t1239_0_0_0;
extern const Il2CppGenericMethod InvokableCall_2__ctor_m5462_GenericMethod;
static Il2CppRGCTXDefinition UnityEvent_2_t1133_RGCTXData[5] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&UnityEvent_2_t1133_gp_0_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&UnityEvent_2_t1133_gp_1_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&InvokableCall_2_t1239_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &InvokableCall_2__ctor_m5462_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityEvent_2_t1133_0_0_0;
extern const Il2CppType UnityEvent_2_t1133_1_0_0;
struct UnityEvent_2_t1133;
const Il2CppTypeDefinitionMetadata UnityEvent_2_t1133_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityEvent_2_t1133_InterfacesOffsets/* interfaceOffsets */
	, &UnityEventBase_t1010_0_0_0/* parent */
	, UnityEvent_2_t1133_VTable/* vtableMethods */
	, UnityEvent_2_t1133_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, UnityEvent_2_t1133_RGCTXData/* rgctxDefinition */
	, 1475/* fieldStart */

};
TypeInfo UnityEvent_2_t1133_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityEvent`2"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityEvent_2_t1133_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UnityEvent_2_t1133_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityEvent_2_t1133_0_0_0/* byval_arg */
	, &UnityEvent_2_t1133_1_0_0/* this_arg */
	, &UnityEvent_2_t1133_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &UnityEvent_2_t1133_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.UnityEvent`3
extern TypeInfo UnityEvent_3_t1134_il2cpp_TypeInfo;
extern const Il2CppGenericContainer UnityEvent_3_t1134_Il2CppGenericContainer;
extern TypeInfo UnityEvent_3_t1134_gp_T0_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityEvent_3_t1134_gp_T0_0_il2cpp_TypeInfo_GenericParamFull = { &UnityEvent_3_t1134_Il2CppGenericContainer, NULL, "T0", 0, 0 };
extern TypeInfo UnityEvent_3_t1134_gp_T1_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityEvent_3_t1134_gp_T1_1_il2cpp_TypeInfo_GenericParamFull = { &UnityEvent_3_t1134_Il2CppGenericContainer, NULL, "T1", 1, 0 };
extern TypeInfo UnityEvent_3_t1134_gp_T2_2_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityEvent_3_t1134_gp_T2_2_il2cpp_TypeInfo_GenericParamFull = { &UnityEvent_3_t1134_Il2CppGenericContainer, NULL, "T2", 2, 0 };
static const Il2CppGenericParameter* UnityEvent_3_t1134_Il2CppGenericParametersArray[3] = 
{
	&UnityEvent_3_t1134_gp_T0_0_il2cpp_TypeInfo_GenericParamFull,
	&UnityEvent_3_t1134_gp_T1_1_il2cpp_TypeInfo_GenericParamFull,
	&UnityEvent_3_t1134_gp_T2_2_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer UnityEvent_3_t1134_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&UnityEvent_3_t1134_il2cpp_TypeInfo, 3, 0, UnityEvent_3_t1134_Il2CppGenericParametersArray };
// System.Void UnityEngine.Events.UnityEvent`3::.ctor()
extern const MethodInfo UnityEvent_3__ctor_m5362_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &UnityEvent_3_t1134_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2274/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo UnityEvent_3_t1134_UnityEvent_3_FindMethod_Impl_m5363_ParameterInfos[] = 
{
	{"name", 0, 134220321, 0, &String_t_0_0_0},
	{"targetObj", 1, 134220322, 0, &Object_t_0_0_0},
};
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`3::FindMethod_Impl(System.String,System.Object)
extern const MethodInfo UnityEvent_3_FindMethod_Impl_m5363_MethodInfo = 
{
	"FindMethod_Impl"/* name */
	, NULL/* method */
	, &UnityEvent_3_t1134_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_3_t1134_UnityEvent_3_FindMethod_Impl_m5363_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2275/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo UnityEvent_3_t1134_UnityEvent_3_GetDelegate_m5364_ParameterInfos[] = 
{
	{"target", 0, 134220323, 0, &Object_t_0_0_0},
	{"theFunction", 1, 134220324, 0, &MethodInfo_t_0_0_0},
};
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`3::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo UnityEvent_3_GetDelegate_m5364_MethodInfo = 
{
	"GetDelegate"/* name */
	, NULL/* method */
	, &UnityEvent_3_t1134_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t1002_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_3_t1134_UnityEvent_3_GetDelegate_m5364_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 195/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2276/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UnityEvent_3_t1134_MethodInfos[] =
{
	&UnityEvent_3__ctor_m5362_MethodInfo,
	&UnityEvent_3_FindMethod_Impl_m5363_MethodInfo,
	&UnityEvent_3_GetDelegate_m5364_MethodInfo,
	NULL
};
extern const MethodInfo UnityEvent_3_FindMethod_Impl_m5363_MethodInfo;
extern const MethodInfo UnityEvent_3_GetDelegate_m5364_MethodInfo;
static const Il2CppMethodReference UnityEvent_3_t1134_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&UnityEventBase_ToString_m3581_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m3582_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m3583_MethodInfo,
	&UnityEvent_3_FindMethod_Impl_m5363_MethodInfo,
	&UnityEvent_3_GetDelegate_m5364_MethodInfo,
};
static bool UnityEvent_3_t1134_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UnityEvent_3_t1134_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t731_0_0_0, 4},
};
extern const Il2CppType UnityEvent_3_t1134_gp_0_0_0_0;
extern const Il2CppType UnityEvent_3_t1134_gp_1_0_0_0;
extern const Il2CppType UnityEvent_3_t1134_gp_2_0_0_0;
extern const Il2CppType InvokableCall_3_t1243_0_0_0;
extern const Il2CppGenericMethod InvokableCall_3__ctor_m5463_GenericMethod;
static Il2CppRGCTXDefinition UnityEvent_3_t1134_RGCTXData[6] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&UnityEvent_3_t1134_gp_0_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&UnityEvent_3_t1134_gp_1_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&UnityEvent_3_t1134_gp_2_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&InvokableCall_3_t1243_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &InvokableCall_3__ctor_m5463_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityEvent_3_t1134_0_0_0;
extern const Il2CppType UnityEvent_3_t1134_1_0_0;
struct UnityEvent_3_t1134;
const Il2CppTypeDefinitionMetadata UnityEvent_3_t1134_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityEvent_3_t1134_InterfacesOffsets/* interfaceOffsets */
	, &UnityEventBase_t1010_0_0_0/* parent */
	, UnityEvent_3_t1134_VTable/* vtableMethods */
	, UnityEvent_3_t1134_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, UnityEvent_3_t1134_RGCTXData/* rgctxDefinition */
	, 1476/* fieldStart */

};
TypeInfo UnityEvent_3_t1134_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityEvent`3"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityEvent_3_t1134_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UnityEvent_3_t1134_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityEvent_3_t1134_0_0_0/* byval_arg */
	, &UnityEvent_3_t1134_1_0_0/* this_arg */
	, &UnityEvent_3_t1134_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &UnityEvent_3_t1134_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.UnityEvent`4
extern TypeInfo UnityEvent_4_t1135_il2cpp_TypeInfo;
extern const Il2CppGenericContainer UnityEvent_4_t1135_Il2CppGenericContainer;
extern TypeInfo UnityEvent_4_t1135_gp_T0_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityEvent_4_t1135_gp_T0_0_il2cpp_TypeInfo_GenericParamFull = { &UnityEvent_4_t1135_Il2CppGenericContainer, NULL, "T0", 0, 0 };
extern TypeInfo UnityEvent_4_t1135_gp_T1_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityEvent_4_t1135_gp_T1_1_il2cpp_TypeInfo_GenericParamFull = { &UnityEvent_4_t1135_Il2CppGenericContainer, NULL, "T1", 1, 0 };
extern TypeInfo UnityEvent_4_t1135_gp_T2_2_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityEvent_4_t1135_gp_T2_2_il2cpp_TypeInfo_GenericParamFull = { &UnityEvent_4_t1135_Il2CppGenericContainer, NULL, "T2", 2, 0 };
extern TypeInfo UnityEvent_4_t1135_gp_T3_3_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityEvent_4_t1135_gp_T3_3_il2cpp_TypeInfo_GenericParamFull = { &UnityEvent_4_t1135_Il2CppGenericContainer, NULL, "T3", 3, 0 };
static const Il2CppGenericParameter* UnityEvent_4_t1135_Il2CppGenericParametersArray[4] = 
{
	&UnityEvent_4_t1135_gp_T0_0_il2cpp_TypeInfo_GenericParamFull,
	&UnityEvent_4_t1135_gp_T1_1_il2cpp_TypeInfo_GenericParamFull,
	&UnityEvent_4_t1135_gp_T2_2_il2cpp_TypeInfo_GenericParamFull,
	&UnityEvent_4_t1135_gp_T3_3_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer UnityEvent_4_t1135_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&UnityEvent_4_t1135_il2cpp_TypeInfo, 4, 0, UnityEvent_4_t1135_Il2CppGenericParametersArray };
// System.Void UnityEngine.Events.UnityEvent`4::.ctor()
extern const MethodInfo UnityEvent_4__ctor_m5365_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &UnityEvent_4_t1135_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2277/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo UnityEvent_4_t1135_UnityEvent_4_FindMethod_Impl_m5366_ParameterInfos[] = 
{
	{"name", 0, 134220325, 0, &String_t_0_0_0},
	{"targetObj", 1, 134220326, 0, &Object_t_0_0_0},
};
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`4::FindMethod_Impl(System.String,System.Object)
extern const MethodInfo UnityEvent_4_FindMethod_Impl_m5366_MethodInfo = 
{
	"FindMethod_Impl"/* name */
	, NULL/* method */
	, &UnityEvent_4_t1135_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_4_t1135_UnityEvent_4_FindMethod_Impl_m5366_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2278/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo UnityEvent_4_t1135_UnityEvent_4_GetDelegate_m5367_ParameterInfos[] = 
{
	{"target", 0, 134220327, 0, &Object_t_0_0_0},
	{"theFunction", 1, 134220328, 0, &MethodInfo_t_0_0_0},
};
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`4::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo UnityEvent_4_GetDelegate_m5367_MethodInfo = 
{
	"GetDelegate"/* name */
	, NULL/* method */
	, &UnityEvent_4_t1135_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t1002_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_4_t1135_UnityEvent_4_GetDelegate_m5367_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 195/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2279/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UnityEvent_4_t1135_MethodInfos[] =
{
	&UnityEvent_4__ctor_m5365_MethodInfo,
	&UnityEvent_4_FindMethod_Impl_m5366_MethodInfo,
	&UnityEvent_4_GetDelegate_m5367_MethodInfo,
	NULL
};
extern const MethodInfo UnityEvent_4_FindMethod_Impl_m5366_MethodInfo;
extern const MethodInfo UnityEvent_4_GetDelegate_m5367_MethodInfo;
static const Il2CppMethodReference UnityEvent_4_t1135_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&UnityEventBase_ToString_m3581_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m3582_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m3583_MethodInfo,
	&UnityEvent_4_FindMethod_Impl_m5366_MethodInfo,
	&UnityEvent_4_GetDelegate_m5367_MethodInfo,
};
static bool UnityEvent_4_t1135_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UnityEvent_4_t1135_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t731_0_0_0, 4},
};
extern const Il2CppType UnityEvent_4_t1135_gp_0_0_0_0;
extern const Il2CppType UnityEvent_4_t1135_gp_1_0_0_0;
extern const Il2CppType UnityEvent_4_t1135_gp_2_0_0_0;
extern const Il2CppType UnityEvent_4_t1135_gp_3_0_0_0;
extern const Il2CppType InvokableCall_4_t1248_0_0_0;
extern const Il2CppGenericMethod InvokableCall_4__ctor_m5464_GenericMethod;
static Il2CppRGCTXDefinition UnityEvent_4_t1135_RGCTXData[7] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&UnityEvent_4_t1135_gp_0_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&UnityEvent_4_t1135_gp_1_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&UnityEvent_4_t1135_gp_2_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&UnityEvent_4_t1135_gp_3_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&InvokableCall_4_t1248_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &InvokableCall_4__ctor_m5464_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityEvent_4_t1135_0_0_0;
extern const Il2CppType UnityEvent_4_t1135_1_0_0;
struct UnityEvent_4_t1135;
const Il2CppTypeDefinitionMetadata UnityEvent_4_t1135_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityEvent_4_t1135_InterfacesOffsets/* interfaceOffsets */
	, &UnityEventBase_t1010_0_0_0/* parent */
	, UnityEvent_4_t1135_VTable/* vtableMethods */
	, UnityEvent_4_t1135_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, UnityEvent_4_t1135_RGCTXData/* rgctxDefinition */
	, 1477/* fieldStart */

};
TypeInfo UnityEvent_4_t1135_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityEvent`4"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityEvent_4_t1135_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UnityEvent_4_t1135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityEvent_4_t1135_0_0_0/* byval_arg */
	, &UnityEvent_4_t1135_1_0_0/* this_arg */
	, &UnityEvent_4_t1135_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &UnityEvent_4_t1135_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.UserAuthorizationDialog
#include "UnityEngine_UnityEngine_UserAuthorizationDialog.h"
// Metadata Definition UnityEngine.UserAuthorizationDialog
extern TypeInfo UserAuthorizationDialog_t1011_il2cpp_TypeInfo;
// UnityEngine.UserAuthorizationDialog
#include "UnityEngine_UnityEngine_UserAuthorizationDialogMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UserAuthorizationDialog::.ctor()
extern const MethodInfo UserAuthorizationDialog__ctor_m5075_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UserAuthorizationDialog__ctor_m5075/* method */
	, &UserAuthorizationDialog_t1011_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2280/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UserAuthorizationDialog::Start()
extern const MethodInfo UserAuthorizationDialog_Start_m5076_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&UserAuthorizationDialog_Start_m5076/* method */
	, &UserAuthorizationDialog_t1011_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2281/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UserAuthorizationDialog::OnGUI()
extern const MethodInfo UserAuthorizationDialog_OnGUI_m5077_MethodInfo = 
{
	"OnGUI"/* name */
	, (methodPointerType)&UserAuthorizationDialog_OnGUI_m5077/* method */
	, &UserAuthorizationDialog_t1011_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2282/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo UserAuthorizationDialog_t1011_UserAuthorizationDialog_DoUserAuthorizationDialog_m5078_ParameterInfos[] = 
{
	{"windowID", 0, 134220329, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UserAuthorizationDialog::DoUserAuthorizationDialog(System.Int32)
extern const MethodInfo UserAuthorizationDialog_DoUserAuthorizationDialog_m5078_MethodInfo = 
{
	"DoUserAuthorizationDialog"/* name */
	, (methodPointerType)&UserAuthorizationDialog_DoUserAuthorizationDialog_m5078/* method */
	, &UserAuthorizationDialog_t1011_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, UserAuthorizationDialog_t1011_UserAuthorizationDialog_DoUserAuthorizationDialog_m5078_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2283/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UserAuthorizationDialog_t1011_MethodInfos[] =
{
	&UserAuthorizationDialog__ctor_m5075_MethodInfo,
	&UserAuthorizationDialog_Start_m5076_MethodInfo,
	&UserAuthorizationDialog_OnGUI_m5077_MethodInfo,
	&UserAuthorizationDialog_DoUserAuthorizationDialog_m5078_MethodInfo,
	NULL
};
static const Il2CppMethodReference UserAuthorizationDialog_t1011_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool UserAuthorizationDialog_t1011_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UserAuthorizationDialog_t1011_0_0_0;
extern const Il2CppType UserAuthorizationDialog_t1011_1_0_0;
extern const Il2CppType MonoBehaviour_t3_0_0_0;
struct UserAuthorizationDialog_t1011;
const Il2CppTypeDefinitionMetadata UserAuthorizationDialog_t1011_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, UserAuthorizationDialog_t1011_VTable/* vtableMethods */
	, UserAuthorizationDialog_t1011_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1478/* fieldStart */

};
TypeInfo UserAuthorizationDialog_t1011_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UserAuthorizationDialog"/* name */
	, "UnityEngine"/* namespaze */
	, UserAuthorizationDialog_t1011_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UserAuthorizationDialog_t1011_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1057/* custom_attributes_cache */
	, &UserAuthorizationDialog_t1011_0_0_0/* byval_arg */
	, &UserAuthorizationDialog_t1011_1_0_0/* this_arg */
	, &UserAuthorizationDialog_t1011_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UserAuthorizationDialog_t1011)/* instance_size */
	, sizeof (UserAuthorizationDialog_t1011)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Internal.DefaultValueAttribute
#include "UnityEngine_UnityEngine_Internal_DefaultValueAttribute.h"
// Metadata Definition UnityEngine.Internal.DefaultValueAttribute
extern TypeInfo DefaultValueAttribute_t1012_il2cpp_TypeInfo;
// UnityEngine.Internal.DefaultValueAttribute
#include "UnityEngine_UnityEngine_Internal_DefaultValueAttributeMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo DefaultValueAttribute_t1012_DefaultValueAttribute__ctor_m5079_ParameterInfos[] = 
{
	{"value", 0, 134220330, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Internal.DefaultValueAttribute::.ctor(System.String)
extern const MethodInfo DefaultValueAttribute__ctor_m5079_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DefaultValueAttribute__ctor_m5079/* method */
	, &DefaultValueAttribute_t1012_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, DefaultValueAttribute_t1012_DefaultValueAttribute__ctor_m5079_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2284/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object UnityEngine.Internal.DefaultValueAttribute::get_Value()
extern const MethodInfo DefaultValueAttribute_get_Value_m5080_MethodInfo = 
{
	"get_Value"/* name */
	, (methodPointerType)&DefaultValueAttribute_get_Value_m5080/* method */
	, &DefaultValueAttribute_t1012_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2285/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo DefaultValueAttribute_t1012_DefaultValueAttribute_Equals_m5081_ParameterInfos[] = 
{
	{"obj", 0, 134220331, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Internal.DefaultValueAttribute::Equals(System.Object)
extern const MethodInfo DefaultValueAttribute_Equals_m5081_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&DefaultValueAttribute_Equals_m5081/* method */
	, &DefaultValueAttribute_t1012_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, DefaultValueAttribute_t1012_DefaultValueAttribute_Equals_m5081_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2286/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.Internal.DefaultValueAttribute::GetHashCode()
extern const MethodInfo DefaultValueAttribute_GetHashCode_m5082_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&DefaultValueAttribute_GetHashCode_m5082/* method */
	, &DefaultValueAttribute_t1012_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2287/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* DefaultValueAttribute_t1012_MethodInfos[] =
{
	&DefaultValueAttribute__ctor_m5079_MethodInfo,
	&DefaultValueAttribute_get_Value_m5080_MethodInfo,
	&DefaultValueAttribute_Equals_m5081_MethodInfo,
	&DefaultValueAttribute_GetHashCode_m5082_MethodInfo,
	NULL
};
extern const MethodInfo DefaultValueAttribute_get_Value_m5080_MethodInfo;
static const PropertyInfo DefaultValueAttribute_t1012____Value_PropertyInfo = 
{
	&DefaultValueAttribute_t1012_il2cpp_TypeInfo/* parent */
	, "Value"/* name */
	, &DefaultValueAttribute_get_Value_m5080_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* DefaultValueAttribute_t1012_PropertyInfos[] =
{
	&DefaultValueAttribute_t1012____Value_PropertyInfo,
	NULL
};
extern const MethodInfo DefaultValueAttribute_Equals_m5081_MethodInfo;
extern const MethodInfo DefaultValueAttribute_GetHashCode_m5082_MethodInfo;
static const Il2CppMethodReference DefaultValueAttribute_t1012_VTable[] =
{
	&DefaultValueAttribute_Equals_m5081_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&DefaultValueAttribute_GetHashCode_m5082_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool DefaultValueAttribute_t1012_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair DefaultValueAttribute_t1012_InterfacesOffsets[] = 
{
	{ &_Attribute_t1145_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType DefaultValueAttribute_t1012_0_0_0;
extern const Il2CppType DefaultValueAttribute_t1012_1_0_0;
struct DefaultValueAttribute_t1012;
const Il2CppTypeDefinitionMetadata DefaultValueAttribute_t1012_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DefaultValueAttribute_t1012_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t805_0_0_0/* parent */
	, DefaultValueAttribute_t1012_VTable/* vtableMethods */
	, DefaultValueAttribute_t1012_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1482/* fieldStart */

};
TypeInfo DefaultValueAttribute_t1012_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "DefaultValueAttribute"/* name */
	, "UnityEngine.Internal"/* namespaze */
	, DefaultValueAttribute_t1012_MethodInfos/* methods */
	, DefaultValueAttribute_t1012_PropertyInfos/* properties */
	, NULL/* events */
	, &DefaultValueAttribute_t1012_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1058/* custom_attributes_cache */
	, &DefaultValueAttribute_t1012_0_0_0/* byval_arg */
	, &DefaultValueAttribute_t1012_1_0_0/* this_arg */
	, &DefaultValueAttribute_t1012_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DefaultValueAttribute_t1012)/* instance_size */
	, sizeof (DefaultValueAttribute_t1012)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.Internal.ExcludeFromDocsAttribute
#include "UnityEngine_UnityEngine_Internal_ExcludeFromDocsAttribute.h"
// Metadata Definition UnityEngine.Internal.ExcludeFromDocsAttribute
extern TypeInfo ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo;
// UnityEngine.Internal.ExcludeFromDocsAttribute
#include "UnityEngine_UnityEngine_Internal_ExcludeFromDocsAttributeMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Internal.ExcludeFromDocsAttribute::.ctor()
extern const MethodInfo ExcludeFromDocsAttribute__ctor_m5083_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ExcludeFromDocsAttribute__ctor_m5083/* method */
	, &ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2288/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ExcludeFromDocsAttribute_t1013_MethodInfos[] =
{
	&ExcludeFromDocsAttribute__ctor_m5083_MethodInfo,
	NULL
};
static const Il2CppMethodReference ExcludeFromDocsAttribute_t1013_VTable[] =
{
	&Attribute_Equals_m5384_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Attribute_GetHashCode_m5253_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool ExcludeFromDocsAttribute_t1013_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ExcludeFromDocsAttribute_t1013_InterfacesOffsets[] = 
{
	{ &_Attribute_t1145_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ExcludeFromDocsAttribute_t1013_0_0_0;
extern const Il2CppType ExcludeFromDocsAttribute_t1013_1_0_0;
struct ExcludeFromDocsAttribute_t1013;
const Il2CppTypeDefinitionMetadata ExcludeFromDocsAttribute_t1013_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ExcludeFromDocsAttribute_t1013_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t805_0_0_0/* parent */
	, ExcludeFromDocsAttribute_t1013_VTable/* vtableMethods */
	, ExcludeFromDocsAttribute_t1013_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ExcludeFromDocsAttribute"/* name */
	, "UnityEngine.Internal"/* namespaze */
	, ExcludeFromDocsAttribute_t1013_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1059/* custom_attributes_cache */
	, &ExcludeFromDocsAttribute_t1013_0_0_0/* byval_arg */
	, &ExcludeFromDocsAttribute_t1013_1_0_0/* this_arg */
	, &ExcludeFromDocsAttribute_t1013_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ExcludeFromDocsAttribute_t1013)/* instance_size */
	, sizeof (ExcludeFromDocsAttribute_t1013)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.Serialization.FormerlySerializedAsAttribute
#include "UnityEngine_UnityEngine_Serialization_FormerlySerializedAsAt.h"
// Metadata Definition UnityEngine.Serialization.FormerlySerializedAsAttribute
extern TypeInfo FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo;
// UnityEngine.Serialization.FormerlySerializedAsAttribute
#include "UnityEngine_UnityEngine_Serialization_FormerlySerializedAsAtMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo FormerlySerializedAsAttribute_t719_FormerlySerializedAsAttribute__ctor_m3494_ParameterInfos[] = 
{
	{"oldName", 0, 134220332, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Serialization.FormerlySerializedAsAttribute::.ctor(System.String)
extern const MethodInfo FormerlySerializedAsAttribute__ctor_m3494_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&FormerlySerializedAsAttribute__ctor_m3494/* method */
	, &FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, FormerlySerializedAsAttribute_t719_FormerlySerializedAsAttribute__ctor_m3494_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2289/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* FormerlySerializedAsAttribute_t719_MethodInfos[] =
{
	&FormerlySerializedAsAttribute__ctor_m3494_MethodInfo,
	NULL
};
static const Il2CppMethodReference FormerlySerializedAsAttribute_t719_VTable[] =
{
	&Attribute_Equals_m5384_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Attribute_GetHashCode_m5253_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool FormerlySerializedAsAttribute_t719_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair FormerlySerializedAsAttribute_t719_InterfacesOffsets[] = 
{
	{ &_Attribute_t1145_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType FormerlySerializedAsAttribute_t719_0_0_0;
extern const Il2CppType FormerlySerializedAsAttribute_t719_1_0_0;
struct FormerlySerializedAsAttribute_t719;
const Il2CppTypeDefinitionMetadata FormerlySerializedAsAttribute_t719_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FormerlySerializedAsAttribute_t719_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t805_0_0_0/* parent */
	, FormerlySerializedAsAttribute_t719_VTable/* vtableMethods */
	, FormerlySerializedAsAttribute_t719_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1483/* fieldStart */

};
TypeInfo FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "FormerlySerializedAsAttribute"/* name */
	, "UnityEngine.Serialization"/* namespaze */
	, FormerlySerializedAsAttribute_t719_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1060/* custom_attributes_cache */
	, &FormerlySerializedAsAttribute_t719_0_0_0/* byval_arg */
	, &FormerlySerializedAsAttribute_t719_1_0_0/* this_arg */
	, &FormerlySerializedAsAttribute_t719_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FormerlySerializedAsAttribute_t719)/* instance_size */
	, sizeof (FormerlySerializedAsAttribute_t719)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngineInternal.TypeInferenceRules
#include "UnityEngine_UnityEngineInternal_TypeInferenceRules.h"
// Metadata Definition UnityEngineInternal.TypeInferenceRules
extern TypeInfo TypeInferenceRules_t1014_il2cpp_TypeInfo;
// UnityEngineInternal.TypeInferenceRules
#include "UnityEngine_UnityEngineInternal_TypeInferenceRulesMethodDeclarations.h"
static const MethodInfo* TypeInferenceRules_t1014_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference TypeInferenceRules_t1014_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool TypeInferenceRules_t1014_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TypeInferenceRules_t1014_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TypeInferenceRules_t1014_0_0_0;
extern const Il2CppType TypeInferenceRules_t1014_1_0_0;
const Il2CppTypeDefinitionMetadata TypeInferenceRules_t1014_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TypeInferenceRules_t1014_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, TypeInferenceRules_t1014_VTable/* vtableMethods */
	, TypeInferenceRules_t1014_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1484/* fieldStart */

};
TypeInfo TypeInferenceRules_t1014_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeInferenceRules"/* name */
	, "UnityEngineInternal"/* namespaze */
	, TypeInferenceRules_t1014_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TypeInferenceRules_t1014_0_0_0/* byval_arg */
	, &TypeInferenceRules_t1014_1_0_0/* this_arg */
	, &TypeInferenceRules_t1014_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeInferenceRules_t1014)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TypeInferenceRules_t1014)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngineInternal.TypeInferenceRuleAttribute
#include "UnityEngine_UnityEngineInternal_TypeInferenceRuleAttribute.h"
// Metadata Definition UnityEngineInternal.TypeInferenceRuleAttribute
extern TypeInfo TypeInferenceRuleAttribute_t1015_il2cpp_TypeInfo;
// UnityEngineInternal.TypeInferenceRuleAttribute
#include "UnityEngine_UnityEngineInternal_TypeInferenceRuleAttributeMethodDeclarations.h"
extern const Il2CppType TypeInferenceRules_t1014_0_0_0;
static const ParameterInfo TypeInferenceRuleAttribute_t1015_TypeInferenceRuleAttribute__ctor_m5084_ParameterInfos[] = 
{
	{"rule", 0, 134220333, 0, &TypeInferenceRules_t1014_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngineInternal.TypeInferenceRuleAttribute::.ctor(UnityEngineInternal.TypeInferenceRules)
extern const MethodInfo TypeInferenceRuleAttribute__ctor_m5084_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TypeInferenceRuleAttribute__ctor_m5084/* method */
	, &TypeInferenceRuleAttribute_t1015_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, TypeInferenceRuleAttribute_t1015_TypeInferenceRuleAttribute__ctor_m5084_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2290/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo TypeInferenceRuleAttribute_t1015_TypeInferenceRuleAttribute__ctor_m5085_ParameterInfos[] = 
{
	{"rule", 0, 134220334, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngineInternal.TypeInferenceRuleAttribute::.ctor(System.String)
extern const MethodInfo TypeInferenceRuleAttribute__ctor_m5085_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TypeInferenceRuleAttribute__ctor_m5085/* method */
	, &TypeInferenceRuleAttribute_t1015_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, TypeInferenceRuleAttribute_t1015_TypeInferenceRuleAttribute__ctor_m5085_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2291/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngineInternal.TypeInferenceRuleAttribute::ToString()
extern const MethodInfo TypeInferenceRuleAttribute_ToString_m5086_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&TypeInferenceRuleAttribute_ToString_m5086/* method */
	, &TypeInferenceRuleAttribute_t1015_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2292/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TypeInferenceRuleAttribute_t1015_MethodInfos[] =
{
	&TypeInferenceRuleAttribute__ctor_m5084_MethodInfo,
	&TypeInferenceRuleAttribute__ctor_m5085_MethodInfo,
	&TypeInferenceRuleAttribute_ToString_m5086_MethodInfo,
	NULL
};
extern const MethodInfo TypeInferenceRuleAttribute_ToString_m5086_MethodInfo;
static const Il2CppMethodReference TypeInferenceRuleAttribute_t1015_VTable[] =
{
	&Attribute_Equals_m5384_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Attribute_GetHashCode_m5253_MethodInfo,
	&TypeInferenceRuleAttribute_ToString_m5086_MethodInfo,
};
static bool TypeInferenceRuleAttribute_t1015_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TypeInferenceRuleAttribute_t1015_InterfacesOffsets[] = 
{
	{ &_Attribute_t1145_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TypeInferenceRuleAttribute_t1015_0_0_0;
extern const Il2CppType TypeInferenceRuleAttribute_t1015_1_0_0;
struct TypeInferenceRuleAttribute_t1015;
const Il2CppTypeDefinitionMetadata TypeInferenceRuleAttribute_t1015_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TypeInferenceRuleAttribute_t1015_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t805_0_0_0/* parent */
	, TypeInferenceRuleAttribute_t1015_VTable/* vtableMethods */
	, TypeInferenceRuleAttribute_t1015_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1489/* fieldStart */

};
TypeInfo TypeInferenceRuleAttribute_t1015_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeInferenceRuleAttribute"/* name */
	, "UnityEngineInternal"/* namespaze */
	, TypeInferenceRuleAttribute_t1015_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TypeInferenceRuleAttribute_t1015_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1061/* custom_attributes_cache */
	, &TypeInferenceRuleAttribute_t1015_0_0_0/* byval_arg */
	, &TypeInferenceRuleAttribute_t1015_1_0_0/* this_arg */
	, &TypeInferenceRuleAttribute_t1015_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeInferenceRuleAttribute_t1015)/* instance_size */
	, sizeof (TypeInferenceRuleAttribute_t1015)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngineInternal.GenericStack
#include "UnityEngine_UnityEngineInternal_GenericStack.h"
// Metadata Definition UnityEngineInternal.GenericStack
extern TypeInfo GenericStack_t814_il2cpp_TypeInfo;
// UnityEngineInternal.GenericStack
#include "UnityEngine_UnityEngineInternal_GenericStackMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngineInternal.GenericStack::.ctor()
extern const MethodInfo GenericStack__ctor_m5087_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&GenericStack__ctor_m5087/* method */
	, &GenericStack_t814_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2293/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* GenericStack_t814_MethodInfos[] =
{
	&GenericStack__ctor_m5087_MethodInfo,
	NULL
};
extern const MethodInfo Stack_GetEnumerator_m5465_MethodInfo;
extern const MethodInfo Stack_get_Count_m5466_MethodInfo;
extern const MethodInfo Stack_get_IsSynchronized_m5467_MethodInfo;
extern const MethodInfo Stack_get_SyncRoot_m5468_MethodInfo;
extern const MethodInfo Stack_CopyTo_m5469_MethodInfo;
extern const MethodInfo Stack_Clear_m5470_MethodInfo;
extern const MethodInfo Stack_Peek_m5471_MethodInfo;
extern const MethodInfo Stack_Pop_m5472_MethodInfo;
extern const MethodInfo Stack_Push_m5473_MethodInfo;
static const Il2CppMethodReference GenericStack_t814_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&Stack_GetEnumerator_m5465_MethodInfo,
	&Stack_get_Count_m5466_MethodInfo,
	&Stack_get_IsSynchronized_m5467_MethodInfo,
	&Stack_get_SyncRoot_m5468_MethodInfo,
	&Stack_CopyTo_m5469_MethodInfo,
	&Stack_get_Count_m5466_MethodInfo,
	&Stack_get_IsSynchronized_m5467_MethodInfo,
	&Stack_get_SyncRoot_m5468_MethodInfo,
	&Stack_Clear_m5470_MethodInfo,
	&Stack_CopyTo_m5469_MethodInfo,
	&Stack_GetEnumerator_m5465_MethodInfo,
	&Stack_Peek_m5471_MethodInfo,
	&Stack_Pop_m5472_MethodInfo,
	&Stack_Push_m5473_MethodInfo,
};
static bool GenericStack_t814_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IEnumerable_t438_0_0_0;
extern const Il2CppType ICloneable_t733_0_0_0;
extern const Il2CppType ICollection_t440_0_0_0;
static Il2CppInterfaceOffsetPair GenericStack_t814_InterfacesOffsets[] = 
{
	{ &IEnumerable_t438_0_0_0, 4},
	{ &ICloneable_t733_0_0_0, 5},
	{ &ICollection_t440_0_0_0, 5},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GenericStack_t814_0_0_0;
extern const Il2CppType GenericStack_t814_1_0_0;
extern const Il2CppType Stack_t1016_0_0_0;
struct GenericStack_t814;
const Il2CppTypeDefinitionMetadata GenericStack_t814_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, GenericStack_t814_InterfacesOffsets/* interfaceOffsets */
	, &Stack_t1016_0_0_0/* parent */
	, GenericStack_t814_VTable/* vtableMethods */
	, GenericStack_t814_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo GenericStack_t814_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GenericStack"/* name */
	, "UnityEngineInternal"/* namespaze */
	, GenericStack_t814_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &GenericStack_t814_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GenericStack_t814_0_0_0/* byval_arg */
	, &GenericStack_t814_1_0_0/* this_arg */
	, &GenericStack_t814_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GenericStack_t814)/* instance_size */
	, sizeof (GenericStack_t814)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 18/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.Events.UnityAction
#include "UnityEngine_UnityEngine_Events_UnityAction.h"
// Metadata Definition UnityEngine.Events.UnityAction
extern TypeInfo UnityAction_t416_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction
#include "UnityEngine_UnityEngine_Events_UnityActionMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo UnityAction_t416_UnityAction__ctor_m1725_ParameterInfos[] = 
{
	{"object", 0, 134220335, 0, &Object_t_0_0_0},
	{"method", 1, 134220336, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityAction::.ctor(System.Object,System.IntPtr)
extern const MethodInfo UnityAction__ctor_m1725_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction__ctor_m1725/* method */
	, &UnityAction_t416_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_IntPtr_t/* invoker_method */
	, UnityAction_t416_UnityAction__ctor_m1725_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2294/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityAction::Invoke()
extern const MethodInfo UnityAction_Invoke_m5088_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_Invoke_m5088/* method */
	, &UnityAction_t416_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2295/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType AsyncCallback_t547_0_0_0;
extern const Il2CppType AsyncCallback_t547_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo UnityAction_t416_UnityAction_BeginInvoke_m5089_ParameterInfos[] = 
{
	{"callback", 0, 134220337, 0, &AsyncCallback_t547_0_0_0},
	{"object", 1, 134220338, 0, &Object_t_0_0_0},
};
extern const Il2CppType IAsyncResult_t546_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult UnityEngine.Events.UnityAction::BeginInvoke(System.AsyncCallback,System.Object)
extern const MethodInfo UnityAction_BeginInvoke_m5089_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_BeginInvoke_m5089/* method */
	, &UnityAction_t416_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t546_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_t416_UnityAction_BeginInvoke_m5089_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2296/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t546_0_0_0;
static const ParameterInfo UnityAction_t416_UnityAction_EndInvoke_m5090_ParameterInfos[] = 
{
	{"result", 0, 134220339, 0, &IAsyncResult_t546_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityAction::EndInvoke(System.IAsyncResult)
extern const MethodInfo UnityAction_EndInvoke_m5090_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_EndInvoke_m5090/* method */
	, &UnityAction_t416_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, UnityAction_t416_UnityAction_EndInvoke_m5090_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2297/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UnityAction_t416_MethodInfos[] =
{
	&UnityAction__ctor_m1725_MethodInfo,
	&UnityAction_Invoke_m5088_MethodInfo,
	&UnityAction_BeginInvoke_m5089_MethodInfo,
	&UnityAction_EndInvoke_m5090_MethodInfo,
	NULL
};
extern const MethodInfo MulticastDelegate_Equals_m3586_MethodInfo;
extern const MethodInfo MulticastDelegate_GetHashCode_m3587_MethodInfo;
extern const MethodInfo MulticastDelegate_GetObjectData_m3588_MethodInfo;
extern const MethodInfo Delegate_Clone_m3589_MethodInfo;
extern const MethodInfo MulticastDelegate_GetInvocationList_m3590_MethodInfo;
extern const MethodInfo MulticastDelegate_CombineImpl_m3591_MethodInfo;
extern const MethodInfo MulticastDelegate_RemoveImpl_m3592_MethodInfo;
extern const MethodInfo UnityAction_Invoke_m5088_MethodInfo;
extern const MethodInfo UnityAction_BeginInvoke_m5089_MethodInfo;
extern const MethodInfo UnityAction_EndInvoke_m5090_MethodInfo;
static const Il2CppMethodReference UnityAction_t416_VTable[] =
{
	&MulticastDelegate_Equals_m3586_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&MulticastDelegate_GetHashCode_m3587_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&Delegate_Clone_m3589_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&MulticastDelegate_GetInvocationList_m3590_MethodInfo,
	&MulticastDelegate_CombineImpl_m3591_MethodInfo,
	&MulticastDelegate_RemoveImpl_m3592_MethodInfo,
	&UnityAction_Invoke_m5088_MethodInfo,
	&UnityAction_BeginInvoke_m5089_MethodInfo,
	&UnityAction_EndInvoke_m5090_MethodInfo,
};
static bool UnityAction_t416_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UnityAction_t416_InterfacesOffsets[] = 
{
	{ &ICloneable_t733_0_0_0, 4},
	{ &ISerializable_t439_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityAction_t416_1_0_0;
extern const Il2CppType MulticastDelegate_t549_0_0_0;
struct UnityAction_t416;
const Il2CppTypeDefinitionMetadata UnityAction_t416_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityAction_t416_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t549_0_0_0/* parent */
	, UnityAction_t416_VTable/* vtableMethods */
	, UnityAction_t416_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo UnityAction_t416_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_t416_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UnityAction_t416_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityAction_t416_0_0_0/* byval_arg */
	, &UnityAction_t416_1_0_0/* this_arg */
	, &UnityAction_t416_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_UnityAction_t416/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_t416)/* instance_size */
	, sizeof (UnityAction_t416)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.UnityAction`1
extern TypeInfo UnityAction_1_t1136_il2cpp_TypeInfo;
extern const Il2CppGenericContainer UnityAction_1_t1136_Il2CppGenericContainer;
extern TypeInfo UnityAction_1_t1136_gp_T0_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityAction_1_t1136_gp_T0_0_il2cpp_TypeInfo_GenericParamFull = { &UnityAction_1_t1136_Il2CppGenericContainer, NULL, "T0", 0, 0 };
static const Il2CppGenericParameter* UnityAction_1_t1136_Il2CppGenericParametersArray[1] = 
{
	&UnityAction_1_t1136_gp_T0_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer UnityAction_1_t1136_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&UnityAction_1_t1136_il2cpp_TypeInfo, 1, 0, UnityAction_1_t1136_Il2CppGenericParametersArray };
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo UnityAction_1_t1136_UnityAction_1__ctor_m5368_ParameterInfos[] = 
{
	{"object", 0, 134220340, 0, &Object_t_0_0_0},
	{"method", 1, 134220341, 0, &IntPtr_t_0_0_0},
};
// System.Void UnityEngine.Events.UnityAction`1::.ctor(System.Object,System.IntPtr)
extern const MethodInfo UnityAction_1__ctor_m5368_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &UnityAction_1_t1136_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_1_t1136_UnityAction_1__ctor_m5368_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2298/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UnityAction_1_t1136_gp_0_0_0_0;
extern const Il2CppType UnityAction_1_t1136_gp_0_0_0_0;
static const ParameterInfo UnityAction_1_t1136_UnityAction_1_Invoke_m5369_ParameterInfos[] = 
{
	{"arg0", 0, 134220342, 0, &UnityAction_1_t1136_gp_0_0_0_0},
};
// System.Void UnityEngine.Events.UnityAction`1::Invoke(T0)
extern const MethodInfo UnityAction_1_Invoke_m5369_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &UnityAction_1_t1136_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_1_t1136_UnityAction_1_Invoke_m5369_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2299/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UnityAction_1_t1136_gp_0_0_0_0;
extern const Il2CppType AsyncCallback_t547_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo UnityAction_1_t1136_UnityAction_1_BeginInvoke_m5370_ParameterInfos[] = 
{
	{"arg0", 0, 134220343, 0, &UnityAction_1_t1136_gp_0_0_0_0},
	{"callback", 1, 134220344, 0, &AsyncCallback_t547_0_0_0},
	{"object", 2, 134220345, 0, &Object_t_0_0_0},
};
// System.IAsyncResult UnityEngine.Events.UnityAction`1::BeginInvoke(T0,System.AsyncCallback,System.Object)
extern const MethodInfo UnityAction_1_BeginInvoke_m5370_MethodInfo = 
{
	"BeginInvoke"/* name */
	, NULL/* method */
	, &UnityAction_1_t1136_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t546_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_1_t1136_UnityAction_1_BeginInvoke_m5370_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2300/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t546_0_0_0;
static const ParameterInfo UnityAction_1_t1136_UnityAction_1_EndInvoke_m5371_ParameterInfos[] = 
{
	{"result", 0, 134220346, 0, &IAsyncResult_t546_0_0_0},
};
// System.Void UnityEngine.Events.UnityAction`1::EndInvoke(System.IAsyncResult)
extern const MethodInfo UnityAction_1_EndInvoke_m5371_MethodInfo = 
{
	"EndInvoke"/* name */
	, NULL/* method */
	, &UnityAction_1_t1136_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_1_t1136_UnityAction_1_EndInvoke_m5371_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2301/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UnityAction_1_t1136_MethodInfos[] =
{
	&UnityAction_1__ctor_m5368_MethodInfo,
	&UnityAction_1_Invoke_m5369_MethodInfo,
	&UnityAction_1_BeginInvoke_m5370_MethodInfo,
	&UnityAction_1_EndInvoke_m5371_MethodInfo,
	NULL
};
extern const MethodInfo UnityAction_1_Invoke_m5369_MethodInfo;
extern const MethodInfo UnityAction_1_BeginInvoke_m5370_MethodInfo;
extern const MethodInfo UnityAction_1_EndInvoke_m5371_MethodInfo;
static const Il2CppMethodReference UnityAction_1_t1136_VTable[] =
{
	&MulticastDelegate_Equals_m3586_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&MulticastDelegate_GetHashCode_m3587_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&Delegate_Clone_m3589_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&MulticastDelegate_GetInvocationList_m3590_MethodInfo,
	&MulticastDelegate_CombineImpl_m3591_MethodInfo,
	&MulticastDelegate_RemoveImpl_m3592_MethodInfo,
	&UnityAction_1_Invoke_m5369_MethodInfo,
	&UnityAction_1_BeginInvoke_m5370_MethodInfo,
	&UnityAction_1_EndInvoke_m5371_MethodInfo,
};
static bool UnityAction_1_t1136_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t1136_InterfacesOffsets[] = 
{
	{ &ICloneable_t733_0_0_0, 4},
	{ &ISerializable_t439_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityAction_1_t1136_0_0_0;
extern const Il2CppType UnityAction_1_t1136_1_0_0;
struct UnityAction_1_t1136;
const Il2CppTypeDefinitionMetadata UnityAction_1_t1136_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityAction_1_t1136_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t549_0_0_0/* parent */
	, UnityAction_1_t1136_VTable/* vtableMethods */
	, UnityAction_1_t1136_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo UnityAction_1_t1136_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t1136_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UnityAction_1_t1136_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityAction_1_t1136_0_0_0/* byval_arg */
	, &UnityAction_1_t1136_1_0_0/* this_arg */
	, &UnityAction_1_t1136_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &UnityAction_1_t1136_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.UnityAction`2
extern TypeInfo UnityAction_2_t1137_il2cpp_TypeInfo;
extern const Il2CppGenericContainer UnityAction_2_t1137_Il2CppGenericContainer;
extern TypeInfo UnityAction_2_t1137_gp_T0_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityAction_2_t1137_gp_T0_0_il2cpp_TypeInfo_GenericParamFull = { &UnityAction_2_t1137_Il2CppGenericContainer, NULL, "T0", 0, 0 };
extern TypeInfo UnityAction_2_t1137_gp_T1_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityAction_2_t1137_gp_T1_1_il2cpp_TypeInfo_GenericParamFull = { &UnityAction_2_t1137_Il2CppGenericContainer, NULL, "T1", 1, 0 };
static const Il2CppGenericParameter* UnityAction_2_t1137_Il2CppGenericParametersArray[2] = 
{
	&UnityAction_2_t1137_gp_T0_0_il2cpp_TypeInfo_GenericParamFull,
	&UnityAction_2_t1137_gp_T1_1_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer UnityAction_2_t1137_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&UnityAction_2_t1137_il2cpp_TypeInfo, 2, 0, UnityAction_2_t1137_Il2CppGenericParametersArray };
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo UnityAction_2_t1137_UnityAction_2__ctor_m5372_ParameterInfos[] = 
{
	{"object", 0, 134220347, 0, &Object_t_0_0_0},
	{"method", 1, 134220348, 0, &IntPtr_t_0_0_0},
};
// System.Void UnityEngine.Events.UnityAction`2::.ctor(System.Object,System.IntPtr)
extern const MethodInfo UnityAction_2__ctor_m5372_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &UnityAction_2_t1137_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_2_t1137_UnityAction_2__ctor_m5372_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2302/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UnityAction_2_t1137_gp_0_0_0_0;
extern const Il2CppType UnityAction_2_t1137_gp_0_0_0_0;
extern const Il2CppType UnityAction_2_t1137_gp_1_0_0_0;
extern const Il2CppType UnityAction_2_t1137_gp_1_0_0_0;
static const ParameterInfo UnityAction_2_t1137_UnityAction_2_Invoke_m5373_ParameterInfos[] = 
{
	{"arg0", 0, 134220349, 0, &UnityAction_2_t1137_gp_0_0_0_0},
	{"arg1", 1, 134220350, 0, &UnityAction_2_t1137_gp_1_0_0_0},
};
// System.Void UnityEngine.Events.UnityAction`2::Invoke(T0,T1)
extern const MethodInfo UnityAction_2_Invoke_m5373_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &UnityAction_2_t1137_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_2_t1137_UnityAction_2_Invoke_m5373_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2303/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UnityAction_2_t1137_gp_0_0_0_0;
extern const Il2CppType UnityAction_2_t1137_gp_1_0_0_0;
extern const Il2CppType AsyncCallback_t547_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo UnityAction_2_t1137_UnityAction_2_BeginInvoke_m5374_ParameterInfos[] = 
{
	{"arg0", 0, 134220351, 0, &UnityAction_2_t1137_gp_0_0_0_0},
	{"arg1", 1, 134220352, 0, &UnityAction_2_t1137_gp_1_0_0_0},
	{"callback", 2, 134220353, 0, &AsyncCallback_t547_0_0_0},
	{"object", 3, 134220354, 0, &Object_t_0_0_0},
};
// System.IAsyncResult UnityEngine.Events.UnityAction`2::BeginInvoke(T0,T1,System.AsyncCallback,System.Object)
extern const MethodInfo UnityAction_2_BeginInvoke_m5374_MethodInfo = 
{
	"BeginInvoke"/* name */
	, NULL/* method */
	, &UnityAction_2_t1137_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t546_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_2_t1137_UnityAction_2_BeginInvoke_m5374_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2304/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t546_0_0_0;
static const ParameterInfo UnityAction_2_t1137_UnityAction_2_EndInvoke_m5375_ParameterInfos[] = 
{
	{"result", 0, 134220355, 0, &IAsyncResult_t546_0_0_0},
};
// System.Void UnityEngine.Events.UnityAction`2::EndInvoke(System.IAsyncResult)
extern const MethodInfo UnityAction_2_EndInvoke_m5375_MethodInfo = 
{
	"EndInvoke"/* name */
	, NULL/* method */
	, &UnityAction_2_t1137_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_2_t1137_UnityAction_2_EndInvoke_m5375_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2305/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UnityAction_2_t1137_MethodInfos[] =
{
	&UnityAction_2__ctor_m5372_MethodInfo,
	&UnityAction_2_Invoke_m5373_MethodInfo,
	&UnityAction_2_BeginInvoke_m5374_MethodInfo,
	&UnityAction_2_EndInvoke_m5375_MethodInfo,
	NULL
};
extern const MethodInfo UnityAction_2_Invoke_m5373_MethodInfo;
extern const MethodInfo UnityAction_2_BeginInvoke_m5374_MethodInfo;
extern const MethodInfo UnityAction_2_EndInvoke_m5375_MethodInfo;
static const Il2CppMethodReference UnityAction_2_t1137_VTable[] =
{
	&MulticastDelegate_Equals_m3586_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&MulticastDelegate_GetHashCode_m3587_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&Delegate_Clone_m3589_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&MulticastDelegate_GetInvocationList_m3590_MethodInfo,
	&MulticastDelegate_CombineImpl_m3591_MethodInfo,
	&MulticastDelegate_RemoveImpl_m3592_MethodInfo,
	&UnityAction_2_Invoke_m5373_MethodInfo,
	&UnityAction_2_BeginInvoke_m5374_MethodInfo,
	&UnityAction_2_EndInvoke_m5375_MethodInfo,
};
static bool UnityAction_2_t1137_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UnityAction_2_t1137_InterfacesOffsets[] = 
{
	{ &ICloneable_t733_0_0_0, 4},
	{ &ISerializable_t439_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityAction_2_t1137_0_0_0;
extern const Il2CppType UnityAction_2_t1137_1_0_0;
struct UnityAction_2_t1137;
const Il2CppTypeDefinitionMetadata UnityAction_2_t1137_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityAction_2_t1137_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t549_0_0_0/* parent */
	, UnityAction_2_t1137_VTable/* vtableMethods */
	, UnityAction_2_t1137_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo UnityAction_2_t1137_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`2"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_2_t1137_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UnityAction_2_t1137_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityAction_2_t1137_0_0_0/* byval_arg */
	, &UnityAction_2_t1137_1_0_0/* this_arg */
	, &UnityAction_2_t1137_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &UnityAction_2_t1137_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.UnityAction`3
extern TypeInfo UnityAction_3_t1138_il2cpp_TypeInfo;
extern const Il2CppGenericContainer UnityAction_3_t1138_Il2CppGenericContainer;
extern TypeInfo UnityAction_3_t1138_gp_T0_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityAction_3_t1138_gp_T0_0_il2cpp_TypeInfo_GenericParamFull = { &UnityAction_3_t1138_Il2CppGenericContainer, NULL, "T0", 0, 0 };
extern TypeInfo UnityAction_3_t1138_gp_T1_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityAction_3_t1138_gp_T1_1_il2cpp_TypeInfo_GenericParamFull = { &UnityAction_3_t1138_Il2CppGenericContainer, NULL, "T1", 1, 0 };
extern TypeInfo UnityAction_3_t1138_gp_T2_2_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityAction_3_t1138_gp_T2_2_il2cpp_TypeInfo_GenericParamFull = { &UnityAction_3_t1138_Il2CppGenericContainer, NULL, "T2", 2, 0 };
static const Il2CppGenericParameter* UnityAction_3_t1138_Il2CppGenericParametersArray[3] = 
{
	&UnityAction_3_t1138_gp_T0_0_il2cpp_TypeInfo_GenericParamFull,
	&UnityAction_3_t1138_gp_T1_1_il2cpp_TypeInfo_GenericParamFull,
	&UnityAction_3_t1138_gp_T2_2_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer UnityAction_3_t1138_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&UnityAction_3_t1138_il2cpp_TypeInfo, 3, 0, UnityAction_3_t1138_Il2CppGenericParametersArray };
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo UnityAction_3_t1138_UnityAction_3__ctor_m5376_ParameterInfos[] = 
{
	{"object", 0, 134220356, 0, &Object_t_0_0_0},
	{"method", 1, 134220357, 0, &IntPtr_t_0_0_0},
};
// System.Void UnityEngine.Events.UnityAction`3::.ctor(System.Object,System.IntPtr)
extern const MethodInfo UnityAction_3__ctor_m5376_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &UnityAction_3_t1138_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_3_t1138_UnityAction_3__ctor_m5376_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2306/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UnityAction_3_t1138_gp_0_0_0_0;
extern const Il2CppType UnityAction_3_t1138_gp_0_0_0_0;
extern const Il2CppType UnityAction_3_t1138_gp_1_0_0_0;
extern const Il2CppType UnityAction_3_t1138_gp_1_0_0_0;
extern const Il2CppType UnityAction_3_t1138_gp_2_0_0_0;
extern const Il2CppType UnityAction_3_t1138_gp_2_0_0_0;
static const ParameterInfo UnityAction_3_t1138_UnityAction_3_Invoke_m5377_ParameterInfos[] = 
{
	{"arg0", 0, 134220358, 0, &UnityAction_3_t1138_gp_0_0_0_0},
	{"arg1", 1, 134220359, 0, &UnityAction_3_t1138_gp_1_0_0_0},
	{"arg2", 2, 134220360, 0, &UnityAction_3_t1138_gp_2_0_0_0},
};
// System.Void UnityEngine.Events.UnityAction`3::Invoke(T0,T1,T2)
extern const MethodInfo UnityAction_3_Invoke_m5377_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &UnityAction_3_t1138_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_3_t1138_UnityAction_3_Invoke_m5377_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2307/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UnityAction_3_t1138_gp_0_0_0_0;
extern const Il2CppType UnityAction_3_t1138_gp_1_0_0_0;
extern const Il2CppType UnityAction_3_t1138_gp_2_0_0_0;
extern const Il2CppType AsyncCallback_t547_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo UnityAction_3_t1138_UnityAction_3_BeginInvoke_m5378_ParameterInfos[] = 
{
	{"arg0", 0, 134220361, 0, &UnityAction_3_t1138_gp_0_0_0_0},
	{"arg1", 1, 134220362, 0, &UnityAction_3_t1138_gp_1_0_0_0},
	{"arg2", 2, 134220363, 0, &UnityAction_3_t1138_gp_2_0_0_0},
	{"callback", 3, 134220364, 0, &AsyncCallback_t547_0_0_0},
	{"object", 4, 134220365, 0, &Object_t_0_0_0},
};
// System.IAsyncResult UnityEngine.Events.UnityAction`3::BeginInvoke(T0,T1,T2,System.AsyncCallback,System.Object)
extern const MethodInfo UnityAction_3_BeginInvoke_m5378_MethodInfo = 
{
	"BeginInvoke"/* name */
	, NULL/* method */
	, &UnityAction_3_t1138_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t546_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_3_t1138_UnityAction_3_BeginInvoke_m5378_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2308/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t546_0_0_0;
static const ParameterInfo UnityAction_3_t1138_UnityAction_3_EndInvoke_m5379_ParameterInfos[] = 
{
	{"result", 0, 134220366, 0, &IAsyncResult_t546_0_0_0},
};
// System.Void UnityEngine.Events.UnityAction`3::EndInvoke(System.IAsyncResult)
extern const MethodInfo UnityAction_3_EndInvoke_m5379_MethodInfo = 
{
	"EndInvoke"/* name */
	, NULL/* method */
	, &UnityAction_3_t1138_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_3_t1138_UnityAction_3_EndInvoke_m5379_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2309/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UnityAction_3_t1138_MethodInfos[] =
{
	&UnityAction_3__ctor_m5376_MethodInfo,
	&UnityAction_3_Invoke_m5377_MethodInfo,
	&UnityAction_3_BeginInvoke_m5378_MethodInfo,
	&UnityAction_3_EndInvoke_m5379_MethodInfo,
	NULL
};
extern const MethodInfo UnityAction_3_Invoke_m5377_MethodInfo;
extern const MethodInfo UnityAction_3_BeginInvoke_m5378_MethodInfo;
extern const MethodInfo UnityAction_3_EndInvoke_m5379_MethodInfo;
static const Il2CppMethodReference UnityAction_3_t1138_VTable[] =
{
	&MulticastDelegate_Equals_m3586_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&MulticastDelegate_GetHashCode_m3587_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&Delegate_Clone_m3589_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&MulticastDelegate_GetInvocationList_m3590_MethodInfo,
	&MulticastDelegate_CombineImpl_m3591_MethodInfo,
	&MulticastDelegate_RemoveImpl_m3592_MethodInfo,
	&UnityAction_3_Invoke_m5377_MethodInfo,
	&UnityAction_3_BeginInvoke_m5378_MethodInfo,
	&UnityAction_3_EndInvoke_m5379_MethodInfo,
};
static bool UnityAction_3_t1138_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UnityAction_3_t1138_InterfacesOffsets[] = 
{
	{ &ICloneable_t733_0_0_0, 4},
	{ &ISerializable_t439_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityAction_3_t1138_0_0_0;
extern const Il2CppType UnityAction_3_t1138_1_0_0;
struct UnityAction_3_t1138;
const Il2CppTypeDefinitionMetadata UnityAction_3_t1138_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityAction_3_t1138_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t549_0_0_0/* parent */
	, UnityAction_3_t1138_VTable/* vtableMethods */
	, UnityAction_3_t1138_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo UnityAction_3_t1138_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`3"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_3_t1138_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UnityAction_3_t1138_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityAction_3_t1138_0_0_0/* byval_arg */
	, &UnityAction_3_t1138_1_0_0/* this_arg */
	, &UnityAction_3_t1138_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &UnityAction_3_t1138_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.UnityAction`4
extern TypeInfo UnityAction_4_t1139_il2cpp_TypeInfo;
extern const Il2CppGenericContainer UnityAction_4_t1139_Il2CppGenericContainer;
extern TypeInfo UnityAction_4_t1139_gp_T0_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityAction_4_t1139_gp_T0_0_il2cpp_TypeInfo_GenericParamFull = { &UnityAction_4_t1139_Il2CppGenericContainer, NULL, "T0", 0, 0 };
extern TypeInfo UnityAction_4_t1139_gp_T1_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityAction_4_t1139_gp_T1_1_il2cpp_TypeInfo_GenericParamFull = { &UnityAction_4_t1139_Il2CppGenericContainer, NULL, "T1", 1, 0 };
extern TypeInfo UnityAction_4_t1139_gp_T2_2_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityAction_4_t1139_gp_T2_2_il2cpp_TypeInfo_GenericParamFull = { &UnityAction_4_t1139_Il2CppGenericContainer, NULL, "T2", 2, 0 };
extern TypeInfo UnityAction_4_t1139_gp_T3_3_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityAction_4_t1139_gp_T3_3_il2cpp_TypeInfo_GenericParamFull = { &UnityAction_4_t1139_Il2CppGenericContainer, NULL, "T3", 3, 0 };
static const Il2CppGenericParameter* UnityAction_4_t1139_Il2CppGenericParametersArray[4] = 
{
	&UnityAction_4_t1139_gp_T0_0_il2cpp_TypeInfo_GenericParamFull,
	&UnityAction_4_t1139_gp_T1_1_il2cpp_TypeInfo_GenericParamFull,
	&UnityAction_4_t1139_gp_T2_2_il2cpp_TypeInfo_GenericParamFull,
	&UnityAction_4_t1139_gp_T3_3_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer UnityAction_4_t1139_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&UnityAction_4_t1139_il2cpp_TypeInfo, 4, 0, UnityAction_4_t1139_Il2CppGenericParametersArray };
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo UnityAction_4_t1139_UnityAction_4__ctor_m5380_ParameterInfos[] = 
{
	{"object", 0, 134220367, 0, &Object_t_0_0_0},
	{"method", 1, 134220368, 0, &IntPtr_t_0_0_0},
};
// System.Void UnityEngine.Events.UnityAction`4::.ctor(System.Object,System.IntPtr)
extern const MethodInfo UnityAction_4__ctor_m5380_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &UnityAction_4_t1139_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_4_t1139_UnityAction_4__ctor_m5380_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2310/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UnityAction_4_t1139_gp_0_0_0_0;
extern const Il2CppType UnityAction_4_t1139_gp_0_0_0_0;
extern const Il2CppType UnityAction_4_t1139_gp_1_0_0_0;
extern const Il2CppType UnityAction_4_t1139_gp_1_0_0_0;
extern const Il2CppType UnityAction_4_t1139_gp_2_0_0_0;
extern const Il2CppType UnityAction_4_t1139_gp_2_0_0_0;
extern const Il2CppType UnityAction_4_t1139_gp_3_0_0_0;
extern const Il2CppType UnityAction_4_t1139_gp_3_0_0_0;
static const ParameterInfo UnityAction_4_t1139_UnityAction_4_Invoke_m5381_ParameterInfos[] = 
{
	{"arg0", 0, 134220369, 0, &UnityAction_4_t1139_gp_0_0_0_0},
	{"arg1", 1, 134220370, 0, &UnityAction_4_t1139_gp_1_0_0_0},
	{"arg2", 2, 134220371, 0, &UnityAction_4_t1139_gp_2_0_0_0},
	{"arg3", 3, 134220372, 0, &UnityAction_4_t1139_gp_3_0_0_0},
};
// System.Void UnityEngine.Events.UnityAction`4::Invoke(T0,T1,T2,T3)
extern const MethodInfo UnityAction_4_Invoke_m5381_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &UnityAction_4_t1139_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_4_t1139_UnityAction_4_Invoke_m5381_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2311/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UnityAction_4_t1139_gp_0_0_0_0;
extern const Il2CppType UnityAction_4_t1139_gp_1_0_0_0;
extern const Il2CppType UnityAction_4_t1139_gp_2_0_0_0;
extern const Il2CppType UnityAction_4_t1139_gp_3_0_0_0;
extern const Il2CppType AsyncCallback_t547_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo UnityAction_4_t1139_UnityAction_4_BeginInvoke_m5382_ParameterInfos[] = 
{
	{"arg0", 0, 134220373, 0, &UnityAction_4_t1139_gp_0_0_0_0},
	{"arg1", 1, 134220374, 0, &UnityAction_4_t1139_gp_1_0_0_0},
	{"arg2", 2, 134220375, 0, &UnityAction_4_t1139_gp_2_0_0_0},
	{"arg3", 3, 134220376, 0, &UnityAction_4_t1139_gp_3_0_0_0},
	{"callback", 4, 134220377, 0, &AsyncCallback_t547_0_0_0},
	{"object", 5, 134220378, 0, &Object_t_0_0_0},
};
// System.IAsyncResult UnityEngine.Events.UnityAction`4::BeginInvoke(T0,T1,T2,T3,System.AsyncCallback,System.Object)
extern const MethodInfo UnityAction_4_BeginInvoke_m5382_MethodInfo = 
{
	"BeginInvoke"/* name */
	, NULL/* method */
	, &UnityAction_4_t1139_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t546_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_4_t1139_UnityAction_4_BeginInvoke_m5382_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2312/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t546_0_0_0;
static const ParameterInfo UnityAction_4_t1139_UnityAction_4_EndInvoke_m5383_ParameterInfos[] = 
{
	{"result", 0, 134220379, 0, &IAsyncResult_t546_0_0_0},
};
// System.Void UnityEngine.Events.UnityAction`4::EndInvoke(System.IAsyncResult)
extern const MethodInfo UnityAction_4_EndInvoke_m5383_MethodInfo = 
{
	"EndInvoke"/* name */
	, NULL/* method */
	, &UnityAction_4_t1139_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_4_t1139_UnityAction_4_EndInvoke_m5383_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2313/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UnityAction_4_t1139_MethodInfos[] =
{
	&UnityAction_4__ctor_m5380_MethodInfo,
	&UnityAction_4_Invoke_m5381_MethodInfo,
	&UnityAction_4_BeginInvoke_m5382_MethodInfo,
	&UnityAction_4_EndInvoke_m5383_MethodInfo,
	NULL
};
extern const MethodInfo UnityAction_4_Invoke_m5381_MethodInfo;
extern const MethodInfo UnityAction_4_BeginInvoke_m5382_MethodInfo;
extern const MethodInfo UnityAction_4_EndInvoke_m5383_MethodInfo;
static const Il2CppMethodReference UnityAction_4_t1139_VTable[] =
{
	&MulticastDelegate_Equals_m3586_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&MulticastDelegate_GetHashCode_m3587_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&Delegate_Clone_m3589_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&MulticastDelegate_GetInvocationList_m3590_MethodInfo,
	&MulticastDelegate_CombineImpl_m3591_MethodInfo,
	&MulticastDelegate_RemoveImpl_m3592_MethodInfo,
	&UnityAction_4_Invoke_m5381_MethodInfo,
	&UnityAction_4_BeginInvoke_m5382_MethodInfo,
	&UnityAction_4_EndInvoke_m5383_MethodInfo,
};
static bool UnityAction_4_t1139_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UnityAction_4_t1139_InterfacesOffsets[] = 
{
	{ &ICloneable_t733_0_0_0, 4},
	{ &ISerializable_t439_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityAction_4_t1139_0_0_0;
extern const Il2CppType UnityAction_4_t1139_1_0_0;
struct UnityAction_4_t1139;
const Il2CppTypeDefinitionMetadata UnityAction_4_t1139_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityAction_4_t1139_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t549_0_0_0/* parent */
	, UnityAction_4_t1139_VTable/* vtableMethods */
	, UnityAction_4_t1139_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo UnityAction_4_t1139_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`4"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_4_t1139_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UnityAction_4_t1139_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityAction_4_t1139_0_0_0/* byval_arg */
	, &UnityAction_4_t1139_1_0_0/* this_arg */
	, &UnityAction_4_t1139_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &UnityAction_4_t1139_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
