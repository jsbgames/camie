﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
// System.Reflection.AssemblyCompanyAttribute
#include "mscorlib_System_Reflection_AssemblyCompanyAttribute.h"
// System.Reflection.AssemblyCompanyAttribute
#include "mscorlib_System_Reflection_AssemblyCompanyAttributeMethodDeclarations.h"
// System.Reflection.AssemblyCopyrightAttribute
#include "mscorlib_System_Reflection_AssemblyCopyrightAttribute.h"
// System.Reflection.AssemblyCopyrightAttribute
#include "mscorlib_System_Reflection_AssemblyCopyrightAttributeMethodDeclarations.h"
// System.Resources.SatelliteContractVersionAttribute
#include "mscorlib_System_Resources_SatelliteContractVersionAttribute.h"
// System.Resources.SatelliteContractVersionAttribute
#include "mscorlib_System_Resources_SatelliteContractVersionAttributeMethodDeclarations.h"
// System.Reflection.AssemblyInformationalVersionAttribute
#include "mscorlib_System_Reflection_AssemblyInformationalVersionAttri.h"
// System.Reflection.AssemblyInformationalVersionAttribute
#include "mscorlib_System_Reflection_AssemblyInformationalVersionAttriMethodDeclarations.h"
// System.Reflection.AssemblyFileVersionAttribute
#include "mscorlib_System_Reflection_AssemblyFileVersionAttribute.h"
// System.Reflection.AssemblyFileVersionAttribute
#include "mscorlib_System_Reflection_AssemblyFileVersionAttributeMethodDeclarations.h"
// System.Resources.NeutralResourcesLanguageAttribute
#include "mscorlib_System_Resources_NeutralResourcesLanguageAttribute.h"
// System.Resources.NeutralResourcesLanguageAttribute
#include "mscorlib_System_Resources_NeutralResourcesLanguageAttributeMethodDeclarations.h"
// System.CLSCompliantAttribute
#include "mscorlib_System_CLSCompliantAttribute.h"
// System.CLSCompliantAttribute
#include "mscorlib_System_CLSCompliantAttributeMethodDeclarations.h"
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilit.h"
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilitMethodDeclarations.h"
// System.Diagnostics.DebuggableAttribute
#include "mscorlib_System_Diagnostics_DebuggableAttribute.h"
// System.Diagnostics.DebuggableAttribute
#include "mscorlib_System_Diagnostics_DebuggableAttributeMethodDeclarations.h"
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilationRelaxati.h"
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilationRelaxatiMethodDeclarations.h"
// System.Runtime.InteropServices.ComVisibleAttribute
#include "mscorlib_System_Runtime_InteropServices_ComVisibleAttribute.h"
// System.Runtime.InteropServices.ComVisibleAttribute
#include "mscorlib_System_Runtime_InteropServices_ComVisibleAttributeMethodDeclarations.h"
// System.Reflection.AssemblyKeyFileAttribute
#include "mscorlib_System_Reflection_AssemblyKeyFileAttribute.h"
// System.Reflection.AssemblyKeyFileAttribute
#include "mscorlib_System_Reflection_AssemblyKeyFileAttributeMethodDeclarations.h"
// System.Reflection.AssemblyDelaySignAttribute
#include "mscorlib_System_Reflection_AssemblyDelaySignAttribute.h"
// System.Reflection.AssemblyDelaySignAttribute
#include "mscorlib_System_Reflection_AssemblyDelaySignAttributeMethodDeclarations.h"
// System.Reflection.AssemblyProductAttribute
#include "mscorlib_System_Reflection_AssemblyProductAttribute.h"
// System.Reflection.AssemblyProductAttribute
#include "mscorlib_System_Reflection_AssemblyProductAttributeMethodDeclarations.h"
// System.Runtime.CompilerServices.ExtensionAttribute
#include "System_Core_System_Runtime_CompilerServices_ExtensionAttribu.h"
// System.Runtime.CompilerServices.ExtensionAttribute
#include "System_Core_System_Runtime_CompilerServices_ExtensionAttribuMethodDeclarations.h"
// System.Reflection.AssemblyDefaultAliasAttribute
#include "mscorlib_System_Reflection_AssemblyDefaultAliasAttribute.h"
// System.Reflection.AssemblyDefaultAliasAttribute
#include "mscorlib_System_Reflection_AssemblyDefaultAliasAttributeMethodDeclarations.h"
// System.Reflection.AssemblyDescriptionAttribute
#include "mscorlib_System_Reflection_AssemblyDescriptionAttribute.h"
// System.Reflection.AssemblyDescriptionAttribute
#include "mscorlib_System_Reflection_AssemblyDescriptionAttributeMethodDeclarations.h"
// System.Reflection.AssemblyTitleAttribute
#include "mscorlib_System_Reflection_AssemblyTitleAttribute.h"
// System.Reflection.AssemblyTitleAttribute
#include "mscorlib_System_Reflection_AssemblyTitleAttributeMethodDeclarations.h"
extern TypeInfo* AssemblyCompanyAttribute_t711_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyCopyrightAttribute_t713_il2cpp_TypeInfo_var;
extern TypeInfo* SatelliteContractVersionAttribute_t1470_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyInformationalVersionAttribute_t1469_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyFileVersionAttribute_t716_il2cpp_TypeInfo_var;
extern TypeInfo* NeutralResourcesLanguageAttribute_t1467_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1468_il2cpp_TypeInfo_var;
extern TypeInfo* RuntimeCompatibilityAttribute_t258_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggableAttribute_t1473_il2cpp_TypeInfo_var;
extern TypeInfo* CompilationRelaxationsAttribute_t1472_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t714_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyKeyFileAttribute_t1475_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyDelaySignAttribute_t1474_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyProductAttribute_t712_il2cpp_TypeInfo_var;
extern TypeInfo* ExtensionAttribute_t1111_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyDefaultAliasAttribute_t1471_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyDescriptionAttribute_t709_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyTitleAttribute_t715_il2cpp_TypeInfo_var;
void g_System_Core_Assembly_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AssemblyCompanyAttribute_t711_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1166);
		AssemblyCopyrightAttribute_t713_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1168);
		SatelliteContractVersionAttribute_t1470_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2907);
		AssemblyInformationalVersionAttribute_t1469_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2906);
		AssemblyFileVersionAttribute_t716_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1171);
		NeutralResourcesLanguageAttribute_t1467_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2904);
		CLSCompliantAttribute_t1468_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2905);
		RuntimeCompatibilityAttribute_t258_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(107);
		DebuggableAttribute_t1473_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2910);
		CompilationRelaxationsAttribute_t1472_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2909);
		ComVisibleAttribute_t714_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1169);
		AssemblyKeyFileAttribute_t1475_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2912);
		AssemblyDelaySignAttribute_t1474_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2911);
		AssemblyProductAttribute_t712_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1167);
		ExtensionAttribute_t1111_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1858);
		AssemblyDefaultAliasAttribute_t1471_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2908);
		AssemblyDescriptionAttribute_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1164);
		AssemblyTitleAttribute_t715_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1170);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 18;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AssemblyCompanyAttribute_t711 * tmp;
		tmp = (AssemblyCompanyAttribute_t711 *)il2cpp_codegen_object_new (AssemblyCompanyAttribute_t711_il2cpp_TypeInfo_var);
		AssemblyCompanyAttribute__ctor_m3469(tmp, il2cpp_codegen_string_new_wrapper("MONO development team"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AssemblyCopyrightAttribute_t713 * tmp;
		tmp = (AssemblyCopyrightAttribute_t713 *)il2cpp_codegen_object_new (AssemblyCopyrightAttribute_t713_il2cpp_TypeInfo_var);
		AssemblyCopyrightAttribute__ctor_m3471(tmp, il2cpp_codegen_string_new_wrapper("(c) various MONO Authors"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		SatelliteContractVersionAttribute_t1470 * tmp;
		tmp = (SatelliteContractVersionAttribute_t1470 *)il2cpp_codegen_object_new (SatelliteContractVersionAttribute_t1470_il2cpp_TypeInfo_var);
		SatelliteContractVersionAttribute__ctor_m6539(tmp, il2cpp_codegen_string_new_wrapper("2.0.5.0"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		AssemblyInformationalVersionAttribute_t1469 * tmp;
		tmp = (AssemblyInformationalVersionAttribute_t1469 *)il2cpp_codegen_object_new (AssemblyInformationalVersionAttribute_t1469_il2cpp_TypeInfo_var);
		AssemblyInformationalVersionAttribute__ctor_m6538(tmp, il2cpp_codegen_string_new_wrapper("3.0.40818.0"), NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		AssemblyFileVersionAttribute_t716 * tmp;
		tmp = (AssemblyFileVersionAttribute_t716 *)il2cpp_codegen_object_new (AssemblyFileVersionAttribute_t716_il2cpp_TypeInfo_var);
		AssemblyFileVersionAttribute__ctor_m3474(tmp, il2cpp_codegen_string_new_wrapper("3.0.40818.0"), NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
	{
		NeutralResourcesLanguageAttribute_t1467 * tmp;
		tmp = (NeutralResourcesLanguageAttribute_t1467 *)il2cpp_codegen_object_new (NeutralResourcesLanguageAttribute_t1467_il2cpp_TypeInfo_var);
		NeutralResourcesLanguageAttribute__ctor_m6536(tmp, il2cpp_codegen_string_new_wrapper("en-US"), NULL);
		cache->attributes[5] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1468 * tmp;
		tmp = (CLSCompliantAttribute_t1468 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1468_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m6537(tmp, true, NULL);
		cache->attributes[6] = (Il2CppObject*)tmp;
	}
	{
		RuntimeCompatibilityAttribute_t258 * tmp;
		tmp = (RuntimeCompatibilityAttribute_t258 *)il2cpp_codegen_object_new (RuntimeCompatibilityAttribute_t258_il2cpp_TypeInfo_var);
		RuntimeCompatibilityAttribute__ctor_m1021(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m1022(tmp, true, NULL);
		cache->attributes[7] = (Il2CppObject*)tmp;
	}
	{
		DebuggableAttribute_t1473 * tmp;
		tmp = (DebuggableAttribute_t1473 *)il2cpp_codegen_object_new (DebuggableAttribute_t1473_il2cpp_TypeInfo_var);
		DebuggableAttribute__ctor_m6542(tmp, 2, NULL);
		cache->attributes[8] = (Il2CppObject*)tmp;
	}
	{
		CompilationRelaxationsAttribute_t1472 * tmp;
		tmp = (CompilationRelaxationsAttribute_t1472 *)il2cpp_codegen_object_new (CompilationRelaxationsAttribute_t1472_il2cpp_TypeInfo_var);
		CompilationRelaxationsAttribute__ctor_m6541(tmp, 8, NULL);
		cache->attributes[9] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t714 * tmp;
		tmp = (ComVisibleAttribute_t714 *)il2cpp_codegen_object_new (ComVisibleAttribute_t714_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m3472(tmp, false, NULL);
		cache->attributes[10] = (Il2CppObject*)tmp;
	}
	{
		AssemblyKeyFileAttribute_t1475 * tmp;
		tmp = (AssemblyKeyFileAttribute_t1475 *)il2cpp_codegen_object_new (AssemblyKeyFileAttribute_t1475_il2cpp_TypeInfo_var);
		AssemblyKeyFileAttribute__ctor_m6544(tmp, il2cpp_codegen_string_new_wrapper("../silverlight.pub"), NULL);
		cache->attributes[11] = (Il2CppObject*)tmp;
	}
	{
		AssemblyDelaySignAttribute_t1474 * tmp;
		tmp = (AssemblyDelaySignAttribute_t1474 *)il2cpp_codegen_object_new (AssemblyDelaySignAttribute_t1474_il2cpp_TypeInfo_var);
		AssemblyDelaySignAttribute__ctor_m6543(tmp, true, NULL);
		cache->attributes[12] = (Il2CppObject*)tmp;
	}
	{
		AssemblyProductAttribute_t712 * tmp;
		tmp = (AssemblyProductAttribute_t712 *)il2cpp_codegen_object_new (AssemblyProductAttribute_t712_il2cpp_TypeInfo_var);
		AssemblyProductAttribute__ctor_m3470(tmp, il2cpp_codegen_string_new_wrapper("MONO Common language infrastructure"), NULL);
		cache->attributes[13] = (Il2CppObject*)tmp;
	}
	{
		ExtensionAttribute_t1111 * tmp;
		tmp = (ExtensionAttribute_t1111 *)il2cpp_codegen_object_new (ExtensionAttribute_t1111_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m5256(tmp, NULL);
		cache->attributes[14] = (Il2CppObject*)tmp;
	}
	{
		AssemblyDefaultAliasAttribute_t1471 * tmp;
		tmp = (AssemblyDefaultAliasAttribute_t1471 *)il2cpp_codegen_object_new (AssemblyDefaultAliasAttribute_t1471_il2cpp_TypeInfo_var);
		AssemblyDefaultAliasAttribute__ctor_m6540(tmp, il2cpp_codegen_string_new_wrapper("System.Core.dll"), NULL);
		cache->attributes[15] = (Il2CppObject*)tmp;
	}
	{
		AssemblyDescriptionAttribute_t709 * tmp;
		tmp = (AssemblyDescriptionAttribute_t709 *)il2cpp_codegen_object_new (AssemblyDescriptionAttribute_t709_il2cpp_TypeInfo_var);
		AssemblyDescriptionAttribute__ctor_m3467(tmp, il2cpp_codegen_string_new_wrapper("System.Core.dll"), NULL);
		cache->attributes[16] = (Il2CppObject*)tmp;
	}
	{
		AssemblyTitleAttribute_t715 * tmp;
		tmp = (AssemblyTitleAttribute_t715 *)il2cpp_codegen_object_new (AssemblyTitleAttribute_t715_il2cpp_TypeInfo_var);
		AssemblyTitleAttribute__ctor_m3473(tmp, il2cpp_codegen_string_new_wrapper("System.Core.dll"), NULL);
		cache->attributes[17] = (Il2CppObject*)tmp;
	}
}
// System.AttributeUsageAttribute
#include "mscorlib_System_AttributeUsageAttribute.h"
// System.AttributeUsageAttribute
#include "mscorlib_System_AttributeUsageAttributeMethodDeclarations.h"
extern TypeInfo* AttributeUsageAttribute_t1123_il2cpp_TypeInfo_var;
void ExtensionAttribute_t1111_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1123_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1870);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1123 * tmp;
		tmp = (AttributeUsageAttribute_t1123 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1123_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m5322(tmp, 69, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t1111_il2cpp_TypeInfo_var;
void Enumerable_t697_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t1111_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1858);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t1111 * tmp;
		tmp = (ExtensionAttribute_t1111 *)il2cpp_codegen_object_new (ExtensionAttribute_t1111_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m5256(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t1111_il2cpp_TypeInfo_var;
void Enumerable_t697_CustomAttributesCacheGenerator_Enumerable_Where_m6654(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t1111_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1858);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t1111 * tmp;
		tmp = (ExtensionAttribute_t1111 *)il2cpp_codegen_object_new (ExtensionAttribute_t1111_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m5256(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Diagnostics.DebuggerHiddenAttribute
#include "mscorlib_System_Diagnostics_DebuggerHiddenAttribute.h"
// System.Diagnostics.DebuggerHiddenAttribute
#include "mscorlib_System_Diagnostics_DebuggerHiddenAttributeMethodDeclarations.h"
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void Enumerable_t697_CustomAttributesCacheGenerator_Enumerable_CreateWhereIterator_m6655(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAt.h"
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAtMethodDeclarations.h"
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1496_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1496_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m6657(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1496_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m6658(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1496_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m6659(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1496_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m6660(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1496_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m6662(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const CustomAttributesCacheGenerator g_System_Core_Assembly_AttributeGenerators[12] = 
{
	NULL,
	g_System_Core_Assembly_CustomAttributesCacheGenerator,
	ExtensionAttribute_t1111_CustomAttributesCacheGenerator,
	Enumerable_t697_CustomAttributesCacheGenerator,
	Enumerable_t697_CustomAttributesCacheGenerator_Enumerable_Where_m6654,
	Enumerable_t697_CustomAttributesCacheGenerator_Enumerable_CreateWhereIterator_m6655,
	U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1496_CustomAttributesCacheGenerator,
	U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1496_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m6657,
	U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1496_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m6658,
	U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1496_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m6659,
	U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1496_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m6660,
	U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1496_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m6662,
};
