﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t558;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerable`1<UnityEngine.UIVertex>
struct IEnumerable_1_t3668;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UIVertex>
struct IEnumerator_1_t3665;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t217;
// System.Collections.Generic.ICollection`1<UnityEngine.UIVertex>
struct ICollection_1_t696;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>
struct ReadOnlyCollection_1_t3099;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t555;
// System.Predicate`1<UnityEngine.UIVertex>
struct Predicate_1_t3100;
// System.Comparison`1<UnityEngine.UIVertex>
struct Comparison_1_t3102;
// UnityEngine.UIVertex
#include "UnityEngine_UnityEngine_UIVertex.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_21.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::.ctor()
extern "C" void List_1__ctor_m3276_gshared (List_1_t558 * __this, const MethodInfo* method);
#define List_1__ctor_m3276(__this, method) (( void (*) (List_1_t558 *, const MethodInfo*))List_1__ctor_m3276_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m17671_gshared (List_1_t558 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m17671(__this, ___collection, method) (( void (*) (List_1_t558 *, Object_t*, const MethodInfo*))List_1__ctor_m17671_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::.ctor(System.Int32)
extern "C" void List_1__ctor_m5144_gshared (List_1_t558 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m5144(__this, ___capacity, method) (( void (*) (List_1_t558 *, int32_t, const MethodInfo*))List_1__ctor_m5144_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::.cctor()
extern "C" void List_1__cctor_m17672_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m17672(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m17672_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m17673_gshared (List_1_t558 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m17673(__this, method) (( Object_t* (*) (List_1_t558 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m17673_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m17674_gshared (List_1_t558 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m17674(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t558 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m17674_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m17675_gshared (List_1_t558 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m17675(__this, method) (( Object_t * (*) (List_1_t558 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m17675_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m17676_gshared (List_1_t558 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m17676(__this, ___item, method) (( int32_t (*) (List_1_t558 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m17676_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m17677_gshared (List_1_t558 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m17677(__this, ___item, method) (( bool (*) (List_1_t558 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m17677_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m17678_gshared (List_1_t558 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m17678(__this, ___item, method) (( int32_t (*) (List_1_t558 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m17678_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m17679_gshared (List_1_t558 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m17679(__this, ___index, ___item, method) (( void (*) (List_1_t558 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m17679_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m17680_gshared (List_1_t558 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m17680(__this, ___item, method) (( void (*) (List_1_t558 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m17680_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17681_gshared (List_1_t558 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17681(__this, method) (( bool (*) (List_1_t558 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17681_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m17682_gshared (List_1_t558 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m17682(__this, method) (( bool (*) (List_1_t558 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m17682_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m17683_gshared (List_1_t558 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m17683(__this, method) (( Object_t * (*) (List_1_t558 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m17683_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m17684_gshared (List_1_t558 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m17684(__this, method) (( bool (*) (List_1_t558 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m17684_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m17685_gshared (List_1_t558 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m17685(__this, method) (( bool (*) (List_1_t558 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m17685_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m17686_gshared (List_1_t558 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m17686(__this, ___index, method) (( Object_t * (*) (List_1_t558 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m17686_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m17687_gshared (List_1_t558 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m17687(__this, ___index, ___value, method) (( void (*) (List_1_t558 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m17687_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Add(T)
extern "C" void List_1_Add_m17688_gshared (List_1_t558 * __this, UIVertex_t556  ___item, const MethodInfo* method);
#define List_1_Add_m17688(__this, ___item, method) (( void (*) (List_1_t558 *, UIVertex_t556 , const MethodInfo*))List_1_Add_m17688_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m17689_gshared (List_1_t558 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m17689(__this, ___newCount, method) (( void (*) (List_1_t558 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m17689_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m17690_gshared (List_1_t558 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m17690(__this, ___collection, method) (( void (*) (List_1_t558 *, Object_t*, const MethodInfo*))List_1_AddCollection_m17690_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m17691_gshared (List_1_t558 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m17691(__this, ___enumerable, method) (( void (*) (List_1_t558 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m17691_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m17692_gshared (List_1_t558 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m17692(__this, ___collection, method) (( void (*) (List_1_t558 *, Object_t*, const MethodInfo*))List_1_AddRange_m17692_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.UIVertex>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t3099 * List_1_AsReadOnly_m17693_gshared (List_1_t558 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m17693(__this, method) (( ReadOnlyCollection_1_t3099 * (*) (List_1_t558 *, const MethodInfo*))List_1_AsReadOnly_m17693_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Clear()
extern "C" void List_1_Clear_m17694_gshared (List_1_t558 * __this, const MethodInfo* method);
#define List_1_Clear_m17694(__this, method) (( void (*) (List_1_t558 *, const MethodInfo*))List_1_Clear_m17694_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::Contains(T)
extern "C" bool List_1_Contains_m17695_gshared (List_1_t558 * __this, UIVertex_t556  ___item, const MethodInfo* method);
#define List_1_Contains_m17695(__this, ___item, method) (( bool (*) (List_1_t558 *, UIVertex_t556 , const MethodInfo*))List_1_Contains_m17695_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m17696_gshared (List_1_t558 * __this, UIVertexU5BU5D_t555* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m17696(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t558 *, UIVertexU5BU5D_t555*, int32_t, const MethodInfo*))List_1_CopyTo_m17696_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.UIVertex>::Find(System.Predicate`1<T>)
extern "C" UIVertex_t556  List_1_Find_m17697_gshared (List_1_t558 * __this, Predicate_1_t3100 * ___match, const MethodInfo* method);
#define List_1_Find_m17697(__this, ___match, method) (( UIVertex_t556  (*) (List_1_t558 *, Predicate_1_t3100 *, const MethodInfo*))List_1_Find_m17697_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m17698_gshared (Object_t * __this /* static, unused */, Predicate_1_t3100 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m17698(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3100 *, const MethodInfo*))List_1_CheckMatch_m17698_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m17699_gshared (List_1_t558 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t3100 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m17699(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t558 *, int32_t, int32_t, Predicate_1_t3100 *, const MethodInfo*))List_1_GetIndex_m17699_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.UIVertex>::GetEnumerator()
extern "C" Enumerator_t3101  List_1_GetEnumerator_m17700_gshared (List_1_t558 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m17700(__this, method) (( Enumerator_t3101  (*) (List_1_t558 *, const MethodInfo*))List_1_GetEnumerator_m17700_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m17701_gshared (List_1_t558 * __this, UIVertex_t556  ___item, const MethodInfo* method);
#define List_1_IndexOf_m17701(__this, ___item, method) (( int32_t (*) (List_1_t558 *, UIVertex_t556 , const MethodInfo*))List_1_IndexOf_m17701_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m17702_gshared (List_1_t558 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m17702(__this, ___start, ___delta, method) (( void (*) (List_1_t558 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m17702_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m17703_gshared (List_1_t558 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m17703(__this, ___index, method) (( void (*) (List_1_t558 *, int32_t, const MethodInfo*))List_1_CheckIndex_m17703_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m17704_gshared (List_1_t558 * __this, int32_t ___index, UIVertex_t556  ___item, const MethodInfo* method);
#define List_1_Insert_m17704(__this, ___index, ___item, method) (( void (*) (List_1_t558 *, int32_t, UIVertex_t556 , const MethodInfo*))List_1_Insert_m17704_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m17705_gshared (List_1_t558 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m17705(__this, ___collection, method) (( void (*) (List_1_t558 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m17705_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::Remove(T)
extern "C" bool List_1_Remove_m17706_gshared (List_1_t558 * __this, UIVertex_t556  ___item, const MethodInfo* method);
#define List_1_Remove_m17706(__this, ___item, method) (( bool (*) (List_1_t558 *, UIVertex_t556 , const MethodInfo*))List_1_Remove_m17706_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m17707_gshared (List_1_t558 * __this, Predicate_1_t3100 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m17707(__this, ___match, method) (( int32_t (*) (List_1_t558 *, Predicate_1_t3100 *, const MethodInfo*))List_1_RemoveAll_m17707_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m17708_gshared (List_1_t558 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m17708(__this, ___index, method) (( void (*) (List_1_t558 *, int32_t, const MethodInfo*))List_1_RemoveAt_m17708_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Reverse()
extern "C" void List_1_Reverse_m17709_gshared (List_1_t558 * __this, const MethodInfo* method);
#define List_1_Reverse_m17709(__this, method) (( void (*) (List_1_t558 *, const MethodInfo*))List_1_Reverse_m17709_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Sort()
extern "C" void List_1_Sort_m17710_gshared (List_1_t558 * __this, const MethodInfo* method);
#define List_1_Sort_m17710(__this, method) (( void (*) (List_1_t558 *, const MethodInfo*))List_1_Sort_m17710_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m17711_gshared (List_1_t558 * __this, Comparison_1_t3102 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m17711(__this, ___comparison, method) (( void (*) (List_1_t558 *, Comparison_1_t3102 *, const MethodInfo*))List_1_Sort_m17711_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.UIVertex>::ToArray()
extern "C" UIVertexU5BU5D_t555* List_1_ToArray_m3323_gshared (List_1_t558 * __this, const MethodInfo* method);
#define List_1_ToArray_m3323(__this, method) (( UIVertexU5BU5D_t555* (*) (List_1_t558 *, const MethodInfo*))List_1_ToArray_m3323_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::TrimExcess()
extern "C" void List_1_TrimExcess_m17712_gshared (List_1_t558 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m17712(__this, method) (( void (*) (List_1_t558 *, const MethodInfo*))List_1_TrimExcess_m17712_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m3201_gshared (List_1_t558 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m3201(__this, method) (( int32_t (*) (List_1_t558 *, const MethodInfo*))List_1_get_Capacity_m3201_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m3202_gshared (List_1_t558 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m3202(__this, ___value, method) (( void (*) (List_1_t558 *, int32_t, const MethodInfo*))List_1_set_Capacity_m3202_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Count()
extern "C" int32_t List_1_get_Count_m17713_gshared (List_1_t558 * __this, const MethodInfo* method);
#define List_1_get_Count_m17713(__this, method) (( int32_t (*) (List_1_t558 *, const MethodInfo*))List_1_get_Count_m17713_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Item(System.Int32)
extern "C" UIVertex_t556  List_1_get_Item_m17714_gshared (List_1_t558 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m17714(__this, ___index, method) (( UIVertex_t556  (*) (List_1_t558 *, int32_t, const MethodInfo*))List_1_get_Item_m17714_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m17715_gshared (List_1_t558 * __this, int32_t ___index, UIVertex_t556  ___value, const MethodInfo* method);
#define List_1_set_Item_m17715(__this, ___index, ___value, method) (( void (*) (List_1_t558 *, int32_t, UIVertex_t556 , const MethodInfo*))List_1_set_Item_m17715_gshared)(__this, ___index, ___value, method)
