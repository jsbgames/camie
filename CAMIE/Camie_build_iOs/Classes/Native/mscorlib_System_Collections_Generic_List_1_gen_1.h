﻿#pragma once
#include <stdint.h>
// UnityEngine.Material[]
struct MaterialU5BU5D_t252;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<UnityEngine.Material>
struct  List_1_t251  : public Object_t
{
	// T[] System.Collections.Generic.List`1<UnityEngine.Material>::_items
	MaterialU5BU5D_t252* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.Material>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.Material>::_version
	int32_t ____version_3;
};
struct List_1_t251_StaticFields{
	// T[] System.Collections.Generic.List`1<UnityEngine.Material>::EmptyArray
	MaterialU5BU5D_t252* ___EmptyArray_4;
};
