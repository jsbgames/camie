﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "stringLiterals.h"
// UnityStandardAssets.ImageEffects.Triangles
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_63.h"
// Metadata Definition UnityStandardAssets.ImageEffects.Triangles
extern TypeInfo Triangles_t130_il2cpp_TypeInfo;
// UnityStandardAssets.ImageEffects.Triangles
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_63MethodDeclarations.h"
extern const Il2CppType Void_t272_0_0_0;
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.Triangles::.ctor()
extern const MethodInfo Triangles__ctor_m381_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Triangles__ctor_m381/* method */
	, &Triangles_t130_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 394/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.Triangles::.cctor()
extern const MethodInfo Triangles__cctor_m382_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&Triangles__cctor_m382/* method */
	, &Triangles_t130_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 395/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityStandardAssets.ImageEffects.Triangles::HasMeshes()
extern const MethodInfo Triangles_HasMeshes_m383_MethodInfo = 
{
	"HasMeshes"/* name */
	, (methodPointerType)&Triangles_HasMeshes_m383/* method */
	, &Triangles_t130_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 396/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.Triangles::Cleanup()
extern const MethodInfo Triangles_Cleanup_m384_MethodInfo = 
{
	"Cleanup"/* name */
	, (methodPointerType)&Triangles_Cleanup_m384/* method */
	, &Triangles_t130_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 397/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo Triangles_t130_Triangles_GetMeshes_m385_ParameterInfos[] = 
{
	{"totalWidth", 0, 134218020, 0, &Int32_t253_0_0_0},
	{"totalHeight", 1, 134218021, 0, &Int32_t253_0_0_0},
};
extern const Il2CppType MeshU5BU5D_t113_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Mesh[] UnityStandardAssets.ImageEffects.Triangles::GetMeshes(System.Int32,System.Int32)
extern const MethodInfo Triangles_GetMeshes_m385_MethodInfo = 
{
	"GetMeshes"/* name */
	, (methodPointerType)&Triangles_GetMeshes_m385/* method */
	, &Triangles_t130_il2cpp_TypeInfo/* declaring_type */
	, &MeshU5BU5D_t113_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t253_Int32_t253/* invoker_method */
	, Triangles_t130_Triangles_GetMeshes_m385_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 398/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo Triangles_t130_Triangles_GetMesh_m386_ParameterInfos[] = 
{
	{"triCount", 0, 134218022, 0, &Int32_t253_0_0_0},
	{"triOffset", 1, 134218023, 0, &Int32_t253_0_0_0},
	{"totalWidth", 2, 134218024, 0, &Int32_t253_0_0_0},
	{"totalHeight", 3, 134218025, 0, &Int32_t253_0_0_0},
};
extern const Il2CppType Mesh_t216_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t253_Int32_t253_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Mesh UnityStandardAssets.ImageEffects.Triangles::GetMesh(System.Int32,System.Int32,System.Int32,System.Int32)
extern const MethodInfo Triangles_GetMesh_m386_MethodInfo = 
{
	"GetMesh"/* name */
	, (methodPointerType)&Triangles_GetMesh_m386/* method */
	, &Triangles_t130_il2cpp_TypeInfo/* declaring_type */
	, &Mesh_t216_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t253_Int32_t253_Int32_t253_Int32_t253/* invoker_method */
	, Triangles_t130_Triangles_GetMesh_m386_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 399/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Triangles_t130_MethodInfos[] =
{
	&Triangles__ctor_m381_MethodInfo,
	&Triangles__cctor_m382_MethodInfo,
	&Triangles_HasMeshes_m383_MethodInfo,
	&Triangles_Cleanup_m384_MethodInfo,
	&Triangles_GetMeshes_m385_MethodInfo,
	&Triangles_GetMesh_m386_MethodInfo,
	NULL
};
extern const MethodInfo Object_Equals_m1073_MethodInfo;
extern const MethodInfo Object_Finalize_m1048_MethodInfo;
extern const MethodInfo Object_GetHashCode_m1074_MethodInfo;
extern const MethodInfo Object_ToString_m1075_MethodInfo;
static const Il2CppMethodReference Triangles_t130_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool Triangles_t130_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType Triangles_t130_0_0_0;
extern const Il2CppType Triangles_t130_1_0_0;
extern const Il2CppType Object_t_0_0_0;
struct Triangles_t130;
const Il2CppTypeDefinitionMetadata Triangles_t130_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Triangles_t130_VTable/* vtableMethods */
	, Triangles_t130_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 691/* fieldStart */

};
TypeInfo Triangles_t130_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "Triangles"/* name */
	, "UnityStandardAssets.ImageEffects"/* namespaze */
	, Triangles_t130_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Triangles_t130_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Triangles_t130_0_0_0/* byval_arg */
	, &Triangles_t130_1_0_0/* this_arg */
	, &Triangles_t130_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Triangles_t130)/* instance_size */
	, sizeof (Triangles_t130)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Triangles_t130_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.ImageEffects.Twirl
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_64.h"
// Metadata Definition UnityStandardAssets.ImageEffects.Twirl
extern TypeInfo Twirl_t131_il2cpp_TypeInfo;
// UnityStandardAssets.ImageEffects.Twirl
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_64MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.Twirl::.ctor()
extern const MethodInfo Twirl__ctor_m387_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Twirl__ctor_m387/* method */
	, &Twirl_t131_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 400/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
static const ParameterInfo Twirl_t131_Twirl_OnRenderImage_m388_ParameterInfos[] = 
{
	{"source", 0, 134218026, 0, &RenderTexture_t101_0_0_0},
	{"destination", 1, 134218027, 0, &RenderTexture_t101_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.Twirl::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern const MethodInfo Twirl_OnRenderImage_m388_MethodInfo = 
{
	"OnRenderImage"/* name */
	, (methodPointerType)&Twirl_OnRenderImage_m388/* method */
	, &Twirl_t131_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, Twirl_t131_Twirl_OnRenderImage_m388_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 401/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Twirl_t131_MethodInfos[] =
{
	&Twirl__ctor_m387_MethodInfo,
	&Twirl_OnRenderImage_m388_MethodInfo,
	NULL
};
extern const MethodInfo Object_Equals_m1047_MethodInfo;
extern const MethodInfo Object_GetHashCode_m1049_MethodInfo;
extern const MethodInfo Object_ToString_m1050_MethodInfo;
extern const MethodInfo ImageEffectBase_Start_m305_MethodInfo;
extern const MethodInfo ImageEffectBase_OnDisable_m307_MethodInfo;
static const Il2CppMethodReference Twirl_t131_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	&ImageEffectBase_Start_m305_MethodInfo,
	&ImageEffectBase_OnDisable_m307_MethodInfo,
};
static bool Twirl_t131_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType Twirl_t131_0_0_0;
extern const Il2CppType Twirl_t131_1_0_0;
extern const Il2CppType ImageEffectBase_t88_0_0_0;
struct Twirl_t131;
const Il2CppTypeDefinitionMetadata Twirl_t131_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ImageEffectBase_t88_0_0_0/* parent */
	, Twirl_t131_VTable/* vtableMethods */
	, Twirl_t131_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 693/* fieldStart */

};
TypeInfo Twirl_t131_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "Twirl"/* name */
	, "UnityStandardAssets.ImageEffects"/* namespaze */
	, Twirl_t131_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Twirl_t131_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 117/* custom_attributes_cache */
	, &Twirl_t131_0_0_0/* byval_arg */
	, &Twirl_t131_1_0_0/* this_arg */
	, &Twirl_t131_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Twirl_t131)/* instance_size */
	, sizeof (Twirl_t131)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration/AberrationMode
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_65.h"
// Metadata Definition UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration/AberrationMode
extern TypeInfo AberrationMode_t132_il2cpp_TypeInfo;
// UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration/AberrationMode
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_65MethodDeclarations.h"
static const MethodInfo* AberrationMode_t132_MethodInfos[] =
{
	NULL
};
extern const MethodInfo Enum_Equals_m1051_MethodInfo;
extern const MethodInfo Enum_GetHashCode_m1052_MethodInfo;
extern const MethodInfo Enum_ToString_m1053_MethodInfo;
extern const MethodInfo Enum_ToString_m1054_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToBoolean_m1055_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToByte_m1056_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToChar_m1057_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDateTime_m1058_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDecimal_m1059_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDouble_m1060_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt16_m1061_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt32_m1062_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt64_m1063_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToSByte_m1064_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToSingle_m1065_MethodInfo;
extern const MethodInfo Enum_ToString_m1066_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToType_m1067_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt16_m1068_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt32_m1069_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt64_m1070_MethodInfo;
extern const MethodInfo Enum_CompareTo_m1071_MethodInfo;
extern const MethodInfo Enum_GetTypeCode_m1072_MethodInfo;
static const Il2CppMethodReference AberrationMode_t132_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool AberrationMode_t132_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IFormattable_t275_0_0_0;
extern const Il2CppType IConvertible_t276_0_0_0;
extern const Il2CppType IComparable_t277_0_0_0;
static Il2CppInterfaceOffsetPair AberrationMode_t132_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType AberrationMode_t132_0_0_0;
extern const Il2CppType AberrationMode_t132_1_0_0;
extern const Il2CppType Enum_t278_0_0_0;
extern TypeInfo VignetteAndChromaticAberration_t133_il2cpp_TypeInfo;
extern const Il2CppType VignetteAndChromaticAberration_t133_0_0_0;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t253_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata AberrationMode_t132_DefinitionMetadata = 
{
	&VignetteAndChromaticAberration_t133_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AberrationMode_t132_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, AberrationMode_t132_VTable/* vtableMethods */
	, AberrationMode_t132_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 696/* fieldStart */

};
TypeInfo AberrationMode_t132_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "AberrationMode"/* name */
	, ""/* namespaze */
	, AberrationMode_t132_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AberrationMode_t132_0_0_0/* byval_arg */
	, &AberrationMode_t132_1_0_0/* this_arg */
	, &AberrationMode_t132_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AberrationMode_t132)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (AberrationMode_t132)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_66.h"
// Metadata Definition UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration
// UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_66MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration::.ctor()
extern const MethodInfo VignetteAndChromaticAberration__ctor_m389_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&VignetteAndChromaticAberration__ctor_m389/* method */
	, &VignetteAndChromaticAberration_t133_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 402/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration::CheckResources()
extern const MethodInfo VignetteAndChromaticAberration_CheckResources_m390_MethodInfo = 
{
	"CheckResources"/* name */
	, (methodPointerType)&VignetteAndChromaticAberration_CheckResources_m390/* method */
	, &VignetteAndChromaticAberration_t133_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 403/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
static const ParameterInfo VignetteAndChromaticAberration_t133_VignetteAndChromaticAberration_OnRenderImage_m391_ParameterInfos[] = 
{
	{"source", 0, 134218028, 0, &RenderTexture_t101_0_0_0},
	{"destination", 1, 134218029, 0, &RenderTexture_t101_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern const MethodInfo VignetteAndChromaticAberration_OnRenderImage_m391_MethodInfo = 
{
	"OnRenderImage"/* name */
	, (methodPointerType)&VignetteAndChromaticAberration_OnRenderImage_m391/* method */
	, &VignetteAndChromaticAberration_t133_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, VignetteAndChromaticAberration_t133_VignetteAndChromaticAberration_OnRenderImage_m391_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 404/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* VignetteAndChromaticAberration_t133_MethodInfos[] =
{
	&VignetteAndChromaticAberration__ctor_m389_MethodInfo,
	&VignetteAndChromaticAberration_CheckResources_m390_MethodInfo,
	&VignetteAndChromaticAberration_OnRenderImage_m391_MethodInfo,
	NULL
};
static const Il2CppType* VignetteAndChromaticAberration_t133_il2cpp_TypeInfo__nestedTypes[1] =
{
	&AberrationMode_t132_0_0_0,
};
extern const MethodInfo VignetteAndChromaticAberration_CheckResources_m390_MethodInfo;
static const Il2CppMethodReference VignetteAndChromaticAberration_t133_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	&VignetteAndChromaticAberration_CheckResources_m390_MethodInfo,
};
static bool VignetteAndChromaticAberration_t133_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType VignetteAndChromaticAberration_t133_1_0_0;
extern const Il2CppType PostEffectsBase_t57_0_0_0;
struct VignetteAndChromaticAberration_t133;
const Il2CppTypeDefinitionMetadata VignetteAndChromaticAberration_t133_DefinitionMetadata = 
{
	NULL/* declaringType */
	, VignetteAndChromaticAberration_t133_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &PostEffectsBase_t57_0_0_0/* parent */
	, VignetteAndChromaticAberration_t133_VTable/* vtableMethods */
	, VignetteAndChromaticAberration_t133_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 699/* fieldStart */

};
TypeInfo VignetteAndChromaticAberration_t133_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "VignetteAndChromaticAberration"/* name */
	, "UnityStandardAssets.ImageEffects"/* namespaze */
	, VignetteAndChromaticAberration_t133_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &VignetteAndChromaticAberration_t133_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 118/* custom_attributes_cache */
	, &VignetteAndChromaticAberration_t133_0_0_0/* byval_arg */
	, &VignetteAndChromaticAberration_t133_1_0_0/* this_arg */
	, &VignetteAndChromaticAberration_t133_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (VignetteAndChromaticAberration_t133)/* instance_size */
	, sizeof (VignetteAndChromaticAberration_t133)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 14/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.ImageEffects.Vortex
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_67.h"
// Metadata Definition UnityStandardAssets.ImageEffects.Vortex
extern TypeInfo Vortex_t134_il2cpp_TypeInfo;
// UnityStandardAssets.ImageEffects.Vortex
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_67MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.Vortex::.ctor()
extern const MethodInfo Vortex__ctor_m392_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Vortex__ctor_m392/* method */
	, &Vortex_t134_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 405/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RenderTexture_t101_0_0_0;
extern const Il2CppType RenderTexture_t101_0_0_0;
static const ParameterInfo Vortex_t134_Vortex_OnRenderImage_m393_ParameterInfos[] = 
{
	{"source", 0, 134218030, 0, &RenderTexture_t101_0_0_0},
	{"destination", 1, 134218031, 0, &RenderTexture_t101_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.ImageEffects.Vortex::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern const MethodInfo Vortex_OnRenderImage_m393_MethodInfo = 
{
	"OnRenderImage"/* name */
	, (methodPointerType)&Vortex_OnRenderImage_m393/* method */
	, &Vortex_t134_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, Vortex_t134_Vortex_OnRenderImage_m393_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 406/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Vortex_t134_MethodInfos[] =
{
	&Vortex__ctor_m392_MethodInfo,
	&Vortex_OnRenderImage_m393_MethodInfo,
	NULL
};
static const Il2CppMethodReference Vortex_t134_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	&ImageEffectBase_Start_m305_MethodInfo,
	&ImageEffectBase_OnDisable_m307_MethodInfo,
};
static bool Vortex_t134_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType Vortex_t134_0_0_0;
extern const Il2CppType Vortex_t134_1_0_0;
struct Vortex_t134;
const Il2CppTypeDefinitionMetadata Vortex_t134_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ImageEffectBase_t88_0_0_0/* parent */
	, Vortex_t134_VTable/* vtableMethods */
	, Vortex_t134_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 713/* fieldStart */

};
TypeInfo Vortex_t134_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "Vortex"/* name */
	, "UnityStandardAssets.ImageEffects"/* namespaze */
	, Vortex_t134_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Vortex_t134_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 119/* custom_attributes_cache */
	, &Vortex_t134_0_0_0/* byval_arg */
	, &Vortex_t134_1_0_0/* this_arg */
	, &Vortex_t134_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Vortex_t134)/* instance_size */
	, sizeof (Vortex_t134)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.Effects.AfterburnerPhysicsForce
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Effects_Af.h"
// Metadata Definition UnityStandardAssets.Effects.AfterburnerPhysicsForce
extern TypeInfo AfterburnerPhysicsForce_t137_il2cpp_TypeInfo;
// UnityStandardAssets.Effects.AfterburnerPhysicsForce
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Effects_AfMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Effects.AfterburnerPhysicsForce::.ctor()
extern const MethodInfo AfterburnerPhysicsForce__ctor_m394_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AfterburnerPhysicsForce__ctor_m394/* method */
	, &AfterburnerPhysicsForce_t137_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 407/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Effects.AfterburnerPhysicsForce::OnEnable()
extern const MethodInfo AfterburnerPhysicsForce_OnEnable_m395_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&AfterburnerPhysicsForce_OnEnable_m395/* method */
	, &AfterburnerPhysicsForce_t137_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 408/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Effects.AfterburnerPhysicsForce::FixedUpdate()
extern const MethodInfo AfterburnerPhysicsForce_FixedUpdate_m396_MethodInfo = 
{
	"FixedUpdate"/* name */
	, (methodPointerType)&AfterburnerPhysicsForce_FixedUpdate_m396/* method */
	, &AfterburnerPhysicsForce_t137_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 409/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Effects.AfterburnerPhysicsForce::OnDrawGizmosSelected()
extern const MethodInfo AfterburnerPhysicsForce_OnDrawGizmosSelected_m397_MethodInfo = 
{
	"OnDrawGizmosSelected"/* name */
	, (methodPointerType)&AfterburnerPhysicsForce_OnDrawGizmosSelected_m397/* method */
	, &AfterburnerPhysicsForce_t137_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 410/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AfterburnerPhysicsForce_t137_MethodInfos[] =
{
	&AfterburnerPhysicsForce__ctor_m394_MethodInfo,
	&AfterburnerPhysicsForce_OnEnable_m395_MethodInfo,
	&AfterburnerPhysicsForce_FixedUpdate_m396_MethodInfo,
	&AfterburnerPhysicsForce_OnDrawGizmosSelected_m397_MethodInfo,
	NULL
};
static const Il2CppMethodReference AfterburnerPhysicsForce_t137_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool AfterburnerPhysicsForce_t137_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType AfterburnerPhysicsForce_t137_0_0_0;
extern const Il2CppType AfterburnerPhysicsForce_t137_1_0_0;
extern const Il2CppType MonoBehaviour_t3_0_0_0;
struct AfterburnerPhysicsForce_t137;
const Il2CppTypeDefinitionMetadata AfterburnerPhysicsForce_t137_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, AfterburnerPhysicsForce_t137_VTable/* vtableMethods */
	, AfterburnerPhysicsForce_t137_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 716/* fieldStart */

};
TypeInfo AfterburnerPhysicsForce_t137_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "AfterburnerPhysicsForce"/* name */
	, "UnityStandardAssets.Effects"/* namespaze */
	, AfterburnerPhysicsForce_t137_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &AfterburnerPhysicsForce_t137_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 120/* custom_attributes_cache */
	, &AfterburnerPhysicsForce_t137_0_0_0/* byval_arg */
	, &AfterburnerPhysicsForce_t137_1_0_0/* this_arg */
	, &AfterburnerPhysicsForce_t137_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AfterburnerPhysicsForce_t137)/* instance_size */
	, sizeof (AfterburnerPhysicsForce_t137)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>c__Iterator0
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Effects_Ex.h"
// Metadata Definition UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>c__Iterator0
extern TypeInfo U3CStartU3Ec__Iterator0_t140_il2cpp_TypeInfo;
// UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>c__Iterator0
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Effects_ExMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>c__Iterator0::.ctor()
extern const MethodInfo U3CStartU3Ec__Iterator0__ctor_m398_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CStartU3Ec__Iterator0__ctor_m398/* method */
	, &U3CStartU3Ec__Iterator0_t140_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 414/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern const MethodInfo U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m399_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<object>.get_Current"/* name */
	, (methodPointerType)&U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m399/* method */
	, &U3CStartU3Ec__Iterator0_t140_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 123/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 415/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern const MethodInfo U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m400_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m400/* method */
	, &U3CStartU3Ec__Iterator0_t140_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 124/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 416/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>c__Iterator0::MoveNext()
extern const MethodInfo U3CStartU3Ec__Iterator0_MoveNext_m401_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&U3CStartU3Ec__Iterator0_MoveNext_m401/* method */
	, &U3CStartU3Ec__Iterator0_t140_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 417/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>c__Iterator0::Dispose()
extern const MethodInfo U3CStartU3Ec__Iterator0_Dispose_m402_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&U3CStartU3Ec__Iterator0_Dispose_m402/* method */
	, &U3CStartU3Ec__Iterator0_t140_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 125/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 418/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>c__Iterator0::Reset()
extern const MethodInfo U3CStartU3Ec__Iterator0_Reset_m403_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&U3CStartU3Ec__Iterator0_Reset_m403/* method */
	, &U3CStartU3Ec__Iterator0_t140_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 126/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 419/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CStartU3Ec__Iterator0_t140_MethodInfos[] =
{
	&U3CStartU3Ec__Iterator0__ctor_m398_MethodInfo,
	&U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m399_MethodInfo,
	&U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m400_MethodInfo,
	&U3CStartU3Ec__Iterator0_MoveNext_m401_MethodInfo,
	&U3CStartU3Ec__Iterator0_Dispose_m402_MethodInfo,
	&U3CStartU3Ec__Iterator0_Reset_m403_MethodInfo,
	NULL
};
extern const MethodInfo U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m399_MethodInfo;
static const PropertyInfo U3CStartU3Ec__Iterator0_t140____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo = 
{
	&U3CStartU3Ec__Iterator0_t140_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<object>.Current"/* name */
	, &U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m399_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m400_MethodInfo;
static const PropertyInfo U3CStartU3Ec__Iterator0_t140____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CStartU3Ec__Iterator0_t140_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m400_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* U3CStartU3Ec__Iterator0_t140_PropertyInfos[] =
{
	&U3CStartU3Ec__Iterator0_t140____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo,
	&U3CStartU3Ec__Iterator0_t140____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern const MethodInfo U3CStartU3Ec__Iterator0_MoveNext_m401_MethodInfo;
extern const MethodInfo U3CStartU3Ec__Iterator0_Dispose_m402_MethodInfo;
extern const MethodInfo U3CStartU3Ec__Iterator0_Reset_m403_MethodInfo;
static const Il2CppMethodReference U3CStartU3Ec__Iterator0_t140_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m399_MethodInfo,
	&U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m400_MethodInfo,
	&U3CStartU3Ec__Iterator0_MoveNext_m401_MethodInfo,
	&U3CStartU3Ec__Iterator0_Dispose_m402_MethodInfo,
	&U3CStartU3Ec__Iterator0_Reset_m403_MethodInfo,
};
static bool U3CStartU3Ec__Iterator0_t140_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IEnumerator_1_t284_0_0_0;
extern const Il2CppType IEnumerator_t217_0_0_0;
extern const Il2CppType IDisposable_t233_0_0_0;
static const Il2CppType* U3CStartU3Ec__Iterator0_t140_InterfacesTypeInfos[] = 
{
	&IEnumerator_1_t284_0_0_0,
	&IEnumerator_t217_0_0_0,
	&IDisposable_t233_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CStartU3Ec__Iterator0_t140_InterfacesOffsets[] = 
{
	{ &IEnumerator_1_t284_0_0_0, 4},
	{ &IEnumerator_t217_0_0_0, 5},
	{ &IDisposable_t233_0_0_0, 7},
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType U3CStartU3Ec__Iterator0_t140_0_0_0;
extern const Il2CppType U3CStartU3Ec__Iterator0_t140_1_0_0;
extern TypeInfo ExplosionFireAndDebris_t139_il2cpp_TypeInfo;
extern const Il2CppType ExplosionFireAndDebris_t139_0_0_0;
struct U3CStartU3Ec__Iterator0_t140;
const Il2CppTypeDefinitionMetadata U3CStartU3Ec__Iterator0_t140_DefinitionMetadata = 
{
	&ExplosionFireAndDebris_t139_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CStartU3Ec__Iterator0_t140_InterfacesTypeInfos/* implementedInterfaces */
	, U3CStartU3Ec__Iterator0_t140_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CStartU3Ec__Iterator0_t140_VTable/* vtableMethods */
	, U3CStartU3Ec__Iterator0_t140_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 722/* fieldStart */

};
TypeInfo U3CStartU3Ec__Iterator0_t140_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "<Start>c__Iterator0"/* name */
	, ""/* namespaze */
	, U3CStartU3Ec__Iterator0_t140_MethodInfos/* methods */
	, U3CStartU3Ec__Iterator0_t140_PropertyInfos/* properties */
	, NULL/* events */
	, &U3CStartU3Ec__Iterator0_t140_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 122/* custom_attributes_cache */
	, &U3CStartU3Ec__Iterator0_t140_0_0_0/* byval_arg */
	, &U3CStartU3Ec__Iterator0_t140_1_0_0/* this_arg */
	, &U3CStartU3Ec__Iterator0_t140_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CStartU3Ec__Iterator0_t140)/* instance_size */
	, sizeof (U3CStartU3Ec__Iterator0_t140)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 18/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityStandardAssets.Effects.ExplosionFireAndDebris
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Effects_Ex_0.h"
// Metadata Definition UnityStandardAssets.Effects.ExplosionFireAndDebris
// UnityStandardAssets.Effects.ExplosionFireAndDebris
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Effects_Ex_0MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Effects.ExplosionFireAndDebris::.ctor()
extern const MethodInfo ExplosionFireAndDebris__ctor_m404_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ExplosionFireAndDebris__ctor_m404/* method */
	, &ExplosionFireAndDebris_t139_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 411/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.IEnumerator UnityStandardAssets.Effects.ExplosionFireAndDebris::Start()
extern const MethodInfo ExplosionFireAndDebris_Start_m405_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&ExplosionFireAndDebris_Start_m405/* method */
	, &ExplosionFireAndDebris_t139_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t217_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 121/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 412/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Transform_t1_0_0_0;
extern const Il2CppType Transform_t1_0_0_0;
extern const Il2CppType Vector3_t4_0_0_0;
extern const Il2CppType Vector3_t4_0_0_0;
extern const Il2CppType Vector3_t4_0_0_0;
static const ParameterInfo ExplosionFireAndDebris_t139_ExplosionFireAndDebris_AddFire_m406_ParameterInfos[] = 
{
	{"t", 0, 134218032, 0, &Transform_t1_0_0_0},
	{"pos", 1, 134218033, 0, &Vector3_t4_0_0_0},
	{"normal", 2, 134218034, 0, &Vector3_t4_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Vector3_t4_Vector3_t4 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Effects.ExplosionFireAndDebris::AddFire(UnityEngine.Transform,UnityEngine.Vector3,UnityEngine.Vector3)
extern const MethodInfo ExplosionFireAndDebris_AddFire_m406_MethodInfo = 
{
	"AddFire"/* name */
	, (methodPointerType)&ExplosionFireAndDebris_AddFire_m406/* method */
	, &ExplosionFireAndDebris_t139_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Vector3_t4_Vector3_t4/* invoker_method */
	, ExplosionFireAndDebris_t139_ExplosionFireAndDebris_AddFire_m406_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 413/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ExplosionFireAndDebris_t139_MethodInfos[] =
{
	&ExplosionFireAndDebris__ctor_m404_MethodInfo,
	&ExplosionFireAndDebris_Start_m405_MethodInfo,
	&ExplosionFireAndDebris_AddFire_m406_MethodInfo,
	NULL
};
static const Il2CppType* ExplosionFireAndDebris_t139_il2cpp_TypeInfo__nestedTypes[1] =
{
	&U3CStartU3Ec__Iterator0_t140_0_0_0,
};
static const Il2CppMethodReference ExplosionFireAndDebris_t139_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool ExplosionFireAndDebris_t139_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType ExplosionFireAndDebris_t139_1_0_0;
struct ExplosionFireAndDebris_t139;
const Il2CppTypeDefinitionMetadata ExplosionFireAndDebris_t139_DefinitionMetadata = 
{
	NULL/* declaringType */
	, ExplosionFireAndDebris_t139_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, ExplosionFireAndDebris_t139_VTable/* vtableMethods */
	, ExplosionFireAndDebris_t139_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 740/* fieldStart */

};
TypeInfo ExplosionFireAndDebris_t139_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "ExplosionFireAndDebris"/* name */
	, "UnityStandardAssets.Effects"/* namespaze */
	, ExplosionFireAndDebris_t139_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ExplosionFireAndDebris_t139_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ExplosionFireAndDebris_t139_0_0_0/* byval_arg */
	, &ExplosionFireAndDebris_t139_1_0_0/* this_arg */
	, &ExplosionFireAndDebris_t139_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ExplosionFireAndDebris_t139)/* instance_size */
	, sizeof (ExplosionFireAndDebris_t139)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.Effects.ExplosionPhysicsForce/<Start>c__Iterator1
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Effects_Ex_1.h"
// Metadata Definition UnityStandardAssets.Effects.ExplosionPhysicsForce/<Start>c__Iterator1
extern TypeInfo U3CStartU3Ec__Iterator1_t144_il2cpp_TypeInfo;
// UnityStandardAssets.Effects.ExplosionPhysicsForce/<Start>c__Iterator1
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Effects_Ex_1MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Effects.ExplosionPhysicsForce/<Start>c__Iterator1::.ctor()
extern const MethodInfo U3CStartU3Ec__Iterator1__ctor_m407_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CStartU3Ec__Iterator1__ctor_m407/* method */
	, &U3CStartU3Ec__Iterator1_t144_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 422/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object UnityStandardAssets.Effects.ExplosionPhysicsForce/<Start>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern const MethodInfo U3CStartU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m408_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<object>.get_Current"/* name */
	, (methodPointerType)&U3CStartU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m408/* method */
	, &U3CStartU3Ec__Iterator1_t144_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 129/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 423/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object UnityStandardAssets.Effects.ExplosionPhysicsForce/<Start>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern const MethodInfo U3CStartU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m409_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&U3CStartU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m409/* method */
	, &U3CStartU3Ec__Iterator1_t144_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 130/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 424/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityStandardAssets.Effects.ExplosionPhysicsForce/<Start>c__Iterator1::MoveNext()
extern const MethodInfo U3CStartU3Ec__Iterator1_MoveNext_m410_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&U3CStartU3Ec__Iterator1_MoveNext_m410/* method */
	, &U3CStartU3Ec__Iterator1_t144_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 425/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Effects.ExplosionPhysicsForce/<Start>c__Iterator1::Dispose()
extern const MethodInfo U3CStartU3Ec__Iterator1_Dispose_m411_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&U3CStartU3Ec__Iterator1_Dispose_m411/* method */
	, &U3CStartU3Ec__Iterator1_t144_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 131/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 426/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Effects.ExplosionPhysicsForce/<Start>c__Iterator1::Reset()
extern const MethodInfo U3CStartU3Ec__Iterator1_Reset_m412_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&U3CStartU3Ec__Iterator1_Reset_m412/* method */
	, &U3CStartU3Ec__Iterator1_t144_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 132/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 427/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CStartU3Ec__Iterator1_t144_MethodInfos[] =
{
	&U3CStartU3Ec__Iterator1__ctor_m407_MethodInfo,
	&U3CStartU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m408_MethodInfo,
	&U3CStartU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m409_MethodInfo,
	&U3CStartU3Ec__Iterator1_MoveNext_m410_MethodInfo,
	&U3CStartU3Ec__Iterator1_Dispose_m411_MethodInfo,
	&U3CStartU3Ec__Iterator1_Reset_m412_MethodInfo,
	NULL
};
extern const MethodInfo U3CStartU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m408_MethodInfo;
static const PropertyInfo U3CStartU3Ec__Iterator1_t144____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo = 
{
	&U3CStartU3Ec__Iterator1_t144_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<object>.Current"/* name */
	, &U3CStartU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m408_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo U3CStartU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m409_MethodInfo;
static const PropertyInfo U3CStartU3Ec__Iterator1_t144____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CStartU3Ec__Iterator1_t144_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CStartU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m409_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* U3CStartU3Ec__Iterator1_t144_PropertyInfos[] =
{
	&U3CStartU3Ec__Iterator1_t144____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo,
	&U3CStartU3Ec__Iterator1_t144____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern const MethodInfo U3CStartU3Ec__Iterator1_MoveNext_m410_MethodInfo;
extern const MethodInfo U3CStartU3Ec__Iterator1_Dispose_m411_MethodInfo;
extern const MethodInfo U3CStartU3Ec__Iterator1_Reset_m412_MethodInfo;
static const Il2CppMethodReference U3CStartU3Ec__Iterator1_t144_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&U3CStartU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m408_MethodInfo,
	&U3CStartU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m409_MethodInfo,
	&U3CStartU3Ec__Iterator1_MoveNext_m410_MethodInfo,
	&U3CStartU3Ec__Iterator1_Dispose_m411_MethodInfo,
	&U3CStartU3Ec__Iterator1_Reset_m412_MethodInfo,
};
static bool U3CStartU3Ec__Iterator1_t144_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* U3CStartU3Ec__Iterator1_t144_InterfacesTypeInfos[] = 
{
	&IEnumerator_1_t284_0_0_0,
	&IEnumerator_t217_0_0_0,
	&IDisposable_t233_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CStartU3Ec__Iterator1_t144_InterfacesOffsets[] = 
{
	{ &IEnumerator_1_t284_0_0_0, 4},
	{ &IEnumerator_t217_0_0_0, 5},
	{ &IDisposable_t233_0_0_0, 7},
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType U3CStartU3Ec__Iterator1_t144_0_0_0;
extern const Il2CppType U3CStartU3Ec__Iterator1_t144_1_0_0;
extern TypeInfo ExplosionPhysicsForce_t143_il2cpp_TypeInfo;
extern const Il2CppType ExplosionPhysicsForce_t143_0_0_0;
struct U3CStartU3Ec__Iterator1_t144;
const Il2CppTypeDefinitionMetadata U3CStartU3Ec__Iterator1_t144_DefinitionMetadata = 
{
	&ExplosionPhysicsForce_t143_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CStartU3Ec__Iterator1_t144_InterfacesTypeInfos/* implementedInterfaces */
	, U3CStartU3Ec__Iterator1_t144_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CStartU3Ec__Iterator1_t144_VTable/* vtableMethods */
	, U3CStartU3Ec__Iterator1_t144_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 744/* fieldStart */

};
TypeInfo U3CStartU3Ec__Iterator1_t144_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "<Start>c__Iterator1"/* name */
	, ""/* namespaze */
	, U3CStartU3Ec__Iterator1_t144_MethodInfos/* methods */
	, U3CStartU3Ec__Iterator1_t144_PropertyInfos/* properties */
	, NULL/* events */
	, &U3CStartU3Ec__Iterator1_t144_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 128/* custom_attributes_cache */
	, &U3CStartU3Ec__Iterator1_t144_0_0_0/* byval_arg */
	, &U3CStartU3Ec__Iterator1_t144_1_0_0/* this_arg */
	, &U3CStartU3Ec__Iterator1_t144_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CStartU3Ec__Iterator1_t144)/* instance_size */
	, sizeof (U3CStartU3Ec__Iterator1_t144)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityStandardAssets.Effects.ExplosionPhysicsForce
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Effects_Ex_2.h"
// Metadata Definition UnityStandardAssets.Effects.ExplosionPhysicsForce
// UnityStandardAssets.Effects.ExplosionPhysicsForce
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Effects_Ex_2MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Effects.ExplosionPhysicsForce::.ctor()
extern const MethodInfo ExplosionPhysicsForce__ctor_m413_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ExplosionPhysicsForce__ctor_m413/* method */
	, &ExplosionPhysicsForce_t143_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 420/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.IEnumerator UnityStandardAssets.Effects.ExplosionPhysicsForce::Start()
extern const MethodInfo ExplosionPhysicsForce_Start_m414_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&ExplosionPhysicsForce_Start_m414/* method */
	, &ExplosionPhysicsForce_t143_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t217_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 127/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 421/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ExplosionPhysicsForce_t143_MethodInfos[] =
{
	&ExplosionPhysicsForce__ctor_m413_MethodInfo,
	&ExplosionPhysicsForce_Start_m414_MethodInfo,
	NULL
};
static const Il2CppType* ExplosionPhysicsForce_t143_il2cpp_TypeInfo__nestedTypes[1] =
{
	&U3CStartU3Ec__Iterator1_t144_0_0_0,
};
static const Il2CppMethodReference ExplosionPhysicsForce_t143_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool ExplosionPhysicsForce_t143_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType ExplosionPhysicsForce_t143_1_0_0;
struct ExplosionPhysicsForce_t143;
const Il2CppTypeDefinitionMetadata ExplosionPhysicsForce_t143_DefinitionMetadata = 
{
	NULL/* declaringType */
	, ExplosionPhysicsForce_t143_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, ExplosionPhysicsForce_t143_VTable/* vtableMethods */
	, ExplosionPhysicsForce_t143_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 756/* fieldStart */

};
TypeInfo ExplosionPhysicsForce_t143_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "ExplosionPhysicsForce"/* name */
	, "UnityStandardAssets.Effects"/* namespaze */
	, ExplosionPhysicsForce_t143_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ExplosionPhysicsForce_t143_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ExplosionPhysicsForce_t143_0_0_0/* byval_arg */
	, &ExplosionPhysicsForce_t143_1_0_0/* this_arg */
	, &ExplosionPhysicsForce_t143_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ExplosionPhysicsForce_t143)/* instance_size */
	, sizeof (ExplosionPhysicsForce_t143)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.Effects.Explosive/<OnCollisionEnter>c__Iterator2
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Effects_Ex_3.h"
// Metadata Definition UnityStandardAssets.Effects.Explosive/<OnCollisionEnter>c__Iterator2
extern TypeInfo U3COnCollisionEnterU3Ec__Iterator2_t148_il2cpp_TypeInfo;
// UnityStandardAssets.Effects.Explosive/<OnCollisionEnter>c__Iterator2
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Effects_Ex_3MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Effects.Explosive/<OnCollisionEnter>c__Iterator2::.ctor()
extern const MethodInfo U3COnCollisionEnterU3Ec__Iterator2__ctor_m415_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3COnCollisionEnterU3Ec__Iterator2__ctor_m415/* method */
	, &U3COnCollisionEnterU3Ec__Iterator2_t148_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 432/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object UnityStandardAssets.Effects.Explosive/<OnCollisionEnter>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern const MethodInfo U3COnCollisionEnterU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m416_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<object>.get_Current"/* name */
	, (methodPointerType)&U3COnCollisionEnterU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m416/* method */
	, &U3COnCollisionEnterU3Ec__Iterator2_t148_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 135/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 433/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object UnityStandardAssets.Effects.Explosive/<OnCollisionEnter>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern const MethodInfo U3COnCollisionEnterU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m417_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&U3COnCollisionEnterU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m417/* method */
	, &U3COnCollisionEnterU3Ec__Iterator2_t148_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 136/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 434/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityStandardAssets.Effects.Explosive/<OnCollisionEnter>c__Iterator2::MoveNext()
extern const MethodInfo U3COnCollisionEnterU3Ec__Iterator2_MoveNext_m418_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&U3COnCollisionEnterU3Ec__Iterator2_MoveNext_m418/* method */
	, &U3COnCollisionEnterU3Ec__Iterator2_t148_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 435/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Effects.Explosive/<OnCollisionEnter>c__Iterator2::Dispose()
extern const MethodInfo U3COnCollisionEnterU3Ec__Iterator2_Dispose_m419_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&U3COnCollisionEnterU3Ec__Iterator2_Dispose_m419/* method */
	, &U3COnCollisionEnterU3Ec__Iterator2_t148_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 137/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 436/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Effects.Explosive/<OnCollisionEnter>c__Iterator2::Reset()
extern const MethodInfo U3COnCollisionEnterU3Ec__Iterator2_Reset_m420_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&U3COnCollisionEnterU3Ec__Iterator2_Reset_m420/* method */
	, &U3COnCollisionEnterU3Ec__Iterator2_t148_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 138/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 437/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3COnCollisionEnterU3Ec__Iterator2_t148_MethodInfos[] =
{
	&U3COnCollisionEnterU3Ec__Iterator2__ctor_m415_MethodInfo,
	&U3COnCollisionEnterU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m416_MethodInfo,
	&U3COnCollisionEnterU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m417_MethodInfo,
	&U3COnCollisionEnterU3Ec__Iterator2_MoveNext_m418_MethodInfo,
	&U3COnCollisionEnterU3Ec__Iterator2_Dispose_m419_MethodInfo,
	&U3COnCollisionEnterU3Ec__Iterator2_Reset_m420_MethodInfo,
	NULL
};
extern const MethodInfo U3COnCollisionEnterU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m416_MethodInfo;
static const PropertyInfo U3COnCollisionEnterU3Ec__Iterator2_t148____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo = 
{
	&U3COnCollisionEnterU3Ec__Iterator2_t148_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<object>.Current"/* name */
	, &U3COnCollisionEnterU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m416_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo U3COnCollisionEnterU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m417_MethodInfo;
static const PropertyInfo U3COnCollisionEnterU3Ec__Iterator2_t148____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3COnCollisionEnterU3Ec__Iterator2_t148_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3COnCollisionEnterU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m417_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* U3COnCollisionEnterU3Ec__Iterator2_t148_PropertyInfos[] =
{
	&U3COnCollisionEnterU3Ec__Iterator2_t148____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo,
	&U3COnCollisionEnterU3Ec__Iterator2_t148____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern const MethodInfo U3COnCollisionEnterU3Ec__Iterator2_MoveNext_m418_MethodInfo;
extern const MethodInfo U3COnCollisionEnterU3Ec__Iterator2_Dispose_m419_MethodInfo;
extern const MethodInfo U3COnCollisionEnterU3Ec__Iterator2_Reset_m420_MethodInfo;
static const Il2CppMethodReference U3COnCollisionEnterU3Ec__Iterator2_t148_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&U3COnCollisionEnterU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m416_MethodInfo,
	&U3COnCollisionEnterU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m417_MethodInfo,
	&U3COnCollisionEnterU3Ec__Iterator2_MoveNext_m418_MethodInfo,
	&U3COnCollisionEnterU3Ec__Iterator2_Dispose_m419_MethodInfo,
	&U3COnCollisionEnterU3Ec__Iterator2_Reset_m420_MethodInfo,
};
static bool U3COnCollisionEnterU3Ec__Iterator2_t148_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* U3COnCollisionEnterU3Ec__Iterator2_t148_InterfacesTypeInfos[] = 
{
	&IEnumerator_1_t284_0_0_0,
	&IEnumerator_t217_0_0_0,
	&IDisposable_t233_0_0_0,
};
static Il2CppInterfaceOffsetPair U3COnCollisionEnterU3Ec__Iterator2_t148_InterfacesOffsets[] = 
{
	{ &IEnumerator_1_t284_0_0_0, 4},
	{ &IEnumerator_t217_0_0_0, 5},
	{ &IDisposable_t233_0_0_0, 7},
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType U3COnCollisionEnterU3Ec__Iterator2_t148_0_0_0;
extern const Il2CppType U3COnCollisionEnterU3Ec__Iterator2_t148_1_0_0;
extern TypeInfo Explosive_t147_il2cpp_TypeInfo;
extern const Il2CppType Explosive_t147_0_0_0;
struct U3COnCollisionEnterU3Ec__Iterator2_t148;
const Il2CppTypeDefinitionMetadata U3COnCollisionEnterU3Ec__Iterator2_t148_DefinitionMetadata = 
{
	&Explosive_t147_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3COnCollisionEnterU3Ec__Iterator2_t148_InterfacesTypeInfos/* implementedInterfaces */
	, U3COnCollisionEnterU3Ec__Iterator2_t148_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3COnCollisionEnterU3Ec__Iterator2_t148_VTable/* vtableMethods */
	, U3COnCollisionEnterU3Ec__Iterator2_t148_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 757/* fieldStart */

};
TypeInfo U3COnCollisionEnterU3Ec__Iterator2_t148_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "<OnCollisionEnter>c__Iterator2"/* name */
	, ""/* namespaze */
	, U3COnCollisionEnterU3Ec__Iterator2_t148_MethodInfos/* methods */
	, U3COnCollisionEnterU3Ec__Iterator2_t148_PropertyInfos/* properties */
	, NULL/* events */
	, &U3COnCollisionEnterU3Ec__Iterator2_t148_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 134/* custom_attributes_cache */
	, &U3COnCollisionEnterU3Ec__Iterator2_t148_0_0_0/* byval_arg */
	, &U3COnCollisionEnterU3Ec__Iterator2_t148_1_0_0/* this_arg */
	, &U3COnCollisionEnterU3Ec__Iterator2_t148_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3COnCollisionEnterU3Ec__Iterator2_t148)/* instance_size */
	, sizeof (U3COnCollisionEnterU3Ec__Iterator2_t148)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityStandardAssets.Effects.Explosive
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Effects_Ex_4.h"
// Metadata Definition UnityStandardAssets.Effects.Explosive
// UnityStandardAssets.Effects.Explosive
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Effects_Ex_4MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Effects.Explosive::.ctor()
extern const MethodInfo Explosive__ctor_m421_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Explosive__ctor_m421/* method */
	, &Explosive_t147_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 428/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Effects.Explosive::Start()
extern const MethodInfo Explosive_Start_m422_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&Explosive_Start_m422/* method */
	, &Explosive_t147_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 429/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Collision_t146_0_0_0;
extern const Il2CppType Collision_t146_0_0_0;
static const ParameterInfo Explosive_t147_Explosive_OnCollisionEnter_m423_ParameterInfos[] = 
{
	{"col", 0, 134218035, 0, &Collision_t146_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.IEnumerator UnityStandardAssets.Effects.Explosive::OnCollisionEnter(UnityEngine.Collision)
extern const MethodInfo Explosive_OnCollisionEnter_m423_MethodInfo = 
{
	"OnCollisionEnter"/* name */
	, (methodPointerType)&Explosive_OnCollisionEnter_m423/* method */
	, &Explosive_t147_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t217_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Explosive_t147_Explosive_OnCollisionEnter_m423_ParameterInfos/* parameters */
	, 133/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 430/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Effects.Explosive::Reset()
extern const MethodInfo Explosive_Reset_m424_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&Explosive_Reset_m424/* method */
	, &Explosive_t147_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 431/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Explosive_t147_MethodInfos[] =
{
	&Explosive__ctor_m421_MethodInfo,
	&Explosive_Start_m422_MethodInfo,
	&Explosive_OnCollisionEnter_m423_MethodInfo,
	&Explosive_Reset_m424_MethodInfo,
	NULL
};
static const Il2CppType* Explosive_t147_il2cpp_TypeInfo__nestedTypes[1] =
{
	&U3COnCollisionEnterU3Ec__Iterator2_t148_0_0_0,
};
static const Il2CppMethodReference Explosive_t147_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool Explosive_t147_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType Explosive_t147_1_0_0;
struct Explosive_t147;
const Il2CppTypeDefinitionMetadata Explosive_t147_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Explosive_t147_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, Explosive_t147_VTable/* vtableMethods */
	, Explosive_t147_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 763/* fieldStart */

};
TypeInfo Explosive_t147_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "Explosive"/* name */
	, "UnityStandardAssets.Effects"/* namespaze */
	, Explosive_t147_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Explosive_t147_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Explosive_t147_0_0_0/* byval_arg */
	, &Explosive_t147_1_0_0/* this_arg */
	, &Explosive_t147_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Explosive_t147)/* instance_size */
	, sizeof (Explosive_t147)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.Effects.ExtinguishableParticleSystem
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Effects_Ex_5.h"
// Metadata Definition UnityStandardAssets.Effects.ExtinguishableParticleSystem
extern TypeInfo ExtinguishableParticleSystem_t151_il2cpp_TypeInfo;
// UnityStandardAssets.Effects.ExtinguishableParticleSystem
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Effects_Ex_5MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Effects.ExtinguishableParticleSystem::.ctor()
extern const MethodInfo ExtinguishableParticleSystem__ctor_m425_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ExtinguishableParticleSystem__ctor_m425/* method */
	, &ExtinguishableParticleSystem_t151_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 438/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Effects.ExtinguishableParticleSystem::Start()
extern const MethodInfo ExtinguishableParticleSystem_Start_m426_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&ExtinguishableParticleSystem_Start_m426/* method */
	, &ExtinguishableParticleSystem_t151_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 439/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Effects.ExtinguishableParticleSystem::Extinguish()
extern const MethodInfo ExtinguishableParticleSystem_Extinguish_m427_MethodInfo = 
{
	"Extinguish"/* name */
	, (methodPointerType)&ExtinguishableParticleSystem_Extinguish_m427/* method */
	, &ExtinguishableParticleSystem_t151_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 440/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ExtinguishableParticleSystem_t151_MethodInfos[] =
{
	&ExtinguishableParticleSystem__ctor_m425_MethodInfo,
	&ExtinguishableParticleSystem_Start_m426_MethodInfo,
	&ExtinguishableParticleSystem_Extinguish_m427_MethodInfo,
	NULL
};
static const Il2CppMethodReference ExtinguishableParticleSystem_t151_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool ExtinguishableParticleSystem_t151_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType ExtinguishableParticleSystem_t151_0_0_0;
extern const Il2CppType ExtinguishableParticleSystem_t151_1_0_0;
struct ExtinguishableParticleSystem_t151;
const Il2CppTypeDefinitionMetadata ExtinguishableParticleSystem_t151_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, ExtinguishableParticleSystem_t151_VTable/* vtableMethods */
	, ExtinguishableParticleSystem_t151_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 770/* fieldStart */

};
TypeInfo ExtinguishableParticleSystem_t151_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "ExtinguishableParticleSystem"/* name */
	, "UnityStandardAssets.Effects"/* namespaze */
	, ExtinguishableParticleSystem_t151_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ExtinguishableParticleSystem_t151_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ExtinguishableParticleSystem_t151_0_0_0/* byval_arg */
	, &ExtinguishableParticleSystem_t151_1_0_0/* this_arg */
	, &ExtinguishableParticleSystem_t151_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ExtinguishableParticleSystem_t151)/* instance_size */
	, sizeof (ExtinguishableParticleSystem_t151)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.Effects.FireLight
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Effects_Fi.h"
// Metadata Definition UnityStandardAssets.Effects.FireLight
extern TypeInfo FireLight_t153_il2cpp_TypeInfo;
// UnityStandardAssets.Effects.FireLight
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Effects_FiMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Effects.FireLight::.ctor()
extern const MethodInfo FireLight__ctor_m428_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&FireLight__ctor_m428/* method */
	, &FireLight_t153_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 441/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Effects.FireLight::Start()
extern const MethodInfo FireLight_Start_m429_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&FireLight_Start_m429/* method */
	, &FireLight_t153_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 442/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Effects.FireLight::Update()
extern const MethodInfo FireLight_Update_m430_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&FireLight_Update_m430/* method */
	, &FireLight_t153_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 443/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Effects.FireLight::Extinguish()
extern const MethodInfo FireLight_Extinguish_m431_MethodInfo = 
{
	"Extinguish"/* name */
	, (methodPointerType)&FireLight_Extinguish_m431/* method */
	, &FireLight_t153_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 444/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* FireLight_t153_MethodInfos[] =
{
	&FireLight__ctor_m428_MethodInfo,
	&FireLight_Start_m429_MethodInfo,
	&FireLight_Update_m430_MethodInfo,
	&FireLight_Extinguish_m431_MethodInfo,
	NULL
};
static const Il2CppMethodReference FireLight_t153_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool FireLight_t153_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType FireLight_t153_0_0_0;
extern const Il2CppType FireLight_t153_1_0_0;
struct FireLight_t153;
const Il2CppTypeDefinitionMetadata FireLight_t153_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, FireLight_t153_VTable/* vtableMethods */
	, FireLight_t153_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 772/* fieldStart */

};
TypeInfo FireLight_t153_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "FireLight"/* name */
	, "UnityStandardAssets.Effects"/* namespaze */
	, FireLight_t153_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &FireLight_t153_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &FireLight_t153_0_0_0/* byval_arg */
	, &FireLight_t153_1_0_0/* this_arg */
	, &FireLight_t153_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FireLight_t153)/* instance_size */
	, sizeof (FireLight_t153)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.Effects.Hose
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Effects_Ho.h"
// Metadata Definition UnityStandardAssets.Effects.Hose
extern TypeInfo Hose_t155_il2cpp_TypeInfo;
// UnityStandardAssets.Effects.Hose
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Effects_HoMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Effects.Hose::.ctor()
extern const MethodInfo Hose__ctor_m432_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Hose__ctor_m432/* method */
	, &Hose_t155_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 445/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Effects.Hose::Update()
extern const MethodInfo Hose_Update_m433_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&Hose_Update_m433/* method */
	, &Hose_t155_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 446/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Hose_t155_MethodInfos[] =
{
	&Hose__ctor_m432_MethodInfo,
	&Hose_Update_m433_MethodInfo,
	NULL
};
static const Il2CppMethodReference Hose_t155_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool Hose_t155_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType Hose_t155_0_0_0;
extern const Il2CppType Hose_t155_1_0_0;
struct Hose_t155;
const Il2CppTypeDefinitionMetadata Hose_t155_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, Hose_t155_VTable/* vtableMethods */
	, Hose_t155_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 775/* fieldStart */

};
TypeInfo Hose_t155_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "Hose"/* name */
	, "UnityStandardAssets.Effects"/* namespaze */
	, Hose_t155_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Hose_t155_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Hose_t155_0_0_0/* byval_arg */
	, &Hose_t155_1_0_0/* this_arg */
	, &Hose_t155_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Hose_t155)/* instance_size */
	, sizeof (Hose_t155)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.Effects.ParticleSystemMultiplier
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Effects_Pa.h"
// Metadata Definition UnityStandardAssets.Effects.ParticleSystemMultiplier
extern TypeInfo ParticleSystemMultiplier_t156_il2cpp_TypeInfo;
// UnityStandardAssets.Effects.ParticleSystemMultiplier
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Effects_PaMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Effects.ParticleSystemMultiplier::.ctor()
extern const MethodInfo ParticleSystemMultiplier__ctor_m434_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ParticleSystemMultiplier__ctor_m434/* method */
	, &ParticleSystemMultiplier_t156_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 447/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Effects.ParticleSystemMultiplier::Start()
extern const MethodInfo ParticleSystemMultiplier_Start_m435_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&ParticleSystemMultiplier_Start_m435/* method */
	, &ParticleSystemMultiplier_t156_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 448/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ParticleSystemMultiplier_t156_MethodInfos[] =
{
	&ParticleSystemMultiplier__ctor_m434_MethodInfo,
	&ParticleSystemMultiplier_Start_m435_MethodInfo,
	NULL
};
static const Il2CppMethodReference ParticleSystemMultiplier_t156_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool ParticleSystemMultiplier_t156_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType ParticleSystemMultiplier_t156_0_0_0;
extern const Il2CppType ParticleSystemMultiplier_t156_1_0_0;
struct ParticleSystemMultiplier_t156;
const Il2CppTypeDefinitionMetadata ParticleSystemMultiplier_t156_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, ParticleSystemMultiplier_t156_VTable/* vtableMethods */
	, ParticleSystemMultiplier_t156_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 781/* fieldStart */

};
TypeInfo ParticleSystemMultiplier_t156_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "ParticleSystemMultiplier"/* name */
	, "UnityStandardAssets.Effects"/* namespaze */
	, ParticleSystemMultiplier_t156_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ParticleSystemMultiplier_t156_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ParticleSystemMultiplier_t156_0_0_0/* byval_arg */
	, &ParticleSystemMultiplier_t156_1_0_0/* this_arg */
	, &ParticleSystemMultiplier_t156_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ParticleSystemMultiplier_t156)/* instance_size */
	, sizeof (ParticleSystemMultiplier_t156)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.Effects.SmokeParticles
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Effects_Sm.h"
// Metadata Definition UnityStandardAssets.Effects.SmokeParticles
extern TypeInfo SmokeParticles_t158_il2cpp_TypeInfo;
// UnityStandardAssets.Effects.SmokeParticles
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Effects_SmMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Effects.SmokeParticles::.ctor()
extern const MethodInfo SmokeParticles__ctor_m436_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SmokeParticles__ctor_m436/* method */
	, &SmokeParticles_t158_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 449/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Effects.SmokeParticles::Start()
extern const MethodInfo SmokeParticles_Start_m437_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&SmokeParticles_Start_m437/* method */
	, &SmokeParticles_t158_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 450/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SmokeParticles_t158_MethodInfos[] =
{
	&SmokeParticles__ctor_m436_MethodInfo,
	&SmokeParticles_Start_m437_MethodInfo,
	NULL
};
static const Il2CppMethodReference SmokeParticles_t158_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool SmokeParticles_t158_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType SmokeParticles_t158_0_0_0;
extern const Il2CppType SmokeParticles_t158_1_0_0;
struct SmokeParticles_t158;
const Il2CppTypeDefinitionMetadata SmokeParticles_t158_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, SmokeParticles_t158_VTable/* vtableMethods */
	, SmokeParticles_t158_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 782/* fieldStart */

};
TypeInfo SmokeParticles_t158_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "SmokeParticles"/* name */
	, "UnityStandardAssets.Effects"/* namespaze */
	, SmokeParticles_t158_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SmokeParticles_t158_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SmokeParticles_t158_0_0_0/* byval_arg */
	, &SmokeParticles_t158_1_0_0/* this_arg */
	, &SmokeParticles_t158_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SmokeParticles_t158)/* instance_size */
	, sizeof (SmokeParticles_t158)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.Effects.WaterHoseParticles
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Effects_Wa.h"
// Metadata Definition UnityStandardAssets.Effects.WaterHoseParticles
extern TypeInfo WaterHoseParticles_t162_il2cpp_TypeInfo;
// UnityStandardAssets.Effects.WaterHoseParticles
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Effects_WaMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Effects.WaterHoseParticles::.ctor()
extern const MethodInfo WaterHoseParticles__ctor_m438_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&WaterHoseParticles__ctor_m438/* method */
	, &WaterHoseParticles_t162_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 451/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Effects.WaterHoseParticles::Start()
extern const MethodInfo WaterHoseParticles_Start_m439_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&WaterHoseParticles_Start_m439/* method */
	, &WaterHoseParticles_t162_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 452/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType GameObject_t78_0_0_0;
extern const Il2CppType GameObject_t78_0_0_0;
static const ParameterInfo WaterHoseParticles_t162_WaterHoseParticles_OnParticleCollision_m440_ParameterInfos[] = 
{
	{"other", 0, 134218036, 0, &GameObject_t78_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Effects.WaterHoseParticles::OnParticleCollision(UnityEngine.GameObject)
extern const MethodInfo WaterHoseParticles_OnParticleCollision_m440_MethodInfo = 
{
	"OnParticleCollision"/* name */
	, (methodPointerType)&WaterHoseParticles_OnParticleCollision_m440/* method */
	, &WaterHoseParticles_t162_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, WaterHoseParticles_t162_WaterHoseParticles_OnParticleCollision_m440_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 453/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* WaterHoseParticles_t162_MethodInfos[] =
{
	&WaterHoseParticles__ctor_m438_MethodInfo,
	&WaterHoseParticles_Start_m439_MethodInfo,
	&WaterHoseParticles_OnParticleCollision_m440_MethodInfo,
	NULL
};
static const Il2CppMethodReference WaterHoseParticles_t162_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool WaterHoseParticles_t162_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType WaterHoseParticles_t162_0_0_0;
extern const Il2CppType WaterHoseParticles_t162_1_0_0;
struct WaterHoseParticles_t162;
const Il2CppTypeDefinitionMetadata WaterHoseParticles_t162_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, WaterHoseParticles_t162_VTable/* vtableMethods */
	, WaterHoseParticles_t162_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 783/* fieldStart */

};
TypeInfo WaterHoseParticles_t162_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "WaterHoseParticles"/* name */
	, "UnityStandardAssets.Effects"/* namespaze */
	, WaterHoseParticles_t162_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &WaterHoseParticles_t162_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &WaterHoseParticles_t162_0_0_0/* byval_arg */
	, &WaterHoseParticles_t162_1_0_0/* this_arg */
	, &WaterHoseParticles_t162_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WaterHoseParticles_t162)/* instance_size */
	, sizeof (WaterHoseParticles_t162)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(WaterHoseParticles_t162_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.Utility.ActivateTrigger/Mode
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Ac.h"
// Metadata Definition UnityStandardAssets.Utility.ActivateTrigger/Mode
extern TypeInfo Mode_t163_il2cpp_TypeInfo;
// UnityStandardAssets.Utility.ActivateTrigger/Mode
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_AcMethodDeclarations.h"
static const MethodInfo* Mode_t163_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference Mode_t163_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool Mode_t163_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Mode_t163_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType Mode_t163_0_0_0;
extern const Il2CppType Mode_t163_1_0_0;
extern TypeInfo ActivateTrigger_t165_il2cpp_TypeInfo;
extern const Il2CppType ActivateTrigger_t165_0_0_0;
const Il2CppTypeDefinitionMetadata Mode_t163_DefinitionMetadata = 
{
	&ActivateTrigger_t165_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Mode_t163_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, Mode_t163_VTable/* vtableMethods */
	, Mode_t163_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 787/* fieldStart */

};
TypeInfo Mode_t163_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "Mode"/* name */
	, ""/* namespaze */
	, Mode_t163_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Mode_t163_0_0_0/* byval_arg */
	, &Mode_t163_1_0_0/* this_arg */
	, &Mode_t163_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Mode_t163)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Mode_t163)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityStandardAssets.Utility.ActivateTrigger
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Ac_0.h"
// Metadata Definition UnityStandardAssets.Utility.ActivateTrigger
// UnityStandardAssets.Utility.ActivateTrigger
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Ac_0MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.ActivateTrigger::.ctor()
extern const MethodInfo ActivateTrigger__ctor_m441_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ActivateTrigger__ctor_m441/* method */
	, &ActivateTrigger_t165_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 454/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.ActivateTrigger::DoActivateTrigger()
extern const MethodInfo ActivateTrigger_DoActivateTrigger_m442_MethodInfo = 
{
	"DoActivateTrigger"/* name */
	, (methodPointerType)&ActivateTrigger_DoActivateTrigger_m442/* method */
	, &ActivateTrigger_t165_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 455/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Collider_t138_0_0_0;
extern const Il2CppType Collider_t138_0_0_0;
static const ParameterInfo ActivateTrigger_t165_ActivateTrigger_OnTriggerEnter_m443_ParameterInfos[] = 
{
	{"other", 0, 134218037, 0, &Collider_t138_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.ActivateTrigger::OnTriggerEnter(UnityEngine.Collider)
extern const MethodInfo ActivateTrigger_OnTriggerEnter_m443_MethodInfo = 
{
	"OnTriggerEnter"/* name */
	, (methodPointerType)&ActivateTrigger_OnTriggerEnter_m443/* method */
	, &ActivateTrigger_t165_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, ActivateTrigger_t165_ActivateTrigger_OnTriggerEnter_m443_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 456/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ActivateTrigger_t165_MethodInfos[] =
{
	&ActivateTrigger__ctor_m441_MethodInfo,
	&ActivateTrigger_DoActivateTrigger_m442_MethodInfo,
	&ActivateTrigger_OnTriggerEnter_m443_MethodInfo,
	NULL
};
static const Il2CppType* ActivateTrigger_t165_il2cpp_TypeInfo__nestedTypes[1] =
{
	&Mode_t163_0_0_0,
};
static const Il2CppMethodReference ActivateTrigger_t165_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool ActivateTrigger_t165_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType ActivateTrigger_t165_1_0_0;
struct ActivateTrigger_t165;
const Il2CppTypeDefinitionMetadata ActivateTrigger_t165_DefinitionMetadata = 
{
	NULL/* declaringType */
	, ActivateTrigger_t165_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, ActivateTrigger_t165_VTable/* vtableMethods */
	, ActivateTrigger_t165_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 794/* fieldStart */

};
TypeInfo ActivateTrigger_t165_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "ActivateTrigger"/* name */
	, "UnityStandardAssets.Utility"/* namespaze */
	, ActivateTrigger_t165_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ActivateTrigger_t165_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ActivateTrigger_t165_0_0_0/* byval_arg */
	, &ActivateTrigger_t165_1_0_0/* this_arg */
	, &ActivateTrigger_t165_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ActivateTrigger_t165)/* instance_size */
	, sizeof (ActivateTrigger_t165)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementDefinition
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Au.h"
// Metadata Definition UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementDefinition
extern TypeInfo ReplacementDefinition_t166_il2cpp_TypeInfo;
// UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementDefinition
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_AuMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementDefinition::.ctor()
extern const MethodInfo ReplacementDefinition__ctor_m444_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ReplacementDefinition__ctor_m444/* method */
	, &ReplacementDefinition_t166_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 459/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ReplacementDefinition_t166_MethodInfos[] =
{
	&ReplacementDefinition__ctor_m444_MethodInfo,
	NULL
};
static const Il2CppMethodReference ReplacementDefinition_t166_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool ReplacementDefinition_t166_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType ReplacementDefinition_t166_0_0_0;
extern const Il2CppType ReplacementDefinition_t166_1_0_0;
extern TypeInfo AutoMobileShaderSwitch_t169_il2cpp_TypeInfo;
extern const Il2CppType AutoMobileShaderSwitch_t169_0_0_0;
struct ReplacementDefinition_t166;
const Il2CppTypeDefinitionMetadata ReplacementDefinition_t166_DefinitionMetadata = 
{
	&AutoMobileShaderSwitch_t169_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ReplacementDefinition_t166_VTable/* vtableMethods */
	, ReplacementDefinition_t166_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 799/* fieldStart */

};
TypeInfo ReplacementDefinition_t166_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "ReplacementDefinition"/* name */
	, ""/* namespaze */
	, ReplacementDefinition_t166_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ReplacementDefinition_t166_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ReplacementDefinition_t166_0_0_0/* byval_arg */
	, &ReplacementDefinition_t166_1_0_0/* this_arg */
	, &ReplacementDefinition_t166_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ReplacementDefinition_t166)/* instance_size */
	, sizeof (ReplacementDefinition_t166)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056770/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementList
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Au_0.h"
// Metadata Definition UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementList
extern TypeInfo ReplacementList_t168_il2cpp_TypeInfo;
// UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementList
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Au_0MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementList::.ctor()
extern const MethodInfo ReplacementList__ctor_m445_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ReplacementList__ctor_m445/* method */
	, &ReplacementList_t168_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 460/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ReplacementList_t168_MethodInfos[] =
{
	&ReplacementList__ctor_m445_MethodInfo,
	NULL
};
static const Il2CppMethodReference ReplacementList_t168_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool ReplacementList_t168_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType ReplacementList_t168_0_0_0;
extern const Il2CppType ReplacementList_t168_1_0_0;
struct ReplacementList_t168;
const Il2CppTypeDefinitionMetadata ReplacementList_t168_DefinitionMetadata = 
{
	&AutoMobileShaderSwitch_t169_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ReplacementList_t168_VTable/* vtableMethods */
	, ReplacementList_t168_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 801/* fieldStart */

};
TypeInfo ReplacementList_t168_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "ReplacementList"/* name */
	, ""/* namespaze */
	, ReplacementList_t168_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ReplacementList_t168_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ReplacementList_t168_0_0_0/* byval_arg */
	, &ReplacementList_t168_1_0_0/* this_arg */
	, &ReplacementList_t168_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ReplacementList_t168)/* instance_size */
	, sizeof (ReplacementList_t168)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056770/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.Utility.AutoMobileShaderSwitch
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Au_1.h"
// Metadata Definition UnityStandardAssets.Utility.AutoMobileShaderSwitch
// UnityStandardAssets.Utility.AutoMobileShaderSwitch
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Au_1MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.AutoMobileShaderSwitch::.ctor()
extern const MethodInfo AutoMobileShaderSwitch__ctor_m446_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AutoMobileShaderSwitch__ctor_m446/* method */
	, &AutoMobileShaderSwitch_t169_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 457/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.AutoMobileShaderSwitch::OnEnable()
extern const MethodInfo AutoMobileShaderSwitch_OnEnable_m447_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&AutoMobileShaderSwitch_OnEnable_m447/* method */
	, &AutoMobileShaderSwitch_t169_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 458/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AutoMobileShaderSwitch_t169_MethodInfos[] =
{
	&AutoMobileShaderSwitch__ctor_m446_MethodInfo,
	&AutoMobileShaderSwitch_OnEnable_m447_MethodInfo,
	NULL
};
static const Il2CppType* AutoMobileShaderSwitch_t169_il2cpp_TypeInfo__nestedTypes[2] =
{
	&ReplacementDefinition_t166_0_0_0,
	&ReplacementList_t168_0_0_0,
};
static const Il2CppMethodReference AutoMobileShaderSwitch_t169_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool AutoMobileShaderSwitch_t169_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType AutoMobileShaderSwitch_t169_1_0_0;
struct AutoMobileShaderSwitch_t169;
const Il2CppTypeDefinitionMetadata AutoMobileShaderSwitch_t169_DefinitionMetadata = 
{
	NULL/* declaringType */
	, AutoMobileShaderSwitch_t169_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, AutoMobileShaderSwitch_t169_VTable/* vtableMethods */
	, AutoMobileShaderSwitch_t169_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 802/* fieldStart */

};
TypeInfo AutoMobileShaderSwitch_t169_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "AutoMobileShaderSwitch"/* name */
	, "UnityStandardAssets.Utility"/* namespaze */
	, AutoMobileShaderSwitch_t169_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &AutoMobileShaderSwitch_t169_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AutoMobileShaderSwitch_t169_0_0_0/* byval_arg */
	, &AutoMobileShaderSwitch_t169_1_0_0/* this_arg */
	, &AutoMobileShaderSwitch_t169_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AutoMobileShaderSwitch_t169)/* instance_size */
	, sizeof (AutoMobileShaderSwitch_t169)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.Utility.AutoMoveAndRotate/Vector3andSpace
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Au_2.h"
// Metadata Definition UnityStandardAssets.Utility.AutoMoveAndRotate/Vector3andSpace
extern TypeInfo Vector3andSpace_t170_il2cpp_TypeInfo;
// UnityStandardAssets.Utility.AutoMoveAndRotate/Vector3andSpace
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Au_2MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.AutoMoveAndRotate/Vector3andSpace::.ctor()
extern const MethodInfo Vector3andSpace__ctor_m448_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Vector3andSpace__ctor_m448/* method */
	, &Vector3andSpace_t170_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 464/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Vector3andSpace_t170_MethodInfos[] =
{
	&Vector3andSpace__ctor_m448_MethodInfo,
	NULL
};
static const Il2CppMethodReference Vector3andSpace_t170_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool Vector3andSpace_t170_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType Vector3andSpace_t170_0_0_0;
extern const Il2CppType Vector3andSpace_t170_1_0_0;
extern TypeInfo AutoMoveAndRotate_t171_il2cpp_TypeInfo;
extern const Il2CppType AutoMoveAndRotate_t171_0_0_0;
struct Vector3andSpace_t170;
const Il2CppTypeDefinitionMetadata Vector3andSpace_t170_DefinitionMetadata = 
{
	&AutoMoveAndRotate_t171_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Vector3andSpace_t170_VTable/* vtableMethods */
	, Vector3andSpace_t170_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 803/* fieldStart */

};
TypeInfo Vector3andSpace_t170_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "Vector3andSpace"/* name */
	, ""/* namespaze */
	, Vector3andSpace_t170_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Vector3andSpace_t170_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Vector3andSpace_t170_0_0_0/* byval_arg */
	, &Vector3andSpace_t170_1_0_0/* this_arg */
	, &Vector3andSpace_t170_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Vector3andSpace_t170)/* instance_size */
	, sizeof (Vector3andSpace_t170)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056770/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.Utility.AutoMoveAndRotate
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Au_3.h"
// Metadata Definition UnityStandardAssets.Utility.AutoMoveAndRotate
// UnityStandardAssets.Utility.AutoMoveAndRotate
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Au_3MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.AutoMoveAndRotate::.ctor()
extern const MethodInfo AutoMoveAndRotate__ctor_m449_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AutoMoveAndRotate__ctor_m449/* method */
	, &AutoMoveAndRotate_t171_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 461/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.AutoMoveAndRotate::Start()
extern const MethodInfo AutoMoveAndRotate_Start_m450_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&AutoMoveAndRotate_Start_m450/* method */
	, &AutoMoveAndRotate_t171_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 462/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.AutoMoveAndRotate::Update()
extern const MethodInfo AutoMoveAndRotate_Update_m451_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&AutoMoveAndRotate_Update_m451/* method */
	, &AutoMoveAndRotate_t171_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 463/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AutoMoveAndRotate_t171_MethodInfos[] =
{
	&AutoMoveAndRotate__ctor_m449_MethodInfo,
	&AutoMoveAndRotate_Start_m450_MethodInfo,
	&AutoMoveAndRotate_Update_m451_MethodInfo,
	NULL
};
static const Il2CppType* AutoMoveAndRotate_t171_il2cpp_TypeInfo__nestedTypes[1] =
{
	&Vector3andSpace_t170_0_0_0,
};
static const Il2CppMethodReference AutoMoveAndRotate_t171_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool AutoMoveAndRotate_t171_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType AutoMoveAndRotate_t171_1_0_0;
struct AutoMoveAndRotate_t171;
const Il2CppTypeDefinitionMetadata AutoMoveAndRotate_t171_DefinitionMetadata = 
{
	NULL/* declaringType */
	, AutoMoveAndRotate_t171_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, AutoMoveAndRotate_t171_VTable/* vtableMethods */
	, AutoMoveAndRotate_t171_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 805/* fieldStart */

};
TypeInfo AutoMoveAndRotate_t171_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "AutoMoveAndRotate"/* name */
	, "UnityStandardAssets.Utility"/* namespaze */
	, AutoMoveAndRotate_t171_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &AutoMoveAndRotate_t171_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AutoMoveAndRotate_t171_0_0_0/* byval_arg */
	, &AutoMoveAndRotate_t171_1_0_0/* this_arg */
	, &AutoMoveAndRotate_t171_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AutoMoveAndRotate_t171)/* instance_size */
	, sizeof (AutoMoveAndRotate_t171)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.Utility.CameraRefocus
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Ca.h"
// Metadata Definition UnityStandardAssets.Utility.CameraRefocus
extern TypeInfo CameraRefocus_t172_il2cpp_TypeInfo;
// UnityStandardAssets.Utility.CameraRefocus
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_CaMethodDeclarations.h"
extern const Il2CppType Camera_t27_0_0_0;
extern const Il2CppType Camera_t27_0_0_0;
extern const Il2CppType Transform_t1_0_0_0;
extern const Il2CppType Vector3_t4_0_0_0;
static const ParameterInfo CameraRefocus_t172_CameraRefocus__ctor_m452_ParameterInfos[] = 
{
	{"camera", 0, 134218038, 0, &Camera_t27_0_0_0},
	{"parent", 1, 134218039, 0, &Transform_t1_0_0_0},
	{"origCameraPos", 2, 134218040, 0, &Vector3_t4_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t_Vector3_t4 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.CameraRefocus::.ctor(UnityEngine.Camera,UnityEngine.Transform,UnityEngine.Vector3)
extern const MethodInfo CameraRefocus__ctor_m452_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CameraRefocus__ctor_m452/* method */
	, &CameraRefocus_t172_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t_Vector3_t4/* invoker_method */
	, CameraRefocus_t172_CameraRefocus__ctor_m452_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 465/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Camera_t27_0_0_0;
static const ParameterInfo CameraRefocus_t172_CameraRefocus_ChangeCamera_m453_ParameterInfos[] = 
{
	{"camera", 0, 134218041, 0, &Camera_t27_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.CameraRefocus::ChangeCamera(UnityEngine.Camera)
extern const MethodInfo CameraRefocus_ChangeCamera_m453_MethodInfo = 
{
	"ChangeCamera"/* name */
	, (methodPointerType)&CameraRefocus_ChangeCamera_m453/* method */
	, &CameraRefocus_t172_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, CameraRefocus_t172_CameraRefocus_ChangeCamera_m453_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 466/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Transform_t1_0_0_0;
static const ParameterInfo CameraRefocus_t172_CameraRefocus_ChangeParent_m454_ParameterInfos[] = 
{
	{"parent", 0, 134218042, 0, &Transform_t1_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.CameraRefocus::ChangeParent(UnityEngine.Transform)
extern const MethodInfo CameraRefocus_ChangeParent_m454_MethodInfo = 
{
	"ChangeParent"/* name */
	, (methodPointerType)&CameraRefocus_ChangeParent_m454/* method */
	, &CameraRefocus_t172_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, CameraRefocus_t172_CameraRefocus_ChangeParent_m454_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 467/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.CameraRefocus::GetFocusPoint()
extern const MethodInfo CameraRefocus_GetFocusPoint_m455_MethodInfo = 
{
	"GetFocusPoint"/* name */
	, (methodPointerType)&CameraRefocus_GetFocusPoint_m455/* method */
	, &CameraRefocus_t172_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 468/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.CameraRefocus::SetFocusPoint()
extern const MethodInfo CameraRefocus_SetFocusPoint_m456_MethodInfo = 
{
	"SetFocusPoint"/* name */
	, (methodPointerType)&CameraRefocus_SetFocusPoint_m456/* method */
	, &CameraRefocus_t172_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 469/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CameraRefocus_t172_MethodInfos[] =
{
	&CameraRefocus__ctor_m452_MethodInfo,
	&CameraRefocus_ChangeCamera_m453_MethodInfo,
	&CameraRefocus_ChangeParent_m454_MethodInfo,
	&CameraRefocus_GetFocusPoint_m455_MethodInfo,
	&CameraRefocus_SetFocusPoint_m456_MethodInfo,
	NULL
};
static const Il2CppMethodReference CameraRefocus_t172_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool CameraRefocus_t172_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType CameraRefocus_t172_0_0_0;
extern const Il2CppType CameraRefocus_t172_1_0_0;
struct CameraRefocus_t172;
const Il2CppTypeDefinitionMetadata CameraRefocus_t172_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CameraRefocus_t172_VTable/* vtableMethods */
	, CameraRefocus_t172_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 809/* fieldStart */

};
TypeInfo CameraRefocus_t172_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraRefocus"/* name */
	, "UnityStandardAssets.Utility"/* namespaze */
	, CameraRefocus_t172_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CameraRefocus_t172_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CameraRefocus_t172_0_0_0/* byval_arg */
	, &CameraRefocus_t172_1_0_0/* this_arg */
	, &CameraRefocus_t172_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraRefocus_t172)/* instance_size */
	, sizeof (CameraRefocus_t172)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.Utility.CurveControlledBob
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Cu.h"
// Metadata Definition UnityStandardAssets.Utility.CurveControlledBob
extern TypeInfo CurveControlledBob_t173_il2cpp_TypeInfo;
// UnityStandardAssets.Utility.CurveControlledBob
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_CuMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.CurveControlledBob::.ctor()
extern const MethodInfo CurveControlledBob__ctor_m457_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CurveControlledBob__ctor_m457/* method */
	, &CurveControlledBob_t173_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 470/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Camera_t27_0_0_0;
extern const Il2CppType Single_t254_0_0_0;
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo CurveControlledBob_t173_CurveControlledBob_Setup_m458_ParameterInfos[] = 
{
	{"camera", 0, 134218043, 0, &Camera_t27_0_0_0},
	{"bobBaseInterval", 1, 134218044, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.CurveControlledBob::Setup(UnityEngine.Camera,System.Single)
extern const MethodInfo CurveControlledBob_Setup_m458_MethodInfo = 
{
	"Setup"/* name */
	, (methodPointerType)&CurveControlledBob_Setup_m458/* method */
	, &CurveControlledBob_t173_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Single_t254/* invoker_method */
	, CurveControlledBob_t173_CurveControlledBob_Setup_m458_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 471/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo CurveControlledBob_t173_CurveControlledBob_DoHeadBob_m459_ParameterInfos[] = 
{
	{"speed", 0, 134218045, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Vector3_t4_Single_t254 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector3 UnityStandardAssets.Utility.CurveControlledBob::DoHeadBob(System.Single)
extern const MethodInfo CurveControlledBob_DoHeadBob_m459_MethodInfo = 
{
	"DoHeadBob"/* name */
	, (methodPointerType)&CurveControlledBob_DoHeadBob_m459/* method */
	, &CurveControlledBob_t173_il2cpp_TypeInfo/* declaring_type */
	, &Vector3_t4_0_0_0/* return_type */
	, RuntimeInvoker_Vector3_t4_Single_t254/* invoker_method */
	, CurveControlledBob_t173_CurveControlledBob_DoHeadBob_m459_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 472/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CurveControlledBob_t173_MethodInfos[] =
{
	&CurveControlledBob__ctor_m457_MethodInfo,
	&CurveControlledBob_Setup_m458_MethodInfo,
	&CurveControlledBob_DoHeadBob_m459_MethodInfo,
	NULL
};
static const Il2CppMethodReference CurveControlledBob_t173_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool CurveControlledBob_t173_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType CurveControlledBob_t173_0_0_0;
extern const Il2CppType CurveControlledBob_t173_1_0_0;
struct CurveControlledBob_t173;
const Il2CppTypeDefinitionMetadata CurveControlledBob_t173_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CurveControlledBob_t173_VTable/* vtableMethods */
	, CurveControlledBob_t173_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 814/* fieldStart */

};
TypeInfo CurveControlledBob_t173_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "CurveControlledBob"/* name */
	, "UnityStandardAssets.Utility"/* namespaze */
	, CurveControlledBob_t173_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CurveControlledBob_t173_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CurveControlledBob_t173_0_0_0/* byval_arg */
	, &CurveControlledBob_t173_1_0_0/* this_arg */
	, &CurveControlledBob_t173_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CurveControlledBob_t173)/* instance_size */
	, sizeof (CurveControlledBob_t173)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 9/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator3
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Dr.h"
// Metadata Definition UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator3
extern TypeInfo U3CDragObjectU3Ec__Iterator3_t175_il2cpp_TypeInfo;
// UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator3
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_DrMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator3::.ctor()
extern const MethodInfo U3CDragObjectU3Ec__Iterator3__ctor_m460_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CDragObjectU3Ec__Iterator3__ctor_m460/* method */
	, &U3CDragObjectU3Ec__Iterator3_t175_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 477/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator3::System.Collections.Generic.IEnumerator<object>.get_Current()
extern const MethodInfo U3CDragObjectU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m461_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<object>.get_Current"/* name */
	, (methodPointerType)&U3CDragObjectU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m461/* method */
	, &U3CDragObjectU3Ec__Iterator3_t175_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 142/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 478/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern const MethodInfo U3CDragObjectU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m462_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&U3CDragObjectU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m462/* method */
	, &U3CDragObjectU3Ec__Iterator3_t175_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 143/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 479/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator3::MoveNext()
extern const MethodInfo U3CDragObjectU3Ec__Iterator3_MoveNext_m463_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&U3CDragObjectU3Ec__Iterator3_MoveNext_m463/* method */
	, &U3CDragObjectU3Ec__Iterator3_t175_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 480/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator3::Dispose()
extern const MethodInfo U3CDragObjectU3Ec__Iterator3_Dispose_m464_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&U3CDragObjectU3Ec__Iterator3_Dispose_m464/* method */
	, &U3CDragObjectU3Ec__Iterator3_t175_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 144/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 481/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator3::Reset()
extern const MethodInfo U3CDragObjectU3Ec__Iterator3_Reset_m465_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&U3CDragObjectU3Ec__Iterator3_Reset_m465/* method */
	, &U3CDragObjectU3Ec__Iterator3_t175_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 145/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 482/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CDragObjectU3Ec__Iterator3_t175_MethodInfos[] =
{
	&U3CDragObjectU3Ec__Iterator3__ctor_m460_MethodInfo,
	&U3CDragObjectU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m461_MethodInfo,
	&U3CDragObjectU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m462_MethodInfo,
	&U3CDragObjectU3Ec__Iterator3_MoveNext_m463_MethodInfo,
	&U3CDragObjectU3Ec__Iterator3_Dispose_m464_MethodInfo,
	&U3CDragObjectU3Ec__Iterator3_Reset_m465_MethodInfo,
	NULL
};
extern const MethodInfo U3CDragObjectU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m461_MethodInfo;
static const PropertyInfo U3CDragObjectU3Ec__Iterator3_t175____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo = 
{
	&U3CDragObjectU3Ec__Iterator3_t175_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<object>.Current"/* name */
	, &U3CDragObjectU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m461_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo U3CDragObjectU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m462_MethodInfo;
static const PropertyInfo U3CDragObjectU3Ec__Iterator3_t175____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CDragObjectU3Ec__Iterator3_t175_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CDragObjectU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m462_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* U3CDragObjectU3Ec__Iterator3_t175_PropertyInfos[] =
{
	&U3CDragObjectU3Ec__Iterator3_t175____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo,
	&U3CDragObjectU3Ec__Iterator3_t175____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern const MethodInfo U3CDragObjectU3Ec__Iterator3_MoveNext_m463_MethodInfo;
extern const MethodInfo U3CDragObjectU3Ec__Iterator3_Dispose_m464_MethodInfo;
extern const MethodInfo U3CDragObjectU3Ec__Iterator3_Reset_m465_MethodInfo;
static const Il2CppMethodReference U3CDragObjectU3Ec__Iterator3_t175_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&U3CDragObjectU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m461_MethodInfo,
	&U3CDragObjectU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m462_MethodInfo,
	&U3CDragObjectU3Ec__Iterator3_MoveNext_m463_MethodInfo,
	&U3CDragObjectU3Ec__Iterator3_Dispose_m464_MethodInfo,
	&U3CDragObjectU3Ec__Iterator3_Reset_m465_MethodInfo,
};
static bool U3CDragObjectU3Ec__Iterator3_t175_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* U3CDragObjectU3Ec__Iterator3_t175_InterfacesTypeInfos[] = 
{
	&IEnumerator_1_t284_0_0_0,
	&IEnumerator_t217_0_0_0,
	&IDisposable_t233_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CDragObjectU3Ec__Iterator3_t175_InterfacesOffsets[] = 
{
	{ &IEnumerator_1_t284_0_0_0, 4},
	{ &IEnumerator_t217_0_0_0, 5},
	{ &IDisposable_t233_0_0_0, 7},
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType U3CDragObjectU3Ec__Iterator3_t175_0_0_0;
extern const Il2CppType U3CDragObjectU3Ec__Iterator3_t175_1_0_0;
extern TypeInfo DragRigidbody_t174_il2cpp_TypeInfo;
extern const Il2CppType DragRigidbody_t174_0_0_0;
struct U3CDragObjectU3Ec__Iterator3_t175;
const Il2CppTypeDefinitionMetadata U3CDragObjectU3Ec__Iterator3_t175_DefinitionMetadata = 
{
	&DragRigidbody_t174_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CDragObjectU3Ec__Iterator3_t175_InterfacesTypeInfos/* implementedInterfaces */
	, U3CDragObjectU3Ec__Iterator3_t175_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CDragObjectU3Ec__Iterator3_t175_VTable/* vtableMethods */
	, U3CDragObjectU3Ec__Iterator3_t175_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 823/* fieldStart */

};
TypeInfo U3CDragObjectU3Ec__Iterator3_t175_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "<DragObject>c__Iterator3"/* name */
	, ""/* namespaze */
	, U3CDragObjectU3Ec__Iterator3_t175_MethodInfos/* methods */
	, U3CDragObjectU3Ec__Iterator3_t175_PropertyInfos/* properties */
	, NULL/* events */
	, &U3CDragObjectU3Ec__Iterator3_t175_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 141/* custom_attributes_cache */
	, &U3CDragObjectU3Ec__Iterator3_t175_0_0_0/* byval_arg */
	, &U3CDragObjectU3Ec__Iterator3_t175_1_0_0/* this_arg */
	, &U3CDragObjectU3Ec__Iterator3_t175_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CDragObjectU3Ec__Iterator3_t175)/* instance_size */
	, sizeof (U3CDragObjectU3Ec__Iterator3_t175)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 9/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityStandardAssets.Utility.DragRigidbody
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Dr_0.h"
// Metadata Definition UnityStandardAssets.Utility.DragRigidbody
// UnityStandardAssets.Utility.DragRigidbody
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Dr_0MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.DragRigidbody::.ctor()
extern const MethodInfo DragRigidbody__ctor_m466_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DragRigidbody__ctor_m466/* method */
	, &DragRigidbody_t174_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 473/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.DragRigidbody::Update()
extern const MethodInfo DragRigidbody_Update_m467_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&DragRigidbody_Update_m467/* method */
	, &DragRigidbody_t174_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 474/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo DragRigidbody_t174_DragRigidbody_DragObject_m468_ParameterInfos[] = 
{
	{"distance", 0, 134218046, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Collections.IEnumerator UnityStandardAssets.Utility.DragRigidbody::DragObject(System.Single)
extern const MethodInfo DragRigidbody_DragObject_m468_MethodInfo = 
{
	"DragObject"/* name */
	, (methodPointerType)&DragRigidbody_DragObject_m468/* method */
	, &DragRigidbody_t174_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t217_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Single_t254/* invoker_method */
	, DragRigidbody_t174_DragRigidbody_DragObject_m468_ParameterInfos/* parameters */
	, 140/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 475/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Camera UnityStandardAssets.Utility.DragRigidbody::FindCamera()
extern const MethodInfo DragRigidbody_FindCamera_m469_MethodInfo = 
{
	"FindCamera"/* name */
	, (methodPointerType)&DragRigidbody_FindCamera_m469/* method */
	, &DragRigidbody_t174_il2cpp_TypeInfo/* declaring_type */
	, &Camera_t27_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 476/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* DragRigidbody_t174_MethodInfos[] =
{
	&DragRigidbody__ctor_m466_MethodInfo,
	&DragRigidbody_Update_m467_MethodInfo,
	&DragRigidbody_DragObject_m468_MethodInfo,
	&DragRigidbody_FindCamera_m469_MethodInfo,
	NULL
};
static const Il2CppType* DragRigidbody_t174_il2cpp_TypeInfo__nestedTypes[1] =
{
	&U3CDragObjectU3Ec__Iterator3_t175_0_0_0,
};
static const Il2CppMethodReference DragRigidbody_t174_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool DragRigidbody_t174_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType DragRigidbody_t174_1_0_0;
struct DragRigidbody_t174;
const Il2CppTypeDefinitionMetadata DragRigidbody_t174_DefinitionMetadata = 
{
	NULL/* declaringType */
	, DragRigidbody_t174_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, DragRigidbody_t174_VTable/* vtableMethods */
	, DragRigidbody_t174_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 832/* fieldStart */

};
TypeInfo DragRigidbody_t174_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "DragRigidbody"/* name */
	, "UnityStandardAssets.Utility"/* namespaze */
	, DragRigidbody_t174_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &DragRigidbody_t174_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DragRigidbody_t174_0_0_0/* byval_arg */
	, &DragRigidbody_t174_1_0_0/* this_arg */
	, &DragRigidbody_t174_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DragRigidbody_t174)/* instance_size */
	, sizeof (DragRigidbody_t174)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.Utility.DynamicShadowSettings
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Dy.h"
// Metadata Definition UnityStandardAssets.Utility.DynamicShadowSettings
extern TypeInfo DynamicShadowSettings_t177_il2cpp_TypeInfo;
// UnityStandardAssets.Utility.DynamicShadowSettings
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_DyMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.DynamicShadowSettings::.ctor()
extern const MethodInfo DynamicShadowSettings__ctor_m470_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DynamicShadowSettings__ctor_m470/* method */
	, &DynamicShadowSettings_t177_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 483/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.DynamicShadowSettings::Start()
extern const MethodInfo DynamicShadowSettings_Start_m471_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&DynamicShadowSettings_Start_m471/* method */
	, &DynamicShadowSettings_t177_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 484/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.DynamicShadowSettings::Update()
extern const MethodInfo DynamicShadowSettings_Update_m472_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&DynamicShadowSettings_Update_m472/* method */
	, &DynamicShadowSettings_t177_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 485/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* DynamicShadowSettings_t177_MethodInfos[] =
{
	&DynamicShadowSettings__ctor_m470_MethodInfo,
	&DynamicShadowSettings_Start_m471_MethodInfo,
	&DynamicShadowSettings_Update_m472_MethodInfo,
	NULL
};
static const Il2CppMethodReference DynamicShadowSettings_t177_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool DynamicShadowSettings_t177_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType DynamicShadowSettings_t177_0_0_0;
extern const Il2CppType DynamicShadowSettings_t177_1_0_0;
struct DynamicShadowSettings_t177;
const Il2CppTypeDefinitionMetadata DynamicShadowSettings_t177_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, DynamicShadowSettings_t177_VTable/* vtableMethods */
	, DynamicShadowSettings_t177_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 839/* fieldStart */

};
TypeInfo DynamicShadowSettings_t177_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "DynamicShadowSettings"/* name */
	, "UnityStandardAssets.Utility"/* namespaze */
	, DynamicShadowSettings_t177_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &DynamicShadowSettings_t177_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DynamicShadowSettings_t177_0_0_0/* byval_arg */
	, &DynamicShadowSettings_t177_1_0_0/* this_arg */
	, &DynamicShadowSettings_t177_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DynamicShadowSettings_t177)/* instance_size */
	, sizeof (DynamicShadowSettings_t177)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.Utility.FOVKick/<FOVKickUp>c__Iterator4
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_FO.h"
// Metadata Definition UnityStandardAssets.Utility.FOVKick/<FOVKickUp>c__Iterator4
extern TypeInfo U3CFOVKickUpU3Ec__Iterator4_t179_il2cpp_TypeInfo;
// UnityStandardAssets.Utility.FOVKick/<FOVKickUp>c__Iterator4
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_FOMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.FOVKick/<FOVKickUp>c__Iterator4::.ctor()
extern const MethodInfo U3CFOVKickUpU3Ec__Iterator4__ctor_m473_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CFOVKickUpU3Ec__Iterator4__ctor_m473/* method */
	, &U3CFOVKickUpU3Ec__Iterator4_t179_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 492/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object UnityStandardAssets.Utility.FOVKick/<FOVKickUp>c__Iterator4::System.Collections.Generic.IEnumerator<object>.get_Current()
extern const MethodInfo U3CFOVKickUpU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m474_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<object>.get_Current"/* name */
	, (methodPointerType)&U3CFOVKickUpU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m474/* method */
	, &U3CFOVKickUpU3Ec__Iterator4_t179_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 150/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 493/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object UnityStandardAssets.Utility.FOVKick/<FOVKickUp>c__Iterator4::System.Collections.IEnumerator.get_Current()
extern const MethodInfo U3CFOVKickUpU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m475_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&U3CFOVKickUpU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m475/* method */
	, &U3CFOVKickUpU3Ec__Iterator4_t179_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 151/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 494/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityStandardAssets.Utility.FOVKick/<FOVKickUp>c__Iterator4::MoveNext()
extern const MethodInfo U3CFOVKickUpU3Ec__Iterator4_MoveNext_m476_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&U3CFOVKickUpU3Ec__Iterator4_MoveNext_m476/* method */
	, &U3CFOVKickUpU3Ec__Iterator4_t179_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 495/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.FOVKick/<FOVKickUp>c__Iterator4::Dispose()
extern const MethodInfo U3CFOVKickUpU3Ec__Iterator4_Dispose_m477_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&U3CFOVKickUpU3Ec__Iterator4_Dispose_m477/* method */
	, &U3CFOVKickUpU3Ec__Iterator4_t179_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 152/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 496/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.FOVKick/<FOVKickUp>c__Iterator4::Reset()
extern const MethodInfo U3CFOVKickUpU3Ec__Iterator4_Reset_m478_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&U3CFOVKickUpU3Ec__Iterator4_Reset_m478/* method */
	, &U3CFOVKickUpU3Ec__Iterator4_t179_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 153/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 497/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CFOVKickUpU3Ec__Iterator4_t179_MethodInfos[] =
{
	&U3CFOVKickUpU3Ec__Iterator4__ctor_m473_MethodInfo,
	&U3CFOVKickUpU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m474_MethodInfo,
	&U3CFOVKickUpU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m475_MethodInfo,
	&U3CFOVKickUpU3Ec__Iterator4_MoveNext_m476_MethodInfo,
	&U3CFOVKickUpU3Ec__Iterator4_Dispose_m477_MethodInfo,
	&U3CFOVKickUpU3Ec__Iterator4_Reset_m478_MethodInfo,
	NULL
};
extern const MethodInfo U3CFOVKickUpU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m474_MethodInfo;
static const PropertyInfo U3CFOVKickUpU3Ec__Iterator4_t179____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo = 
{
	&U3CFOVKickUpU3Ec__Iterator4_t179_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<object>.Current"/* name */
	, &U3CFOVKickUpU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m474_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo U3CFOVKickUpU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m475_MethodInfo;
static const PropertyInfo U3CFOVKickUpU3Ec__Iterator4_t179____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CFOVKickUpU3Ec__Iterator4_t179_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CFOVKickUpU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m475_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* U3CFOVKickUpU3Ec__Iterator4_t179_PropertyInfos[] =
{
	&U3CFOVKickUpU3Ec__Iterator4_t179____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo,
	&U3CFOVKickUpU3Ec__Iterator4_t179____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern const MethodInfo U3CFOVKickUpU3Ec__Iterator4_MoveNext_m476_MethodInfo;
extern const MethodInfo U3CFOVKickUpU3Ec__Iterator4_Dispose_m477_MethodInfo;
extern const MethodInfo U3CFOVKickUpU3Ec__Iterator4_Reset_m478_MethodInfo;
static const Il2CppMethodReference U3CFOVKickUpU3Ec__Iterator4_t179_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&U3CFOVKickUpU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m474_MethodInfo,
	&U3CFOVKickUpU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m475_MethodInfo,
	&U3CFOVKickUpU3Ec__Iterator4_MoveNext_m476_MethodInfo,
	&U3CFOVKickUpU3Ec__Iterator4_Dispose_m477_MethodInfo,
	&U3CFOVKickUpU3Ec__Iterator4_Reset_m478_MethodInfo,
};
static bool U3CFOVKickUpU3Ec__Iterator4_t179_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* U3CFOVKickUpU3Ec__Iterator4_t179_InterfacesTypeInfos[] = 
{
	&IEnumerator_1_t284_0_0_0,
	&IEnumerator_t217_0_0_0,
	&IDisposable_t233_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CFOVKickUpU3Ec__Iterator4_t179_InterfacesOffsets[] = 
{
	{ &IEnumerator_1_t284_0_0_0, 4},
	{ &IEnumerator_t217_0_0_0, 5},
	{ &IDisposable_t233_0_0_0, 7},
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType U3CFOVKickUpU3Ec__Iterator4_t179_0_0_0;
extern const Il2CppType U3CFOVKickUpU3Ec__Iterator4_t179_1_0_0;
extern TypeInfo FOVKick_t178_il2cpp_TypeInfo;
extern const Il2CppType FOVKick_t178_0_0_0;
struct U3CFOVKickUpU3Ec__Iterator4_t179;
const Il2CppTypeDefinitionMetadata U3CFOVKickUpU3Ec__Iterator4_t179_DefinitionMetadata = 
{
	&FOVKick_t178_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CFOVKickUpU3Ec__Iterator4_t179_InterfacesTypeInfos/* implementedInterfaces */
	, U3CFOVKickUpU3Ec__Iterator4_t179_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CFOVKickUpU3Ec__Iterator4_t179_VTable/* vtableMethods */
	, U3CFOVKickUpU3Ec__Iterator4_t179_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 850/* fieldStart */

};
TypeInfo U3CFOVKickUpU3Ec__Iterator4_t179_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "<FOVKickUp>c__Iterator4"/* name */
	, ""/* namespaze */
	, U3CFOVKickUpU3Ec__Iterator4_t179_MethodInfos/* methods */
	, U3CFOVKickUpU3Ec__Iterator4_t179_PropertyInfos/* properties */
	, NULL/* events */
	, &U3CFOVKickUpU3Ec__Iterator4_t179_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 149/* custom_attributes_cache */
	, &U3CFOVKickUpU3Ec__Iterator4_t179_0_0_0/* byval_arg */
	, &U3CFOVKickUpU3Ec__Iterator4_t179_1_0_0/* this_arg */
	, &U3CFOVKickUpU3Ec__Iterator4_t179_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CFOVKickUpU3Ec__Iterator4_t179)/* instance_size */
	, sizeof (U3CFOVKickUpU3Ec__Iterator4_t179)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityStandardAssets.Utility.FOVKick/<FOVKickDown>c__Iterator5
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_FO_0.h"
// Metadata Definition UnityStandardAssets.Utility.FOVKick/<FOVKickDown>c__Iterator5
extern TypeInfo U3CFOVKickDownU3Ec__Iterator5_t180_il2cpp_TypeInfo;
// UnityStandardAssets.Utility.FOVKick/<FOVKickDown>c__Iterator5
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_FO_0MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.FOVKick/<FOVKickDown>c__Iterator5::.ctor()
extern const MethodInfo U3CFOVKickDownU3Ec__Iterator5__ctor_m479_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CFOVKickDownU3Ec__Iterator5__ctor_m479/* method */
	, &U3CFOVKickDownU3Ec__Iterator5_t180_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 498/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object UnityStandardAssets.Utility.FOVKick/<FOVKickDown>c__Iterator5::System.Collections.Generic.IEnumerator<object>.get_Current()
extern const MethodInfo U3CFOVKickDownU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m480_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<object>.get_Current"/* name */
	, (methodPointerType)&U3CFOVKickDownU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m480/* method */
	, &U3CFOVKickDownU3Ec__Iterator5_t180_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 155/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 499/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object UnityStandardAssets.Utility.FOVKick/<FOVKickDown>c__Iterator5::System.Collections.IEnumerator.get_Current()
extern const MethodInfo U3CFOVKickDownU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m481_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&U3CFOVKickDownU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m481/* method */
	, &U3CFOVKickDownU3Ec__Iterator5_t180_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 156/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 500/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityStandardAssets.Utility.FOVKick/<FOVKickDown>c__Iterator5::MoveNext()
extern const MethodInfo U3CFOVKickDownU3Ec__Iterator5_MoveNext_m482_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&U3CFOVKickDownU3Ec__Iterator5_MoveNext_m482/* method */
	, &U3CFOVKickDownU3Ec__Iterator5_t180_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 501/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.FOVKick/<FOVKickDown>c__Iterator5::Dispose()
extern const MethodInfo U3CFOVKickDownU3Ec__Iterator5_Dispose_m483_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&U3CFOVKickDownU3Ec__Iterator5_Dispose_m483/* method */
	, &U3CFOVKickDownU3Ec__Iterator5_t180_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 157/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 502/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.FOVKick/<FOVKickDown>c__Iterator5::Reset()
extern const MethodInfo U3CFOVKickDownU3Ec__Iterator5_Reset_m484_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&U3CFOVKickDownU3Ec__Iterator5_Reset_m484/* method */
	, &U3CFOVKickDownU3Ec__Iterator5_t180_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 158/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 503/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CFOVKickDownU3Ec__Iterator5_t180_MethodInfos[] =
{
	&U3CFOVKickDownU3Ec__Iterator5__ctor_m479_MethodInfo,
	&U3CFOVKickDownU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m480_MethodInfo,
	&U3CFOVKickDownU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m481_MethodInfo,
	&U3CFOVKickDownU3Ec__Iterator5_MoveNext_m482_MethodInfo,
	&U3CFOVKickDownU3Ec__Iterator5_Dispose_m483_MethodInfo,
	&U3CFOVKickDownU3Ec__Iterator5_Reset_m484_MethodInfo,
	NULL
};
extern const MethodInfo U3CFOVKickDownU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m480_MethodInfo;
static const PropertyInfo U3CFOVKickDownU3Ec__Iterator5_t180____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo = 
{
	&U3CFOVKickDownU3Ec__Iterator5_t180_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<object>.Current"/* name */
	, &U3CFOVKickDownU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m480_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo U3CFOVKickDownU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m481_MethodInfo;
static const PropertyInfo U3CFOVKickDownU3Ec__Iterator5_t180____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CFOVKickDownU3Ec__Iterator5_t180_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CFOVKickDownU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m481_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* U3CFOVKickDownU3Ec__Iterator5_t180_PropertyInfos[] =
{
	&U3CFOVKickDownU3Ec__Iterator5_t180____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo,
	&U3CFOVKickDownU3Ec__Iterator5_t180____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern const MethodInfo U3CFOVKickDownU3Ec__Iterator5_MoveNext_m482_MethodInfo;
extern const MethodInfo U3CFOVKickDownU3Ec__Iterator5_Dispose_m483_MethodInfo;
extern const MethodInfo U3CFOVKickDownU3Ec__Iterator5_Reset_m484_MethodInfo;
static const Il2CppMethodReference U3CFOVKickDownU3Ec__Iterator5_t180_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&U3CFOVKickDownU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m480_MethodInfo,
	&U3CFOVKickDownU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m481_MethodInfo,
	&U3CFOVKickDownU3Ec__Iterator5_MoveNext_m482_MethodInfo,
	&U3CFOVKickDownU3Ec__Iterator5_Dispose_m483_MethodInfo,
	&U3CFOVKickDownU3Ec__Iterator5_Reset_m484_MethodInfo,
};
static bool U3CFOVKickDownU3Ec__Iterator5_t180_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* U3CFOVKickDownU3Ec__Iterator5_t180_InterfacesTypeInfos[] = 
{
	&IEnumerator_1_t284_0_0_0,
	&IEnumerator_t217_0_0_0,
	&IDisposable_t233_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CFOVKickDownU3Ec__Iterator5_t180_InterfacesOffsets[] = 
{
	{ &IEnumerator_1_t284_0_0_0, 4},
	{ &IEnumerator_t217_0_0_0, 5},
	{ &IDisposable_t233_0_0_0, 7},
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType U3CFOVKickDownU3Ec__Iterator5_t180_0_0_0;
extern const Il2CppType U3CFOVKickDownU3Ec__Iterator5_t180_1_0_0;
struct U3CFOVKickDownU3Ec__Iterator5_t180;
const Il2CppTypeDefinitionMetadata U3CFOVKickDownU3Ec__Iterator5_t180_DefinitionMetadata = 
{
	&FOVKick_t178_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CFOVKickDownU3Ec__Iterator5_t180_InterfacesTypeInfos/* implementedInterfaces */
	, U3CFOVKickDownU3Ec__Iterator5_t180_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CFOVKickDownU3Ec__Iterator5_t180_VTable/* vtableMethods */
	, U3CFOVKickDownU3Ec__Iterator5_t180_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 854/* fieldStart */

};
TypeInfo U3CFOVKickDownU3Ec__Iterator5_t180_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "<FOVKickDown>c__Iterator5"/* name */
	, ""/* namespaze */
	, U3CFOVKickDownU3Ec__Iterator5_t180_MethodInfos/* methods */
	, U3CFOVKickDownU3Ec__Iterator5_t180_PropertyInfos/* properties */
	, NULL/* events */
	, &U3CFOVKickDownU3Ec__Iterator5_t180_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 154/* custom_attributes_cache */
	, &U3CFOVKickDownU3Ec__Iterator5_t180_0_0_0/* byval_arg */
	, &U3CFOVKickDownU3Ec__Iterator5_t180_1_0_0/* this_arg */
	, &U3CFOVKickDownU3Ec__Iterator5_t180_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CFOVKickDownU3Ec__Iterator5_t180)/* instance_size */
	, sizeof (U3CFOVKickDownU3Ec__Iterator5_t180)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityStandardAssets.Utility.FOVKick
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_FO_1.h"
// Metadata Definition UnityStandardAssets.Utility.FOVKick
// UnityStandardAssets.Utility.FOVKick
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_FO_1MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.FOVKick::.ctor()
extern const MethodInfo FOVKick__ctor_m485_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&FOVKick__ctor_m485/* method */
	, &FOVKick_t178_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 486/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Camera_t27_0_0_0;
static const ParameterInfo FOVKick_t178_FOVKick_Setup_m486_ParameterInfos[] = 
{
	{"camera", 0, 134218047, 0, &Camera_t27_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.FOVKick::Setup(UnityEngine.Camera)
extern const MethodInfo FOVKick_Setup_m486_MethodInfo = 
{
	"Setup"/* name */
	, (methodPointerType)&FOVKick_Setup_m486/* method */
	, &FOVKick_t178_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, FOVKick_t178_FOVKick_Setup_m486_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 487/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Camera_t27_0_0_0;
static const ParameterInfo FOVKick_t178_FOVKick_CheckStatus_m487_ParameterInfos[] = 
{
	{"camera", 0, 134218048, 0, &Camera_t27_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.FOVKick::CheckStatus(UnityEngine.Camera)
extern const MethodInfo FOVKick_CheckStatus_m487_MethodInfo = 
{
	"CheckStatus"/* name */
	, (methodPointerType)&FOVKick_CheckStatus_m487/* method */
	, &FOVKick_t178_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, FOVKick_t178_FOVKick_CheckStatus_m487_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 488/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Camera_t27_0_0_0;
static const ParameterInfo FOVKick_t178_FOVKick_ChangeCamera_m488_ParameterInfos[] = 
{
	{"camera", 0, 134218049, 0, &Camera_t27_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.FOVKick::ChangeCamera(UnityEngine.Camera)
extern const MethodInfo FOVKick_ChangeCamera_m488_MethodInfo = 
{
	"ChangeCamera"/* name */
	, (methodPointerType)&FOVKick_ChangeCamera_m488/* method */
	, &FOVKick_t178_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, FOVKick_t178_FOVKick_ChangeCamera_m488_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 489/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.IEnumerator UnityStandardAssets.Utility.FOVKick::FOVKickUp()
extern const MethodInfo FOVKick_FOVKickUp_m489_MethodInfo = 
{
	"FOVKickUp"/* name */
	, (methodPointerType)&FOVKick_FOVKickUp_m489/* method */
	, &FOVKick_t178_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t217_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 147/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 490/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.IEnumerator UnityStandardAssets.Utility.FOVKick::FOVKickDown()
extern const MethodInfo FOVKick_FOVKickDown_m490_MethodInfo = 
{
	"FOVKickDown"/* name */
	, (methodPointerType)&FOVKick_FOVKickDown_m490/* method */
	, &FOVKick_t178_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t217_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 148/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 491/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* FOVKick_t178_MethodInfos[] =
{
	&FOVKick__ctor_m485_MethodInfo,
	&FOVKick_Setup_m486_MethodInfo,
	&FOVKick_CheckStatus_m487_MethodInfo,
	&FOVKick_ChangeCamera_m488_MethodInfo,
	&FOVKick_FOVKickUp_m489_MethodInfo,
	&FOVKick_FOVKickDown_m490_MethodInfo,
	NULL
};
static const Il2CppType* FOVKick_t178_il2cpp_TypeInfo__nestedTypes[2] =
{
	&U3CFOVKickUpU3Ec__Iterator4_t179_0_0_0,
	&U3CFOVKickDownU3Ec__Iterator5_t180_0_0_0,
};
static const Il2CppMethodReference FOVKick_t178_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool FOVKick_t178_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType FOVKick_t178_1_0_0;
struct FOVKick_t178;
const Il2CppTypeDefinitionMetadata FOVKick_t178_DefinitionMetadata = 
{
	NULL/* declaringType */
	, FOVKick_t178_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, FOVKick_t178_VTable/* vtableMethods */
	, FOVKick_t178_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 858/* fieldStart */

};
TypeInfo FOVKick_t178_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "FOVKick"/* name */
	, "UnityStandardAssets.Utility"/* namespaze */
	, FOVKick_t178_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &FOVKick_t178_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &FOVKick_t178_0_0_0/* byval_arg */
	, &FOVKick_t178_1_0_0/* this_arg */
	, &FOVKick_t178_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FOVKick_t178)/* instance_size */
	, sizeof (FOVKick_t178)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.Utility.FPSCounter
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_FP.h"
// Metadata Definition UnityStandardAssets.Utility.FPSCounter
extern TypeInfo FPSCounter_t182_il2cpp_TypeInfo;
// UnityStandardAssets.Utility.FPSCounter
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_FPMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.FPSCounter::.ctor()
extern const MethodInfo FPSCounter__ctor_m491_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&FPSCounter__ctor_m491/* method */
	, &FPSCounter_t182_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 504/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.FPSCounter::Start()
extern const MethodInfo FPSCounter_Start_m492_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&FPSCounter_Start_m492/* method */
	, &FPSCounter_t182_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 505/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.FPSCounter::Update()
extern const MethodInfo FPSCounter_Update_m493_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&FPSCounter_Update_m493/* method */
	, &FPSCounter_t182_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 506/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* FPSCounter_t182_MethodInfos[] =
{
	&FPSCounter__ctor_m491_MethodInfo,
	&FPSCounter_Start_m492_MethodInfo,
	&FPSCounter_Update_m493_MethodInfo,
	NULL
};
static const Il2CppMethodReference FPSCounter_t182_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool FPSCounter_t182_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType FPSCounter_t182_0_0_0;
extern const Il2CppType FPSCounter_t182_1_0_0;
struct FPSCounter_t182;
const Il2CppTypeDefinitionMetadata FPSCounter_t182_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, FPSCounter_t182_VTable/* vtableMethods */
	, FPSCounter_t182_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 864/* fieldStart */

};
TypeInfo FPSCounter_t182_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "FPSCounter"/* name */
	, "UnityStandardAssets.Utility"/* namespaze */
	, FPSCounter_t182_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &FPSCounter_t182_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 159/* custom_attributes_cache */
	, &FPSCounter_t182_0_0_0/* byval_arg */
	, &FPSCounter_t182_1_0_0/* this_arg */
	, &FPSCounter_t182_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FPSCounter_t182)/* instance_size */
	, sizeof (FPSCounter_t182)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.Utility.FollowTarget
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Fo_2.h"
// Metadata Definition UnityStandardAssets.Utility.FollowTarget
extern TypeInfo FollowTarget_t183_il2cpp_TypeInfo;
// UnityStandardAssets.Utility.FollowTarget
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Fo_2MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.FollowTarget::.ctor()
extern const MethodInfo FollowTarget__ctor_m494_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&FollowTarget__ctor_m494/* method */
	, &FollowTarget_t183_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 507/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.FollowTarget::LateUpdate()
extern const MethodInfo FollowTarget_LateUpdate_m495_MethodInfo = 
{
	"LateUpdate"/* name */
	, (methodPointerType)&FollowTarget_LateUpdate_m495/* method */
	, &FollowTarget_t183_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 508/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* FollowTarget_t183_MethodInfos[] =
{
	&FollowTarget__ctor_m494_MethodInfo,
	&FollowTarget_LateUpdate_m495_MethodInfo,
	NULL
};
static const Il2CppMethodReference FollowTarget_t183_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool FollowTarget_t183_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType FollowTarget_t183_0_0_0;
extern const Il2CppType FollowTarget_t183_1_0_0;
struct FollowTarget_t183;
const Il2CppTypeDefinitionMetadata FollowTarget_t183_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, FollowTarget_t183_VTable/* vtableMethods */
	, FollowTarget_t183_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 870/* fieldStart */

};
TypeInfo FollowTarget_t183_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "FollowTarget"/* name */
	, "UnityStandardAssets.Utility"/* namespaze */
	, FollowTarget_t183_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &FollowTarget_t183_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &FollowTarget_t183_0_0_0/* byval_arg */
	, &FollowTarget_t183_1_0_0/* this_arg */
	, &FollowTarget_t183_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FollowTarget_t183)/* instance_size */
	, sizeof (FollowTarget_t183)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// ForcedReset
#include "AssemblyU2DCSharpU2Dfirstpass_ForcedReset.h"
// Metadata Definition ForcedReset
extern TypeInfo ForcedReset_t184_il2cpp_TypeInfo;
// ForcedReset
#include "AssemblyU2DCSharpU2Dfirstpass_ForcedResetMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void ForcedReset::.ctor()
extern const MethodInfo ForcedReset__ctor_m496_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ForcedReset__ctor_m496/* method */
	, &ForcedReset_t184_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 509/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void ForcedReset::Update()
extern const MethodInfo ForcedReset_Update_m497_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&ForcedReset_Update_m497/* method */
	, &ForcedReset_t184_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 510/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ForcedReset_t184_MethodInfos[] =
{
	&ForcedReset__ctor_m496_MethodInfo,
	&ForcedReset_Update_m497_MethodInfo,
	NULL
};
static const Il2CppMethodReference ForcedReset_t184_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool ForcedReset_t184_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType ForcedReset_t184_0_0_0;
extern const Il2CppType ForcedReset_t184_1_0_0;
struct ForcedReset_t184;
const Il2CppTypeDefinitionMetadata ForcedReset_t184_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, ForcedReset_t184_VTable/* vtableMethods */
	, ForcedReset_t184_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ForcedReset_t184_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "ForcedReset"/* name */
	, ""/* namespaze */
	, ForcedReset_t184_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ForcedReset_t184_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 160/* custom_attributes_cache */
	, &ForcedReset_t184_0_0_0/* byval_arg */
	, &ForcedReset_t184_1_0_0/* this_arg */
	, &ForcedReset_t184_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ForcedReset_t184)/* instance_size */
	, sizeof (ForcedReset_t184)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>c__Iterator6
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Le.h"
// Metadata Definition UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>c__Iterator6
extern TypeInfo U3CDoBobCycleU3Ec__Iterator6_t186_il2cpp_TypeInfo;
// UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>c__Iterator6
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_LeMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>c__Iterator6::.ctor()
extern const MethodInfo U3CDoBobCycleU3Ec__Iterator6__ctor_m498_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CDoBobCycleU3Ec__Iterator6__ctor_m498/* method */
	, &U3CDoBobCycleU3Ec__Iterator6_t186_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 514/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>c__Iterator6::System.Collections.Generic.IEnumerator<object>.get_Current()
extern const MethodInfo U3CDoBobCycleU3Ec__Iterator6_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m499_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<object>.get_Current"/* name */
	, (methodPointerType)&U3CDoBobCycleU3Ec__Iterator6_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m499/* method */
	, &U3CDoBobCycleU3Ec__Iterator6_t186_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 163/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 515/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>c__Iterator6::System.Collections.IEnumerator.get_Current()
extern const MethodInfo U3CDoBobCycleU3Ec__Iterator6_System_Collections_IEnumerator_get_Current_m500_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&U3CDoBobCycleU3Ec__Iterator6_System_Collections_IEnumerator_get_Current_m500/* method */
	, &U3CDoBobCycleU3Ec__Iterator6_t186_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 164/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 516/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>c__Iterator6::MoveNext()
extern const MethodInfo U3CDoBobCycleU3Ec__Iterator6_MoveNext_m501_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&U3CDoBobCycleU3Ec__Iterator6_MoveNext_m501/* method */
	, &U3CDoBobCycleU3Ec__Iterator6_t186_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 517/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>c__Iterator6::Dispose()
extern const MethodInfo U3CDoBobCycleU3Ec__Iterator6_Dispose_m502_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&U3CDoBobCycleU3Ec__Iterator6_Dispose_m502/* method */
	, &U3CDoBobCycleU3Ec__Iterator6_t186_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 165/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 518/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>c__Iterator6::Reset()
extern const MethodInfo U3CDoBobCycleU3Ec__Iterator6_Reset_m503_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&U3CDoBobCycleU3Ec__Iterator6_Reset_m503/* method */
	, &U3CDoBobCycleU3Ec__Iterator6_t186_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 166/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 519/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CDoBobCycleU3Ec__Iterator6_t186_MethodInfos[] =
{
	&U3CDoBobCycleU3Ec__Iterator6__ctor_m498_MethodInfo,
	&U3CDoBobCycleU3Ec__Iterator6_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m499_MethodInfo,
	&U3CDoBobCycleU3Ec__Iterator6_System_Collections_IEnumerator_get_Current_m500_MethodInfo,
	&U3CDoBobCycleU3Ec__Iterator6_MoveNext_m501_MethodInfo,
	&U3CDoBobCycleU3Ec__Iterator6_Dispose_m502_MethodInfo,
	&U3CDoBobCycleU3Ec__Iterator6_Reset_m503_MethodInfo,
	NULL
};
extern const MethodInfo U3CDoBobCycleU3Ec__Iterator6_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m499_MethodInfo;
static const PropertyInfo U3CDoBobCycleU3Ec__Iterator6_t186____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo = 
{
	&U3CDoBobCycleU3Ec__Iterator6_t186_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<object>.Current"/* name */
	, &U3CDoBobCycleU3Ec__Iterator6_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m499_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo U3CDoBobCycleU3Ec__Iterator6_System_Collections_IEnumerator_get_Current_m500_MethodInfo;
static const PropertyInfo U3CDoBobCycleU3Ec__Iterator6_t186____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CDoBobCycleU3Ec__Iterator6_t186_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CDoBobCycleU3Ec__Iterator6_System_Collections_IEnumerator_get_Current_m500_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* U3CDoBobCycleU3Ec__Iterator6_t186_PropertyInfos[] =
{
	&U3CDoBobCycleU3Ec__Iterator6_t186____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo,
	&U3CDoBobCycleU3Ec__Iterator6_t186____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern const MethodInfo U3CDoBobCycleU3Ec__Iterator6_MoveNext_m501_MethodInfo;
extern const MethodInfo U3CDoBobCycleU3Ec__Iterator6_Dispose_m502_MethodInfo;
extern const MethodInfo U3CDoBobCycleU3Ec__Iterator6_Reset_m503_MethodInfo;
static const Il2CppMethodReference U3CDoBobCycleU3Ec__Iterator6_t186_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&U3CDoBobCycleU3Ec__Iterator6_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m499_MethodInfo,
	&U3CDoBobCycleU3Ec__Iterator6_System_Collections_IEnumerator_get_Current_m500_MethodInfo,
	&U3CDoBobCycleU3Ec__Iterator6_MoveNext_m501_MethodInfo,
	&U3CDoBobCycleU3Ec__Iterator6_Dispose_m502_MethodInfo,
	&U3CDoBobCycleU3Ec__Iterator6_Reset_m503_MethodInfo,
};
static bool U3CDoBobCycleU3Ec__Iterator6_t186_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* U3CDoBobCycleU3Ec__Iterator6_t186_InterfacesTypeInfos[] = 
{
	&IEnumerator_1_t284_0_0_0,
	&IEnumerator_t217_0_0_0,
	&IDisposable_t233_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CDoBobCycleU3Ec__Iterator6_t186_InterfacesOffsets[] = 
{
	{ &IEnumerator_1_t284_0_0_0, 4},
	{ &IEnumerator_t217_0_0_0, 5},
	{ &IDisposable_t233_0_0_0, 7},
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType U3CDoBobCycleU3Ec__Iterator6_t186_0_0_0;
extern const Il2CppType U3CDoBobCycleU3Ec__Iterator6_t186_1_0_0;
extern TypeInfo LerpControlledBob_t185_il2cpp_TypeInfo;
extern const Il2CppType LerpControlledBob_t185_0_0_0;
struct U3CDoBobCycleU3Ec__Iterator6_t186;
const Il2CppTypeDefinitionMetadata U3CDoBobCycleU3Ec__Iterator6_t186_DefinitionMetadata = 
{
	&LerpControlledBob_t185_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CDoBobCycleU3Ec__Iterator6_t186_InterfacesTypeInfos/* implementedInterfaces */
	, U3CDoBobCycleU3Ec__Iterator6_t186_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CDoBobCycleU3Ec__Iterator6_t186_VTable/* vtableMethods */
	, U3CDoBobCycleU3Ec__Iterator6_t186_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 872/* fieldStart */

};
TypeInfo U3CDoBobCycleU3Ec__Iterator6_t186_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "<DoBobCycle>c__Iterator6"/* name */
	, ""/* namespaze */
	, U3CDoBobCycleU3Ec__Iterator6_t186_MethodInfos/* methods */
	, U3CDoBobCycleU3Ec__Iterator6_t186_PropertyInfos/* properties */
	, NULL/* events */
	, &U3CDoBobCycleU3Ec__Iterator6_t186_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 162/* custom_attributes_cache */
	, &U3CDoBobCycleU3Ec__Iterator6_t186_0_0_0/* byval_arg */
	, &U3CDoBobCycleU3Ec__Iterator6_t186_1_0_0/* this_arg */
	, &U3CDoBobCycleU3Ec__Iterator6_t186_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CDoBobCycleU3Ec__Iterator6_t186)/* instance_size */
	, sizeof (U3CDoBobCycleU3Ec__Iterator6_t186)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityStandardAssets.Utility.LerpControlledBob
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Le_0.h"
// Metadata Definition UnityStandardAssets.Utility.LerpControlledBob
// UnityStandardAssets.Utility.LerpControlledBob
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Le_0MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.LerpControlledBob::.ctor()
extern const MethodInfo LerpControlledBob__ctor_m504_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&LerpControlledBob__ctor_m504/* method */
	, &LerpControlledBob_t185_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 511/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityStandardAssets.Utility.LerpControlledBob::Offset()
extern const MethodInfo LerpControlledBob_Offset_m505_MethodInfo = 
{
	"Offset"/* name */
	, (methodPointerType)&LerpControlledBob_Offset_m505/* method */
	, &LerpControlledBob_t185_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 512/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.IEnumerator UnityStandardAssets.Utility.LerpControlledBob::DoBobCycle()
extern const MethodInfo LerpControlledBob_DoBobCycle_m506_MethodInfo = 
{
	"DoBobCycle"/* name */
	, (methodPointerType)&LerpControlledBob_DoBobCycle_m506/* method */
	, &LerpControlledBob_t185_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t217_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 161/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 513/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* LerpControlledBob_t185_MethodInfos[] =
{
	&LerpControlledBob__ctor_m504_MethodInfo,
	&LerpControlledBob_Offset_m505_MethodInfo,
	&LerpControlledBob_DoBobCycle_m506_MethodInfo,
	NULL
};
static const Il2CppType* LerpControlledBob_t185_il2cpp_TypeInfo__nestedTypes[1] =
{
	&U3CDoBobCycleU3Ec__Iterator6_t186_0_0_0,
};
static const Il2CppMethodReference LerpControlledBob_t185_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool LerpControlledBob_t185_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType LerpControlledBob_t185_1_0_0;
struct LerpControlledBob_t185;
const Il2CppTypeDefinitionMetadata LerpControlledBob_t185_DefinitionMetadata = 
{
	NULL/* declaringType */
	, LerpControlledBob_t185_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, LerpControlledBob_t185_VTable/* vtableMethods */
	, LerpControlledBob_t185_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 876/* fieldStart */

};
TypeInfo LerpControlledBob_t185_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "LerpControlledBob"/* name */
	, "UnityStandardAssets.Utility"/* namespaze */
	, LerpControlledBob_t185_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &LerpControlledBob_t185_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &LerpControlledBob_t185_0_0_0/* byval_arg */
	, &LerpControlledBob_t185_1_0_0/* this_arg */
	, &LerpControlledBob_t185_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LerpControlledBob_t185)/* instance_size */
	, sizeof (LerpControlledBob_t185)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator7
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Ob.h"
// Metadata Definition UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator7
extern TypeInfo U3CResetCoroutineU3Ec__Iterator7_t187_il2cpp_TypeInfo;
// UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator7
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_ObMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator7::.ctor()
extern const MethodInfo U3CResetCoroutineU3Ec__Iterator7__ctor_m507_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CResetCoroutineU3Ec__Iterator7__ctor_m507/* method */
	, &U3CResetCoroutineU3Ec__Iterator7_t187_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 524/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator7::System.Collections.Generic.IEnumerator<object>.get_Current()
extern const MethodInfo U3CResetCoroutineU3Ec__Iterator7_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m508_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<object>.get_Current"/* name */
	, (methodPointerType)&U3CResetCoroutineU3Ec__Iterator7_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m508/* method */
	, &U3CResetCoroutineU3Ec__Iterator7_t187_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 169/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 525/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator7::System.Collections.IEnumerator.get_Current()
extern const MethodInfo U3CResetCoroutineU3Ec__Iterator7_System_Collections_IEnumerator_get_Current_m509_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&U3CResetCoroutineU3Ec__Iterator7_System_Collections_IEnumerator_get_Current_m509/* method */
	, &U3CResetCoroutineU3Ec__Iterator7_t187_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 170/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 526/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator7::MoveNext()
extern const MethodInfo U3CResetCoroutineU3Ec__Iterator7_MoveNext_m510_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&U3CResetCoroutineU3Ec__Iterator7_MoveNext_m510/* method */
	, &U3CResetCoroutineU3Ec__Iterator7_t187_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 527/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator7::Dispose()
extern const MethodInfo U3CResetCoroutineU3Ec__Iterator7_Dispose_m511_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&U3CResetCoroutineU3Ec__Iterator7_Dispose_m511/* method */
	, &U3CResetCoroutineU3Ec__Iterator7_t187_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 171/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 528/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator7::Reset()
extern const MethodInfo U3CResetCoroutineU3Ec__Iterator7_Reset_m512_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&U3CResetCoroutineU3Ec__Iterator7_Reset_m512/* method */
	, &U3CResetCoroutineU3Ec__Iterator7_t187_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 172/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 529/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CResetCoroutineU3Ec__Iterator7_t187_MethodInfos[] =
{
	&U3CResetCoroutineU3Ec__Iterator7__ctor_m507_MethodInfo,
	&U3CResetCoroutineU3Ec__Iterator7_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m508_MethodInfo,
	&U3CResetCoroutineU3Ec__Iterator7_System_Collections_IEnumerator_get_Current_m509_MethodInfo,
	&U3CResetCoroutineU3Ec__Iterator7_MoveNext_m510_MethodInfo,
	&U3CResetCoroutineU3Ec__Iterator7_Dispose_m511_MethodInfo,
	&U3CResetCoroutineU3Ec__Iterator7_Reset_m512_MethodInfo,
	NULL
};
extern const MethodInfo U3CResetCoroutineU3Ec__Iterator7_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m508_MethodInfo;
static const PropertyInfo U3CResetCoroutineU3Ec__Iterator7_t187____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo = 
{
	&U3CResetCoroutineU3Ec__Iterator7_t187_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<object>.Current"/* name */
	, &U3CResetCoroutineU3Ec__Iterator7_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m508_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo U3CResetCoroutineU3Ec__Iterator7_System_Collections_IEnumerator_get_Current_m509_MethodInfo;
static const PropertyInfo U3CResetCoroutineU3Ec__Iterator7_t187____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CResetCoroutineU3Ec__Iterator7_t187_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CResetCoroutineU3Ec__Iterator7_System_Collections_IEnumerator_get_Current_m509_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* U3CResetCoroutineU3Ec__Iterator7_t187_PropertyInfos[] =
{
	&U3CResetCoroutineU3Ec__Iterator7_t187____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo,
	&U3CResetCoroutineU3Ec__Iterator7_t187____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern const MethodInfo U3CResetCoroutineU3Ec__Iterator7_MoveNext_m510_MethodInfo;
extern const MethodInfo U3CResetCoroutineU3Ec__Iterator7_Dispose_m511_MethodInfo;
extern const MethodInfo U3CResetCoroutineU3Ec__Iterator7_Reset_m512_MethodInfo;
static const Il2CppMethodReference U3CResetCoroutineU3Ec__Iterator7_t187_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&U3CResetCoroutineU3Ec__Iterator7_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m508_MethodInfo,
	&U3CResetCoroutineU3Ec__Iterator7_System_Collections_IEnumerator_get_Current_m509_MethodInfo,
	&U3CResetCoroutineU3Ec__Iterator7_MoveNext_m510_MethodInfo,
	&U3CResetCoroutineU3Ec__Iterator7_Dispose_m511_MethodInfo,
	&U3CResetCoroutineU3Ec__Iterator7_Reset_m512_MethodInfo,
};
static bool U3CResetCoroutineU3Ec__Iterator7_t187_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* U3CResetCoroutineU3Ec__Iterator7_t187_InterfacesTypeInfos[] = 
{
	&IEnumerator_1_t284_0_0_0,
	&IEnumerator_t217_0_0_0,
	&IDisposable_t233_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CResetCoroutineU3Ec__Iterator7_t187_InterfacesOffsets[] = 
{
	{ &IEnumerator_1_t284_0_0_0, 4},
	{ &IEnumerator_t217_0_0_0, 5},
	{ &IDisposable_t233_0_0_0, 7},
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType U3CResetCoroutineU3Ec__Iterator7_t187_0_0_0;
extern const Il2CppType U3CResetCoroutineU3Ec__Iterator7_t187_1_0_0;
extern TypeInfo ObjectResetter_t149_il2cpp_TypeInfo;
extern const Il2CppType ObjectResetter_t149_0_0_0;
struct U3CResetCoroutineU3Ec__Iterator7_t187;
const Il2CppTypeDefinitionMetadata U3CResetCoroutineU3Ec__Iterator7_t187_DefinitionMetadata = 
{
	&ObjectResetter_t149_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CResetCoroutineU3Ec__Iterator7_t187_InterfacesTypeInfos/* implementedInterfaces */
	, U3CResetCoroutineU3Ec__Iterator7_t187_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CResetCoroutineU3Ec__Iterator7_t187_VTable/* vtableMethods */
	, U3CResetCoroutineU3Ec__Iterator7_t187_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 879/* fieldStart */

};
TypeInfo U3CResetCoroutineU3Ec__Iterator7_t187_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "<ResetCoroutine>c__Iterator7"/* name */
	, ""/* namespaze */
	, U3CResetCoroutineU3Ec__Iterator7_t187_MethodInfos/* methods */
	, U3CResetCoroutineU3Ec__Iterator7_t187_PropertyInfos/* properties */
	, NULL/* events */
	, &U3CResetCoroutineU3Ec__Iterator7_t187_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 168/* custom_attributes_cache */
	, &U3CResetCoroutineU3Ec__Iterator7_t187_0_0_0/* byval_arg */
	, &U3CResetCoroutineU3Ec__Iterator7_t187_1_0_0/* this_arg */
	, &U3CResetCoroutineU3Ec__Iterator7_t187_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CResetCoroutineU3Ec__Iterator7_t187)/* instance_size */
	, sizeof (U3CResetCoroutineU3Ec__Iterator7_t187)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityStandardAssets.Utility.ObjectResetter
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Ob_0.h"
// Metadata Definition UnityStandardAssets.Utility.ObjectResetter
// UnityStandardAssets.Utility.ObjectResetter
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Ob_0MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.ObjectResetter::.ctor()
extern const MethodInfo ObjectResetter__ctor_m513_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ObjectResetter__ctor_m513/* method */
	, &ObjectResetter_t149_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 520/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.ObjectResetter::Start()
extern const MethodInfo ObjectResetter_Start_m514_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&ObjectResetter_Start_m514/* method */
	, &ObjectResetter_t149_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 521/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo ObjectResetter_t149_ObjectResetter_DelayedReset_m515_ParameterInfos[] = 
{
	{"delay", 0, 134218050, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.ObjectResetter::DelayedReset(System.Single)
extern const MethodInfo ObjectResetter_DelayedReset_m515_MethodInfo = 
{
	"DelayedReset"/* name */
	, (methodPointerType)&ObjectResetter_DelayedReset_m515/* method */
	, &ObjectResetter_t149_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Single_t254/* invoker_method */
	, ObjectResetter_t149_ObjectResetter_DelayedReset_m515_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 522/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo ObjectResetter_t149_ObjectResetter_ResetCoroutine_m516_ParameterInfos[] = 
{
	{"delay", 0, 134218051, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Collections.IEnumerator UnityStandardAssets.Utility.ObjectResetter::ResetCoroutine(System.Single)
extern const MethodInfo ObjectResetter_ResetCoroutine_m516_MethodInfo = 
{
	"ResetCoroutine"/* name */
	, (methodPointerType)&ObjectResetter_ResetCoroutine_m516/* method */
	, &ObjectResetter_t149_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t217_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Single_t254/* invoker_method */
	, ObjectResetter_t149_ObjectResetter_ResetCoroutine_m516_ParameterInfos/* parameters */
	, 167/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 523/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ObjectResetter_t149_MethodInfos[] =
{
	&ObjectResetter__ctor_m513_MethodInfo,
	&ObjectResetter_Start_m514_MethodInfo,
	&ObjectResetter_DelayedReset_m515_MethodInfo,
	&ObjectResetter_ResetCoroutine_m516_MethodInfo,
	NULL
};
static const Il2CppType* ObjectResetter_t149_il2cpp_TypeInfo__nestedTypes[1] =
{
	&U3CResetCoroutineU3Ec__Iterator7_t187_0_0_0,
};
static const Il2CppMethodReference ObjectResetter_t149_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool ObjectResetter_t149_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType ObjectResetter_t149_1_0_0;
struct ObjectResetter_t149;
const Il2CppTypeDefinitionMetadata ObjectResetter_t149_DefinitionMetadata = 
{
	NULL/* declaringType */
	, ObjectResetter_t149_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, ObjectResetter_t149_VTable/* vtableMethods */
	, ObjectResetter_t149_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 887/* fieldStart */

};
TypeInfo ObjectResetter_t149_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "ObjectResetter"/* name */
	, "UnityStandardAssets.Utility"/* namespaze */
	, ObjectResetter_t149_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ObjectResetter_t149_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ObjectResetter_t149_0_0_0/* byval_arg */
	, &ObjectResetter_t149_1_0_0/* this_arg */
	, &ObjectResetter_t149_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ObjectResetter_t149)/* instance_size */
	, sizeof (ObjectResetter_t149)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator8
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Pa.h"
// Metadata Definition UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator8
extern TypeInfo U3CStartU3Ec__Iterator8_t190_il2cpp_TypeInfo;
// UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator8
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_PaMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator8::.ctor()
extern const MethodInfo U3CStartU3Ec__Iterator8__ctor_m517_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CStartU3Ec__Iterator8__ctor_m517/* method */
	, &U3CStartU3Ec__Iterator8_t190_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 533/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator8::System.Collections.Generic.IEnumerator<object>.get_Current()
extern const MethodInfo U3CStartU3Ec__Iterator8_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m518_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<object>.get_Current"/* name */
	, (methodPointerType)&U3CStartU3Ec__Iterator8_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m518/* method */
	, &U3CStartU3Ec__Iterator8_t190_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 175/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 534/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator8::System.Collections.IEnumerator.get_Current()
extern const MethodInfo U3CStartU3Ec__Iterator8_System_Collections_IEnumerator_get_Current_m519_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&U3CStartU3Ec__Iterator8_System_Collections_IEnumerator_get_Current_m519/* method */
	, &U3CStartU3Ec__Iterator8_t190_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 176/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 535/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator8::MoveNext()
extern const MethodInfo U3CStartU3Ec__Iterator8_MoveNext_m520_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&U3CStartU3Ec__Iterator8_MoveNext_m520/* method */
	, &U3CStartU3Ec__Iterator8_t190_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 536/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator8::Dispose()
extern const MethodInfo U3CStartU3Ec__Iterator8_Dispose_m521_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&U3CStartU3Ec__Iterator8_Dispose_m521/* method */
	, &U3CStartU3Ec__Iterator8_t190_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 177/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 537/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator8::Reset()
extern const MethodInfo U3CStartU3Ec__Iterator8_Reset_m522_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&U3CStartU3Ec__Iterator8_Reset_m522/* method */
	, &U3CStartU3Ec__Iterator8_t190_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 178/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 538/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CStartU3Ec__Iterator8_t190_MethodInfos[] =
{
	&U3CStartU3Ec__Iterator8__ctor_m517_MethodInfo,
	&U3CStartU3Ec__Iterator8_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m518_MethodInfo,
	&U3CStartU3Ec__Iterator8_System_Collections_IEnumerator_get_Current_m519_MethodInfo,
	&U3CStartU3Ec__Iterator8_MoveNext_m520_MethodInfo,
	&U3CStartU3Ec__Iterator8_Dispose_m521_MethodInfo,
	&U3CStartU3Ec__Iterator8_Reset_m522_MethodInfo,
	NULL
};
extern const MethodInfo U3CStartU3Ec__Iterator8_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m518_MethodInfo;
static const PropertyInfo U3CStartU3Ec__Iterator8_t190____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo = 
{
	&U3CStartU3Ec__Iterator8_t190_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<object>.Current"/* name */
	, &U3CStartU3Ec__Iterator8_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m518_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo U3CStartU3Ec__Iterator8_System_Collections_IEnumerator_get_Current_m519_MethodInfo;
static const PropertyInfo U3CStartU3Ec__Iterator8_t190____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CStartU3Ec__Iterator8_t190_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CStartU3Ec__Iterator8_System_Collections_IEnumerator_get_Current_m519_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* U3CStartU3Ec__Iterator8_t190_PropertyInfos[] =
{
	&U3CStartU3Ec__Iterator8_t190____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo,
	&U3CStartU3Ec__Iterator8_t190____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern const MethodInfo U3CStartU3Ec__Iterator8_MoveNext_m520_MethodInfo;
extern const MethodInfo U3CStartU3Ec__Iterator8_Dispose_m521_MethodInfo;
extern const MethodInfo U3CStartU3Ec__Iterator8_Reset_m522_MethodInfo;
static const Il2CppMethodReference U3CStartU3Ec__Iterator8_t190_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&U3CStartU3Ec__Iterator8_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m518_MethodInfo,
	&U3CStartU3Ec__Iterator8_System_Collections_IEnumerator_get_Current_m519_MethodInfo,
	&U3CStartU3Ec__Iterator8_MoveNext_m520_MethodInfo,
	&U3CStartU3Ec__Iterator8_Dispose_m521_MethodInfo,
	&U3CStartU3Ec__Iterator8_Reset_m522_MethodInfo,
};
static bool U3CStartU3Ec__Iterator8_t190_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* U3CStartU3Ec__Iterator8_t190_InterfacesTypeInfos[] = 
{
	&IEnumerator_1_t284_0_0_0,
	&IEnumerator_t217_0_0_0,
	&IDisposable_t233_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CStartU3Ec__Iterator8_t190_InterfacesOffsets[] = 
{
	{ &IEnumerator_1_t284_0_0_0, 4},
	{ &IEnumerator_t217_0_0_0, 5},
	{ &IDisposable_t233_0_0_0, 7},
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType U3CStartU3Ec__Iterator8_t190_0_0_0;
extern const Il2CppType U3CStartU3Ec__Iterator8_t190_1_0_0;
extern TypeInfo ParticleSystemDestroyer_t189_il2cpp_TypeInfo;
extern const Il2CppType ParticleSystemDestroyer_t189_0_0_0;
struct U3CStartU3Ec__Iterator8_t190;
const Il2CppTypeDefinitionMetadata U3CStartU3Ec__Iterator8_t190_DefinitionMetadata = 
{
	&ParticleSystemDestroyer_t189_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CStartU3Ec__Iterator8_t190_InterfacesTypeInfos/* implementedInterfaces */
	, U3CStartU3Ec__Iterator8_t190_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CStartU3Ec__Iterator8_t190_VTable/* vtableMethods */
	, U3CStartU3Ec__Iterator8_t190_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 891/* fieldStart */

};
TypeInfo U3CStartU3Ec__Iterator8_t190_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "<Start>c__Iterator8"/* name */
	, ""/* namespaze */
	, U3CStartU3Ec__Iterator8_t190_MethodInfos/* methods */
	, U3CStartU3Ec__Iterator8_t190_PropertyInfos/* properties */
	, NULL/* events */
	, &U3CStartU3Ec__Iterator8_t190_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 174/* custom_attributes_cache */
	, &U3CStartU3Ec__Iterator8_t190_0_0_0/* byval_arg */
	, &U3CStartU3Ec__Iterator8_t190_1_0_0/* this_arg */
	, &U3CStartU3Ec__Iterator8_t190_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CStartU3Ec__Iterator8_t190)/* instance_size */
	, sizeof (U3CStartU3Ec__Iterator8_t190)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityStandardAssets.Utility.ParticleSystemDestroyer
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Pa_0.h"
// Metadata Definition UnityStandardAssets.Utility.ParticleSystemDestroyer
// UnityStandardAssets.Utility.ParticleSystemDestroyer
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Pa_0MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.ParticleSystemDestroyer::.ctor()
extern const MethodInfo ParticleSystemDestroyer__ctor_m523_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ParticleSystemDestroyer__ctor_m523/* method */
	, &ParticleSystemDestroyer_t189_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 530/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.IEnumerator UnityStandardAssets.Utility.ParticleSystemDestroyer::Start()
extern const MethodInfo ParticleSystemDestroyer_Start_m524_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&ParticleSystemDestroyer_Start_m524/* method */
	, &ParticleSystemDestroyer_t189_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t217_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 173/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 531/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.ParticleSystemDestroyer::Stop()
extern const MethodInfo ParticleSystemDestroyer_Stop_m525_MethodInfo = 
{
	"Stop"/* name */
	, (methodPointerType)&ParticleSystemDestroyer_Stop_m525/* method */
	, &ParticleSystemDestroyer_t189_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 532/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ParticleSystemDestroyer_t189_MethodInfos[] =
{
	&ParticleSystemDestroyer__ctor_m523_MethodInfo,
	&ParticleSystemDestroyer_Start_m524_MethodInfo,
	&ParticleSystemDestroyer_Stop_m525_MethodInfo,
	NULL
};
static const Il2CppType* ParticleSystemDestroyer_t189_il2cpp_TypeInfo__nestedTypes[1] =
{
	&U3CStartU3Ec__Iterator8_t190_0_0_0,
};
static const Il2CppMethodReference ParticleSystemDestroyer_t189_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool ParticleSystemDestroyer_t189_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType ParticleSystemDestroyer_t189_1_0_0;
struct ParticleSystemDestroyer_t189;
const Il2CppTypeDefinitionMetadata ParticleSystemDestroyer_t189_DefinitionMetadata = 
{
	NULL/* declaringType */
	, ParticleSystemDestroyer_t189_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, ParticleSystemDestroyer_t189_VTable/* vtableMethods */
	, ParticleSystemDestroyer_t189_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 902/* fieldStart */

};
TypeInfo ParticleSystemDestroyer_t189_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "ParticleSystemDestroyer"/* name */
	, "UnityStandardAssets.Utility"/* namespaze */
	, ParticleSystemDestroyer_t189_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ParticleSystemDestroyer_t189_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ParticleSystemDestroyer_t189_0_0_0/* byval_arg */
	, &ParticleSystemDestroyer_t189_1_0_0/* this_arg */
	, &ParticleSystemDestroyer_t189_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ParticleSystemDestroyer_t189)/* instance_size */
	, sizeof (ParticleSystemDestroyer_t189)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.Utility.PlatformSpecificContent/BuildTargetGroup
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Pl.h"
// Metadata Definition UnityStandardAssets.Utility.PlatformSpecificContent/BuildTargetGroup
extern TypeInfo BuildTargetGroup_t191_il2cpp_TypeInfo;
// UnityStandardAssets.Utility.PlatformSpecificContent/BuildTargetGroup
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_PlMethodDeclarations.h"
static const MethodInfo* BuildTargetGroup_t191_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference BuildTargetGroup_t191_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool BuildTargetGroup_t191_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair BuildTargetGroup_t191_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType BuildTargetGroup_t191_0_0_0;
extern const Il2CppType BuildTargetGroup_t191_1_0_0;
extern TypeInfo PlatformSpecificContent_t194_il2cpp_TypeInfo;
extern const Il2CppType PlatformSpecificContent_t194_0_0_0;
const Il2CppTypeDefinitionMetadata BuildTargetGroup_t191_DefinitionMetadata = 
{
	&PlatformSpecificContent_t194_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, BuildTargetGroup_t191_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, BuildTargetGroup_t191_VTable/* vtableMethods */
	, BuildTargetGroup_t191_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 906/* fieldStart */

};
TypeInfo BuildTargetGroup_t191_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "BuildTargetGroup"/* name */
	, ""/* namespaze */
	, BuildTargetGroup_t191_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BuildTargetGroup_t191_0_0_0/* byval_arg */
	, &BuildTargetGroup_t191_1_0_0/* this_arg */
	, &BuildTargetGroup_t191_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BuildTargetGroup_t191)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (BuildTargetGroup_t191)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityStandardAssets.Utility.PlatformSpecificContent
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Pl_0.h"
// Metadata Definition UnityStandardAssets.Utility.PlatformSpecificContent
// UnityStandardAssets.Utility.PlatformSpecificContent
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Pl_0MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.PlatformSpecificContent::.ctor()
extern const MethodInfo PlatformSpecificContent__ctor_m526_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PlatformSpecificContent__ctor_m526/* method */
	, &PlatformSpecificContent_t194_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 539/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.PlatformSpecificContent::OnEnable()
extern const MethodInfo PlatformSpecificContent_OnEnable_m527_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&PlatformSpecificContent_OnEnable_m527/* method */
	, &PlatformSpecificContent_t194_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 540/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.PlatformSpecificContent::CheckEnableContent()
extern const MethodInfo PlatformSpecificContent_CheckEnableContent_m528_MethodInfo = 
{
	"CheckEnableContent"/* name */
	, (methodPointerType)&PlatformSpecificContent_CheckEnableContent_m528/* method */
	, &PlatformSpecificContent_t194_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 541/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo PlatformSpecificContent_t194_PlatformSpecificContent_EnableContent_m529_ParameterInfos[] = 
{
	{"enabled", 0, 134218052, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.PlatformSpecificContent::EnableContent(System.Boolean)
extern const MethodInfo PlatformSpecificContent_EnableContent_m529_MethodInfo = 
{
	"EnableContent"/* name */
	, (methodPointerType)&PlatformSpecificContent_EnableContent_m529/* method */
	, &PlatformSpecificContent_t194_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274/* invoker_method */
	, PlatformSpecificContent_t194_PlatformSpecificContent_EnableContent_m529_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 542/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* PlatformSpecificContent_t194_MethodInfos[] =
{
	&PlatformSpecificContent__ctor_m526_MethodInfo,
	&PlatformSpecificContent_OnEnable_m527_MethodInfo,
	&PlatformSpecificContent_CheckEnableContent_m528_MethodInfo,
	&PlatformSpecificContent_EnableContent_m529_MethodInfo,
	NULL
};
static const Il2CppType* PlatformSpecificContent_t194_il2cpp_TypeInfo__nestedTypes[1] =
{
	&BuildTargetGroup_t191_0_0_0,
};
static const Il2CppMethodReference PlatformSpecificContent_t194_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool PlatformSpecificContent_t194_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType PlatformSpecificContent_t194_1_0_0;
struct PlatformSpecificContent_t194;
const Il2CppTypeDefinitionMetadata PlatformSpecificContent_t194_DefinitionMetadata = 
{
	NULL/* declaringType */
	, PlatformSpecificContent_t194_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, PlatformSpecificContent_t194_VTable/* vtableMethods */
	, PlatformSpecificContent_t194_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 909/* fieldStart */

};
TypeInfo PlatformSpecificContent_t194_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "PlatformSpecificContent"/* name */
	, "UnityStandardAssets.Utility"/* namespaze */
	, PlatformSpecificContent_t194_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &PlatformSpecificContent_t194_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PlatformSpecificContent_t194_0_0_0/* byval_arg */
	, &PlatformSpecificContent_t194_1_0_0/* this_arg */
	, &PlatformSpecificContent_t194_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PlatformSpecificContent_t194)/* instance_size */
	, sizeof (PlatformSpecificContent_t194)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.Utility.SimpleActivatorMenu
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Si.h"
// Metadata Definition UnityStandardAssets.Utility.SimpleActivatorMenu
extern TypeInfo SimpleActivatorMenu_t195_il2cpp_TypeInfo;
// UnityStandardAssets.Utility.SimpleActivatorMenu
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_SiMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.SimpleActivatorMenu::.ctor()
extern const MethodInfo SimpleActivatorMenu__ctor_m530_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SimpleActivatorMenu__ctor_m530/* method */
	, &SimpleActivatorMenu_t195_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 543/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.SimpleActivatorMenu::OnEnable()
extern const MethodInfo SimpleActivatorMenu_OnEnable_m531_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&SimpleActivatorMenu_OnEnable_m531/* method */
	, &SimpleActivatorMenu_t195_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 544/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.SimpleActivatorMenu::NextCamera()
extern const MethodInfo SimpleActivatorMenu_NextCamera_m532_MethodInfo = 
{
	"NextCamera"/* name */
	, (methodPointerType)&SimpleActivatorMenu_NextCamera_m532/* method */
	, &SimpleActivatorMenu_t195_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 545/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SimpleActivatorMenu_t195_MethodInfos[] =
{
	&SimpleActivatorMenu__ctor_m530_MethodInfo,
	&SimpleActivatorMenu_OnEnable_m531_MethodInfo,
	&SimpleActivatorMenu_NextCamera_m532_MethodInfo,
	NULL
};
static const Il2CppMethodReference SimpleActivatorMenu_t195_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool SimpleActivatorMenu_t195_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType SimpleActivatorMenu_t195_0_0_0;
extern const Il2CppType SimpleActivatorMenu_t195_1_0_0;
struct SimpleActivatorMenu_t195;
const Il2CppTypeDefinitionMetadata SimpleActivatorMenu_t195_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, SimpleActivatorMenu_t195_VTable/* vtableMethods */
	, SimpleActivatorMenu_t195_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 913/* fieldStart */

};
TypeInfo SimpleActivatorMenu_t195_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "SimpleActivatorMenu"/* name */
	, "UnityStandardAssets.Utility"/* namespaze */
	, SimpleActivatorMenu_t195_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SimpleActivatorMenu_t195_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SimpleActivatorMenu_t195_0_0_0/* byval_arg */
	, &SimpleActivatorMenu_t195_1_0_0/* this_arg */
	, &SimpleActivatorMenu_t195_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SimpleActivatorMenu_t195)/* instance_size */
	, sizeof (SimpleActivatorMenu_t195)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.Utility.SimpleMouseRotator
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Si_0.h"
// Metadata Definition UnityStandardAssets.Utility.SimpleMouseRotator
extern TypeInfo SimpleMouseRotator_t196_il2cpp_TypeInfo;
// UnityStandardAssets.Utility.SimpleMouseRotator
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Si_0MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.SimpleMouseRotator::.ctor()
extern const MethodInfo SimpleMouseRotator__ctor_m533_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SimpleMouseRotator__ctor_m533/* method */
	, &SimpleMouseRotator_t196_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 546/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.SimpleMouseRotator::Start()
extern const MethodInfo SimpleMouseRotator_Start_m534_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&SimpleMouseRotator_Start_m534/* method */
	, &SimpleMouseRotator_t196_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 547/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.SimpleMouseRotator::Update()
extern const MethodInfo SimpleMouseRotator_Update_m535_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&SimpleMouseRotator_Update_m535/* method */
	, &SimpleMouseRotator_t196_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 548/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SimpleMouseRotator_t196_MethodInfos[] =
{
	&SimpleMouseRotator__ctor_m533_MethodInfo,
	&SimpleMouseRotator_Start_m534_MethodInfo,
	&SimpleMouseRotator_Update_m535_MethodInfo,
	NULL
};
static const Il2CppMethodReference SimpleMouseRotator_t196_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool SimpleMouseRotator_t196_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType SimpleMouseRotator_t196_0_0_0;
extern const Il2CppType SimpleMouseRotator_t196_1_0_0;
struct SimpleMouseRotator_t196;
const Il2CppTypeDefinitionMetadata SimpleMouseRotator_t196_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, SimpleMouseRotator_t196_VTable/* vtableMethods */
	, SimpleMouseRotator_t196_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 916/* fieldStart */

};
TypeInfo SimpleMouseRotator_t196_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "SimpleMouseRotator"/* name */
	, "UnityStandardAssets.Utility"/* namespaze */
	, SimpleMouseRotator_t196_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SimpleMouseRotator_t196_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SimpleMouseRotator_t196_0_0_0/* byval_arg */
	, &SimpleMouseRotator_t196_1_0_0/* this_arg */
	, &SimpleMouseRotator_t196_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SimpleMouseRotator_t196)/* instance_size */
	, sizeof (SimpleMouseRotator_t196)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 10/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.Utility.SmoothFollow
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Sm.h"
// Metadata Definition UnityStandardAssets.Utility.SmoothFollow
extern TypeInfo SmoothFollow_t197_il2cpp_TypeInfo;
// UnityStandardAssets.Utility.SmoothFollow
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_SmMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.SmoothFollow::.ctor()
extern const MethodInfo SmoothFollow__ctor_m536_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SmoothFollow__ctor_m536/* method */
	, &SmoothFollow_t197_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 549/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.SmoothFollow::Start()
extern const MethodInfo SmoothFollow_Start_m537_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&SmoothFollow_Start_m537/* method */
	, &SmoothFollow_t197_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 550/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.SmoothFollow::LateUpdate()
extern const MethodInfo SmoothFollow_LateUpdate_m538_MethodInfo = 
{
	"LateUpdate"/* name */
	, (methodPointerType)&SmoothFollow_LateUpdate_m538/* method */
	, &SmoothFollow_t197_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 551/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SmoothFollow_t197_MethodInfos[] =
{
	&SmoothFollow__ctor_m536_MethodInfo,
	&SmoothFollow_Start_m537_MethodInfo,
	&SmoothFollow_LateUpdate_m538_MethodInfo,
	NULL
};
static const Il2CppMethodReference SmoothFollow_t197_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool SmoothFollow_t197_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType SmoothFollow_t197_0_0_0;
extern const Il2CppType SmoothFollow_t197_1_0_0;
struct SmoothFollow_t197;
const Il2CppTypeDefinitionMetadata SmoothFollow_t197_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, SmoothFollow_t197_VTable/* vtableMethods */
	, SmoothFollow_t197_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 926/* fieldStart */

};
TypeInfo SmoothFollow_t197_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "SmoothFollow"/* name */
	, "UnityStandardAssets.Utility"/* namespaze */
	, SmoothFollow_t197_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SmoothFollow_t197_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SmoothFollow_t197_0_0_0/* byval_arg */
	, &SmoothFollow_t197_1_0_0/* this_arg */
	, &SmoothFollow_t197_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SmoothFollow_t197)/* instance_size */
	, sizeof (SmoothFollow_t197)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.Utility.TimedObjectActivator/Action
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Ti.h"
// Metadata Definition UnityStandardAssets.Utility.TimedObjectActivator/Action
extern TypeInfo Action_t198_il2cpp_TypeInfo;
// UnityStandardAssets.Utility.TimedObjectActivator/Action
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_TiMethodDeclarations.h"
static const MethodInfo* Action_t198_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference Action_t198_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool Action_t198_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Action_t198_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType Action_t198_0_0_0;
extern const Il2CppType Action_t198_1_0_0;
extern TypeInfo TimedObjectActivator_t205_il2cpp_TypeInfo;
extern const Il2CppType TimedObjectActivator_t205_0_0_0;
const Il2CppTypeDefinitionMetadata Action_t198_DefinitionMetadata = 
{
	&TimedObjectActivator_t205_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Action_t198_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, Action_t198_VTable/* vtableMethods */
	, Action_t198_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 931/* fieldStart */

};
TypeInfo Action_t198_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "Action"/* name */
	, ""/* namespaze */
	, Action_t198_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Action_t198_0_0_0/* byval_arg */
	, &Action_t198_1_0_0/* this_arg */
	, &Action_t198_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Action_t198)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Action_t198)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityStandardAssets.Utility.TimedObjectActivator/Entry
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Ti_0.h"
// Metadata Definition UnityStandardAssets.Utility.TimedObjectActivator/Entry
extern TypeInfo Entry_t199_il2cpp_TypeInfo;
// UnityStandardAssets.Utility.TimedObjectActivator/Entry
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Ti_0MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.TimedObjectActivator/Entry::.ctor()
extern const MethodInfo Entry__ctor_m539_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Entry__ctor_m539/* method */
	, &Entry_t199_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 557/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Entry_t199_MethodInfos[] =
{
	&Entry__ctor_m539_MethodInfo,
	NULL
};
static const Il2CppMethodReference Entry_t199_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool Entry_t199_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType Entry_t199_0_0_0;
extern const Il2CppType Entry_t199_1_0_0;
struct Entry_t199;
const Il2CppTypeDefinitionMetadata Entry_t199_DefinitionMetadata = 
{
	&TimedObjectActivator_t205_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Entry_t199_VTable/* vtableMethods */
	, Entry_t199_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 937/* fieldStart */

};
TypeInfo Entry_t199_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "Entry"/* name */
	, ""/* namespaze */
	, Entry_t199_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Entry_t199_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Entry_t199_0_0_0/* byval_arg */
	, &Entry_t199_1_0_0/* this_arg */
	, &Entry_t199_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Entry_t199)/* instance_size */
	, sizeof (Entry_t199)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056770/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.Utility.TimedObjectActivator/Entries
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Ti_1.h"
// Metadata Definition UnityStandardAssets.Utility.TimedObjectActivator/Entries
extern TypeInfo Entries_t201_il2cpp_TypeInfo;
// UnityStandardAssets.Utility.TimedObjectActivator/Entries
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Ti_1MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.TimedObjectActivator/Entries::.ctor()
extern const MethodInfo Entries__ctor_m540_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Entries__ctor_m540/* method */
	, &Entries_t201_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 558/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Entries_t201_MethodInfos[] =
{
	&Entries__ctor_m540_MethodInfo,
	NULL
};
static const Il2CppMethodReference Entries_t201_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool Entries_t201_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType Entries_t201_0_0_0;
extern const Il2CppType Entries_t201_1_0_0;
struct Entries_t201;
const Il2CppTypeDefinitionMetadata Entries_t201_DefinitionMetadata = 
{
	&TimedObjectActivator_t205_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Entries_t201_VTable/* vtableMethods */
	, Entries_t201_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 940/* fieldStart */

};
TypeInfo Entries_t201_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "Entries"/* name */
	, ""/* namespaze */
	, Entries_t201_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Entries_t201_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Entries_t201_0_0_0/* byval_arg */
	, &Entries_t201_1_0_0/* this_arg */
	, &Entries_t201_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Entries_t201)/* instance_size */
	, sizeof (Entries_t201)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056770/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.Utility.TimedObjectActivator/<Activate>c__Iterator9
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Ti_2.h"
// Metadata Definition UnityStandardAssets.Utility.TimedObjectActivator/<Activate>c__Iterator9
extern TypeInfo U3CActivateU3Ec__Iterator9_t202_il2cpp_TypeInfo;
// UnityStandardAssets.Utility.TimedObjectActivator/<Activate>c__Iterator9
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Ti_2MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.TimedObjectActivator/<Activate>c__Iterator9::.ctor()
extern const MethodInfo U3CActivateU3Ec__Iterator9__ctor_m541_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CActivateU3Ec__Iterator9__ctor_m541/* method */
	, &U3CActivateU3Ec__Iterator9_t202_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 559/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object UnityStandardAssets.Utility.TimedObjectActivator/<Activate>c__Iterator9::System.Collections.Generic.IEnumerator<object>.get_Current()
extern const MethodInfo U3CActivateU3Ec__Iterator9_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m542_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<object>.get_Current"/* name */
	, (methodPointerType)&U3CActivateU3Ec__Iterator9_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m542/* method */
	, &U3CActivateU3Ec__Iterator9_t202_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 192/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 560/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object UnityStandardAssets.Utility.TimedObjectActivator/<Activate>c__Iterator9::System.Collections.IEnumerator.get_Current()
extern const MethodInfo U3CActivateU3Ec__Iterator9_System_Collections_IEnumerator_get_Current_m543_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&U3CActivateU3Ec__Iterator9_System_Collections_IEnumerator_get_Current_m543/* method */
	, &U3CActivateU3Ec__Iterator9_t202_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 193/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 561/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityStandardAssets.Utility.TimedObjectActivator/<Activate>c__Iterator9::MoveNext()
extern const MethodInfo U3CActivateU3Ec__Iterator9_MoveNext_m544_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&U3CActivateU3Ec__Iterator9_MoveNext_m544/* method */
	, &U3CActivateU3Ec__Iterator9_t202_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 562/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.TimedObjectActivator/<Activate>c__Iterator9::Dispose()
extern const MethodInfo U3CActivateU3Ec__Iterator9_Dispose_m545_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&U3CActivateU3Ec__Iterator9_Dispose_m545/* method */
	, &U3CActivateU3Ec__Iterator9_t202_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 194/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 563/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.TimedObjectActivator/<Activate>c__Iterator9::Reset()
extern const MethodInfo U3CActivateU3Ec__Iterator9_Reset_m546_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&U3CActivateU3Ec__Iterator9_Reset_m546/* method */
	, &U3CActivateU3Ec__Iterator9_t202_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 195/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 564/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CActivateU3Ec__Iterator9_t202_MethodInfos[] =
{
	&U3CActivateU3Ec__Iterator9__ctor_m541_MethodInfo,
	&U3CActivateU3Ec__Iterator9_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m542_MethodInfo,
	&U3CActivateU3Ec__Iterator9_System_Collections_IEnumerator_get_Current_m543_MethodInfo,
	&U3CActivateU3Ec__Iterator9_MoveNext_m544_MethodInfo,
	&U3CActivateU3Ec__Iterator9_Dispose_m545_MethodInfo,
	&U3CActivateU3Ec__Iterator9_Reset_m546_MethodInfo,
	NULL
};
extern const MethodInfo U3CActivateU3Ec__Iterator9_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m542_MethodInfo;
static const PropertyInfo U3CActivateU3Ec__Iterator9_t202____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo = 
{
	&U3CActivateU3Ec__Iterator9_t202_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<object>.Current"/* name */
	, &U3CActivateU3Ec__Iterator9_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m542_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo U3CActivateU3Ec__Iterator9_System_Collections_IEnumerator_get_Current_m543_MethodInfo;
static const PropertyInfo U3CActivateU3Ec__Iterator9_t202____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CActivateU3Ec__Iterator9_t202_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CActivateU3Ec__Iterator9_System_Collections_IEnumerator_get_Current_m543_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* U3CActivateU3Ec__Iterator9_t202_PropertyInfos[] =
{
	&U3CActivateU3Ec__Iterator9_t202____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo,
	&U3CActivateU3Ec__Iterator9_t202____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern const MethodInfo U3CActivateU3Ec__Iterator9_MoveNext_m544_MethodInfo;
extern const MethodInfo U3CActivateU3Ec__Iterator9_Dispose_m545_MethodInfo;
extern const MethodInfo U3CActivateU3Ec__Iterator9_Reset_m546_MethodInfo;
static const Il2CppMethodReference U3CActivateU3Ec__Iterator9_t202_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&U3CActivateU3Ec__Iterator9_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m542_MethodInfo,
	&U3CActivateU3Ec__Iterator9_System_Collections_IEnumerator_get_Current_m543_MethodInfo,
	&U3CActivateU3Ec__Iterator9_MoveNext_m544_MethodInfo,
	&U3CActivateU3Ec__Iterator9_Dispose_m545_MethodInfo,
	&U3CActivateU3Ec__Iterator9_Reset_m546_MethodInfo,
};
static bool U3CActivateU3Ec__Iterator9_t202_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* U3CActivateU3Ec__Iterator9_t202_InterfacesTypeInfos[] = 
{
	&IEnumerator_1_t284_0_0_0,
	&IEnumerator_t217_0_0_0,
	&IDisposable_t233_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CActivateU3Ec__Iterator9_t202_InterfacesOffsets[] = 
{
	{ &IEnumerator_1_t284_0_0_0, 4},
	{ &IEnumerator_t217_0_0_0, 5},
	{ &IDisposable_t233_0_0_0, 7},
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType U3CActivateU3Ec__Iterator9_t202_0_0_0;
extern const Il2CppType U3CActivateU3Ec__Iterator9_t202_1_0_0;
struct U3CActivateU3Ec__Iterator9_t202;
const Il2CppTypeDefinitionMetadata U3CActivateU3Ec__Iterator9_t202_DefinitionMetadata = 
{
	&TimedObjectActivator_t205_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CActivateU3Ec__Iterator9_t202_InterfacesTypeInfos/* implementedInterfaces */
	, U3CActivateU3Ec__Iterator9_t202_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CActivateU3Ec__Iterator9_t202_VTable/* vtableMethods */
	, U3CActivateU3Ec__Iterator9_t202_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 941/* fieldStart */

};
TypeInfo U3CActivateU3Ec__Iterator9_t202_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "<Activate>c__Iterator9"/* name */
	, ""/* namespaze */
	, U3CActivateU3Ec__Iterator9_t202_MethodInfos/* methods */
	, U3CActivateU3Ec__Iterator9_t202_PropertyInfos/* properties */
	, NULL/* events */
	, &U3CActivateU3Ec__Iterator9_t202_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 191/* custom_attributes_cache */
	, &U3CActivateU3Ec__Iterator9_t202_0_0_0/* byval_arg */
	, &U3CActivateU3Ec__Iterator9_t202_1_0_0/* this_arg */
	, &U3CActivateU3Ec__Iterator9_t202_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CActivateU3Ec__Iterator9_t202)/* instance_size */
	, sizeof (U3CActivateU3Ec__Iterator9_t202)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityStandardAssets.Utility.TimedObjectActivator/<Deactivate>c__IteratorA
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Ti_3.h"
// Metadata Definition UnityStandardAssets.Utility.TimedObjectActivator/<Deactivate>c__IteratorA
extern TypeInfo U3CDeactivateU3Ec__IteratorA_t203_il2cpp_TypeInfo;
// UnityStandardAssets.Utility.TimedObjectActivator/<Deactivate>c__IteratorA
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Ti_3MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.TimedObjectActivator/<Deactivate>c__IteratorA::.ctor()
extern const MethodInfo U3CDeactivateU3Ec__IteratorA__ctor_m547_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CDeactivateU3Ec__IteratorA__ctor_m547/* method */
	, &U3CDeactivateU3Ec__IteratorA_t203_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 565/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object UnityStandardAssets.Utility.TimedObjectActivator/<Deactivate>c__IteratorA::System.Collections.Generic.IEnumerator<object>.get_Current()
extern const MethodInfo U3CDeactivateU3Ec__IteratorA_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m548_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<object>.get_Current"/* name */
	, (methodPointerType)&U3CDeactivateU3Ec__IteratorA_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m548/* method */
	, &U3CDeactivateU3Ec__IteratorA_t203_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 197/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 566/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object UnityStandardAssets.Utility.TimedObjectActivator/<Deactivate>c__IteratorA::System.Collections.IEnumerator.get_Current()
extern const MethodInfo U3CDeactivateU3Ec__IteratorA_System_Collections_IEnumerator_get_Current_m549_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&U3CDeactivateU3Ec__IteratorA_System_Collections_IEnumerator_get_Current_m549/* method */
	, &U3CDeactivateU3Ec__IteratorA_t203_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 198/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 567/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityStandardAssets.Utility.TimedObjectActivator/<Deactivate>c__IteratorA::MoveNext()
extern const MethodInfo U3CDeactivateU3Ec__IteratorA_MoveNext_m550_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&U3CDeactivateU3Ec__IteratorA_MoveNext_m550/* method */
	, &U3CDeactivateU3Ec__IteratorA_t203_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 568/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.TimedObjectActivator/<Deactivate>c__IteratorA::Dispose()
extern const MethodInfo U3CDeactivateU3Ec__IteratorA_Dispose_m551_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&U3CDeactivateU3Ec__IteratorA_Dispose_m551/* method */
	, &U3CDeactivateU3Ec__IteratorA_t203_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 199/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 569/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.TimedObjectActivator/<Deactivate>c__IteratorA::Reset()
extern const MethodInfo U3CDeactivateU3Ec__IteratorA_Reset_m552_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&U3CDeactivateU3Ec__IteratorA_Reset_m552/* method */
	, &U3CDeactivateU3Ec__IteratorA_t203_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 200/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 570/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CDeactivateU3Ec__IteratorA_t203_MethodInfos[] =
{
	&U3CDeactivateU3Ec__IteratorA__ctor_m547_MethodInfo,
	&U3CDeactivateU3Ec__IteratorA_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m548_MethodInfo,
	&U3CDeactivateU3Ec__IteratorA_System_Collections_IEnumerator_get_Current_m549_MethodInfo,
	&U3CDeactivateU3Ec__IteratorA_MoveNext_m550_MethodInfo,
	&U3CDeactivateU3Ec__IteratorA_Dispose_m551_MethodInfo,
	&U3CDeactivateU3Ec__IteratorA_Reset_m552_MethodInfo,
	NULL
};
extern const MethodInfo U3CDeactivateU3Ec__IteratorA_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m548_MethodInfo;
static const PropertyInfo U3CDeactivateU3Ec__IteratorA_t203____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo = 
{
	&U3CDeactivateU3Ec__IteratorA_t203_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<object>.Current"/* name */
	, &U3CDeactivateU3Ec__IteratorA_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m548_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo U3CDeactivateU3Ec__IteratorA_System_Collections_IEnumerator_get_Current_m549_MethodInfo;
static const PropertyInfo U3CDeactivateU3Ec__IteratorA_t203____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CDeactivateU3Ec__IteratorA_t203_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CDeactivateU3Ec__IteratorA_System_Collections_IEnumerator_get_Current_m549_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* U3CDeactivateU3Ec__IteratorA_t203_PropertyInfos[] =
{
	&U3CDeactivateU3Ec__IteratorA_t203____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo,
	&U3CDeactivateU3Ec__IteratorA_t203____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern const MethodInfo U3CDeactivateU3Ec__IteratorA_MoveNext_m550_MethodInfo;
extern const MethodInfo U3CDeactivateU3Ec__IteratorA_Dispose_m551_MethodInfo;
extern const MethodInfo U3CDeactivateU3Ec__IteratorA_Reset_m552_MethodInfo;
static const Il2CppMethodReference U3CDeactivateU3Ec__IteratorA_t203_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&U3CDeactivateU3Ec__IteratorA_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m548_MethodInfo,
	&U3CDeactivateU3Ec__IteratorA_System_Collections_IEnumerator_get_Current_m549_MethodInfo,
	&U3CDeactivateU3Ec__IteratorA_MoveNext_m550_MethodInfo,
	&U3CDeactivateU3Ec__IteratorA_Dispose_m551_MethodInfo,
	&U3CDeactivateU3Ec__IteratorA_Reset_m552_MethodInfo,
};
static bool U3CDeactivateU3Ec__IteratorA_t203_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* U3CDeactivateU3Ec__IteratorA_t203_InterfacesTypeInfos[] = 
{
	&IEnumerator_1_t284_0_0_0,
	&IEnumerator_t217_0_0_0,
	&IDisposable_t233_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CDeactivateU3Ec__IteratorA_t203_InterfacesOffsets[] = 
{
	{ &IEnumerator_1_t284_0_0_0, 4},
	{ &IEnumerator_t217_0_0_0, 5},
	{ &IDisposable_t233_0_0_0, 7},
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType U3CDeactivateU3Ec__IteratorA_t203_0_0_0;
extern const Il2CppType U3CDeactivateU3Ec__IteratorA_t203_1_0_0;
struct U3CDeactivateU3Ec__IteratorA_t203;
const Il2CppTypeDefinitionMetadata U3CDeactivateU3Ec__IteratorA_t203_DefinitionMetadata = 
{
	&TimedObjectActivator_t205_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CDeactivateU3Ec__IteratorA_t203_InterfacesTypeInfos/* implementedInterfaces */
	, U3CDeactivateU3Ec__IteratorA_t203_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CDeactivateU3Ec__IteratorA_t203_VTable/* vtableMethods */
	, U3CDeactivateU3Ec__IteratorA_t203_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 945/* fieldStart */

};
TypeInfo U3CDeactivateU3Ec__IteratorA_t203_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "<Deactivate>c__IteratorA"/* name */
	, ""/* namespaze */
	, U3CDeactivateU3Ec__IteratorA_t203_MethodInfos/* methods */
	, U3CDeactivateU3Ec__IteratorA_t203_PropertyInfos/* properties */
	, NULL/* events */
	, &U3CDeactivateU3Ec__IteratorA_t203_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 196/* custom_attributes_cache */
	, &U3CDeactivateU3Ec__IteratorA_t203_0_0_0/* byval_arg */
	, &U3CDeactivateU3Ec__IteratorA_t203_1_0_0/* this_arg */
	, &U3CDeactivateU3Ec__IteratorA_t203_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CDeactivateU3Ec__IteratorA_t203)/* instance_size */
	, sizeof (U3CDeactivateU3Ec__IteratorA_t203)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>c__IteratorB
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Ti_4.h"
// Metadata Definition UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>c__IteratorB
extern TypeInfo U3CReloadLevelU3Ec__IteratorB_t204_il2cpp_TypeInfo;
// UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>c__IteratorB
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Ti_4MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>c__IteratorB::.ctor()
extern const MethodInfo U3CReloadLevelU3Ec__IteratorB__ctor_m553_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CReloadLevelU3Ec__IteratorB__ctor_m553/* method */
	, &U3CReloadLevelU3Ec__IteratorB_t204_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 571/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>c__IteratorB::System.Collections.Generic.IEnumerator<object>.get_Current()
extern const MethodInfo U3CReloadLevelU3Ec__IteratorB_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m554_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<object>.get_Current"/* name */
	, (methodPointerType)&U3CReloadLevelU3Ec__IteratorB_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m554/* method */
	, &U3CReloadLevelU3Ec__IteratorB_t204_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 202/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 572/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>c__IteratorB::System.Collections.IEnumerator.get_Current()
extern const MethodInfo U3CReloadLevelU3Ec__IteratorB_System_Collections_IEnumerator_get_Current_m555_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&U3CReloadLevelU3Ec__IteratorB_System_Collections_IEnumerator_get_Current_m555/* method */
	, &U3CReloadLevelU3Ec__IteratorB_t204_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 203/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 573/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>c__IteratorB::MoveNext()
extern const MethodInfo U3CReloadLevelU3Ec__IteratorB_MoveNext_m556_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&U3CReloadLevelU3Ec__IteratorB_MoveNext_m556/* method */
	, &U3CReloadLevelU3Ec__IteratorB_t204_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 574/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>c__IteratorB::Dispose()
extern const MethodInfo U3CReloadLevelU3Ec__IteratorB_Dispose_m557_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&U3CReloadLevelU3Ec__IteratorB_Dispose_m557/* method */
	, &U3CReloadLevelU3Ec__IteratorB_t204_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 204/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 575/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>c__IteratorB::Reset()
extern const MethodInfo U3CReloadLevelU3Ec__IteratorB_Reset_m558_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&U3CReloadLevelU3Ec__IteratorB_Reset_m558/* method */
	, &U3CReloadLevelU3Ec__IteratorB_t204_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 205/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 576/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CReloadLevelU3Ec__IteratorB_t204_MethodInfos[] =
{
	&U3CReloadLevelU3Ec__IteratorB__ctor_m553_MethodInfo,
	&U3CReloadLevelU3Ec__IteratorB_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m554_MethodInfo,
	&U3CReloadLevelU3Ec__IteratorB_System_Collections_IEnumerator_get_Current_m555_MethodInfo,
	&U3CReloadLevelU3Ec__IteratorB_MoveNext_m556_MethodInfo,
	&U3CReloadLevelU3Ec__IteratorB_Dispose_m557_MethodInfo,
	&U3CReloadLevelU3Ec__IteratorB_Reset_m558_MethodInfo,
	NULL
};
extern const MethodInfo U3CReloadLevelU3Ec__IteratorB_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m554_MethodInfo;
static const PropertyInfo U3CReloadLevelU3Ec__IteratorB_t204____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo = 
{
	&U3CReloadLevelU3Ec__IteratorB_t204_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<object>.Current"/* name */
	, &U3CReloadLevelU3Ec__IteratorB_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m554_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo U3CReloadLevelU3Ec__IteratorB_System_Collections_IEnumerator_get_Current_m555_MethodInfo;
static const PropertyInfo U3CReloadLevelU3Ec__IteratorB_t204____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CReloadLevelU3Ec__IteratorB_t204_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CReloadLevelU3Ec__IteratorB_System_Collections_IEnumerator_get_Current_m555_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* U3CReloadLevelU3Ec__IteratorB_t204_PropertyInfos[] =
{
	&U3CReloadLevelU3Ec__IteratorB_t204____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo,
	&U3CReloadLevelU3Ec__IteratorB_t204____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern const MethodInfo U3CReloadLevelU3Ec__IteratorB_MoveNext_m556_MethodInfo;
extern const MethodInfo U3CReloadLevelU3Ec__IteratorB_Dispose_m557_MethodInfo;
extern const MethodInfo U3CReloadLevelU3Ec__IteratorB_Reset_m558_MethodInfo;
static const Il2CppMethodReference U3CReloadLevelU3Ec__IteratorB_t204_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&U3CReloadLevelU3Ec__IteratorB_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m554_MethodInfo,
	&U3CReloadLevelU3Ec__IteratorB_System_Collections_IEnumerator_get_Current_m555_MethodInfo,
	&U3CReloadLevelU3Ec__IteratorB_MoveNext_m556_MethodInfo,
	&U3CReloadLevelU3Ec__IteratorB_Dispose_m557_MethodInfo,
	&U3CReloadLevelU3Ec__IteratorB_Reset_m558_MethodInfo,
};
static bool U3CReloadLevelU3Ec__IteratorB_t204_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* U3CReloadLevelU3Ec__IteratorB_t204_InterfacesTypeInfos[] = 
{
	&IEnumerator_1_t284_0_0_0,
	&IEnumerator_t217_0_0_0,
	&IDisposable_t233_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CReloadLevelU3Ec__IteratorB_t204_InterfacesOffsets[] = 
{
	{ &IEnumerator_1_t284_0_0_0, 4},
	{ &IEnumerator_t217_0_0_0, 5},
	{ &IDisposable_t233_0_0_0, 7},
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType U3CReloadLevelU3Ec__IteratorB_t204_0_0_0;
extern const Il2CppType U3CReloadLevelU3Ec__IteratorB_t204_1_0_0;
struct U3CReloadLevelU3Ec__IteratorB_t204;
const Il2CppTypeDefinitionMetadata U3CReloadLevelU3Ec__IteratorB_t204_DefinitionMetadata = 
{
	&TimedObjectActivator_t205_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CReloadLevelU3Ec__IteratorB_t204_InterfacesTypeInfos/* implementedInterfaces */
	, U3CReloadLevelU3Ec__IteratorB_t204_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CReloadLevelU3Ec__IteratorB_t204_VTable/* vtableMethods */
	, U3CReloadLevelU3Ec__IteratorB_t204_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 949/* fieldStart */

};
TypeInfo U3CReloadLevelU3Ec__IteratorB_t204_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "<ReloadLevel>c__IteratorB"/* name */
	, ""/* namespaze */
	, U3CReloadLevelU3Ec__IteratorB_t204_MethodInfos/* methods */
	, U3CReloadLevelU3Ec__IteratorB_t204_PropertyInfos/* properties */
	, NULL/* events */
	, &U3CReloadLevelU3Ec__IteratorB_t204_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 201/* custom_attributes_cache */
	, &U3CReloadLevelU3Ec__IteratorB_t204_0_0_0/* byval_arg */
	, &U3CReloadLevelU3Ec__IteratorB_t204_1_0_0/* this_arg */
	, &U3CReloadLevelU3Ec__IteratorB_t204_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CReloadLevelU3Ec__IteratorB_t204)/* instance_size */
	, sizeof (U3CReloadLevelU3Ec__IteratorB_t204)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityStandardAssets.Utility.TimedObjectActivator
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Ti_5.h"
// Metadata Definition UnityStandardAssets.Utility.TimedObjectActivator
// UnityStandardAssets.Utility.TimedObjectActivator
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Ti_5MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.TimedObjectActivator::.ctor()
extern const MethodInfo TimedObjectActivator__ctor_m559_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TimedObjectActivator__ctor_m559/* method */
	, &TimedObjectActivator_t205_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 552/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.TimedObjectActivator::Awake()
extern const MethodInfo TimedObjectActivator_Awake_m560_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&TimedObjectActivator_Awake_m560/* method */
	, &TimedObjectActivator_t205_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 553/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Entry_t199_0_0_0;
static const ParameterInfo TimedObjectActivator_t205_TimedObjectActivator_Activate_m561_ParameterInfos[] = 
{
	{"entry", 0, 134218053, 0, &Entry_t199_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.IEnumerator UnityStandardAssets.Utility.TimedObjectActivator::Activate(UnityStandardAssets.Utility.TimedObjectActivator/Entry)
extern const MethodInfo TimedObjectActivator_Activate_m561_MethodInfo = 
{
	"Activate"/* name */
	, (methodPointerType)&TimedObjectActivator_Activate_m561/* method */
	, &TimedObjectActivator_t205_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t217_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, TimedObjectActivator_t205_TimedObjectActivator_Activate_m561_ParameterInfos/* parameters */
	, 188/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 554/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Entry_t199_0_0_0;
static const ParameterInfo TimedObjectActivator_t205_TimedObjectActivator_Deactivate_m562_ParameterInfos[] = 
{
	{"entry", 0, 134218054, 0, &Entry_t199_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.IEnumerator UnityStandardAssets.Utility.TimedObjectActivator::Deactivate(UnityStandardAssets.Utility.TimedObjectActivator/Entry)
extern const MethodInfo TimedObjectActivator_Deactivate_m562_MethodInfo = 
{
	"Deactivate"/* name */
	, (methodPointerType)&TimedObjectActivator_Deactivate_m562/* method */
	, &TimedObjectActivator_t205_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t217_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, TimedObjectActivator_t205_TimedObjectActivator_Deactivate_m562_ParameterInfos/* parameters */
	, 189/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 555/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Entry_t199_0_0_0;
static const ParameterInfo TimedObjectActivator_t205_TimedObjectActivator_ReloadLevel_m563_ParameterInfos[] = 
{
	{"entry", 0, 134218055, 0, &Entry_t199_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.IEnumerator UnityStandardAssets.Utility.TimedObjectActivator::ReloadLevel(UnityStandardAssets.Utility.TimedObjectActivator/Entry)
extern const MethodInfo TimedObjectActivator_ReloadLevel_m563_MethodInfo = 
{
	"ReloadLevel"/* name */
	, (methodPointerType)&TimedObjectActivator_ReloadLevel_m563/* method */
	, &TimedObjectActivator_t205_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t217_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, TimedObjectActivator_t205_TimedObjectActivator_ReloadLevel_m563_ParameterInfos/* parameters */
	, 190/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 556/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TimedObjectActivator_t205_MethodInfos[] =
{
	&TimedObjectActivator__ctor_m559_MethodInfo,
	&TimedObjectActivator_Awake_m560_MethodInfo,
	&TimedObjectActivator_Activate_m561_MethodInfo,
	&TimedObjectActivator_Deactivate_m562_MethodInfo,
	&TimedObjectActivator_ReloadLevel_m563_MethodInfo,
	NULL
};
static const Il2CppType* TimedObjectActivator_t205_il2cpp_TypeInfo__nestedTypes[6] =
{
	&Action_t198_0_0_0,
	&Entry_t199_0_0_0,
	&Entries_t201_0_0_0,
	&U3CActivateU3Ec__Iterator9_t202_0_0_0,
	&U3CDeactivateU3Ec__IteratorA_t203_0_0_0,
	&U3CReloadLevelU3Ec__IteratorB_t204_0_0_0,
};
static const Il2CppMethodReference TimedObjectActivator_t205_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool TimedObjectActivator_t205_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType TimedObjectActivator_t205_1_0_0;
struct TimedObjectActivator_t205;
const Il2CppTypeDefinitionMetadata TimedObjectActivator_t205_DefinitionMetadata = 
{
	NULL/* declaringType */
	, TimedObjectActivator_t205_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, TimedObjectActivator_t205_VTable/* vtableMethods */
	, TimedObjectActivator_t205_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 953/* fieldStart */

};
TypeInfo TimedObjectActivator_t205_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "TimedObjectActivator"/* name */
	, "UnityStandardAssets.Utility"/* namespaze */
	, TimedObjectActivator_t205_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TimedObjectActivator_t205_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TimedObjectActivator_t205_0_0_0/* byval_arg */
	, &TimedObjectActivator_t205_1_0_0/* this_arg */
	, &TimedObjectActivator_t205_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TimedObjectActivator_t205)/* instance_size */
	, sizeof (TimedObjectActivator_t205)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 6/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.Utility.TimedObjectDestructor
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Ti_6.h"
// Metadata Definition UnityStandardAssets.Utility.TimedObjectDestructor
extern TypeInfo TimedObjectDestructor_t206_il2cpp_TypeInfo;
// UnityStandardAssets.Utility.TimedObjectDestructor
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Ti_6MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.TimedObjectDestructor::.ctor()
extern const MethodInfo TimedObjectDestructor__ctor_m564_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TimedObjectDestructor__ctor_m564/* method */
	, &TimedObjectDestructor_t206_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 577/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.TimedObjectDestructor::Awake()
extern const MethodInfo TimedObjectDestructor_Awake_m565_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&TimedObjectDestructor_Awake_m565/* method */
	, &TimedObjectDestructor_t206_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 578/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.TimedObjectDestructor::DestroyNow()
extern const MethodInfo TimedObjectDestructor_DestroyNow_m566_MethodInfo = 
{
	"DestroyNow"/* name */
	, (methodPointerType)&TimedObjectDestructor_DestroyNow_m566/* method */
	, &TimedObjectDestructor_t206_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 579/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TimedObjectDestructor_t206_MethodInfos[] =
{
	&TimedObjectDestructor__ctor_m564_MethodInfo,
	&TimedObjectDestructor_Awake_m565_MethodInfo,
	&TimedObjectDestructor_DestroyNow_m566_MethodInfo,
	NULL
};
static const Il2CppMethodReference TimedObjectDestructor_t206_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool TimedObjectDestructor_t206_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType TimedObjectDestructor_t206_0_0_0;
extern const Il2CppType TimedObjectDestructor_t206_1_0_0;
struct TimedObjectDestructor_t206;
const Il2CppTypeDefinitionMetadata TimedObjectDestructor_t206_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, TimedObjectDestructor_t206_VTable/* vtableMethods */
	, TimedObjectDestructor_t206_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 954/* fieldStart */

};
TypeInfo TimedObjectDestructor_t206_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "TimedObjectDestructor"/* name */
	, "UnityStandardAssets.Utility"/* namespaze */
	, TimedObjectDestructor_t206_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TimedObjectDestructor_t206_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TimedObjectDestructor_t206_0_0_0/* byval_arg */
	, &TimedObjectDestructor_t206_1_0_0/* this_arg */
	, &TimedObjectDestructor_t206_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TimedObjectDestructor_t206)/* instance_size */
	, sizeof (TimedObjectDestructor_t206)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.Utility.WaypointCircuit/WaypointList
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Wa.h"
// Metadata Definition UnityStandardAssets.Utility.WaypointCircuit/WaypointList
extern TypeInfo WaypointList_t208_il2cpp_TypeInfo;
// UnityStandardAssets.Utility.WaypointCircuit/WaypointList
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_WaMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.WaypointCircuit/WaypointList::.ctor()
extern const MethodInfo WaypointList__ctor_m567_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&WaypointList__ctor_m567/* method */
	, &WaypointList_t208_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 592/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* WaypointList_t208_MethodInfos[] =
{
	&WaypointList__ctor_m567_MethodInfo,
	NULL
};
static const Il2CppMethodReference WaypointList_t208_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool WaypointList_t208_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType WaypointList_t208_0_0_0;
extern const Il2CppType WaypointList_t208_1_0_0;
extern TypeInfo WaypointCircuit_t207_il2cpp_TypeInfo;
extern const Il2CppType WaypointCircuit_t207_0_0_0;
struct WaypointList_t208;
const Il2CppTypeDefinitionMetadata WaypointList_t208_DefinitionMetadata = 
{
	&WaypointCircuit_t207_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, WaypointList_t208_VTable/* vtableMethods */
	, WaypointList_t208_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 956/* fieldStart */

};
TypeInfo WaypointList_t208_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "WaypointList"/* name */
	, ""/* namespaze */
	, WaypointList_t208_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &WaypointList_t208_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &WaypointList_t208_0_0_0/* byval_arg */
	, &WaypointList_t208_1_0_0/* this_arg */
	, &WaypointList_t208_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WaypointList_t208)/* instance_size */
	, sizeof (WaypointList_t208)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056770/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.Utility.WaypointCircuit/RoutePoint
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Wa_0.h"
// Metadata Definition UnityStandardAssets.Utility.WaypointCircuit/RoutePoint
extern TypeInfo RoutePoint_t209_il2cpp_TypeInfo;
// UnityStandardAssets.Utility.WaypointCircuit/RoutePoint
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Wa_0MethodDeclarations.h"
extern const Il2CppType Vector3_t4_0_0_0;
extern const Il2CppType Vector3_t4_0_0_0;
static const ParameterInfo RoutePoint_t209_RoutePoint__ctor_m568_ParameterInfos[] = 
{
	{"position", 0, 134218065, 0, &Vector3_t4_0_0_0},
	{"direction", 1, 134218066, 0, &Vector3_t4_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Vector3_t4_Vector3_t4 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.WaypointCircuit/RoutePoint::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern const MethodInfo RoutePoint__ctor_m568_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RoutePoint__ctor_m568/* method */
	, &RoutePoint_t209_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Vector3_t4_Vector3_t4/* invoker_method */
	, RoutePoint_t209_RoutePoint__ctor_m568_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 593/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* RoutePoint_t209_MethodInfos[] =
{
	&RoutePoint__ctor_m568_MethodInfo,
	NULL
};
extern const MethodInfo ValueType_Equals_m1076_MethodInfo;
extern const MethodInfo ValueType_GetHashCode_m1077_MethodInfo;
extern const MethodInfo ValueType_ToString_m1078_MethodInfo;
static const Il2CppMethodReference RoutePoint_t209_VTable[] =
{
	&ValueType_Equals_m1076_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&ValueType_GetHashCode_m1077_MethodInfo,
	&ValueType_ToString_m1078_MethodInfo,
};
static bool RoutePoint_t209_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType RoutePoint_t209_0_0_0;
extern const Il2CppType RoutePoint_t209_1_0_0;
extern const Il2CppType ValueType_t285_0_0_0;
const Il2CppTypeDefinitionMetadata RoutePoint_t209_DefinitionMetadata = 
{
	&WaypointCircuit_t207_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t285_0_0_0/* parent */
	, RoutePoint_t209_VTable/* vtableMethods */
	, RoutePoint_t209_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 958/* fieldStart */

};
TypeInfo RoutePoint_t209_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "RoutePoint"/* name */
	, ""/* namespaze */
	, RoutePoint_t209_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &RoutePoint_t209_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RoutePoint_t209_0_0_0/* byval_arg */
	, &RoutePoint_t209_1_0_0/* this_arg */
	, &RoutePoint_t209_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RoutePoint_t209)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (RoutePoint_t209)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(RoutePoint_t209 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048842/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.Utility.WaypointCircuit
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Wa_1.h"
// Metadata Definition UnityStandardAssets.Utility.WaypointCircuit
// UnityStandardAssets.Utility.WaypointCircuit
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Wa_1MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.WaypointCircuit::.ctor()
extern const MethodInfo WaypointCircuit__ctor_m569_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&WaypointCircuit__ctor_m569/* method */
	, &WaypointCircuit_t207_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 580/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityStandardAssets.Utility.WaypointCircuit::get_Length()
extern const MethodInfo WaypointCircuit_get_Length_m570_MethodInfo = 
{
	"get_Length"/* name */
	, (methodPointerType)&WaypointCircuit_get_Length_m570/* method */
	, &WaypointCircuit_t207_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 210/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 581/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo WaypointCircuit_t207_WaypointCircuit_set_Length_m571_ParameterInfos[] = 
{
	{"value", 0, 134218056, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.WaypointCircuit::set_Length(System.Single)
extern const MethodInfo WaypointCircuit_set_Length_m571_MethodInfo = 
{
	"set_Length"/* name */
	, (methodPointerType)&WaypointCircuit_set_Length_m571/* method */
	, &WaypointCircuit_t207_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Single_t254/* invoker_method */
	, WaypointCircuit_t207_WaypointCircuit_set_Length_m571_ParameterInfos/* parameters */
	, 211/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 582/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TransformU5BU5D_t141_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Transform[] UnityStandardAssets.Utility.WaypointCircuit::get_Waypoints()
extern const MethodInfo WaypointCircuit_get_Waypoints_m572_MethodInfo = 
{
	"get_Waypoints"/* name */
	, (methodPointerType)&WaypointCircuit_get_Waypoints_m572/* method */
	, &WaypointCircuit_t207_il2cpp_TypeInfo/* declaring_type */
	, &TransformU5BU5D_t141_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 583/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.WaypointCircuit::Awake()
extern const MethodInfo WaypointCircuit_Awake_m573_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&WaypointCircuit_Awake_m573/* method */
	, &WaypointCircuit_t207_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 584/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo WaypointCircuit_t207_WaypointCircuit_GetRoutePoint_m574_ParameterInfos[] = 
{
	{"dist", 0, 134218057, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_RoutePoint_t209_Single_t254 (const MethodInfo* method, void* obj, void** args);
// UnityStandardAssets.Utility.WaypointCircuit/RoutePoint UnityStandardAssets.Utility.WaypointCircuit::GetRoutePoint(System.Single)
extern const MethodInfo WaypointCircuit_GetRoutePoint_m574_MethodInfo = 
{
	"GetRoutePoint"/* name */
	, (methodPointerType)&WaypointCircuit_GetRoutePoint_m574/* method */
	, &WaypointCircuit_t207_il2cpp_TypeInfo/* declaring_type */
	, &RoutePoint_t209_0_0_0/* return_type */
	, RuntimeInvoker_RoutePoint_t209_Single_t254/* invoker_method */
	, WaypointCircuit_t207_WaypointCircuit_GetRoutePoint_m574_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 585/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo WaypointCircuit_t207_WaypointCircuit_GetRoutePosition_m575_ParameterInfos[] = 
{
	{"dist", 0, 134218058, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Vector3_t4_Single_t254 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector3 UnityStandardAssets.Utility.WaypointCircuit::GetRoutePosition(System.Single)
extern const MethodInfo WaypointCircuit_GetRoutePosition_m575_MethodInfo = 
{
	"GetRoutePosition"/* name */
	, (methodPointerType)&WaypointCircuit_GetRoutePosition_m575/* method */
	, &WaypointCircuit_t207_il2cpp_TypeInfo/* declaring_type */
	, &Vector3_t4_0_0_0/* return_type */
	, RuntimeInvoker_Vector3_t4_Single_t254/* invoker_method */
	, WaypointCircuit_t207_WaypointCircuit_GetRoutePosition_m575_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 586/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector3_t4_0_0_0;
extern const Il2CppType Vector3_t4_0_0_0;
extern const Il2CppType Vector3_t4_0_0_0;
extern const Il2CppType Vector3_t4_0_0_0;
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo WaypointCircuit_t207_WaypointCircuit_CatmullRom_m576_ParameterInfos[] = 
{
	{"p0", 0, 134218059, 0, &Vector3_t4_0_0_0},
	{"p1", 1, 134218060, 0, &Vector3_t4_0_0_0},
	{"p2", 2, 134218061, 0, &Vector3_t4_0_0_0},
	{"p3", 3, 134218062, 0, &Vector3_t4_0_0_0},
	{"i", 4, 134218063, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Vector3_t4_Vector3_t4_Vector3_t4_Vector3_t4_Vector3_t4_Single_t254 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector3 UnityStandardAssets.Utility.WaypointCircuit::CatmullRom(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern const MethodInfo WaypointCircuit_CatmullRom_m576_MethodInfo = 
{
	"CatmullRom"/* name */
	, (methodPointerType)&WaypointCircuit_CatmullRom_m576/* method */
	, &WaypointCircuit_t207_il2cpp_TypeInfo/* declaring_type */
	, &Vector3_t4_0_0_0/* return_type */
	, RuntimeInvoker_Vector3_t4_Vector3_t4_Vector3_t4_Vector3_t4_Vector3_t4_Single_t254/* invoker_method */
	, WaypointCircuit_t207_WaypointCircuit_CatmullRom_m576_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 587/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.WaypointCircuit::CachePositionsAndDistances()
extern const MethodInfo WaypointCircuit_CachePositionsAndDistances_m577_MethodInfo = 
{
	"CachePositionsAndDistances"/* name */
	, (methodPointerType)&WaypointCircuit_CachePositionsAndDistances_m577/* method */
	, &WaypointCircuit_t207_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 588/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.WaypointCircuit::OnDrawGizmos()
extern const MethodInfo WaypointCircuit_OnDrawGizmos_m578_MethodInfo = 
{
	"OnDrawGizmos"/* name */
	, (methodPointerType)&WaypointCircuit_OnDrawGizmos_m578/* method */
	, &WaypointCircuit_t207_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 589/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.WaypointCircuit::OnDrawGizmosSelected()
extern const MethodInfo WaypointCircuit_OnDrawGizmosSelected_m579_MethodInfo = 
{
	"OnDrawGizmosSelected"/* name */
	, (methodPointerType)&WaypointCircuit_OnDrawGizmosSelected_m579/* method */
	, &WaypointCircuit_t207_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 590/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo WaypointCircuit_t207_WaypointCircuit_DrawGizmos_m580_ParameterInfos[] = 
{
	{"selected", 0, 134218064, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.WaypointCircuit::DrawGizmos(System.Boolean)
extern const MethodInfo WaypointCircuit_DrawGizmos_m580_MethodInfo = 
{
	"DrawGizmos"/* name */
	, (methodPointerType)&WaypointCircuit_DrawGizmos_m580/* method */
	, &WaypointCircuit_t207_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274/* invoker_method */
	, WaypointCircuit_t207_WaypointCircuit_DrawGizmos_m580_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 591/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* WaypointCircuit_t207_MethodInfos[] =
{
	&WaypointCircuit__ctor_m569_MethodInfo,
	&WaypointCircuit_get_Length_m570_MethodInfo,
	&WaypointCircuit_set_Length_m571_MethodInfo,
	&WaypointCircuit_get_Waypoints_m572_MethodInfo,
	&WaypointCircuit_Awake_m573_MethodInfo,
	&WaypointCircuit_GetRoutePoint_m574_MethodInfo,
	&WaypointCircuit_GetRoutePosition_m575_MethodInfo,
	&WaypointCircuit_CatmullRom_m576_MethodInfo,
	&WaypointCircuit_CachePositionsAndDistances_m577_MethodInfo,
	&WaypointCircuit_OnDrawGizmos_m578_MethodInfo,
	&WaypointCircuit_OnDrawGizmosSelected_m579_MethodInfo,
	&WaypointCircuit_DrawGizmos_m580_MethodInfo,
	NULL
};
extern const MethodInfo WaypointCircuit_get_Length_m570_MethodInfo;
extern const MethodInfo WaypointCircuit_set_Length_m571_MethodInfo;
static const PropertyInfo WaypointCircuit_t207____Length_PropertyInfo = 
{
	&WaypointCircuit_t207_il2cpp_TypeInfo/* parent */
	, "Length"/* name */
	, &WaypointCircuit_get_Length_m570_MethodInfo/* get */
	, &WaypointCircuit_set_Length_m571_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo WaypointCircuit_get_Waypoints_m572_MethodInfo;
static const PropertyInfo WaypointCircuit_t207____Waypoints_PropertyInfo = 
{
	&WaypointCircuit_t207_il2cpp_TypeInfo/* parent */
	, "Waypoints"/* name */
	, &WaypointCircuit_get_Waypoints_m572_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* WaypointCircuit_t207_PropertyInfos[] =
{
	&WaypointCircuit_t207____Length_PropertyInfo,
	&WaypointCircuit_t207____Waypoints_PropertyInfo,
	NULL
};
static const Il2CppType* WaypointCircuit_t207_il2cpp_TypeInfo__nestedTypes[2] =
{
	&WaypointList_t208_0_0_0,
	&RoutePoint_t209_0_0_0,
};
static const Il2CppMethodReference WaypointCircuit_t207_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool WaypointCircuit_t207_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType WaypointCircuit_t207_1_0_0;
struct WaypointCircuit_t207;
const Il2CppTypeDefinitionMetadata WaypointCircuit_t207_DefinitionMetadata = 
{
	NULL/* declaringType */
	, WaypointCircuit_t207_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, WaypointCircuit_t207_VTable/* vtableMethods */
	, WaypointCircuit_t207_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 960/* fieldStart */

};
TypeInfo WaypointCircuit_t207_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "WaypointCircuit"/* name */
	, "UnityStandardAssets.Utility"/* namespaze */
	, WaypointCircuit_t207_MethodInfos/* methods */
	, WaypointCircuit_t207_PropertyInfos/* properties */
	, NULL/* events */
	, &WaypointCircuit_t207_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &WaypointCircuit_t207_0_0_0/* byval_arg */
	, &WaypointCircuit_t207_1_0_0/* this_arg */
	, &WaypointCircuit_t207_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WaypointCircuit_t207)/* instance_size */
	, sizeof (WaypointCircuit_t207)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 12/* method_count */
	, 2/* property_count */
	, 16/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityStandardAssets.Utility.WaypointProgressTracker/ProgressStyle
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Wa_2.h"
// Metadata Definition UnityStandardAssets.Utility.WaypointProgressTracker/ProgressStyle
extern TypeInfo ProgressStyle_t212_il2cpp_TypeInfo;
// UnityStandardAssets.Utility.WaypointProgressTracker/ProgressStyle
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Wa_2MethodDeclarations.h"
static const MethodInfo* ProgressStyle_t212_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference ProgressStyle_t212_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool ProgressStyle_t212_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ProgressStyle_t212_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType ProgressStyle_t212_0_0_0;
extern const Il2CppType ProgressStyle_t212_1_0_0;
extern TypeInfo WaypointProgressTracker_t213_il2cpp_TypeInfo;
extern const Il2CppType WaypointProgressTracker_t213_0_0_0;
const Il2CppTypeDefinitionMetadata ProgressStyle_t212_DefinitionMetadata = 
{
	&WaypointProgressTracker_t213_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ProgressStyle_t212_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, ProgressStyle_t212_VTable/* vtableMethods */
	, ProgressStyle_t212_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 976/* fieldStart */

};
TypeInfo ProgressStyle_t212_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "ProgressStyle"/* name */
	, ""/* namespaze */
	, ProgressStyle_t212_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ProgressStyle_t212_0_0_0/* byval_arg */
	, &ProgressStyle_t212_1_0_0/* this_arg */
	, &ProgressStyle_t212_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ProgressStyle_t212)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ProgressStyle_t212)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityStandardAssets.Utility.WaypointProgressTracker
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Wa_3.h"
// Metadata Definition UnityStandardAssets.Utility.WaypointProgressTracker
// UnityStandardAssets.Utility.WaypointProgressTracker
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Wa_3MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.WaypointProgressTracker::.ctor()
extern const MethodInfo WaypointProgressTracker__ctor_m581_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&WaypointProgressTracker__ctor_m581/* method */
	, &WaypointProgressTracker_t213_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 594/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_RoutePoint_t209 (const MethodInfo* method, void* obj, void** args);
// UnityStandardAssets.Utility.WaypointCircuit/RoutePoint UnityStandardAssets.Utility.WaypointProgressTracker::get_targetPoint()
extern const MethodInfo WaypointProgressTracker_get_targetPoint_m582_MethodInfo = 
{
	"get_targetPoint"/* name */
	, (methodPointerType)&WaypointProgressTracker_get_targetPoint_m582/* method */
	, &WaypointProgressTracker_t213_il2cpp_TypeInfo/* declaring_type */
	, &RoutePoint_t209_0_0_0/* return_type */
	, RuntimeInvoker_RoutePoint_t209/* invoker_method */
	, NULL/* parameters */
	, 222/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 595/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RoutePoint_t209_0_0_0;
static const ParameterInfo WaypointProgressTracker_t213_WaypointProgressTracker_set_targetPoint_m583_ParameterInfos[] = 
{
	{"value", 0, 134218067, 0, &RoutePoint_t209_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_RoutePoint_t209 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.WaypointProgressTracker::set_targetPoint(UnityStandardAssets.Utility.WaypointCircuit/RoutePoint)
extern const MethodInfo WaypointProgressTracker_set_targetPoint_m583_MethodInfo = 
{
	"set_targetPoint"/* name */
	, (methodPointerType)&WaypointProgressTracker_set_targetPoint_m583/* method */
	, &WaypointProgressTracker_t213_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_RoutePoint_t209/* invoker_method */
	, WaypointProgressTracker_t213_WaypointProgressTracker_set_targetPoint_m583_ParameterInfos/* parameters */
	, 223/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 596/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_RoutePoint_t209 (const MethodInfo* method, void* obj, void** args);
// UnityStandardAssets.Utility.WaypointCircuit/RoutePoint UnityStandardAssets.Utility.WaypointProgressTracker::get_speedPoint()
extern const MethodInfo WaypointProgressTracker_get_speedPoint_m584_MethodInfo = 
{
	"get_speedPoint"/* name */
	, (methodPointerType)&WaypointProgressTracker_get_speedPoint_m584/* method */
	, &WaypointProgressTracker_t213_il2cpp_TypeInfo/* declaring_type */
	, &RoutePoint_t209_0_0_0/* return_type */
	, RuntimeInvoker_RoutePoint_t209/* invoker_method */
	, NULL/* parameters */
	, 224/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 597/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RoutePoint_t209_0_0_0;
static const ParameterInfo WaypointProgressTracker_t213_WaypointProgressTracker_set_speedPoint_m585_ParameterInfos[] = 
{
	{"value", 0, 134218068, 0, &RoutePoint_t209_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_RoutePoint_t209 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.WaypointProgressTracker::set_speedPoint(UnityStandardAssets.Utility.WaypointCircuit/RoutePoint)
extern const MethodInfo WaypointProgressTracker_set_speedPoint_m585_MethodInfo = 
{
	"set_speedPoint"/* name */
	, (methodPointerType)&WaypointProgressTracker_set_speedPoint_m585/* method */
	, &WaypointProgressTracker_t213_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_RoutePoint_t209/* invoker_method */
	, WaypointProgressTracker_t213_WaypointProgressTracker_set_speedPoint_m585_ParameterInfos/* parameters */
	, 225/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 598/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_RoutePoint_t209 (const MethodInfo* method, void* obj, void** args);
// UnityStandardAssets.Utility.WaypointCircuit/RoutePoint UnityStandardAssets.Utility.WaypointProgressTracker::get_progressPoint()
extern const MethodInfo WaypointProgressTracker_get_progressPoint_m586_MethodInfo = 
{
	"get_progressPoint"/* name */
	, (methodPointerType)&WaypointProgressTracker_get_progressPoint_m586/* method */
	, &WaypointProgressTracker_t213_il2cpp_TypeInfo/* declaring_type */
	, &RoutePoint_t209_0_0_0/* return_type */
	, RuntimeInvoker_RoutePoint_t209/* invoker_method */
	, NULL/* parameters */
	, 226/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 599/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RoutePoint_t209_0_0_0;
static const ParameterInfo WaypointProgressTracker_t213_WaypointProgressTracker_set_progressPoint_m587_ParameterInfos[] = 
{
	{"value", 0, 134218069, 0, &RoutePoint_t209_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_RoutePoint_t209 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.WaypointProgressTracker::set_progressPoint(UnityStandardAssets.Utility.WaypointCircuit/RoutePoint)
extern const MethodInfo WaypointProgressTracker_set_progressPoint_m587_MethodInfo = 
{
	"set_progressPoint"/* name */
	, (methodPointerType)&WaypointProgressTracker_set_progressPoint_m587/* method */
	, &WaypointProgressTracker_t213_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_RoutePoint_t209/* invoker_method */
	, WaypointProgressTracker_t213_WaypointProgressTracker_set_progressPoint_m587_ParameterInfos/* parameters */
	, 227/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 600/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.WaypointProgressTracker::Start()
extern const MethodInfo WaypointProgressTracker_Start_m588_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&WaypointProgressTracker_Start_m588/* method */
	, &WaypointProgressTracker_t213_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 601/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.WaypointProgressTracker::Reset()
extern const MethodInfo WaypointProgressTracker_Reset_m589_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&WaypointProgressTracker_Reset_m589/* method */
	, &WaypointProgressTracker_t213_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 602/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.WaypointProgressTracker::Update()
extern const MethodInfo WaypointProgressTracker_Update_m590_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&WaypointProgressTracker_Update_m590/* method */
	, &WaypointProgressTracker_t213_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 603/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityStandardAssets.Utility.WaypointProgressTracker::OnDrawGizmos()
extern const MethodInfo WaypointProgressTracker_OnDrawGizmos_m591_MethodInfo = 
{
	"OnDrawGizmos"/* name */
	, (methodPointerType)&WaypointProgressTracker_OnDrawGizmos_m591/* method */
	, &WaypointProgressTracker_t213_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 604/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* WaypointProgressTracker_t213_MethodInfos[] =
{
	&WaypointProgressTracker__ctor_m581_MethodInfo,
	&WaypointProgressTracker_get_targetPoint_m582_MethodInfo,
	&WaypointProgressTracker_set_targetPoint_m583_MethodInfo,
	&WaypointProgressTracker_get_speedPoint_m584_MethodInfo,
	&WaypointProgressTracker_set_speedPoint_m585_MethodInfo,
	&WaypointProgressTracker_get_progressPoint_m586_MethodInfo,
	&WaypointProgressTracker_set_progressPoint_m587_MethodInfo,
	&WaypointProgressTracker_Start_m588_MethodInfo,
	&WaypointProgressTracker_Reset_m589_MethodInfo,
	&WaypointProgressTracker_Update_m590_MethodInfo,
	&WaypointProgressTracker_OnDrawGizmos_m591_MethodInfo,
	NULL
};
extern const MethodInfo WaypointProgressTracker_get_targetPoint_m582_MethodInfo;
extern const MethodInfo WaypointProgressTracker_set_targetPoint_m583_MethodInfo;
static const PropertyInfo WaypointProgressTracker_t213____targetPoint_PropertyInfo = 
{
	&WaypointProgressTracker_t213_il2cpp_TypeInfo/* parent */
	, "targetPoint"/* name */
	, &WaypointProgressTracker_get_targetPoint_m582_MethodInfo/* get */
	, &WaypointProgressTracker_set_targetPoint_m583_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo WaypointProgressTracker_get_speedPoint_m584_MethodInfo;
extern const MethodInfo WaypointProgressTracker_set_speedPoint_m585_MethodInfo;
static const PropertyInfo WaypointProgressTracker_t213____speedPoint_PropertyInfo = 
{
	&WaypointProgressTracker_t213_il2cpp_TypeInfo/* parent */
	, "speedPoint"/* name */
	, &WaypointProgressTracker_get_speedPoint_m584_MethodInfo/* get */
	, &WaypointProgressTracker_set_speedPoint_m585_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo WaypointProgressTracker_get_progressPoint_m586_MethodInfo;
extern const MethodInfo WaypointProgressTracker_set_progressPoint_m587_MethodInfo;
static const PropertyInfo WaypointProgressTracker_t213____progressPoint_PropertyInfo = 
{
	&WaypointProgressTracker_t213_il2cpp_TypeInfo/* parent */
	, "progressPoint"/* name */
	, &WaypointProgressTracker_get_progressPoint_m586_MethodInfo/* get */
	, &WaypointProgressTracker_set_progressPoint_m587_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* WaypointProgressTracker_t213_PropertyInfos[] =
{
	&WaypointProgressTracker_t213____targetPoint_PropertyInfo,
	&WaypointProgressTracker_t213____speedPoint_PropertyInfo,
	&WaypointProgressTracker_t213____progressPoint_PropertyInfo,
	NULL
};
static const Il2CppType* WaypointProgressTracker_t213_il2cpp_TypeInfo__nestedTypes[1] =
{
	&ProgressStyle_t212_0_0_0,
};
static const Il2CppMethodReference WaypointProgressTracker_t213_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool WaypointProgressTracker_t213_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern const Il2CppType WaypointProgressTracker_t213_1_0_0;
struct WaypointProgressTracker_t213;
const Il2CppTypeDefinitionMetadata WaypointProgressTracker_t213_DefinitionMetadata = 
{
	NULL/* declaringType */
	, WaypointProgressTracker_t213_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, WaypointProgressTracker_t213_VTable/* vtableMethods */
	, WaypointProgressTracker_t213_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 979/* fieldStart */

};
TypeInfo WaypointProgressTracker_t213_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "WaypointProgressTracker"/* name */
	, "UnityStandardAssets.Utility"/* namespaze */
	, WaypointProgressTracker_t213_MethodInfos/* methods */
	, WaypointProgressTracker_t213_PropertyInfos/* properties */
	, NULL/* events */
	, &WaypointProgressTracker_t213_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &WaypointProgressTracker_t213_0_0_0/* byval_arg */
	, &WaypointProgressTracker_t213_1_0_0/* this_arg */
	, &WaypointProgressTracker_t213_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WaypointProgressTracker_t213)/* instance_size */
	, sizeof (WaypointProgressTracker_t213)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 3/* property_count */
	, 15/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
