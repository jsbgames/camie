﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets.Utility.PlatformSpecificContent
struct PlatformSpecificContent_t194;

// System.Void UnityStandardAssets.Utility.PlatformSpecificContent::.ctor()
extern "C" void PlatformSpecificContent__ctor_m526 (PlatformSpecificContent_t194 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Utility.PlatformSpecificContent::OnEnable()
extern "C" void PlatformSpecificContent_OnEnable_m527 (PlatformSpecificContent_t194 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Utility.PlatformSpecificContent::CheckEnableContent()
extern "C" void PlatformSpecificContent_CheckEnableContent_m528 (PlatformSpecificContent_t194 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Utility.PlatformSpecificContent::EnableContent(System.Boolean)
extern "C" void PlatformSpecificContent_EnableContent_m529 (PlatformSpecificContent_t194 * __this, bool ___enabled, const MethodInfo* method) IL2CPP_METHOD_ATTR;
