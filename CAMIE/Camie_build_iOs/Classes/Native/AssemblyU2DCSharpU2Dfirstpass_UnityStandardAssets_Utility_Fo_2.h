﻿#pragma once
#include <stdint.h>
// UnityEngine.Transform
struct Transform_t1;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// UnityStandardAssets.Utility.FollowTarget
struct  FollowTarget_t183  : public MonoBehaviour_t3
{
	// UnityEngine.Transform UnityStandardAssets.Utility.FollowTarget::target
	Transform_t1 * ___target_2;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.FollowTarget::offset
	Vector3_t4  ___offset_3;
};
