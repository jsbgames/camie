﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// TestReporter
struct TestReporter_t308;

// System.Void TestReporter::.ctor()
extern "C" void TestReporter__ctor_m1136 (TestReporter_t308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestReporter::Start()
extern "C" void TestReporter_Start_m1137 (TestReporter_t308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestReporter::threadLogTest()
extern "C" void TestReporter_threadLogTest_m1138 (TestReporter_t308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestReporter::Update()
extern "C" void TestReporter_Update_m1139 (TestReporter_t308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestReporter::OnGUI()
extern "C" void TestReporter_OnGUI_m1140 (TestReporter_t308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
