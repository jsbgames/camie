﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Reporter/Log
struct Log_t291;

// System.Void Reporter/Log::.ctor()
extern "C" void Log__ctor_m1082 (Log_t291 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Reporter/Log Reporter/Log::CreateCopy()
extern "C" Log_t291 * Log_CreateCopy_m1083 (Log_t291 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Reporter/Log::GetMemoryUsage()
extern "C" float Log_GetMemoryUsage_m1084 (Log_t291 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
