﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.WaitForSeconds
struct WaitForSeconds_t257;
struct WaitForSeconds_t257_marshaled;

// System.Void UnityEngine.WaitForSeconds::.ctor(System.Single)
extern "C" void WaitForSeconds__ctor_m1002 (WaitForSeconds_t257 * __this, float ___seconds, const MethodInfo* method) IL2CPP_METHOD_ATTR;
void WaitForSeconds_t257_marshal(const WaitForSeconds_t257& unmarshaled, WaitForSeconds_t257_marshaled& marshaled);
void WaitForSeconds_t257_marshal_back(const WaitForSeconds_t257_marshaled& marshaled, WaitForSeconds_t257& unmarshaled);
void WaitForSeconds_t257_marshal_cleanup(WaitForSeconds_t257_marshaled& marshaled);
