﻿#pragma once
#include <stdint.h>
// System.Collections.CaseInsensitiveComparer
struct CaseInsensitiveComparer_t1440;
// System.Globalization.CultureInfo
struct CultureInfo_t1068;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.CaseInsensitiveComparer
struct  CaseInsensitiveComparer_t1440  : public Object_t
{
	// System.Globalization.CultureInfo System.Collections.CaseInsensitiveComparer::culture
	CultureInfo_t1068 * ___culture_2;
};
struct CaseInsensitiveComparer_t1440_StaticFields{
	// System.Collections.CaseInsensitiveComparer System.Collections.CaseInsensitiveComparer::defaultComparer
	CaseInsensitiveComparer_t1440 * ___defaultComparer_0;
	// System.Collections.CaseInsensitiveComparer System.Collections.CaseInsensitiveComparer::defaultInvariantComparer
	CaseInsensitiveComparer_t1440 * ___defaultInvariantComparer_1;
};
