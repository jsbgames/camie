﻿#pragma once
#include <stdint.h>
// System.String[]
struct StringU5BU5D_t243;
// System.Collections.Specialized.NameObjectCollectionBase
#include "System_System_Collections_Specialized_NameObjectCollectionBa_2.h"
// System.Collections.Specialized.NameValueCollection
struct  NameValueCollection_t1274  : public NameObjectCollectionBase_t1268
{
	// System.String[] System.Collections.Specialized.NameValueCollection::cachedAllKeys
	StringU5BU5D_t243* ___cachedAllKeys_10;
	// System.String[] System.Collections.Specialized.NameValueCollection::cachedAll
	StringU5BU5D_t243* ___cachedAll_11;
};
