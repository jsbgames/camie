﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.Canvas>
struct Enumerator_t3113;
// System.Object
struct Object_t;
// UnityEngine.Canvas
struct Canvas_t414;
// System.Collections.Generic.List`1<UnityEngine.Canvas>
struct List_1_t653;

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Canvas>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_4MethodDeclarations.h"
#define Enumerator__ctor_m17860(__this, ___l, method) (( void (*) (Enumerator_t3113 *, List_1_t653 *, const MethodInfo*))Enumerator__ctor_m13983_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Canvas>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m17861(__this, method) (( Object_t * (*) (Enumerator_t3113 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m13984_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Canvas>::Dispose()
#define Enumerator_Dispose_m17862(__this, method) (( void (*) (Enumerator_t3113 *, const MethodInfo*))Enumerator_Dispose_m13985_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Canvas>::VerifyState()
#define Enumerator_VerifyState_m17863(__this, method) (( void (*) (Enumerator_t3113 *, const MethodInfo*))Enumerator_VerifyState_m13986_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Canvas>::MoveNext()
#define Enumerator_MoveNext_m17864(__this, method) (( bool (*) (Enumerator_t3113 *, const MethodInfo*))Enumerator_MoveNext_m13987_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Canvas>::get_Current()
#define Enumerator_get_Current_m17865(__this, method) (( Canvas_t414 * (*) (Enumerator_t3113 *, const MethodInfo*))Enumerator_get_Current_m13988_gshared)(__this, method)
