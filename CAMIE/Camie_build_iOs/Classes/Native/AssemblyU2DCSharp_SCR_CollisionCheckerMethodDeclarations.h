﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SCR_CollisionChecker
struct SCR_CollisionChecker_t349;
// UnityEngine.Collision
struct Collision_t146;

// System.Void SCR_CollisionChecker::.ctor()
extern "C" void SCR_CollisionChecker__ctor_m1270 (SCR_CollisionChecker_t349 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_CollisionChecker::Start()
extern "C" void SCR_CollisionChecker_Start_m1271 (SCR_CollisionChecker_t349 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_CollisionChecker::Update()
extern "C" void SCR_CollisionChecker_Update_m1272 (SCR_CollisionChecker_t349 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_CollisionChecker::OnCollisionEnter(UnityEngine.Collision)
extern "C" void SCR_CollisionChecker_OnCollisionEnter_m1273 (SCR_CollisionChecker_t349 * __this, Collision_t146 * ___info, const MethodInfo* method) IL2CPP_METHOD_ATTR;
