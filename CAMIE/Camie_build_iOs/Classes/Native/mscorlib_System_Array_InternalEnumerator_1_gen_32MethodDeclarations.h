﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>
struct InternalEnumerator_1_t2934;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.EventSystems.RaycastResult
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResult.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m15256_gshared (InternalEnumerator_1_t2934 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m15256(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2934 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m15256_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15257_gshared (InternalEnumerator_1_t2934 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15257(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2934 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15257_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m15258_gshared (InternalEnumerator_1_t2934 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m15258(__this, method) (( void (*) (InternalEnumerator_1_t2934 *, const MethodInfo*))InternalEnumerator_1_Dispose_m15258_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m15259_gshared (InternalEnumerator_1_t2934 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m15259(__this, method) (( bool (*) (InternalEnumerator_1_t2934 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m15259_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::get_Current()
extern "C" RaycastResult_t486  InternalEnumerator_1_get_Current_m15260_gshared (InternalEnumerator_1_t2934 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m15260(__this, method) (( RaycastResult_t486  (*) (InternalEnumerator_1_t2934 *, const MethodInfo*))InternalEnumerator_1_get_Current_m15260_gshared)(__this, method)
