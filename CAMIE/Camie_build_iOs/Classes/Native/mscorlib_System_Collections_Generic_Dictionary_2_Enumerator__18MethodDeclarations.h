﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>
struct Enumerator_t3135;
// System.Object
struct Object_t;
// UnityEngine.UI.ICanvasElement
struct ICanvasElement_t646;
// System.Collections.Generic.Dictionary`2<UnityEngine.UI.ICanvasElement,System.Int32>
struct Dictionary_2_t678;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.ICanvasElement,System.Int32>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_20.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__9MethodDeclarations.h"
#define Enumerator__ctor_m18214(__this, ___dictionary, method) (( void (*) (Enumerator_t3135 *, Dictionary_2_t678 *, const MethodInfo*))Enumerator__ctor_m15593_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m18215(__this, method) (( Object_t * (*) (Enumerator_t3135 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15594_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m18216(__this, method) (( DictionaryEntry_t1430  (*) (Enumerator_t3135 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15595_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m18217(__this, method) (( Object_t * (*) (Enumerator_t3135 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15596_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m18218(__this, method) (( Object_t * (*) (Enumerator_t3135 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15597_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::MoveNext()
#define Enumerator_MoveNext_m18219(__this, method) (( bool (*) (Enumerator_t3135 *, const MethodInfo*))Enumerator_MoveNext_m15598_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::get_Current()
#define Enumerator_get_Current_m18220(__this, method) (( KeyValuePair_2_t3132  (*) (Enumerator_t3135 *, const MethodInfo*))Enumerator_get_Current_m15599_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m18221(__this, method) (( Object_t * (*) (Enumerator_t3135 *, const MethodInfo*))Enumerator_get_CurrentKey_m15600_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m18222(__this, method) (( int32_t (*) (Enumerator_t3135 *, const MethodInfo*))Enumerator_get_CurrentValue_m15601_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::VerifyState()
#define Enumerator_VerifyState_m18223(__this, method) (( void (*) (Enumerator_t3135 *, const MethodInfo*))Enumerator_VerifyState_m15602_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m18224(__this, method) (( void (*) (Enumerator_t3135 *, const MethodInfo*))Enumerator_VerifyCurrent_m15603_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::Dispose()
#define Enumerator_Dispose_m18225(__this, method) (( void (*) (Enumerator_t3135 *, const MethodInfo*))Enumerator_Dispose_m15604_gshared)(__this, method)
