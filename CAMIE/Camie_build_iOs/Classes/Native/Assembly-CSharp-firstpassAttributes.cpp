﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilit.h"
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilitMethodDeclarations.h"
extern TypeInfo* RuntimeCompatibilityAttribute_t258_il2cpp_TypeInfo_var;
void g_AssemblyU2DCSharpU2Dfirstpass_Assembly_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RuntimeCompatibilityAttribute_t258_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(107);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RuntimeCompatibilityAttribute_t258 * tmp;
		tmp = (RuntimeCompatibilityAttribute_t258 *)il2cpp_codegen_object_new (RuntimeCompatibilityAttribute_t258_il2cpp_TypeInfo_var);
		RuntimeCompatibilityAttribute__ctor_m1021(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m1022(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.RequireComponent
#include "UnityEngine_UnityEngine_RequireComponent.h"
// UnityEngine.RequireComponent
#include "UnityEngine_UnityEngine_RequireComponentMethodDeclarations.h"
// UnityStandardAssets._2D.PlatformerCharacter2D
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets__2D_Platfo_0.h"
extern const Il2CppType* PlatformerCharacter2D_t7_0_0_0_var;
extern TypeInfo* RequireComponent_t259_il2cpp_TypeInfo_var;
void Platformer2DUserControl_t8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PlatformerCharacter2D_t7_0_0_0_var = il2cpp_codegen_type_from_index(2);
		RequireComponent_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(108);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RequireComponent_t259 * tmp;
		tmp = (RequireComponent_t259 *)il2cpp_codegen_object_new (RequireComponent_t259_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m1023(tmp, il2cpp_codegen_type_get_object(PlatformerCharacter2D_t7_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.SerializeField
#include "UnityEngine_UnityEngine_SerializeField.h"
// UnityEngine.SerializeField
#include "UnityEngine_UnityEngine_SerializeFieldMethodDeclarations.h"
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void PlatformerCharacter2D_t7_CustomAttributesCacheGenerator_m_MaxSpeed(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void PlatformerCharacter2D_t7_CustomAttributesCacheGenerator_m_JumpForce(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.RangeAttribute
#include "UnityEngine_UnityEngine_RangeAttribute.h"
// UnityEngine.RangeAttribute
#include "UnityEngine_UnityEngine_RangeAttributeMethodDeclarations.h"
extern TypeInfo* RangeAttribute_t261_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void PlatformerCharacter2D_t7_CustomAttributesCacheGenerator_m_CrouchSpeed(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RangeAttribute_t261_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(110);
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RangeAttribute_t261 * tmp;
		tmp = (RangeAttribute_t261 *)il2cpp_codegen_object_new (RangeAttribute_t261_il2cpp_TypeInfo_var);
		RangeAttribute__ctor_m1025(tmp, 0.0f, 1.0f, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void PlatformerCharacter2D_t7_CustomAttributesCacheGenerator_m_AirControl(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void PlatformerCharacter2D_t7_CustomAttributesCacheGenerator_m_WhatIsGround(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void AbstractTargetFollower_t15_CustomAttributesCacheGenerator_m_Target(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void AbstractTargetFollower_t15_CustomAttributesCacheGenerator_m_AutoTargetPlayer(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void AbstractTargetFollower_t15_CustomAttributesCacheGenerator_m_UpdateType(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.ExecuteInEditMode
#include "UnityEngine_UnityEngine_ExecuteInEditMode.h"
// UnityEngine.ExecuteInEditMode
#include "UnityEngine_UnityEngine_ExecuteInEditModeMethodDeclarations.h"
extern TypeInfo* ExecuteInEditMode_t262_il2cpp_TypeInfo_var;
void AutoCam_t16_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExecuteInEditMode_t262_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(111);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExecuteInEditMode_t262 * tmp;
		tmp = (ExecuteInEditMode_t262 *)il2cpp_codegen_object_new (ExecuteInEditMode_t262_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m1027(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void AutoCam_t16_CustomAttributesCacheGenerator_m_MoveSpeed(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void AutoCam_t16_CustomAttributesCacheGenerator_m_TurnSpeed(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void AutoCam_t16_CustomAttributesCacheGenerator_m_RollSpeed(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void AutoCam_t16_CustomAttributesCacheGenerator_m_FollowVelocity(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void AutoCam_t16_CustomAttributesCacheGenerator_m_FollowTilt(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void AutoCam_t16_CustomAttributesCacheGenerator_m_SpinTurnLimit(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void AutoCam_t16_CustomAttributesCacheGenerator_m_TargetVelocityLowerLimit(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void AutoCam_t16_CustomAttributesCacheGenerator_m_SmoothTurnTime(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void FreeLookCam_t18_CustomAttributesCacheGenerator_m_MoveSpeed(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* RangeAttribute_t261_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void FreeLookCam_t18_CustomAttributesCacheGenerator_m_TurnSpeed(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RangeAttribute_t261_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(110);
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RangeAttribute_t261 * tmp;
		tmp = (RangeAttribute_t261 *)il2cpp_codegen_object_new (RangeAttribute_t261_il2cpp_TypeInfo_var);
		RangeAttribute__ctor_m1025(tmp, 0.0f, 10.0f, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void FreeLookCam_t18_CustomAttributesCacheGenerator_m_TurnSmoothing(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void FreeLookCam_t18_CustomAttributesCacheGenerator_m_TiltMax(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void FreeLookCam_t18_CustomAttributesCacheGenerator_m_TiltMin(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void FreeLookCam_t18_CustomAttributesCacheGenerator_m_LockCursor(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void FreeLookCam_t18_CustomAttributesCacheGenerator_m_VerticalAutoReturn(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void HandHeldCam_t20_CustomAttributesCacheGenerator_m_SwaySpeed(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void HandHeldCam_t20_CustomAttributesCacheGenerator_m_BaseSwayAmount(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void HandHeldCam_t20_CustomAttributesCacheGenerator_m_TrackingSwayAmount(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* RangeAttribute_t261_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void HandHeldCam_t20_CustomAttributesCacheGenerator_m_TrackingBias(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RangeAttribute_t261_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(110);
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RangeAttribute_t261 * tmp;
		tmp = (RangeAttribute_t261 *)il2cpp_codegen_object_new (RangeAttribute_t261_il2cpp_TypeInfo_var);
		RangeAttribute__ctor_m1025(tmp, -1.0f, 1.0f, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void LookatTarget_t21_CustomAttributesCacheGenerator_m_RotationRange(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void LookatTarget_t21_CustomAttributesCacheGenerator_m_FollowSpeed(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAt.h"
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAtMethodDeclarations.h"
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void ProtectCameraFromWallClip_t25_CustomAttributesCacheGenerator_U3CprotectingU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void ProtectCameraFromWallClip_t25_CustomAttributesCacheGenerator_ProtectCameraFromWallClip_get_protecting_m46(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void ProtectCameraFromWallClip_t25_CustomAttributesCacheGenerator_ProtectCameraFromWallClip_set_protecting_m47(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void TargetFieldOfView_t28_CustomAttributesCacheGenerator_m_FovAdjustTime(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void TargetFieldOfView_t28_CustomAttributesCacheGenerator_m_ZoomAmountMultiplier(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void TargetFieldOfView_t28_CustomAttributesCacheGenerator_m_IncludeEffectsInSize(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void VirtualAxis_t30_CustomAttributesCacheGenerator_U3CnameU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void VirtualAxis_t30_CustomAttributesCacheGenerator_U3CmatchWithInputManagerU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void VirtualAxis_t30_CustomAttributesCacheGenerator_VirtualAxis_get_name_m71(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void VirtualAxis_t30_CustomAttributesCacheGenerator_VirtualAxis_set_name_m72(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void VirtualAxis_t30_CustomAttributesCacheGenerator_VirtualAxis_get_matchWithInputManager_m73(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void VirtualAxis_t30_CustomAttributesCacheGenerator_VirtualAxis_set_matchWithInputManager_m74(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void VirtualButton_t33_CustomAttributesCacheGenerator_U3CnameU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void VirtualButton_t33_CustomAttributesCacheGenerator_U3CmatchWithInputManagerU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void VirtualButton_t33_CustomAttributesCacheGenerator_VirtualButton_get_name_m81(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void VirtualButton_t33_CustomAttributesCacheGenerator_VirtualButton_set_name_m82(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void VirtualButton_t33_CustomAttributesCacheGenerator_VirtualButton_get_matchWithInputManager_m83(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void VirtualButton_t33_CustomAttributesCacheGenerator_VirtualButton_set_matchWithInputManager_m84(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExecuteInEditMode_t262_il2cpp_TypeInfo_var;
void MobileControlRig_t39_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExecuteInEditMode_t262_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(111);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExecuteInEditMode_t262 * tmp;
		tmp = (ExecuteInEditMode_t262 *)il2cpp_codegen_object_new (ExecuteInEditMode_t262_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m1027(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.UI.Image
#include "UnityEngine_UI_UnityEngine_UI_Image.h"
extern const Il2CppType* Image_t48_0_0_0_var;
extern TypeInfo* RequireComponent_t259_il2cpp_TypeInfo_var;
void TouchPad_t49_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Image_t48_0_0_0_var = il2cpp_codegen_type_from_index(32);
		RequireComponent_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(108);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RequireComponent_t259 * tmp;
		tmp = (RequireComponent_t259 *)il2cpp_codegen_object_new (RequireComponent_t259_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m1023(tmp, il2cpp_codegen_type_get_object(Image_t48_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void VirtualInput_t34_CustomAttributesCacheGenerator_U3CvirtualMousePositionU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void VirtualInput_t34_CustomAttributesCacheGenerator_VirtualInput_get_virtualMousePosition_m171(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void VirtualInput_t34_CustomAttributesCacheGenerator_VirtualInput_set_virtualMousePosition_m172(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.AddComponentMenu
#include "UnityEngine_UnityEngine_AddComponentMenu.h"
// UnityEngine.AddComponentMenu
#include "UnityEngine_UnityEngine_AddComponentMenuMethodDeclarations.h"
// UnityEngine.Camera
#include "UnityEngine_UnityEngine_Camera.h"
extern const Il2CppType* Camera_t27_0_0_0_var;
extern TypeInfo* AddComponentMenu_t264_il2cpp_TypeInfo_var;
extern TypeInfo* RequireComponent_t259_il2cpp_TypeInfo_var;
extern TypeInfo* ExecuteInEditMode_t262_il2cpp_TypeInfo_var;
void Antialiasing_t56_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Camera_t27_0_0_0_var = il2cpp_codegen_type_from_index(10);
		AddComponentMenu_t264_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		RequireComponent_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(108);
		ExecuteInEditMode_t262_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(111);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t264 * tmp;
		tmp = (AddComponentMenu_t264 *)il2cpp_codegen_object_new (AddComponentMenu_t264_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m1040(tmp, il2cpp_codegen_string_new_wrapper("Image Effects/Other/Antialiasing"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		RequireComponent_t259 * tmp;
		tmp = (RequireComponent_t259 *)il2cpp_codegen_object_new (RequireComponent_t259_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m1023(tmp, il2cpp_codegen_type_get_object(Camera_t27_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ExecuteInEditMode_t262 * tmp;
		tmp = (ExecuteInEditMode_t262 *)il2cpp_codegen_object_new (ExecuteInEditMode_t262_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m1027(tmp, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* Camera_t27_0_0_0_var;
extern TypeInfo* RequireComponent_t259_il2cpp_TypeInfo_var;
extern TypeInfo* ExecuteInEditMode_t262_il2cpp_TypeInfo_var;
extern TypeInfo* AddComponentMenu_t264_il2cpp_TypeInfo_var;
void Bloom_t64_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Camera_t27_0_0_0_var = il2cpp_codegen_type_from_index(10);
		RequireComponent_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(108);
		ExecuteInEditMode_t262_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(111);
		AddComponentMenu_t264_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RequireComponent_t259 * tmp;
		tmp = (RequireComponent_t259 *)il2cpp_codegen_object_new (RequireComponent_t259_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m1023(tmp, il2cpp_codegen_type_get_object(Camera_t27_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ExecuteInEditMode_t262 * tmp;
		tmp = (ExecuteInEditMode_t262 *)il2cpp_codegen_object_new (ExecuteInEditMode_t262_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m1027(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		AddComponentMenu_t264 * tmp;
		tmp = (AddComponentMenu_t264 *)il2cpp_codegen_object_new (AddComponentMenu_t264_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m1040(tmp, il2cpp_codegen_string_new_wrapper("Image Effects/Bloom and Glow/Bloom"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* Camera_t27_0_0_0_var;
extern TypeInfo* AddComponentMenu_t264_il2cpp_TypeInfo_var;
extern TypeInfo* RequireComponent_t259_il2cpp_TypeInfo_var;
extern TypeInfo* ExecuteInEditMode_t262_il2cpp_TypeInfo_var;
void BloomAndFlares_t70_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Camera_t27_0_0_0_var = il2cpp_codegen_type_from_index(10);
		AddComponentMenu_t264_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		RequireComponent_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(108);
		ExecuteInEditMode_t262_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(111);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t264 * tmp;
		tmp = (AddComponentMenu_t264 *)il2cpp_codegen_object_new (AddComponentMenu_t264_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m1040(tmp, il2cpp_codegen_string_new_wrapper("Image Effects/Bloom and Glow/BloomAndFlares (3.5, Deprecated)"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		RequireComponent_t259 * tmp;
		tmp = (RequireComponent_t259 *)il2cpp_codegen_object_new (RequireComponent_t259_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m1023(tmp, il2cpp_codegen_type_get_object(Camera_t27_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ExecuteInEditMode_t262 * tmp;
		tmp = (ExecuteInEditMode_t262 *)il2cpp_codegen_object_new (ExecuteInEditMode_t262_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m1027(tmp, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* Camera_t27_0_0_0_var;
extern TypeInfo* AddComponentMenu_t264_il2cpp_TypeInfo_var;
extern TypeInfo* RequireComponent_t259_il2cpp_TypeInfo_var;
extern TypeInfo* ExecuteInEditMode_t262_il2cpp_TypeInfo_var;
void BloomOptimized_t73_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Camera_t27_0_0_0_var = il2cpp_codegen_type_from_index(10);
		AddComponentMenu_t264_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		RequireComponent_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(108);
		ExecuteInEditMode_t262_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(111);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t264 * tmp;
		tmp = (AddComponentMenu_t264 *)il2cpp_codegen_object_new (AddComponentMenu_t264_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m1040(tmp, il2cpp_codegen_string_new_wrapper("Image Effects/Bloom and Glow/Bloom (Optimized)"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		RequireComponent_t259 * tmp;
		tmp = (RequireComponent_t259 *)il2cpp_codegen_object_new (RequireComponent_t259_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m1023(tmp, il2cpp_codegen_type_get_object(Camera_t27_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ExecuteInEditMode_t262 * tmp;
		tmp = (ExecuteInEditMode_t262 *)il2cpp_codegen_object_new (ExecuteInEditMode_t262_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m1027(tmp, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* RangeAttribute_t261_il2cpp_TypeInfo_var;
void BloomOptimized_t73_CustomAttributesCacheGenerator_threshold(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RangeAttribute_t261_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RangeAttribute_t261 * tmp;
		tmp = (RangeAttribute_t261 *)il2cpp_codegen_object_new (RangeAttribute_t261_il2cpp_TypeInfo_var);
		RangeAttribute__ctor_m1025(tmp, 0.0f, 1.5f, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* RangeAttribute_t261_il2cpp_TypeInfo_var;
void BloomOptimized_t73_CustomAttributesCacheGenerator_intensity(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RangeAttribute_t261_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RangeAttribute_t261 * tmp;
		tmp = (RangeAttribute_t261 *)il2cpp_codegen_object_new (RangeAttribute_t261_il2cpp_TypeInfo_var);
		RangeAttribute__ctor_m1025(tmp, 0.0f, 2.5f, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* RangeAttribute_t261_il2cpp_TypeInfo_var;
void BloomOptimized_t73_CustomAttributesCacheGenerator_blurSize(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RangeAttribute_t261_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RangeAttribute_t261 * tmp;
		tmp = (RangeAttribute_t261 *)il2cpp_codegen_object_new (RangeAttribute_t261_il2cpp_TypeInfo_var);
		RangeAttribute__ctor_m1025(tmp, 0.25f, 5.5f, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* RangeAttribute_t261_il2cpp_TypeInfo_var;
void BloomOptimized_t73_CustomAttributesCacheGenerator_blurIterations(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RangeAttribute_t261_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RangeAttribute_t261 * tmp;
		tmp = (RangeAttribute_t261 *)il2cpp_codegen_object_new (RangeAttribute_t261_il2cpp_TypeInfo_var);
		RangeAttribute__ctor_m1025(tmp, 1.0f, 4.0f, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AddComponentMenu_t264_il2cpp_TypeInfo_var;
extern TypeInfo* ExecuteInEditMode_t262_il2cpp_TypeInfo_var;
void Blur_t74_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t264_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		ExecuteInEditMode_t262_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(111);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t264 * tmp;
		tmp = (AddComponentMenu_t264 *)il2cpp_codegen_object_new (AddComponentMenu_t264_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m1040(tmp, il2cpp_codegen_string_new_wrapper("Image Effects/Blur/Blur"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ExecuteInEditMode_t262 * tmp;
		tmp = (ExecuteInEditMode_t262 *)il2cpp_codegen_object_new (ExecuteInEditMode_t262_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m1027(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* Camera_t27_0_0_0_var;
extern TypeInfo* ExecuteInEditMode_t262_il2cpp_TypeInfo_var;
extern TypeInfo* RequireComponent_t259_il2cpp_TypeInfo_var;
extern TypeInfo* AddComponentMenu_t264_il2cpp_TypeInfo_var;
void BlurOptimized_t76_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Camera_t27_0_0_0_var = il2cpp_codegen_type_from_index(10);
		ExecuteInEditMode_t262_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(111);
		RequireComponent_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(108);
		AddComponentMenu_t264_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExecuteInEditMode_t262 * tmp;
		tmp = (ExecuteInEditMode_t262 *)il2cpp_codegen_object_new (ExecuteInEditMode_t262_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m1027(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		RequireComponent_t259 * tmp;
		tmp = (RequireComponent_t259 *)il2cpp_codegen_object_new (RequireComponent_t259_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m1023(tmp, il2cpp_codegen_type_get_object(Camera_t27_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		AddComponentMenu_t264 * tmp;
		tmp = (AddComponentMenu_t264 *)il2cpp_codegen_object_new (AddComponentMenu_t264_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m1040(tmp, il2cpp_codegen_string_new_wrapper("Image Effects/Blur/Blur (Optimized)"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* RangeAttribute_t261_il2cpp_TypeInfo_var;
void BlurOptimized_t76_CustomAttributesCacheGenerator_downsample(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RangeAttribute_t261_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RangeAttribute_t261 * tmp;
		tmp = (RangeAttribute_t261 *)il2cpp_codegen_object_new (RangeAttribute_t261_il2cpp_TypeInfo_var);
		RangeAttribute__ctor_m1025(tmp, 0.0f, 2.0f, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* RangeAttribute_t261_il2cpp_TypeInfo_var;
void BlurOptimized_t76_CustomAttributesCacheGenerator_blurSize(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RangeAttribute_t261_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RangeAttribute_t261 * tmp;
		tmp = (RangeAttribute_t261 *)il2cpp_codegen_object_new (RangeAttribute_t261_il2cpp_TypeInfo_var);
		RangeAttribute__ctor_m1025(tmp, 0.0f, 10.0f, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* RangeAttribute_t261_il2cpp_TypeInfo_var;
void BlurOptimized_t76_CustomAttributesCacheGenerator_blurIterations(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RangeAttribute_t261_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RangeAttribute_t261 * tmp;
		tmp = (RangeAttribute_t261 *)il2cpp_codegen_object_new (RangeAttribute_t261_il2cpp_TypeInfo_var);
		RangeAttribute__ctor_m1025(tmp, 1.0f, 4.0f, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* Camera_t27_0_0_0_var;
extern TypeInfo* ExecuteInEditMode_t262_il2cpp_TypeInfo_var;
extern TypeInfo* AddComponentMenu_t264_il2cpp_TypeInfo_var;
extern TypeInfo* RequireComponent_t259_il2cpp_TypeInfo_var;
void CameraMotionBlur_t79_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Camera_t27_0_0_0_var = il2cpp_codegen_type_from_index(10);
		ExecuteInEditMode_t262_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(111);
		AddComponentMenu_t264_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		RequireComponent_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(108);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExecuteInEditMode_t262 * tmp;
		tmp = (ExecuteInEditMode_t262 *)il2cpp_codegen_object_new (ExecuteInEditMode_t262_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m1027(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AddComponentMenu_t264 * tmp;
		tmp = (AddComponentMenu_t264 *)il2cpp_codegen_object_new (AddComponentMenu_t264_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m1040(tmp, il2cpp_codegen_string_new_wrapper("Image Effects/Camera/Camera Motion Blur"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		RequireComponent_t259 * tmp;
		tmp = (RequireComponent_t259 *)il2cpp_codegen_object_new (RequireComponent_t259_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m1023(tmp, il2cpp_codegen_type_get_object(Camera_t27_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExecuteInEditMode_t262_il2cpp_TypeInfo_var;
extern TypeInfo* AddComponentMenu_t264_il2cpp_TypeInfo_var;
void ColorCorrectionCurves_t83_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExecuteInEditMode_t262_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(111);
		AddComponentMenu_t264_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExecuteInEditMode_t262 * tmp;
		tmp = (ExecuteInEditMode_t262 *)il2cpp_codegen_object_new (ExecuteInEditMode_t262_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m1027(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AddComponentMenu_t264 * tmp;
		tmp = (AddComponentMenu_t264 *)il2cpp_codegen_object_new (AddComponentMenu_t264_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m1040(tmp, il2cpp_codegen_string_new_wrapper("Image Effects/Color Adjustments/Color Correction (Curves, Saturation)"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExecuteInEditMode_t262_il2cpp_TypeInfo_var;
extern TypeInfo* AddComponentMenu_t264_il2cpp_TypeInfo_var;
void ColorCorrectionLookup_t85_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExecuteInEditMode_t262_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(111);
		AddComponentMenu_t264_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExecuteInEditMode_t262 * tmp;
		tmp = (ExecuteInEditMode_t262 *)il2cpp_codegen_object_new (ExecuteInEditMode_t262_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m1027(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AddComponentMenu_t264 * tmp;
		tmp = (AddComponentMenu_t264 *)il2cpp_codegen_object_new (AddComponentMenu_t264_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m1040(tmp, il2cpp_codegen_string_new_wrapper("Image Effects/Color Adjustments/Color Correction (3D Lookup Texture)"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExecuteInEditMode_t262_il2cpp_TypeInfo_var;
extern TypeInfo* AddComponentMenu_t264_il2cpp_TypeInfo_var;
void ColorCorrectionRamp_t87_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExecuteInEditMode_t262_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(111);
		AddComponentMenu_t264_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExecuteInEditMode_t262 * tmp;
		tmp = (ExecuteInEditMode_t262 *)il2cpp_codegen_object_new (ExecuteInEditMode_t262_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m1027(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AddComponentMenu_t264 * tmp;
		tmp = (AddComponentMenu_t264 *)il2cpp_codegen_object_new (AddComponentMenu_t264_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m1040(tmp, il2cpp_codegen_string_new_wrapper("Image Effects/Color Adjustments/Color Correction (Ramp)"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* Camera_t27_0_0_0_var;
extern TypeInfo* AddComponentMenu_t264_il2cpp_TypeInfo_var;
extern TypeInfo* ExecuteInEditMode_t262_il2cpp_TypeInfo_var;
extern TypeInfo* RequireComponent_t259_il2cpp_TypeInfo_var;
void ContrastEnhance_t89_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Camera_t27_0_0_0_var = il2cpp_codegen_type_from_index(10);
		AddComponentMenu_t264_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		ExecuteInEditMode_t262_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(111);
		RequireComponent_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(108);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t264 * tmp;
		tmp = (AddComponentMenu_t264 *)il2cpp_codegen_object_new (AddComponentMenu_t264_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m1040(tmp, il2cpp_codegen_string_new_wrapper("Image Effects/Color Adjustments/Contrast Enhance (Unsharp Mask)"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ExecuteInEditMode_t262 * tmp;
		tmp = (ExecuteInEditMode_t262 *)il2cpp_codegen_object_new (ExecuteInEditMode_t262_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m1027(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		RequireComponent_t259 * tmp;
		tmp = (RequireComponent_t259 *)il2cpp_codegen_object_new (RequireComponent_t259_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m1023(tmp, il2cpp_codegen_type_get_object(Camera_t27_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AddComponentMenu_t264_il2cpp_TypeInfo_var;
extern TypeInfo* ExecuteInEditMode_t262_il2cpp_TypeInfo_var;
void ContrastStretch_t91_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t264_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		ExecuteInEditMode_t262_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(111);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t264 * tmp;
		tmp = (AddComponentMenu_t264 *)il2cpp_codegen_object_new (AddComponentMenu_t264_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m1040(tmp, il2cpp_codegen_string_new_wrapper("Image Effects/Color Adjustments/Contrast Stretch"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ExecuteInEditMode_t262 * tmp;
		tmp = (ExecuteInEditMode_t262 *)il2cpp_codegen_object_new (ExecuteInEditMode_t262_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m1027(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* Camera_t27_0_0_0_var;
extern TypeInfo* RequireComponent_t259_il2cpp_TypeInfo_var;
extern TypeInfo* AddComponentMenu_t264_il2cpp_TypeInfo_var;
extern TypeInfo* ExecuteInEditMode_t262_il2cpp_TypeInfo_var;
void CreaseShading_t92_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Camera_t27_0_0_0_var = il2cpp_codegen_type_from_index(10);
		RequireComponent_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(108);
		AddComponentMenu_t264_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		ExecuteInEditMode_t262_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(111);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RequireComponent_t259 * tmp;
		tmp = (RequireComponent_t259 *)il2cpp_codegen_object_new (RequireComponent_t259_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m1023(tmp, il2cpp_codegen_type_get_object(Camera_t27_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AddComponentMenu_t264 * tmp;
		tmp = (AddComponentMenu_t264 *)il2cpp_codegen_object_new (AddComponentMenu_t264_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m1040(tmp, il2cpp_codegen_string_new_wrapper("Image Effects/Edge Detection/Crease Shading"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ExecuteInEditMode_t262 * tmp;
		tmp = (ExecuteInEditMode_t262 *)il2cpp_codegen_object_new (ExecuteInEditMode_t262_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m1027(tmp, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* Camera_t27_0_0_0_var;
extern TypeInfo* AddComponentMenu_t264_il2cpp_TypeInfo_var;
extern TypeInfo* ExecuteInEditMode_t262_il2cpp_TypeInfo_var;
extern TypeInfo* RequireComponent_t259_il2cpp_TypeInfo_var;
void DepthOfField_t96_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Camera_t27_0_0_0_var = il2cpp_codegen_type_from_index(10);
		AddComponentMenu_t264_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		ExecuteInEditMode_t262_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(111);
		RequireComponent_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(108);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t264 * tmp;
		tmp = (AddComponentMenu_t264 *)il2cpp_codegen_object_new (AddComponentMenu_t264_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m1040(tmp, il2cpp_codegen_string_new_wrapper("Image Effects/Camera/Depth of Field (Lens Blur, Scatter, DX11)"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ExecuteInEditMode_t262 * tmp;
		tmp = (ExecuteInEditMode_t262 *)il2cpp_codegen_object_new (ExecuteInEditMode_t262_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m1027(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		RequireComponent_t259 * tmp;
		tmp = (RequireComponent_t259 *)il2cpp_codegen_object_new (RequireComponent_t259_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m1023(tmp, il2cpp_codegen_type_get_object(Camera_t27_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* Camera_t27_0_0_0_var;
extern TypeInfo* ExecuteInEditMode_t262_il2cpp_TypeInfo_var;
extern TypeInfo* RequireComponent_t259_il2cpp_TypeInfo_var;
extern TypeInfo* AddComponentMenu_t264_il2cpp_TypeInfo_var;
void DepthOfFieldDeprecated_t102_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Camera_t27_0_0_0_var = il2cpp_codegen_type_from_index(10);
		ExecuteInEditMode_t262_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(111);
		RequireComponent_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(108);
		AddComponentMenu_t264_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExecuteInEditMode_t262 * tmp;
		tmp = (ExecuteInEditMode_t262 *)il2cpp_codegen_object_new (ExecuteInEditMode_t262_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m1027(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		RequireComponent_t259 * tmp;
		tmp = (RequireComponent_t259 *)il2cpp_codegen_object_new (RequireComponent_t259_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m1023(tmp, il2cpp_codegen_type_get_object(Camera_t27_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		AddComponentMenu_t264 * tmp;
		tmp = (AddComponentMenu_t264 *)il2cpp_codegen_object_new (AddComponentMenu_t264_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m1040(tmp, il2cpp_codegen_string_new_wrapper("Image Effects/Camera/Depth of Field (deprecated)"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* Camera_t27_0_0_0_var;
extern TypeInfo* AddComponentMenu_t264_il2cpp_TypeInfo_var;
extern TypeInfo* RequireComponent_t259_il2cpp_TypeInfo_var;
extern TypeInfo* ExecuteInEditMode_t262_il2cpp_TypeInfo_var;
void EdgeDetection_t104_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Camera_t27_0_0_0_var = il2cpp_codegen_type_from_index(10);
		AddComponentMenu_t264_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		RequireComponent_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(108);
		ExecuteInEditMode_t262_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(111);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t264 * tmp;
		tmp = (AddComponentMenu_t264 *)il2cpp_codegen_object_new (AddComponentMenu_t264_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m1040(tmp, il2cpp_codegen_string_new_wrapper("Image Effects/Edge Detection/Edge Detection"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		RequireComponent_t259 * tmp;
		tmp = (RequireComponent_t259 *)il2cpp_codegen_object_new (RequireComponent_t259_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m1023(tmp, il2cpp_codegen_type_get_object(Camera_t27_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ExecuteInEditMode_t262 * tmp;
		tmp = (ExecuteInEditMode_t262 *)il2cpp_codegen_object_new (ExecuteInEditMode_t262_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m1027(tmp, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.ImageEffectOpaque
#include "UnityEngine_UnityEngine_ImageEffectOpaque.h"
// UnityEngine.ImageEffectOpaque
#include "UnityEngine_UnityEngine_ImageEffectOpaqueMethodDeclarations.h"
extern TypeInfo* ImageEffectOpaque_t265_il2cpp_TypeInfo_var;
void EdgeDetection_t104_CustomAttributesCacheGenerator_EdgeDetection_OnRenderImage_m294(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ImageEffectOpaque_t265_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(114);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ImageEffectOpaque_t265 * tmp;
		tmp = (ImageEffectOpaque_t265 *)il2cpp_codegen_object_new (ImageEffectOpaque_t265_il2cpp_TypeInfo_var);
		ImageEffectOpaque__ctor_m1041(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* Camera_t27_0_0_0_var;
extern TypeInfo* RequireComponent_t259_il2cpp_TypeInfo_var;
extern TypeInfo* ExecuteInEditMode_t262_il2cpp_TypeInfo_var;
extern TypeInfo* AddComponentMenu_t264_il2cpp_TypeInfo_var;
void Fisheye_t105_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Camera_t27_0_0_0_var = il2cpp_codegen_type_from_index(10);
		RequireComponent_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(108);
		ExecuteInEditMode_t262_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(111);
		AddComponentMenu_t264_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RequireComponent_t259 * tmp;
		tmp = (RequireComponent_t259 *)il2cpp_codegen_object_new (RequireComponent_t259_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m1023(tmp, il2cpp_codegen_type_get_object(Camera_t27_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ExecuteInEditMode_t262 * tmp;
		tmp = (ExecuteInEditMode_t262 *)il2cpp_codegen_object_new (ExecuteInEditMode_t262_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m1027(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		AddComponentMenu_t264 * tmp;
		tmp = (AddComponentMenu_t264 *)il2cpp_codegen_object_new (AddComponentMenu_t264_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m1040(tmp, il2cpp_codegen_string_new_wrapper("Image Effects/Displacement/Fisheye"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* Camera_t27_0_0_0_var;
extern TypeInfo* ExecuteInEditMode_t262_il2cpp_TypeInfo_var;
extern TypeInfo* RequireComponent_t259_il2cpp_TypeInfo_var;
extern TypeInfo* AddComponentMenu_t264_il2cpp_TypeInfo_var;
void GlobalFog_t106_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Camera_t27_0_0_0_var = il2cpp_codegen_type_from_index(10);
		ExecuteInEditMode_t262_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(111);
		RequireComponent_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(108);
		AddComponentMenu_t264_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExecuteInEditMode_t262 * tmp;
		tmp = (ExecuteInEditMode_t262 *)il2cpp_codegen_object_new (ExecuteInEditMode_t262_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m1027(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		RequireComponent_t259 * tmp;
		tmp = (RequireComponent_t259 *)il2cpp_codegen_object_new (RequireComponent_t259_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m1023(tmp, il2cpp_codegen_type_get_object(Camera_t27_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		AddComponentMenu_t264 * tmp;
		tmp = (AddComponentMenu_t264 *)il2cpp_codegen_object_new (AddComponentMenu_t264_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m1040(tmp, il2cpp_codegen_string_new_wrapper("Image Effects/Rendering/Global Fog"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.TooltipAttribute
#include "UnityEngine_UnityEngine_TooltipAttribute.h"
// UnityEngine.TooltipAttribute
#include "UnityEngine_UnityEngine_TooltipAttributeMethodDeclarations.h"
extern TypeInfo* TooltipAttribute_t266_il2cpp_TypeInfo_var;
void GlobalFog_t106_CustomAttributesCacheGenerator_distanceFog(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TooltipAttribute_t266_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(115);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TooltipAttribute_t266 * tmp;
		tmp = (TooltipAttribute_t266 *)il2cpp_codegen_object_new (TooltipAttribute_t266_il2cpp_TypeInfo_var);
		TooltipAttribute__ctor_m1042(tmp, il2cpp_codegen_string_new_wrapper("Apply distance-based fog?"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* TooltipAttribute_t266_il2cpp_TypeInfo_var;
void GlobalFog_t106_CustomAttributesCacheGenerator_useRadialDistance(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TooltipAttribute_t266_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(115);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TooltipAttribute_t266 * tmp;
		tmp = (TooltipAttribute_t266 *)il2cpp_codegen_object_new (TooltipAttribute_t266_il2cpp_TypeInfo_var);
		TooltipAttribute__ctor_m1042(tmp, il2cpp_codegen_string_new_wrapper("Distance fog is based on radial distance from camera when checked"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* TooltipAttribute_t266_il2cpp_TypeInfo_var;
void GlobalFog_t106_CustomAttributesCacheGenerator_heightFog(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TooltipAttribute_t266_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(115);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TooltipAttribute_t266 * tmp;
		tmp = (TooltipAttribute_t266 *)il2cpp_codegen_object_new (TooltipAttribute_t266_il2cpp_TypeInfo_var);
		TooltipAttribute__ctor_m1042(tmp, il2cpp_codegen_string_new_wrapper("Apply height-based fog?"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* TooltipAttribute_t266_il2cpp_TypeInfo_var;
void GlobalFog_t106_CustomAttributesCacheGenerator_height(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TooltipAttribute_t266_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(115);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TooltipAttribute_t266 * tmp;
		tmp = (TooltipAttribute_t266 *)il2cpp_codegen_object_new (TooltipAttribute_t266_il2cpp_TypeInfo_var);
		TooltipAttribute__ctor_m1042(tmp, il2cpp_codegen_string_new_wrapper("Fog top Y coordinate"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* RangeAttribute_t261_il2cpp_TypeInfo_var;
void GlobalFog_t106_CustomAttributesCacheGenerator_heightDensity(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RangeAttribute_t261_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RangeAttribute_t261 * tmp;
		tmp = (RangeAttribute_t261 *)il2cpp_codegen_object_new (RangeAttribute_t261_il2cpp_TypeInfo_var);
		RangeAttribute__ctor_m1025(tmp, 0.001f, 10.0f, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* TooltipAttribute_t266_il2cpp_TypeInfo_var;
void GlobalFog_t106_CustomAttributesCacheGenerator_startDistance(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TooltipAttribute_t266_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(115);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TooltipAttribute_t266 * tmp;
		tmp = (TooltipAttribute_t266 *)il2cpp_codegen_object_new (TooltipAttribute_t266_il2cpp_TypeInfo_var);
		TooltipAttribute__ctor_m1042(tmp, il2cpp_codegen_string_new_wrapper("Push fog away from the camera by this amount"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ImageEffectOpaque_t265_il2cpp_TypeInfo_var;
void GlobalFog_t106_CustomAttributesCacheGenerator_GlobalFog_OnRenderImage_m300(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ImageEffectOpaque_t265_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(114);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ImageEffectOpaque_t265 * tmp;
		tmp = (ImageEffectOpaque_t265 *)il2cpp_codegen_object_new (ImageEffectOpaque_t265_il2cpp_TypeInfo_var);
		ImageEffectOpaque__ctor_m1041(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExecuteInEditMode_t262_il2cpp_TypeInfo_var;
extern TypeInfo* AddComponentMenu_t264_il2cpp_TypeInfo_var;
void Grayscale_t107_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExecuteInEditMode_t262_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(111);
		AddComponentMenu_t264_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExecuteInEditMode_t262 * tmp;
		tmp = (ExecuteInEditMode_t262 *)il2cpp_codegen_object_new (ExecuteInEditMode_t262_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m1027(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AddComponentMenu_t264 * tmp;
		tmp = (AddComponentMenu_t264 *)il2cpp_codegen_object_new (AddComponentMenu_t264_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m1040(tmp, il2cpp_codegen_string_new_wrapper("Image Effects/Color Adjustments/Grayscale"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* Camera_t27_0_0_0_var;
extern TypeInfo* RequireComponent_t259_il2cpp_TypeInfo_var;
extern TypeInfo* AddComponentMenu_t264_il2cpp_TypeInfo_var;
void ImageEffectBase_t88_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Camera_t27_0_0_0_var = il2cpp_codegen_type_from_index(10);
		RequireComponent_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(108);
		AddComponentMenu_t264_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RequireComponent_t259 * tmp;
		tmp = (RequireComponent_t259 *)il2cpp_codegen_object_new (RequireComponent_t259_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m1023(tmp, il2cpp_codegen_type_get_object(Camera_t27_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AddComponentMenu_t264 * tmp;
		tmp = (AddComponentMenu_t264 *)il2cpp_codegen_object_new (AddComponentMenu_t264_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m1040(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AddComponentMenu_t264_il2cpp_TypeInfo_var;
void ImageEffects_t108_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t264_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t264 * tmp;
		tmp = (AddComponentMenu_t264 *)il2cpp_codegen_object_new (AddComponentMenu_t264_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m1040(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.ObsoleteAttribute
#include "mscorlib_System_ObsoleteAttribute.h"
// System.ObsoleteAttribute
#include "mscorlib_System_ObsoleteAttributeMethodDeclarations.h"
extern TypeInfo* ObsoleteAttribute_t267_il2cpp_TypeInfo_var;
void ImageEffects_t108_CustomAttributesCacheGenerator_ImageEffects_Blit_m310(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t267_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t267 * tmp;
		tmp = (ObsoleteAttribute_t267 *)il2cpp_codegen_object_new (ObsoleteAttribute_t267_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m1043(tmp, il2cpp_codegen_string_new_wrapper("Use Graphics.Blit(source,dest) instead"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t267_il2cpp_TypeInfo_var;
void ImageEffects_t108_CustomAttributesCacheGenerator_ImageEffects_BlitWithMaterial_m311(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t267_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t267 * tmp;
		tmp = (ObsoleteAttribute_t267 *)il2cpp_codegen_object_new (ObsoleteAttribute_t267_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m1043(tmp, il2cpp_codegen_string_new_wrapper("Use Graphics.Blit(source, destination, material) instead"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* Camera_t27_0_0_0_var;
extern TypeInfo* ExecuteInEditMode_t262_il2cpp_TypeInfo_var;
extern TypeInfo* RequireComponent_t259_il2cpp_TypeInfo_var;
extern TypeInfo* AddComponentMenu_t264_il2cpp_TypeInfo_var;
void MotionBlur_t109_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Camera_t27_0_0_0_var = il2cpp_codegen_type_from_index(10);
		ExecuteInEditMode_t262_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(111);
		RequireComponent_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(108);
		AddComponentMenu_t264_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExecuteInEditMode_t262 * tmp;
		tmp = (ExecuteInEditMode_t262 *)il2cpp_codegen_object_new (ExecuteInEditMode_t262_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m1027(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		RequireComponent_t259 * tmp;
		tmp = (RequireComponent_t259 *)il2cpp_codegen_object_new (RequireComponent_t259_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m1023(tmp, il2cpp_codegen_type_get_object(Camera_t27_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		AddComponentMenu_t264 * tmp;
		tmp = (AddComponentMenu_t264 *)il2cpp_codegen_object_new (AddComponentMenu_t264_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m1040(tmp, il2cpp_codegen_string_new_wrapper("Image Effects/Blur/Motion Blur (Color Accumulation)"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* Camera_t27_0_0_0_var;
extern TypeInfo* RequireComponent_t259_il2cpp_TypeInfo_var;
extern TypeInfo* ExecuteInEditMode_t262_il2cpp_TypeInfo_var;
extern TypeInfo* AddComponentMenu_t264_il2cpp_TypeInfo_var;
void NoiseAndGrain_t110_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Camera_t27_0_0_0_var = il2cpp_codegen_type_from_index(10);
		RequireComponent_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(108);
		ExecuteInEditMode_t262_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(111);
		AddComponentMenu_t264_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RequireComponent_t259 * tmp;
		tmp = (RequireComponent_t259 *)il2cpp_codegen_object_new (RequireComponent_t259_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m1023(tmp, il2cpp_codegen_type_get_object(Camera_t27_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ExecuteInEditMode_t262 * tmp;
		tmp = (ExecuteInEditMode_t262 *)il2cpp_codegen_object_new (ExecuteInEditMode_t262_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m1027(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		AddComponentMenu_t264 * tmp;
		tmp = (AddComponentMenu_t264 *)il2cpp_codegen_object_new (AddComponentMenu_t264_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m1040(tmp, il2cpp_codegen_string_new_wrapper("Image Effects/Noise/Noise And Grain (Filmic)"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* Camera_t27_0_0_0_var;
extern TypeInfo* RequireComponent_t259_il2cpp_TypeInfo_var;
extern TypeInfo* ExecuteInEditMode_t262_il2cpp_TypeInfo_var;
extern TypeInfo* AddComponentMenu_t264_il2cpp_TypeInfo_var;
void NoiseAndScratches_t111_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Camera_t27_0_0_0_var = il2cpp_codegen_type_from_index(10);
		RequireComponent_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(108);
		ExecuteInEditMode_t262_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(111);
		AddComponentMenu_t264_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RequireComponent_t259 * tmp;
		tmp = (RequireComponent_t259 *)il2cpp_codegen_object_new (RequireComponent_t259_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m1023(tmp, il2cpp_codegen_type_get_object(Camera_t27_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ExecuteInEditMode_t262 * tmp;
		tmp = (ExecuteInEditMode_t262 *)il2cpp_codegen_object_new (ExecuteInEditMode_t262_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m1027(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		AddComponentMenu_t264 * tmp;
		tmp = (AddComponentMenu_t264 *)il2cpp_codegen_object_new (AddComponentMenu_t264_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m1040(tmp, il2cpp_codegen_string_new_wrapper("Image Effects/Noise/Noise and Scratches"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* Camera_t27_0_0_0_var;
extern TypeInfo* RequireComponent_t259_il2cpp_TypeInfo_var;
extern TypeInfo* ExecuteInEditMode_t262_il2cpp_TypeInfo_var;
void PostEffectsBase_t57_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Camera_t27_0_0_0_var = il2cpp_codegen_type_from_index(10);
		RequireComponent_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(108);
		ExecuteInEditMode_t262_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(111);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RequireComponent_t259 * tmp;
		tmp = (RequireComponent_t259 *)il2cpp_codegen_object_new (RequireComponent_t259_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m1023(tmp, il2cpp_codegen_type_get_object(Camera_t27_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ExecuteInEditMode_t262 * tmp;
		tmp = (ExecuteInEditMode_t262 *)il2cpp_codegen_object_new (ExecuteInEditMode_t262_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m1027(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* Camera_t27_0_0_0_var;
extern TypeInfo* ExecuteInEditMode_t262_il2cpp_TypeInfo_var;
extern TypeInfo* RequireComponent_t259_il2cpp_TypeInfo_var;
void PostEffectsHelper_t112_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Camera_t27_0_0_0_var = il2cpp_codegen_type_from_index(10);
		ExecuteInEditMode_t262_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(111);
		RequireComponent_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(108);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExecuteInEditMode_t262 * tmp;
		tmp = (ExecuteInEditMode_t262 *)il2cpp_codegen_object_new (ExecuteInEditMode_t262_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m1027(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		RequireComponent_t259 * tmp;
		tmp = (RequireComponent_t259 *)il2cpp_codegen_object_new (RequireComponent_t259_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m1023(tmp, il2cpp_codegen_type_get_object(Camera_t27_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* Camera_t27_0_0_0_var;
extern TypeInfo* AddComponentMenu_t264_il2cpp_TypeInfo_var;
extern TypeInfo* ExecuteInEditMode_t262_il2cpp_TypeInfo_var;
extern TypeInfo* RequireComponent_t259_il2cpp_TypeInfo_var;
void ScreenOverlay_t116_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Camera_t27_0_0_0_var = il2cpp_codegen_type_from_index(10);
		AddComponentMenu_t264_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		ExecuteInEditMode_t262_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(111);
		RequireComponent_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(108);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t264 * tmp;
		tmp = (AddComponentMenu_t264 *)il2cpp_codegen_object_new (AddComponentMenu_t264_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m1040(tmp, il2cpp_codegen_string_new_wrapper("Image Effects/Other/Screen Overlay"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ExecuteInEditMode_t262 * tmp;
		tmp = (ExecuteInEditMode_t262 *)il2cpp_codegen_object_new (ExecuteInEditMode_t262_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m1027(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		RequireComponent_t259 * tmp;
		tmp = (RequireComponent_t259 *)il2cpp_codegen_object_new (RequireComponent_t259_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m1023(tmp, il2cpp_codegen_type_get_object(Camera_t27_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* Camera_t27_0_0_0_var;
extern TypeInfo* RequireComponent_t259_il2cpp_TypeInfo_var;
extern TypeInfo* ExecuteInEditMode_t262_il2cpp_TypeInfo_var;
extern TypeInfo* AddComponentMenu_t264_il2cpp_TypeInfo_var;
void ScreenSpaceAmbientObscurance_t117_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Camera_t27_0_0_0_var = il2cpp_codegen_type_from_index(10);
		RequireComponent_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(108);
		ExecuteInEditMode_t262_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(111);
		AddComponentMenu_t264_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RequireComponent_t259 * tmp;
		tmp = (RequireComponent_t259 *)il2cpp_codegen_object_new (RequireComponent_t259_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m1023(tmp, il2cpp_codegen_type_get_object(Camera_t27_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ExecuteInEditMode_t262 * tmp;
		tmp = (ExecuteInEditMode_t262 *)il2cpp_codegen_object_new (ExecuteInEditMode_t262_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m1027(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		AddComponentMenu_t264 * tmp;
		tmp = (AddComponentMenu_t264 *)il2cpp_codegen_object_new (AddComponentMenu_t264_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m1040(tmp, il2cpp_codegen_string_new_wrapper("Image Effects/Rendering/Screen Space Ambient Obscurance"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* RangeAttribute_t261_il2cpp_TypeInfo_var;
void ScreenSpaceAmbientObscurance_t117_CustomAttributesCacheGenerator_intensity(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RangeAttribute_t261_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RangeAttribute_t261 * tmp;
		tmp = (RangeAttribute_t261 *)il2cpp_codegen_object_new (RangeAttribute_t261_il2cpp_TypeInfo_var);
		RangeAttribute__ctor_m1025(tmp, 0.0f, 3.0f, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* RangeAttribute_t261_il2cpp_TypeInfo_var;
void ScreenSpaceAmbientObscurance_t117_CustomAttributesCacheGenerator_radius(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RangeAttribute_t261_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RangeAttribute_t261 * tmp;
		tmp = (RangeAttribute_t261 *)il2cpp_codegen_object_new (RangeAttribute_t261_il2cpp_TypeInfo_var);
		RangeAttribute__ctor_m1025(tmp, 0.1f, 3.0f, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* RangeAttribute_t261_il2cpp_TypeInfo_var;
void ScreenSpaceAmbientObscurance_t117_CustomAttributesCacheGenerator_blurIterations(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RangeAttribute_t261_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RangeAttribute_t261 * tmp;
		tmp = (RangeAttribute_t261 *)il2cpp_codegen_object_new (RangeAttribute_t261_il2cpp_TypeInfo_var);
		RangeAttribute__ctor_m1025(tmp, 0.0f, 3.0f, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* RangeAttribute_t261_il2cpp_TypeInfo_var;
void ScreenSpaceAmbientObscurance_t117_CustomAttributesCacheGenerator_blurFilterDistance(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RangeAttribute_t261_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RangeAttribute_t261 * tmp;
		tmp = (RangeAttribute_t261 *)il2cpp_codegen_object_new (RangeAttribute_t261_il2cpp_TypeInfo_var);
		RangeAttribute__ctor_m1025(tmp, 0.0f, 5.0f, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* RangeAttribute_t261_il2cpp_TypeInfo_var;
void ScreenSpaceAmbientObscurance_t117_CustomAttributesCacheGenerator_downsample(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RangeAttribute_t261_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RangeAttribute_t261 * tmp;
		tmp = (RangeAttribute_t261 *)il2cpp_codegen_object_new (RangeAttribute_t261_il2cpp_TypeInfo_var);
		RangeAttribute__ctor_m1025(tmp, 0.0f, 1.0f, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ImageEffectOpaque_t265_il2cpp_TypeInfo_var;
void ScreenSpaceAmbientObscurance_t117_CustomAttributesCacheGenerator_ScreenSpaceAmbientObscurance_OnRenderImage_m358(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ImageEffectOpaque_t265_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(114);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ImageEffectOpaque_t265 * tmp;
		tmp = (ImageEffectOpaque_t265 *)il2cpp_codegen_object_new (ImageEffectOpaque_t265_il2cpp_TypeInfo_var);
		ImageEffectOpaque__ctor_m1041(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* Camera_t27_0_0_0_var;
extern TypeInfo* ExecuteInEditMode_t262_il2cpp_TypeInfo_var;
extern TypeInfo* AddComponentMenu_t264_il2cpp_TypeInfo_var;
extern TypeInfo* RequireComponent_t259_il2cpp_TypeInfo_var;
void ScreenSpaceAmbientOcclusion_t119_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Camera_t27_0_0_0_var = il2cpp_codegen_type_from_index(10);
		ExecuteInEditMode_t262_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(111);
		AddComponentMenu_t264_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		RequireComponent_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(108);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExecuteInEditMode_t262 * tmp;
		tmp = (ExecuteInEditMode_t262 *)il2cpp_codegen_object_new (ExecuteInEditMode_t262_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m1027(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AddComponentMenu_t264 * tmp;
		tmp = (AddComponentMenu_t264 *)il2cpp_codegen_object_new (AddComponentMenu_t264_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m1040(tmp, il2cpp_codegen_string_new_wrapper("Image Effects/Rendering/Screen Space Ambient Occlusion"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		RequireComponent_t259 * tmp;
		tmp = (RequireComponent_t259 *)il2cpp_codegen_object_new (RequireComponent_t259_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m1023(tmp, il2cpp_codegen_type_get_object(Camera_t27_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ImageEffectOpaque_t265_il2cpp_TypeInfo_var;
void ScreenSpaceAmbientOcclusion_t119_CustomAttributesCacheGenerator_ScreenSpaceAmbientOcclusion_OnRenderImage_m366(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ImageEffectOpaque_t265_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(114);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ImageEffectOpaque_t265 * tmp;
		tmp = (ImageEffectOpaque_t265 *)il2cpp_codegen_object_new (ImageEffectOpaque_t265_il2cpp_TypeInfo_var);
		ImageEffectOpaque__ctor_m1041(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExecuteInEditMode_t262_il2cpp_TypeInfo_var;
extern TypeInfo* AddComponentMenu_t264_il2cpp_TypeInfo_var;
void SepiaTone_t120_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExecuteInEditMode_t262_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(111);
		AddComponentMenu_t264_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExecuteInEditMode_t262 * tmp;
		tmp = (ExecuteInEditMode_t262 *)il2cpp_codegen_object_new (ExecuteInEditMode_t262_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m1027(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AddComponentMenu_t264 * tmp;
		tmp = (AddComponentMenu_t264 *)il2cpp_codegen_object_new (AddComponentMenu_t264_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m1040(tmp, il2cpp_codegen_string_new_wrapper("Image Effects/Color Adjustments/Sepia Tone"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* Camera_t27_0_0_0_var;
extern TypeInfo* ExecuteInEditMode_t262_il2cpp_TypeInfo_var;
extern TypeInfo* RequireComponent_t259_il2cpp_TypeInfo_var;
extern TypeInfo* AddComponentMenu_t264_il2cpp_TypeInfo_var;
void SunShafts_t123_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Camera_t27_0_0_0_var = il2cpp_codegen_type_from_index(10);
		ExecuteInEditMode_t262_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(111);
		RequireComponent_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(108);
		AddComponentMenu_t264_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExecuteInEditMode_t262 * tmp;
		tmp = (ExecuteInEditMode_t262 *)il2cpp_codegen_object_new (ExecuteInEditMode_t262_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m1027(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		RequireComponent_t259 * tmp;
		tmp = (RequireComponent_t259 *)il2cpp_codegen_object_new (RequireComponent_t259_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m1023(tmp, il2cpp_codegen_type_get_object(Camera_t27_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		AddComponentMenu_t264 * tmp;
		tmp = (AddComponentMenu_t264 *)il2cpp_codegen_object_new (AddComponentMenu_t264_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m1040(tmp, il2cpp_codegen_string_new_wrapper("Image Effects/Rendering/Sun Shafts"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* Camera_t27_0_0_0_var;
extern TypeInfo* AddComponentMenu_t264_il2cpp_TypeInfo_var;
extern TypeInfo* RequireComponent_t259_il2cpp_TypeInfo_var;
void TiltShift_t126_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Camera_t27_0_0_0_var = il2cpp_codegen_type_from_index(10);
		AddComponentMenu_t264_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		RequireComponent_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(108);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t264 * tmp;
		tmp = (AddComponentMenu_t264 *)il2cpp_codegen_object_new (AddComponentMenu_t264_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m1040(tmp, il2cpp_codegen_string_new_wrapper("Image Effects/Camera/Tilt Shift (Lens Blur)"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		RequireComponent_t259 * tmp;
		tmp = (RequireComponent_t259 *)il2cpp_codegen_object_new (RequireComponent_t259_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m1023(tmp, il2cpp_codegen_type_get_object(Camera_t27_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* RangeAttribute_t261_il2cpp_TypeInfo_var;
void TiltShift_t126_CustomAttributesCacheGenerator_blurArea(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RangeAttribute_t261_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RangeAttribute_t261 * tmp;
		tmp = (RangeAttribute_t261 *)il2cpp_codegen_object_new (RangeAttribute_t261_il2cpp_TypeInfo_var);
		RangeAttribute__ctor_m1025(tmp, 0.0f, 15.0f, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* RangeAttribute_t261_il2cpp_TypeInfo_var;
void TiltShift_t126_CustomAttributesCacheGenerator_maxBlurSize(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RangeAttribute_t261_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RangeAttribute_t261 * tmp;
		tmp = (RangeAttribute_t261 *)il2cpp_codegen_object_new (RangeAttribute_t261_il2cpp_TypeInfo_var);
		RangeAttribute__ctor_m1025(tmp, 0.0f, 25.0f, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* RangeAttribute_t261_il2cpp_TypeInfo_var;
void TiltShift_t126_CustomAttributesCacheGenerator_downsample(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RangeAttribute_t261_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RangeAttribute_t261 * tmp;
		tmp = (RangeAttribute_t261 *)il2cpp_codegen_object_new (RangeAttribute_t261_il2cpp_TypeInfo_var);
		RangeAttribute__ctor_m1025(tmp, 0.0f, 1.0f, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* Camera_t27_0_0_0_var;
extern TypeInfo* ExecuteInEditMode_t262_il2cpp_TypeInfo_var;
extern TypeInfo* AddComponentMenu_t264_il2cpp_TypeInfo_var;
extern TypeInfo* RequireComponent_t259_il2cpp_TypeInfo_var;
void Tonemapping_t129_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Camera_t27_0_0_0_var = il2cpp_codegen_type_from_index(10);
		ExecuteInEditMode_t262_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(111);
		AddComponentMenu_t264_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		RequireComponent_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(108);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExecuteInEditMode_t262 * tmp;
		tmp = (ExecuteInEditMode_t262 *)il2cpp_codegen_object_new (ExecuteInEditMode_t262_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m1027(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AddComponentMenu_t264 * tmp;
		tmp = (AddComponentMenu_t264 *)il2cpp_codegen_object_new (AddComponentMenu_t264_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m1040(tmp, il2cpp_codegen_string_new_wrapper("Image Effects/Color Adjustments/Tonemapping"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		RequireComponent_t259 * tmp;
		tmp = (RequireComponent_t259 *)il2cpp_codegen_object_new (RequireComponent_t259_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m1023(tmp, il2cpp_codegen_type_get_object(Camera_t27_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.ImageEffectTransformsToLDR
#include "UnityEngine_UnityEngine_ImageEffectTransformsToLDR.h"
// UnityEngine.ImageEffectTransformsToLDR
#include "UnityEngine_UnityEngine_ImageEffectTransformsToLDRMethodDeclarations.h"
extern TypeInfo* ImageEffectTransformsToLDR_t268_il2cpp_TypeInfo_var;
void Tonemapping_t129_CustomAttributesCacheGenerator_Tonemapping_OnRenderImage_m380(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ImageEffectTransformsToLDR_t268_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(117);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ImageEffectTransformsToLDR_t268 * tmp;
		tmp = (ImageEffectTransformsToLDR_t268 *)il2cpp_codegen_object_new (ImageEffectTransformsToLDR_t268_il2cpp_TypeInfo_var);
		ImageEffectTransformsToLDR__ctor_m1044(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExecuteInEditMode_t262_il2cpp_TypeInfo_var;
extern TypeInfo* AddComponentMenu_t264_il2cpp_TypeInfo_var;
void Twirl_t131_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExecuteInEditMode_t262_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(111);
		AddComponentMenu_t264_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExecuteInEditMode_t262 * tmp;
		tmp = (ExecuteInEditMode_t262 *)il2cpp_codegen_object_new (ExecuteInEditMode_t262_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m1027(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AddComponentMenu_t264 * tmp;
		tmp = (AddComponentMenu_t264 *)il2cpp_codegen_object_new (AddComponentMenu_t264_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m1040(tmp, il2cpp_codegen_string_new_wrapper("Image Effects/Displacement/Twirl"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* Camera_t27_0_0_0_var;
extern TypeInfo* ExecuteInEditMode_t262_il2cpp_TypeInfo_var;
extern TypeInfo* AddComponentMenu_t264_il2cpp_TypeInfo_var;
extern TypeInfo* RequireComponent_t259_il2cpp_TypeInfo_var;
void VignetteAndChromaticAberration_t133_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Camera_t27_0_0_0_var = il2cpp_codegen_type_from_index(10);
		ExecuteInEditMode_t262_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(111);
		AddComponentMenu_t264_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		RequireComponent_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(108);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExecuteInEditMode_t262 * tmp;
		tmp = (ExecuteInEditMode_t262 *)il2cpp_codegen_object_new (ExecuteInEditMode_t262_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m1027(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AddComponentMenu_t264 * tmp;
		tmp = (AddComponentMenu_t264 *)il2cpp_codegen_object_new (AddComponentMenu_t264_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m1040(tmp, il2cpp_codegen_string_new_wrapper("Image Effects/Camera/Vignette and Chromatic Aberration"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		RequireComponent_t259 * tmp;
		tmp = (RequireComponent_t259 *)il2cpp_codegen_object_new (RequireComponent_t259_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m1023(tmp, il2cpp_codegen_type_get_object(Camera_t27_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExecuteInEditMode_t262_il2cpp_TypeInfo_var;
extern TypeInfo* AddComponentMenu_t264_il2cpp_TypeInfo_var;
void Vortex_t134_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExecuteInEditMode_t262_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(111);
		AddComponentMenu_t264_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExecuteInEditMode_t262 * tmp;
		tmp = (ExecuteInEditMode_t262 *)il2cpp_codegen_object_new (ExecuteInEditMode_t262_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m1027(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AddComponentMenu_t264 * tmp;
		tmp = (AddComponentMenu_t264 *)il2cpp_codegen_object_new (AddComponentMenu_t264_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m1040(tmp, il2cpp_codegen_string_new_wrapper("Image Effects/Displacement/Vortex"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.SphereCollider
#include "UnityEngine_UnityEngine_SphereCollider.h"
extern const Il2CppType* SphereCollider_t136_0_0_0_var;
extern TypeInfo* RequireComponent_t259_il2cpp_TypeInfo_var;
void AfterburnerPhysicsForce_t137_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SphereCollider_t136_0_0_0_var = il2cpp_codegen_type_from_index(65);
		RequireComponent_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(108);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RequireComponent_t259 * tmp;
		tmp = (RequireComponent_t259 *)il2cpp_codegen_object_new (RequireComponent_t259_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m1023(tmp, il2cpp_codegen_type_get_object(SphereCollider_t136_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Diagnostics.DebuggerHiddenAttribute
#include "mscorlib_System_Diagnostics_DebuggerHiddenAttribute.h"
// System.Diagnostics.DebuggerHiddenAttribute
#include "mscorlib_System_Diagnostics_DebuggerHiddenAttributeMethodDeclarations.h"
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void ExplosionFireAndDebris_t139_CustomAttributesCacheGenerator_ExplosionFireAndDebris_Start_m405(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void U3CStartU3Ec__Iterator0_t140_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CStartU3Ec__Iterator0_t140_CustomAttributesCacheGenerator_U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m399(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CStartU3Ec__Iterator0_t140_CustomAttributesCacheGenerator_U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m400(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CStartU3Ec__Iterator0_t140_CustomAttributesCacheGenerator_U3CStartU3Ec__Iterator0_Dispose_m402(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CStartU3Ec__Iterator0_t140_CustomAttributesCacheGenerator_U3CStartU3Ec__Iterator0_Reset_m403(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void ExplosionPhysicsForce_t143_CustomAttributesCacheGenerator_ExplosionPhysicsForce_Start_m414(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void U3CStartU3Ec__Iterator1_t144_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CStartU3Ec__Iterator1_t144_CustomAttributesCacheGenerator_U3CStartU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m408(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CStartU3Ec__Iterator1_t144_CustomAttributesCacheGenerator_U3CStartU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m409(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CStartU3Ec__Iterator1_t144_CustomAttributesCacheGenerator_U3CStartU3Ec__Iterator1_Dispose_m411(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CStartU3Ec__Iterator1_t144_CustomAttributesCacheGenerator_U3CStartU3Ec__Iterator1_Reset_m412(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void Explosive_t147_CustomAttributesCacheGenerator_Explosive_OnCollisionEnter_m423(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void U3COnCollisionEnterU3Ec__Iterator2_t148_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3COnCollisionEnterU3Ec__Iterator2_t148_CustomAttributesCacheGenerator_U3COnCollisionEnterU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m416(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3COnCollisionEnterU3Ec__Iterator2_t148_CustomAttributesCacheGenerator_U3COnCollisionEnterU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m417(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3COnCollisionEnterU3Ec__Iterator2_t148_CustomAttributesCacheGenerator_U3COnCollisionEnterU3Ec__Iterator2_Dispose_m419(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3COnCollisionEnterU3Ec__Iterator2_t148_CustomAttributesCacheGenerator_U3COnCollisionEnterU3Ec__Iterator2_Reset_m420(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void AutoMobileShaderSwitch_t169_CustomAttributesCacheGenerator_m_ReplacementList(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void DragRigidbody_t174_CustomAttributesCacheGenerator_DragRigidbody_DragObject_m468(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void U3CDragObjectU3Ec__Iterator3_t175_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CDragObjectU3Ec__Iterator3_t175_CustomAttributesCacheGenerator_U3CDragObjectU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m461(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CDragObjectU3Ec__Iterator3_t175_CustomAttributesCacheGenerator_U3CDragObjectU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m462(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CDragObjectU3Ec__Iterator3_t175_CustomAttributesCacheGenerator_U3CDragObjectU3Ec__Iterator3_Dispose_m464(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CDragObjectU3Ec__Iterator3_t175_CustomAttributesCacheGenerator_U3CDragObjectU3Ec__Iterator3_Reset_m465(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.HideInInspector
#include "UnityEngine_UnityEngine_HideInInspector.h"
// UnityEngine.HideInInspector
#include "UnityEngine_UnityEngine_HideInInspectorMethodDeclarations.h"
extern TypeInfo* HideInInspector_t270_il2cpp_TypeInfo_var;
void FOVKick_t178_CustomAttributesCacheGenerator_originalFov(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t270_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(119);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t270 * tmp;
		tmp = (HideInInspector_t270 *)il2cpp_codegen_object_new (HideInInspector_t270_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m1046(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void FOVKick_t178_CustomAttributesCacheGenerator_FOVKick_FOVKickUp_m489(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void FOVKick_t178_CustomAttributesCacheGenerator_FOVKick_FOVKickDown_m490(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void U3CFOVKickUpU3Ec__Iterator4_t179_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CFOVKickUpU3Ec__Iterator4_t179_CustomAttributesCacheGenerator_U3CFOVKickUpU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m474(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CFOVKickUpU3Ec__Iterator4_t179_CustomAttributesCacheGenerator_U3CFOVKickUpU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m475(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CFOVKickUpU3Ec__Iterator4_t179_CustomAttributesCacheGenerator_U3CFOVKickUpU3Ec__Iterator4_Dispose_m477(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CFOVKickUpU3Ec__Iterator4_t179_CustomAttributesCacheGenerator_U3CFOVKickUpU3Ec__Iterator4_Reset_m478(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void U3CFOVKickDownU3Ec__Iterator5_t180_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CFOVKickDownU3Ec__Iterator5_t180_CustomAttributesCacheGenerator_U3CFOVKickDownU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m480(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CFOVKickDownU3Ec__Iterator5_t180_CustomAttributesCacheGenerator_U3CFOVKickDownU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m481(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CFOVKickDownU3Ec__Iterator5_t180_CustomAttributesCacheGenerator_U3CFOVKickDownU3Ec__Iterator5_Dispose_m483(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CFOVKickDownU3Ec__Iterator5_t180_CustomAttributesCacheGenerator_U3CFOVKickDownU3Ec__Iterator5_Reset_m484(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.GUIText
#include "UnityEngine_UnityEngine_GUIText.h"
extern const Il2CppType* GUIText_t181_0_0_0_var;
extern TypeInfo* RequireComponent_t259_il2cpp_TypeInfo_var;
void FPSCounter_t182_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIText_t181_0_0_0_var = il2cpp_codegen_type_from_index(91);
		RequireComponent_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(108);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RequireComponent_t259 * tmp;
		tmp = (RequireComponent_t259 *)il2cpp_codegen_object_new (RequireComponent_t259_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m1023(tmp, il2cpp_codegen_type_get_object(GUIText_t181_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.GUITexture
#include "UnityEngine_UnityEngine_GUITexture.h"
extern const Il2CppType* GUITexture_t271_0_0_0_var;
extern TypeInfo* RequireComponent_t259_il2cpp_TypeInfo_var;
void ForcedReset_t184_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUITexture_t271_0_0_0_var = il2cpp_codegen_type_from_index(120);
		RequireComponent_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(108);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RequireComponent_t259 * tmp;
		tmp = (RequireComponent_t259 *)il2cpp_codegen_object_new (RequireComponent_t259_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m1023(tmp, il2cpp_codegen_type_get_object(GUITexture_t271_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void LerpControlledBob_t185_CustomAttributesCacheGenerator_LerpControlledBob_DoBobCycle_m506(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void U3CDoBobCycleU3Ec__Iterator6_t186_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CDoBobCycleU3Ec__Iterator6_t186_CustomAttributesCacheGenerator_U3CDoBobCycleU3Ec__Iterator6_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m499(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CDoBobCycleU3Ec__Iterator6_t186_CustomAttributesCacheGenerator_U3CDoBobCycleU3Ec__Iterator6_System_Collections_IEnumerator_get_Current_m500(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CDoBobCycleU3Ec__Iterator6_t186_CustomAttributesCacheGenerator_U3CDoBobCycleU3Ec__Iterator6_Dispose_m502(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CDoBobCycleU3Ec__Iterator6_t186_CustomAttributesCacheGenerator_U3CDoBobCycleU3Ec__Iterator6_Reset_m503(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void ObjectResetter_t149_CustomAttributesCacheGenerator_ObjectResetter_ResetCoroutine_m516(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void U3CResetCoroutineU3Ec__Iterator7_t187_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CResetCoroutineU3Ec__Iterator7_t187_CustomAttributesCacheGenerator_U3CResetCoroutineU3Ec__Iterator7_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m508(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CResetCoroutineU3Ec__Iterator7_t187_CustomAttributesCacheGenerator_U3CResetCoroutineU3Ec__Iterator7_System_Collections_IEnumerator_get_Current_m509(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CResetCoroutineU3Ec__Iterator7_t187_CustomAttributesCacheGenerator_U3CResetCoroutineU3Ec__Iterator7_Dispose_m511(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CResetCoroutineU3Ec__Iterator7_t187_CustomAttributesCacheGenerator_U3CResetCoroutineU3Ec__Iterator7_Reset_m512(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void ParticleSystemDestroyer_t189_CustomAttributesCacheGenerator_ParticleSystemDestroyer_Start_m524(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void U3CStartU3Ec__Iterator8_t190_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CStartU3Ec__Iterator8_t190_CustomAttributesCacheGenerator_U3CStartU3Ec__Iterator8_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m518(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CStartU3Ec__Iterator8_t190_CustomAttributesCacheGenerator_U3CStartU3Ec__Iterator8_System_Collections_IEnumerator_get_Current_m519(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CStartU3Ec__Iterator8_t190_CustomAttributesCacheGenerator_U3CStartU3Ec__Iterator8_Dispose_m521(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CStartU3Ec__Iterator8_t190_CustomAttributesCacheGenerator_U3CStartU3Ec__Iterator8_Reset_m522(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void PlatformSpecificContent_t194_CustomAttributesCacheGenerator_m_BuildTargetGroup(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void PlatformSpecificContent_t194_CustomAttributesCacheGenerator_m_Content(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void PlatformSpecificContent_t194_CustomAttributesCacheGenerator_m_MonoBehaviours(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void PlatformSpecificContent_t194_CustomAttributesCacheGenerator_m_ChildrenOfThisObject(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void SmoothFollow_t197_CustomAttributesCacheGenerator_target(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void SmoothFollow_t197_CustomAttributesCacheGenerator_distance(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void SmoothFollow_t197_CustomAttributesCacheGenerator_height(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void SmoothFollow_t197_CustomAttributesCacheGenerator_rotationDamping(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void SmoothFollow_t197_CustomAttributesCacheGenerator_heightDamping(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void TimedObjectActivator_t205_CustomAttributesCacheGenerator_TimedObjectActivator_Activate_m561(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void TimedObjectActivator_t205_CustomAttributesCacheGenerator_TimedObjectActivator_Deactivate_m562(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void TimedObjectActivator_t205_CustomAttributesCacheGenerator_TimedObjectActivator_ReloadLevel_m563(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void U3CActivateU3Ec__Iterator9_t202_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CActivateU3Ec__Iterator9_t202_CustomAttributesCacheGenerator_U3CActivateU3Ec__Iterator9_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m542(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CActivateU3Ec__Iterator9_t202_CustomAttributesCacheGenerator_U3CActivateU3Ec__Iterator9_System_Collections_IEnumerator_get_Current_m543(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CActivateU3Ec__Iterator9_t202_CustomAttributesCacheGenerator_U3CActivateU3Ec__Iterator9_Dispose_m545(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CActivateU3Ec__Iterator9_t202_CustomAttributesCacheGenerator_U3CActivateU3Ec__Iterator9_Reset_m546(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void U3CDeactivateU3Ec__IteratorA_t203_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CDeactivateU3Ec__IteratorA_t203_CustomAttributesCacheGenerator_U3CDeactivateU3Ec__IteratorA_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m548(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CDeactivateU3Ec__IteratorA_t203_CustomAttributesCacheGenerator_U3CDeactivateU3Ec__IteratorA_System_Collections_IEnumerator_get_Current_m549(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CDeactivateU3Ec__IteratorA_t203_CustomAttributesCacheGenerator_U3CDeactivateU3Ec__IteratorA_Dispose_m551(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CDeactivateU3Ec__IteratorA_t203_CustomAttributesCacheGenerator_U3CDeactivateU3Ec__IteratorA_Reset_m552(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void U3CReloadLevelU3Ec__IteratorB_t204_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CReloadLevelU3Ec__IteratorB_t204_CustomAttributesCacheGenerator_U3CReloadLevelU3Ec__IteratorB_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m554(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CReloadLevelU3Ec__IteratorB_t204_CustomAttributesCacheGenerator_U3CReloadLevelU3Ec__IteratorB_System_Collections_IEnumerator_get_Current_m555(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CReloadLevelU3Ec__IteratorB_t204_CustomAttributesCacheGenerator_U3CReloadLevelU3Ec__IteratorB_Dispose_m557(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CReloadLevelU3Ec__IteratorB_t204_CustomAttributesCacheGenerator_U3CReloadLevelU3Ec__IteratorB_Reset_m558(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void TimedObjectDestructor_t206_CustomAttributesCacheGenerator_m_TimeOut(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void TimedObjectDestructor_t206_CustomAttributesCacheGenerator_m_DetachChildren(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void WaypointCircuit_t207_CustomAttributesCacheGenerator_smoothRoute(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void WaypointCircuit_t207_CustomAttributesCacheGenerator_U3CLengthU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void WaypointCircuit_t207_CustomAttributesCacheGenerator_WaypointCircuit_get_Length_m570(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void WaypointCircuit_t207_CustomAttributesCacheGenerator_WaypointCircuit_set_Length_m571(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void WaypointProgressTracker_t213_CustomAttributesCacheGenerator_circuit(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void WaypointProgressTracker_t213_CustomAttributesCacheGenerator_lookAheadForTargetOffset(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void WaypointProgressTracker_t213_CustomAttributesCacheGenerator_lookAheadForTargetFactor(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void WaypointProgressTracker_t213_CustomAttributesCacheGenerator_lookAheadForSpeedOffset(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void WaypointProgressTracker_t213_CustomAttributesCacheGenerator_lookAheadForSpeedFactor(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void WaypointProgressTracker_t213_CustomAttributesCacheGenerator_progressStyle(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void WaypointProgressTracker_t213_CustomAttributesCacheGenerator_pointToPointThreshold(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void WaypointProgressTracker_t213_CustomAttributesCacheGenerator_U3CtargetPointU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void WaypointProgressTracker_t213_CustomAttributesCacheGenerator_U3CspeedPointU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void WaypointProgressTracker_t213_CustomAttributesCacheGenerator_U3CprogressPointU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void WaypointProgressTracker_t213_CustomAttributesCacheGenerator_WaypointProgressTracker_get_targetPoint_m582(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void WaypointProgressTracker_t213_CustomAttributesCacheGenerator_WaypointProgressTracker_set_targetPoint_m583(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void WaypointProgressTracker_t213_CustomAttributesCacheGenerator_WaypointProgressTracker_get_speedPoint_m584(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void WaypointProgressTracker_t213_CustomAttributesCacheGenerator_WaypointProgressTracker_set_speedPoint_m585(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void WaypointProgressTracker_t213_CustomAttributesCacheGenerator_WaypointProgressTracker_get_progressPoint_m586(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void WaypointProgressTracker_t213_CustomAttributesCacheGenerator_WaypointProgressTracker_set_progressPoint_m587(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharpU2Dfirstpass_Assembly_AttributeGenerators[228] = 
{
	NULL,
	g_AssemblyU2DCSharpU2Dfirstpass_Assembly_CustomAttributesCacheGenerator,
	Platformer2DUserControl_t8_CustomAttributesCacheGenerator,
	PlatformerCharacter2D_t7_CustomAttributesCacheGenerator_m_MaxSpeed,
	PlatformerCharacter2D_t7_CustomAttributesCacheGenerator_m_JumpForce,
	PlatformerCharacter2D_t7_CustomAttributesCacheGenerator_m_CrouchSpeed,
	PlatformerCharacter2D_t7_CustomAttributesCacheGenerator_m_AirControl,
	PlatformerCharacter2D_t7_CustomAttributesCacheGenerator_m_WhatIsGround,
	AbstractTargetFollower_t15_CustomAttributesCacheGenerator_m_Target,
	AbstractTargetFollower_t15_CustomAttributesCacheGenerator_m_AutoTargetPlayer,
	AbstractTargetFollower_t15_CustomAttributesCacheGenerator_m_UpdateType,
	AutoCam_t16_CustomAttributesCacheGenerator,
	AutoCam_t16_CustomAttributesCacheGenerator_m_MoveSpeed,
	AutoCam_t16_CustomAttributesCacheGenerator_m_TurnSpeed,
	AutoCam_t16_CustomAttributesCacheGenerator_m_RollSpeed,
	AutoCam_t16_CustomAttributesCacheGenerator_m_FollowVelocity,
	AutoCam_t16_CustomAttributesCacheGenerator_m_FollowTilt,
	AutoCam_t16_CustomAttributesCacheGenerator_m_SpinTurnLimit,
	AutoCam_t16_CustomAttributesCacheGenerator_m_TargetVelocityLowerLimit,
	AutoCam_t16_CustomAttributesCacheGenerator_m_SmoothTurnTime,
	FreeLookCam_t18_CustomAttributesCacheGenerator_m_MoveSpeed,
	FreeLookCam_t18_CustomAttributesCacheGenerator_m_TurnSpeed,
	FreeLookCam_t18_CustomAttributesCacheGenerator_m_TurnSmoothing,
	FreeLookCam_t18_CustomAttributesCacheGenerator_m_TiltMax,
	FreeLookCam_t18_CustomAttributesCacheGenerator_m_TiltMin,
	FreeLookCam_t18_CustomAttributesCacheGenerator_m_LockCursor,
	FreeLookCam_t18_CustomAttributesCacheGenerator_m_VerticalAutoReturn,
	HandHeldCam_t20_CustomAttributesCacheGenerator_m_SwaySpeed,
	HandHeldCam_t20_CustomAttributesCacheGenerator_m_BaseSwayAmount,
	HandHeldCam_t20_CustomAttributesCacheGenerator_m_TrackingSwayAmount,
	HandHeldCam_t20_CustomAttributesCacheGenerator_m_TrackingBias,
	LookatTarget_t21_CustomAttributesCacheGenerator_m_RotationRange,
	LookatTarget_t21_CustomAttributesCacheGenerator_m_FollowSpeed,
	ProtectCameraFromWallClip_t25_CustomAttributesCacheGenerator_U3CprotectingU3Ek__BackingField,
	ProtectCameraFromWallClip_t25_CustomAttributesCacheGenerator_ProtectCameraFromWallClip_get_protecting_m46,
	ProtectCameraFromWallClip_t25_CustomAttributesCacheGenerator_ProtectCameraFromWallClip_set_protecting_m47,
	TargetFieldOfView_t28_CustomAttributesCacheGenerator_m_FovAdjustTime,
	TargetFieldOfView_t28_CustomAttributesCacheGenerator_m_ZoomAmountMultiplier,
	TargetFieldOfView_t28_CustomAttributesCacheGenerator_m_IncludeEffectsInSize,
	VirtualAxis_t30_CustomAttributesCacheGenerator_U3CnameU3Ek__BackingField,
	VirtualAxis_t30_CustomAttributesCacheGenerator_U3CmatchWithInputManagerU3Ek__BackingField,
	VirtualAxis_t30_CustomAttributesCacheGenerator_VirtualAxis_get_name_m71,
	VirtualAxis_t30_CustomAttributesCacheGenerator_VirtualAxis_set_name_m72,
	VirtualAxis_t30_CustomAttributesCacheGenerator_VirtualAxis_get_matchWithInputManager_m73,
	VirtualAxis_t30_CustomAttributesCacheGenerator_VirtualAxis_set_matchWithInputManager_m74,
	VirtualButton_t33_CustomAttributesCacheGenerator_U3CnameU3Ek__BackingField,
	VirtualButton_t33_CustomAttributesCacheGenerator_U3CmatchWithInputManagerU3Ek__BackingField,
	VirtualButton_t33_CustomAttributesCacheGenerator_VirtualButton_get_name_m81,
	VirtualButton_t33_CustomAttributesCacheGenerator_VirtualButton_set_name_m82,
	VirtualButton_t33_CustomAttributesCacheGenerator_VirtualButton_get_matchWithInputManager_m83,
	VirtualButton_t33_CustomAttributesCacheGenerator_VirtualButton_set_matchWithInputManager_m84,
	MobileControlRig_t39_CustomAttributesCacheGenerator,
	TouchPad_t49_CustomAttributesCacheGenerator,
	VirtualInput_t34_CustomAttributesCacheGenerator_U3CvirtualMousePositionU3Ek__BackingField,
	VirtualInput_t34_CustomAttributesCacheGenerator_VirtualInput_get_virtualMousePosition_m171,
	VirtualInput_t34_CustomAttributesCacheGenerator_VirtualInput_set_virtualMousePosition_m172,
	Antialiasing_t56_CustomAttributesCacheGenerator,
	Bloom_t64_CustomAttributesCacheGenerator,
	BloomAndFlares_t70_CustomAttributesCacheGenerator,
	BloomOptimized_t73_CustomAttributesCacheGenerator,
	BloomOptimized_t73_CustomAttributesCacheGenerator_threshold,
	BloomOptimized_t73_CustomAttributesCacheGenerator_intensity,
	BloomOptimized_t73_CustomAttributesCacheGenerator_blurSize,
	BloomOptimized_t73_CustomAttributesCacheGenerator_blurIterations,
	Blur_t74_CustomAttributesCacheGenerator,
	BlurOptimized_t76_CustomAttributesCacheGenerator,
	BlurOptimized_t76_CustomAttributesCacheGenerator_downsample,
	BlurOptimized_t76_CustomAttributesCacheGenerator_blurSize,
	BlurOptimized_t76_CustomAttributesCacheGenerator_blurIterations,
	CameraMotionBlur_t79_CustomAttributesCacheGenerator,
	ColorCorrectionCurves_t83_CustomAttributesCacheGenerator,
	ColorCorrectionLookup_t85_CustomAttributesCacheGenerator,
	ColorCorrectionRamp_t87_CustomAttributesCacheGenerator,
	ContrastEnhance_t89_CustomAttributesCacheGenerator,
	ContrastStretch_t91_CustomAttributesCacheGenerator,
	CreaseShading_t92_CustomAttributesCacheGenerator,
	DepthOfField_t96_CustomAttributesCacheGenerator,
	DepthOfFieldDeprecated_t102_CustomAttributesCacheGenerator,
	EdgeDetection_t104_CustomAttributesCacheGenerator,
	EdgeDetection_t104_CustomAttributesCacheGenerator_EdgeDetection_OnRenderImage_m294,
	Fisheye_t105_CustomAttributesCacheGenerator,
	GlobalFog_t106_CustomAttributesCacheGenerator,
	GlobalFog_t106_CustomAttributesCacheGenerator_distanceFog,
	GlobalFog_t106_CustomAttributesCacheGenerator_useRadialDistance,
	GlobalFog_t106_CustomAttributesCacheGenerator_heightFog,
	GlobalFog_t106_CustomAttributesCacheGenerator_height,
	GlobalFog_t106_CustomAttributesCacheGenerator_heightDensity,
	GlobalFog_t106_CustomAttributesCacheGenerator_startDistance,
	GlobalFog_t106_CustomAttributesCacheGenerator_GlobalFog_OnRenderImage_m300,
	Grayscale_t107_CustomAttributesCacheGenerator,
	ImageEffectBase_t88_CustomAttributesCacheGenerator,
	ImageEffects_t108_CustomAttributesCacheGenerator,
	ImageEffects_t108_CustomAttributesCacheGenerator_ImageEffects_Blit_m310,
	ImageEffects_t108_CustomAttributesCacheGenerator_ImageEffects_BlitWithMaterial_m311,
	MotionBlur_t109_CustomAttributesCacheGenerator,
	NoiseAndGrain_t110_CustomAttributesCacheGenerator,
	NoiseAndScratches_t111_CustomAttributesCacheGenerator,
	PostEffectsBase_t57_CustomAttributesCacheGenerator,
	PostEffectsHelper_t112_CustomAttributesCacheGenerator,
	ScreenOverlay_t116_CustomAttributesCacheGenerator,
	ScreenSpaceAmbientObscurance_t117_CustomAttributesCacheGenerator,
	ScreenSpaceAmbientObscurance_t117_CustomAttributesCacheGenerator_intensity,
	ScreenSpaceAmbientObscurance_t117_CustomAttributesCacheGenerator_radius,
	ScreenSpaceAmbientObscurance_t117_CustomAttributesCacheGenerator_blurIterations,
	ScreenSpaceAmbientObscurance_t117_CustomAttributesCacheGenerator_blurFilterDistance,
	ScreenSpaceAmbientObscurance_t117_CustomAttributesCacheGenerator_downsample,
	ScreenSpaceAmbientObscurance_t117_CustomAttributesCacheGenerator_ScreenSpaceAmbientObscurance_OnRenderImage_m358,
	ScreenSpaceAmbientOcclusion_t119_CustomAttributesCacheGenerator,
	ScreenSpaceAmbientOcclusion_t119_CustomAttributesCacheGenerator_ScreenSpaceAmbientOcclusion_OnRenderImage_m366,
	SepiaTone_t120_CustomAttributesCacheGenerator,
	SunShafts_t123_CustomAttributesCacheGenerator,
	TiltShift_t126_CustomAttributesCacheGenerator,
	TiltShift_t126_CustomAttributesCacheGenerator_blurArea,
	TiltShift_t126_CustomAttributesCacheGenerator_maxBlurSize,
	TiltShift_t126_CustomAttributesCacheGenerator_downsample,
	Tonemapping_t129_CustomAttributesCacheGenerator,
	Tonemapping_t129_CustomAttributesCacheGenerator_Tonemapping_OnRenderImage_m380,
	Twirl_t131_CustomAttributesCacheGenerator,
	VignetteAndChromaticAberration_t133_CustomAttributesCacheGenerator,
	Vortex_t134_CustomAttributesCacheGenerator,
	AfterburnerPhysicsForce_t137_CustomAttributesCacheGenerator,
	ExplosionFireAndDebris_t139_CustomAttributesCacheGenerator_ExplosionFireAndDebris_Start_m405,
	U3CStartU3Ec__Iterator0_t140_CustomAttributesCacheGenerator,
	U3CStartU3Ec__Iterator0_t140_CustomAttributesCacheGenerator_U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m399,
	U3CStartU3Ec__Iterator0_t140_CustomAttributesCacheGenerator_U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m400,
	U3CStartU3Ec__Iterator0_t140_CustomAttributesCacheGenerator_U3CStartU3Ec__Iterator0_Dispose_m402,
	U3CStartU3Ec__Iterator0_t140_CustomAttributesCacheGenerator_U3CStartU3Ec__Iterator0_Reset_m403,
	ExplosionPhysicsForce_t143_CustomAttributesCacheGenerator_ExplosionPhysicsForce_Start_m414,
	U3CStartU3Ec__Iterator1_t144_CustomAttributesCacheGenerator,
	U3CStartU3Ec__Iterator1_t144_CustomAttributesCacheGenerator_U3CStartU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m408,
	U3CStartU3Ec__Iterator1_t144_CustomAttributesCacheGenerator_U3CStartU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m409,
	U3CStartU3Ec__Iterator1_t144_CustomAttributesCacheGenerator_U3CStartU3Ec__Iterator1_Dispose_m411,
	U3CStartU3Ec__Iterator1_t144_CustomAttributesCacheGenerator_U3CStartU3Ec__Iterator1_Reset_m412,
	Explosive_t147_CustomAttributesCacheGenerator_Explosive_OnCollisionEnter_m423,
	U3COnCollisionEnterU3Ec__Iterator2_t148_CustomAttributesCacheGenerator,
	U3COnCollisionEnterU3Ec__Iterator2_t148_CustomAttributesCacheGenerator_U3COnCollisionEnterU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m416,
	U3COnCollisionEnterU3Ec__Iterator2_t148_CustomAttributesCacheGenerator_U3COnCollisionEnterU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m417,
	U3COnCollisionEnterU3Ec__Iterator2_t148_CustomAttributesCacheGenerator_U3COnCollisionEnterU3Ec__Iterator2_Dispose_m419,
	U3COnCollisionEnterU3Ec__Iterator2_t148_CustomAttributesCacheGenerator_U3COnCollisionEnterU3Ec__Iterator2_Reset_m420,
	AutoMobileShaderSwitch_t169_CustomAttributesCacheGenerator_m_ReplacementList,
	DragRigidbody_t174_CustomAttributesCacheGenerator_DragRigidbody_DragObject_m468,
	U3CDragObjectU3Ec__Iterator3_t175_CustomAttributesCacheGenerator,
	U3CDragObjectU3Ec__Iterator3_t175_CustomAttributesCacheGenerator_U3CDragObjectU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m461,
	U3CDragObjectU3Ec__Iterator3_t175_CustomAttributesCacheGenerator_U3CDragObjectU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m462,
	U3CDragObjectU3Ec__Iterator3_t175_CustomAttributesCacheGenerator_U3CDragObjectU3Ec__Iterator3_Dispose_m464,
	U3CDragObjectU3Ec__Iterator3_t175_CustomAttributesCacheGenerator_U3CDragObjectU3Ec__Iterator3_Reset_m465,
	FOVKick_t178_CustomAttributesCacheGenerator_originalFov,
	FOVKick_t178_CustomAttributesCacheGenerator_FOVKick_FOVKickUp_m489,
	FOVKick_t178_CustomAttributesCacheGenerator_FOVKick_FOVKickDown_m490,
	U3CFOVKickUpU3Ec__Iterator4_t179_CustomAttributesCacheGenerator,
	U3CFOVKickUpU3Ec__Iterator4_t179_CustomAttributesCacheGenerator_U3CFOVKickUpU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m474,
	U3CFOVKickUpU3Ec__Iterator4_t179_CustomAttributesCacheGenerator_U3CFOVKickUpU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m475,
	U3CFOVKickUpU3Ec__Iterator4_t179_CustomAttributesCacheGenerator_U3CFOVKickUpU3Ec__Iterator4_Dispose_m477,
	U3CFOVKickUpU3Ec__Iterator4_t179_CustomAttributesCacheGenerator_U3CFOVKickUpU3Ec__Iterator4_Reset_m478,
	U3CFOVKickDownU3Ec__Iterator5_t180_CustomAttributesCacheGenerator,
	U3CFOVKickDownU3Ec__Iterator5_t180_CustomAttributesCacheGenerator_U3CFOVKickDownU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m480,
	U3CFOVKickDownU3Ec__Iterator5_t180_CustomAttributesCacheGenerator_U3CFOVKickDownU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m481,
	U3CFOVKickDownU3Ec__Iterator5_t180_CustomAttributesCacheGenerator_U3CFOVKickDownU3Ec__Iterator5_Dispose_m483,
	U3CFOVKickDownU3Ec__Iterator5_t180_CustomAttributesCacheGenerator_U3CFOVKickDownU3Ec__Iterator5_Reset_m484,
	FPSCounter_t182_CustomAttributesCacheGenerator,
	ForcedReset_t184_CustomAttributesCacheGenerator,
	LerpControlledBob_t185_CustomAttributesCacheGenerator_LerpControlledBob_DoBobCycle_m506,
	U3CDoBobCycleU3Ec__Iterator6_t186_CustomAttributesCacheGenerator,
	U3CDoBobCycleU3Ec__Iterator6_t186_CustomAttributesCacheGenerator_U3CDoBobCycleU3Ec__Iterator6_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m499,
	U3CDoBobCycleU3Ec__Iterator6_t186_CustomAttributesCacheGenerator_U3CDoBobCycleU3Ec__Iterator6_System_Collections_IEnumerator_get_Current_m500,
	U3CDoBobCycleU3Ec__Iterator6_t186_CustomAttributesCacheGenerator_U3CDoBobCycleU3Ec__Iterator6_Dispose_m502,
	U3CDoBobCycleU3Ec__Iterator6_t186_CustomAttributesCacheGenerator_U3CDoBobCycleU3Ec__Iterator6_Reset_m503,
	ObjectResetter_t149_CustomAttributesCacheGenerator_ObjectResetter_ResetCoroutine_m516,
	U3CResetCoroutineU3Ec__Iterator7_t187_CustomAttributesCacheGenerator,
	U3CResetCoroutineU3Ec__Iterator7_t187_CustomAttributesCacheGenerator_U3CResetCoroutineU3Ec__Iterator7_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m508,
	U3CResetCoroutineU3Ec__Iterator7_t187_CustomAttributesCacheGenerator_U3CResetCoroutineU3Ec__Iterator7_System_Collections_IEnumerator_get_Current_m509,
	U3CResetCoroutineU3Ec__Iterator7_t187_CustomAttributesCacheGenerator_U3CResetCoroutineU3Ec__Iterator7_Dispose_m511,
	U3CResetCoroutineU3Ec__Iterator7_t187_CustomAttributesCacheGenerator_U3CResetCoroutineU3Ec__Iterator7_Reset_m512,
	ParticleSystemDestroyer_t189_CustomAttributesCacheGenerator_ParticleSystemDestroyer_Start_m524,
	U3CStartU3Ec__Iterator8_t190_CustomAttributesCacheGenerator,
	U3CStartU3Ec__Iterator8_t190_CustomAttributesCacheGenerator_U3CStartU3Ec__Iterator8_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m518,
	U3CStartU3Ec__Iterator8_t190_CustomAttributesCacheGenerator_U3CStartU3Ec__Iterator8_System_Collections_IEnumerator_get_Current_m519,
	U3CStartU3Ec__Iterator8_t190_CustomAttributesCacheGenerator_U3CStartU3Ec__Iterator8_Dispose_m521,
	U3CStartU3Ec__Iterator8_t190_CustomAttributesCacheGenerator_U3CStartU3Ec__Iterator8_Reset_m522,
	PlatformSpecificContent_t194_CustomAttributesCacheGenerator_m_BuildTargetGroup,
	PlatformSpecificContent_t194_CustomAttributesCacheGenerator_m_Content,
	PlatformSpecificContent_t194_CustomAttributesCacheGenerator_m_MonoBehaviours,
	PlatformSpecificContent_t194_CustomAttributesCacheGenerator_m_ChildrenOfThisObject,
	SmoothFollow_t197_CustomAttributesCacheGenerator_target,
	SmoothFollow_t197_CustomAttributesCacheGenerator_distance,
	SmoothFollow_t197_CustomAttributesCacheGenerator_height,
	SmoothFollow_t197_CustomAttributesCacheGenerator_rotationDamping,
	SmoothFollow_t197_CustomAttributesCacheGenerator_heightDamping,
	TimedObjectActivator_t205_CustomAttributesCacheGenerator_TimedObjectActivator_Activate_m561,
	TimedObjectActivator_t205_CustomAttributesCacheGenerator_TimedObjectActivator_Deactivate_m562,
	TimedObjectActivator_t205_CustomAttributesCacheGenerator_TimedObjectActivator_ReloadLevel_m563,
	U3CActivateU3Ec__Iterator9_t202_CustomAttributesCacheGenerator,
	U3CActivateU3Ec__Iterator9_t202_CustomAttributesCacheGenerator_U3CActivateU3Ec__Iterator9_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m542,
	U3CActivateU3Ec__Iterator9_t202_CustomAttributesCacheGenerator_U3CActivateU3Ec__Iterator9_System_Collections_IEnumerator_get_Current_m543,
	U3CActivateU3Ec__Iterator9_t202_CustomAttributesCacheGenerator_U3CActivateU3Ec__Iterator9_Dispose_m545,
	U3CActivateU3Ec__Iterator9_t202_CustomAttributesCacheGenerator_U3CActivateU3Ec__Iterator9_Reset_m546,
	U3CDeactivateU3Ec__IteratorA_t203_CustomAttributesCacheGenerator,
	U3CDeactivateU3Ec__IteratorA_t203_CustomAttributesCacheGenerator_U3CDeactivateU3Ec__IteratorA_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m548,
	U3CDeactivateU3Ec__IteratorA_t203_CustomAttributesCacheGenerator_U3CDeactivateU3Ec__IteratorA_System_Collections_IEnumerator_get_Current_m549,
	U3CDeactivateU3Ec__IteratorA_t203_CustomAttributesCacheGenerator_U3CDeactivateU3Ec__IteratorA_Dispose_m551,
	U3CDeactivateU3Ec__IteratorA_t203_CustomAttributesCacheGenerator_U3CDeactivateU3Ec__IteratorA_Reset_m552,
	U3CReloadLevelU3Ec__IteratorB_t204_CustomAttributesCacheGenerator,
	U3CReloadLevelU3Ec__IteratorB_t204_CustomAttributesCacheGenerator_U3CReloadLevelU3Ec__IteratorB_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m554,
	U3CReloadLevelU3Ec__IteratorB_t204_CustomAttributesCacheGenerator_U3CReloadLevelU3Ec__IteratorB_System_Collections_IEnumerator_get_Current_m555,
	U3CReloadLevelU3Ec__IteratorB_t204_CustomAttributesCacheGenerator_U3CReloadLevelU3Ec__IteratorB_Dispose_m557,
	U3CReloadLevelU3Ec__IteratorB_t204_CustomAttributesCacheGenerator_U3CReloadLevelU3Ec__IteratorB_Reset_m558,
	TimedObjectDestructor_t206_CustomAttributesCacheGenerator_m_TimeOut,
	TimedObjectDestructor_t206_CustomAttributesCacheGenerator_m_DetachChildren,
	WaypointCircuit_t207_CustomAttributesCacheGenerator_smoothRoute,
	WaypointCircuit_t207_CustomAttributesCacheGenerator_U3CLengthU3Ek__BackingField,
	WaypointCircuit_t207_CustomAttributesCacheGenerator_WaypointCircuit_get_Length_m570,
	WaypointCircuit_t207_CustomAttributesCacheGenerator_WaypointCircuit_set_Length_m571,
	WaypointProgressTracker_t213_CustomAttributesCacheGenerator_circuit,
	WaypointProgressTracker_t213_CustomAttributesCacheGenerator_lookAheadForTargetOffset,
	WaypointProgressTracker_t213_CustomAttributesCacheGenerator_lookAheadForTargetFactor,
	WaypointProgressTracker_t213_CustomAttributesCacheGenerator_lookAheadForSpeedOffset,
	WaypointProgressTracker_t213_CustomAttributesCacheGenerator_lookAheadForSpeedFactor,
	WaypointProgressTracker_t213_CustomAttributesCacheGenerator_progressStyle,
	WaypointProgressTracker_t213_CustomAttributesCacheGenerator_pointToPointThreshold,
	WaypointProgressTracker_t213_CustomAttributesCacheGenerator_U3CtargetPointU3Ek__BackingField,
	WaypointProgressTracker_t213_CustomAttributesCacheGenerator_U3CspeedPointU3Ek__BackingField,
	WaypointProgressTracker_t213_CustomAttributesCacheGenerator_U3CprogressPointU3Ek__BackingField,
	WaypointProgressTracker_t213_CustomAttributesCacheGenerator_WaypointProgressTracker_get_targetPoint_m582,
	WaypointProgressTracker_t213_CustomAttributesCacheGenerator_WaypointProgressTracker_set_targetPoint_m583,
	WaypointProgressTracker_t213_CustomAttributesCacheGenerator_WaypointProgressTracker_get_speedPoint_m584,
	WaypointProgressTracker_t213_CustomAttributesCacheGenerator_WaypointProgressTracker_set_speedPoint_m585,
	WaypointProgressTracker_t213_CustomAttributesCacheGenerator_WaypointProgressTracker_get_progressPoint_m586,
	WaypointProgressTracker_t213_CustomAttributesCacheGenerator_WaypointProgressTracker_set_progressPoint_m587,
};
