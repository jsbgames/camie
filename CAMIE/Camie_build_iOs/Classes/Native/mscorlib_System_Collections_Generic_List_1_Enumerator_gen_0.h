﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<SCR_Menu>
struct List_1_t377;
// SCR_Menu
struct SCR_Menu_t336;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<SCR_Menu>
struct  Enumerator_t423 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<SCR_Menu>::l
	List_1_t377 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<SCR_Menu>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<SCR_Menu>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<SCR_Menu>::current
	SCR_Menu_t336 * ___current_3;
};
