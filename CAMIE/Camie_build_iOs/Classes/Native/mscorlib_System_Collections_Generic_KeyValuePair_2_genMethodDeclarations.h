﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GameObject>
struct KeyValuePair_2_t419;
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t78;

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GameObject>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_5MethodDeclarations.h"
#define KeyValuePair_2__ctor_m15411(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t419 *, String_t*, GameObject_t78 *, const MethodInfo*))KeyValuePair_2__ctor_m13651_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GameObject>::get_Key()
#define KeyValuePair_2_get_Key_m15412(__this, method) (( String_t* (*) (KeyValuePair_2_t419 *, const MethodInfo*))KeyValuePair_2_get_Key_m13652_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GameObject>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m15413(__this, ___value, method) (( void (*) (KeyValuePair_2_t419 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m13653_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GameObject>::get_Value()
#define KeyValuePair_2_get_Value_m1745(__this, method) (( GameObject_t78 * (*) (KeyValuePair_2_t419 *, const MethodInfo*))KeyValuePair_2_get_Value_m13654_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GameObject>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m15414(__this, ___value, method) (( void (*) (KeyValuePair_2_t419 *, GameObject_t78 *, const MethodInfo*))KeyValuePair_2_set_Value_m13655_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GameObject>::ToString()
#define KeyValuePair_2_ToString_m15415(__this, method) (( String_t* (*) (KeyValuePair_2_t419 *, const MethodInfo*))KeyValuePair_2_ToString_m13656_gshared)(__this, method)
