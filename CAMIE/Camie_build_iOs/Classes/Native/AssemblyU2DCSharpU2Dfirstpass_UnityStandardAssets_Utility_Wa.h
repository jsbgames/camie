﻿#pragma once
#include <stdint.h>
// UnityStandardAssets.Utility.WaypointCircuit
struct WaypointCircuit_t207;
// UnityEngine.Transform[]
struct TransformU5BU5D_t141;
// System.Object
#include "mscorlib_System_Object.h"
// UnityStandardAssets.Utility.WaypointCircuit/WaypointList
struct  WaypointList_t208  : public Object_t
{
	// UnityStandardAssets.Utility.WaypointCircuit UnityStandardAssets.Utility.WaypointCircuit/WaypointList::circuit
	WaypointCircuit_t207 * ___circuit_0;
	// UnityEngine.Transform[] UnityStandardAssets.Utility.WaypointCircuit/WaypointList::items
	TransformU5BU5D_t141* ___items_1;
};
