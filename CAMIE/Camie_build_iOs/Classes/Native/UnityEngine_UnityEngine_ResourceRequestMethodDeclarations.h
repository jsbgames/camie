﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.ResourceRequest
struct ResourceRequest_t844;
// UnityEngine.Object
struct Object_t164;
struct Object_t164_marshaled;

// System.Void UnityEngine.ResourceRequest::.ctor()
extern "C" void ResourceRequest__ctor_m4230 (ResourceRequest_t844 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.ResourceRequest::get_asset()
extern "C" Object_t164 * ResourceRequest_get_asset_m4231 (ResourceRequest_t844 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
