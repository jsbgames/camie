﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.SpringJoint
struct SpringJoint_t176;

// System.Void UnityEngine.SpringJoint::set_spring(System.Single)
extern "C" void SpringJoint_set_spring_m985 (SpringJoint_t176 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SpringJoint::set_damper(System.Single)
extern "C" void SpringJoint_set_damper_m986 (SpringJoint_t176 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SpringJoint::set_maxDistance(System.Single)
extern "C" void SpringJoint_set_maxDistance_m987 (SpringJoint_t176 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
