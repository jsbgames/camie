﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementDefinition
struct ReplacementDefinition_t166;

// System.Void UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementDefinition::.ctor()
extern "C" void ReplacementDefinition__ctor_m444 (ReplacementDefinition_t166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
