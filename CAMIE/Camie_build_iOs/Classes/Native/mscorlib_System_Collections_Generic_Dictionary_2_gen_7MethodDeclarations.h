﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioSource>
struct Dictionary_2_t389;
// System.Collections.Generic.ICollection`1<System.String>
struct ICollection_1_t1032;
// System.Collections.Generic.ICollection`1<UnityEngine.AudioSource>
struct ICollection_1_t3609;
// System.Object
struct Object_t;
// UnityEngine.AudioSource
struct AudioSource_t247;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,UnityEngine.AudioSource>
struct KeyCollection_t3011;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,UnityEngine.AudioSource>
struct ValueCollection_t429;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_t2789;
// System.Collections.Generic.IDictionary`2<System.String,UnityEngine.AudioSource>
struct IDictionary_2_t3610;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1043;
// System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.AudioSource>[]
struct KeyValuePair_2U5BU5D_t3611;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t217;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.AudioSource>>
struct IEnumerator_1_t3612;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1429;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.AudioSource>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_15.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.AudioSource>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__13.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioSource>::.ctor()
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_22MethodDeclarations.h"
#define Dictionary_2__ctor_m1811(__this, method) (( void (*) (Dictionary_2_t389 *, const MethodInfo*))Dictionary_2__ctor_m13547_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioSource>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m16157(__this, ___comparer, method) (( void (*) (Dictionary_2_t389 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m13549_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioSource>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
#define Dictionary_2__ctor_m16158(__this, ___dictionary, method) (( void (*) (Dictionary_2_t389 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m13551_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioSource>::.ctor(System.Int32)
#define Dictionary_2__ctor_m16159(__this, ___capacity, method) (( void (*) (Dictionary_2_t389 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m13553_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioSource>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m16160(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t389 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m13555_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioSource>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2__ctor_m16161(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t389 *, SerializationInfo_t1043 *, StreamingContext_t1044 , const MethodInfo*))Dictionary_2__ctor_m13557_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioSource>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m16162(__this, method) (( Object_t* (*) (Dictionary_2_t389 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m13559_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioSource>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m16163(__this, method) (( Object_t* (*) (Dictionary_2_t389 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m13561_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioSource>::System.Collections.IDictionary.get_Item(System.Object)
#define Dictionary_2_System_Collections_IDictionary_get_Item_m16164(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t389 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m13563_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioSource>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_set_Item_m16165(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t389 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m13565_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioSource>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_Add_m16166(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t389 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m13567_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioSource>::System.Collections.IDictionary.Contains(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Contains_m16167(__this, ___key, method) (( bool (*) (Dictionary_2_t389 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m13569_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioSource>::System.Collections.IDictionary.Remove(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Remove_m16168(__this, ___key, method) (( void (*) (Dictionary_2_t389 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m13571_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioSource>::System.Collections.ICollection.get_IsSynchronized()
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m16169(__this, method) (( bool (*) (Dictionary_2_t389 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m13573_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioSource>::System.Collections.ICollection.get_SyncRoot()
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m16170(__this, method) (( Object_t * (*) (Dictionary_2_t389 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m13575_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioSource>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m16171(__this, method) (( bool (*) (Dictionary_2_t389 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m13577_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioSource>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m16172(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t389 *, KeyValuePair_2_t3010 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m13579_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioSource>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m16173(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t389 *, KeyValuePair_2_t3010 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m13581_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioSource>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m16174(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t389 *, KeyValuePair_2U5BU5D_t3611*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m13583_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioSource>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m16175(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t389 *, KeyValuePair_2_t3010 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m13585_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioSource>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Dictionary_2_System_Collections_ICollection_CopyTo_m16176(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t389 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m13587_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioSource>::System.Collections.IEnumerable.GetEnumerator()
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m16177(__this, method) (( Object_t * (*) (Dictionary_2_t389 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m13589_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioSource>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m16178(__this, method) (( Object_t* (*) (Dictionary_2_t389 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m13591_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioSource>::System.Collections.IDictionary.GetEnumerator()
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m16179(__this, method) (( Object_t * (*) (Dictionary_2_t389 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m13593_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioSource>::get_Count()
#define Dictionary_2_get_Count_m16180(__this, method) (( int32_t (*) (Dictionary_2_t389 *, const MethodInfo*))Dictionary_2_get_Count_m13595_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioSource>::get_Item(TKey)
#define Dictionary_2_get_Item_m16181(__this, ___key, method) (( AudioSource_t247 * (*) (Dictionary_2_t389 *, String_t*, const MethodInfo*))Dictionary_2_get_Item_m13597_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioSource>::set_Item(TKey,TValue)
#define Dictionary_2_set_Item_m16182(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t389 *, String_t*, AudioSource_t247 *, const MethodInfo*))Dictionary_2_set_Item_m13599_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioSource>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2_Init_m16183(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t389 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m13601_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioSource>::InitArrays(System.Int32)
#define Dictionary_2_InitArrays_m16184(__this, ___size, method) (( void (*) (Dictionary_2_t389 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m13603_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioSource>::CopyToCheck(System.Array,System.Int32)
#define Dictionary_2_CopyToCheck_m16185(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t389 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m13605_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioSource>::make_pair(TKey,TValue)
#define Dictionary_2_make_pair_m16186(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t3010  (*) (Object_t * /* static, unused */, String_t*, AudioSource_t247 *, const MethodInfo*))Dictionary_2_make_pair_m13607_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioSource>::pick_key(TKey,TValue)
#define Dictionary_2_pick_key_m16187(__this /* static, unused */, ___key, ___value, method) (( String_t* (*) (Object_t * /* static, unused */, String_t*, AudioSource_t247 *, const MethodInfo*))Dictionary_2_pick_key_m13609_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioSource>::pick_value(TKey,TValue)
#define Dictionary_2_pick_value_m16188(__this /* static, unused */, ___key, ___value, method) (( AudioSource_t247 * (*) (Object_t * /* static, unused */, String_t*, AudioSource_t247 *, const MethodInfo*))Dictionary_2_pick_value_m13611_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioSource>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_CopyTo_m16189(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t389 *, KeyValuePair_2U5BU5D_t3611*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m13613_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioSource>::Resize()
#define Dictionary_2_Resize_m16190(__this, method) (( void (*) (Dictionary_2_t389 *, const MethodInfo*))Dictionary_2_Resize_m13615_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioSource>::Add(TKey,TValue)
#define Dictionary_2_Add_m16191(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t389 *, String_t*, AudioSource_t247 *, const MethodInfo*))Dictionary_2_Add_m13617_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioSource>::Clear()
#define Dictionary_2_Clear_m16192(__this, method) (( void (*) (Dictionary_2_t389 *, const MethodInfo*))Dictionary_2_Clear_m13619_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioSource>::ContainsKey(TKey)
#define Dictionary_2_ContainsKey_m16193(__this, ___key, method) (( bool (*) (Dictionary_2_t389 *, String_t*, const MethodInfo*))Dictionary_2_ContainsKey_m13621_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioSource>::ContainsValue(TValue)
#define Dictionary_2_ContainsValue_m16194(__this, ___value, method) (( bool (*) (Dictionary_2_t389 *, AudioSource_t247 *, const MethodInfo*))Dictionary_2_ContainsValue_m13623_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioSource>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2_GetObjectData_m16195(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t389 *, SerializationInfo_t1043 *, StreamingContext_t1044 , const MethodInfo*))Dictionary_2_GetObjectData_m13625_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioSource>::OnDeserialization(System.Object)
#define Dictionary_2_OnDeserialization_m16196(__this, ___sender, method) (( void (*) (Dictionary_2_t389 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m13627_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioSource>::Remove(TKey)
#define Dictionary_2_Remove_m16197(__this, ___key, method) (( bool (*) (Dictionary_2_t389 *, String_t*, const MethodInfo*))Dictionary_2_Remove_m13629_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioSource>::TryGetValue(TKey,TValue&)
#define Dictionary_2_TryGetValue_m16198(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t389 *, String_t*, AudioSource_t247 **, const MethodInfo*))Dictionary_2_TryGetValue_m13631_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioSource>::get_Keys()
#define Dictionary_2_get_Keys_m16199(__this, method) (( KeyCollection_t3011 * (*) (Dictionary_2_t389 *, const MethodInfo*))Dictionary_2_get_Keys_m13633_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioSource>::get_Values()
#define Dictionary_2_get_Values_m1814(__this, method) (( ValueCollection_t429 * (*) (Dictionary_2_t389 *, const MethodInfo*))Dictionary_2_get_Values_m13635_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioSource>::ToTKey(System.Object)
#define Dictionary_2_ToTKey_m16200(__this, ___key, method) (( String_t* (*) (Dictionary_2_t389 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m13637_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioSource>::ToTValue(System.Object)
#define Dictionary_2_ToTValue_m16201(__this, ___value, method) (( AudioSource_t247 * (*) (Dictionary_2_t389 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m13639_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioSource>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_ContainsKeyValuePair_m16202(__this, ___pair, method) (( bool (*) (Dictionary_2_t389 *, KeyValuePair_2_t3010 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m13641_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioSource>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m16203(__this, method) (( Enumerator_t3012  (*) (Dictionary_2_t389 *, const MethodInfo*))Dictionary_2_GetEnumerator_m13643_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioSource>::<CopyTo>m__0(TKey,TValue)
#define Dictionary_2_U3CCopyToU3Em__0_m16204(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t1430  (*) (Object_t * /* static, unused */, String_t*, AudioSource_t247 *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m13645_gshared)(__this /* static, unused */, ___key, ___value, method)
