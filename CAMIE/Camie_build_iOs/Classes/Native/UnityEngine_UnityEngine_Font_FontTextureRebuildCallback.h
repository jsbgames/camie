﻿#pragma once
#include <stdint.h>
// System.IAsyncResult
struct IAsyncResult_t546;
// System.AsyncCallback
struct AsyncCallback_t547;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.Font/FontTextureRebuildCallback
struct  FontTextureRebuildCallback_t897  : public MulticastDelegate_t549
{
};
