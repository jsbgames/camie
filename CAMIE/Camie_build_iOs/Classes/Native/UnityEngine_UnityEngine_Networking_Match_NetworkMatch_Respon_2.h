﻿#pragma once
#include <stdint.h>
// UnityEngine.Networking.Match.ListMatchResponse
struct ListMatchResponse_t917;
// System.IAsyncResult
struct IAsyncResult_t546;
// System.AsyncCallback
struct AsyncCallback_t547;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.Networking.Match.NetworkMatch/ResponseDelegate`1<UnityEngine.Networking.Match.ListMatchResponse>
struct  ResponseDelegate_1_t1031  : public MulticastDelegate_t549
{
};
