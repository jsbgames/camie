﻿#pragma once
#include <stdint.h>
// System.Byte[]
struct ByteU5BU5D_t850;
// System.Security.Cryptography.HashAlgorithm
#include "mscorlib_System_Security_Cryptography_HashAlgorithm.h"
// System.Security.Cryptography.KeyedHashAlgorithm
struct  KeyedHashAlgorithm_t1563  : public HashAlgorithm_t1539
{
	// System.Byte[] System.Security.Cryptography.KeyedHashAlgorithm::KeyValue
	ByteU5BU5D_t850* ___KeyValue_4;
};
