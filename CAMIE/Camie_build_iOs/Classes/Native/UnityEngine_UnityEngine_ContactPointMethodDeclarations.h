﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.ContactPoint
struct ContactPoint_t246;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// UnityEngine.Vector3 UnityEngine.ContactPoint::get_point()
extern "C" Vector3_t4  ContactPoint_get_point_m916 (ContactPoint_t246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.ContactPoint::get_normal()
extern "C" Vector3_t4  ContactPoint_get_normal_m914 (ContactPoint_t246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
