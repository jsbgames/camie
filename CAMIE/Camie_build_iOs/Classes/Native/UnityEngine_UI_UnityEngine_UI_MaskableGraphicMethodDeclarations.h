﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.MaskableGraphic
struct MaskableGraphic_t537;
// UnityEngine.Material
struct Material_t55;

// System.Void UnityEngine.UI.MaskableGraphic::.ctor()
extern "C" void MaskableGraphic__ctor_m2498 (MaskableGraphic_t537 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.MaskableGraphic::get_maskable()
extern "C" bool MaskableGraphic_get_maskable_m2499 (MaskableGraphic_t537 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.MaskableGraphic::set_maskable(System.Boolean)
extern "C" void MaskableGraphic_set_maskable_m2500 (MaskableGraphic_t537 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material UnityEngine.UI.MaskableGraphic::get_material()
extern "C" Material_t55 * MaskableGraphic_get_material_m2501 (MaskableGraphic_t537 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.MaskableGraphic::set_material(UnityEngine.Material)
extern "C" void MaskableGraphic_set_material_m2502 (MaskableGraphic_t537 * __this, Material_t55 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.MaskableGraphic::UpdateInternalState()
extern "C" void MaskableGraphic_UpdateInternalState_m2503 (MaskableGraphic_t537 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.MaskableGraphic::OnEnable()
extern "C" void MaskableGraphic_OnEnable_m2504 (MaskableGraphic_t537 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.MaskableGraphic::OnDisable()
extern "C" void MaskableGraphic_OnDisable_m2505 (MaskableGraphic_t537 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.MaskableGraphic::OnTransformParentChanged()
extern "C" void MaskableGraphic_OnTransformParentChanged_m2506 (MaskableGraphic_t537 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.MaskableGraphic::ParentMaskStateChanged()
extern "C" void MaskableGraphic_ParentMaskStateChanged_m2507 (MaskableGraphic_t537 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.MaskableGraphic::ClearMaskMaterial()
extern "C" void MaskableGraphic_ClearMaskMaterial_m2508 (MaskableGraphic_t537 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.MaskableGraphic::SetMaterialDirty()
extern "C" void MaskableGraphic_SetMaterialDirty_m2509 (MaskableGraphic_t537 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.UI.MaskableGraphic::GetStencilForGraphic()
extern "C" int32_t MaskableGraphic_GetStencilForGraphic_m2510 (MaskableGraphic_t537 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
