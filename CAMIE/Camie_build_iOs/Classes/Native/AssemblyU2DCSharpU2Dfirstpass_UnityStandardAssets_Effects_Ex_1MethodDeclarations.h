﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets.Effects.ExplosionPhysicsForce/<Start>c__Iterator1
struct U3CStartU3Ec__Iterator1_t144;
// System.Object
struct Object_t;

// System.Void UnityStandardAssets.Effects.ExplosionPhysicsForce/<Start>c__Iterator1::.ctor()
extern "C" void U3CStartU3Ec__Iterator1__ctor_m407 (U3CStartU3Ec__Iterator1_t144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityStandardAssets.Effects.ExplosionPhysicsForce/<Start>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CStartU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m408 (U3CStartU3Ec__Iterator1_t144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityStandardAssets.Effects.ExplosionPhysicsForce/<Start>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CStartU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m409 (U3CStartU3Ec__Iterator1_t144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityStandardAssets.Effects.ExplosionPhysicsForce/<Start>c__Iterator1::MoveNext()
extern "C" bool U3CStartU3Ec__Iterator1_MoveNext_m410 (U3CStartU3Ec__Iterator1_t144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Effects.ExplosionPhysicsForce/<Start>c__Iterator1::Dispose()
extern "C" void U3CStartU3Ec__Iterator1_Dispose_m411 (U3CStartU3Ec__Iterator1_t144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Effects.ExplosionPhysicsForce/<Start>c__Iterator1::Reset()
extern "C" void U3CStartU3Ec__Iterator1_Reset_m412 (U3CStartU3Ec__Iterator1_t144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
