﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.RenderSettings
struct RenderSettings_t799;
// UnityEngine.FogMode
#include "UnityEngine_UnityEngine_FogMode.h"

// UnityEngine.FogMode UnityEngine.RenderSettings::get_fogMode()
extern "C" int32_t RenderSettings_get_fogMode_m843 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.RenderSettings::get_fogDensity()
extern "C" float RenderSettings_get_fogDensity_m844 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.RenderSettings::get_fogStartDistance()
extern "C" float RenderSettings_get_fogStartDistance_m845 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.RenderSettings::get_fogEndDistance()
extern "C" float RenderSettings_get_fogEndDistance_m846 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
