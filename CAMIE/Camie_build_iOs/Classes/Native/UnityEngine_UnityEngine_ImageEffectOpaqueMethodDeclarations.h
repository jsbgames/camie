﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.ImageEffectOpaque
struct ImageEffectOpaque_t265;

// System.Void UnityEngine.ImageEffectOpaque::.ctor()
extern "C" void ImageEffectOpaque__ctor_m1041 (ImageEffectOpaque_t265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
