﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>
struct KeyValuePair_2_t1088;
// System.String
struct String_t;
// SimpleJson.Reflection.ReflectionUtils/GetDelegate
struct GetDelegate_t937;

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_5MethodDeclarations.h"
#define KeyValuePair_2__ctor_m20982(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t1088 *, String_t*, GetDelegate_t937 *, const MethodInfo*))KeyValuePair_2__ctor_m13651_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::get_Key()
#define KeyValuePair_2_get_Key_m5210(__this, method) (( String_t* (*) (KeyValuePair_2_t1088 *, const MethodInfo*))KeyValuePair_2_get_Key_m13652_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m20983(__this, ___value, method) (( void (*) (KeyValuePair_2_t1088 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m13653_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::get_Value()
#define KeyValuePair_2_get_Value_m5209(__this, method) (( GetDelegate_t937 * (*) (KeyValuePair_2_t1088 *, const MethodInfo*))KeyValuePair_2_get_Value_m13654_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m20984(__this, ___value, method) (( void (*) (KeyValuePair_2_t1088 *, GetDelegate_t937 *, const MethodInfo*))KeyValuePair_2_set_Value_m13655_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::ToString()
#define KeyValuePair_2_ToString_m20985(__this, method) (( String_t* (*) (KeyValuePair_2_t1088 *, const MethodInfo*))KeyValuePair_2_ToString_m13656_gshared)(__this, method)
