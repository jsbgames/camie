﻿#pragma once
#include <stdint.h>
// System.Text.Encoding
#include "mscorlib_System_Text_Encoding.h"
// System.Text.UTF32Encoding
struct  UTF32Encoding_t2150  : public Encoding_t1022
{
	// System.Boolean System.Text.UTF32Encoding::bigEndian
	bool ___bigEndian_28;
	// System.Boolean System.Text.UTF32Encoding::byteOrderMark
	bool ___byteOrderMark_29;
};
