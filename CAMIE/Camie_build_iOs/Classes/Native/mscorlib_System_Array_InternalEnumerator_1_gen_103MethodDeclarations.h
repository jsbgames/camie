﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Reflection.Emit.MonoResource>
struct InternalEnumerator_1_t3468;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Reflection.Emit.MonoResource
#include "mscorlib_System_Reflection_Emit_MonoResource.h"

// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.MonoResource>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m22262_gshared (InternalEnumerator_1_t3468 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m22262(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3468 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m22262_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.MonoResource>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22263_gshared (InternalEnumerator_1_t3468 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22263(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3468 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22263_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.MonoResource>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m22264_gshared (InternalEnumerator_1_t3468 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m22264(__this, method) (( void (*) (InternalEnumerator_1_t3468 *, const MethodInfo*))InternalEnumerator_1_Dispose_m22264_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.MonoResource>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m22265_gshared (InternalEnumerator_1_t3468 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m22265(__this, method) (( bool (*) (InternalEnumerator_1_t3468 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m22265_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.MonoResource>::get_Current()
extern "C" MonoResource_t1838  InternalEnumerator_1_get_Current_m22266_gshared (InternalEnumerator_1_t3468 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m22266(__this, method) (( MonoResource_t1838  (*) (InternalEnumerator_1_t3468 *, const MethodInfo*))InternalEnumerator_1_get_Current_m22266_gshared)(__this, method)
