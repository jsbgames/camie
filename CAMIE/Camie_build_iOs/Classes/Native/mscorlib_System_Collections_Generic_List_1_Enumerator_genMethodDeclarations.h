﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.Rigidbody>
struct Enumerator_t145;
// System.Object
struct Object_t;
// UnityEngine.Rigidbody
struct Rigidbody_t14;
// System.Collections.Generic.List`1<UnityEngine.Rigidbody>
struct List_1_t142;

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Rigidbody>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_4MethodDeclarations.h"
#define Enumerator__ctor_m14245(__this, ___l, method) (( void (*) (Enumerator_t145 *, List_1_t142 *, const MethodInfo*))Enumerator__ctor_m13983_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Rigidbody>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m14246(__this, method) (( Object_t * (*) (Enumerator_t145 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m13984_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Rigidbody>::Dispose()
#define Enumerator_Dispose_m14247(__this, method) (( void (*) (Enumerator_t145 *, const MethodInfo*))Enumerator_Dispose_m13985_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Rigidbody>::VerifyState()
#define Enumerator_VerifyState_m14248(__this, method) (( void (*) (Enumerator_t145 *, const MethodInfo*))Enumerator_VerifyState_m13986_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Rigidbody>::MoveNext()
#define Enumerator_MoveNext_m910(__this, method) (( bool (*) (Enumerator_t145 *, const MethodInfo*))Enumerator_MoveNext_m13987_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Rigidbody>::get_Current()
#define Enumerator_get_Current_m908(__this, method) (( Rigidbody_t14 * (*) (Enumerator_t145 *, const MethodInfo*))Enumerator_get_Current_m13988_gshared)(__this, method)
