﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets.Effects.FireLight
struct FireLight_t153;

// System.Void UnityStandardAssets.Effects.FireLight::.ctor()
extern "C" void FireLight__ctor_m428 (FireLight_t153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Effects.FireLight::Start()
extern "C" void FireLight_Start_m429 (FireLight_t153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Effects.FireLight::Update()
extern "C" void FireLight_Update_m430 (FireLight_t153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Effects.FireLight::Extinguish()
extern "C" void FireLight_Extinguish_m431 (FireLight_t153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
