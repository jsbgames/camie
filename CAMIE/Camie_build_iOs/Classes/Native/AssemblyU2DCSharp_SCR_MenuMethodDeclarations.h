﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SCR_Menu
struct SCR_Menu_t336;
// UnityEngine.Transform[]
struct TransformU5BU5D_t141;
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t78;
// System.Collections.IEnumerator
struct IEnumerator_t217;
// UnityEngine.UI.Text
struct Text_t316;
// UnityEngine.UI.Image
struct Image_t48;

// System.Void SCR_Menu::.ctor()
extern "C" void SCR_Menu__ctor_m1204 (SCR_Menu_t336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Menu::Awake()
extern "C" void SCR_Menu_Awake_m1205 (SCR_Menu_t336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Menu::OnEnable()
extern "C" void SCR_Menu_OnEnable_m1206 (SCR_Menu_t336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Menu::FillDictionary(UnityEngine.Transform[])
extern "C" void SCR_Menu_FillDictionary_m1207 (SCR_Menu_t336 * __this, TransformU5BU5D_t141* ___children, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Menu::ShowMenuItem(System.String,System.Boolean)
extern "C" void SCR_Menu_ShowMenuItem_m1208 (SCR_Menu_t336 * __this, String_t* ___itemName, bool ___show, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Menu::ResetMenu()
extern "C" void SCR_Menu_ResetMenu_m1209 (SCR_Menu_t336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject SCR_Menu::GetMenuItem(System.String)
extern "C" GameObject_t78 * SCR_Menu_GetMenuItem_m1210 (SCR_Menu_t336 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Menu::ModifyText(System.String,System.String)
extern "C" void SCR_Menu_ModifyText_m1211 (SCR_Menu_t336 * __this, String_t* ___textGuiName, String_t* ___textToWrite, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator SCR_Menu::AlphaLerp(UnityEngine.UI.Text,System.Single,System.Boolean)
extern "C" Object_t * SCR_Menu_AlphaLerp_m1212 (SCR_Menu_t336 * __this, Text_t316 * ___text, float ___speed, bool ___isRepeating, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator SCR_Menu::AlphaLerp(UnityEngine.UI.Image,System.Single,System.Boolean)
extern "C" Object_t * SCR_Menu_AlphaLerp_m1213 (SCR_Menu_t336 * __this, Image_t48 * ___image, float ___speed, bool ___isRepeating, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Menu::Options()
extern "C" void SCR_Menu_Options_m1214 (SCR_Menu_t336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Menu::SwitchLanguage(System.Boolean)
extern "C" void SCR_Menu_SwitchLanguage_m1215 (SCR_Menu_t336 * __this, bool ___isFrench, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Menu::ToggleSound(System.Boolean)
extern "C" void SCR_Menu_ToggleSound_m1216 (SCR_Menu_t336 * __this, bool ___soundOn, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Menu::SetMenuLanguage()
extern "C" void SCR_Menu_SetMenuLanguage_m1217 (SCR_Menu_t336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Menu::ReturnToMainMenu()
extern "C" void SCR_Menu_ReturnToMainMenu_m1218 (SCR_Menu_t336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Menu::LoadLevel(System.Int32)
extern "C" void SCR_Menu_LoadLevel_m1219 (SCR_Menu_t336 * __this, int32_t ___level, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Menu::ButtonSound()
extern "C" void SCR_Menu_ButtonSound_m1220 (SCR_Menu_t336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
