﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.Contexts.CrossContextChannel
struct CrossContextChannel_t1958;

// System.Void System.Runtime.Remoting.Contexts.CrossContextChannel::.ctor()
extern "C" void CrossContextChannel__ctor_m10492 (CrossContextChannel_t1958 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
