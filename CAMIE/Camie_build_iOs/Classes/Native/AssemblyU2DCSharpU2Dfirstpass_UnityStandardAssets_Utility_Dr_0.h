﻿#pragma once
#include <stdint.h>
// UnityEngine.SpringJoint
struct SpringJoint_t176;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityStandardAssets.Utility.DragRigidbody
struct  DragRigidbody_t174  : public MonoBehaviour_t3
{
	// UnityEngine.SpringJoint UnityStandardAssets.Utility.DragRigidbody::m_SpringJoint
	SpringJoint_t176 * ___m_SpringJoint_8;
};
