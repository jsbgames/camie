﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
struct GcAchievementData_t958;
struct GcAchievementData_t958_marshaled;
// UnityEngine.SocialPlatforms.Impl.Achievement
struct Achievement_t977;

// UnityEngine.SocialPlatforms.Impl.Achievement UnityEngine.SocialPlatforms.GameCenter.GcAchievementData::ToAchievement()
extern "C" Achievement_t977 * GcAchievementData_ToAchievement_m4887 (GcAchievementData_t958 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
void GcAchievementData_t958_marshal(const GcAchievementData_t958& unmarshaled, GcAchievementData_t958_marshaled& marshaled);
void GcAchievementData_t958_marshal_back(const GcAchievementData_t958_marshaled& marshaled, GcAchievementData_t958& unmarshaled);
void GcAchievementData_t958_marshal_cleanup(GcAchievementData_t958_marshaled& marshaled);
