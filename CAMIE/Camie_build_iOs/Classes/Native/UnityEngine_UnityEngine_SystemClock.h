﻿#pragma once
#include <stdint.h>
// System.Object
#include "mscorlib_System_Object.h"
// System.DateTime
#include "mscorlib_System_DateTime.h"
// UnityEngine.SystemClock
struct  SystemClock_t996  : public Object_t
{
};
struct SystemClock_t996_StaticFields{
	// System.DateTime UnityEngine.SystemClock::s_Epoch
	DateTime_t406  ___s_Epoch_0;
};
