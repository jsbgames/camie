﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.Emit.LocalBuilder
struct LocalBuilder_t1865;
// System.Type
struct Type_t;
// System.Reflection.Emit.ILGenerator
struct ILGenerator_t1843;

// System.Void System.Reflection.Emit.LocalBuilder::.ctor(System.Type,System.Reflection.Emit.ILGenerator)
extern "C" void LocalBuilder__ctor_m9915 (LocalBuilder_t1865 * __this, Type_t * ___t, ILGenerator_t1843 * ___ilgen, const MethodInfo* method) IL2CPP_METHOD_ATTR;
