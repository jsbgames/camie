﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// ReporterMessageReceiver
struct ReporterMessageReceiver_t306;
// Reporter/Log
struct Log_t291;

// System.Void ReporterMessageReceiver::.ctor()
extern "C" void ReporterMessageReceiver__ctor_m1127 (ReporterMessageReceiver_t306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReporterMessageReceiver::Start()
extern "C" void ReporterMessageReceiver_Start_m1128 (ReporterMessageReceiver_t306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReporterMessageReceiver::OnPreStart()
extern "C" void ReporterMessageReceiver_OnPreStart_m1129 (ReporterMessageReceiver_t306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReporterMessageReceiver::OnHideReporter()
extern "C" void ReporterMessageReceiver_OnHideReporter_m1130 (ReporterMessageReceiver_t306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReporterMessageReceiver::OnShowReporter()
extern "C" void ReporterMessageReceiver_OnShowReporter_m1131 (ReporterMessageReceiver_t306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReporterMessageReceiver::OnLog(Reporter/Log)
extern "C" void ReporterMessageReceiver_OnLog_m1132 (ReporterMessageReceiver_t306 * __this, Log_t291 * ___log, const MethodInfo* method) IL2CPP_METHOD_ATTR;
