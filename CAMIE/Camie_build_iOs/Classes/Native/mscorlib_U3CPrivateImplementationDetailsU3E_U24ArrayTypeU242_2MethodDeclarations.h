﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$256
struct U24ArrayTypeU24256_t2269;
struct U24ArrayTypeU24256_t2269_marshaled;

void U24ArrayTypeU24256_t2269_marshal(const U24ArrayTypeU24256_t2269& unmarshaled, U24ArrayTypeU24256_t2269_marshaled& marshaled);
void U24ArrayTypeU24256_t2269_marshal_back(const U24ArrayTypeU24256_t2269_marshaled& marshaled, U24ArrayTypeU24256_t2269& unmarshaled);
void U24ArrayTypeU24256_t2269_marshal_cleanup(U24ArrayTypeU24256_t2269_marshaled& marshaled);
