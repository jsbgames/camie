﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Serialization.Formatters.Binary.ObjectWriter/MetadataReference
struct MetadataReference_t2039;
// System.Runtime.Serialization.Formatters.Binary.TypeMetadata
struct TypeMetadata_t2035;

// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectWriter/MetadataReference::.ctor(System.Runtime.Serialization.Formatters.Binary.TypeMetadata,System.Int64)
extern "C" void MetadataReference__ctor_m10862 (MetadataReference_t2039 * __this, TypeMetadata_t2035 * ___metadata, int64_t ___id, const MethodInfo* method) IL2CPP_METHOD_ATTR;
