﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>
struct KeyValuePair_2_t1085;
// System.Type
struct Type_t;
// SimpleJson.Reflection.ReflectionUtils/SetDelegate
struct SetDelegate_t938;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_5MethodDeclarations.h"
#define KeyValuePair_2__ctor_m5203(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t1085 *, Type_t *, SetDelegate_t938 *, const MethodInfo*))KeyValuePair_2__ctor_m13651_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>::get_Key()
#define KeyValuePair_2_get_Key_m20659(__this, method) (( Type_t * (*) (KeyValuePair_2_t1085 *, const MethodInfo*))KeyValuePair_2_get_Key_m13652_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m20660(__this, ___value, method) (( void (*) (KeyValuePair_2_t1085 *, Type_t *, const MethodInfo*))KeyValuePair_2_set_Key_m13653_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>::get_Value()
#define KeyValuePair_2_get_Value_m20661(__this, method) (( SetDelegate_t938 * (*) (KeyValuePair_2_t1085 *, const MethodInfo*))KeyValuePair_2_get_Value_m13654_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m20662(__this, ___value, method) (( void (*) (KeyValuePair_2_t1085 *, SetDelegate_t938 *, const MethodInfo*))KeyValuePair_2_set_Value_m13655_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>::ToString()
#define KeyValuePair_2_ToString_m20663(__this, method) (( String_t* (*) (KeyValuePair_2_t1085 *, const MethodInfo*))KeyValuePair_2_ToString_m13656_gshared)(__this, method)
