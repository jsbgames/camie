﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Events.InvokableCall`1<System.Object>
struct InvokableCall_1_t3049;
// System.Object
struct Object_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.Events.UnityAction`1<System.Object>
struct UnityAction_1_t3027;
// System.Object[]
struct ObjectU5BU5D_t224;

// System.Void UnityEngine.Events.InvokableCall`1<System.Object>::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C" void InvokableCall_1__ctor_m16794_gshared (InvokableCall_1_t3049 * __this, Object_t * ___target, MethodInfo_t * ___theFunction, const MethodInfo* method);
#define InvokableCall_1__ctor_m16794(__this, ___target, ___theFunction, method) (( void (*) (InvokableCall_1_t3049 *, Object_t *, MethodInfo_t *, const MethodInfo*))InvokableCall_1__ctor_m16794_gshared)(__this, ___target, ___theFunction, method)
// System.Void UnityEngine.Events.InvokableCall`1<System.Object>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
extern "C" void InvokableCall_1__ctor_m16795_gshared (InvokableCall_1_t3049 * __this, UnityAction_1_t3027 * ___callback, const MethodInfo* method);
#define InvokableCall_1__ctor_m16795(__this, ___callback, method) (( void (*) (InvokableCall_1_t3049 *, UnityAction_1_t3027 *, const MethodInfo*))InvokableCall_1__ctor_m16795_gshared)(__this, ___callback, method)
// System.Void UnityEngine.Events.InvokableCall`1<System.Object>::Invoke(System.Object[])
extern "C" void InvokableCall_1_Invoke_m16796_gshared (InvokableCall_1_t3049 * __this, ObjectU5BU5D_t224* ___args, const MethodInfo* method);
#define InvokableCall_1_Invoke_m16796(__this, ___args, method) (( void (*) (InvokableCall_1_t3049 *, ObjectU5BU5D_t224*, const MethodInfo*))InvokableCall_1_Invoke_m16796_gshared)(__this, ___args, method)
// System.Boolean UnityEngine.Events.InvokableCall`1<System.Object>::Find(System.Object,System.Reflection.MethodInfo)
extern "C" bool InvokableCall_1_Find_m16797_gshared (InvokableCall_1_t3049 * __this, Object_t * ___targetObj, MethodInfo_t * ___method, const MethodInfo* method);
#define InvokableCall_1_Find_m16797(__this, ___targetObj, ___method, method) (( bool (*) (InvokableCall_1_t3049 *, Object_t *, MethodInfo_t *, const MethodInfo*))InvokableCall_1_Find_m16797_gshared)(__this, ___targetObj, ___method, method)
