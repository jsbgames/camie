﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "stringLiterals.h"
// <Module>
#include "replacements_U3CModuleU3E.h"
// Metadata Definition <Module>
extern TypeInfo U3CModuleU3E_t1511_il2cpp_TypeInfo;
// <Module>
#include "replacements_U3CModuleU3EMethodDeclarations.h"
static const MethodInfo* U3CModuleU3E_t1511_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_replacements_dll_Image;
extern const Il2CppType U3CModuleU3E_t1511_0_0_0;
extern const Il2CppType U3CModuleU3E_t1511_1_0_0;
struct U3CModuleU3E_t1511;
const Il2CppTypeDefinitionMetadata U3CModuleU3E_t1511_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U3CModuleU3E_t1511_il2cpp_TypeInfo = 
{
	&g_replacements_dll_Image/* image */
	, NULL/* gc_desc */
	, "<Module>"/* name */
	, ""/* namespaze */
	, U3CModuleU3E_t1511_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U3CModuleU3E_t1511_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U3CModuleU3E_t1511_0_0_0/* byval_arg */
	, &U3CModuleU3E_t1511_1_0_0/* this_arg */
	, &U3CModuleU3E_t1511_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CModuleU3E_t1511)/* instance_size */
	, sizeof (U3CModuleU3E_t1511)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Replacements.MSCompatUnicodeTable
#include "replacements_Replacements_MSCompatUnicodeTable.h"
// Metadata Definition Replacements.MSCompatUnicodeTable
extern TypeInfo MSCompatUnicodeTable_t1512_il2cpp_TypeInfo;
// Replacements.MSCompatUnicodeTable
#include "replacements_Replacements_MSCompatUnicodeTableMethodDeclarations.h"
extern const Il2CppType Boolean_t273_0_0_0;
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean Replacements.MSCompatUnicodeTable::get_IsReady()
extern const MethodInfo MSCompatUnicodeTable_get_IsReady_m6672_MethodInfo = 
{
	"get_IsReady"/* name */
	, (methodPointerType)&MSCompatUnicodeTable_get_IsReady_m6672/* method */
	, &MSCompatUnicodeTable_t1512_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MSCompatUnicodeTable_t1512_MethodInfos[] =
{
	&MSCompatUnicodeTable_get_IsReady_m6672_MethodInfo,
	NULL
};
extern const MethodInfo MSCompatUnicodeTable_get_IsReady_m6672_MethodInfo;
static const PropertyInfo MSCompatUnicodeTable_t1512____IsReady_PropertyInfo = 
{
	&MSCompatUnicodeTable_t1512_il2cpp_TypeInfo/* parent */
	, "IsReady"/* name */
	, &MSCompatUnicodeTable_get_IsReady_m6672_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* MSCompatUnicodeTable_t1512_PropertyInfos[] =
{
	&MSCompatUnicodeTable_t1512____IsReady_PropertyInfo,
	NULL
};
extern const MethodInfo Object_Equals_m1073_MethodInfo;
extern const MethodInfo Object_Finalize_m1048_MethodInfo;
extern const MethodInfo Object_GetHashCode_m1074_MethodInfo;
extern const MethodInfo Object_ToString_m1075_MethodInfo;
static const Il2CppMethodReference MSCompatUnicodeTable_t1512_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool MSCompatUnicodeTable_t1512_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_replacements_dll_Image;
extern const Il2CppType MSCompatUnicodeTable_t1512_0_0_0;
extern const Il2CppType MSCompatUnicodeTable_t1512_1_0_0;
extern const Il2CppType Object_t_0_0_0;
struct MSCompatUnicodeTable_t1512;
const Il2CppTypeDefinitionMetadata MSCompatUnicodeTable_t1512_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MSCompatUnicodeTable_t1512_VTable/* vtableMethods */
	, MSCompatUnicodeTable_t1512_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo MSCompatUnicodeTable_t1512_il2cpp_TypeInfo = 
{
	&g_replacements_dll_Image/* image */
	, NULL/* gc_desc */
	, "MSCompatUnicodeTable"/* name */
	, "Replacements"/* namespaze */
	, MSCompatUnicodeTable_t1512_MethodInfos/* methods */
	, MSCompatUnicodeTable_t1512_PropertyInfos/* properties */
	, NULL/* events */
	, &MSCompatUnicodeTable_t1512_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MSCompatUnicodeTable_t1512_0_0_0/* byval_arg */
	, &MSCompatUnicodeTable_t1512_1_0_0/* this_arg */
	, &MSCompatUnicodeTable_t1512_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MSCompatUnicodeTable_t1512)/* instance_size */
	, sizeof (MSCompatUnicodeTable_t1512)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Replacements.TypeBuilder
#include "replacements_Replacements_TypeBuilder.h"
// Metadata Definition Replacements.TypeBuilder
extern TypeInfo TypeBuilder_t1513_il2cpp_TypeInfo;
// Replacements.TypeBuilder
#include "replacements_Replacements_TypeBuilderMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo TypeBuilder_t1513_TypeBuilder_CreateType_m6673_ParameterInfos[] = 
{
	{"__this", 0, 134217729, 0, &Object_t_0_0_0},
};
extern const Il2CppType Type_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type Replacements.TypeBuilder::CreateType(System.Object)
extern const MethodInfo TypeBuilder_CreateType_m6673_MethodInfo = 
{
	"CreateType"/* name */
	, (methodPointerType)&TypeBuilder_CreateType_m6673/* method */
	, &TypeBuilder_t1513_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, TypeBuilder_t1513_TypeBuilder_CreateType_m6673_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TypeBuilder_t1513_MethodInfos[] =
{
	&TypeBuilder_CreateType_m6673_MethodInfo,
	NULL
};
static const Il2CppMethodReference TypeBuilder_t1513_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool TypeBuilder_t1513_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_replacements_dll_Image;
extern const Il2CppType TypeBuilder_t1513_0_0_0;
extern const Il2CppType TypeBuilder_t1513_1_0_0;
struct TypeBuilder_t1513;
const Il2CppTypeDefinitionMetadata TypeBuilder_t1513_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TypeBuilder_t1513_VTable/* vtableMethods */
	, TypeBuilder_t1513_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo TypeBuilder_t1513_il2cpp_TypeInfo = 
{
	&g_replacements_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeBuilder"/* name */
	, "Replacements"/* namespaze */
	, TypeBuilder_t1513_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TypeBuilder_t1513_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TypeBuilder_t1513_0_0_0/* byval_arg */
	, &TypeBuilder_t1513_1_0_0/* this_arg */
	, &TypeBuilder_t1513_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeBuilder_t1513)/* instance_size */
	, sizeof (TypeBuilder_t1513)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Replacements.SecurityElement
#include "replacements_Replacements_SecurityElement.h"
// Metadata Definition Replacements.SecurityElement
extern TypeInfo SecurityElement_t1514_il2cpp_TypeInfo;
// Replacements.SecurityElement
#include "replacements_Replacements_SecurityElementMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo SecurityElement_t1514_SecurityElement_ToString_m6674_ParameterInfos[] = 
{
	{"__this", 0, 134217730, 0, &Object_t_0_0_0},
};
extern const Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String Replacements.SecurityElement::ToString(System.Object)
extern const MethodInfo SecurityElement_ToString_m6674_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&SecurityElement_ToString_m6674/* method */
	, &SecurityElement_t1514_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, SecurityElement_t1514_SecurityElement_ToString_m6674_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SecurityElement_t1514_MethodInfos[] =
{
	&SecurityElement_ToString_m6674_MethodInfo,
	NULL
};
static const Il2CppMethodReference SecurityElement_t1514_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool SecurityElement_t1514_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_replacements_dll_Image;
extern const Il2CppType SecurityElement_t1514_0_0_0;
extern const Il2CppType SecurityElement_t1514_1_0_0;
struct SecurityElement_t1514;
const Il2CppTypeDefinitionMetadata SecurityElement_t1514_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SecurityElement_t1514_VTable/* vtableMethods */
	, SecurityElement_t1514_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo SecurityElement_t1514_il2cpp_TypeInfo = 
{
	&g_replacements_dll_Image/* image */
	, NULL/* gc_desc */
	, "SecurityElement"/* name */
	, "Replacements"/* namespaze */
	, SecurityElement_t1514_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SecurityElement_t1514_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SecurityElement_t1514_0_0_0/* byval_arg */
	, &SecurityElement_t1514_1_0_0/* this_arg */
	, &SecurityElement_t1514_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SecurityElement_t1514)/* instance_size */
	, sizeof (SecurityElement_t1514)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Replacements.RemotingServices
#include "replacements_Replacements_RemotingServices.h"
// Metadata Definition Replacements.RemotingServices
extern TypeInfo RemotingServices_t1515_il2cpp_TypeInfo;
// Replacements.RemotingServices
#include "replacements_Replacements_RemotingServicesMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType String_t_1_0_2;
extern const Il2CppType String_t_1_0_0;
static const ParameterInfo RemotingServices_t1515_RemotingServices_GetClientChannelSinkChain_m6675_ParameterInfos[] = 
{
	{"url", 0, 134217731, 0, &String_t_0_0_0},
	{"channelData", 1, 134217732, 0, &Object_t_0_0_0},
	{"objectUri", 2, 134217733, 0, &String_t_1_0_2},
};
extern const Il2CppType IMessageSink_t1516_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_StringU26_t1216 (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Messaging.IMessageSink Replacements.RemotingServices::GetClientChannelSinkChain(System.String,System.Object,System.String&)
extern const MethodInfo RemotingServices_GetClientChannelSinkChain_m6675_MethodInfo = 
{
	"GetClientChannelSinkChain"/* name */
	, (methodPointerType)&RemotingServices_GetClientChannelSinkChain_m6675/* method */
	, &RemotingServices_t1515_il2cpp_TypeInfo/* declaring_type */
	, &IMessageSink_t1516_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_StringU26_t1216/* invoker_method */
	, RemotingServices_t1515_RemotingServices_GetClientChannelSinkChain_m6675_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType ObjectU5BU5D_t224_0_0_0;
extern const Il2CppType ObjectU5BU5D_t224_0_0_0;
static const ParameterInfo RemotingServices_t1515_RemotingServices_CreateClientProxy_m6676_ParameterInfos[] = 
{
	{"objectType", 0, 134217734, 0, &Type_t_0_0_0},
	{"url", 1, 134217735, 0, &String_t_0_0_0},
	{"activationAttributes", 2, 134217736, 0, &ObjectU5BU5D_t224_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object Replacements.RemotingServices::CreateClientProxy(System.Type,System.String,System.Object[])
extern const MethodInfo RemotingServices_CreateClientProxy_m6676_MethodInfo = 
{
	"CreateClientProxy"/* name */
	, (methodPointerType)&RemotingServices_CreateClientProxy_m6676/* method */
	, &RemotingServices_t1515_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, RemotingServices_t1515_RemotingServices_CreateClientProxy_m6676_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* RemotingServices_t1515_MethodInfos[] =
{
	&RemotingServices_GetClientChannelSinkChain_m6675_MethodInfo,
	&RemotingServices_CreateClientProxy_m6676_MethodInfo,
	NULL
};
static const Il2CppMethodReference RemotingServices_t1515_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool RemotingServices_t1515_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_replacements_dll_Image;
extern const Il2CppType RemotingServices_t1515_0_0_0;
extern const Il2CppType RemotingServices_t1515_1_0_0;
struct RemotingServices_t1515;
const Il2CppTypeDefinitionMetadata RemotingServices_t1515_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, RemotingServices_t1515_VTable/* vtableMethods */
	, RemotingServices_t1515_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo RemotingServices_t1515_il2cpp_TypeInfo = 
{
	&g_replacements_dll_Image/* image */
	, NULL/* gc_desc */
	, "RemotingServices"/* name */
	, "Replacements"/* namespaze */
	, RemotingServices_t1515_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &RemotingServices_t1515_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RemotingServices_t1515_0_0_0/* byval_arg */
	, &RemotingServices_t1515_1_0_0/* this_arg */
	, &RemotingServices_t1515_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RemotingServices_t1515)/* instance_size */
	, sizeof (RemotingServices_t1515)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
