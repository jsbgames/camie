﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.Activation.RemoteActivator
struct RemoteActivator_t1954;
// System.Object
struct Object_t;

// System.Object System.Runtime.Remoting.Activation.RemoteActivator::InitializeLifetimeService()
extern "C" Object_t * RemoteActivator_InitializeLifetimeService_m10457 (RemoteActivator_t1954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
