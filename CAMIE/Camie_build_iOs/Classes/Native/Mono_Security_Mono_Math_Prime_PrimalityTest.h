﻿#pragma once
#include <stdint.h>
// Mono.Math.BigInteger
struct BigInteger_t1520;
// System.IAsyncResult
struct IAsyncResult_t546;
// System.AsyncCallback
struct AsyncCallback_t547;
// System.Object
struct Object_t;
// Mono.Math.Prime.ConfidenceFactor
#include "Mono_Security_Mono_Math_Prime_ConfidenceFactor.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// Mono.Math.Prime.PrimalityTest
struct  PrimalityTest_t1625  : public MulticastDelegate_t549
{
};
