﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "stringLiterals.h"
// UnityEngine.RenderMode
#include "UnityEngine_UnityEngine_RenderMode.h"
// Metadata Definition UnityEngine.RenderMode
extern TypeInfo RenderMode_t900_il2cpp_TypeInfo;
// UnityEngine.RenderMode
#include "UnityEngine_UnityEngine_RenderModeMethodDeclarations.h"
static const MethodInfo* RenderMode_t900_MethodInfos[] =
{
	NULL
};
extern const MethodInfo Enum_Equals_m1051_MethodInfo;
extern const MethodInfo Object_Finalize_m1048_MethodInfo;
extern const MethodInfo Enum_GetHashCode_m1052_MethodInfo;
extern const MethodInfo Enum_ToString_m1053_MethodInfo;
extern const MethodInfo Enum_ToString_m1054_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToBoolean_m1055_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToByte_m1056_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToChar_m1057_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDateTime_m1058_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDecimal_m1059_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDouble_m1060_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt16_m1061_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt32_m1062_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt64_m1063_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToSByte_m1064_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToSingle_m1065_MethodInfo;
extern const MethodInfo Enum_ToString_m1066_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToType_m1067_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt16_m1068_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt32_m1069_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt64_m1070_MethodInfo;
extern const MethodInfo Enum_CompareTo_m1071_MethodInfo;
extern const MethodInfo Enum_GetTypeCode_m1072_MethodInfo;
static const Il2CppMethodReference RenderMode_t900_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool RenderMode_t900_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IFormattable_t275_0_0_0;
extern const Il2CppType IConvertible_t276_0_0_0;
extern const Il2CppType IComparable_t277_0_0_0;
static Il2CppInterfaceOffsetPair RenderMode_t900_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType RenderMode_t900_0_0_0;
extern const Il2CppType RenderMode_t900_1_0_0;
extern const Il2CppType Enum_t278_0_0_0;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t253_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata RenderMode_t900_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RenderMode_t900_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, RenderMode_t900_VTable/* vtableMethods */
	, RenderMode_t900_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1011/* fieldStart */

};
TypeInfo RenderMode_t900_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "RenderMode"/* name */
	, "UnityEngine"/* namespaze */
	, RenderMode_t900_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RenderMode_t900_0_0_0/* byval_arg */
	, &RenderMode_t900_1_0_0/* this_arg */
	, &RenderMode_t900_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RenderMode_t900)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (RenderMode_t900)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.Canvas/WillRenderCanvases
#include "UnityEngine_UnityEngine_Canvas_WillRenderCanvases.h"
// Metadata Definition UnityEngine.Canvas/WillRenderCanvases
extern TypeInfo WillRenderCanvases_t666_il2cpp_TypeInfo;
// UnityEngine.Canvas/WillRenderCanvases
#include "UnityEngine_UnityEngine_Canvas_WillRenderCanvasesMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo WillRenderCanvases_t666_WillRenderCanvases__ctor_m3157_ParameterInfos[] = 
{
	{"object", 0, 134219794, 0, &Object_t_0_0_0},
	{"method", 1, 134219795, 0, &IntPtr_t_0_0_0},
};
extern const Il2CppType Void_t272_0_0_0;
extern void* RuntimeInvoker_Void_t272_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Canvas/WillRenderCanvases::.ctor(System.Object,System.IntPtr)
extern const MethodInfo WillRenderCanvases__ctor_m3157_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&WillRenderCanvases__ctor_m3157/* method */
	, &WillRenderCanvases_t666_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_IntPtr_t/* invoker_method */
	, WillRenderCanvases_t666_WillRenderCanvases__ctor_m3157_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1711/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Canvas/WillRenderCanvases::Invoke()
extern const MethodInfo WillRenderCanvases_Invoke_m4625_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&WillRenderCanvases_Invoke_m4625/* method */
	, &WillRenderCanvases_t666_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1712/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType AsyncCallback_t547_0_0_0;
extern const Il2CppType AsyncCallback_t547_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo WillRenderCanvases_t666_WillRenderCanvases_BeginInvoke_m4626_ParameterInfos[] = 
{
	{"callback", 0, 134219796, 0, &AsyncCallback_t547_0_0_0},
	{"object", 1, 134219797, 0, &Object_t_0_0_0},
};
extern const Il2CppType IAsyncResult_t546_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult UnityEngine.Canvas/WillRenderCanvases::BeginInvoke(System.AsyncCallback,System.Object)
extern const MethodInfo WillRenderCanvases_BeginInvoke_m4626_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&WillRenderCanvases_BeginInvoke_m4626/* method */
	, &WillRenderCanvases_t666_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t546_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, WillRenderCanvases_t666_WillRenderCanvases_BeginInvoke_m4626_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1713/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t546_0_0_0;
static const ParameterInfo WillRenderCanvases_t666_WillRenderCanvases_EndInvoke_m4627_ParameterInfos[] = 
{
	{"result", 0, 134219798, 0, &IAsyncResult_t546_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Canvas/WillRenderCanvases::EndInvoke(System.IAsyncResult)
extern const MethodInfo WillRenderCanvases_EndInvoke_m4627_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&WillRenderCanvases_EndInvoke_m4627/* method */
	, &WillRenderCanvases_t666_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, WillRenderCanvases_t666_WillRenderCanvases_EndInvoke_m4627_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1714/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* WillRenderCanvases_t666_MethodInfos[] =
{
	&WillRenderCanvases__ctor_m3157_MethodInfo,
	&WillRenderCanvases_Invoke_m4625_MethodInfo,
	&WillRenderCanvases_BeginInvoke_m4626_MethodInfo,
	&WillRenderCanvases_EndInvoke_m4627_MethodInfo,
	NULL
};
extern const MethodInfo MulticastDelegate_Equals_m3586_MethodInfo;
extern const MethodInfo MulticastDelegate_GetHashCode_m3587_MethodInfo;
extern const MethodInfo Object_ToString_m1075_MethodInfo;
extern const MethodInfo MulticastDelegate_GetObjectData_m3588_MethodInfo;
extern const MethodInfo Delegate_Clone_m3589_MethodInfo;
extern const MethodInfo MulticastDelegate_GetInvocationList_m3590_MethodInfo;
extern const MethodInfo MulticastDelegate_CombineImpl_m3591_MethodInfo;
extern const MethodInfo MulticastDelegate_RemoveImpl_m3592_MethodInfo;
extern const MethodInfo WillRenderCanvases_Invoke_m4625_MethodInfo;
extern const MethodInfo WillRenderCanvases_BeginInvoke_m4626_MethodInfo;
extern const MethodInfo WillRenderCanvases_EndInvoke_m4627_MethodInfo;
static const Il2CppMethodReference WillRenderCanvases_t666_VTable[] =
{
	&MulticastDelegate_Equals_m3586_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&MulticastDelegate_GetHashCode_m3587_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&Delegate_Clone_m3589_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&MulticastDelegate_GetInvocationList_m3590_MethodInfo,
	&MulticastDelegate_CombineImpl_m3591_MethodInfo,
	&MulticastDelegate_RemoveImpl_m3592_MethodInfo,
	&WillRenderCanvases_Invoke_m4625_MethodInfo,
	&WillRenderCanvases_BeginInvoke_m4626_MethodInfo,
	&WillRenderCanvases_EndInvoke_m4627_MethodInfo,
};
static bool WillRenderCanvases_t666_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ICloneable_t733_0_0_0;
extern const Il2CppType ISerializable_t439_0_0_0;
static Il2CppInterfaceOffsetPair WillRenderCanvases_t666_InterfacesOffsets[] = 
{
	{ &ICloneable_t733_0_0_0, 4},
	{ &ISerializable_t439_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType WillRenderCanvases_t666_0_0_0;
extern const Il2CppType WillRenderCanvases_t666_1_0_0;
extern const Il2CppType MulticastDelegate_t549_0_0_0;
extern TypeInfo Canvas_t414_il2cpp_TypeInfo;
extern const Il2CppType Canvas_t414_0_0_0;
struct WillRenderCanvases_t666;
const Il2CppTypeDefinitionMetadata WillRenderCanvases_t666_DefinitionMetadata = 
{
	&Canvas_t414_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, WillRenderCanvases_t666_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t549_0_0_0/* parent */
	, WillRenderCanvases_t666_VTable/* vtableMethods */
	, WillRenderCanvases_t666_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo WillRenderCanvases_t666_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "WillRenderCanvases"/* name */
	, ""/* namespaze */
	, WillRenderCanvases_t666_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &WillRenderCanvases_t666_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &WillRenderCanvases_t666_0_0_0/* byval_arg */
	, &WillRenderCanvases_t666_1_0_0/* this_arg */
	, &WillRenderCanvases_t666_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_WillRenderCanvases_t666/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WillRenderCanvases_t666)/* instance_size */
	, sizeof (WillRenderCanvases_t666)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.Canvas
#include "UnityEngine_UnityEngine_Canvas.h"
// Metadata Definition UnityEngine.Canvas
// UnityEngine.Canvas
#include "UnityEngine_UnityEngine_CanvasMethodDeclarations.h"
extern const Il2CppType WillRenderCanvases_t666_0_0_0;
static const ParameterInfo Canvas_t414_Canvas_add_willRenderCanvases_m3158_ParameterInfos[] = 
{
	{"value", 0, 134219790, 0, &WillRenderCanvases_t666_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Canvas::add_willRenderCanvases(UnityEngine.Canvas/WillRenderCanvases)
extern const MethodInfo Canvas_add_willRenderCanvases_m3158_MethodInfo = 
{
	"add_willRenderCanvases"/* name */
	, (methodPointerType)&Canvas_add_willRenderCanvases_m3158/* method */
	, &Canvas_t414_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, Canvas_t414_Canvas_add_willRenderCanvases_m3158_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 32/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1694/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType WillRenderCanvases_t666_0_0_0;
static const ParameterInfo Canvas_t414_Canvas_remove_willRenderCanvases_m4628_ParameterInfos[] = 
{
	{"value", 0, 134219791, 0, &WillRenderCanvases_t666_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Canvas::remove_willRenderCanvases(UnityEngine.Canvas/WillRenderCanvases)
extern const MethodInfo Canvas_remove_willRenderCanvases_m4628_MethodInfo = 
{
	"remove_willRenderCanvases"/* name */
	, (methodPointerType)&Canvas_remove_willRenderCanvases_m4628/* method */
	, &Canvas_t414_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, Canvas_t414_Canvas_remove_willRenderCanvases_m4628_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 32/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1695/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_RenderMode_t900 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.RenderMode UnityEngine.Canvas::get_renderMode()
extern const MethodInfo Canvas_get_renderMode_m3205_MethodInfo = 
{
	"get_renderMode"/* name */
	, (methodPointerType)&Canvas_get_renderMode_m3205/* method */
	, &Canvas_t414_il2cpp_TypeInfo/* declaring_type */
	, &RenderMode_t900_0_0_0/* return_type */
	, RuntimeInvoker_RenderMode_t900/* invoker_method */
	, NULL/* parameters */
	, 814/* custom_attributes_cache */
	, 2182/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1696/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Canvas::get_isRootCanvas()
extern const MethodInfo Canvas_get_isRootCanvas_m3422_MethodInfo = 
{
	"get_isRootCanvas"/* name */
	, (methodPointerType)&Canvas_get_isRootCanvas_m3422/* method */
	, &Canvas_t414_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 815/* custom_attributes_cache */
	, 2182/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1697/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Camera_t27_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Camera UnityEngine.Canvas::get_worldCamera()
extern const MethodInfo Canvas_get_worldCamera_m3213_MethodInfo = 
{
	"get_worldCamera"/* name */
	, (methodPointerType)&Canvas_get_worldCamera_m3213/* method */
	, &Canvas_t414_il2cpp_TypeInfo/* declaring_type */
	, &Camera_t27_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 816/* custom_attributes_cache */
	, 2182/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1698/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.Canvas::get_scaleFactor()
extern const MethodInfo Canvas_get_scaleFactor_m3405_MethodInfo = 
{
	"get_scaleFactor"/* name */
	, (methodPointerType)&Canvas_get_scaleFactor_m3405/* method */
	, &Canvas_t414_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 817/* custom_attributes_cache */
	, 2182/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1699/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo Canvas_t414_Canvas_set_scaleFactor_m3424_ParameterInfos[] = 
{
	{"value", 0, 134219792, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Canvas::set_scaleFactor(System.Single)
extern const MethodInfo Canvas_set_scaleFactor_m3424_MethodInfo = 
{
	"set_scaleFactor"/* name */
	, (methodPointerType)&Canvas_set_scaleFactor_m3424/* method */
	, &Canvas_t414_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Single_t254/* invoker_method */
	, Canvas_t414_Canvas_set_scaleFactor_m3424_ParameterInfos/* parameters */
	, 818/* custom_attributes_cache */
	, 2182/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1700/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.Canvas::get_referencePixelsPerUnit()
extern const MethodInfo Canvas_get_referencePixelsPerUnit_m3232_MethodInfo = 
{
	"get_referencePixelsPerUnit"/* name */
	, (methodPointerType)&Canvas_get_referencePixelsPerUnit_m3232/* method */
	, &Canvas_t414_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 819/* custom_attributes_cache */
	, 2182/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1701/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo Canvas_t414_Canvas_set_referencePixelsPerUnit_m3425_ParameterInfos[] = 
{
	{"value", 0, 134219793, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Canvas::set_referencePixelsPerUnit(System.Single)
extern const MethodInfo Canvas_set_referencePixelsPerUnit_m3425_MethodInfo = 
{
	"set_referencePixelsPerUnit"/* name */
	, (methodPointerType)&Canvas_set_referencePixelsPerUnit_m3425/* method */
	, &Canvas_t414_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Single_t254/* invoker_method */
	, Canvas_t414_Canvas_set_referencePixelsPerUnit_m3425_ParameterInfos/* parameters */
	, 820/* custom_attributes_cache */
	, 2182/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1702/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Canvas::get_pixelPerfect()
extern const MethodInfo Canvas_get_pixelPerfect_m3191_MethodInfo = 
{
	"get_pixelPerfect"/* name */
	, (methodPointerType)&Canvas_get_pixelPerfect_m3191/* method */
	, &Canvas_t414_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 821/* custom_attributes_cache */
	, 2182/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1703/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.Canvas::get_renderOrder()
extern const MethodInfo Canvas_get_renderOrder_m3207_MethodInfo = 
{
	"get_renderOrder"/* name */
	, (methodPointerType)&Canvas_get_renderOrder_m3207/* method */
	, &Canvas_t414_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 822/* custom_attributes_cache */
	, 2182/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1704/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.Canvas::get_sortingOrder()
extern const MethodInfo Canvas_get_sortingOrder_m3206_MethodInfo = 
{
	"get_sortingOrder"/* name */
	, (methodPointerType)&Canvas_get_sortingOrder_m3206/* method */
	, &Canvas_t414_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 823/* custom_attributes_cache */
	, 2182/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1705/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.Canvas::get_cachedSortingLayerValue()
extern const MethodInfo Canvas_get_cachedSortingLayerValue_m3212_MethodInfo = 
{
	"get_cachedSortingLayerValue"/* name */
	, (methodPointerType)&Canvas_get_cachedSortingLayerValue_m3212/* method */
	, &Canvas_t414_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 824/* custom_attributes_cache */
	, 2182/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1706/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Material_t55_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Material UnityEngine.Canvas::GetDefaultCanvasMaterial()
extern const MethodInfo Canvas_GetDefaultCanvasMaterial_m3178_MethodInfo = 
{
	"GetDefaultCanvasMaterial"/* name */
	, (methodPointerType)&Canvas_GetDefaultCanvasMaterial_m3178/* method */
	, &Canvas_t414_il2cpp_TypeInfo/* declaring_type */
	, &Material_t55_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 825/* custom_attributes_cache */
	, 150/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1707/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Material UnityEngine.Canvas::GetDefaultCanvasTextMaterial()
extern const MethodInfo Canvas_GetDefaultCanvasTextMaterial_m3400_MethodInfo = 
{
	"GetDefaultCanvasTextMaterial"/* name */
	, (methodPointerType)&Canvas_GetDefaultCanvasTextMaterial_m3400/* method */
	, &Canvas_t414_il2cpp_TypeInfo/* declaring_type */
	, &Material_t55_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 826/* custom_attributes_cache */
	, 150/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1708/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Canvas::SendWillRenderCanvases()
extern const MethodInfo Canvas_SendWillRenderCanvases_m4629_MethodInfo = 
{
	"SendWillRenderCanvases"/* name */
	, (methodPointerType)&Canvas_SendWillRenderCanvases_m4629/* method */
	, &Canvas_t414_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1709/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Canvas::ForceUpdateCanvases()
extern const MethodInfo Canvas_ForceUpdateCanvases_m3360_MethodInfo = 
{
	"ForceUpdateCanvases"/* name */
	, (methodPointerType)&Canvas_ForceUpdateCanvases_m3360/* method */
	, &Canvas_t414_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1710/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Canvas_t414_MethodInfos[] =
{
	&Canvas_add_willRenderCanvases_m3158_MethodInfo,
	&Canvas_remove_willRenderCanvases_m4628_MethodInfo,
	&Canvas_get_renderMode_m3205_MethodInfo,
	&Canvas_get_isRootCanvas_m3422_MethodInfo,
	&Canvas_get_worldCamera_m3213_MethodInfo,
	&Canvas_get_scaleFactor_m3405_MethodInfo,
	&Canvas_set_scaleFactor_m3424_MethodInfo,
	&Canvas_get_referencePixelsPerUnit_m3232_MethodInfo,
	&Canvas_set_referencePixelsPerUnit_m3425_MethodInfo,
	&Canvas_get_pixelPerfect_m3191_MethodInfo,
	&Canvas_get_renderOrder_m3207_MethodInfo,
	&Canvas_get_sortingOrder_m3206_MethodInfo,
	&Canvas_get_cachedSortingLayerValue_m3212_MethodInfo,
	&Canvas_GetDefaultCanvasMaterial_m3178_MethodInfo,
	&Canvas_GetDefaultCanvasTextMaterial_m3400_MethodInfo,
	&Canvas_SendWillRenderCanvases_m4629_MethodInfo,
	&Canvas_ForceUpdateCanvases_m3360_MethodInfo,
	NULL
};
extern const MethodInfo Canvas_get_renderMode_m3205_MethodInfo;
static const PropertyInfo Canvas_t414____renderMode_PropertyInfo = 
{
	&Canvas_t414_il2cpp_TypeInfo/* parent */
	, "renderMode"/* name */
	, &Canvas_get_renderMode_m3205_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Canvas_get_isRootCanvas_m3422_MethodInfo;
static const PropertyInfo Canvas_t414____isRootCanvas_PropertyInfo = 
{
	&Canvas_t414_il2cpp_TypeInfo/* parent */
	, "isRootCanvas"/* name */
	, &Canvas_get_isRootCanvas_m3422_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Canvas_get_worldCamera_m3213_MethodInfo;
static const PropertyInfo Canvas_t414____worldCamera_PropertyInfo = 
{
	&Canvas_t414_il2cpp_TypeInfo/* parent */
	, "worldCamera"/* name */
	, &Canvas_get_worldCamera_m3213_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Canvas_get_scaleFactor_m3405_MethodInfo;
extern const MethodInfo Canvas_set_scaleFactor_m3424_MethodInfo;
static const PropertyInfo Canvas_t414____scaleFactor_PropertyInfo = 
{
	&Canvas_t414_il2cpp_TypeInfo/* parent */
	, "scaleFactor"/* name */
	, &Canvas_get_scaleFactor_m3405_MethodInfo/* get */
	, &Canvas_set_scaleFactor_m3424_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Canvas_get_referencePixelsPerUnit_m3232_MethodInfo;
extern const MethodInfo Canvas_set_referencePixelsPerUnit_m3425_MethodInfo;
static const PropertyInfo Canvas_t414____referencePixelsPerUnit_PropertyInfo = 
{
	&Canvas_t414_il2cpp_TypeInfo/* parent */
	, "referencePixelsPerUnit"/* name */
	, &Canvas_get_referencePixelsPerUnit_m3232_MethodInfo/* get */
	, &Canvas_set_referencePixelsPerUnit_m3425_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Canvas_get_pixelPerfect_m3191_MethodInfo;
static const PropertyInfo Canvas_t414____pixelPerfect_PropertyInfo = 
{
	&Canvas_t414_il2cpp_TypeInfo/* parent */
	, "pixelPerfect"/* name */
	, &Canvas_get_pixelPerfect_m3191_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Canvas_get_renderOrder_m3207_MethodInfo;
static const PropertyInfo Canvas_t414____renderOrder_PropertyInfo = 
{
	&Canvas_t414_il2cpp_TypeInfo/* parent */
	, "renderOrder"/* name */
	, &Canvas_get_renderOrder_m3207_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Canvas_get_sortingOrder_m3206_MethodInfo;
static const PropertyInfo Canvas_t414____sortingOrder_PropertyInfo = 
{
	&Canvas_t414_il2cpp_TypeInfo/* parent */
	, "sortingOrder"/* name */
	, &Canvas_get_sortingOrder_m3206_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Canvas_get_cachedSortingLayerValue_m3212_MethodInfo;
static const PropertyInfo Canvas_t414____cachedSortingLayerValue_PropertyInfo = 
{
	&Canvas_t414_il2cpp_TypeInfo/* parent */
	, "cachedSortingLayerValue"/* name */
	, &Canvas_get_cachedSortingLayerValue_m3212_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Canvas_t414_PropertyInfos[] =
{
	&Canvas_t414____renderMode_PropertyInfo,
	&Canvas_t414____isRootCanvas_PropertyInfo,
	&Canvas_t414____worldCamera_PropertyInfo,
	&Canvas_t414____scaleFactor_PropertyInfo,
	&Canvas_t414____referencePixelsPerUnit_PropertyInfo,
	&Canvas_t414____pixelPerfect_PropertyInfo,
	&Canvas_t414____renderOrder_PropertyInfo,
	&Canvas_t414____sortingOrder_PropertyInfo,
	&Canvas_t414____cachedSortingLayerValue_PropertyInfo,
	NULL
};
extern const Il2CppType WillRenderCanvases_t666_0_0_0;
extern const MethodInfo Canvas_add_willRenderCanvases_m3158_MethodInfo;
extern const MethodInfo Canvas_remove_willRenderCanvases_m4628_MethodInfo;
static const EventInfo Canvas_t414____willRenderCanvases_EventInfo = 
{
	"willRenderCanvases"/* name */
	, &WillRenderCanvases_t666_0_0_0/* type */
	, &Canvas_t414_il2cpp_TypeInfo/* parent */
	, &Canvas_add_willRenderCanvases_m3158_MethodInfo/* add */
	, &Canvas_remove_willRenderCanvases_m4628_MethodInfo/* remove */
	, NULL/* raise */
	, 0/* custom_attributes_cache */

};
static const EventInfo* Canvas_t414__EventInfos[] =
{
	&Canvas_t414____willRenderCanvases_EventInfo,
	NULL
};
static const Il2CppType* Canvas_t414_il2cpp_TypeInfo__nestedTypes[1] =
{
	&WillRenderCanvases_t666_0_0_0,
};
extern const MethodInfo Object_Equals_m1047_MethodInfo;
extern const MethodInfo Object_GetHashCode_m1049_MethodInfo;
extern const MethodInfo Object_ToString_m1050_MethodInfo;
static const Il2CppMethodReference Canvas_t414_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool Canvas_t414_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Canvas_t414_1_0_0;
extern const Il2CppType Behaviour_t250_0_0_0;
struct Canvas_t414;
const Il2CppTypeDefinitionMetadata Canvas_t414_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Canvas_t414_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Behaviour_t250_0_0_0/* parent */
	, Canvas_t414_VTable/* vtableMethods */
	, Canvas_t414_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1015/* fieldStart */

};
TypeInfo Canvas_t414_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Canvas"/* name */
	, "UnityEngine"/* namespaze */
	, Canvas_t414_MethodInfos/* methods */
	, Canvas_t414_PropertyInfos/* properties */
	, Canvas_t414__EventInfos/* events */
	, &Canvas_t414_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Canvas_t414_0_0_0/* byval_arg */
	, &Canvas_t414_1_0_0/* this_arg */
	, &Canvas_t414_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Canvas_t414)/* instance_size */
	, sizeof (Canvas_t414)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Canvas_t414_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 17/* method_count */
	, 9/* property_count */
	, 1/* field_count */
	, 1/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.ICanvasRaycastFilter
extern TypeInfo ICanvasRaycastFilter_t673_il2cpp_TypeInfo;
extern const Il2CppType Vector2_t6_0_0_0;
extern const Il2CppType Vector2_t6_0_0_0;
extern const Il2CppType Camera_t27_0_0_0;
static const ParameterInfo ICanvasRaycastFilter_t673_ICanvasRaycastFilter_IsRaycastLocationValid_m5281_ParameterInfos[] = 
{
	{"sp", 0, 134219799, 0, &Vector2_t6_0_0_0},
	{"eventCamera", 1, 134219800, 0, &Camera_t27_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Vector2_t6_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.ICanvasRaycastFilter::IsRaycastLocationValid(UnityEngine.Vector2,UnityEngine.Camera)
extern const MethodInfo ICanvasRaycastFilter_IsRaycastLocationValid_m5281_MethodInfo = 
{
	"IsRaycastLocationValid"/* name */
	, NULL/* method */
	, &ICanvasRaycastFilter_t673_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Vector2_t6_Object_t/* invoker_method */
	, ICanvasRaycastFilter_t673_ICanvasRaycastFilter_IsRaycastLocationValid_m5281_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1715/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ICanvasRaycastFilter_t673_MethodInfos[] =
{
	&ICanvasRaycastFilter_IsRaycastLocationValid_m5281_MethodInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ICanvasRaycastFilter_t673_0_0_0;
extern const Il2CppType ICanvasRaycastFilter_t673_1_0_0;
struct ICanvasRaycastFilter_t673;
const Il2CppTypeDefinitionMetadata ICanvasRaycastFilter_t673_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ICanvasRaycastFilter_t673_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICanvasRaycastFilter"/* name */
	, "UnityEngine"/* namespaze */
	, ICanvasRaycastFilter_t673_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ICanvasRaycastFilter_t673_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ICanvasRaycastFilter_t673_0_0_0/* byval_arg */
	, &ICanvasRaycastFilter_t673_1_0_0/* this_arg */
	, &ICanvasRaycastFilter_t673_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.CanvasGroup
#include "UnityEngine_UnityEngine_CanvasGroup.h"
// Metadata Definition UnityEngine.CanvasGroup
extern TypeInfo CanvasGroup_t672_il2cpp_TypeInfo;
// UnityEngine.CanvasGroup
#include "UnityEngine_UnityEngine_CanvasGroupMethodDeclarations.h"
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.CanvasGroup::get_interactable()
extern const MethodInfo CanvasGroup_get_interactable_m3387_MethodInfo = 
{
	"get_interactable"/* name */
	, (methodPointerType)&CanvasGroup_get_interactable_m3387/* method */
	, &CanvasGroup_t672_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 827/* custom_attributes_cache */
	, 2182/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1716/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.CanvasGroup::get_blocksRaycasts()
extern const MethodInfo CanvasGroup_get_blocksRaycasts_m4630_MethodInfo = 
{
	"get_blocksRaycasts"/* name */
	, (methodPointerType)&CanvasGroup_get_blocksRaycasts_m4630/* method */
	, &CanvasGroup_t672_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 828/* custom_attributes_cache */
	, 2182/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1717/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.CanvasGroup::get_ignoreParentGroups()
extern const MethodInfo CanvasGroup_get_ignoreParentGroups_m3190_MethodInfo = 
{
	"get_ignoreParentGroups"/* name */
	, (methodPointerType)&CanvasGroup_get_ignoreParentGroups_m3190/* method */
	, &CanvasGroup_t672_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 829/* custom_attributes_cache */
	, 2182/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1718/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t6_0_0_0;
extern const Il2CppType Camera_t27_0_0_0;
static const ParameterInfo CanvasGroup_t672_CanvasGroup_IsRaycastLocationValid_m4631_ParameterInfos[] = 
{
	{"sp", 0, 134219801, 0, &Vector2_t6_0_0_0},
	{"eventCamera", 1, 134219802, 0, &Camera_t27_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Vector2_t6_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.CanvasGroup::IsRaycastLocationValid(UnityEngine.Vector2,UnityEngine.Camera)
extern const MethodInfo CanvasGroup_IsRaycastLocationValid_m4631_MethodInfo = 
{
	"IsRaycastLocationValid"/* name */
	, (methodPointerType)&CanvasGroup_IsRaycastLocationValid_m4631/* method */
	, &CanvasGroup_t672_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Vector2_t6_Object_t/* invoker_method */
	, CanvasGroup_t672_CanvasGroup_IsRaycastLocationValid_m4631_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1719/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CanvasGroup_t672_MethodInfos[] =
{
	&CanvasGroup_get_interactable_m3387_MethodInfo,
	&CanvasGroup_get_blocksRaycasts_m4630_MethodInfo,
	&CanvasGroup_get_ignoreParentGroups_m3190_MethodInfo,
	&CanvasGroup_IsRaycastLocationValid_m4631_MethodInfo,
	NULL
};
extern const MethodInfo CanvasGroup_get_interactable_m3387_MethodInfo;
static const PropertyInfo CanvasGroup_t672____interactable_PropertyInfo = 
{
	&CanvasGroup_t672_il2cpp_TypeInfo/* parent */
	, "interactable"/* name */
	, &CanvasGroup_get_interactable_m3387_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo CanvasGroup_get_blocksRaycasts_m4630_MethodInfo;
static const PropertyInfo CanvasGroup_t672____blocksRaycasts_PropertyInfo = 
{
	&CanvasGroup_t672_il2cpp_TypeInfo/* parent */
	, "blocksRaycasts"/* name */
	, &CanvasGroup_get_blocksRaycasts_m4630_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo CanvasGroup_get_ignoreParentGroups_m3190_MethodInfo;
static const PropertyInfo CanvasGroup_t672____ignoreParentGroups_PropertyInfo = 
{
	&CanvasGroup_t672_il2cpp_TypeInfo/* parent */
	, "ignoreParentGroups"/* name */
	, &CanvasGroup_get_ignoreParentGroups_m3190_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* CanvasGroup_t672_PropertyInfos[] =
{
	&CanvasGroup_t672____interactable_PropertyInfo,
	&CanvasGroup_t672____blocksRaycasts_PropertyInfo,
	&CanvasGroup_t672____ignoreParentGroups_PropertyInfo,
	NULL
};
extern const MethodInfo CanvasGroup_IsRaycastLocationValid_m4631_MethodInfo;
static const Il2CppMethodReference CanvasGroup_t672_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	&CanvasGroup_IsRaycastLocationValid_m4631_MethodInfo,
};
static bool CanvasGroup_t672_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* CanvasGroup_t672_InterfacesTypeInfos[] = 
{
	&ICanvasRaycastFilter_t673_0_0_0,
};
static Il2CppInterfaceOffsetPair CanvasGroup_t672_InterfacesOffsets[] = 
{
	{ &ICanvasRaycastFilter_t673_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType CanvasGroup_t672_0_0_0;
extern const Il2CppType CanvasGroup_t672_1_0_0;
extern const Il2CppType Component_t219_0_0_0;
struct CanvasGroup_t672;
const Il2CppTypeDefinitionMetadata CanvasGroup_t672_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, CanvasGroup_t672_InterfacesTypeInfos/* implementedInterfaces */
	, CanvasGroup_t672_InterfacesOffsets/* interfaceOffsets */
	, &Component_t219_0_0_0/* parent */
	, CanvasGroup_t672_VTable/* vtableMethods */
	, CanvasGroup_t672_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo CanvasGroup_t672_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CanvasGroup"/* name */
	, "UnityEngine"/* namespaze */
	, CanvasGroup_t672_MethodInfos/* methods */
	, CanvasGroup_t672_PropertyInfos/* properties */
	, NULL/* events */
	, &CanvasGroup_t672_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CanvasGroup_t672_0_0_0/* byval_arg */
	, &CanvasGroup_t672_1_0_0/* this_arg */
	, &CanvasGroup_t672_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CanvasGroup_t672)/* instance_size */
	, sizeof (CanvasGroup_t672)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 3/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.UIVertex
#include "UnityEngine_UnityEngine_UIVertex.h"
// Metadata Definition UnityEngine.UIVertex
extern TypeInfo UIVertex_t556_il2cpp_TypeInfo;
// UnityEngine.UIVertex
#include "UnityEngine_UnityEngine_UIVertexMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UIVertex::.cctor()
extern const MethodInfo UIVertex__cctor_m4632_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&UIVertex__cctor_m4632/* method */
	, &UIVertex_t556_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1720/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UIVertex_t556_MethodInfos[] =
{
	&UIVertex__cctor_m4632_MethodInfo,
	NULL
};
extern const MethodInfo ValueType_Equals_m1076_MethodInfo;
extern const MethodInfo ValueType_GetHashCode_m1077_MethodInfo;
extern const MethodInfo ValueType_ToString_m1078_MethodInfo;
static const Il2CppMethodReference UIVertex_t556_VTable[] =
{
	&ValueType_Equals_m1076_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&ValueType_GetHashCode_m1077_MethodInfo,
	&ValueType_ToString_m1078_MethodInfo,
};
static bool UIVertex_t556_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UIVertex_t556_0_0_0;
extern const Il2CppType UIVertex_t556_1_0_0;
extern const Il2CppType ValueType_t285_0_0_0;
const Il2CppTypeDefinitionMetadata UIVertex_t556_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t285_0_0_0/* parent */
	, UIVertex_t556_VTable/* vtableMethods */
	, UIVertex_t556_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1016/* fieldStart */

};
TypeInfo UIVertex_t556_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UIVertex"/* name */
	, "UnityEngine"/* namespaze */
	, UIVertex_t556_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UIVertex_t556_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UIVertex_t556_0_0_0/* byval_arg */
	, &UIVertex_t556_1_0_0/* this_arg */
	, &UIVertex_t556_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UIVertex_t556)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UIVertex_t556)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(UIVertex_t556 )/* native_size */
	, sizeof(UIVertex_t556_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, true/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 9/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.CanvasRenderer
#include "UnityEngine_UnityEngine_CanvasRenderer.h"
// Metadata Definition UnityEngine.CanvasRenderer
extern TypeInfo CanvasRenderer_t522_il2cpp_TypeInfo;
// UnityEngine.CanvasRenderer
#include "UnityEngine_UnityEngine_CanvasRendererMethodDeclarations.h"
extern const Il2CppType Color_t65_0_0_0;
extern const Il2CppType Color_t65_0_0_0;
static const ParameterInfo CanvasRenderer_t522_CanvasRenderer_SetColor_m3196_ParameterInfos[] = 
{
	{"color", 0, 134219803, 0, &Color_t65_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Color_t65 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.CanvasRenderer::SetColor(UnityEngine.Color)
extern const MethodInfo CanvasRenderer_SetColor_m3196_MethodInfo = 
{
	"SetColor"/* name */
	, (methodPointerType)&CanvasRenderer_SetColor_m3196/* method */
	, &CanvasRenderer_t522_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Color_t65/* invoker_method */
	, CanvasRenderer_t522_CanvasRenderer_SetColor_m3196_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1721/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType CanvasRenderer_t522_0_0_0;
extern const Il2CppType CanvasRenderer_t522_0_0_0;
extern const Il2CppType Color_t65_1_0_0;
extern const Il2CppType Color_t65_1_0_0;
static const ParameterInfo CanvasRenderer_t522_CanvasRenderer_INTERNAL_CALL_SetColor_m4633_ParameterInfos[] = 
{
	{"self", 0, 134219804, 0, &CanvasRenderer_t522_0_0_0},
	{"color", 1, 134219805, 0, &Color_t65_1_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_ColorU26_t754 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.CanvasRenderer::INTERNAL_CALL_SetColor(UnityEngine.CanvasRenderer,UnityEngine.Color&)
extern const MethodInfo CanvasRenderer_INTERNAL_CALL_SetColor_m4633_MethodInfo = 
{
	"INTERNAL_CALL_SetColor"/* name */
	, (methodPointerType)&CanvasRenderer_INTERNAL_CALL_SetColor_m4633/* method */
	, &CanvasRenderer_t522_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_ColorU26_t754/* invoker_method */
	, CanvasRenderer_t522_CanvasRenderer_INTERNAL_CALL_SetColor_m4633_ParameterInfos/* parameters */
	, 830/* custom_attributes_cache */
	, 145/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1722/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Color_t65 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Color UnityEngine.CanvasRenderer::GetColor()
extern const MethodInfo CanvasRenderer_GetColor_m3194_MethodInfo = 
{
	"GetColor"/* name */
	, (methodPointerType)&CanvasRenderer_GetColor_m3194/* method */
	, &CanvasRenderer_t522_il2cpp_TypeInfo/* declaring_type */
	, &Color_t65_0_0_0/* return_type */
	, RuntimeInvoker_Color_t65/* invoker_method */
	, NULL/* parameters */
	, 831/* custom_attributes_cache */
	, 134/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1723/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo CanvasRenderer_t522_CanvasRenderer_set_isMask_m3456_ParameterInfos[] = 
{
	{"value", 0, 134219806, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.CanvasRenderer::set_isMask(System.Boolean)
extern const MethodInfo CanvasRenderer_set_isMask_m3456_MethodInfo = 
{
	"set_isMask"/* name */
	, (methodPointerType)&CanvasRenderer_set_isMask_m3456/* method */
	, &CanvasRenderer_t522_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274/* invoker_method */
	, CanvasRenderer_t522_CanvasRenderer_set_isMask_m3456_ParameterInfos/* parameters */
	, 832/* custom_attributes_cache */
	, 2182/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1724/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Material_t55_0_0_0;
extern const Il2CppType Texture_t86_0_0_0;
extern const Il2CppType Texture_t86_0_0_0;
static const ParameterInfo CanvasRenderer_t522_CanvasRenderer_SetMaterial_m3188_ParameterInfos[] = 
{
	{"material", 0, 134219807, 0, &Material_t55_0_0_0},
	{"texture", 1, 134219808, 0, &Texture_t86_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.CanvasRenderer::SetMaterial(UnityEngine.Material,UnityEngine.Texture)
extern const MethodInfo CanvasRenderer_SetMaterial_m3188_MethodInfo = 
{
	"SetMaterial"/* name */
	, (methodPointerType)&CanvasRenderer_SetMaterial_m3188/* method */
	, &CanvasRenderer_t522_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, CanvasRenderer_t522_CanvasRenderer_SetMaterial_m3188_ParameterInfos/* parameters */
	, 833/* custom_attributes_cache */
	, 134/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1725/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType List_1_t558_0_0_0;
extern const Il2CppType List_1_t558_0_0_0;
static const ParameterInfo CanvasRenderer_t522_CanvasRenderer_SetVertices_m3186_ParameterInfos[] = 
{
	{"vertices", 0, 134219809, 0, &List_1_t558_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.CanvasRenderer::SetVertices(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
extern const MethodInfo CanvasRenderer_SetVertices_m3186_MethodInfo = 
{
	"SetVertices"/* name */
	, (methodPointerType)&CanvasRenderer_SetVertices_m3186/* method */
	, &CanvasRenderer_t522_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, CanvasRenderer_t522_CanvasRenderer_SetVertices_m3186_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1726/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo CanvasRenderer_t522_CanvasRenderer_SetVerticesInternal_m4634_ParameterInfos[] = 
{
	{"vertices", 0, 134219810, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.CanvasRenderer::SetVerticesInternal(System.Object)
extern const MethodInfo CanvasRenderer_SetVerticesInternal_m4634_MethodInfo = 
{
	"SetVerticesInternal"/* name */
	, (methodPointerType)&CanvasRenderer_SetVerticesInternal_m4634/* method */
	, &CanvasRenderer_t522_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, CanvasRenderer_t522_CanvasRenderer_SetVerticesInternal_m4634_ParameterInfos/* parameters */
	, 834/* custom_attributes_cache */
	, 129/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1727/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UIVertexU5BU5D_t555_0_0_0;
extern const Il2CppType UIVertexU5BU5D_t555_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo CanvasRenderer_t522_CanvasRenderer_SetVertices_m3283_ParameterInfos[] = 
{
	{"vertices", 0, 134219811, 0, &UIVertexU5BU5D_t555_0_0_0},
	{"size", 1, 134219812, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.CanvasRenderer::SetVertices(UnityEngine.UIVertex[],System.Int32)
extern const MethodInfo CanvasRenderer_SetVertices_m3283_MethodInfo = 
{
	"SetVertices"/* name */
	, (methodPointerType)&CanvasRenderer_SetVertices_m3283/* method */
	, &CanvasRenderer_t522_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Int32_t253/* invoker_method */
	, CanvasRenderer_t522_CanvasRenderer_SetVertices_m3283_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1728/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UIVertexU5BU5D_t555_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo CanvasRenderer_t522_CanvasRenderer_SetVerticesInternalArray_m4635_ParameterInfos[] = 
{
	{"vertices", 0, 134219813, 0, &UIVertexU5BU5D_t555_0_0_0},
	{"size", 1, 134219814, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.CanvasRenderer::SetVerticesInternalArray(UnityEngine.UIVertex[],System.Int32)
extern const MethodInfo CanvasRenderer_SetVerticesInternalArray_m4635_MethodInfo = 
{
	"SetVerticesInternalArray"/* name */
	, (methodPointerType)&CanvasRenderer_SetVerticesInternalArray_m4635/* method */
	, &CanvasRenderer_t522_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Int32_t253/* invoker_method */
	, CanvasRenderer_t522_CanvasRenderer_SetVerticesInternalArray_m4635_ParameterInfos/* parameters */
	, 835/* custom_attributes_cache */
	, 129/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1729/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.CanvasRenderer::Clear()
extern const MethodInfo CanvasRenderer_Clear_m3182_MethodInfo = 
{
	"Clear"/* name */
	, (methodPointerType)&CanvasRenderer_Clear_m3182/* method */
	, &CanvasRenderer_t522_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 836/* custom_attributes_cache */
	, 134/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1730/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.CanvasRenderer::get_absoluteDepth()
extern const MethodInfo CanvasRenderer_get_absoluteDepth_m3179_MethodInfo = 
{
	"get_absoluteDepth"/* name */
	, (methodPointerType)&CanvasRenderer_get_absoluteDepth_m3179/* method */
	, &CanvasRenderer_t522_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 837/* custom_attributes_cache */
	, 2182/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1731/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CanvasRenderer_t522_MethodInfos[] =
{
	&CanvasRenderer_SetColor_m3196_MethodInfo,
	&CanvasRenderer_INTERNAL_CALL_SetColor_m4633_MethodInfo,
	&CanvasRenderer_GetColor_m3194_MethodInfo,
	&CanvasRenderer_set_isMask_m3456_MethodInfo,
	&CanvasRenderer_SetMaterial_m3188_MethodInfo,
	&CanvasRenderer_SetVertices_m3186_MethodInfo,
	&CanvasRenderer_SetVerticesInternal_m4634_MethodInfo,
	&CanvasRenderer_SetVertices_m3283_MethodInfo,
	&CanvasRenderer_SetVerticesInternalArray_m4635_MethodInfo,
	&CanvasRenderer_Clear_m3182_MethodInfo,
	&CanvasRenderer_get_absoluteDepth_m3179_MethodInfo,
	NULL
};
extern const MethodInfo CanvasRenderer_set_isMask_m3456_MethodInfo;
static const PropertyInfo CanvasRenderer_t522____isMask_PropertyInfo = 
{
	&CanvasRenderer_t522_il2cpp_TypeInfo/* parent */
	, "isMask"/* name */
	, NULL/* get */
	, &CanvasRenderer_set_isMask_m3456_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo CanvasRenderer_get_absoluteDepth_m3179_MethodInfo;
static const PropertyInfo CanvasRenderer_t522____absoluteDepth_PropertyInfo = 
{
	&CanvasRenderer_t522_il2cpp_TypeInfo/* parent */
	, "absoluteDepth"/* name */
	, &CanvasRenderer_get_absoluteDepth_m3179_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* CanvasRenderer_t522_PropertyInfos[] =
{
	&CanvasRenderer_t522____isMask_PropertyInfo,
	&CanvasRenderer_t522____absoluteDepth_PropertyInfo,
	NULL
};
static const Il2CppMethodReference CanvasRenderer_t522_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool CanvasRenderer_t522_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType CanvasRenderer_t522_1_0_0;
struct CanvasRenderer_t522;
const Il2CppTypeDefinitionMetadata CanvasRenderer_t522_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Component_t219_0_0_0/* parent */
	, CanvasRenderer_t522_VTable/* vtableMethods */
	, CanvasRenderer_t522_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo CanvasRenderer_t522_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CanvasRenderer"/* name */
	, "UnityEngine"/* namespaze */
	, CanvasRenderer_t522_MethodInfos/* methods */
	, CanvasRenderer_t522_PropertyInfos/* properties */
	, NULL/* events */
	, &CanvasRenderer_t522_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CanvasRenderer_t522_0_0_0/* byval_arg */
	, &CanvasRenderer_t522_1_0_0/* this_arg */
	, &CanvasRenderer_t522_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CanvasRenderer_t522)/* instance_size */
	, sizeof (CanvasRenderer_t522)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.RectTransformUtility
#include "UnityEngine_UnityEngine_RectTransformUtility.h"
// Metadata Definition UnityEngine.RectTransformUtility
extern TypeInfo RectTransformUtility_t674_il2cpp_TypeInfo;
// UnityEngine.RectTransformUtility
#include "UnityEngine_UnityEngine_RectTransformUtilityMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.RectTransformUtility::.cctor()
extern const MethodInfo RectTransformUtility__cctor_m4636_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&RectTransformUtility__cctor_m4636/* method */
	, &RectTransformUtility_t674_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1732/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t364_0_0_0;
extern const Il2CppType RectTransform_t364_0_0_0;
extern const Il2CppType Vector2_t6_0_0_0;
extern const Il2CppType Camera_t27_0_0_0;
static const ParameterInfo RectTransformUtility_t674_RectTransformUtility_RectangleContainsScreenPoint_m3214_ParameterInfos[] = 
{
	{"rect", 0, 134219815, 0, &RectTransform_t364_0_0_0},
	{"screenPoint", 1, 134219816, 0, &Vector2_t6_0_0_0},
	{"cam", 2, 134219817, 0, &Camera_t27_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t_Vector2_t6_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.RectTransformUtility::RectangleContainsScreenPoint(UnityEngine.RectTransform,UnityEngine.Vector2,UnityEngine.Camera)
extern const MethodInfo RectTransformUtility_RectangleContainsScreenPoint_m3214_MethodInfo = 
{
	"RectangleContainsScreenPoint"/* name */
	, (methodPointerType)&RectTransformUtility_RectangleContainsScreenPoint_m3214/* method */
	, &RectTransformUtility_t674_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t_Vector2_t6_Object_t/* invoker_method */
	, RectTransformUtility_t674_RectTransformUtility_RectangleContainsScreenPoint_m3214_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1733/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t364_0_0_0;
extern const Il2CppType Vector2_t6_1_0_0;
extern const Il2CppType Vector2_t6_1_0_0;
extern const Il2CppType Camera_t27_0_0_0;
static const ParameterInfo RectTransformUtility_t674_RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m4637_ParameterInfos[] = 
{
	{"rect", 0, 134219818, 0, &RectTransform_t364_0_0_0},
	{"screenPoint", 1, 134219819, 0, &Vector2_t6_1_0_0},
	{"cam", 2, 134219820, 0, &Camera_t27_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t_Vector2U26_t1148_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.RectTransformUtility::INTERNAL_CALL_RectangleContainsScreenPoint(UnityEngine.RectTransform,UnityEngine.Vector2&,UnityEngine.Camera)
extern const MethodInfo RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m4637_MethodInfo = 
{
	"INTERNAL_CALL_RectangleContainsScreenPoint"/* name */
	, (methodPointerType)&RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m4637/* method */
	, &RectTransformUtility_t674_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t_Vector2U26_t1148_Object_t/* invoker_method */
	, RectTransformUtility_t674_RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m4637_ParameterInfos/* parameters */
	, 838/* custom_attributes_cache */
	, 145/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1734/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t6_0_0_0;
extern const Il2CppType Transform_t1_0_0_0;
extern const Il2CppType Transform_t1_0_0_0;
extern const Il2CppType Canvas_t414_0_0_0;
static const ParameterInfo RectTransformUtility_t674_RectTransformUtility_PixelAdjustPoint_m3192_ParameterInfos[] = 
{
	{"point", 0, 134219821, 0, &Vector2_t6_0_0_0},
	{"elementTransform", 1, 134219822, 0, &Transform_t1_0_0_0},
	{"canvas", 2, 134219823, 0, &Canvas_t414_0_0_0},
};
extern void* RuntimeInvoker_Vector2_t6_Vector2_t6_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector2 UnityEngine.RectTransformUtility::PixelAdjustPoint(UnityEngine.Vector2,UnityEngine.Transform,UnityEngine.Canvas)
extern const MethodInfo RectTransformUtility_PixelAdjustPoint_m3192_MethodInfo = 
{
	"PixelAdjustPoint"/* name */
	, (methodPointerType)&RectTransformUtility_PixelAdjustPoint_m3192/* method */
	, &RectTransformUtility_t674_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t6_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t6_Vector2_t6_Object_t_Object_t/* invoker_method */
	, RectTransformUtility_t674_RectTransformUtility_PixelAdjustPoint_m3192_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1735/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t6_0_0_0;
extern const Il2CppType Transform_t1_0_0_0;
extern const Il2CppType Canvas_t414_0_0_0;
extern const Il2CppType Vector2_t6_1_0_2;
static const ParameterInfo RectTransformUtility_t674_RectTransformUtility_PixelAdjustPoint_m4638_ParameterInfos[] = 
{
	{"point", 0, 134219824, 0, &Vector2_t6_0_0_0},
	{"elementTransform", 1, 134219825, 0, &Transform_t1_0_0_0},
	{"canvas", 2, 134219826, 0, &Canvas_t414_0_0_0},
	{"output", 3, 134219827, 0, &Vector2_t6_1_0_2},
};
extern void* RuntimeInvoker_Void_t272_Vector2_t6_Object_t_Object_t_Vector2U26_t1148 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.RectTransformUtility::PixelAdjustPoint(UnityEngine.Vector2,UnityEngine.Transform,UnityEngine.Canvas,UnityEngine.Vector2&)
extern const MethodInfo RectTransformUtility_PixelAdjustPoint_m4638_MethodInfo = 
{
	"PixelAdjustPoint"/* name */
	, (methodPointerType)&RectTransformUtility_PixelAdjustPoint_m4638/* method */
	, &RectTransformUtility_t674_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Vector2_t6_Object_t_Object_t_Vector2U26_t1148/* invoker_method */
	, RectTransformUtility_t674_RectTransformUtility_PixelAdjustPoint_m4638_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1736/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t6_1_0_0;
extern const Il2CppType Transform_t1_0_0_0;
extern const Il2CppType Canvas_t414_0_0_0;
extern const Il2CppType Vector2_t6_1_0_2;
static const ParameterInfo RectTransformUtility_t674_RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m4639_ParameterInfos[] = 
{
	{"point", 0, 134219828, 0, &Vector2_t6_1_0_0},
	{"elementTransform", 1, 134219829, 0, &Transform_t1_0_0_0},
	{"canvas", 2, 134219830, 0, &Canvas_t414_0_0_0},
	{"output", 3, 134219831, 0, &Vector2_t6_1_0_2},
};
extern void* RuntimeInvoker_Void_t272_Vector2U26_t1148_Object_t_Object_t_Vector2U26_t1148 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.RectTransformUtility::INTERNAL_CALL_PixelAdjustPoint(UnityEngine.Vector2&,UnityEngine.Transform,UnityEngine.Canvas,UnityEngine.Vector2&)
extern const MethodInfo RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m4639_MethodInfo = 
{
	"INTERNAL_CALL_PixelAdjustPoint"/* name */
	, (methodPointerType)&RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m4639/* method */
	, &RectTransformUtility_t674_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Vector2U26_t1148_Object_t_Object_t_Vector2U26_t1148/* invoker_method */
	, RectTransformUtility_t674_RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m4639_ParameterInfos/* parameters */
	, 839/* custom_attributes_cache */
	, 145/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1737/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t364_0_0_0;
extern const Il2CppType Canvas_t414_0_0_0;
static const ParameterInfo RectTransformUtility_t674_RectTransformUtility_PixelAdjustRect_m3193_ParameterInfos[] = 
{
	{"rectTransform", 0, 134219832, 0, &RectTransform_t364_0_0_0},
	{"canvas", 1, 134219833, 0, &Canvas_t414_0_0_0},
};
extern const Il2CppType Rect_t304_0_0_0;
extern void* RuntimeInvoker_Rect_t304_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Rect UnityEngine.RectTransformUtility::PixelAdjustRect(UnityEngine.RectTransform,UnityEngine.Canvas)
extern const MethodInfo RectTransformUtility_PixelAdjustRect_m3193_MethodInfo = 
{
	"PixelAdjustRect"/* name */
	, (methodPointerType)&RectTransformUtility_PixelAdjustRect_m3193/* method */
	, &RectTransformUtility_t674_il2cpp_TypeInfo/* declaring_type */
	, &Rect_t304_0_0_0/* return_type */
	, RuntimeInvoker_Rect_t304_Object_t_Object_t/* invoker_method */
	, RectTransformUtility_t674_RectTransformUtility_PixelAdjustRect_m3193_ParameterInfos/* parameters */
	, 840/* custom_attributes_cache */
	, 150/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1738/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t364_0_0_0;
extern const Il2CppType Vector2_t6_0_0_0;
extern const Il2CppType Camera_t27_0_0_0;
extern const Il2CppType Vector3_t4_1_0_2;
extern const Il2CppType Vector3_t4_1_0_0;
static const ParameterInfo RectTransformUtility_t674_RectTransformUtility_ScreenPointToWorldPointInRectangle_m4640_ParameterInfos[] = 
{
	{"rect", 0, 134219834, 0, &RectTransform_t364_0_0_0},
	{"screenPoint", 1, 134219835, 0, &Vector2_t6_0_0_0},
	{"cam", 2, 134219836, 0, &Camera_t27_0_0_0},
	{"worldPoint", 3, 134219837, 0, &Vector3_t4_1_0_2},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t_Vector2_t6_Object_t_Vector3U26_t1142 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.RectTransformUtility::ScreenPointToWorldPointInRectangle(UnityEngine.RectTransform,UnityEngine.Vector2,UnityEngine.Camera,UnityEngine.Vector3&)
extern const MethodInfo RectTransformUtility_ScreenPointToWorldPointInRectangle_m4640_MethodInfo = 
{
	"ScreenPointToWorldPointInRectangle"/* name */
	, (methodPointerType)&RectTransformUtility_ScreenPointToWorldPointInRectangle_m4640/* method */
	, &RectTransformUtility_t674_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t_Vector2_t6_Object_t_Vector3U26_t1142/* invoker_method */
	, RectTransformUtility_t674_RectTransformUtility_ScreenPointToWorldPointInRectangle_m4640_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1739/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t364_0_0_0;
extern const Il2CppType Vector2_t6_0_0_0;
extern const Il2CppType Camera_t27_0_0_0;
extern const Il2CppType Vector2_t6_1_0_2;
static const ParameterInfo RectTransformUtility_t674_RectTransformUtility_ScreenPointToLocalPointInRectangle_m3251_ParameterInfos[] = 
{
	{"rect", 0, 134219838, 0, &RectTransform_t364_0_0_0},
	{"screenPoint", 1, 134219839, 0, &Vector2_t6_0_0_0},
	{"cam", 2, 134219840, 0, &Camera_t27_0_0_0},
	{"localPoint", 3, 134219841, 0, &Vector2_t6_1_0_2},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t_Vector2_t6_Object_t_Vector2U26_t1148 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.RectTransformUtility::ScreenPointToLocalPointInRectangle(UnityEngine.RectTransform,UnityEngine.Vector2,UnityEngine.Camera,UnityEngine.Vector2&)
extern const MethodInfo RectTransformUtility_ScreenPointToLocalPointInRectangle_m3251_MethodInfo = 
{
	"ScreenPointToLocalPointInRectangle"/* name */
	, (methodPointerType)&RectTransformUtility_ScreenPointToLocalPointInRectangle_m3251/* method */
	, &RectTransformUtility_t674_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t_Vector2_t6_Object_t_Vector2U26_t1148/* invoker_method */
	, RectTransformUtility_t674_RectTransformUtility_ScreenPointToLocalPointInRectangle_m3251_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1740/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Camera_t27_0_0_0;
extern const Il2CppType Vector2_t6_0_0_0;
static const ParameterInfo RectTransformUtility_t674_RectTransformUtility_ScreenPointToRay_m4641_ParameterInfos[] = 
{
	{"cam", 0, 134219842, 0, &Camera_t27_0_0_0},
	{"screenPos", 1, 134219843, 0, &Vector2_t6_0_0_0},
};
extern const Il2CppType Ray_t26_0_0_0;
extern void* RuntimeInvoker_Ray_t26_Object_t_Vector2_t6 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Ray UnityEngine.RectTransformUtility::ScreenPointToRay(UnityEngine.Camera,UnityEngine.Vector2)
extern const MethodInfo RectTransformUtility_ScreenPointToRay_m4641_MethodInfo = 
{
	"ScreenPointToRay"/* name */
	, (methodPointerType)&RectTransformUtility_ScreenPointToRay_m4641/* method */
	, &RectTransformUtility_t674_il2cpp_TypeInfo/* declaring_type */
	, &Ray_t26_0_0_0/* return_type */
	, RuntimeInvoker_Ray_t26_Object_t_Vector2_t6/* invoker_method */
	, RectTransformUtility_t674_RectTransformUtility_ScreenPointToRay_m4641_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1741/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t364_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo RectTransformUtility_t674_RectTransformUtility_FlipLayoutOnAxis_m3355_ParameterInfos[] = 
{
	{"rect", 0, 134219844, 0, &RectTransform_t364_0_0_0},
	{"axis", 1, 134219845, 0, &Int32_t253_0_0_0},
	{"keepPositioning", 2, 134219846, 0, &Boolean_t273_0_0_0},
	{"recursive", 3, 134219847, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Int32_t253_SByte_t274_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.RectTransformUtility::FlipLayoutOnAxis(UnityEngine.RectTransform,System.Int32,System.Boolean,System.Boolean)
extern const MethodInfo RectTransformUtility_FlipLayoutOnAxis_m3355_MethodInfo = 
{
	"FlipLayoutOnAxis"/* name */
	, (methodPointerType)&RectTransformUtility_FlipLayoutOnAxis_m3355/* method */
	, &RectTransformUtility_t674_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Int32_t253_SByte_t274_SByte_t274/* invoker_method */
	, RectTransformUtility_t674_RectTransformUtility_FlipLayoutOnAxis_m3355_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1742/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t364_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo RectTransformUtility_t674_RectTransformUtility_FlipLayoutAxes_m3354_ParameterInfos[] = 
{
	{"rect", 0, 134219848, 0, &RectTransform_t364_0_0_0},
	{"keepPositioning", 1, 134219849, 0, &Boolean_t273_0_0_0},
	{"recursive", 2, 134219850, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_SByte_t274_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.RectTransformUtility::FlipLayoutAxes(UnityEngine.RectTransform,System.Boolean,System.Boolean)
extern const MethodInfo RectTransformUtility_FlipLayoutAxes_m3354_MethodInfo = 
{
	"FlipLayoutAxes"/* name */
	, (methodPointerType)&RectTransformUtility_FlipLayoutAxes_m3354/* method */
	, &RectTransformUtility_t674_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_SByte_t274_SByte_t274/* invoker_method */
	, RectTransformUtility_t674_RectTransformUtility_FlipLayoutAxes_m3354_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1743/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t6_0_0_0;
static const ParameterInfo RectTransformUtility_t674_RectTransformUtility_GetTransposed_m4642_ParameterInfos[] = 
{
	{"input", 0, 134219851, 0, &Vector2_t6_0_0_0},
};
extern void* RuntimeInvoker_Vector2_t6_Vector2_t6 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector2 UnityEngine.RectTransformUtility::GetTransposed(UnityEngine.Vector2)
extern const MethodInfo RectTransformUtility_GetTransposed_m4642_MethodInfo = 
{
	"GetTransposed"/* name */
	, (methodPointerType)&RectTransformUtility_GetTransposed_m4642/* method */
	, &RectTransformUtility_t674_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t6_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t6_Vector2_t6/* invoker_method */
	, RectTransformUtility_t674_RectTransformUtility_GetTransposed_m4642_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1744/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* RectTransformUtility_t674_MethodInfos[] =
{
	&RectTransformUtility__cctor_m4636_MethodInfo,
	&RectTransformUtility_RectangleContainsScreenPoint_m3214_MethodInfo,
	&RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m4637_MethodInfo,
	&RectTransformUtility_PixelAdjustPoint_m3192_MethodInfo,
	&RectTransformUtility_PixelAdjustPoint_m4638_MethodInfo,
	&RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m4639_MethodInfo,
	&RectTransformUtility_PixelAdjustRect_m3193_MethodInfo,
	&RectTransformUtility_ScreenPointToWorldPointInRectangle_m4640_MethodInfo,
	&RectTransformUtility_ScreenPointToLocalPointInRectangle_m3251_MethodInfo,
	&RectTransformUtility_ScreenPointToRay_m4641_MethodInfo,
	&RectTransformUtility_FlipLayoutOnAxis_m3355_MethodInfo,
	&RectTransformUtility_FlipLayoutAxes_m3354_MethodInfo,
	&RectTransformUtility_GetTransposed_m4642_MethodInfo,
	NULL
};
extern const MethodInfo Object_Equals_m1073_MethodInfo;
extern const MethodInfo Object_GetHashCode_m1074_MethodInfo;
static const Il2CppMethodReference RectTransformUtility_t674_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool RectTransformUtility_t674_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType RectTransformUtility_t674_0_0_0;
extern const Il2CppType RectTransformUtility_t674_1_0_0;
struct RectTransformUtility_t674;
const Il2CppTypeDefinitionMetadata RectTransformUtility_t674_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, RectTransformUtility_t674_VTable/* vtableMethods */
	, RectTransformUtility_t674_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1025/* fieldStart */

};
TypeInfo RectTransformUtility_t674_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "RectTransformUtility"/* name */
	, "UnityEngine"/* namespaze */
	, RectTransformUtility_t674_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &RectTransformUtility_t674_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RectTransformUtility_t674_0_0_0/* byval_arg */
	, &RectTransformUtility_t674_1_0_0/* this_arg */
	, &RectTransformUtility_t674_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RectTransformUtility_t674)/* instance_size */
	, sizeof (RectTransformUtility_t674)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(RectTransformUtility_t674_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 13/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Networking.Match.Request
#include "UnityEngine_UnityEngine_Networking_Match_Request.h"
// Metadata Definition UnityEngine.Networking.Match.Request
extern TypeInfo Request_t901_il2cpp_TypeInfo;
// UnityEngine.Networking.Match.Request
#include "UnityEngine_UnityEngine_Networking_Match_RequestMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Networking.Match.Request::.ctor()
extern const MethodInfo Request__ctor_m4643_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Request__ctor_m4643/* method */
	, &Request_t901_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1745/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SourceID_t919_0_0_0;
extern void* RuntimeInvoker_SourceID_t919 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Networking.Types.SourceID UnityEngine.Networking.Match.Request::get_sourceId()
extern const MethodInfo Request_get_sourceId_m4644_MethodInfo = 
{
	"get_sourceId"/* name */
	, (methodPointerType)&Request_get_sourceId_m4644/* method */
	, &Request_t901_il2cpp_TypeInfo/* declaring_type */
	, &SourceID_t919_0_0_0/* return_type */
	, RuntimeInvoker_SourceID_t919/* invoker_method */
	, NULL/* parameters */
	, 844/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1746/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType AppID_t918_0_0_0;
extern void* RuntimeInvoker_AppID_t918 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Networking.Types.AppID UnityEngine.Networking.Match.Request::get_appId()
extern const MethodInfo Request_get_appId_m4645_MethodInfo = 
{
	"get_appId"/* name */
	, (methodPointerType)&Request_get_appId_m4645/* method */
	, &Request_t901_il2cpp_TypeInfo/* declaring_type */
	, &AppID_t918_0_0_0/* return_type */
	, RuntimeInvoker_AppID_t918/* invoker_method */
	, NULL/* parameters */
	, 845/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1747/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.Networking.Match.Request::get_domain()
extern const MethodInfo Request_get_domain_m4646_MethodInfo = 
{
	"get_domain"/* name */
	, (methodPointerType)&Request_get_domain_m4646/* method */
	, &Request_t901_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 846/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1748/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Networking.Match.Request::ToString()
extern const MethodInfo Request_ToString_m4647_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&Request_ToString_m4647/* method */
	, &Request_t901_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1749/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Request_t901_MethodInfos[] =
{
	&Request__ctor_m4643_MethodInfo,
	&Request_get_sourceId_m4644_MethodInfo,
	&Request_get_appId_m4645_MethodInfo,
	&Request_get_domain_m4646_MethodInfo,
	&Request_ToString_m4647_MethodInfo,
	NULL
};
extern const MethodInfo Request_get_sourceId_m4644_MethodInfo;
static const PropertyInfo Request_t901____sourceId_PropertyInfo = 
{
	&Request_t901_il2cpp_TypeInfo/* parent */
	, "sourceId"/* name */
	, &Request_get_sourceId_m4644_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Request_get_appId_m4645_MethodInfo;
static const PropertyInfo Request_t901____appId_PropertyInfo = 
{
	&Request_t901_il2cpp_TypeInfo/* parent */
	, "appId"/* name */
	, &Request_get_appId_m4645_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Request_get_domain_m4646_MethodInfo;
static const PropertyInfo Request_t901____domain_PropertyInfo = 
{
	&Request_t901_il2cpp_TypeInfo/* parent */
	, "domain"/* name */
	, &Request_get_domain_m4646_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Request_t901_PropertyInfos[] =
{
	&Request_t901____sourceId_PropertyInfo,
	&Request_t901____appId_PropertyInfo,
	&Request_t901____domain_PropertyInfo,
	NULL
};
extern const MethodInfo Request_ToString_m4647_MethodInfo;
static const Il2CppMethodReference Request_t901_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Request_ToString_m4647_MethodInfo,
};
static bool Request_t901_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Request_t901_0_0_0;
extern const Il2CppType Request_t901_1_0_0;
struct Request_t901;
const Il2CppTypeDefinitionMetadata Request_t901_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Request_t901_VTable/* vtableMethods */
	, Request_t901_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1026/* fieldStart */

};
TypeInfo Request_t901_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Request"/* name */
	, "UnityEngine.Networking.Match"/* namespaze */
	, Request_t901_MethodInfos/* methods */
	, Request_t901_PropertyInfos/* properties */
	, NULL/* events */
	, &Request_t901_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Request_t901_0_0_0/* byval_arg */
	, &Request_t901_1_0_0/* this_arg */
	, &Request_t901_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Request_t901)/* instance_size */
	, sizeof (Request_t901)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 3/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Networking.Match.ResponseBase
#include "UnityEngine_UnityEngine_Networking_Match_ResponseBase.h"
// Metadata Definition UnityEngine.Networking.Match.ResponseBase
extern TypeInfo ResponseBase_t902_il2cpp_TypeInfo;
// UnityEngine.Networking.Match.ResponseBase
#include "UnityEngine_UnityEngine_Networking_Match_ResponseBaseMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Networking.Match.ResponseBase::.ctor()
extern const MethodInfo ResponseBase__ctor_m4648_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ResponseBase__ctor_m4648/* method */
	, &ResponseBase_t902_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1750/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo ResponseBase_t902_ResponseBase_Parse_m5282_ParameterInfos[] = 
{
	{"obj", 0, 134219852, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Networking.Match.ResponseBase::Parse(System.Object)
extern const MethodInfo ResponseBase_Parse_m5282_MethodInfo = 
{
	"Parse"/* name */
	, NULL/* method */
	, &ResponseBase_t902_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, ResponseBase_t902_ResponseBase_Parse_m5282_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1751/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IDictionary_2_t1027_0_0_0;
extern const Il2CppType IDictionary_2_t1027_0_0_0;
static const ParameterInfo ResponseBase_t902_ResponseBase_ParseJSONString_m4649_ParameterInfos[] = 
{
	{"name", 0, 134219853, 0, &String_t_0_0_0},
	{"obj", 1, 134219854, 0, &Object_t_0_0_0},
	{"dictJsonObj", 2, 134219855, 0, &IDictionary_2_t1027_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Networking.Match.ResponseBase::ParseJSONString(System.String,System.Object,System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern const MethodInfo ResponseBase_ParseJSONString_m4649_MethodInfo = 
{
	"ParseJSONString"/* name */
	, (methodPointerType)&ResponseBase_ParseJSONString_m4649/* method */
	, &ResponseBase_t902_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, ResponseBase_t902_ResponseBase_ParseJSONString_m4649_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1752/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IDictionary_2_t1027_0_0_0;
static const ParameterInfo ResponseBase_t902_ResponseBase_ParseJSONInt32_m4650_ParameterInfos[] = 
{
	{"name", 0, 134219856, 0, &String_t_0_0_0},
	{"obj", 1, 134219857, 0, &Object_t_0_0_0},
	{"dictJsonObj", 2, 134219858, 0, &IDictionary_2_t1027_0_0_0},
};
extern void* RuntimeInvoker_Int32_t253_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.Networking.Match.ResponseBase::ParseJSONInt32(System.String,System.Object,System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern const MethodInfo ResponseBase_ParseJSONInt32_m4650_MethodInfo = 
{
	"ParseJSONInt32"/* name */
	, (methodPointerType)&ResponseBase_ParseJSONInt32_m4650/* method */
	, &ResponseBase_t902_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253_Object_t_Object_t_Object_t/* invoker_method */
	, ResponseBase_t902_ResponseBase_ParseJSONInt32_m4650_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1753/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IDictionary_2_t1027_0_0_0;
static const ParameterInfo ResponseBase_t902_ResponseBase_ParseJSONUInt16_m4651_ParameterInfos[] = 
{
	{"name", 0, 134219859, 0, &String_t_0_0_0},
	{"obj", 1, 134219860, 0, &Object_t_0_0_0},
	{"dictJsonObj", 2, 134219861, 0, &IDictionary_2_t1027_0_0_0},
};
extern const Il2CppType UInt16_t684_0_0_0;
extern void* RuntimeInvoker_UInt16_t684_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.UInt16 UnityEngine.Networking.Match.ResponseBase::ParseJSONUInt16(System.String,System.Object,System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern const MethodInfo ResponseBase_ParseJSONUInt16_m4651_MethodInfo = 
{
	"ParseJSONUInt16"/* name */
	, (methodPointerType)&ResponseBase_ParseJSONUInt16_m4651/* method */
	, &ResponseBase_t902_il2cpp_TypeInfo/* declaring_type */
	, &UInt16_t684_0_0_0/* return_type */
	, RuntimeInvoker_UInt16_t684_Object_t_Object_t_Object_t/* invoker_method */
	, ResponseBase_t902_ResponseBase_ParseJSONUInt16_m4651_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1754/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IDictionary_2_t1027_0_0_0;
static const ParameterInfo ResponseBase_t902_ResponseBase_ParseJSONUInt64_m4652_ParameterInfos[] = 
{
	{"name", 0, 134219862, 0, &String_t_0_0_0},
	{"obj", 1, 134219863, 0, &Object_t_0_0_0},
	{"dictJsonObj", 2, 134219864, 0, &IDictionary_2_t1027_0_0_0},
};
extern const Il2CppType UInt64_t1074_0_0_0;
extern void* RuntimeInvoker_UInt64_t1074_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.UInt64 UnityEngine.Networking.Match.ResponseBase::ParseJSONUInt64(System.String,System.Object,System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern const MethodInfo ResponseBase_ParseJSONUInt64_m4652_MethodInfo = 
{
	"ParseJSONUInt64"/* name */
	, (methodPointerType)&ResponseBase_ParseJSONUInt64_m4652/* method */
	, &ResponseBase_t902_il2cpp_TypeInfo/* declaring_type */
	, &UInt64_t1074_0_0_0/* return_type */
	, RuntimeInvoker_UInt64_t1074_Object_t_Object_t_Object_t/* invoker_method */
	, ResponseBase_t902_ResponseBase_ParseJSONUInt64_m4652_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1755/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IDictionary_2_t1027_0_0_0;
static const ParameterInfo ResponseBase_t902_ResponseBase_ParseJSONBool_m4653_ParameterInfos[] = 
{
	{"name", 0, 134219865, 0, &String_t_0_0_0},
	{"obj", 1, 134219866, 0, &Object_t_0_0_0},
	{"dictJsonObj", 2, 134219867, 0, &IDictionary_2_t1027_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Networking.Match.ResponseBase::ParseJSONBool(System.String,System.Object,System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern const MethodInfo ResponseBase_ParseJSONBool_m4653_MethodInfo = 
{
	"ParseJSONBool"/* name */
	, (methodPointerType)&ResponseBase_ParseJSONBool_m4653/* method */
	, &ResponseBase_t902_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t_Object_t_Object_t/* invoker_method */
	, ResponseBase_t902_ResponseBase_ParseJSONBool_m4653_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1756/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IDictionary_2_t1027_0_0_0;
static const ParameterInfo ResponseBase_t902_ResponseBase_ParseJSONList_m5283_ParameterInfos[] = 
{
	{"name", 0, 134219868, 0, &String_t_0_0_0},
	{"obj", 1, 134219869, 0, &Object_t_0_0_0},
	{"dictJsonObj", 2, 134219870, 0, &IDictionary_2_t1027_0_0_0},
};
extern const Il2CppType List_1_t1188_0_0_0;
extern const Il2CppGenericContainer ResponseBase_ParseJSONList_m5283_Il2CppGenericContainer;
extern TypeInfo ResponseBase_ParseJSONList_m5283_gp_T_0_il2cpp_TypeInfo;
extern const Il2CppType ResponseBase_t902_0_0_0;
static const Il2CppType* ResponseBase_ParseJSONList_m5283_gp_T_0_il2cpp_TypeInfo_constraints[] = { 
&ResponseBase_t902_0_0_0 /* UnityEngine.Networking.Match.ResponseBase */, 
 NULL };
extern const Il2CppGenericParameter ResponseBase_ParseJSONList_m5283_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &ResponseBase_ParseJSONList_m5283_Il2CppGenericContainer, ResponseBase_ParseJSONList_m5283_gp_T_0_il2cpp_TypeInfo_constraints, "T", 0, 16 };
static const Il2CppGenericParameter* ResponseBase_ParseJSONList_m5283_Il2CppGenericParametersArray[1] = 
{
	&ResponseBase_ParseJSONList_m5283_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo ResponseBase_ParseJSONList_m5283_MethodInfo;
extern const Il2CppGenericContainer ResponseBase_ParseJSONList_m5283_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&ResponseBase_ParseJSONList_m5283_MethodInfo, 1, 1, ResponseBase_ParseJSONList_m5283_Il2CppGenericParametersArray };
extern const Il2CppGenericMethod List_1__ctor_m5398_GenericMethod;
extern const Il2CppGenericMethod Activator_CreateInstance_TisT_t1189_m5399_GenericMethod;
extern const Il2CppType ResponseBase_ParseJSONList_m5283_gp_0_0_0_0;
extern const Il2CppGenericMethod List_1_Add_m5400_GenericMethod;
static Il2CppRGCTXDefinition ResponseBase_ParseJSONList_m5283_RGCTXData[6] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&List_1_t1188_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &List_1__ctor_m5398_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Activator_CreateInstance_TisT_t1189_m5399_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&ResponseBase_ParseJSONList_m5283_gp_0_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &List_1_Add_m5400_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Collections.Generic.List`1<T> UnityEngine.Networking.Match.ResponseBase::ParseJSONList(System.String,System.Object,System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern const MethodInfo ResponseBase_ParseJSONList_m5283_MethodInfo = 
{
	"ParseJSONList"/* name */
	, NULL/* method */
	, &ResponseBase_t902_il2cpp_TypeInfo/* declaring_type */
	, &List_1_t1188_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ResponseBase_t902_ResponseBase_ParseJSONList_m5283_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 1757/* token */
	, ResponseBase_ParseJSONList_m5283_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ResponseBase_ParseJSONList_m5283_Il2CppGenericContainer/* genericContainer */

};
static const MethodInfo* ResponseBase_t902_MethodInfos[] =
{
	&ResponseBase__ctor_m4648_MethodInfo,
	&ResponseBase_Parse_m5282_MethodInfo,
	&ResponseBase_ParseJSONString_m4649_MethodInfo,
	&ResponseBase_ParseJSONInt32_m4650_MethodInfo,
	&ResponseBase_ParseJSONUInt16_m4651_MethodInfo,
	&ResponseBase_ParseJSONUInt64_m4652_MethodInfo,
	&ResponseBase_ParseJSONBool_m4653_MethodInfo,
	&ResponseBase_ParseJSONList_m5283_MethodInfo,
	NULL
};
static const Il2CppMethodReference ResponseBase_t902_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	NULL,
};
static bool ResponseBase_t902_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ResponseBase_t902_1_0_0;
struct ResponseBase_t902;
const Il2CppTypeDefinitionMetadata ResponseBase_t902_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ResponseBase_t902_VTable/* vtableMethods */
	, ResponseBase_t902_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ResponseBase_t902_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ResponseBase"/* name */
	, "UnityEngine.Networking.Match"/* namespaze */
	, ResponseBase_t902_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ResponseBase_t902_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ResponseBase_t902_0_0_0/* byval_arg */
	, &ResponseBase_t902_1_0_0/* this_arg */
	, &ResponseBase_t902_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ResponseBase_t902)/* instance_size */
	, sizeof (ResponseBase_t902)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Networking.Match.IResponse
extern TypeInfo IResponse_t1115_il2cpp_TypeInfo;
static const MethodInfo* IResponse_t1115_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType IResponse_t1115_0_0_0;
extern const Il2CppType IResponse_t1115_1_0_0;
struct IResponse_t1115;
const Il2CppTypeDefinitionMetadata IResponse_t1115_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IResponse_t1115_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "IResponse"/* name */
	, "UnityEngine.Networking.Match"/* namespaze */
	, IResponse_t1115_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IResponse_t1115_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IResponse_t1115_0_0_0/* byval_arg */
	, &IResponse_t1115_1_0_0/* this_arg */
	, &IResponse_t1115_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Networking.Match.Response
#include "UnityEngine_UnityEngine_Networking_Match_Response.h"
// Metadata Definition UnityEngine.Networking.Match.Response
extern TypeInfo Response_t903_il2cpp_TypeInfo;
// UnityEngine.Networking.Match.Response
#include "UnityEngine_UnityEngine_Networking_Match_ResponseMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Networking.Match.Response::.ctor()
extern const MethodInfo Response__ctor_m4654_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Response__ctor_m4654/* method */
	, &Response_t903_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1758/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Networking.Match.Response::get_success()
extern const MethodInfo Response_get_success_m4655_MethodInfo = 
{
	"get_success"/* name */
	, (methodPointerType)&Response_get_success_m4655/* method */
	, &Response_t903_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 849/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1759/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo Response_t903_Response_set_success_m4656_ParameterInfos[] = 
{
	{"value", 0, 134219871, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Networking.Match.Response::set_success(System.Boolean)
extern const MethodInfo Response_set_success_m4656_MethodInfo = 
{
	"set_success"/* name */
	, (methodPointerType)&Response_set_success_m4656/* method */
	, &Response_t903_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274/* invoker_method */
	, Response_t903_Response_set_success_m4656_ParameterInfos/* parameters */
	, 850/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1760/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Networking.Match.Response::get_extendedInfo()
extern const MethodInfo Response_get_extendedInfo_m4657_MethodInfo = 
{
	"get_extendedInfo"/* name */
	, (methodPointerType)&Response_get_extendedInfo_m4657/* method */
	, &Response_t903_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 851/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1761/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Response_t903_Response_set_extendedInfo_m4658_ParameterInfos[] = 
{
	{"value", 0, 134219872, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Networking.Match.Response::set_extendedInfo(System.String)
extern const MethodInfo Response_set_extendedInfo_m4658_MethodInfo = 
{
	"set_extendedInfo"/* name */
	, (methodPointerType)&Response_set_extendedInfo_m4658/* method */
	, &Response_t903_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, Response_t903_Response_set_extendedInfo_m4658_ParameterInfos/* parameters */
	, 852/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1762/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Networking.Match.Response::ToString()
extern const MethodInfo Response_ToString_m4659_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&Response_ToString_m4659/* method */
	, &Response_t903_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1763/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Response_t903_Response_Parse_m4660_ParameterInfos[] = 
{
	{"obj", 0, 134219873, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Networking.Match.Response::Parse(System.Object)
extern const MethodInfo Response_Parse_m4660_MethodInfo = 
{
	"Parse"/* name */
	, (methodPointerType)&Response_Parse_m4660/* method */
	, &Response_t903_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, Response_t903_Response_Parse_m4660_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1764/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Response_t903_MethodInfos[] =
{
	&Response__ctor_m4654_MethodInfo,
	&Response_get_success_m4655_MethodInfo,
	&Response_set_success_m4656_MethodInfo,
	&Response_get_extendedInfo_m4657_MethodInfo,
	&Response_set_extendedInfo_m4658_MethodInfo,
	&Response_ToString_m4659_MethodInfo,
	&Response_Parse_m4660_MethodInfo,
	NULL
};
extern const MethodInfo Response_get_success_m4655_MethodInfo;
extern const MethodInfo Response_set_success_m4656_MethodInfo;
static const PropertyInfo Response_t903____success_PropertyInfo = 
{
	&Response_t903_il2cpp_TypeInfo/* parent */
	, "success"/* name */
	, &Response_get_success_m4655_MethodInfo/* get */
	, &Response_set_success_m4656_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Response_get_extendedInfo_m4657_MethodInfo;
extern const MethodInfo Response_set_extendedInfo_m4658_MethodInfo;
static const PropertyInfo Response_t903____extendedInfo_PropertyInfo = 
{
	&Response_t903_il2cpp_TypeInfo/* parent */
	, "extendedInfo"/* name */
	, &Response_get_extendedInfo_m4657_MethodInfo/* get */
	, &Response_set_extendedInfo_m4658_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Response_t903_PropertyInfos[] =
{
	&Response_t903____success_PropertyInfo,
	&Response_t903____extendedInfo_PropertyInfo,
	NULL
};
extern const MethodInfo Response_ToString_m4659_MethodInfo;
extern const MethodInfo Response_Parse_m4660_MethodInfo;
static const Il2CppMethodReference Response_t903_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Response_ToString_m4659_MethodInfo,
	&Response_Parse_m4660_MethodInfo,
};
static bool Response_t903_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* Response_t903_InterfacesTypeInfos[] = 
{
	&IResponse_t1115_0_0_0,
};
static Il2CppInterfaceOffsetPair Response_t903_InterfacesOffsets[] = 
{
	{ &IResponse_t1115_0_0_0, 5},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Response_t903_0_0_0;
extern const Il2CppType Response_t903_1_0_0;
struct Response_t903;
const Il2CppTypeDefinitionMetadata Response_t903_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Response_t903_InterfacesTypeInfos/* implementedInterfaces */
	, Response_t903_InterfacesOffsets/* interfaceOffsets */
	, &ResponseBase_t902_0_0_0/* parent */
	, Response_t903_VTable/* vtableMethods */
	, Response_t903_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1030/* fieldStart */

};
TypeInfo Response_t903_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Response"/* name */
	, "UnityEngine.Networking.Match"/* namespaze */
	, Response_t903_MethodInfos/* methods */
	, Response_t903_PropertyInfos/* properties */
	, NULL/* events */
	, &Response_t903_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Response_t903_0_0_0/* byval_arg */
	, &Response_t903_1_0_0/* this_arg */
	, &Response_t903_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Response_t903)/* instance_size */
	, sizeof (Response_t903)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.Networking.Match.BasicResponse
#include "UnityEngine_UnityEngine_Networking_Match_BasicResponse.h"
// Metadata Definition UnityEngine.Networking.Match.BasicResponse
extern TypeInfo BasicResponse_t904_il2cpp_TypeInfo;
// UnityEngine.Networking.Match.BasicResponse
#include "UnityEngine_UnityEngine_Networking_Match_BasicResponseMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Networking.Match.BasicResponse::.ctor()
extern const MethodInfo BasicResponse__ctor_m4661_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&BasicResponse__ctor_m4661/* method */
	, &BasicResponse_t904_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1765/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* BasicResponse_t904_MethodInfos[] =
{
	&BasicResponse__ctor_m4661_MethodInfo,
	NULL
};
static const Il2CppMethodReference BasicResponse_t904_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Response_ToString_m4659_MethodInfo,
	&Response_Parse_m4660_MethodInfo,
};
static bool BasicResponse_t904_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair BasicResponse_t904_InterfacesOffsets[] = 
{
	{ &IResponse_t1115_0_0_0, 5},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType BasicResponse_t904_0_0_0;
extern const Il2CppType BasicResponse_t904_1_0_0;
struct BasicResponse_t904;
const Il2CppTypeDefinitionMetadata BasicResponse_t904_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, BasicResponse_t904_InterfacesOffsets/* interfaceOffsets */
	, &Response_t903_0_0_0/* parent */
	, BasicResponse_t904_VTable/* vtableMethods */
	, BasicResponse_t904_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo BasicResponse_t904_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "BasicResponse"/* name */
	, "UnityEngine.Networking.Match"/* namespaze */
	, BasicResponse_t904_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &BasicResponse_t904_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BasicResponse_t904_0_0_0/* byval_arg */
	, &BasicResponse_t904_1_0_0/* this_arg */
	, &BasicResponse_t904_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BasicResponse_t904)/* instance_size */
	, sizeof (BasicResponse_t904)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.Networking.Match.CreateMatchRequest
#include "UnityEngine_UnityEngine_Networking_Match_CreateMatchRequest.h"
// Metadata Definition UnityEngine.Networking.Match.CreateMatchRequest
extern TypeInfo CreateMatchRequest_t906_il2cpp_TypeInfo;
// UnityEngine.Networking.Match.CreateMatchRequest
#include "UnityEngine_UnityEngine_Networking_Match_CreateMatchRequestMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Networking.Match.CreateMatchRequest::.ctor()
extern const MethodInfo CreateMatchRequest__ctor_m4662_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CreateMatchRequest__ctor_m4662/* method */
	, &CreateMatchRequest_t906_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1766/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Networking.Match.CreateMatchRequest::get_name()
extern const MethodInfo CreateMatchRequest_get_name_m4663_MethodInfo = 
{
	"get_name"/* name */
	, (methodPointerType)&CreateMatchRequest_get_name_m4663/* method */
	, &CreateMatchRequest_t906_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 858/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1767/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo CreateMatchRequest_t906_CreateMatchRequest_set_name_m4664_ParameterInfos[] = 
{
	{"value", 0, 134219874, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Networking.Match.CreateMatchRequest::set_name(System.String)
extern const MethodInfo CreateMatchRequest_set_name_m4664_MethodInfo = 
{
	"set_name"/* name */
	, (methodPointerType)&CreateMatchRequest_set_name_m4664/* method */
	, &CreateMatchRequest_t906_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, CreateMatchRequest_t906_CreateMatchRequest_set_name_m4664_ParameterInfos/* parameters */
	, 859/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1768/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UInt32_t1063_0_0_0;
extern void* RuntimeInvoker_UInt32_t1063 (const MethodInfo* method, void* obj, void** args);
// System.UInt32 UnityEngine.Networking.Match.CreateMatchRequest::get_size()
extern const MethodInfo CreateMatchRequest_get_size_m4665_MethodInfo = 
{
	"get_size"/* name */
	, (methodPointerType)&CreateMatchRequest_get_size_m4665/* method */
	, &CreateMatchRequest_t906_il2cpp_TypeInfo/* declaring_type */
	, &UInt32_t1063_0_0_0/* return_type */
	, RuntimeInvoker_UInt32_t1063/* invoker_method */
	, NULL/* parameters */
	, 860/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1769/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UInt32_t1063_0_0_0;
static const ParameterInfo CreateMatchRequest_t906_CreateMatchRequest_set_size_m4666_ParameterInfos[] = 
{
	{"value", 0, 134219875, 0, &UInt32_t1063_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Networking.Match.CreateMatchRequest::set_size(System.UInt32)
extern const MethodInfo CreateMatchRequest_set_size_m4666_MethodInfo = 
{
	"set_size"/* name */
	, (methodPointerType)&CreateMatchRequest_set_size_m4666/* method */
	, &CreateMatchRequest_t906_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, CreateMatchRequest_t906_CreateMatchRequest_set_size_m4666_ParameterInfos/* parameters */
	, 861/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1770/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Networking.Match.CreateMatchRequest::get_advertise()
extern const MethodInfo CreateMatchRequest_get_advertise_m4667_MethodInfo = 
{
	"get_advertise"/* name */
	, (methodPointerType)&CreateMatchRequest_get_advertise_m4667/* method */
	, &CreateMatchRequest_t906_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 862/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1771/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo CreateMatchRequest_t906_CreateMatchRequest_set_advertise_m4668_ParameterInfos[] = 
{
	{"value", 0, 134219876, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Networking.Match.CreateMatchRequest::set_advertise(System.Boolean)
extern const MethodInfo CreateMatchRequest_set_advertise_m4668_MethodInfo = 
{
	"set_advertise"/* name */
	, (methodPointerType)&CreateMatchRequest_set_advertise_m4668/* method */
	, &CreateMatchRequest_t906_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274/* invoker_method */
	, CreateMatchRequest_t906_CreateMatchRequest_set_advertise_m4668_ParameterInfos/* parameters */
	, 863/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1772/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Networking.Match.CreateMatchRequest::get_password()
extern const MethodInfo CreateMatchRequest_get_password_m4669_MethodInfo = 
{
	"get_password"/* name */
	, (methodPointerType)&CreateMatchRequest_get_password_m4669/* method */
	, &CreateMatchRequest_t906_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 864/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1773/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo CreateMatchRequest_t906_CreateMatchRequest_set_password_m4670_ParameterInfos[] = 
{
	{"value", 0, 134219877, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Networking.Match.CreateMatchRequest::set_password(System.String)
extern const MethodInfo CreateMatchRequest_set_password_m4670_MethodInfo = 
{
	"set_password"/* name */
	, (methodPointerType)&CreateMatchRequest_set_password_m4670/* method */
	, &CreateMatchRequest_t906_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, CreateMatchRequest_t906_CreateMatchRequest_set_password_m4670_ParameterInfos/* parameters */
	, 865/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1774/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Dictionary_2_t905_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.Generic.Dictionary`2<System.String,System.Int64> UnityEngine.Networking.Match.CreateMatchRequest::get_matchAttributes()
extern const MethodInfo CreateMatchRequest_get_matchAttributes_m4671_MethodInfo = 
{
	"get_matchAttributes"/* name */
	, (methodPointerType)&CreateMatchRequest_get_matchAttributes_m4671/* method */
	, &CreateMatchRequest_t906_il2cpp_TypeInfo/* declaring_type */
	, &Dictionary_2_t905_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 866/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1775/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Networking.Match.CreateMatchRequest::ToString()
extern const MethodInfo CreateMatchRequest_ToString_m4672_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&CreateMatchRequest_ToString_m4672/* method */
	, &CreateMatchRequest_t906_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1776/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CreateMatchRequest_t906_MethodInfos[] =
{
	&CreateMatchRequest__ctor_m4662_MethodInfo,
	&CreateMatchRequest_get_name_m4663_MethodInfo,
	&CreateMatchRequest_set_name_m4664_MethodInfo,
	&CreateMatchRequest_get_size_m4665_MethodInfo,
	&CreateMatchRequest_set_size_m4666_MethodInfo,
	&CreateMatchRequest_get_advertise_m4667_MethodInfo,
	&CreateMatchRequest_set_advertise_m4668_MethodInfo,
	&CreateMatchRequest_get_password_m4669_MethodInfo,
	&CreateMatchRequest_set_password_m4670_MethodInfo,
	&CreateMatchRequest_get_matchAttributes_m4671_MethodInfo,
	&CreateMatchRequest_ToString_m4672_MethodInfo,
	NULL
};
extern const MethodInfo CreateMatchRequest_get_name_m4663_MethodInfo;
extern const MethodInfo CreateMatchRequest_set_name_m4664_MethodInfo;
static const PropertyInfo CreateMatchRequest_t906____name_PropertyInfo = 
{
	&CreateMatchRequest_t906_il2cpp_TypeInfo/* parent */
	, "name"/* name */
	, &CreateMatchRequest_get_name_m4663_MethodInfo/* get */
	, &CreateMatchRequest_set_name_m4664_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo CreateMatchRequest_get_size_m4665_MethodInfo;
extern const MethodInfo CreateMatchRequest_set_size_m4666_MethodInfo;
static const PropertyInfo CreateMatchRequest_t906____size_PropertyInfo = 
{
	&CreateMatchRequest_t906_il2cpp_TypeInfo/* parent */
	, "size"/* name */
	, &CreateMatchRequest_get_size_m4665_MethodInfo/* get */
	, &CreateMatchRequest_set_size_m4666_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo CreateMatchRequest_get_advertise_m4667_MethodInfo;
extern const MethodInfo CreateMatchRequest_set_advertise_m4668_MethodInfo;
static const PropertyInfo CreateMatchRequest_t906____advertise_PropertyInfo = 
{
	&CreateMatchRequest_t906_il2cpp_TypeInfo/* parent */
	, "advertise"/* name */
	, &CreateMatchRequest_get_advertise_m4667_MethodInfo/* get */
	, &CreateMatchRequest_set_advertise_m4668_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo CreateMatchRequest_get_password_m4669_MethodInfo;
extern const MethodInfo CreateMatchRequest_set_password_m4670_MethodInfo;
static const PropertyInfo CreateMatchRequest_t906____password_PropertyInfo = 
{
	&CreateMatchRequest_t906_il2cpp_TypeInfo/* parent */
	, "password"/* name */
	, &CreateMatchRequest_get_password_m4669_MethodInfo/* get */
	, &CreateMatchRequest_set_password_m4670_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo CreateMatchRequest_get_matchAttributes_m4671_MethodInfo;
static const PropertyInfo CreateMatchRequest_t906____matchAttributes_PropertyInfo = 
{
	&CreateMatchRequest_t906_il2cpp_TypeInfo/* parent */
	, "matchAttributes"/* name */
	, &CreateMatchRequest_get_matchAttributes_m4671_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* CreateMatchRequest_t906_PropertyInfos[] =
{
	&CreateMatchRequest_t906____name_PropertyInfo,
	&CreateMatchRequest_t906____size_PropertyInfo,
	&CreateMatchRequest_t906____advertise_PropertyInfo,
	&CreateMatchRequest_t906____password_PropertyInfo,
	&CreateMatchRequest_t906____matchAttributes_PropertyInfo,
	NULL
};
extern const MethodInfo CreateMatchRequest_ToString_m4672_MethodInfo;
static const Il2CppMethodReference CreateMatchRequest_t906_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&CreateMatchRequest_ToString_m4672_MethodInfo,
};
static bool CreateMatchRequest_t906_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType CreateMatchRequest_t906_0_0_0;
extern const Il2CppType CreateMatchRequest_t906_1_0_0;
struct CreateMatchRequest_t906;
const Il2CppTypeDefinitionMetadata CreateMatchRequest_t906_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Request_t901_0_0_0/* parent */
	, CreateMatchRequest_t906_VTable/* vtableMethods */
	, CreateMatchRequest_t906_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1032/* fieldStart */

};
TypeInfo CreateMatchRequest_t906_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CreateMatchRequest"/* name */
	, "UnityEngine.Networking.Match"/* namespaze */
	, CreateMatchRequest_t906_MethodInfos/* methods */
	, CreateMatchRequest_t906_PropertyInfos/* properties */
	, NULL/* events */
	, &CreateMatchRequest_t906_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CreateMatchRequest_t906_0_0_0/* byval_arg */
	, &CreateMatchRequest_t906_1_0_0/* this_arg */
	, &CreateMatchRequest_t906_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CreateMatchRequest_t906)/* instance_size */
	, sizeof (CreateMatchRequest_t906)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 5/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Networking.Match.CreateMatchResponse
#include "UnityEngine_UnityEngine_Networking_Match_CreateMatchResponse.h"
// Metadata Definition UnityEngine.Networking.Match.CreateMatchResponse
extern TypeInfo CreateMatchResponse_t907_il2cpp_TypeInfo;
// UnityEngine.Networking.Match.CreateMatchResponse
#include "UnityEngine_UnityEngine_Networking_Match_CreateMatchResponseMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Networking.Match.CreateMatchResponse::.ctor()
extern const MethodInfo CreateMatchResponse__ctor_m4673_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CreateMatchResponse__ctor_m4673/* method */
	, &CreateMatchResponse_t907_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1777/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Networking.Match.CreateMatchResponse::get_address()
extern const MethodInfo CreateMatchResponse_get_address_m4674_MethodInfo = 
{
	"get_address"/* name */
	, (methodPointerType)&CreateMatchResponse_get_address_m4674/* method */
	, &CreateMatchResponse_t907_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 873/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1778/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo CreateMatchResponse_t907_CreateMatchResponse_set_address_m4675_ParameterInfos[] = 
{
	{"value", 0, 134219878, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Networking.Match.CreateMatchResponse::set_address(System.String)
extern const MethodInfo CreateMatchResponse_set_address_m4675_MethodInfo = 
{
	"set_address"/* name */
	, (methodPointerType)&CreateMatchResponse_set_address_m4675/* method */
	, &CreateMatchResponse_t907_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, CreateMatchResponse_t907_CreateMatchResponse_set_address_m4675_ParameterInfos/* parameters */
	, 874/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1779/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.Networking.Match.CreateMatchResponse::get_port()
extern const MethodInfo CreateMatchResponse_get_port_m4676_MethodInfo = 
{
	"get_port"/* name */
	, (methodPointerType)&CreateMatchResponse_get_port_m4676/* method */
	, &CreateMatchResponse_t907_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 875/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1780/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo CreateMatchResponse_t907_CreateMatchResponse_set_port_m4677_ParameterInfos[] = 
{
	{"value", 0, 134219879, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Networking.Match.CreateMatchResponse::set_port(System.Int32)
extern const MethodInfo CreateMatchResponse_set_port_m4677_MethodInfo = 
{
	"set_port"/* name */
	, (methodPointerType)&CreateMatchResponse_set_port_m4677/* method */
	, &CreateMatchResponse_t907_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, CreateMatchResponse_t907_CreateMatchResponse_set_port_m4677_ParameterInfos/* parameters */
	, 876/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1781/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType NetworkID_t920_0_0_0;
extern void* RuntimeInvoker_NetworkID_t920 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Networking.Types.NetworkID UnityEngine.Networking.Match.CreateMatchResponse::get_networkId()
extern const MethodInfo CreateMatchResponse_get_networkId_m4678_MethodInfo = 
{
	"get_networkId"/* name */
	, (methodPointerType)&CreateMatchResponse_get_networkId_m4678/* method */
	, &CreateMatchResponse_t907_il2cpp_TypeInfo/* declaring_type */
	, &NetworkID_t920_0_0_0/* return_type */
	, RuntimeInvoker_NetworkID_t920/* invoker_method */
	, NULL/* parameters */
	, 877/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1782/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType NetworkID_t920_0_0_0;
static const ParameterInfo CreateMatchResponse_t907_CreateMatchResponse_set_networkId_m4679_ParameterInfos[] = 
{
	{"value", 0, 134219880, 0, &NetworkID_t920_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_UInt64_t1074 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Networking.Match.CreateMatchResponse::set_networkId(UnityEngine.Networking.Types.NetworkID)
extern const MethodInfo CreateMatchResponse_set_networkId_m4679_MethodInfo = 
{
	"set_networkId"/* name */
	, (methodPointerType)&CreateMatchResponse_set_networkId_m4679/* method */
	, &CreateMatchResponse_t907_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_UInt64_t1074/* invoker_method */
	, CreateMatchResponse_t907_CreateMatchResponse_set_networkId_m4679_ParameterInfos/* parameters */
	, 878/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1783/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Networking.Match.CreateMatchResponse::get_accessTokenString()
extern const MethodInfo CreateMatchResponse_get_accessTokenString_m4680_MethodInfo = 
{
	"get_accessTokenString"/* name */
	, (methodPointerType)&CreateMatchResponse_get_accessTokenString_m4680/* method */
	, &CreateMatchResponse_t907_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 879/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1784/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo CreateMatchResponse_t907_CreateMatchResponse_set_accessTokenString_m4681_ParameterInfos[] = 
{
	{"value", 0, 134219881, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Networking.Match.CreateMatchResponse::set_accessTokenString(System.String)
extern const MethodInfo CreateMatchResponse_set_accessTokenString_m4681_MethodInfo = 
{
	"set_accessTokenString"/* name */
	, (methodPointerType)&CreateMatchResponse_set_accessTokenString_m4681/* method */
	, &CreateMatchResponse_t907_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, CreateMatchResponse_t907_CreateMatchResponse_set_accessTokenString_m4681_ParameterInfos/* parameters */
	, 880/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1785/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType NodeID_t921_0_0_0;
extern void* RuntimeInvoker_NodeID_t921 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Networking.Types.NodeID UnityEngine.Networking.Match.CreateMatchResponse::get_nodeId()
extern const MethodInfo CreateMatchResponse_get_nodeId_m4682_MethodInfo = 
{
	"get_nodeId"/* name */
	, (methodPointerType)&CreateMatchResponse_get_nodeId_m4682/* method */
	, &CreateMatchResponse_t907_il2cpp_TypeInfo/* declaring_type */
	, &NodeID_t921_0_0_0/* return_type */
	, RuntimeInvoker_NodeID_t921/* invoker_method */
	, NULL/* parameters */
	, 881/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1786/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType NodeID_t921_0_0_0;
static const ParameterInfo CreateMatchResponse_t907_CreateMatchResponse_set_nodeId_m4683_ParameterInfos[] = 
{
	{"value", 0, 134219882, 0, &NodeID_t921_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_UInt16_t684 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Networking.Match.CreateMatchResponse::set_nodeId(UnityEngine.Networking.Types.NodeID)
extern const MethodInfo CreateMatchResponse_set_nodeId_m4683_MethodInfo = 
{
	"set_nodeId"/* name */
	, (methodPointerType)&CreateMatchResponse_set_nodeId_m4683/* method */
	, &CreateMatchResponse_t907_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_UInt16_t684/* invoker_method */
	, CreateMatchResponse_t907_CreateMatchResponse_set_nodeId_m4683_ParameterInfos/* parameters */
	, 882/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1787/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Networking.Match.CreateMatchResponse::get_usingRelay()
extern const MethodInfo CreateMatchResponse_get_usingRelay_m4684_MethodInfo = 
{
	"get_usingRelay"/* name */
	, (methodPointerType)&CreateMatchResponse_get_usingRelay_m4684/* method */
	, &CreateMatchResponse_t907_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 883/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1788/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo CreateMatchResponse_t907_CreateMatchResponse_set_usingRelay_m4685_ParameterInfos[] = 
{
	{"value", 0, 134219883, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Networking.Match.CreateMatchResponse::set_usingRelay(System.Boolean)
extern const MethodInfo CreateMatchResponse_set_usingRelay_m4685_MethodInfo = 
{
	"set_usingRelay"/* name */
	, (methodPointerType)&CreateMatchResponse_set_usingRelay_m4685/* method */
	, &CreateMatchResponse_t907_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274/* invoker_method */
	, CreateMatchResponse_t907_CreateMatchResponse_set_usingRelay_m4685_ParameterInfos/* parameters */
	, 884/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1789/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Networking.Match.CreateMatchResponse::ToString()
extern const MethodInfo CreateMatchResponse_ToString_m4686_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&CreateMatchResponse_ToString_m4686/* method */
	, &CreateMatchResponse_t907_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1790/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo CreateMatchResponse_t907_CreateMatchResponse_Parse_m4687_ParameterInfos[] = 
{
	{"obj", 0, 134219884, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Networking.Match.CreateMatchResponse::Parse(System.Object)
extern const MethodInfo CreateMatchResponse_Parse_m4687_MethodInfo = 
{
	"Parse"/* name */
	, (methodPointerType)&CreateMatchResponse_Parse_m4687/* method */
	, &CreateMatchResponse_t907_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, CreateMatchResponse_t907_CreateMatchResponse_Parse_m4687_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1791/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CreateMatchResponse_t907_MethodInfos[] =
{
	&CreateMatchResponse__ctor_m4673_MethodInfo,
	&CreateMatchResponse_get_address_m4674_MethodInfo,
	&CreateMatchResponse_set_address_m4675_MethodInfo,
	&CreateMatchResponse_get_port_m4676_MethodInfo,
	&CreateMatchResponse_set_port_m4677_MethodInfo,
	&CreateMatchResponse_get_networkId_m4678_MethodInfo,
	&CreateMatchResponse_set_networkId_m4679_MethodInfo,
	&CreateMatchResponse_get_accessTokenString_m4680_MethodInfo,
	&CreateMatchResponse_set_accessTokenString_m4681_MethodInfo,
	&CreateMatchResponse_get_nodeId_m4682_MethodInfo,
	&CreateMatchResponse_set_nodeId_m4683_MethodInfo,
	&CreateMatchResponse_get_usingRelay_m4684_MethodInfo,
	&CreateMatchResponse_set_usingRelay_m4685_MethodInfo,
	&CreateMatchResponse_ToString_m4686_MethodInfo,
	&CreateMatchResponse_Parse_m4687_MethodInfo,
	NULL
};
extern const MethodInfo CreateMatchResponse_get_address_m4674_MethodInfo;
extern const MethodInfo CreateMatchResponse_set_address_m4675_MethodInfo;
static const PropertyInfo CreateMatchResponse_t907____address_PropertyInfo = 
{
	&CreateMatchResponse_t907_il2cpp_TypeInfo/* parent */
	, "address"/* name */
	, &CreateMatchResponse_get_address_m4674_MethodInfo/* get */
	, &CreateMatchResponse_set_address_m4675_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo CreateMatchResponse_get_port_m4676_MethodInfo;
extern const MethodInfo CreateMatchResponse_set_port_m4677_MethodInfo;
static const PropertyInfo CreateMatchResponse_t907____port_PropertyInfo = 
{
	&CreateMatchResponse_t907_il2cpp_TypeInfo/* parent */
	, "port"/* name */
	, &CreateMatchResponse_get_port_m4676_MethodInfo/* get */
	, &CreateMatchResponse_set_port_m4677_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo CreateMatchResponse_get_networkId_m4678_MethodInfo;
extern const MethodInfo CreateMatchResponse_set_networkId_m4679_MethodInfo;
static const PropertyInfo CreateMatchResponse_t907____networkId_PropertyInfo = 
{
	&CreateMatchResponse_t907_il2cpp_TypeInfo/* parent */
	, "networkId"/* name */
	, &CreateMatchResponse_get_networkId_m4678_MethodInfo/* get */
	, &CreateMatchResponse_set_networkId_m4679_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo CreateMatchResponse_get_accessTokenString_m4680_MethodInfo;
extern const MethodInfo CreateMatchResponse_set_accessTokenString_m4681_MethodInfo;
static const PropertyInfo CreateMatchResponse_t907____accessTokenString_PropertyInfo = 
{
	&CreateMatchResponse_t907_il2cpp_TypeInfo/* parent */
	, "accessTokenString"/* name */
	, &CreateMatchResponse_get_accessTokenString_m4680_MethodInfo/* get */
	, &CreateMatchResponse_set_accessTokenString_m4681_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo CreateMatchResponse_get_nodeId_m4682_MethodInfo;
extern const MethodInfo CreateMatchResponse_set_nodeId_m4683_MethodInfo;
static const PropertyInfo CreateMatchResponse_t907____nodeId_PropertyInfo = 
{
	&CreateMatchResponse_t907_il2cpp_TypeInfo/* parent */
	, "nodeId"/* name */
	, &CreateMatchResponse_get_nodeId_m4682_MethodInfo/* get */
	, &CreateMatchResponse_set_nodeId_m4683_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo CreateMatchResponse_get_usingRelay_m4684_MethodInfo;
extern const MethodInfo CreateMatchResponse_set_usingRelay_m4685_MethodInfo;
static const PropertyInfo CreateMatchResponse_t907____usingRelay_PropertyInfo = 
{
	&CreateMatchResponse_t907_il2cpp_TypeInfo/* parent */
	, "usingRelay"/* name */
	, &CreateMatchResponse_get_usingRelay_m4684_MethodInfo/* get */
	, &CreateMatchResponse_set_usingRelay_m4685_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* CreateMatchResponse_t907_PropertyInfos[] =
{
	&CreateMatchResponse_t907____address_PropertyInfo,
	&CreateMatchResponse_t907____port_PropertyInfo,
	&CreateMatchResponse_t907____networkId_PropertyInfo,
	&CreateMatchResponse_t907____accessTokenString_PropertyInfo,
	&CreateMatchResponse_t907____nodeId_PropertyInfo,
	&CreateMatchResponse_t907____usingRelay_PropertyInfo,
	NULL
};
extern const MethodInfo CreateMatchResponse_ToString_m4686_MethodInfo;
extern const MethodInfo CreateMatchResponse_Parse_m4687_MethodInfo;
static const Il2CppMethodReference CreateMatchResponse_t907_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&CreateMatchResponse_ToString_m4686_MethodInfo,
	&CreateMatchResponse_Parse_m4687_MethodInfo,
};
static bool CreateMatchResponse_t907_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair CreateMatchResponse_t907_InterfacesOffsets[] = 
{
	{ &IResponse_t1115_0_0_0, 5},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType CreateMatchResponse_t907_0_0_0;
extern const Il2CppType CreateMatchResponse_t907_1_0_0;
struct CreateMatchResponse_t907;
const Il2CppTypeDefinitionMetadata CreateMatchResponse_t907_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CreateMatchResponse_t907_InterfacesOffsets/* interfaceOffsets */
	, &BasicResponse_t904_0_0_0/* parent */
	, CreateMatchResponse_t907_VTable/* vtableMethods */
	, CreateMatchResponse_t907_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1037/* fieldStart */

};
TypeInfo CreateMatchResponse_t907_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CreateMatchResponse"/* name */
	, "UnityEngine.Networking.Match"/* namespaze */
	, CreateMatchResponse_t907_MethodInfos/* methods */
	, CreateMatchResponse_t907_PropertyInfos/* properties */
	, NULL/* events */
	, &CreateMatchResponse_t907_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CreateMatchResponse_t907_0_0_0/* byval_arg */
	, &CreateMatchResponse_t907_1_0_0/* this_arg */
	, &CreateMatchResponse_t907_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CreateMatchResponse_t907)/* instance_size */
	, sizeof (CreateMatchResponse_t907)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 15/* method_count */
	, 6/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.Networking.Match.JoinMatchRequest
#include "UnityEngine_UnityEngine_Networking_Match_JoinMatchRequest.h"
// Metadata Definition UnityEngine.Networking.Match.JoinMatchRequest
extern TypeInfo JoinMatchRequest_t908_il2cpp_TypeInfo;
// UnityEngine.Networking.Match.JoinMatchRequest
#include "UnityEngine_UnityEngine_Networking_Match_JoinMatchRequestMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Networking.Match.JoinMatchRequest::.ctor()
extern const MethodInfo JoinMatchRequest__ctor_m4688_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&JoinMatchRequest__ctor_m4688/* method */
	, &JoinMatchRequest_t908_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1792/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_NetworkID_t920 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Networking.Types.NetworkID UnityEngine.Networking.Match.JoinMatchRequest::get_networkId()
extern const MethodInfo JoinMatchRequest_get_networkId_m4689_MethodInfo = 
{
	"get_networkId"/* name */
	, (methodPointerType)&JoinMatchRequest_get_networkId_m4689/* method */
	, &JoinMatchRequest_t908_il2cpp_TypeInfo/* declaring_type */
	, &NetworkID_t920_0_0_0/* return_type */
	, RuntimeInvoker_NetworkID_t920/* invoker_method */
	, NULL/* parameters */
	, 887/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1793/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType NetworkID_t920_0_0_0;
static const ParameterInfo JoinMatchRequest_t908_JoinMatchRequest_set_networkId_m4690_ParameterInfos[] = 
{
	{"value", 0, 134219885, 0, &NetworkID_t920_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_UInt64_t1074 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Networking.Match.JoinMatchRequest::set_networkId(UnityEngine.Networking.Types.NetworkID)
extern const MethodInfo JoinMatchRequest_set_networkId_m4690_MethodInfo = 
{
	"set_networkId"/* name */
	, (methodPointerType)&JoinMatchRequest_set_networkId_m4690/* method */
	, &JoinMatchRequest_t908_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_UInt64_t1074/* invoker_method */
	, JoinMatchRequest_t908_JoinMatchRequest_set_networkId_m4690_ParameterInfos/* parameters */
	, 888/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1794/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Networking.Match.JoinMatchRequest::get_password()
extern const MethodInfo JoinMatchRequest_get_password_m4691_MethodInfo = 
{
	"get_password"/* name */
	, (methodPointerType)&JoinMatchRequest_get_password_m4691/* method */
	, &JoinMatchRequest_t908_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 889/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1795/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo JoinMatchRequest_t908_JoinMatchRequest_set_password_m4692_ParameterInfos[] = 
{
	{"value", 0, 134219886, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Networking.Match.JoinMatchRequest::set_password(System.String)
extern const MethodInfo JoinMatchRequest_set_password_m4692_MethodInfo = 
{
	"set_password"/* name */
	, (methodPointerType)&JoinMatchRequest_set_password_m4692/* method */
	, &JoinMatchRequest_t908_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, JoinMatchRequest_t908_JoinMatchRequest_set_password_m4692_ParameterInfos/* parameters */
	, 890/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1796/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Networking.Match.JoinMatchRequest::ToString()
extern const MethodInfo JoinMatchRequest_ToString_m4693_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&JoinMatchRequest_ToString_m4693/* method */
	, &JoinMatchRequest_t908_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1797/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* JoinMatchRequest_t908_MethodInfos[] =
{
	&JoinMatchRequest__ctor_m4688_MethodInfo,
	&JoinMatchRequest_get_networkId_m4689_MethodInfo,
	&JoinMatchRequest_set_networkId_m4690_MethodInfo,
	&JoinMatchRequest_get_password_m4691_MethodInfo,
	&JoinMatchRequest_set_password_m4692_MethodInfo,
	&JoinMatchRequest_ToString_m4693_MethodInfo,
	NULL
};
extern const MethodInfo JoinMatchRequest_get_networkId_m4689_MethodInfo;
extern const MethodInfo JoinMatchRequest_set_networkId_m4690_MethodInfo;
static const PropertyInfo JoinMatchRequest_t908____networkId_PropertyInfo = 
{
	&JoinMatchRequest_t908_il2cpp_TypeInfo/* parent */
	, "networkId"/* name */
	, &JoinMatchRequest_get_networkId_m4689_MethodInfo/* get */
	, &JoinMatchRequest_set_networkId_m4690_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo JoinMatchRequest_get_password_m4691_MethodInfo;
extern const MethodInfo JoinMatchRequest_set_password_m4692_MethodInfo;
static const PropertyInfo JoinMatchRequest_t908____password_PropertyInfo = 
{
	&JoinMatchRequest_t908_il2cpp_TypeInfo/* parent */
	, "password"/* name */
	, &JoinMatchRequest_get_password_m4691_MethodInfo/* get */
	, &JoinMatchRequest_set_password_m4692_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* JoinMatchRequest_t908_PropertyInfos[] =
{
	&JoinMatchRequest_t908____networkId_PropertyInfo,
	&JoinMatchRequest_t908____password_PropertyInfo,
	NULL
};
extern const MethodInfo JoinMatchRequest_ToString_m4693_MethodInfo;
static const Il2CppMethodReference JoinMatchRequest_t908_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&JoinMatchRequest_ToString_m4693_MethodInfo,
};
static bool JoinMatchRequest_t908_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType JoinMatchRequest_t908_0_0_0;
extern const Il2CppType JoinMatchRequest_t908_1_0_0;
struct JoinMatchRequest_t908;
const Il2CppTypeDefinitionMetadata JoinMatchRequest_t908_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Request_t901_0_0_0/* parent */
	, JoinMatchRequest_t908_VTable/* vtableMethods */
	, JoinMatchRequest_t908_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1043/* fieldStart */

};
TypeInfo JoinMatchRequest_t908_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "JoinMatchRequest"/* name */
	, "UnityEngine.Networking.Match"/* namespaze */
	, JoinMatchRequest_t908_MethodInfos/* methods */
	, JoinMatchRequest_t908_PropertyInfos/* properties */
	, NULL/* events */
	, &JoinMatchRequest_t908_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &JoinMatchRequest_t908_0_0_0/* byval_arg */
	, &JoinMatchRequest_t908_1_0_0/* this_arg */
	, &JoinMatchRequest_t908_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (JoinMatchRequest_t908)/* instance_size */
	, sizeof (JoinMatchRequest_t908)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Networking.Match.JoinMatchResponse
#include "UnityEngine_UnityEngine_Networking_Match_JoinMatchResponse.h"
// Metadata Definition UnityEngine.Networking.Match.JoinMatchResponse
extern TypeInfo JoinMatchResponse_t909_il2cpp_TypeInfo;
// UnityEngine.Networking.Match.JoinMatchResponse
#include "UnityEngine_UnityEngine_Networking_Match_JoinMatchResponseMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Networking.Match.JoinMatchResponse::.ctor()
extern const MethodInfo JoinMatchResponse__ctor_m4694_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&JoinMatchResponse__ctor_m4694/* method */
	, &JoinMatchResponse_t909_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1798/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Networking.Match.JoinMatchResponse::get_address()
extern const MethodInfo JoinMatchResponse_get_address_m4695_MethodInfo = 
{
	"get_address"/* name */
	, (methodPointerType)&JoinMatchResponse_get_address_m4695/* method */
	, &JoinMatchResponse_t909_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 897/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1799/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo JoinMatchResponse_t909_JoinMatchResponse_set_address_m4696_ParameterInfos[] = 
{
	{"value", 0, 134219887, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Networking.Match.JoinMatchResponse::set_address(System.String)
extern const MethodInfo JoinMatchResponse_set_address_m4696_MethodInfo = 
{
	"set_address"/* name */
	, (methodPointerType)&JoinMatchResponse_set_address_m4696/* method */
	, &JoinMatchResponse_t909_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, JoinMatchResponse_t909_JoinMatchResponse_set_address_m4696_ParameterInfos/* parameters */
	, 898/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1800/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.Networking.Match.JoinMatchResponse::get_port()
extern const MethodInfo JoinMatchResponse_get_port_m4697_MethodInfo = 
{
	"get_port"/* name */
	, (methodPointerType)&JoinMatchResponse_get_port_m4697/* method */
	, &JoinMatchResponse_t909_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 899/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1801/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo JoinMatchResponse_t909_JoinMatchResponse_set_port_m4698_ParameterInfos[] = 
{
	{"value", 0, 134219888, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Networking.Match.JoinMatchResponse::set_port(System.Int32)
extern const MethodInfo JoinMatchResponse_set_port_m4698_MethodInfo = 
{
	"set_port"/* name */
	, (methodPointerType)&JoinMatchResponse_set_port_m4698/* method */
	, &JoinMatchResponse_t909_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, JoinMatchResponse_t909_JoinMatchResponse_set_port_m4698_ParameterInfos/* parameters */
	, 900/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1802/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_NetworkID_t920 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Networking.Types.NetworkID UnityEngine.Networking.Match.JoinMatchResponse::get_networkId()
extern const MethodInfo JoinMatchResponse_get_networkId_m4699_MethodInfo = 
{
	"get_networkId"/* name */
	, (methodPointerType)&JoinMatchResponse_get_networkId_m4699/* method */
	, &JoinMatchResponse_t909_il2cpp_TypeInfo/* declaring_type */
	, &NetworkID_t920_0_0_0/* return_type */
	, RuntimeInvoker_NetworkID_t920/* invoker_method */
	, NULL/* parameters */
	, 901/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1803/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType NetworkID_t920_0_0_0;
static const ParameterInfo JoinMatchResponse_t909_JoinMatchResponse_set_networkId_m4700_ParameterInfos[] = 
{
	{"value", 0, 134219889, 0, &NetworkID_t920_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_UInt64_t1074 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Networking.Match.JoinMatchResponse::set_networkId(UnityEngine.Networking.Types.NetworkID)
extern const MethodInfo JoinMatchResponse_set_networkId_m4700_MethodInfo = 
{
	"set_networkId"/* name */
	, (methodPointerType)&JoinMatchResponse_set_networkId_m4700/* method */
	, &JoinMatchResponse_t909_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_UInt64_t1074/* invoker_method */
	, JoinMatchResponse_t909_JoinMatchResponse_set_networkId_m4700_ParameterInfos/* parameters */
	, 902/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1804/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Networking.Match.JoinMatchResponse::get_accessTokenString()
extern const MethodInfo JoinMatchResponse_get_accessTokenString_m4701_MethodInfo = 
{
	"get_accessTokenString"/* name */
	, (methodPointerType)&JoinMatchResponse_get_accessTokenString_m4701/* method */
	, &JoinMatchResponse_t909_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 903/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1805/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo JoinMatchResponse_t909_JoinMatchResponse_set_accessTokenString_m4702_ParameterInfos[] = 
{
	{"value", 0, 134219890, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Networking.Match.JoinMatchResponse::set_accessTokenString(System.String)
extern const MethodInfo JoinMatchResponse_set_accessTokenString_m4702_MethodInfo = 
{
	"set_accessTokenString"/* name */
	, (methodPointerType)&JoinMatchResponse_set_accessTokenString_m4702/* method */
	, &JoinMatchResponse_t909_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, JoinMatchResponse_t909_JoinMatchResponse_set_accessTokenString_m4702_ParameterInfos/* parameters */
	, 904/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1806/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_NodeID_t921 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Networking.Types.NodeID UnityEngine.Networking.Match.JoinMatchResponse::get_nodeId()
extern const MethodInfo JoinMatchResponse_get_nodeId_m4703_MethodInfo = 
{
	"get_nodeId"/* name */
	, (methodPointerType)&JoinMatchResponse_get_nodeId_m4703/* method */
	, &JoinMatchResponse_t909_il2cpp_TypeInfo/* declaring_type */
	, &NodeID_t921_0_0_0/* return_type */
	, RuntimeInvoker_NodeID_t921/* invoker_method */
	, NULL/* parameters */
	, 905/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1807/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType NodeID_t921_0_0_0;
static const ParameterInfo JoinMatchResponse_t909_JoinMatchResponse_set_nodeId_m4704_ParameterInfos[] = 
{
	{"value", 0, 134219891, 0, &NodeID_t921_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_UInt16_t684 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Networking.Match.JoinMatchResponse::set_nodeId(UnityEngine.Networking.Types.NodeID)
extern const MethodInfo JoinMatchResponse_set_nodeId_m4704_MethodInfo = 
{
	"set_nodeId"/* name */
	, (methodPointerType)&JoinMatchResponse_set_nodeId_m4704/* method */
	, &JoinMatchResponse_t909_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_UInt16_t684/* invoker_method */
	, JoinMatchResponse_t909_JoinMatchResponse_set_nodeId_m4704_ParameterInfos/* parameters */
	, 906/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1808/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Networking.Match.JoinMatchResponse::get_usingRelay()
extern const MethodInfo JoinMatchResponse_get_usingRelay_m4705_MethodInfo = 
{
	"get_usingRelay"/* name */
	, (methodPointerType)&JoinMatchResponse_get_usingRelay_m4705/* method */
	, &JoinMatchResponse_t909_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 907/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1809/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo JoinMatchResponse_t909_JoinMatchResponse_set_usingRelay_m4706_ParameterInfos[] = 
{
	{"value", 0, 134219892, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Networking.Match.JoinMatchResponse::set_usingRelay(System.Boolean)
extern const MethodInfo JoinMatchResponse_set_usingRelay_m4706_MethodInfo = 
{
	"set_usingRelay"/* name */
	, (methodPointerType)&JoinMatchResponse_set_usingRelay_m4706/* method */
	, &JoinMatchResponse_t909_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274/* invoker_method */
	, JoinMatchResponse_t909_JoinMatchResponse_set_usingRelay_m4706_ParameterInfos/* parameters */
	, 908/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1810/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Networking.Match.JoinMatchResponse::ToString()
extern const MethodInfo JoinMatchResponse_ToString_m4707_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&JoinMatchResponse_ToString_m4707/* method */
	, &JoinMatchResponse_t909_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1811/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo JoinMatchResponse_t909_JoinMatchResponse_Parse_m4708_ParameterInfos[] = 
{
	{"obj", 0, 134219893, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Networking.Match.JoinMatchResponse::Parse(System.Object)
extern const MethodInfo JoinMatchResponse_Parse_m4708_MethodInfo = 
{
	"Parse"/* name */
	, (methodPointerType)&JoinMatchResponse_Parse_m4708/* method */
	, &JoinMatchResponse_t909_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, JoinMatchResponse_t909_JoinMatchResponse_Parse_m4708_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1812/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* JoinMatchResponse_t909_MethodInfos[] =
{
	&JoinMatchResponse__ctor_m4694_MethodInfo,
	&JoinMatchResponse_get_address_m4695_MethodInfo,
	&JoinMatchResponse_set_address_m4696_MethodInfo,
	&JoinMatchResponse_get_port_m4697_MethodInfo,
	&JoinMatchResponse_set_port_m4698_MethodInfo,
	&JoinMatchResponse_get_networkId_m4699_MethodInfo,
	&JoinMatchResponse_set_networkId_m4700_MethodInfo,
	&JoinMatchResponse_get_accessTokenString_m4701_MethodInfo,
	&JoinMatchResponse_set_accessTokenString_m4702_MethodInfo,
	&JoinMatchResponse_get_nodeId_m4703_MethodInfo,
	&JoinMatchResponse_set_nodeId_m4704_MethodInfo,
	&JoinMatchResponse_get_usingRelay_m4705_MethodInfo,
	&JoinMatchResponse_set_usingRelay_m4706_MethodInfo,
	&JoinMatchResponse_ToString_m4707_MethodInfo,
	&JoinMatchResponse_Parse_m4708_MethodInfo,
	NULL
};
extern const MethodInfo JoinMatchResponse_get_address_m4695_MethodInfo;
extern const MethodInfo JoinMatchResponse_set_address_m4696_MethodInfo;
static const PropertyInfo JoinMatchResponse_t909____address_PropertyInfo = 
{
	&JoinMatchResponse_t909_il2cpp_TypeInfo/* parent */
	, "address"/* name */
	, &JoinMatchResponse_get_address_m4695_MethodInfo/* get */
	, &JoinMatchResponse_set_address_m4696_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo JoinMatchResponse_get_port_m4697_MethodInfo;
extern const MethodInfo JoinMatchResponse_set_port_m4698_MethodInfo;
static const PropertyInfo JoinMatchResponse_t909____port_PropertyInfo = 
{
	&JoinMatchResponse_t909_il2cpp_TypeInfo/* parent */
	, "port"/* name */
	, &JoinMatchResponse_get_port_m4697_MethodInfo/* get */
	, &JoinMatchResponse_set_port_m4698_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo JoinMatchResponse_get_networkId_m4699_MethodInfo;
extern const MethodInfo JoinMatchResponse_set_networkId_m4700_MethodInfo;
static const PropertyInfo JoinMatchResponse_t909____networkId_PropertyInfo = 
{
	&JoinMatchResponse_t909_il2cpp_TypeInfo/* parent */
	, "networkId"/* name */
	, &JoinMatchResponse_get_networkId_m4699_MethodInfo/* get */
	, &JoinMatchResponse_set_networkId_m4700_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo JoinMatchResponse_get_accessTokenString_m4701_MethodInfo;
extern const MethodInfo JoinMatchResponse_set_accessTokenString_m4702_MethodInfo;
static const PropertyInfo JoinMatchResponse_t909____accessTokenString_PropertyInfo = 
{
	&JoinMatchResponse_t909_il2cpp_TypeInfo/* parent */
	, "accessTokenString"/* name */
	, &JoinMatchResponse_get_accessTokenString_m4701_MethodInfo/* get */
	, &JoinMatchResponse_set_accessTokenString_m4702_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo JoinMatchResponse_get_nodeId_m4703_MethodInfo;
extern const MethodInfo JoinMatchResponse_set_nodeId_m4704_MethodInfo;
static const PropertyInfo JoinMatchResponse_t909____nodeId_PropertyInfo = 
{
	&JoinMatchResponse_t909_il2cpp_TypeInfo/* parent */
	, "nodeId"/* name */
	, &JoinMatchResponse_get_nodeId_m4703_MethodInfo/* get */
	, &JoinMatchResponse_set_nodeId_m4704_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo JoinMatchResponse_get_usingRelay_m4705_MethodInfo;
extern const MethodInfo JoinMatchResponse_set_usingRelay_m4706_MethodInfo;
static const PropertyInfo JoinMatchResponse_t909____usingRelay_PropertyInfo = 
{
	&JoinMatchResponse_t909_il2cpp_TypeInfo/* parent */
	, "usingRelay"/* name */
	, &JoinMatchResponse_get_usingRelay_m4705_MethodInfo/* get */
	, &JoinMatchResponse_set_usingRelay_m4706_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* JoinMatchResponse_t909_PropertyInfos[] =
{
	&JoinMatchResponse_t909____address_PropertyInfo,
	&JoinMatchResponse_t909____port_PropertyInfo,
	&JoinMatchResponse_t909____networkId_PropertyInfo,
	&JoinMatchResponse_t909____accessTokenString_PropertyInfo,
	&JoinMatchResponse_t909____nodeId_PropertyInfo,
	&JoinMatchResponse_t909____usingRelay_PropertyInfo,
	NULL
};
extern const MethodInfo JoinMatchResponse_ToString_m4707_MethodInfo;
extern const MethodInfo JoinMatchResponse_Parse_m4708_MethodInfo;
static const Il2CppMethodReference JoinMatchResponse_t909_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&JoinMatchResponse_ToString_m4707_MethodInfo,
	&JoinMatchResponse_Parse_m4708_MethodInfo,
};
static bool JoinMatchResponse_t909_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair JoinMatchResponse_t909_InterfacesOffsets[] = 
{
	{ &IResponse_t1115_0_0_0, 5},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType JoinMatchResponse_t909_0_0_0;
extern const Il2CppType JoinMatchResponse_t909_1_0_0;
struct JoinMatchResponse_t909;
const Il2CppTypeDefinitionMetadata JoinMatchResponse_t909_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, JoinMatchResponse_t909_InterfacesOffsets/* interfaceOffsets */
	, &BasicResponse_t904_0_0_0/* parent */
	, JoinMatchResponse_t909_VTable/* vtableMethods */
	, JoinMatchResponse_t909_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1045/* fieldStart */

};
TypeInfo JoinMatchResponse_t909_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "JoinMatchResponse"/* name */
	, "UnityEngine.Networking.Match"/* namespaze */
	, JoinMatchResponse_t909_MethodInfos/* methods */
	, JoinMatchResponse_t909_PropertyInfos/* properties */
	, NULL/* events */
	, &JoinMatchResponse_t909_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &JoinMatchResponse_t909_0_0_0/* byval_arg */
	, &JoinMatchResponse_t909_1_0_0/* this_arg */
	, &JoinMatchResponse_t909_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (JoinMatchResponse_t909)/* instance_size */
	, sizeof (JoinMatchResponse_t909)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 15/* method_count */
	, 6/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.Networking.Match.DestroyMatchRequest
#include "UnityEngine_UnityEngine_Networking_Match_DestroyMatchRequest.h"
// Metadata Definition UnityEngine.Networking.Match.DestroyMatchRequest
extern TypeInfo DestroyMatchRequest_t910_il2cpp_TypeInfo;
// UnityEngine.Networking.Match.DestroyMatchRequest
#include "UnityEngine_UnityEngine_Networking_Match_DestroyMatchRequestMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Networking.Match.DestroyMatchRequest::.ctor()
extern const MethodInfo DestroyMatchRequest__ctor_m4709_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DestroyMatchRequest__ctor_m4709/* method */
	, &DestroyMatchRequest_t910_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1813/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_NetworkID_t920 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Networking.Types.NetworkID UnityEngine.Networking.Match.DestroyMatchRequest::get_networkId()
extern const MethodInfo DestroyMatchRequest_get_networkId_m4710_MethodInfo = 
{
	"get_networkId"/* name */
	, (methodPointerType)&DestroyMatchRequest_get_networkId_m4710/* method */
	, &DestroyMatchRequest_t910_il2cpp_TypeInfo/* declaring_type */
	, &NetworkID_t920_0_0_0/* return_type */
	, RuntimeInvoker_NetworkID_t920/* invoker_method */
	, NULL/* parameters */
	, 910/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1814/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType NetworkID_t920_0_0_0;
static const ParameterInfo DestroyMatchRequest_t910_DestroyMatchRequest_set_networkId_m4711_ParameterInfos[] = 
{
	{"value", 0, 134219894, 0, &NetworkID_t920_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_UInt64_t1074 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Networking.Match.DestroyMatchRequest::set_networkId(UnityEngine.Networking.Types.NetworkID)
extern const MethodInfo DestroyMatchRequest_set_networkId_m4711_MethodInfo = 
{
	"set_networkId"/* name */
	, (methodPointerType)&DestroyMatchRequest_set_networkId_m4711/* method */
	, &DestroyMatchRequest_t910_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_UInt64_t1074/* invoker_method */
	, DestroyMatchRequest_t910_DestroyMatchRequest_set_networkId_m4711_ParameterInfos/* parameters */
	, 911/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1815/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Networking.Match.DestroyMatchRequest::ToString()
extern const MethodInfo DestroyMatchRequest_ToString_m4712_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&DestroyMatchRequest_ToString_m4712/* method */
	, &DestroyMatchRequest_t910_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1816/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* DestroyMatchRequest_t910_MethodInfos[] =
{
	&DestroyMatchRequest__ctor_m4709_MethodInfo,
	&DestroyMatchRequest_get_networkId_m4710_MethodInfo,
	&DestroyMatchRequest_set_networkId_m4711_MethodInfo,
	&DestroyMatchRequest_ToString_m4712_MethodInfo,
	NULL
};
extern const MethodInfo DestroyMatchRequest_get_networkId_m4710_MethodInfo;
extern const MethodInfo DestroyMatchRequest_set_networkId_m4711_MethodInfo;
static const PropertyInfo DestroyMatchRequest_t910____networkId_PropertyInfo = 
{
	&DestroyMatchRequest_t910_il2cpp_TypeInfo/* parent */
	, "networkId"/* name */
	, &DestroyMatchRequest_get_networkId_m4710_MethodInfo/* get */
	, &DestroyMatchRequest_set_networkId_m4711_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* DestroyMatchRequest_t910_PropertyInfos[] =
{
	&DestroyMatchRequest_t910____networkId_PropertyInfo,
	NULL
};
extern const MethodInfo DestroyMatchRequest_ToString_m4712_MethodInfo;
static const Il2CppMethodReference DestroyMatchRequest_t910_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&DestroyMatchRequest_ToString_m4712_MethodInfo,
};
static bool DestroyMatchRequest_t910_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType DestroyMatchRequest_t910_0_0_0;
extern const Il2CppType DestroyMatchRequest_t910_1_0_0;
struct DestroyMatchRequest_t910;
const Il2CppTypeDefinitionMetadata DestroyMatchRequest_t910_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Request_t901_0_0_0/* parent */
	, DestroyMatchRequest_t910_VTable/* vtableMethods */
	, DestroyMatchRequest_t910_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1051/* fieldStart */

};
TypeInfo DestroyMatchRequest_t910_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "DestroyMatchRequest"/* name */
	, "UnityEngine.Networking.Match"/* namespaze */
	, DestroyMatchRequest_t910_MethodInfos/* methods */
	, DestroyMatchRequest_t910_PropertyInfos/* properties */
	, NULL/* events */
	, &DestroyMatchRequest_t910_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DestroyMatchRequest_t910_0_0_0/* byval_arg */
	, &DestroyMatchRequest_t910_1_0_0/* this_arg */
	, &DestroyMatchRequest_t910_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DestroyMatchRequest_t910)/* instance_size */
	, sizeof (DestroyMatchRequest_t910)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Networking.Match.DropConnectionRequest
#include "UnityEngine_UnityEngine_Networking_Match_DropConnectionReque.h"
// Metadata Definition UnityEngine.Networking.Match.DropConnectionRequest
extern TypeInfo DropConnectionRequest_t911_il2cpp_TypeInfo;
// UnityEngine.Networking.Match.DropConnectionRequest
#include "UnityEngine_UnityEngine_Networking_Match_DropConnectionRequeMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Networking.Match.DropConnectionRequest::.ctor()
extern const MethodInfo DropConnectionRequest__ctor_m4713_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DropConnectionRequest__ctor_m4713/* method */
	, &DropConnectionRequest_t911_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1817/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_NetworkID_t920 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Networking.Types.NetworkID UnityEngine.Networking.Match.DropConnectionRequest::get_networkId()
extern const MethodInfo DropConnectionRequest_get_networkId_m4714_MethodInfo = 
{
	"get_networkId"/* name */
	, (methodPointerType)&DropConnectionRequest_get_networkId_m4714/* method */
	, &DropConnectionRequest_t911_il2cpp_TypeInfo/* declaring_type */
	, &NetworkID_t920_0_0_0/* return_type */
	, RuntimeInvoker_NetworkID_t920/* invoker_method */
	, NULL/* parameters */
	, 914/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1818/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType NetworkID_t920_0_0_0;
static const ParameterInfo DropConnectionRequest_t911_DropConnectionRequest_set_networkId_m4715_ParameterInfos[] = 
{
	{"value", 0, 134219895, 0, &NetworkID_t920_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_UInt64_t1074 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Networking.Match.DropConnectionRequest::set_networkId(UnityEngine.Networking.Types.NetworkID)
extern const MethodInfo DropConnectionRequest_set_networkId_m4715_MethodInfo = 
{
	"set_networkId"/* name */
	, (methodPointerType)&DropConnectionRequest_set_networkId_m4715/* method */
	, &DropConnectionRequest_t911_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_UInt64_t1074/* invoker_method */
	, DropConnectionRequest_t911_DropConnectionRequest_set_networkId_m4715_ParameterInfos/* parameters */
	, 915/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1819/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_NodeID_t921 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Networking.Types.NodeID UnityEngine.Networking.Match.DropConnectionRequest::get_nodeId()
extern const MethodInfo DropConnectionRequest_get_nodeId_m4716_MethodInfo = 
{
	"get_nodeId"/* name */
	, (methodPointerType)&DropConnectionRequest_get_nodeId_m4716/* method */
	, &DropConnectionRequest_t911_il2cpp_TypeInfo/* declaring_type */
	, &NodeID_t921_0_0_0/* return_type */
	, RuntimeInvoker_NodeID_t921/* invoker_method */
	, NULL/* parameters */
	, 916/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1820/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType NodeID_t921_0_0_0;
static const ParameterInfo DropConnectionRequest_t911_DropConnectionRequest_set_nodeId_m4717_ParameterInfos[] = 
{
	{"value", 0, 134219896, 0, &NodeID_t921_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_UInt16_t684 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Networking.Match.DropConnectionRequest::set_nodeId(UnityEngine.Networking.Types.NodeID)
extern const MethodInfo DropConnectionRequest_set_nodeId_m4717_MethodInfo = 
{
	"set_nodeId"/* name */
	, (methodPointerType)&DropConnectionRequest_set_nodeId_m4717/* method */
	, &DropConnectionRequest_t911_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_UInt16_t684/* invoker_method */
	, DropConnectionRequest_t911_DropConnectionRequest_set_nodeId_m4717_ParameterInfos/* parameters */
	, 917/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1821/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Networking.Match.DropConnectionRequest::ToString()
extern const MethodInfo DropConnectionRequest_ToString_m4718_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&DropConnectionRequest_ToString_m4718/* method */
	, &DropConnectionRequest_t911_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1822/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* DropConnectionRequest_t911_MethodInfos[] =
{
	&DropConnectionRequest__ctor_m4713_MethodInfo,
	&DropConnectionRequest_get_networkId_m4714_MethodInfo,
	&DropConnectionRequest_set_networkId_m4715_MethodInfo,
	&DropConnectionRequest_get_nodeId_m4716_MethodInfo,
	&DropConnectionRequest_set_nodeId_m4717_MethodInfo,
	&DropConnectionRequest_ToString_m4718_MethodInfo,
	NULL
};
extern const MethodInfo DropConnectionRequest_get_networkId_m4714_MethodInfo;
extern const MethodInfo DropConnectionRequest_set_networkId_m4715_MethodInfo;
static const PropertyInfo DropConnectionRequest_t911____networkId_PropertyInfo = 
{
	&DropConnectionRequest_t911_il2cpp_TypeInfo/* parent */
	, "networkId"/* name */
	, &DropConnectionRequest_get_networkId_m4714_MethodInfo/* get */
	, &DropConnectionRequest_set_networkId_m4715_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo DropConnectionRequest_get_nodeId_m4716_MethodInfo;
extern const MethodInfo DropConnectionRequest_set_nodeId_m4717_MethodInfo;
static const PropertyInfo DropConnectionRequest_t911____nodeId_PropertyInfo = 
{
	&DropConnectionRequest_t911_il2cpp_TypeInfo/* parent */
	, "nodeId"/* name */
	, &DropConnectionRequest_get_nodeId_m4716_MethodInfo/* get */
	, &DropConnectionRequest_set_nodeId_m4717_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* DropConnectionRequest_t911_PropertyInfos[] =
{
	&DropConnectionRequest_t911____networkId_PropertyInfo,
	&DropConnectionRequest_t911____nodeId_PropertyInfo,
	NULL
};
extern const MethodInfo DropConnectionRequest_ToString_m4718_MethodInfo;
static const Il2CppMethodReference DropConnectionRequest_t911_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&DropConnectionRequest_ToString_m4718_MethodInfo,
};
static bool DropConnectionRequest_t911_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType DropConnectionRequest_t911_0_0_0;
extern const Il2CppType DropConnectionRequest_t911_1_0_0;
struct DropConnectionRequest_t911;
const Il2CppTypeDefinitionMetadata DropConnectionRequest_t911_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Request_t901_0_0_0/* parent */
	, DropConnectionRequest_t911_VTable/* vtableMethods */
	, DropConnectionRequest_t911_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1052/* fieldStart */

};
TypeInfo DropConnectionRequest_t911_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "DropConnectionRequest"/* name */
	, "UnityEngine.Networking.Match"/* namespaze */
	, DropConnectionRequest_t911_MethodInfos/* methods */
	, DropConnectionRequest_t911_PropertyInfos/* properties */
	, NULL/* events */
	, &DropConnectionRequest_t911_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DropConnectionRequest_t911_0_0_0/* byval_arg */
	, &DropConnectionRequest_t911_1_0_0/* this_arg */
	, &DropConnectionRequest_t911_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DropConnectionRequest_t911)/* instance_size */
	, sizeof (DropConnectionRequest_t911)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Networking.Match.ListMatchRequest
#include "UnityEngine_UnityEngine_Networking_Match_ListMatchRequest.h"
// Metadata Definition UnityEngine.Networking.Match.ListMatchRequest
extern TypeInfo ListMatchRequest_t912_il2cpp_TypeInfo;
// UnityEngine.Networking.Match.ListMatchRequest
#include "UnityEngine_UnityEngine_Networking_Match_ListMatchRequestMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Networking.Match.ListMatchRequest::.ctor()
extern const MethodInfo ListMatchRequest__ctor_m4719_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ListMatchRequest__ctor_m4719/* method */
	, &ListMatchRequest_t912_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1823/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.Networking.Match.ListMatchRequest::get_pageSize()
extern const MethodInfo ListMatchRequest_get_pageSize_m4720_MethodInfo = 
{
	"get_pageSize"/* name */
	, (methodPointerType)&ListMatchRequest_get_pageSize_m4720/* method */
	, &ListMatchRequest_t912_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 923/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1824/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo ListMatchRequest_t912_ListMatchRequest_set_pageSize_m4721_ParameterInfos[] = 
{
	{"value", 0, 134219897, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Networking.Match.ListMatchRequest::set_pageSize(System.Int32)
extern const MethodInfo ListMatchRequest_set_pageSize_m4721_MethodInfo = 
{
	"set_pageSize"/* name */
	, (methodPointerType)&ListMatchRequest_set_pageSize_m4721/* method */
	, &ListMatchRequest_t912_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, ListMatchRequest_t912_ListMatchRequest_set_pageSize_m4721_ParameterInfos/* parameters */
	, 924/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1825/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.Networking.Match.ListMatchRequest::get_pageNum()
extern const MethodInfo ListMatchRequest_get_pageNum_m4722_MethodInfo = 
{
	"get_pageNum"/* name */
	, (methodPointerType)&ListMatchRequest_get_pageNum_m4722/* method */
	, &ListMatchRequest_t912_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 925/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1826/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo ListMatchRequest_t912_ListMatchRequest_set_pageNum_m4723_ParameterInfos[] = 
{
	{"value", 0, 134219898, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Networking.Match.ListMatchRequest::set_pageNum(System.Int32)
extern const MethodInfo ListMatchRequest_set_pageNum_m4723_MethodInfo = 
{
	"set_pageNum"/* name */
	, (methodPointerType)&ListMatchRequest_set_pageNum_m4723/* method */
	, &ListMatchRequest_t912_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, ListMatchRequest_t912_ListMatchRequest_set_pageNum_m4723_ParameterInfos/* parameters */
	, 926/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1827/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Networking.Match.ListMatchRequest::get_nameFilter()
extern const MethodInfo ListMatchRequest_get_nameFilter_m4724_MethodInfo = 
{
	"get_nameFilter"/* name */
	, (methodPointerType)&ListMatchRequest_get_nameFilter_m4724/* method */
	, &ListMatchRequest_t912_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 927/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1828/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo ListMatchRequest_t912_ListMatchRequest_set_nameFilter_m4725_ParameterInfos[] = 
{
	{"value", 0, 134219899, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Networking.Match.ListMatchRequest::set_nameFilter(System.String)
extern const MethodInfo ListMatchRequest_set_nameFilter_m4725_MethodInfo = 
{
	"set_nameFilter"/* name */
	, (methodPointerType)&ListMatchRequest_set_nameFilter_m4725/* method */
	, &ListMatchRequest_t912_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, ListMatchRequest_t912_ListMatchRequest_set_nameFilter_m4725_ParameterInfos/* parameters */
	, 928/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1829/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.Generic.Dictionary`2<System.String,System.Int64> UnityEngine.Networking.Match.ListMatchRequest::get_matchAttributeFilterLessThan()
extern const MethodInfo ListMatchRequest_get_matchAttributeFilterLessThan_m4726_MethodInfo = 
{
	"get_matchAttributeFilterLessThan"/* name */
	, (methodPointerType)&ListMatchRequest_get_matchAttributeFilterLessThan_m4726/* method */
	, &ListMatchRequest_t912_il2cpp_TypeInfo/* declaring_type */
	, &Dictionary_2_t905_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 929/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1830/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.Generic.Dictionary`2<System.String,System.Int64> UnityEngine.Networking.Match.ListMatchRequest::get_matchAttributeFilterGreaterThan()
extern const MethodInfo ListMatchRequest_get_matchAttributeFilterGreaterThan_m4727_MethodInfo = 
{
	"get_matchAttributeFilterGreaterThan"/* name */
	, (methodPointerType)&ListMatchRequest_get_matchAttributeFilterGreaterThan_m4727/* method */
	, &ListMatchRequest_t912_il2cpp_TypeInfo/* declaring_type */
	, &Dictionary_2_t905_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 930/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1831/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Networking.Match.ListMatchRequest::ToString()
extern const MethodInfo ListMatchRequest_ToString_m4728_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&ListMatchRequest_ToString_m4728/* method */
	, &ListMatchRequest_t912_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1832/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ListMatchRequest_t912_MethodInfos[] =
{
	&ListMatchRequest__ctor_m4719_MethodInfo,
	&ListMatchRequest_get_pageSize_m4720_MethodInfo,
	&ListMatchRequest_set_pageSize_m4721_MethodInfo,
	&ListMatchRequest_get_pageNum_m4722_MethodInfo,
	&ListMatchRequest_set_pageNum_m4723_MethodInfo,
	&ListMatchRequest_get_nameFilter_m4724_MethodInfo,
	&ListMatchRequest_set_nameFilter_m4725_MethodInfo,
	&ListMatchRequest_get_matchAttributeFilterLessThan_m4726_MethodInfo,
	&ListMatchRequest_get_matchAttributeFilterGreaterThan_m4727_MethodInfo,
	&ListMatchRequest_ToString_m4728_MethodInfo,
	NULL
};
extern const MethodInfo ListMatchRequest_get_pageSize_m4720_MethodInfo;
extern const MethodInfo ListMatchRequest_set_pageSize_m4721_MethodInfo;
static const PropertyInfo ListMatchRequest_t912____pageSize_PropertyInfo = 
{
	&ListMatchRequest_t912_il2cpp_TypeInfo/* parent */
	, "pageSize"/* name */
	, &ListMatchRequest_get_pageSize_m4720_MethodInfo/* get */
	, &ListMatchRequest_set_pageSize_m4721_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ListMatchRequest_get_pageNum_m4722_MethodInfo;
extern const MethodInfo ListMatchRequest_set_pageNum_m4723_MethodInfo;
static const PropertyInfo ListMatchRequest_t912____pageNum_PropertyInfo = 
{
	&ListMatchRequest_t912_il2cpp_TypeInfo/* parent */
	, "pageNum"/* name */
	, &ListMatchRequest_get_pageNum_m4722_MethodInfo/* get */
	, &ListMatchRequest_set_pageNum_m4723_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ListMatchRequest_get_nameFilter_m4724_MethodInfo;
extern const MethodInfo ListMatchRequest_set_nameFilter_m4725_MethodInfo;
static const PropertyInfo ListMatchRequest_t912____nameFilter_PropertyInfo = 
{
	&ListMatchRequest_t912_il2cpp_TypeInfo/* parent */
	, "nameFilter"/* name */
	, &ListMatchRequest_get_nameFilter_m4724_MethodInfo/* get */
	, &ListMatchRequest_set_nameFilter_m4725_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ListMatchRequest_get_matchAttributeFilterLessThan_m4726_MethodInfo;
static const PropertyInfo ListMatchRequest_t912____matchAttributeFilterLessThan_PropertyInfo = 
{
	&ListMatchRequest_t912_il2cpp_TypeInfo/* parent */
	, "matchAttributeFilterLessThan"/* name */
	, &ListMatchRequest_get_matchAttributeFilterLessThan_m4726_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ListMatchRequest_get_matchAttributeFilterGreaterThan_m4727_MethodInfo;
static const PropertyInfo ListMatchRequest_t912____matchAttributeFilterGreaterThan_PropertyInfo = 
{
	&ListMatchRequest_t912_il2cpp_TypeInfo/* parent */
	, "matchAttributeFilterGreaterThan"/* name */
	, &ListMatchRequest_get_matchAttributeFilterGreaterThan_m4727_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ListMatchRequest_t912_PropertyInfos[] =
{
	&ListMatchRequest_t912____pageSize_PropertyInfo,
	&ListMatchRequest_t912____pageNum_PropertyInfo,
	&ListMatchRequest_t912____nameFilter_PropertyInfo,
	&ListMatchRequest_t912____matchAttributeFilterLessThan_PropertyInfo,
	&ListMatchRequest_t912____matchAttributeFilterGreaterThan_PropertyInfo,
	NULL
};
extern const MethodInfo ListMatchRequest_ToString_m4728_MethodInfo;
static const Il2CppMethodReference ListMatchRequest_t912_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&ListMatchRequest_ToString_m4728_MethodInfo,
};
static bool ListMatchRequest_t912_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ListMatchRequest_t912_0_0_0;
extern const Il2CppType ListMatchRequest_t912_1_0_0;
struct ListMatchRequest_t912;
const Il2CppTypeDefinitionMetadata ListMatchRequest_t912_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Request_t901_0_0_0/* parent */
	, ListMatchRequest_t912_VTable/* vtableMethods */
	, ListMatchRequest_t912_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1054/* fieldStart */

};
TypeInfo ListMatchRequest_t912_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ListMatchRequest"/* name */
	, "UnityEngine.Networking.Match"/* namespaze */
	, ListMatchRequest_t912_MethodInfos/* methods */
	, ListMatchRequest_t912_PropertyInfos/* properties */
	, NULL/* events */
	, &ListMatchRequest_t912_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ListMatchRequest_t912_0_0_0/* byval_arg */
	, &ListMatchRequest_t912_1_0_0/* this_arg */
	, &ListMatchRequest_t912_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ListMatchRequest_t912)/* instance_size */
	, sizeof (ListMatchRequest_t912)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 5/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Networking.Match.MatchDirectConnectInfo
#include "UnityEngine_UnityEngine_Networking_Match_MatchDirectConnectI.h"
// Metadata Definition UnityEngine.Networking.Match.MatchDirectConnectInfo
extern TypeInfo MatchDirectConnectInfo_t913_il2cpp_TypeInfo;
// UnityEngine.Networking.Match.MatchDirectConnectInfo
#include "UnityEngine_UnityEngine_Networking_Match_MatchDirectConnectIMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Networking.Match.MatchDirectConnectInfo::.ctor()
extern const MethodInfo MatchDirectConnectInfo__ctor_m4729_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MatchDirectConnectInfo__ctor_m4729/* method */
	, &MatchDirectConnectInfo_t913_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1833/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_NodeID_t921 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Networking.Types.NodeID UnityEngine.Networking.Match.MatchDirectConnectInfo::get_nodeId()
extern const MethodInfo MatchDirectConnectInfo_get_nodeId_m4730_MethodInfo = 
{
	"get_nodeId"/* name */
	, (methodPointerType)&MatchDirectConnectInfo_get_nodeId_m4730/* method */
	, &MatchDirectConnectInfo_t913_il2cpp_TypeInfo/* declaring_type */
	, &NodeID_t921_0_0_0/* return_type */
	, RuntimeInvoker_NodeID_t921/* invoker_method */
	, NULL/* parameters */
	, 934/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1834/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType NodeID_t921_0_0_0;
static const ParameterInfo MatchDirectConnectInfo_t913_MatchDirectConnectInfo_set_nodeId_m4731_ParameterInfos[] = 
{
	{"value", 0, 134219900, 0, &NodeID_t921_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_UInt16_t684 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Networking.Match.MatchDirectConnectInfo::set_nodeId(UnityEngine.Networking.Types.NodeID)
extern const MethodInfo MatchDirectConnectInfo_set_nodeId_m4731_MethodInfo = 
{
	"set_nodeId"/* name */
	, (methodPointerType)&MatchDirectConnectInfo_set_nodeId_m4731/* method */
	, &MatchDirectConnectInfo_t913_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_UInt16_t684/* invoker_method */
	, MatchDirectConnectInfo_t913_MatchDirectConnectInfo_set_nodeId_m4731_ParameterInfos/* parameters */
	, 935/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1835/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Networking.Match.MatchDirectConnectInfo::get_publicAddress()
extern const MethodInfo MatchDirectConnectInfo_get_publicAddress_m4732_MethodInfo = 
{
	"get_publicAddress"/* name */
	, (methodPointerType)&MatchDirectConnectInfo_get_publicAddress_m4732/* method */
	, &MatchDirectConnectInfo_t913_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 936/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1836/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo MatchDirectConnectInfo_t913_MatchDirectConnectInfo_set_publicAddress_m4733_ParameterInfos[] = 
{
	{"value", 0, 134219901, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Networking.Match.MatchDirectConnectInfo::set_publicAddress(System.String)
extern const MethodInfo MatchDirectConnectInfo_set_publicAddress_m4733_MethodInfo = 
{
	"set_publicAddress"/* name */
	, (methodPointerType)&MatchDirectConnectInfo_set_publicAddress_m4733/* method */
	, &MatchDirectConnectInfo_t913_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, MatchDirectConnectInfo_t913_MatchDirectConnectInfo_set_publicAddress_m4733_ParameterInfos/* parameters */
	, 937/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1837/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Networking.Match.MatchDirectConnectInfo::get_privateAddress()
extern const MethodInfo MatchDirectConnectInfo_get_privateAddress_m4734_MethodInfo = 
{
	"get_privateAddress"/* name */
	, (methodPointerType)&MatchDirectConnectInfo_get_privateAddress_m4734/* method */
	, &MatchDirectConnectInfo_t913_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 938/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1838/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo MatchDirectConnectInfo_t913_MatchDirectConnectInfo_set_privateAddress_m4735_ParameterInfos[] = 
{
	{"value", 0, 134219902, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Networking.Match.MatchDirectConnectInfo::set_privateAddress(System.String)
extern const MethodInfo MatchDirectConnectInfo_set_privateAddress_m4735_MethodInfo = 
{
	"set_privateAddress"/* name */
	, (methodPointerType)&MatchDirectConnectInfo_set_privateAddress_m4735/* method */
	, &MatchDirectConnectInfo_t913_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, MatchDirectConnectInfo_t913_MatchDirectConnectInfo_set_privateAddress_m4735_ParameterInfos/* parameters */
	, 939/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1839/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Networking.Match.MatchDirectConnectInfo::ToString()
extern const MethodInfo MatchDirectConnectInfo_ToString_m4736_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&MatchDirectConnectInfo_ToString_m4736/* method */
	, &MatchDirectConnectInfo_t913_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1840/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo MatchDirectConnectInfo_t913_MatchDirectConnectInfo_Parse_m4737_ParameterInfos[] = 
{
	{"obj", 0, 134219903, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Networking.Match.MatchDirectConnectInfo::Parse(System.Object)
extern const MethodInfo MatchDirectConnectInfo_Parse_m4737_MethodInfo = 
{
	"Parse"/* name */
	, (methodPointerType)&MatchDirectConnectInfo_Parse_m4737/* method */
	, &MatchDirectConnectInfo_t913_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, MatchDirectConnectInfo_t913_MatchDirectConnectInfo_Parse_m4737_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1841/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MatchDirectConnectInfo_t913_MethodInfos[] =
{
	&MatchDirectConnectInfo__ctor_m4729_MethodInfo,
	&MatchDirectConnectInfo_get_nodeId_m4730_MethodInfo,
	&MatchDirectConnectInfo_set_nodeId_m4731_MethodInfo,
	&MatchDirectConnectInfo_get_publicAddress_m4732_MethodInfo,
	&MatchDirectConnectInfo_set_publicAddress_m4733_MethodInfo,
	&MatchDirectConnectInfo_get_privateAddress_m4734_MethodInfo,
	&MatchDirectConnectInfo_set_privateAddress_m4735_MethodInfo,
	&MatchDirectConnectInfo_ToString_m4736_MethodInfo,
	&MatchDirectConnectInfo_Parse_m4737_MethodInfo,
	NULL
};
extern const MethodInfo MatchDirectConnectInfo_get_nodeId_m4730_MethodInfo;
extern const MethodInfo MatchDirectConnectInfo_set_nodeId_m4731_MethodInfo;
static const PropertyInfo MatchDirectConnectInfo_t913____nodeId_PropertyInfo = 
{
	&MatchDirectConnectInfo_t913_il2cpp_TypeInfo/* parent */
	, "nodeId"/* name */
	, &MatchDirectConnectInfo_get_nodeId_m4730_MethodInfo/* get */
	, &MatchDirectConnectInfo_set_nodeId_m4731_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MatchDirectConnectInfo_get_publicAddress_m4732_MethodInfo;
extern const MethodInfo MatchDirectConnectInfo_set_publicAddress_m4733_MethodInfo;
static const PropertyInfo MatchDirectConnectInfo_t913____publicAddress_PropertyInfo = 
{
	&MatchDirectConnectInfo_t913_il2cpp_TypeInfo/* parent */
	, "publicAddress"/* name */
	, &MatchDirectConnectInfo_get_publicAddress_m4732_MethodInfo/* get */
	, &MatchDirectConnectInfo_set_publicAddress_m4733_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MatchDirectConnectInfo_get_privateAddress_m4734_MethodInfo;
extern const MethodInfo MatchDirectConnectInfo_set_privateAddress_m4735_MethodInfo;
static const PropertyInfo MatchDirectConnectInfo_t913____privateAddress_PropertyInfo = 
{
	&MatchDirectConnectInfo_t913_il2cpp_TypeInfo/* parent */
	, "privateAddress"/* name */
	, &MatchDirectConnectInfo_get_privateAddress_m4734_MethodInfo/* get */
	, &MatchDirectConnectInfo_set_privateAddress_m4735_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* MatchDirectConnectInfo_t913_PropertyInfos[] =
{
	&MatchDirectConnectInfo_t913____nodeId_PropertyInfo,
	&MatchDirectConnectInfo_t913____publicAddress_PropertyInfo,
	&MatchDirectConnectInfo_t913____privateAddress_PropertyInfo,
	NULL
};
extern const MethodInfo MatchDirectConnectInfo_ToString_m4736_MethodInfo;
extern const MethodInfo MatchDirectConnectInfo_Parse_m4737_MethodInfo;
static const Il2CppMethodReference MatchDirectConnectInfo_t913_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&MatchDirectConnectInfo_ToString_m4736_MethodInfo,
	&MatchDirectConnectInfo_Parse_m4737_MethodInfo,
};
static bool MatchDirectConnectInfo_t913_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType MatchDirectConnectInfo_t913_0_0_0;
extern const Il2CppType MatchDirectConnectInfo_t913_1_0_0;
struct MatchDirectConnectInfo_t913;
const Il2CppTypeDefinitionMetadata MatchDirectConnectInfo_t913_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ResponseBase_t902_0_0_0/* parent */
	, MatchDirectConnectInfo_t913_VTable/* vtableMethods */
	, MatchDirectConnectInfo_t913_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1059/* fieldStart */

};
TypeInfo MatchDirectConnectInfo_t913_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "MatchDirectConnectInfo"/* name */
	, "UnityEngine.Networking.Match"/* namespaze */
	, MatchDirectConnectInfo_t913_MethodInfos/* methods */
	, MatchDirectConnectInfo_t913_PropertyInfos/* properties */
	, NULL/* events */
	, &MatchDirectConnectInfo_t913_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MatchDirectConnectInfo_t913_0_0_0/* byval_arg */
	, &MatchDirectConnectInfo_t913_1_0_0/* this_arg */
	, &MatchDirectConnectInfo_t913_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MatchDirectConnectInfo_t913)/* instance_size */
	, sizeof (MatchDirectConnectInfo_t913)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 3/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Networking.Match.MatchDesc
#include "UnityEngine_UnityEngine_Networking_Match_MatchDesc.h"
// Metadata Definition UnityEngine.Networking.Match.MatchDesc
extern TypeInfo MatchDesc_t915_il2cpp_TypeInfo;
// UnityEngine.Networking.Match.MatchDesc
#include "UnityEngine_UnityEngine_Networking_Match_MatchDescMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Networking.Match.MatchDesc::.ctor()
extern const MethodInfo MatchDesc__ctor_m4738_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MatchDesc__ctor_m4738/* method */
	, &MatchDesc_t915_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1842/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_NetworkID_t920 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Networking.Types.NetworkID UnityEngine.Networking.Match.MatchDesc::get_networkId()
extern const MethodInfo MatchDesc_get_networkId_m4739_MethodInfo = 
{
	"get_networkId"/* name */
	, (methodPointerType)&MatchDesc_get_networkId_m4739/* method */
	, &MatchDesc_t915_il2cpp_TypeInfo/* declaring_type */
	, &NetworkID_t920_0_0_0/* return_type */
	, RuntimeInvoker_NetworkID_t920/* invoker_method */
	, NULL/* parameters */
	, 949/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1843/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType NetworkID_t920_0_0_0;
static const ParameterInfo MatchDesc_t915_MatchDesc_set_networkId_m4740_ParameterInfos[] = 
{
	{"value", 0, 134219904, 0, &NetworkID_t920_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_UInt64_t1074 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Networking.Match.MatchDesc::set_networkId(UnityEngine.Networking.Types.NetworkID)
extern const MethodInfo MatchDesc_set_networkId_m4740_MethodInfo = 
{
	"set_networkId"/* name */
	, (methodPointerType)&MatchDesc_set_networkId_m4740/* method */
	, &MatchDesc_t915_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_UInt64_t1074/* invoker_method */
	, MatchDesc_t915_MatchDesc_set_networkId_m4740_ParameterInfos/* parameters */
	, 950/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1844/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Networking.Match.MatchDesc::get_name()
extern const MethodInfo MatchDesc_get_name_m4741_MethodInfo = 
{
	"get_name"/* name */
	, (methodPointerType)&MatchDesc_get_name_m4741/* method */
	, &MatchDesc_t915_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 951/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1845/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo MatchDesc_t915_MatchDesc_set_name_m4742_ParameterInfos[] = 
{
	{"value", 0, 134219905, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Networking.Match.MatchDesc::set_name(System.String)
extern const MethodInfo MatchDesc_set_name_m4742_MethodInfo = 
{
	"set_name"/* name */
	, (methodPointerType)&MatchDesc_set_name_m4742/* method */
	, &MatchDesc_t915_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, MatchDesc_t915_MatchDesc_set_name_m4742_ParameterInfos/* parameters */
	, 952/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1846/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.Networking.Match.MatchDesc::get_averageEloScore()
extern const MethodInfo MatchDesc_get_averageEloScore_m4743_MethodInfo = 
{
	"get_averageEloScore"/* name */
	, (methodPointerType)&MatchDesc_get_averageEloScore_m4743/* method */
	, &MatchDesc_t915_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 953/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1847/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.Networking.Match.MatchDesc::get_maxSize()
extern const MethodInfo MatchDesc_get_maxSize_m4744_MethodInfo = 
{
	"get_maxSize"/* name */
	, (methodPointerType)&MatchDesc_get_maxSize_m4744/* method */
	, &MatchDesc_t915_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 954/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1848/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo MatchDesc_t915_MatchDesc_set_maxSize_m4745_ParameterInfos[] = 
{
	{"value", 0, 134219906, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Networking.Match.MatchDesc::set_maxSize(System.Int32)
extern const MethodInfo MatchDesc_set_maxSize_m4745_MethodInfo = 
{
	"set_maxSize"/* name */
	, (methodPointerType)&MatchDesc_set_maxSize_m4745/* method */
	, &MatchDesc_t915_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, MatchDesc_t915_MatchDesc_set_maxSize_m4745_ParameterInfos/* parameters */
	, 955/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1849/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.Networking.Match.MatchDesc::get_currentSize()
extern const MethodInfo MatchDesc_get_currentSize_m4746_MethodInfo = 
{
	"get_currentSize"/* name */
	, (methodPointerType)&MatchDesc_get_currentSize_m4746/* method */
	, &MatchDesc_t915_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 956/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1850/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo MatchDesc_t915_MatchDesc_set_currentSize_m4747_ParameterInfos[] = 
{
	{"value", 0, 134219907, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Networking.Match.MatchDesc::set_currentSize(System.Int32)
extern const MethodInfo MatchDesc_set_currentSize_m4747_MethodInfo = 
{
	"set_currentSize"/* name */
	, (methodPointerType)&MatchDesc_set_currentSize_m4747/* method */
	, &MatchDesc_t915_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, MatchDesc_t915_MatchDesc_set_currentSize_m4747_ParameterInfos/* parameters */
	, 957/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1851/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Networking.Match.MatchDesc::get_isPrivate()
extern const MethodInfo MatchDesc_get_isPrivate_m4748_MethodInfo = 
{
	"get_isPrivate"/* name */
	, (methodPointerType)&MatchDesc_get_isPrivate_m4748/* method */
	, &MatchDesc_t915_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 958/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1852/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo MatchDesc_t915_MatchDesc_set_isPrivate_m4749_ParameterInfos[] = 
{
	{"value", 0, 134219908, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Networking.Match.MatchDesc::set_isPrivate(System.Boolean)
extern const MethodInfo MatchDesc_set_isPrivate_m4749_MethodInfo = 
{
	"set_isPrivate"/* name */
	, (methodPointerType)&MatchDesc_set_isPrivate_m4749/* method */
	, &MatchDesc_t915_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274/* invoker_method */
	, MatchDesc_t915_MatchDesc_set_isPrivate_m4749_ParameterInfos/* parameters */
	, 959/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1853/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.Generic.Dictionary`2<System.String,System.Int64> UnityEngine.Networking.Match.MatchDesc::get_matchAttributes()
extern const MethodInfo MatchDesc_get_matchAttributes_m4750_MethodInfo = 
{
	"get_matchAttributes"/* name */
	, (methodPointerType)&MatchDesc_get_matchAttributes_m4750/* method */
	, &MatchDesc_t915_il2cpp_TypeInfo/* declaring_type */
	, &Dictionary_2_t905_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 960/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1854/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_NodeID_t921 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Networking.Types.NodeID UnityEngine.Networking.Match.MatchDesc::get_hostNodeId()
extern const MethodInfo MatchDesc_get_hostNodeId_m4751_MethodInfo = 
{
	"get_hostNodeId"/* name */
	, (methodPointerType)&MatchDesc_get_hostNodeId_m4751/* method */
	, &MatchDesc_t915_il2cpp_TypeInfo/* declaring_type */
	, &NodeID_t921_0_0_0/* return_type */
	, RuntimeInvoker_NodeID_t921/* invoker_method */
	, NULL/* parameters */
	, 961/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1855/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType List_1_t914_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo> UnityEngine.Networking.Match.MatchDesc::get_directConnectInfos()
extern const MethodInfo MatchDesc_get_directConnectInfos_m4752_MethodInfo = 
{
	"get_directConnectInfos"/* name */
	, (methodPointerType)&MatchDesc_get_directConnectInfos_m4752/* method */
	, &MatchDesc_t915_il2cpp_TypeInfo/* declaring_type */
	, &List_1_t914_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 962/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1856/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType List_1_t914_0_0_0;
static const ParameterInfo MatchDesc_t915_MatchDesc_set_directConnectInfos_m4753_ParameterInfos[] = 
{
	{"value", 0, 134219909, 0, &List_1_t914_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Networking.Match.MatchDesc::set_directConnectInfos(System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>)
extern const MethodInfo MatchDesc_set_directConnectInfos_m4753_MethodInfo = 
{
	"set_directConnectInfos"/* name */
	, (methodPointerType)&MatchDesc_set_directConnectInfos_m4753/* method */
	, &MatchDesc_t915_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, MatchDesc_t915_MatchDesc_set_directConnectInfos_m4753_ParameterInfos/* parameters */
	, 963/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1857/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Networking.Match.MatchDesc::ToString()
extern const MethodInfo MatchDesc_ToString_m4754_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&MatchDesc_ToString_m4754/* method */
	, &MatchDesc_t915_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1858/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo MatchDesc_t915_MatchDesc_Parse_m4755_ParameterInfos[] = 
{
	{"obj", 0, 134219910, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Networking.Match.MatchDesc::Parse(System.Object)
extern const MethodInfo MatchDesc_Parse_m4755_MethodInfo = 
{
	"Parse"/* name */
	, (methodPointerType)&MatchDesc_Parse_m4755/* method */
	, &MatchDesc_t915_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, MatchDesc_t915_MatchDesc_Parse_m4755_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1859/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MatchDesc_t915_MethodInfos[] =
{
	&MatchDesc__ctor_m4738_MethodInfo,
	&MatchDesc_get_networkId_m4739_MethodInfo,
	&MatchDesc_set_networkId_m4740_MethodInfo,
	&MatchDesc_get_name_m4741_MethodInfo,
	&MatchDesc_set_name_m4742_MethodInfo,
	&MatchDesc_get_averageEloScore_m4743_MethodInfo,
	&MatchDesc_get_maxSize_m4744_MethodInfo,
	&MatchDesc_set_maxSize_m4745_MethodInfo,
	&MatchDesc_get_currentSize_m4746_MethodInfo,
	&MatchDesc_set_currentSize_m4747_MethodInfo,
	&MatchDesc_get_isPrivate_m4748_MethodInfo,
	&MatchDesc_set_isPrivate_m4749_MethodInfo,
	&MatchDesc_get_matchAttributes_m4750_MethodInfo,
	&MatchDesc_get_hostNodeId_m4751_MethodInfo,
	&MatchDesc_get_directConnectInfos_m4752_MethodInfo,
	&MatchDesc_set_directConnectInfos_m4753_MethodInfo,
	&MatchDesc_ToString_m4754_MethodInfo,
	&MatchDesc_Parse_m4755_MethodInfo,
	NULL
};
extern const MethodInfo MatchDesc_get_networkId_m4739_MethodInfo;
extern const MethodInfo MatchDesc_set_networkId_m4740_MethodInfo;
static const PropertyInfo MatchDesc_t915____networkId_PropertyInfo = 
{
	&MatchDesc_t915_il2cpp_TypeInfo/* parent */
	, "networkId"/* name */
	, &MatchDesc_get_networkId_m4739_MethodInfo/* get */
	, &MatchDesc_set_networkId_m4740_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MatchDesc_get_name_m4741_MethodInfo;
extern const MethodInfo MatchDesc_set_name_m4742_MethodInfo;
static const PropertyInfo MatchDesc_t915____name_PropertyInfo = 
{
	&MatchDesc_t915_il2cpp_TypeInfo/* parent */
	, "name"/* name */
	, &MatchDesc_get_name_m4741_MethodInfo/* get */
	, &MatchDesc_set_name_m4742_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MatchDesc_get_averageEloScore_m4743_MethodInfo;
static const PropertyInfo MatchDesc_t915____averageEloScore_PropertyInfo = 
{
	&MatchDesc_t915_il2cpp_TypeInfo/* parent */
	, "averageEloScore"/* name */
	, &MatchDesc_get_averageEloScore_m4743_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MatchDesc_get_maxSize_m4744_MethodInfo;
extern const MethodInfo MatchDesc_set_maxSize_m4745_MethodInfo;
static const PropertyInfo MatchDesc_t915____maxSize_PropertyInfo = 
{
	&MatchDesc_t915_il2cpp_TypeInfo/* parent */
	, "maxSize"/* name */
	, &MatchDesc_get_maxSize_m4744_MethodInfo/* get */
	, &MatchDesc_set_maxSize_m4745_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MatchDesc_get_currentSize_m4746_MethodInfo;
extern const MethodInfo MatchDesc_set_currentSize_m4747_MethodInfo;
static const PropertyInfo MatchDesc_t915____currentSize_PropertyInfo = 
{
	&MatchDesc_t915_il2cpp_TypeInfo/* parent */
	, "currentSize"/* name */
	, &MatchDesc_get_currentSize_m4746_MethodInfo/* get */
	, &MatchDesc_set_currentSize_m4747_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MatchDesc_get_isPrivate_m4748_MethodInfo;
extern const MethodInfo MatchDesc_set_isPrivate_m4749_MethodInfo;
static const PropertyInfo MatchDesc_t915____isPrivate_PropertyInfo = 
{
	&MatchDesc_t915_il2cpp_TypeInfo/* parent */
	, "isPrivate"/* name */
	, &MatchDesc_get_isPrivate_m4748_MethodInfo/* get */
	, &MatchDesc_set_isPrivate_m4749_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MatchDesc_get_matchAttributes_m4750_MethodInfo;
static const PropertyInfo MatchDesc_t915____matchAttributes_PropertyInfo = 
{
	&MatchDesc_t915_il2cpp_TypeInfo/* parent */
	, "matchAttributes"/* name */
	, &MatchDesc_get_matchAttributes_m4750_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MatchDesc_get_hostNodeId_m4751_MethodInfo;
static const PropertyInfo MatchDesc_t915____hostNodeId_PropertyInfo = 
{
	&MatchDesc_t915_il2cpp_TypeInfo/* parent */
	, "hostNodeId"/* name */
	, &MatchDesc_get_hostNodeId_m4751_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MatchDesc_get_directConnectInfos_m4752_MethodInfo;
extern const MethodInfo MatchDesc_set_directConnectInfos_m4753_MethodInfo;
static const PropertyInfo MatchDesc_t915____directConnectInfos_PropertyInfo = 
{
	&MatchDesc_t915_il2cpp_TypeInfo/* parent */
	, "directConnectInfos"/* name */
	, &MatchDesc_get_directConnectInfos_m4752_MethodInfo/* get */
	, &MatchDesc_set_directConnectInfos_m4753_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* MatchDesc_t915_PropertyInfos[] =
{
	&MatchDesc_t915____networkId_PropertyInfo,
	&MatchDesc_t915____name_PropertyInfo,
	&MatchDesc_t915____averageEloScore_PropertyInfo,
	&MatchDesc_t915____maxSize_PropertyInfo,
	&MatchDesc_t915____currentSize_PropertyInfo,
	&MatchDesc_t915____isPrivate_PropertyInfo,
	&MatchDesc_t915____matchAttributes_PropertyInfo,
	&MatchDesc_t915____hostNodeId_PropertyInfo,
	&MatchDesc_t915____directConnectInfos_PropertyInfo,
	NULL
};
extern const MethodInfo MatchDesc_ToString_m4754_MethodInfo;
extern const MethodInfo MatchDesc_Parse_m4755_MethodInfo;
static const Il2CppMethodReference MatchDesc_t915_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&MatchDesc_ToString_m4754_MethodInfo,
	&MatchDesc_Parse_m4755_MethodInfo,
};
static bool MatchDesc_t915_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType MatchDesc_t915_0_0_0;
extern const Il2CppType MatchDesc_t915_1_0_0;
struct MatchDesc_t915;
const Il2CppTypeDefinitionMetadata MatchDesc_t915_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ResponseBase_t902_0_0_0/* parent */
	, MatchDesc_t915_VTable/* vtableMethods */
	, MatchDesc_t915_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1062/* fieldStart */

};
TypeInfo MatchDesc_t915_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "MatchDesc"/* name */
	, "UnityEngine.Networking.Match"/* namespaze */
	, MatchDesc_t915_MethodInfos/* methods */
	, MatchDesc_t915_PropertyInfos/* properties */
	, NULL/* events */
	, &MatchDesc_t915_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MatchDesc_t915_0_0_0/* byval_arg */
	, &MatchDesc_t915_1_0_0/* this_arg */
	, &MatchDesc_t915_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MatchDesc_t915)/* instance_size */
	, sizeof (MatchDesc_t915)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 18/* method_count */
	, 9/* property_count */
	, 9/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Networking.Match.ListMatchResponse
#include "UnityEngine_UnityEngine_Networking_Match_ListMatchResponse.h"
// Metadata Definition UnityEngine.Networking.Match.ListMatchResponse
extern TypeInfo ListMatchResponse_t917_il2cpp_TypeInfo;
// UnityEngine.Networking.Match.ListMatchResponse
#include "UnityEngine_UnityEngine_Networking_Match_ListMatchResponseMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Networking.Match.ListMatchResponse::.ctor()
extern const MethodInfo ListMatchResponse__ctor_m4756_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ListMatchResponse__ctor_m4756/* method */
	, &ListMatchResponse_t917_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1860/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType List_1_t916_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDesc> UnityEngine.Networking.Match.ListMatchResponse::get_matches()
extern const MethodInfo ListMatchResponse_get_matches_m4757_MethodInfo = 
{
	"get_matches"/* name */
	, (methodPointerType)&ListMatchResponse_get_matches_m4757/* method */
	, &ListMatchResponse_t917_il2cpp_TypeInfo/* declaring_type */
	, &List_1_t916_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 965/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1861/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType List_1_t916_0_0_0;
static const ParameterInfo ListMatchResponse_t917_ListMatchResponse_set_matches_m4758_ParameterInfos[] = 
{
	{"value", 0, 134219911, 0, &List_1_t916_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Networking.Match.ListMatchResponse::set_matches(System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDesc>)
extern const MethodInfo ListMatchResponse_set_matches_m4758_MethodInfo = 
{
	"set_matches"/* name */
	, (methodPointerType)&ListMatchResponse_set_matches_m4758/* method */
	, &ListMatchResponse_t917_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, ListMatchResponse_t917_ListMatchResponse_set_matches_m4758_ParameterInfos/* parameters */
	, 966/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1862/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Networking.Match.ListMatchResponse::ToString()
extern const MethodInfo ListMatchResponse_ToString_m4759_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&ListMatchResponse_ToString_m4759/* method */
	, &ListMatchResponse_t917_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1863/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo ListMatchResponse_t917_ListMatchResponse_Parse_m4760_ParameterInfos[] = 
{
	{"obj", 0, 134219912, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Networking.Match.ListMatchResponse::Parse(System.Object)
extern const MethodInfo ListMatchResponse_Parse_m4760_MethodInfo = 
{
	"Parse"/* name */
	, (methodPointerType)&ListMatchResponse_Parse_m4760/* method */
	, &ListMatchResponse_t917_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, ListMatchResponse_t917_ListMatchResponse_Parse_m4760_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1864/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ListMatchResponse_t917_MethodInfos[] =
{
	&ListMatchResponse__ctor_m4756_MethodInfo,
	&ListMatchResponse_get_matches_m4757_MethodInfo,
	&ListMatchResponse_set_matches_m4758_MethodInfo,
	&ListMatchResponse_ToString_m4759_MethodInfo,
	&ListMatchResponse_Parse_m4760_MethodInfo,
	NULL
};
extern const MethodInfo ListMatchResponse_get_matches_m4757_MethodInfo;
extern const MethodInfo ListMatchResponse_set_matches_m4758_MethodInfo;
static const PropertyInfo ListMatchResponse_t917____matches_PropertyInfo = 
{
	&ListMatchResponse_t917_il2cpp_TypeInfo/* parent */
	, "matches"/* name */
	, &ListMatchResponse_get_matches_m4757_MethodInfo/* get */
	, &ListMatchResponse_set_matches_m4758_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ListMatchResponse_t917_PropertyInfos[] =
{
	&ListMatchResponse_t917____matches_PropertyInfo,
	NULL
};
extern const MethodInfo ListMatchResponse_ToString_m4759_MethodInfo;
extern const MethodInfo ListMatchResponse_Parse_m4760_MethodInfo;
static const Il2CppMethodReference ListMatchResponse_t917_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&ListMatchResponse_ToString_m4759_MethodInfo,
	&ListMatchResponse_Parse_m4760_MethodInfo,
};
static bool ListMatchResponse_t917_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ListMatchResponse_t917_InterfacesOffsets[] = 
{
	{ &IResponse_t1115_0_0_0, 5},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ListMatchResponse_t917_0_0_0;
extern const Il2CppType ListMatchResponse_t917_1_0_0;
struct ListMatchResponse_t917;
const Il2CppTypeDefinitionMetadata ListMatchResponse_t917_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ListMatchResponse_t917_InterfacesOffsets/* interfaceOffsets */
	, &BasicResponse_t904_0_0_0/* parent */
	, ListMatchResponse_t917_VTable/* vtableMethods */
	, ListMatchResponse_t917_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1071/* fieldStart */

};
TypeInfo ListMatchResponse_t917_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ListMatchResponse"/* name */
	, "UnityEngine.Networking.Match"/* namespaze */
	, ListMatchResponse_t917_MethodInfos/* methods */
	, ListMatchResponse_t917_PropertyInfos/* properties */
	, NULL/* events */
	, &ListMatchResponse_t917_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ListMatchResponse_t917_0_0_0/* byval_arg */
	, &ListMatchResponse_t917_1_0_0/* this_arg */
	, &ListMatchResponse_t917_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ListMatchResponse_t917)/* instance_size */
	, sizeof (ListMatchResponse_t917)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.Networking.Types.AppID
#include "UnityEngine_UnityEngine_Networking_Types_AppID.h"
// Metadata Definition UnityEngine.Networking.Types.AppID
extern TypeInfo AppID_t918_il2cpp_TypeInfo;
// UnityEngine.Networking.Types.AppID
#include "UnityEngine_UnityEngine_Networking_Types_AppIDMethodDeclarations.h"
static const MethodInfo* AppID_t918_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference AppID_t918_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool AppID_t918_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair AppID_t918_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType AppID_t918_1_0_0;
// System.UInt64
#include "mscorlib_System_UInt64.h"
extern TypeInfo UInt64_t1074_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata AppID_t918_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AppID_t918_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, AppID_t918_VTable/* vtableMethods */
	, AppID_t918_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1072/* fieldStart */

};
TypeInfo AppID_t918_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "AppID"/* name */
	, "UnityEngine.Networking.Types"/* namespaze */
	, AppID_t918_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UInt64_t1074_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 967/* custom_attributes_cache */
	, &AppID_t918_0_0_0/* byval_arg */
	, &AppID_t918_1_0_0/* this_arg */
	, &AppID_t918_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AppID_t918)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (AppID_t918)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint64_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.Networking.Types.SourceID
#include "UnityEngine_UnityEngine_Networking_Types_SourceID.h"
// Metadata Definition UnityEngine.Networking.Types.SourceID
extern TypeInfo SourceID_t919_il2cpp_TypeInfo;
// UnityEngine.Networking.Types.SourceID
#include "UnityEngine_UnityEngine_Networking_Types_SourceIDMethodDeclarations.h"
static const MethodInfo* SourceID_t919_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference SourceID_t919_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool SourceID_t919_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair SourceID_t919_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType SourceID_t919_1_0_0;
const Il2CppTypeDefinitionMetadata SourceID_t919_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SourceID_t919_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, SourceID_t919_VTable/* vtableMethods */
	, SourceID_t919_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1074/* fieldStart */

};
TypeInfo SourceID_t919_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SourceID"/* name */
	, "UnityEngine.Networking.Types"/* namespaze */
	, SourceID_t919_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UInt64_t1074_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 968/* custom_attributes_cache */
	, &SourceID_t919_0_0_0/* byval_arg */
	, &SourceID_t919_1_0_0/* this_arg */
	, &SourceID_t919_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SourceID_t919)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (SourceID_t919)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint64_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.Networking.Types.NetworkID
#include "UnityEngine_UnityEngine_Networking_Types_NetworkID.h"
// Metadata Definition UnityEngine.Networking.Types.NetworkID
extern TypeInfo NetworkID_t920_il2cpp_TypeInfo;
// UnityEngine.Networking.Types.NetworkID
#include "UnityEngine_UnityEngine_Networking_Types_NetworkIDMethodDeclarations.h"
static const MethodInfo* NetworkID_t920_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference NetworkID_t920_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool NetworkID_t920_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair NetworkID_t920_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType NetworkID_t920_1_0_0;
const Il2CppTypeDefinitionMetadata NetworkID_t920_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NetworkID_t920_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, NetworkID_t920_VTable/* vtableMethods */
	, NetworkID_t920_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1076/* fieldStart */

};
TypeInfo NetworkID_t920_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "NetworkID"/* name */
	, "UnityEngine.Networking.Types"/* namespaze */
	, NetworkID_t920_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UInt64_t1074_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 969/* custom_attributes_cache */
	, &NetworkID_t920_0_0_0/* byval_arg */
	, &NetworkID_t920_1_0_0/* this_arg */
	, &NetworkID_t920_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NetworkID_t920)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (NetworkID_t920)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint64_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.Networking.Types.NodeID
#include "UnityEngine_UnityEngine_Networking_Types_NodeID.h"
// Metadata Definition UnityEngine.Networking.Types.NodeID
extern TypeInfo NodeID_t921_il2cpp_TypeInfo;
// UnityEngine.Networking.Types.NodeID
#include "UnityEngine_UnityEngine_Networking_Types_NodeIDMethodDeclarations.h"
static const MethodInfo* NodeID_t921_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference NodeID_t921_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool NodeID_t921_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair NodeID_t921_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType NodeID_t921_1_0_0;
// System.UInt16
#include "mscorlib_System_UInt16.h"
extern TypeInfo UInt16_t684_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata NodeID_t921_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NodeID_t921_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, NodeID_t921_VTable/* vtableMethods */
	, NodeID_t921_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1078/* fieldStart */

};
TypeInfo NodeID_t921_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "NodeID"/* name */
	, "UnityEngine.Networking.Types"/* namespaze */
	, NodeID_t921_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UInt16_t684_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 970/* custom_attributes_cache */
	, &NodeID_t921_0_0_0/* byval_arg */
	, &NodeID_t921_1_0_0/* this_arg */
	, &NodeID_t921_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NodeID_t921)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (NodeID_t921)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint16_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.Networking.Types.NetworkAccessToken
#include "UnityEngine_UnityEngine_Networking_Types_NetworkAccessToken.h"
// Metadata Definition UnityEngine.Networking.Types.NetworkAccessToken
extern TypeInfo NetworkAccessToken_t922_il2cpp_TypeInfo;
// UnityEngine.Networking.Types.NetworkAccessToken
#include "UnityEngine_UnityEngine_Networking_Types_NetworkAccessTokenMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Networking.Types.NetworkAccessToken::.ctor()
extern const MethodInfo NetworkAccessToken__ctor_m4761_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&NetworkAccessToken__ctor_m4761/* method */
	, &NetworkAccessToken_t922_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1865/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Networking.Types.NetworkAccessToken::GetByteString()
extern const MethodInfo NetworkAccessToken_GetByteString_m4762_MethodInfo = 
{
	"GetByteString"/* name */
	, (methodPointerType)&NetworkAccessToken_GetByteString_m4762/* method */
	, &NetworkAccessToken_t922_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1866/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* NetworkAccessToken_t922_MethodInfos[] =
{
	&NetworkAccessToken__ctor_m4761_MethodInfo,
	&NetworkAccessToken_GetByteString_m4762_MethodInfo,
	NULL
};
static const Il2CppMethodReference NetworkAccessToken_t922_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool NetworkAccessToken_t922_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType NetworkAccessToken_t922_0_0_0;
extern const Il2CppType NetworkAccessToken_t922_1_0_0;
struct NetworkAccessToken_t922;
const Il2CppTypeDefinitionMetadata NetworkAccessToken_t922_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, NetworkAccessToken_t922_VTable/* vtableMethods */
	, NetworkAccessToken_t922_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1080/* fieldStart */

};
TypeInfo NetworkAccessToken_t922_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "NetworkAccessToken"/* name */
	, "UnityEngine.Networking.Types"/* namespaze */
	, NetworkAccessToken_t922_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &NetworkAccessToken_t922_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &NetworkAccessToken_t922_0_0_0/* byval_arg */
	, &NetworkAccessToken_t922_1_0_0/* this_arg */
	, &NetworkAccessToken_t922_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NetworkAccessToken_t922)/* instance_size */
	, sizeof (NetworkAccessToken_t922)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Networking.Utility
#include "UnityEngine_UnityEngine_Networking_Utility.h"
// Metadata Definition UnityEngine.Networking.Utility
extern TypeInfo Utility_t925_il2cpp_TypeInfo;
// UnityEngine.Networking.Utility
#include "UnityEngine_UnityEngine_Networking_UtilityMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Networking.Utility::.cctor()
extern const MethodInfo Utility__cctor_m4763_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&Utility__cctor_m4763/* method */
	, &Utility_t925_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1867/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_SourceID_t919 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Networking.Types.SourceID UnityEngine.Networking.Utility::GetSourceID()
extern const MethodInfo Utility_GetSourceID_m4764_MethodInfo = 
{
	"GetSourceID"/* name */
	, (methodPointerType)&Utility_GetSourceID_m4764/* method */
	, &Utility_t925_il2cpp_TypeInfo/* declaring_type */
	, &SourceID_t919_0_0_0/* return_type */
	, RuntimeInvoker_SourceID_t919/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1868/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType AppID_t918_0_0_0;
static const ParameterInfo Utility_t925_Utility_SetAppID_m4765_ParameterInfos[] = 
{
	{"newAppID", 0, 134219913, 0, &AppID_t918_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_UInt64_t1074 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Networking.Utility::SetAppID(UnityEngine.Networking.Types.AppID)
extern const MethodInfo Utility_SetAppID_m4765_MethodInfo = 
{
	"SetAppID"/* name */
	, (methodPointerType)&Utility_SetAppID_m4765/* method */
	, &Utility_t925_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_UInt64_t1074/* invoker_method */
	, Utility_t925_Utility_SetAppID_m4765_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1869/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_AppID_t918 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Networking.Types.AppID UnityEngine.Networking.Utility::GetAppID()
extern const MethodInfo Utility_GetAppID_m4766_MethodInfo = 
{
	"GetAppID"/* name */
	, (methodPointerType)&Utility_GetAppID_m4766/* method */
	, &Utility_t925_il2cpp_TypeInfo/* declaring_type */
	, &AppID_t918_0_0_0/* return_type */
	, RuntimeInvoker_AppID_t918/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1870/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType NetworkID_t920_0_0_0;
static const ParameterInfo Utility_t925_Utility_GetAccessTokenForNetwork_m4767_ParameterInfos[] = 
{
	{"netId", 0, 134219914, 0, &NetworkID_t920_0_0_0},
};
extern void* RuntimeInvoker_Object_t_UInt64_t1074 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Networking.Types.NetworkAccessToken UnityEngine.Networking.Utility::GetAccessTokenForNetwork(UnityEngine.Networking.Types.NetworkID)
extern const MethodInfo Utility_GetAccessTokenForNetwork_m4767_MethodInfo = 
{
	"GetAccessTokenForNetwork"/* name */
	, (methodPointerType)&Utility_GetAccessTokenForNetwork_m4767/* method */
	, &Utility_t925_il2cpp_TypeInfo/* declaring_type */
	, &NetworkAccessToken_t922_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_UInt64_t1074/* invoker_method */
	, Utility_t925_Utility_GetAccessTokenForNetwork_m4767_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1871/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Utility_t925_MethodInfos[] =
{
	&Utility__cctor_m4763_MethodInfo,
	&Utility_GetSourceID_m4764_MethodInfo,
	&Utility_SetAppID_m4765_MethodInfo,
	&Utility_GetAppID_m4766_MethodInfo,
	&Utility_GetAccessTokenForNetwork_m4767_MethodInfo,
	NULL
};
static const Il2CppMethodReference Utility_t925_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool Utility_t925_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Utility_t925_0_0_0;
extern const Il2CppType Utility_t925_1_0_0;
struct Utility_t925;
const Il2CppTypeDefinitionMetadata Utility_t925_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Utility_t925_VTable/* vtableMethods */
	, Utility_t925_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1081/* fieldStart */

};
TypeInfo Utility_t925_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Utility"/* name */
	, "UnityEngine.Networking"/* namespaze */
	, Utility_t925_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Utility_t925_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Utility_t925_0_0_0/* byval_arg */
	, &Utility_t925_1_0_0/* this_arg */
	, &Utility_t925_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Utility_t925)/* instance_size */
	, sizeof (Utility_t925)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Utility_t925_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Networking.Match.NetworkMatch/ResponseDelegate`1
extern TypeInfo ResponseDelegate_1_t1117_il2cpp_TypeInfo;
extern const Il2CppGenericContainer ResponseDelegate_1_t1117_Il2CppGenericContainer;
extern TypeInfo ResponseDelegate_1_t1117_gp_T_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter ResponseDelegate_1_t1117_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &ResponseDelegate_1_t1117_Il2CppGenericContainer, NULL, "T", 0, 0 };
static const Il2CppGenericParameter* ResponseDelegate_1_t1117_Il2CppGenericParametersArray[1] = 
{
	&ResponseDelegate_1_t1117_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer ResponseDelegate_1_t1117_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&ResponseDelegate_1_t1117_il2cpp_TypeInfo, 1, 0, ResponseDelegate_1_t1117_Il2CppGenericParametersArray };
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo ResponseDelegate_1_t1117_ResponseDelegate_1__ctor_m5286_ParameterInfos[] = 
{
	{"object", 0, 134219946, 0, &Object_t_0_0_0},
	{"method", 1, 134219947, 0, &IntPtr_t_0_0_0},
};
// System.Void UnityEngine.Networking.Match.NetworkMatch/ResponseDelegate`1::.ctor(System.Object,System.IntPtr)
extern const MethodInfo ResponseDelegate_1__ctor_m5286_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &ResponseDelegate_1_t1117_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ResponseDelegate_1_t1117_ResponseDelegate_1__ctor_m5286_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1887/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ResponseDelegate_1_t1117_gp_0_0_0_0;
extern const Il2CppType ResponseDelegate_1_t1117_gp_0_0_0_0;
static const ParameterInfo ResponseDelegate_1_t1117_ResponseDelegate_1_Invoke_m5287_ParameterInfos[] = 
{
	{"response", 0, 134219948, 0, &ResponseDelegate_1_t1117_gp_0_0_0_0},
};
// System.Void UnityEngine.Networking.Match.NetworkMatch/ResponseDelegate`1::Invoke(T)
extern const MethodInfo ResponseDelegate_1_Invoke_m5287_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &ResponseDelegate_1_t1117_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ResponseDelegate_1_t1117_ResponseDelegate_1_Invoke_m5287_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1888/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ResponseDelegate_1_t1117_gp_0_0_0_0;
extern const Il2CppType AsyncCallback_t547_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo ResponseDelegate_1_t1117_ResponseDelegate_1_BeginInvoke_m5288_ParameterInfos[] = 
{
	{"response", 0, 134219949, 0, &ResponseDelegate_1_t1117_gp_0_0_0_0},
	{"callback", 1, 134219950, 0, &AsyncCallback_t547_0_0_0},
	{"object", 2, 134219951, 0, &Object_t_0_0_0},
};
// System.IAsyncResult UnityEngine.Networking.Match.NetworkMatch/ResponseDelegate`1::BeginInvoke(T,System.AsyncCallback,System.Object)
extern const MethodInfo ResponseDelegate_1_BeginInvoke_m5288_MethodInfo = 
{
	"BeginInvoke"/* name */
	, NULL/* method */
	, &ResponseDelegate_1_t1117_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t546_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ResponseDelegate_1_t1117_ResponseDelegate_1_BeginInvoke_m5288_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1889/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t546_0_0_0;
static const ParameterInfo ResponseDelegate_1_t1117_ResponseDelegate_1_EndInvoke_m5289_ParameterInfos[] = 
{
	{"result", 0, 134219952, 0, &IAsyncResult_t546_0_0_0},
};
// System.Void UnityEngine.Networking.Match.NetworkMatch/ResponseDelegate`1::EndInvoke(System.IAsyncResult)
extern const MethodInfo ResponseDelegate_1_EndInvoke_m5289_MethodInfo = 
{
	"EndInvoke"/* name */
	, NULL/* method */
	, &ResponseDelegate_1_t1117_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ResponseDelegate_1_t1117_ResponseDelegate_1_EndInvoke_m5289_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1890/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ResponseDelegate_1_t1117_MethodInfos[] =
{
	&ResponseDelegate_1__ctor_m5286_MethodInfo,
	&ResponseDelegate_1_Invoke_m5287_MethodInfo,
	&ResponseDelegate_1_BeginInvoke_m5288_MethodInfo,
	&ResponseDelegate_1_EndInvoke_m5289_MethodInfo,
	NULL
};
extern const MethodInfo ResponseDelegate_1_Invoke_m5287_MethodInfo;
extern const MethodInfo ResponseDelegate_1_BeginInvoke_m5288_MethodInfo;
extern const MethodInfo ResponseDelegate_1_EndInvoke_m5289_MethodInfo;
static const Il2CppMethodReference ResponseDelegate_1_t1117_VTable[] =
{
	&MulticastDelegate_Equals_m3586_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&MulticastDelegate_GetHashCode_m3587_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&Delegate_Clone_m3589_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&MulticastDelegate_GetInvocationList_m3590_MethodInfo,
	&MulticastDelegate_CombineImpl_m3591_MethodInfo,
	&MulticastDelegate_RemoveImpl_m3592_MethodInfo,
	&ResponseDelegate_1_Invoke_m5287_MethodInfo,
	&ResponseDelegate_1_BeginInvoke_m5288_MethodInfo,
	&ResponseDelegate_1_EndInvoke_m5289_MethodInfo,
};
static bool ResponseDelegate_1_t1117_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ResponseDelegate_1_t1117_InterfacesOffsets[] = 
{
	{ &ICloneable_t733_0_0_0, 4},
	{ &ISerializable_t439_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ResponseDelegate_1_t1117_0_0_0;
extern const Il2CppType ResponseDelegate_1_t1117_1_0_0;
extern TypeInfo NetworkMatch_t927_il2cpp_TypeInfo;
extern const Il2CppType NetworkMatch_t927_0_0_0;
struct ResponseDelegate_1_t1117;
const Il2CppTypeDefinitionMetadata ResponseDelegate_1_t1117_DefinitionMetadata = 
{
	&NetworkMatch_t927_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ResponseDelegate_1_t1117_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t549_0_0_0/* parent */
	, ResponseDelegate_1_t1117_VTable/* vtableMethods */
	, ResponseDelegate_1_t1117_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ResponseDelegate_1_t1117_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ResponseDelegate`1"/* name */
	, ""/* namespaze */
	, ResponseDelegate_1_t1117_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ResponseDelegate_1_t1117_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ResponseDelegate_1_t1117_0_0_0/* byval_arg */
	, &ResponseDelegate_1_t1117_1_0_0/* this_arg */
	, &ResponseDelegate_1_t1117_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &ResponseDelegate_1_t1117_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Networking.Match.NetworkMatch/<ProcessMatchResponse>c__Iterator0`1
extern TypeInfo U3CProcessMatchResponseU3Ec__Iterator0_1_t1118_il2cpp_TypeInfo;
extern const Il2CppGenericContainer U3CProcessMatchResponseU3Ec__Iterator0_1_t1118_Il2CppGenericContainer;
extern TypeInfo U3CProcessMatchResponseU3Ec__Iterator0_1_t1118_gp_JSONRESPONSE_0_il2cpp_TypeInfo;
static const Il2CppType* U3CProcessMatchResponseU3Ec__Iterator0_1_t1118_gp_JSONRESPONSE_0_il2cpp_TypeInfo_constraints[] = { 
&Response_t903_0_0_0 /* UnityEngine.Networking.Match.Response */, 
 NULL };
extern const Il2CppGenericParameter U3CProcessMatchResponseU3Ec__Iterator0_1_t1118_gp_JSONRESPONSE_0_il2cpp_TypeInfo_GenericParamFull = { &U3CProcessMatchResponseU3Ec__Iterator0_1_t1118_Il2CppGenericContainer, U3CProcessMatchResponseU3Ec__Iterator0_1_t1118_gp_JSONRESPONSE_0_il2cpp_TypeInfo_constraints, "JSONRESPONSE", 0, 16 };
static const Il2CppGenericParameter* U3CProcessMatchResponseU3Ec__Iterator0_1_t1118_Il2CppGenericParametersArray[1] = 
{
	&U3CProcessMatchResponseU3Ec__Iterator0_1_t1118_gp_JSONRESPONSE_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer U3CProcessMatchResponseU3Ec__Iterator0_1_t1118_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&U3CProcessMatchResponseU3Ec__Iterator0_1_t1118_il2cpp_TypeInfo, 1, 0, U3CProcessMatchResponseU3Ec__Iterator0_1_t1118_Il2CppGenericParametersArray };
// System.Void UnityEngine.Networking.Match.NetworkMatch/<ProcessMatchResponse>c__Iterator0`1::.ctor()
extern const MethodInfo U3CProcessMatchResponseU3Ec__Iterator0_1__ctor_m5290_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &U3CProcessMatchResponseU3Ec__Iterator0_1_t1118_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1891/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Object UnityEngine.Networking.Match.NetworkMatch/<ProcessMatchResponse>c__Iterator0`1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern const MethodInfo U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m5291_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<object>.get_Current"/* name */
	, NULL/* method */
	, &U3CProcessMatchResponseU3Ec__Iterator0_1_t1118_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 973/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1892/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Object UnityEngine.Networking.Match.NetworkMatch/<ProcessMatchResponse>c__Iterator0`1::System.Collections.IEnumerator.get_Current()
extern const MethodInfo U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m5292_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, NULL/* method */
	, &U3CProcessMatchResponseU3Ec__Iterator0_1_t1118_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 974/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1893/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Boolean UnityEngine.Networking.Match.NetworkMatch/<ProcessMatchResponse>c__Iterator0`1::MoveNext()
extern const MethodInfo U3CProcessMatchResponseU3Ec__Iterator0_1_MoveNext_m5293_MethodInfo = 
{
	"MoveNext"/* name */
	, NULL/* method */
	, &U3CProcessMatchResponseU3Ec__Iterator0_1_t1118_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1894/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Void UnityEngine.Networking.Match.NetworkMatch/<ProcessMatchResponse>c__Iterator0`1::Dispose()
extern const MethodInfo U3CProcessMatchResponseU3Ec__Iterator0_1_Dispose_m5294_MethodInfo = 
{
	"Dispose"/* name */
	, NULL/* method */
	, &U3CProcessMatchResponseU3Ec__Iterator0_1_t1118_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 975/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1895/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CProcessMatchResponseU3Ec__Iterator0_1_t1118_MethodInfos[] =
{
	&U3CProcessMatchResponseU3Ec__Iterator0_1__ctor_m5290_MethodInfo,
	&U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m5291_MethodInfo,
	&U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m5292_MethodInfo,
	&U3CProcessMatchResponseU3Ec__Iterator0_1_MoveNext_m5293_MethodInfo,
	&U3CProcessMatchResponseU3Ec__Iterator0_1_Dispose_m5294_MethodInfo,
	NULL
};
extern const MethodInfo U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m5291_MethodInfo;
static const PropertyInfo U3CProcessMatchResponseU3Ec__Iterator0_1_t1118____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo = 
{
	&U3CProcessMatchResponseU3Ec__Iterator0_1_t1118_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<object>.Current"/* name */
	, &U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m5291_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m5292_MethodInfo;
static const PropertyInfo U3CProcessMatchResponseU3Ec__Iterator0_1_t1118____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CProcessMatchResponseU3Ec__Iterator0_1_t1118_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m5292_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* U3CProcessMatchResponseU3Ec__Iterator0_1_t1118_PropertyInfos[] =
{
	&U3CProcessMatchResponseU3Ec__Iterator0_1_t1118____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo,
	&U3CProcessMatchResponseU3Ec__Iterator0_1_t1118____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern const MethodInfo U3CProcessMatchResponseU3Ec__Iterator0_1_Dispose_m5294_MethodInfo;
extern const MethodInfo U3CProcessMatchResponseU3Ec__Iterator0_1_MoveNext_m5293_MethodInfo;
static const Il2CppMethodReference U3CProcessMatchResponseU3Ec__Iterator0_1_t1118_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&U3CProcessMatchResponseU3Ec__Iterator0_1_Dispose_m5294_MethodInfo,
	&U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m5292_MethodInfo,
	&U3CProcessMatchResponseU3Ec__Iterator0_1_MoveNext_m5293_MethodInfo,
	&U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m5291_MethodInfo,
};
static bool U3CProcessMatchResponseU3Ec__Iterator0_1_t1118_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IDisposable_t233_0_0_0;
extern const Il2CppType IEnumerator_t217_0_0_0;
extern const Il2CppType IEnumerator_1_t284_0_0_0;
static const Il2CppType* U3CProcessMatchResponseU3Ec__Iterator0_1_t1118_InterfacesTypeInfos[] = 
{
	&IDisposable_t233_0_0_0,
	&IEnumerator_t217_0_0_0,
	&IEnumerator_1_t284_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CProcessMatchResponseU3Ec__Iterator0_1_t1118_InterfacesOffsets[] = 
{
	{ &IDisposable_t233_0_0_0, 4},
	{ &IEnumerator_t217_0_0_0, 5},
	{ &IEnumerator_1_t284_0_0_0, 7},
};
extern const Il2CppType U3CProcessMatchResponseU3Ec__Iterator0_1_t1118_gp_0_0_0_0;
extern const Il2CppGenericMethod Activator_CreateInstance_TisJSONRESPONSE_t1191_m5401_GenericMethod;
extern const Il2CppGenericMethod ResponseDelegate_1_Invoke_m5402_GenericMethod;
static Il2CppRGCTXDefinition U3CProcessMatchResponseU3Ec__Iterator0_1_t1118_RGCTXData[4] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&U3CProcessMatchResponseU3Ec__Iterator0_1_t1118_gp_0_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Activator_CreateInstance_TisJSONRESPONSE_t1191_m5401_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &ResponseDelegate_1_Invoke_m5402_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType U3CProcessMatchResponseU3Ec__Iterator0_1_t1118_0_0_0;
extern const Il2CppType U3CProcessMatchResponseU3Ec__Iterator0_1_t1118_1_0_0;
struct U3CProcessMatchResponseU3Ec__Iterator0_1_t1118;
const Il2CppTypeDefinitionMetadata U3CProcessMatchResponseU3Ec__Iterator0_1_t1118_DefinitionMetadata = 
{
	&NetworkMatch_t927_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CProcessMatchResponseU3Ec__Iterator0_1_t1118_InterfacesTypeInfos/* implementedInterfaces */
	, U3CProcessMatchResponseU3Ec__Iterator0_1_t1118_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CProcessMatchResponseU3Ec__Iterator0_1_t1118_VTable/* vtableMethods */
	, U3CProcessMatchResponseU3Ec__Iterator0_1_t1118_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, U3CProcessMatchResponseU3Ec__Iterator0_1_t1118_RGCTXData/* rgctxDefinition */
	, 1086/* fieldStart */

};
TypeInfo U3CProcessMatchResponseU3Ec__Iterator0_1_t1118_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "<ProcessMatchResponse>c__Iterator0`1"/* name */
	, ""/* namespaze */
	, U3CProcessMatchResponseU3Ec__Iterator0_1_t1118_MethodInfos/* methods */
	, U3CProcessMatchResponseU3Ec__Iterator0_1_t1118_PropertyInfos/* properties */
	, NULL/* events */
	, &U3CProcessMatchResponseU3Ec__Iterator0_1_t1118_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 972/* custom_attributes_cache */
	, &U3CProcessMatchResponseU3Ec__Iterator0_1_t1118_0_0_0/* byval_arg */
	, &U3CProcessMatchResponseU3Ec__Iterator0_1_t1118_1_0_0/* this_arg */
	, &U3CProcessMatchResponseU3Ec__Iterator0_1_t1118_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &U3CProcessMatchResponseU3Ec__Iterator0_1_t1118_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 2/* property_count */
	, 10/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.Networking.Match.NetworkMatch
#include "UnityEngine_UnityEngine_Networking_Match_NetworkMatch.h"
// Metadata Definition UnityEngine.Networking.Match.NetworkMatch
// UnityEngine.Networking.Match.NetworkMatch
#include "UnityEngine_UnityEngine_Networking_Match_NetworkMatchMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Networking.Match.NetworkMatch::.ctor()
extern const MethodInfo NetworkMatch__ctor_m4768_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&NetworkMatch__ctor_m4768/* method */
	, &NetworkMatch_t927_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1872/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Uri_t926_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Uri UnityEngine.Networking.Match.NetworkMatch::get_baseUri()
extern const MethodInfo NetworkMatch_get_baseUri_m4769_MethodInfo = 
{
	"get_baseUri"/* name */
	, (methodPointerType)&NetworkMatch_get_baseUri_m4769/* method */
	, &NetworkMatch_t927_il2cpp_TypeInfo/* declaring_type */
	, &Uri_t926_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1873/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Uri_t926_0_0_0;
static const ParameterInfo NetworkMatch_t927_NetworkMatch_set_baseUri_m4770_ParameterInfos[] = 
{
	{"value", 0, 134219915, 0, &Uri_t926_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Networking.Match.NetworkMatch::set_baseUri(System.Uri)
extern const MethodInfo NetworkMatch_set_baseUri_m4770_MethodInfo = 
{
	"set_baseUri"/* name */
	, (methodPointerType)&NetworkMatch_set_baseUri_m4770/* method */
	, &NetworkMatch_t927_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, NetworkMatch_t927_NetworkMatch_set_baseUri_m4770_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1874/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType AppID_t918_0_0_0;
static const ParameterInfo NetworkMatch_t927_NetworkMatch_SetProgramAppID_m4771_ParameterInfos[] = 
{
	{"programAppID", 0, 134219916, 0, &AppID_t918_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_UInt64_t1074 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Networking.Match.NetworkMatch::SetProgramAppID(UnityEngine.Networking.Types.AppID)
extern const MethodInfo NetworkMatch_SetProgramAppID_m4771_MethodInfo = 
{
	"SetProgramAppID"/* name */
	, (methodPointerType)&NetworkMatch_SetProgramAppID_m4771/* method */
	, &NetworkMatch_t927_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_UInt64_t1074/* invoker_method */
	, NetworkMatch_t927_NetworkMatch_SetProgramAppID_m4771_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1875/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType UInt32_t1063_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType ResponseDelegate_1_t1028_0_0_0;
extern const Il2CppType ResponseDelegate_1_t1028_0_0_0;
static const ParameterInfo NetworkMatch_t927_NetworkMatch_CreateMatch_m4772_ParameterInfos[] = 
{
	{"matchName", 0, 134219917, 0, &String_t_0_0_0},
	{"matchSize", 1, 134219918, 0, &UInt32_t1063_0_0_0},
	{"matchAdvertise", 2, 134219919, 0, &Boolean_t273_0_0_0},
	{"matchPassword", 3, 134219920, 0, &String_t_0_0_0},
	{"callback", 4, 134219921, 0, &ResponseDelegate_1_t1028_0_0_0},
};
extern const Il2CppType Coroutine_t559_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t253_SByte_t274_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Coroutine UnityEngine.Networking.Match.NetworkMatch::CreateMatch(System.String,System.UInt32,System.Boolean,System.String,UnityEngine.Networking.Match.NetworkMatch/ResponseDelegate`1<UnityEngine.Networking.Match.CreateMatchResponse>)
extern const MethodInfo NetworkMatch_CreateMatch_m4772_MethodInfo = 
{
	"CreateMatch"/* name */
	, (methodPointerType)&NetworkMatch_CreateMatch_m4772/* method */
	, &NetworkMatch_t927_il2cpp_TypeInfo/* declaring_type */
	, &Coroutine_t559_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t253_SByte_t274_Object_t_Object_t/* invoker_method */
	, NetworkMatch_t927_NetworkMatch_CreateMatch_m4772_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1876/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType CreateMatchRequest_t906_0_0_0;
extern const Il2CppType ResponseDelegate_1_t1028_0_0_0;
static const ParameterInfo NetworkMatch_t927_NetworkMatch_CreateMatch_m4773_ParameterInfos[] = 
{
	{"req", 0, 134219922, 0, &CreateMatchRequest_t906_0_0_0},
	{"callback", 1, 134219923, 0, &ResponseDelegate_1_t1028_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Coroutine UnityEngine.Networking.Match.NetworkMatch::CreateMatch(UnityEngine.Networking.Match.CreateMatchRequest,UnityEngine.Networking.Match.NetworkMatch/ResponseDelegate`1<UnityEngine.Networking.Match.CreateMatchResponse>)
extern const MethodInfo NetworkMatch_CreateMatch_m4773_MethodInfo = 
{
	"CreateMatch"/* name */
	, (methodPointerType)&NetworkMatch_CreateMatch_m4773/* method */
	, &NetworkMatch_t927_il2cpp_TypeInfo/* declaring_type */
	, &Coroutine_t559_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, NetworkMatch_t927_NetworkMatch_CreateMatch_m4773_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1877/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType NetworkID_t920_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType ResponseDelegate_1_t1029_0_0_0;
extern const Il2CppType ResponseDelegate_1_t1029_0_0_0;
static const ParameterInfo NetworkMatch_t927_NetworkMatch_JoinMatch_m4774_ParameterInfos[] = 
{
	{"netId", 0, 134219924, 0, &NetworkID_t920_0_0_0},
	{"matchPassword", 1, 134219925, 0, &String_t_0_0_0},
	{"callback", 2, 134219926, 0, &ResponseDelegate_1_t1029_0_0_0},
};
extern void* RuntimeInvoker_Object_t_UInt64_t1074_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Coroutine UnityEngine.Networking.Match.NetworkMatch::JoinMatch(UnityEngine.Networking.Types.NetworkID,System.String,UnityEngine.Networking.Match.NetworkMatch/ResponseDelegate`1<UnityEngine.Networking.Match.JoinMatchResponse>)
extern const MethodInfo NetworkMatch_JoinMatch_m4774_MethodInfo = 
{
	"JoinMatch"/* name */
	, (methodPointerType)&NetworkMatch_JoinMatch_m4774/* method */
	, &NetworkMatch_t927_il2cpp_TypeInfo/* declaring_type */
	, &Coroutine_t559_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_UInt64_t1074_Object_t_Object_t/* invoker_method */
	, NetworkMatch_t927_NetworkMatch_JoinMatch_m4774_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1878/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType JoinMatchRequest_t908_0_0_0;
extern const Il2CppType ResponseDelegate_1_t1029_0_0_0;
static const ParameterInfo NetworkMatch_t927_NetworkMatch_JoinMatch_m4775_ParameterInfos[] = 
{
	{"req", 0, 134219927, 0, &JoinMatchRequest_t908_0_0_0},
	{"callback", 1, 134219928, 0, &ResponseDelegate_1_t1029_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Coroutine UnityEngine.Networking.Match.NetworkMatch::JoinMatch(UnityEngine.Networking.Match.JoinMatchRequest,UnityEngine.Networking.Match.NetworkMatch/ResponseDelegate`1<UnityEngine.Networking.Match.JoinMatchResponse>)
extern const MethodInfo NetworkMatch_JoinMatch_m4775_MethodInfo = 
{
	"JoinMatch"/* name */
	, (methodPointerType)&NetworkMatch_JoinMatch_m4775/* method */
	, &NetworkMatch_t927_il2cpp_TypeInfo/* declaring_type */
	, &Coroutine_t559_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, NetworkMatch_t927_NetworkMatch_JoinMatch_m4775_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1879/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType NetworkID_t920_0_0_0;
extern const Il2CppType ResponseDelegate_1_t1030_0_0_0;
extern const Il2CppType ResponseDelegate_1_t1030_0_0_0;
static const ParameterInfo NetworkMatch_t927_NetworkMatch_DestroyMatch_m4776_ParameterInfos[] = 
{
	{"netId", 0, 134219929, 0, &NetworkID_t920_0_0_0},
	{"callback", 1, 134219930, 0, &ResponseDelegate_1_t1030_0_0_0},
};
extern void* RuntimeInvoker_Object_t_UInt64_t1074_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Coroutine UnityEngine.Networking.Match.NetworkMatch::DestroyMatch(UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Match.NetworkMatch/ResponseDelegate`1<UnityEngine.Networking.Match.BasicResponse>)
extern const MethodInfo NetworkMatch_DestroyMatch_m4776_MethodInfo = 
{
	"DestroyMatch"/* name */
	, (methodPointerType)&NetworkMatch_DestroyMatch_m4776/* method */
	, &NetworkMatch_t927_il2cpp_TypeInfo/* declaring_type */
	, &Coroutine_t559_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_UInt64_t1074_Object_t/* invoker_method */
	, NetworkMatch_t927_NetworkMatch_DestroyMatch_m4776_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1880/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType DestroyMatchRequest_t910_0_0_0;
extern const Il2CppType ResponseDelegate_1_t1030_0_0_0;
static const ParameterInfo NetworkMatch_t927_NetworkMatch_DestroyMatch_m4777_ParameterInfos[] = 
{
	{"req", 0, 134219931, 0, &DestroyMatchRequest_t910_0_0_0},
	{"callback", 1, 134219932, 0, &ResponseDelegate_1_t1030_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Coroutine UnityEngine.Networking.Match.NetworkMatch::DestroyMatch(UnityEngine.Networking.Match.DestroyMatchRequest,UnityEngine.Networking.Match.NetworkMatch/ResponseDelegate`1<UnityEngine.Networking.Match.BasicResponse>)
extern const MethodInfo NetworkMatch_DestroyMatch_m4777_MethodInfo = 
{
	"DestroyMatch"/* name */
	, (methodPointerType)&NetworkMatch_DestroyMatch_m4777/* method */
	, &NetworkMatch_t927_il2cpp_TypeInfo/* declaring_type */
	, &Coroutine_t559_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, NetworkMatch_t927_NetworkMatch_DestroyMatch_m4777_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1881/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType NetworkID_t920_0_0_0;
extern const Il2CppType NodeID_t921_0_0_0;
extern const Il2CppType ResponseDelegate_1_t1030_0_0_0;
static const ParameterInfo NetworkMatch_t927_NetworkMatch_DropConnection_m4778_ParameterInfos[] = 
{
	{"netId", 0, 134219933, 0, &NetworkID_t920_0_0_0},
	{"dropNodeId", 1, 134219934, 0, &NodeID_t921_0_0_0},
	{"callback", 2, 134219935, 0, &ResponseDelegate_1_t1030_0_0_0},
};
extern void* RuntimeInvoker_Object_t_UInt64_t1074_UInt16_t684_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Coroutine UnityEngine.Networking.Match.NetworkMatch::DropConnection(UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NodeID,UnityEngine.Networking.Match.NetworkMatch/ResponseDelegate`1<UnityEngine.Networking.Match.BasicResponse>)
extern const MethodInfo NetworkMatch_DropConnection_m4778_MethodInfo = 
{
	"DropConnection"/* name */
	, (methodPointerType)&NetworkMatch_DropConnection_m4778/* method */
	, &NetworkMatch_t927_il2cpp_TypeInfo/* declaring_type */
	, &Coroutine_t559_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_UInt64_t1074_UInt16_t684_Object_t/* invoker_method */
	, NetworkMatch_t927_NetworkMatch_DropConnection_m4778_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1882/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType DropConnectionRequest_t911_0_0_0;
extern const Il2CppType ResponseDelegate_1_t1030_0_0_0;
static const ParameterInfo NetworkMatch_t927_NetworkMatch_DropConnection_m4779_ParameterInfos[] = 
{
	{"req", 0, 134219936, 0, &DropConnectionRequest_t911_0_0_0},
	{"callback", 1, 134219937, 0, &ResponseDelegate_1_t1030_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Coroutine UnityEngine.Networking.Match.NetworkMatch::DropConnection(UnityEngine.Networking.Match.DropConnectionRequest,UnityEngine.Networking.Match.NetworkMatch/ResponseDelegate`1<UnityEngine.Networking.Match.BasicResponse>)
extern const MethodInfo NetworkMatch_DropConnection_m4779_MethodInfo = 
{
	"DropConnection"/* name */
	, (methodPointerType)&NetworkMatch_DropConnection_m4779/* method */
	, &NetworkMatch_t927_il2cpp_TypeInfo/* declaring_type */
	, &Coroutine_t559_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, NetworkMatch_t927_NetworkMatch_DropConnection_m4779_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1883/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType ResponseDelegate_1_t1031_0_0_0;
extern const Il2CppType ResponseDelegate_1_t1031_0_0_0;
static const ParameterInfo NetworkMatch_t927_NetworkMatch_ListMatches_m4780_ParameterInfos[] = 
{
	{"startPageNumber", 0, 134219938, 0, &Int32_t253_0_0_0},
	{"resultPageSize", 1, 134219939, 0, &Int32_t253_0_0_0},
	{"matchNameFilter", 2, 134219940, 0, &String_t_0_0_0},
	{"callback", 3, 134219941, 0, &ResponseDelegate_1_t1031_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t253_Int32_t253_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Coroutine UnityEngine.Networking.Match.NetworkMatch::ListMatches(System.Int32,System.Int32,System.String,UnityEngine.Networking.Match.NetworkMatch/ResponseDelegate`1<UnityEngine.Networking.Match.ListMatchResponse>)
extern const MethodInfo NetworkMatch_ListMatches_m4780_MethodInfo = 
{
	"ListMatches"/* name */
	, (methodPointerType)&NetworkMatch_ListMatches_m4780/* method */
	, &NetworkMatch_t927_il2cpp_TypeInfo/* declaring_type */
	, &Coroutine_t559_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t253_Int32_t253_Object_t_Object_t/* invoker_method */
	, NetworkMatch_t927_NetworkMatch_ListMatches_m4780_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1884/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ListMatchRequest_t912_0_0_0;
extern const Il2CppType ResponseDelegate_1_t1031_0_0_0;
static const ParameterInfo NetworkMatch_t927_NetworkMatch_ListMatches_m4781_ParameterInfos[] = 
{
	{"req", 0, 134219942, 0, &ListMatchRequest_t912_0_0_0},
	{"callback", 1, 134219943, 0, &ResponseDelegate_1_t1031_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Coroutine UnityEngine.Networking.Match.NetworkMatch::ListMatches(UnityEngine.Networking.Match.ListMatchRequest,UnityEngine.Networking.Match.NetworkMatch/ResponseDelegate`1<UnityEngine.Networking.Match.ListMatchResponse>)
extern const MethodInfo NetworkMatch_ListMatches_m4781_MethodInfo = 
{
	"ListMatches"/* name */
	, (methodPointerType)&NetworkMatch_ListMatches_m4781/* method */
	, &NetworkMatch_t927_il2cpp_TypeInfo/* declaring_type */
	, &Coroutine_t559_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, NetworkMatch_t927_NetworkMatch_ListMatches_m4781_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1885/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType WWW_t294_0_0_0;
extern const Il2CppType WWW_t294_0_0_0;
extern const Il2CppType ResponseDelegate_1_t1192_0_0_0;
extern const Il2CppType ResponseDelegate_1_t1192_0_0_0;
static const ParameterInfo NetworkMatch_t927_NetworkMatch_ProcessMatchResponse_m5285_ParameterInfos[] = 
{
	{"client", 0, 134219944, 0, &WWW_t294_0_0_0},
	{"callback", 1, 134219945, 0, &ResponseDelegate_1_t1192_0_0_0},
};
extern const Il2CppGenericContainer NetworkMatch_ProcessMatchResponse_m5285_Il2CppGenericContainer;
extern TypeInfo NetworkMatch_ProcessMatchResponse_m5285_gp_JSONRESPONSE_0_il2cpp_TypeInfo;
static const Il2CppType* NetworkMatch_ProcessMatchResponse_m5285_gp_JSONRESPONSE_0_il2cpp_TypeInfo_constraints[] = { 
&Response_t903_0_0_0 /* UnityEngine.Networking.Match.Response */, 
 NULL };
extern const Il2CppGenericParameter NetworkMatch_ProcessMatchResponse_m5285_gp_JSONRESPONSE_0_il2cpp_TypeInfo_GenericParamFull = { &NetworkMatch_ProcessMatchResponse_m5285_Il2CppGenericContainer, NetworkMatch_ProcessMatchResponse_m5285_gp_JSONRESPONSE_0_il2cpp_TypeInfo_constraints, "JSONRESPONSE", 0, 16 };
static const Il2CppGenericParameter* NetworkMatch_ProcessMatchResponse_m5285_Il2CppGenericParametersArray[1] = 
{
	&NetworkMatch_ProcessMatchResponse_m5285_gp_JSONRESPONSE_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo NetworkMatch_ProcessMatchResponse_m5285_MethodInfo;
extern const Il2CppGenericContainer NetworkMatch_ProcessMatchResponse_m5285_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&NetworkMatch_ProcessMatchResponse_m5285_MethodInfo, 1, 1, NetworkMatch_ProcessMatchResponse_m5285_Il2CppGenericParametersArray };
extern const Il2CppType U3CProcessMatchResponseU3Ec__Iterator0_1_t1194_0_0_0;
extern const Il2CppGenericMethod U3CProcessMatchResponseU3Ec__Iterator0_1__ctor_m5403_GenericMethod;
static Il2CppRGCTXDefinition NetworkMatch_ProcessMatchResponse_m5285_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&U3CProcessMatchResponseU3Ec__Iterator0_1_t1194_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &U3CProcessMatchResponseU3Ec__Iterator0_1__ctor_m5403_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Collections.IEnumerator UnityEngine.Networking.Match.NetworkMatch::ProcessMatchResponse(UnityEngine.WWW,UnityEngine.Networking.Match.NetworkMatch/ResponseDelegate`1<JSONRESPONSE>)
extern const MethodInfo NetworkMatch_ProcessMatchResponse_m5285_MethodInfo = 
{
	"ProcessMatchResponse"/* name */
	, NULL/* method */
	, &NetworkMatch_t927_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t217_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NetworkMatch_t927_NetworkMatch_ProcessMatchResponse_m5285_ParameterInfos/* parameters */
	, 971/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 1886/* token */
	, NetworkMatch_ProcessMatchResponse_m5285_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &NetworkMatch_ProcessMatchResponse_m5285_Il2CppGenericContainer/* genericContainer */

};
static const MethodInfo* NetworkMatch_t927_MethodInfos[] =
{
	&NetworkMatch__ctor_m4768_MethodInfo,
	&NetworkMatch_get_baseUri_m4769_MethodInfo,
	&NetworkMatch_set_baseUri_m4770_MethodInfo,
	&NetworkMatch_SetProgramAppID_m4771_MethodInfo,
	&NetworkMatch_CreateMatch_m4772_MethodInfo,
	&NetworkMatch_CreateMatch_m4773_MethodInfo,
	&NetworkMatch_JoinMatch_m4774_MethodInfo,
	&NetworkMatch_JoinMatch_m4775_MethodInfo,
	&NetworkMatch_DestroyMatch_m4776_MethodInfo,
	&NetworkMatch_DestroyMatch_m4777_MethodInfo,
	&NetworkMatch_DropConnection_m4778_MethodInfo,
	&NetworkMatch_DropConnection_m4779_MethodInfo,
	&NetworkMatch_ListMatches_m4780_MethodInfo,
	&NetworkMatch_ListMatches_m4781_MethodInfo,
	&NetworkMatch_ProcessMatchResponse_m5285_MethodInfo,
	NULL
};
extern const MethodInfo NetworkMatch_get_baseUri_m4769_MethodInfo;
extern const MethodInfo NetworkMatch_set_baseUri_m4770_MethodInfo;
static const PropertyInfo NetworkMatch_t927____baseUri_PropertyInfo = 
{
	&NetworkMatch_t927_il2cpp_TypeInfo/* parent */
	, "baseUri"/* name */
	, &NetworkMatch_get_baseUri_m4769_MethodInfo/* get */
	, &NetworkMatch_set_baseUri_m4770_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* NetworkMatch_t927_PropertyInfos[] =
{
	&NetworkMatch_t927____baseUri_PropertyInfo,
	NULL
};
static const Il2CppType* NetworkMatch_t927_il2cpp_TypeInfo__nestedTypes[2] =
{
	&ResponseDelegate_1_t1117_0_0_0,
	&U3CProcessMatchResponseU3Ec__Iterator0_1_t1118_0_0_0,
};
static const Il2CppMethodReference NetworkMatch_t927_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
};
static bool NetworkMatch_t927_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType NetworkMatch_t927_1_0_0;
extern const Il2CppType MonoBehaviour_t3_0_0_0;
struct NetworkMatch_t927;
const Il2CppTypeDefinitionMetadata NetworkMatch_t927_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NetworkMatch_t927_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t3_0_0_0/* parent */
	, NetworkMatch_t927_VTable/* vtableMethods */
	, NetworkMatch_t927_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1096/* fieldStart */

};
TypeInfo NetworkMatch_t927_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "NetworkMatch"/* name */
	, "UnityEngine.Networking.Match"/* namespaze */
	, NetworkMatch_t927_MethodInfos/* methods */
	, NetworkMatch_t927_PropertyInfos/* properties */
	, NULL/* events */
	, &NetworkMatch_t927_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &NetworkMatch_t927_0_0_0/* byval_arg */
	, &NetworkMatch_t927_1_0_0/* this_arg */
	, &NetworkMatch_t927_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NetworkMatch_t927)/* instance_size */
	, sizeof (NetworkMatch_t927)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 15/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SimpleJson.JsonArray
#include "UnityEngine_SimpleJson_JsonArray.h"
// Metadata Definition SimpleJson.JsonArray
extern TypeInfo JsonArray_t928_il2cpp_TypeInfo;
// SimpleJson.JsonArray
#include "UnityEngine_SimpleJson_JsonArrayMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SimpleJson.JsonArray::.ctor()
extern const MethodInfo JsonArray__ctor_m4782_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&JsonArray__ctor_m4782/* method */
	, &JsonArray_t928_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1896/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String SimpleJson.JsonArray::ToString()
extern const MethodInfo JsonArray_ToString_m4783_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&JsonArray_ToString_m4783/* method */
	, &JsonArray_t928_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1897/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* JsonArray_t928_MethodInfos[] =
{
	&JsonArray__ctor_m4782_MethodInfo,
	&JsonArray_ToString_m4783_MethodInfo,
	NULL
};
extern const MethodInfo JsonArray_ToString_m4783_MethodInfo;
extern const Il2CppGenericMethod List_1_System_Collections_IEnumerable_GetEnumerator_m5404_GenericMethod;
extern const Il2CppGenericMethod List_1_get_Count_m5405_GenericMethod;
extern const Il2CppGenericMethod List_1_System_Collections_ICollection_get_IsSynchronized_m5406_GenericMethod;
extern const Il2CppGenericMethod List_1_System_Collections_ICollection_get_SyncRoot_m5407_GenericMethod;
extern const Il2CppGenericMethod List_1_System_Collections_ICollection_CopyTo_m5408_GenericMethod;
extern const Il2CppGenericMethod List_1_System_Collections_IList_get_IsFixedSize_m5409_GenericMethod;
extern const Il2CppGenericMethod List_1_System_Collections_IList_get_IsReadOnly_m5410_GenericMethod;
extern const Il2CppGenericMethod List_1_System_Collections_IList_get_Item_m5411_GenericMethod;
extern const Il2CppGenericMethod List_1_System_Collections_IList_set_Item_m5412_GenericMethod;
extern const Il2CppGenericMethod List_1_System_Collections_IList_Add_m5413_GenericMethod;
extern const Il2CppGenericMethod List_1_Clear_m5414_GenericMethod;
extern const Il2CppGenericMethod List_1_System_Collections_IList_Contains_m5415_GenericMethod;
extern const Il2CppGenericMethod List_1_System_Collections_IList_IndexOf_m5416_GenericMethod;
extern const Il2CppGenericMethod List_1_System_Collections_IList_Insert_m5417_GenericMethod;
extern const Il2CppGenericMethod List_1_System_Collections_IList_Remove_m5418_GenericMethod;
extern const Il2CppGenericMethod List_1_RemoveAt_m5419_GenericMethod;
extern const Il2CppGenericMethod List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m5420_GenericMethod;
extern const Il2CppGenericMethod List_1_Add_m5421_GenericMethod;
extern const Il2CppGenericMethod List_1_Contains_m5422_GenericMethod;
extern const Il2CppGenericMethod List_1_CopyTo_m5423_GenericMethod;
extern const Il2CppGenericMethod List_1_Remove_m5424_GenericMethod;
extern const Il2CppGenericMethod List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m5425_GenericMethod;
extern const Il2CppGenericMethod List_1_IndexOf_m5426_GenericMethod;
extern const Il2CppGenericMethod List_1_Insert_m5427_GenericMethod;
extern const Il2CppGenericMethod List_1_get_Item_m5428_GenericMethod;
extern const Il2CppGenericMethod List_1_set_Item_m5429_GenericMethod;
static const Il2CppMethodReference JsonArray_t928_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&JsonArray_ToString_m4783_MethodInfo,
	&List_1_System_Collections_IEnumerable_GetEnumerator_m5404_GenericMethod,
	&List_1_get_Count_m5405_GenericMethod,
	&List_1_System_Collections_ICollection_get_IsSynchronized_m5406_GenericMethod,
	&List_1_System_Collections_ICollection_get_SyncRoot_m5407_GenericMethod,
	&List_1_System_Collections_ICollection_CopyTo_m5408_GenericMethod,
	&List_1_System_Collections_IList_get_IsFixedSize_m5409_GenericMethod,
	&List_1_System_Collections_IList_get_IsReadOnly_m5410_GenericMethod,
	&List_1_System_Collections_IList_get_Item_m5411_GenericMethod,
	&List_1_System_Collections_IList_set_Item_m5412_GenericMethod,
	&List_1_System_Collections_IList_Add_m5413_GenericMethod,
	&List_1_Clear_m5414_GenericMethod,
	&List_1_System_Collections_IList_Contains_m5415_GenericMethod,
	&List_1_System_Collections_IList_IndexOf_m5416_GenericMethod,
	&List_1_System_Collections_IList_Insert_m5417_GenericMethod,
	&List_1_System_Collections_IList_Remove_m5418_GenericMethod,
	&List_1_RemoveAt_m5419_GenericMethod,
	&List_1_get_Count_m5405_GenericMethod,
	&List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m5420_GenericMethod,
	&List_1_Add_m5421_GenericMethod,
	&List_1_Clear_m5414_GenericMethod,
	&List_1_Contains_m5422_GenericMethod,
	&List_1_CopyTo_m5423_GenericMethod,
	&List_1_Remove_m5424_GenericMethod,
	&List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m5425_GenericMethod,
	&List_1_IndexOf_m5426_GenericMethod,
	&List_1_Insert_m5427_GenericMethod,
	&List_1_RemoveAt_m5419_GenericMethod,
	&List_1_get_Item_m5428_GenericMethod,
	&List_1_set_Item_m5429_GenericMethod,
};
static bool JsonArray_t928_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	true,
	true,
	true,
	true,
	true,
	true,
	true,
	true,
	true,
	true,
	true,
	true,
	true,
	true,
	true,
	true,
	true,
	true,
	true,
	true,
	true,
	true,
	true,
	true,
	true,
	true,
	true,
	true,
	true,
};
extern const Il2CppType IEnumerable_t438_0_0_0;
extern const Il2CppType ICollection_t440_0_0_0;
extern const Il2CppType IList_t1195_0_0_0;
extern const Il2CppType ICollection_1_t1033_0_0_0;
extern const Il2CppType IEnumerable_1_t698_0_0_0;
extern const Il2CppType IList_1_t1196_0_0_0;
static Il2CppInterfaceOffsetPair JsonArray_t928_InterfacesOffsets[] = 
{
	{ &IEnumerable_t438_0_0_0, 4},
	{ &ICollection_t440_0_0_0, 5},
	{ &IList_t1195_0_0_0, 9},
	{ &ICollection_1_t1033_0_0_0, 20},
	{ &IEnumerable_1_t698_0_0_0, 27},
	{ &IList_1_t1196_0_0_0, 28},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType JsonArray_t928_0_0_0;
extern const Il2CppType JsonArray_t928_1_0_0;
extern const Il2CppType List_1_t655_0_0_0;
struct JsonArray_t928;
const Il2CppTypeDefinitionMetadata JsonArray_t928_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, JsonArray_t928_InterfacesOffsets/* interfaceOffsets */
	, &List_1_t655_0_0_0/* parent */
	, JsonArray_t928_VTable/* vtableMethods */
	, JsonArray_t928_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo JsonArray_t928_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "JsonArray"/* name */
	, "SimpleJson"/* namespaze */
	, JsonArray_t928_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &JsonArray_t928_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 976/* custom_attributes_cache */
	, &JsonArray_t928_0_0_0/* byval_arg */
	, &JsonArray_t928_1_0_0/* this_arg */
	, &JsonArray_t928_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (JsonArray_t928)/* instance_size */
	, sizeof (JsonArray_t928)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 33/* vtable_count */
	, 0/* interfaces_count */
	, 6/* interface_offsets_count */

};
// SimpleJson.JsonObject
#include "UnityEngine_SimpleJson_JsonObject.h"
// Metadata Definition SimpleJson.JsonObject
extern TypeInfo JsonObject_t930_il2cpp_TypeInfo;
// SimpleJson.JsonObject
#include "UnityEngine_SimpleJson_JsonObjectMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SimpleJson.JsonObject::.ctor()
extern const MethodInfo JsonObject__ctor_m4784_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&JsonObject__ctor_m4784/* method */
	, &JsonObject_t930_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1898/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.IEnumerator SimpleJson.JsonObject::System.Collections.IEnumerable.GetEnumerator()
extern const MethodInfo JsonObject_System_Collections_IEnumerable_GetEnumerator_m4785_MethodInfo = 
{
	"System.Collections.IEnumerable.GetEnumerator"/* name */
	, (methodPointerType)&JsonObject_System_Collections_IEnumerable_GetEnumerator_m4785/* method */
	, &JsonObject_t930_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t217_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1899/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo JsonObject_t930_JsonObject_Add_m4786_ParameterInfos[] = 
{
	{"key", 0, 134219953, 0, &String_t_0_0_0},
	{"value", 1, 134219954, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void SimpleJson.JsonObject::Add(System.String,System.Object)
extern const MethodInfo JsonObject_Add_m4786_MethodInfo = 
{
	"Add"/* name */
	, (methodPointerType)&JsonObject_Add_m4786/* method */
	, &JsonObject_t930_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, JsonObject_t930_JsonObject_Add_m4786_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1900/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICollection_1_t1032_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.Generic.ICollection`1<System.String> SimpleJson.JsonObject::get_Keys()
extern const MethodInfo JsonObject_get_Keys_m4787_MethodInfo = 
{
	"get_Keys"/* name */
	, (methodPointerType)&JsonObject_get_Keys_m4787/* method */
	, &JsonObject_t930_il2cpp_TypeInfo/* declaring_type */
	, &ICollection_1_t1032_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1901/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo JsonObject_t930_JsonObject_Remove_m4788_ParameterInfos[] = 
{
	{"key", 0, 134219955, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean SimpleJson.JsonObject::Remove(System.String)
extern const MethodInfo JsonObject_Remove_m4788_MethodInfo = 
{
	"Remove"/* name */
	, (methodPointerType)&JsonObject_Remove_m4788/* method */
	, &JsonObject_t930_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, JsonObject_t930_JsonObject_Remove_m4788_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1902/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_1_0_2;
extern const Il2CppType Object_t_1_0_0;
static const ParameterInfo JsonObject_t930_JsonObject_TryGetValue_m4789_ParameterInfos[] = 
{
	{"key", 0, 134219956, 0, &String_t_0_0_0},
	{"value", 1, 134219957, 0, &Object_t_1_0_2},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t_ObjectU26_t1197 (const MethodInfo* method, void* obj, void** args);
// System.Boolean SimpleJson.JsonObject::TryGetValue(System.String,System.Object&)
extern const MethodInfo JsonObject_TryGetValue_m4789_MethodInfo = 
{
	"TryGetValue"/* name */
	, (methodPointerType)&JsonObject_TryGetValue_m4789/* method */
	, &JsonObject_t930_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t_ObjectU26_t1197/* invoker_method */
	, JsonObject_t930_JsonObject_TryGetValue_m4789_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1903/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.Generic.ICollection`1<System.Object> SimpleJson.JsonObject::get_Values()
extern const MethodInfo JsonObject_get_Values_m4790_MethodInfo = 
{
	"get_Values"/* name */
	, (methodPointerType)&JsonObject_get_Values_m4790/* method */
	, &JsonObject_t930_il2cpp_TypeInfo/* declaring_type */
	, &ICollection_1_t1033_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1904/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo JsonObject_t930_JsonObject_get_Item_m4791_ParameterInfos[] = 
{
	{"key", 0, 134219958, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object SimpleJson.JsonObject::get_Item(System.String)
extern const MethodInfo JsonObject_get_Item_m4791_MethodInfo = 
{
	"get_Item"/* name */
	, (methodPointerType)&JsonObject_get_Item_m4791/* method */
	, &JsonObject_t930_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, JsonObject_t930_JsonObject_get_Item_m4791_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1905/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo JsonObject_t930_JsonObject_set_Item_m4792_ParameterInfos[] = 
{
	{"key", 0, 134219959, 0, &String_t_0_0_0},
	{"value", 1, 134219960, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void SimpleJson.JsonObject::set_Item(System.String,System.Object)
extern const MethodInfo JsonObject_set_Item_m4792_MethodInfo = 
{
	"set_Item"/* name */
	, (methodPointerType)&JsonObject_set_Item_m4792/* method */
	, &JsonObject_t930_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, JsonObject_t930_JsonObject_set_Item_m4792_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1906/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType KeyValuePair_2_t1035_0_0_0;
extern const Il2CppType KeyValuePair_2_t1035_0_0_0;
static const ParameterInfo JsonObject_t930_JsonObject_Add_m4793_ParameterInfos[] = 
{
	{"item", 0, 134219961, 0, &KeyValuePair_2_t1035_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_KeyValuePair_2_t1035 (const MethodInfo* method, void* obj, void** args);
// System.Void SimpleJson.JsonObject::Add(System.Collections.Generic.KeyValuePair`2<System.String,System.Object>)
extern const MethodInfo JsonObject_Add_m4793_MethodInfo = 
{
	"Add"/* name */
	, (methodPointerType)&JsonObject_Add_m4793/* method */
	, &JsonObject_t930_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_KeyValuePair_2_t1035/* invoker_method */
	, JsonObject_t930_JsonObject_Add_m4793_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1907/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SimpleJson.JsonObject::Clear()
extern const MethodInfo JsonObject_Clear_m4794_MethodInfo = 
{
	"Clear"/* name */
	, (methodPointerType)&JsonObject_Clear_m4794/* method */
	, &JsonObject_t930_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1908/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType KeyValuePair_2_t1035_0_0_0;
static const ParameterInfo JsonObject_t930_JsonObject_Contains_m4795_ParameterInfos[] = 
{
	{"item", 0, 134219962, 0, &KeyValuePair_2_t1035_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_KeyValuePair_2_t1035 (const MethodInfo* method, void* obj, void** args);
// System.Boolean SimpleJson.JsonObject::Contains(System.Collections.Generic.KeyValuePair`2<System.String,System.Object>)
extern const MethodInfo JsonObject_Contains_m4795_MethodInfo = 
{
	"Contains"/* name */
	, (methodPointerType)&JsonObject_Contains_m4795/* method */
	, &JsonObject_t930_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_KeyValuePair_2_t1035/* invoker_method */
	, JsonObject_t930_JsonObject_Contains_m4795_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1909/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType KeyValuePair_2U5BU5D_t1034_0_0_0;
extern const Il2CppType KeyValuePair_2U5BU5D_t1034_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo JsonObject_t930_JsonObject_CopyTo_m4796_ParameterInfos[] = 
{
	{"array", 0, 134219963, 0, &KeyValuePair_2U5BU5D_t1034_0_0_0},
	{"arrayIndex", 1, 134219964, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void SimpleJson.JsonObject::CopyTo(System.Collections.Generic.KeyValuePair`2<System.String,System.Object>[],System.Int32)
extern const MethodInfo JsonObject_CopyTo_m4796_MethodInfo = 
{
	"CopyTo"/* name */
	, (methodPointerType)&JsonObject_CopyTo_m4796/* method */
	, &JsonObject_t930_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Int32_t253/* invoker_method */
	, JsonObject_t930_JsonObject_CopyTo_m4796_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 18/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1910/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 SimpleJson.JsonObject::get_Count()
extern const MethodInfo JsonObject_get_Count_m4797_MethodInfo = 
{
	"get_Count"/* name */
	, (methodPointerType)&JsonObject_get_Count_m4797/* method */
	, &JsonObject_t930_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1911/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean SimpleJson.JsonObject::get_IsReadOnly()
extern const MethodInfo JsonObject_get_IsReadOnly_m4798_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, (methodPointerType)&JsonObject_get_IsReadOnly_m4798/* method */
	, &JsonObject_t930_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1912/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType KeyValuePair_2_t1035_0_0_0;
static const ParameterInfo JsonObject_t930_JsonObject_Remove_m4799_ParameterInfos[] = 
{
	{"item", 0, 134219965, 0, &KeyValuePair_2_t1035_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_KeyValuePair_2_t1035 (const MethodInfo* method, void* obj, void** args);
// System.Boolean SimpleJson.JsonObject::Remove(System.Collections.Generic.KeyValuePair`2<System.String,System.Object>)
extern const MethodInfo JsonObject_Remove_m4799_MethodInfo = 
{
	"Remove"/* name */
	, (methodPointerType)&JsonObject_Remove_m4799/* method */
	, &JsonObject_t930_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_KeyValuePair_2_t1035/* invoker_method */
	, JsonObject_t930_JsonObject_Remove_m4799_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 19/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1913/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IEnumerator_1_t1036_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Object>> SimpleJson.JsonObject::GetEnumerator()
extern const MethodInfo JsonObject_GetEnumerator_m4800_MethodInfo = 
{
	"GetEnumerator"/* name */
	, (methodPointerType)&JsonObject_GetEnumerator_m4800/* method */
	, &JsonObject_t930_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t1036_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1914/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String SimpleJson.JsonObject::ToString()
extern const MethodInfo JsonObject_ToString_m4801_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&JsonObject_ToString_m4801/* method */
	, &JsonObject_t930_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1915/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* JsonObject_t930_MethodInfos[] =
{
	&JsonObject__ctor_m4784_MethodInfo,
	&JsonObject_System_Collections_IEnumerable_GetEnumerator_m4785_MethodInfo,
	&JsonObject_Add_m4786_MethodInfo,
	&JsonObject_get_Keys_m4787_MethodInfo,
	&JsonObject_Remove_m4788_MethodInfo,
	&JsonObject_TryGetValue_m4789_MethodInfo,
	&JsonObject_get_Values_m4790_MethodInfo,
	&JsonObject_get_Item_m4791_MethodInfo,
	&JsonObject_set_Item_m4792_MethodInfo,
	&JsonObject_Add_m4793_MethodInfo,
	&JsonObject_Clear_m4794_MethodInfo,
	&JsonObject_Contains_m4795_MethodInfo,
	&JsonObject_CopyTo_m4796_MethodInfo,
	&JsonObject_get_Count_m4797_MethodInfo,
	&JsonObject_get_IsReadOnly_m4798_MethodInfo,
	&JsonObject_Remove_m4799_MethodInfo,
	&JsonObject_GetEnumerator_m4800_MethodInfo,
	&JsonObject_ToString_m4801_MethodInfo,
	NULL
};
extern const MethodInfo JsonObject_get_Keys_m4787_MethodInfo;
static const PropertyInfo JsonObject_t930____Keys_PropertyInfo = 
{
	&JsonObject_t930_il2cpp_TypeInfo/* parent */
	, "Keys"/* name */
	, &JsonObject_get_Keys_m4787_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo JsonObject_get_Values_m4790_MethodInfo;
static const PropertyInfo JsonObject_t930____Values_PropertyInfo = 
{
	&JsonObject_t930_il2cpp_TypeInfo/* parent */
	, "Values"/* name */
	, &JsonObject_get_Values_m4790_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo JsonObject_get_Item_m4791_MethodInfo;
extern const MethodInfo JsonObject_set_Item_m4792_MethodInfo;
static const PropertyInfo JsonObject_t930____Item_PropertyInfo = 
{
	&JsonObject_t930_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &JsonObject_get_Item_m4791_MethodInfo/* get */
	, &JsonObject_set_Item_m4792_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo JsonObject_get_Count_m4797_MethodInfo;
static const PropertyInfo JsonObject_t930____Count_PropertyInfo = 
{
	&JsonObject_t930_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &JsonObject_get_Count_m4797_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo JsonObject_get_IsReadOnly_m4798_MethodInfo;
static const PropertyInfo JsonObject_t930____IsReadOnly_PropertyInfo = 
{
	&JsonObject_t930_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &JsonObject_get_IsReadOnly_m4798_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* JsonObject_t930_PropertyInfos[] =
{
	&JsonObject_t930____Keys_PropertyInfo,
	&JsonObject_t930____Values_PropertyInfo,
	&JsonObject_t930____Item_PropertyInfo,
	&JsonObject_t930____Count_PropertyInfo,
	&JsonObject_t930____IsReadOnly_PropertyInfo,
	NULL
};
extern const MethodInfo JsonObject_ToString_m4801_MethodInfo;
extern const MethodInfo JsonObject_Add_m4786_MethodInfo;
extern const MethodInfo JsonObject_Remove_m4788_MethodInfo;
extern const MethodInfo JsonObject_TryGetValue_m4789_MethodInfo;
extern const MethodInfo JsonObject_GetEnumerator_m4800_MethodInfo;
extern const MethodInfo JsonObject_System_Collections_IEnumerable_GetEnumerator_m4785_MethodInfo;
extern const MethodInfo JsonObject_Add_m4793_MethodInfo;
extern const MethodInfo JsonObject_Clear_m4794_MethodInfo;
extern const MethodInfo JsonObject_Contains_m4795_MethodInfo;
extern const MethodInfo JsonObject_CopyTo_m4796_MethodInfo;
extern const MethodInfo JsonObject_Remove_m4799_MethodInfo;
static const Il2CppMethodReference JsonObject_t930_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&JsonObject_ToString_m4801_MethodInfo,
	&JsonObject_Add_m4786_MethodInfo,
	&JsonObject_Remove_m4788_MethodInfo,
	&JsonObject_TryGetValue_m4789_MethodInfo,
	&JsonObject_get_Item_m4791_MethodInfo,
	&JsonObject_set_Item_m4792_MethodInfo,
	&JsonObject_get_Keys_m4787_MethodInfo,
	&JsonObject_get_Values_m4790_MethodInfo,
	&JsonObject_GetEnumerator_m4800_MethodInfo,
	&JsonObject_System_Collections_IEnumerable_GetEnumerator_m4785_MethodInfo,
	&JsonObject_get_Count_m4797_MethodInfo,
	&JsonObject_get_IsReadOnly_m4798_MethodInfo,
	&JsonObject_Add_m4793_MethodInfo,
	&JsonObject_Clear_m4794_MethodInfo,
	&JsonObject_Contains_m4795_MethodInfo,
	&JsonObject_CopyTo_m4796_MethodInfo,
	&JsonObject_Remove_m4799_MethodInfo,
};
static bool JsonObject_t930_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IEnumerable_1_t1198_0_0_0;
extern const Il2CppType ICollection_1_t1199_0_0_0;
static const Il2CppType* JsonObject_t930_InterfacesTypeInfos[] = 
{
	&IDictionary_2_t1027_0_0_0,
	&IEnumerable_1_t1198_0_0_0,
	&IEnumerable_t438_0_0_0,
	&ICollection_1_t1199_0_0_0,
};
static Il2CppInterfaceOffsetPair JsonObject_t930_InterfacesOffsets[] = 
{
	{ &IDictionary_2_t1027_0_0_0, 4},
	{ &IEnumerable_1_t1198_0_0_0, 11},
	{ &IEnumerable_t438_0_0_0, 12},
	{ &ICollection_1_t1199_0_0_0, 13},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType JsonObject_t930_0_0_0;
extern const Il2CppType JsonObject_t930_1_0_0;
struct JsonObject_t930;
const Il2CppTypeDefinitionMetadata JsonObject_t930_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, JsonObject_t930_InterfacesTypeInfos/* implementedInterfaces */
	, JsonObject_t930_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, JsonObject_t930_VTable/* vtableMethods */
	, JsonObject_t930_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1098/* fieldStart */

};
TypeInfo JsonObject_t930_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "JsonObject"/* name */
	, "SimpleJson"/* namespaze */
	, JsonObject_t930_MethodInfos/* methods */
	, JsonObject_t930_PropertyInfos/* properties */
	, NULL/* events */
	, &JsonObject_t930_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 977/* custom_attributes_cache */
	, &JsonObject_t930_0_0_0/* byval_arg */
	, &JsonObject_t930_1_0_0/* this_arg */
	, &JsonObject_t930_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (JsonObject_t930)/* instance_size */
	, sizeof (JsonObject_t930)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 18/* method_count */
	, 5/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 20/* vtable_count */
	, 4/* interfaces_count */
	, 4/* interface_offsets_count */

};
// SimpleJson.SimpleJson
#include "UnityEngine_SimpleJson_SimpleJson.h"
// Metadata Definition SimpleJson.SimpleJson
extern TypeInfo SimpleJson_t933_il2cpp_TypeInfo;
// SimpleJson.SimpleJson
#include "UnityEngine_SimpleJson_SimpleJsonMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_1_0_2;
static const ParameterInfo SimpleJson_t933_SimpleJson_TryDeserializeObject_m4802_ParameterInfos[] = 
{
	{"json", 0, 134219966, 0, &String_t_0_0_0},
	{"obj", 1, 134219967, 0, &Object_t_1_0_2},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t_ObjectU26_t1197 (const MethodInfo* method, void* obj, void** args);
// System.Boolean SimpleJson.SimpleJson::TryDeserializeObject(System.String,System.Object&)
extern const MethodInfo SimpleJson_TryDeserializeObject_m4802_MethodInfo = 
{
	"TryDeserializeObject"/* name */
	, (methodPointerType)&SimpleJson_TryDeserializeObject_m4802/* method */
	, &SimpleJson_t933_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t_ObjectU26_t1197/* invoker_method */
	, SimpleJson_t933_SimpleJson_TryDeserializeObject_m4802_ParameterInfos/* parameters */
	, 979/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1916/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IJsonSerializerStrategy_t931_0_0_0;
extern const Il2CppType IJsonSerializerStrategy_t931_0_0_0;
static const ParameterInfo SimpleJson_t933_SimpleJson_SerializeObject_m4803_ParameterInfos[] = 
{
	{"json", 0, 134219968, 0, &Object_t_0_0_0},
	{"jsonSerializerStrategy", 1, 134219969, 0, &IJsonSerializerStrategy_t931_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String SimpleJson.SimpleJson::SerializeObject(System.Object,SimpleJson.IJsonSerializerStrategy)
extern const MethodInfo SimpleJson_SerializeObject_m4803_MethodInfo = 
{
	"SerializeObject"/* name */
	, (methodPointerType)&SimpleJson_SerializeObject_m4803/* method */
	, &SimpleJson_t933_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, SimpleJson_t933_SimpleJson_SerializeObject_m4803_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1917/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo SimpleJson_t933_SimpleJson_SerializeObject_m4804_ParameterInfos[] = 
{
	{"json", 0, 134219970, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String SimpleJson.SimpleJson::SerializeObject(System.Object)
extern const MethodInfo SimpleJson_SerializeObject_m4804_MethodInfo = 
{
	"SerializeObject"/* name */
	, (methodPointerType)&SimpleJson_SerializeObject_m4804/* method */
	, &SimpleJson_t933_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, SimpleJson_t933_SimpleJson_SerializeObject_m4804_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1918/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType CharU5BU5D_t554_0_0_0;
extern const Il2CppType CharU5BU5D_t554_0_0_0;
extern const Il2CppType Int32_t253_1_0_0;
extern const Il2CppType Int32_t253_1_0_0;
extern const Il2CppType Boolean_t273_1_0_0;
extern const Il2CppType Boolean_t273_1_0_0;
static const ParameterInfo SimpleJson_t933_SimpleJson_ParseObject_m4805_ParameterInfos[] = 
{
	{"json", 0, 134219971, 0, &CharU5BU5D_t554_0_0_0},
	{"index", 1, 134219972, 0, &Int32_t253_1_0_0},
	{"success", 2, 134219973, 0, &Boolean_t273_1_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Int32U26_t753_BooleanU26_t745 (const MethodInfo* method, void* obj, void** args);
// System.Collections.Generic.IDictionary`2<System.String,System.Object> SimpleJson.SimpleJson::ParseObject(System.Char[],System.Int32&,System.Boolean&)
extern const MethodInfo SimpleJson_ParseObject_m4805_MethodInfo = 
{
	"ParseObject"/* name */
	, (methodPointerType)&SimpleJson_ParseObject_m4805/* method */
	, &SimpleJson_t933_il2cpp_TypeInfo/* declaring_type */
	, &IDictionary_2_t1027_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32U26_t753_BooleanU26_t745/* invoker_method */
	, SimpleJson_t933_SimpleJson_ParseObject_m4805_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1919/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType CharU5BU5D_t554_0_0_0;
extern const Il2CppType Int32_t253_1_0_0;
extern const Il2CppType Boolean_t273_1_0_0;
static const ParameterInfo SimpleJson_t933_SimpleJson_ParseArray_m4806_ParameterInfos[] = 
{
	{"json", 0, 134219974, 0, &CharU5BU5D_t554_0_0_0},
	{"index", 1, 134219975, 0, &Int32_t253_1_0_0},
	{"success", 2, 134219976, 0, &Boolean_t273_1_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Int32U26_t753_BooleanU26_t745 (const MethodInfo* method, void* obj, void** args);
// SimpleJson.JsonArray SimpleJson.SimpleJson::ParseArray(System.Char[],System.Int32&,System.Boolean&)
extern const MethodInfo SimpleJson_ParseArray_m4806_MethodInfo = 
{
	"ParseArray"/* name */
	, (methodPointerType)&SimpleJson_ParseArray_m4806/* method */
	, &SimpleJson_t933_il2cpp_TypeInfo/* declaring_type */
	, &JsonArray_t928_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32U26_t753_BooleanU26_t745/* invoker_method */
	, SimpleJson_t933_SimpleJson_ParseArray_m4806_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1920/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType CharU5BU5D_t554_0_0_0;
extern const Il2CppType Int32_t253_1_0_0;
extern const Il2CppType Boolean_t273_1_0_0;
static const ParameterInfo SimpleJson_t933_SimpleJson_ParseValue_m4807_ParameterInfos[] = 
{
	{"json", 0, 134219977, 0, &CharU5BU5D_t554_0_0_0},
	{"index", 1, 134219978, 0, &Int32_t253_1_0_0},
	{"success", 2, 134219979, 0, &Boolean_t273_1_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Int32U26_t753_BooleanU26_t745 (const MethodInfo* method, void* obj, void** args);
// System.Object SimpleJson.SimpleJson::ParseValue(System.Char[],System.Int32&,System.Boolean&)
extern const MethodInfo SimpleJson_ParseValue_m4807_MethodInfo = 
{
	"ParseValue"/* name */
	, (methodPointerType)&SimpleJson_ParseValue_m4807/* method */
	, &SimpleJson_t933_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32U26_t753_BooleanU26_t745/* invoker_method */
	, SimpleJson_t933_SimpleJson_ParseValue_m4807_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1921/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType CharU5BU5D_t554_0_0_0;
extern const Il2CppType Int32_t253_1_0_0;
extern const Il2CppType Boolean_t273_1_0_0;
static const ParameterInfo SimpleJson_t933_SimpleJson_ParseString_m4808_ParameterInfos[] = 
{
	{"json", 0, 134219980, 0, &CharU5BU5D_t554_0_0_0},
	{"index", 1, 134219981, 0, &Int32_t253_1_0_0},
	{"success", 2, 134219982, 0, &Boolean_t273_1_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Int32U26_t753_BooleanU26_t745 (const MethodInfo* method, void* obj, void** args);
// System.String SimpleJson.SimpleJson::ParseString(System.Char[],System.Int32&,System.Boolean&)
extern const MethodInfo SimpleJson_ParseString_m4808_MethodInfo = 
{
	"ParseString"/* name */
	, (methodPointerType)&SimpleJson_ParseString_m4808/* method */
	, &SimpleJson_t933_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32U26_t753_BooleanU26_t745/* invoker_method */
	, SimpleJson_t933_SimpleJson_ParseString_m4808_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1922/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo SimpleJson_t933_SimpleJson_ConvertFromUtf32_m4809_ParameterInfos[] = 
{
	{"utf32", 0, 134219983, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.String SimpleJson.SimpleJson::ConvertFromUtf32(System.Int32)
extern const MethodInfo SimpleJson_ConvertFromUtf32_m4809_MethodInfo = 
{
	"ConvertFromUtf32"/* name */
	, (methodPointerType)&SimpleJson_ConvertFromUtf32_m4809/* method */
	, &SimpleJson_t933_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t253/* invoker_method */
	, SimpleJson_t933_SimpleJson_ConvertFromUtf32_m4809_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1923/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType CharU5BU5D_t554_0_0_0;
extern const Il2CppType Int32_t253_1_0_0;
extern const Il2CppType Boolean_t273_1_0_0;
static const ParameterInfo SimpleJson_t933_SimpleJson_ParseNumber_m4810_ParameterInfos[] = 
{
	{"json", 0, 134219984, 0, &CharU5BU5D_t554_0_0_0},
	{"index", 1, 134219985, 0, &Int32_t253_1_0_0},
	{"success", 2, 134219986, 0, &Boolean_t273_1_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Int32U26_t753_BooleanU26_t745 (const MethodInfo* method, void* obj, void** args);
// System.Object SimpleJson.SimpleJson::ParseNumber(System.Char[],System.Int32&,System.Boolean&)
extern const MethodInfo SimpleJson_ParseNumber_m4810_MethodInfo = 
{
	"ParseNumber"/* name */
	, (methodPointerType)&SimpleJson_ParseNumber_m4810/* method */
	, &SimpleJson_t933_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32U26_t753_BooleanU26_t745/* invoker_method */
	, SimpleJson_t933_SimpleJson_ParseNumber_m4810_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1924/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType CharU5BU5D_t554_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo SimpleJson_t933_SimpleJson_GetLastIndexOfNumber_m4811_ParameterInfos[] = 
{
	{"json", 0, 134219987, 0, &CharU5BU5D_t554_0_0_0},
	{"index", 1, 134219988, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Int32_t253_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 SimpleJson.SimpleJson::GetLastIndexOfNumber(System.Char[],System.Int32)
extern const MethodInfo SimpleJson_GetLastIndexOfNumber_m4811_MethodInfo = 
{
	"GetLastIndexOfNumber"/* name */
	, (methodPointerType)&SimpleJson_GetLastIndexOfNumber_m4811/* method */
	, &SimpleJson_t933_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253_Object_t_Int32_t253/* invoker_method */
	, SimpleJson_t933_SimpleJson_GetLastIndexOfNumber_m4811_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1925/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType CharU5BU5D_t554_0_0_0;
extern const Il2CppType Int32_t253_1_0_0;
static const ParameterInfo SimpleJson_t933_SimpleJson_EatWhitespace_m4812_ParameterInfos[] = 
{
	{"json", 0, 134219989, 0, &CharU5BU5D_t554_0_0_0},
	{"index", 1, 134219990, 0, &Int32_t253_1_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Int32U26_t753 (const MethodInfo* method, void* obj, void** args);
// System.Void SimpleJson.SimpleJson::EatWhitespace(System.Char[],System.Int32&)
extern const MethodInfo SimpleJson_EatWhitespace_m4812_MethodInfo = 
{
	"EatWhitespace"/* name */
	, (methodPointerType)&SimpleJson_EatWhitespace_m4812/* method */
	, &SimpleJson_t933_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Int32U26_t753/* invoker_method */
	, SimpleJson_t933_SimpleJson_EatWhitespace_m4812_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1926/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType CharU5BU5D_t554_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo SimpleJson_t933_SimpleJson_LookAhead_m4813_ParameterInfos[] = 
{
	{"json", 0, 134219991, 0, &CharU5BU5D_t554_0_0_0},
	{"index", 1, 134219992, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Int32_t253_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 SimpleJson.SimpleJson::LookAhead(System.Char[],System.Int32)
extern const MethodInfo SimpleJson_LookAhead_m4813_MethodInfo = 
{
	"LookAhead"/* name */
	, (methodPointerType)&SimpleJson_LookAhead_m4813/* method */
	, &SimpleJson_t933_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253_Object_t_Int32_t253/* invoker_method */
	, SimpleJson_t933_SimpleJson_LookAhead_m4813_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1927/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType CharU5BU5D_t554_0_0_0;
extern const Il2CppType Int32_t253_1_0_0;
static const ParameterInfo SimpleJson_t933_SimpleJson_NextToken_m4814_ParameterInfos[] = 
{
	{"json", 0, 134219993, 0, &CharU5BU5D_t554_0_0_0},
	{"index", 1, 134219994, 0, &Int32_t253_1_0_0},
};
extern void* RuntimeInvoker_Int32_t253_Object_t_Int32U26_t753 (const MethodInfo* method, void* obj, void** args);
// System.Int32 SimpleJson.SimpleJson::NextToken(System.Char[],System.Int32&)
extern const MethodInfo SimpleJson_NextToken_m4814_MethodInfo = 
{
	"NextToken"/* name */
	, (methodPointerType)&SimpleJson_NextToken_m4814/* method */
	, &SimpleJson_t933_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253_Object_t_Int32U26_t753/* invoker_method */
	, SimpleJson_t933_SimpleJson_NextToken_m4814_ParameterInfos/* parameters */
	, 980/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1928/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IJsonSerializerStrategy_t931_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType StringBuilder_t657_0_0_0;
extern const Il2CppType StringBuilder_t657_0_0_0;
static const ParameterInfo SimpleJson_t933_SimpleJson_SerializeValue_m4815_ParameterInfos[] = 
{
	{"jsonSerializerStrategy", 0, 134219995, 0, &IJsonSerializerStrategy_t931_0_0_0},
	{"value", 1, 134219996, 0, &Object_t_0_0_0},
	{"builder", 2, 134219997, 0, &StringBuilder_t657_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean SimpleJson.SimpleJson::SerializeValue(SimpleJson.IJsonSerializerStrategy,System.Object,System.Text.StringBuilder)
extern const MethodInfo SimpleJson_SerializeValue_m4815_MethodInfo = 
{
	"SerializeValue"/* name */
	, (methodPointerType)&SimpleJson_SerializeValue_m4815/* method */
	, &SimpleJson_t933_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t_Object_t_Object_t/* invoker_method */
	, SimpleJson_t933_SimpleJson_SerializeValue_m4815_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1929/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IJsonSerializerStrategy_t931_0_0_0;
extern const Il2CppType IEnumerable_t438_0_0_0;
extern const Il2CppType IEnumerable_t438_0_0_0;
extern const Il2CppType StringBuilder_t657_0_0_0;
static const ParameterInfo SimpleJson_t933_SimpleJson_SerializeObject_m4816_ParameterInfos[] = 
{
	{"jsonSerializerStrategy", 0, 134219998, 0, &IJsonSerializerStrategy_t931_0_0_0},
	{"keys", 1, 134219999, 0, &IEnumerable_t438_0_0_0},
	{"values", 2, 134220000, 0, &IEnumerable_t438_0_0_0},
	{"builder", 3, 134220001, 0, &StringBuilder_t657_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean SimpleJson.SimpleJson::SerializeObject(SimpleJson.IJsonSerializerStrategy,System.Collections.IEnumerable,System.Collections.IEnumerable,System.Text.StringBuilder)
extern const MethodInfo SimpleJson_SerializeObject_m4816_MethodInfo = 
{
	"SerializeObject"/* name */
	, (methodPointerType)&SimpleJson_SerializeObject_m4816/* method */
	, &SimpleJson_t933_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, SimpleJson_t933_SimpleJson_SerializeObject_m4816_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1930/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IJsonSerializerStrategy_t931_0_0_0;
extern const Il2CppType IEnumerable_t438_0_0_0;
extern const Il2CppType StringBuilder_t657_0_0_0;
static const ParameterInfo SimpleJson_t933_SimpleJson_SerializeArray_m4817_ParameterInfos[] = 
{
	{"jsonSerializerStrategy", 0, 134220002, 0, &IJsonSerializerStrategy_t931_0_0_0},
	{"anArray", 1, 134220003, 0, &IEnumerable_t438_0_0_0},
	{"builder", 2, 134220004, 0, &StringBuilder_t657_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean SimpleJson.SimpleJson::SerializeArray(SimpleJson.IJsonSerializerStrategy,System.Collections.IEnumerable,System.Text.StringBuilder)
extern const MethodInfo SimpleJson_SerializeArray_m4817_MethodInfo = 
{
	"SerializeArray"/* name */
	, (methodPointerType)&SimpleJson_SerializeArray_m4817/* method */
	, &SimpleJson_t933_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t_Object_t_Object_t/* invoker_method */
	, SimpleJson_t933_SimpleJson_SerializeArray_m4817_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1931/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType StringBuilder_t657_0_0_0;
static const ParameterInfo SimpleJson_t933_SimpleJson_SerializeString_m4818_ParameterInfos[] = 
{
	{"aString", 0, 134220005, 0, &String_t_0_0_0},
	{"builder", 1, 134220006, 0, &StringBuilder_t657_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean SimpleJson.SimpleJson::SerializeString(System.String,System.Text.StringBuilder)
extern const MethodInfo SimpleJson_SerializeString_m4818_MethodInfo = 
{
	"SerializeString"/* name */
	, (methodPointerType)&SimpleJson_SerializeString_m4818/* method */
	, &SimpleJson_t933_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t_Object_t/* invoker_method */
	, SimpleJson_t933_SimpleJson_SerializeString_m4818_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1932/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType StringBuilder_t657_0_0_0;
static const ParameterInfo SimpleJson_t933_SimpleJson_SerializeNumber_m4819_ParameterInfos[] = 
{
	{"number", 0, 134220007, 0, &Object_t_0_0_0},
	{"builder", 1, 134220008, 0, &StringBuilder_t657_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean SimpleJson.SimpleJson::SerializeNumber(System.Object,System.Text.StringBuilder)
extern const MethodInfo SimpleJson_SerializeNumber_m4819_MethodInfo = 
{
	"SerializeNumber"/* name */
	, (methodPointerType)&SimpleJson_SerializeNumber_m4819/* method */
	, &SimpleJson_t933_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t_Object_t/* invoker_method */
	, SimpleJson_t933_SimpleJson_SerializeNumber_m4819_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1933/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo SimpleJson_t933_SimpleJson_IsNumeric_m4820_ParameterInfos[] = 
{
	{"value", 0, 134220009, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean SimpleJson.SimpleJson::IsNumeric(System.Object)
extern const MethodInfo SimpleJson_IsNumeric_m4820_MethodInfo = 
{
	"IsNumeric"/* name */
	, (methodPointerType)&SimpleJson_IsNumeric_m4820/* method */
	, &SimpleJson_t933_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, SimpleJson_t933_SimpleJson_IsNumeric_m4820_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1934/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// SimpleJson.IJsonSerializerStrategy SimpleJson.SimpleJson::get_CurrentJsonSerializerStrategy()
extern const MethodInfo SimpleJson_get_CurrentJsonSerializerStrategy_m4821_MethodInfo = 
{
	"get_CurrentJsonSerializerStrategy"/* name */
	, (methodPointerType)&SimpleJson_get_CurrentJsonSerializerStrategy_m4821/* method */
	, &SimpleJson_t933_il2cpp_TypeInfo/* declaring_type */
	, &IJsonSerializerStrategy_t931_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1935/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PocoJsonSerializerStrategy_t932_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// SimpleJson.PocoJsonSerializerStrategy SimpleJson.SimpleJson::get_PocoJsonSerializerStrategy()
extern const MethodInfo SimpleJson_get_PocoJsonSerializerStrategy_m4822_MethodInfo = 
{
	"get_PocoJsonSerializerStrategy"/* name */
	, (methodPointerType)&SimpleJson_get_PocoJsonSerializerStrategy_m4822/* method */
	, &SimpleJson_t933_il2cpp_TypeInfo/* declaring_type */
	, &PocoJsonSerializerStrategy_t932_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1936/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SimpleJson_t933_MethodInfos[] =
{
	&SimpleJson_TryDeserializeObject_m4802_MethodInfo,
	&SimpleJson_SerializeObject_m4803_MethodInfo,
	&SimpleJson_SerializeObject_m4804_MethodInfo,
	&SimpleJson_ParseObject_m4805_MethodInfo,
	&SimpleJson_ParseArray_m4806_MethodInfo,
	&SimpleJson_ParseValue_m4807_MethodInfo,
	&SimpleJson_ParseString_m4808_MethodInfo,
	&SimpleJson_ConvertFromUtf32_m4809_MethodInfo,
	&SimpleJson_ParseNumber_m4810_MethodInfo,
	&SimpleJson_GetLastIndexOfNumber_m4811_MethodInfo,
	&SimpleJson_EatWhitespace_m4812_MethodInfo,
	&SimpleJson_LookAhead_m4813_MethodInfo,
	&SimpleJson_NextToken_m4814_MethodInfo,
	&SimpleJson_SerializeValue_m4815_MethodInfo,
	&SimpleJson_SerializeObject_m4816_MethodInfo,
	&SimpleJson_SerializeArray_m4817_MethodInfo,
	&SimpleJson_SerializeString_m4818_MethodInfo,
	&SimpleJson_SerializeNumber_m4819_MethodInfo,
	&SimpleJson_IsNumeric_m4820_MethodInfo,
	&SimpleJson_get_CurrentJsonSerializerStrategy_m4821_MethodInfo,
	&SimpleJson_get_PocoJsonSerializerStrategy_m4822_MethodInfo,
	NULL
};
extern const MethodInfo SimpleJson_get_CurrentJsonSerializerStrategy_m4821_MethodInfo;
static const PropertyInfo SimpleJson_t933____CurrentJsonSerializerStrategy_PropertyInfo = 
{
	&SimpleJson_t933_il2cpp_TypeInfo/* parent */
	, "CurrentJsonSerializerStrategy"/* name */
	, &SimpleJson_get_CurrentJsonSerializerStrategy_m4821_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo SimpleJson_get_PocoJsonSerializerStrategy_m4822_MethodInfo;
static const PropertyInfo SimpleJson_t933____PocoJsonSerializerStrategy_PropertyInfo = 
{
	&SimpleJson_t933_il2cpp_TypeInfo/* parent */
	, "PocoJsonSerializerStrategy"/* name */
	, &SimpleJson_get_PocoJsonSerializerStrategy_m4822_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 981/* custom_attributes_cache */

};
static const PropertyInfo* SimpleJson_t933_PropertyInfos[] =
{
	&SimpleJson_t933____CurrentJsonSerializerStrategy_PropertyInfo,
	&SimpleJson_t933____PocoJsonSerializerStrategy_PropertyInfo,
	NULL
};
static const Il2CppMethodReference SimpleJson_t933_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool SimpleJson_t933_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType SimpleJson_t933_0_0_0;
extern const Il2CppType SimpleJson_t933_1_0_0;
struct SimpleJson_t933;
const Il2CppTypeDefinitionMetadata SimpleJson_t933_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SimpleJson_t933_VTable/* vtableMethods */
	, SimpleJson_t933_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1099/* fieldStart */

};
TypeInfo SimpleJson_t933_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SimpleJson"/* name */
	, "SimpleJson"/* namespaze */
	, SimpleJson_t933_MethodInfos/* methods */
	, SimpleJson_t933_PropertyInfos/* properties */
	, NULL/* events */
	, &SimpleJson_t933_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 978/* custom_attributes_cache */
	, &SimpleJson_t933_0_0_0/* byval_arg */
	, &SimpleJson_t933_1_0_0/* this_arg */
	, &SimpleJson_t933_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SimpleJson_t933)/* instance_size */
	, sizeof (SimpleJson_t933)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(SimpleJson_t933_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048960/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 21/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition SimpleJson.IJsonSerializerStrategy
extern TypeInfo IJsonSerializerStrategy_t931_il2cpp_TypeInfo;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_1_0_2;
static const ParameterInfo IJsonSerializerStrategy_t931_IJsonSerializerStrategy_TrySerializeNonPrimitiveObject_m5298_ParameterInfos[] = 
{
	{"input", 0, 134220010, 0, &Object_t_0_0_0},
	{"output", 1, 134220011, 0, &Object_t_1_0_2},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t_ObjectU26_t1197 (const MethodInfo* method, void* obj, void** args);
// System.Boolean SimpleJson.IJsonSerializerStrategy::TrySerializeNonPrimitiveObject(System.Object,System.Object&)
extern const MethodInfo IJsonSerializerStrategy_TrySerializeNonPrimitiveObject_m5298_MethodInfo = 
{
	"TrySerializeNonPrimitiveObject"/* name */
	, NULL/* method */
	, &IJsonSerializerStrategy_t931_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t_ObjectU26_t1197/* invoker_method */
	, IJsonSerializerStrategy_t931_IJsonSerializerStrategy_TrySerializeNonPrimitiveObject_m5298_ParameterInfos/* parameters */
	, 983/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1937/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IJsonSerializerStrategy_t931_MethodInfos[] =
{
	&IJsonSerializerStrategy_TrySerializeNonPrimitiveObject_m5298_MethodInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType IJsonSerializerStrategy_t931_1_0_0;
struct IJsonSerializerStrategy_t931;
const Il2CppTypeDefinitionMetadata IJsonSerializerStrategy_t931_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IJsonSerializerStrategy_t931_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "IJsonSerializerStrategy"/* name */
	, "SimpleJson"/* namespaze */
	, IJsonSerializerStrategy_t931_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IJsonSerializerStrategy_t931_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 982/* custom_attributes_cache */
	, &IJsonSerializerStrategy_t931_0_0_0/* byval_arg */
	, &IJsonSerializerStrategy_t931_1_0_0/* this_arg */
	, &IJsonSerializerStrategy_t931_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 160/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SimpleJson.PocoJsonSerializerStrategy
#include "UnityEngine_SimpleJson_PocoJsonSerializerStrategy.h"
// Metadata Definition SimpleJson.PocoJsonSerializerStrategy
extern TypeInfo PocoJsonSerializerStrategy_t932_il2cpp_TypeInfo;
// SimpleJson.PocoJsonSerializerStrategy
#include "UnityEngine_SimpleJson_PocoJsonSerializerStrategyMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SimpleJson.PocoJsonSerializerStrategy::.ctor()
extern const MethodInfo PocoJsonSerializerStrategy__ctor_m4823_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PocoJsonSerializerStrategy__ctor_m4823/* method */
	, &PocoJsonSerializerStrategy_t932_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1938/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SimpleJson.PocoJsonSerializerStrategy::.cctor()
extern const MethodInfo PocoJsonSerializerStrategy__cctor_m4824_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&PocoJsonSerializerStrategy__cctor_m4824/* method */
	, &PocoJsonSerializerStrategy_t932_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1939/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo PocoJsonSerializerStrategy_t932_PocoJsonSerializerStrategy_MapClrMemberNameToJsonFieldName_m4825_ParameterInfos[] = 
{
	{"clrPropertyName", 0, 134220012, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String SimpleJson.PocoJsonSerializerStrategy::MapClrMemberNameToJsonFieldName(System.String)
extern const MethodInfo PocoJsonSerializerStrategy_MapClrMemberNameToJsonFieldName_m4825_MethodInfo = 
{
	"MapClrMemberNameToJsonFieldName"/* name */
	, (methodPointerType)&PocoJsonSerializerStrategy_MapClrMemberNameToJsonFieldName_m4825/* method */
	, &PocoJsonSerializerStrategy_t932_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, PocoJsonSerializerStrategy_t932_PocoJsonSerializerStrategy_MapClrMemberNameToJsonFieldName_m4825_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1940/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo PocoJsonSerializerStrategy_t932_PocoJsonSerializerStrategy_ContructorDelegateFactory_m4826_ParameterInfos[] = 
{
	{"key", 0, 134220013, 0, &Type_t_0_0_0},
};
extern const Il2CppType ConstructorDelegate_t939_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate SimpleJson.PocoJsonSerializerStrategy::ContructorDelegateFactory(System.Type)
extern const MethodInfo PocoJsonSerializerStrategy_ContructorDelegateFactory_m4826_MethodInfo = 
{
	"ContructorDelegateFactory"/* name */
	, (methodPointerType)&PocoJsonSerializerStrategy_ContructorDelegateFactory_m4826/* method */
	, &PocoJsonSerializerStrategy_t932_il2cpp_TypeInfo/* declaring_type */
	, &ConstructorDelegate_t939_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, PocoJsonSerializerStrategy_t932_PocoJsonSerializerStrategy_ContructorDelegateFactory_m4826_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 451/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1941/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo PocoJsonSerializerStrategy_t932_PocoJsonSerializerStrategy_GetterValueFactory_m4827_ParameterInfos[] = 
{
	{"type", 0, 134220014, 0, &Type_t_0_0_0},
};
extern const Il2CppType IDictionary_2_t1037_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.Generic.IDictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate> SimpleJson.PocoJsonSerializerStrategy::GetterValueFactory(System.Type)
extern const MethodInfo PocoJsonSerializerStrategy_GetterValueFactory_m4827_MethodInfo = 
{
	"GetterValueFactory"/* name */
	, (methodPointerType)&PocoJsonSerializerStrategy_GetterValueFactory_m4827/* method */
	, &PocoJsonSerializerStrategy_t932_il2cpp_TypeInfo/* declaring_type */
	, &IDictionary_2_t1037_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, PocoJsonSerializerStrategy_t932_PocoJsonSerializerStrategy_GetterValueFactory_m4827_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 451/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1942/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo PocoJsonSerializerStrategy_t932_PocoJsonSerializerStrategy_SetterValueFactory_m4828_ParameterInfos[] = 
{
	{"type", 0, 134220015, 0, &Type_t_0_0_0},
};
extern const Il2CppType IDictionary_2_t1038_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>> SimpleJson.PocoJsonSerializerStrategy::SetterValueFactory(System.Type)
extern const MethodInfo PocoJsonSerializerStrategy_SetterValueFactory_m4828_MethodInfo = 
{
	"SetterValueFactory"/* name */
	, (methodPointerType)&PocoJsonSerializerStrategy_SetterValueFactory_m4828/* method */
	, &PocoJsonSerializerStrategy_t932_il2cpp_TypeInfo/* declaring_type */
	, &IDictionary_2_t1038_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, PocoJsonSerializerStrategy_t932_PocoJsonSerializerStrategy_SetterValueFactory_m4828_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 451/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1943/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_1_0_2;
static const ParameterInfo PocoJsonSerializerStrategy_t932_PocoJsonSerializerStrategy_TrySerializeNonPrimitiveObject_m4829_ParameterInfos[] = 
{
	{"input", 0, 134220016, 0, &Object_t_0_0_0},
	{"output", 1, 134220017, 0, &Object_t_1_0_2},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t_ObjectU26_t1197 (const MethodInfo* method, void* obj, void** args);
// System.Boolean SimpleJson.PocoJsonSerializerStrategy::TrySerializeNonPrimitiveObject(System.Object,System.Object&)
extern const MethodInfo PocoJsonSerializerStrategy_TrySerializeNonPrimitiveObject_m4829_MethodInfo = 
{
	"TrySerializeNonPrimitiveObject"/* name */
	, (methodPointerType)&PocoJsonSerializerStrategy_TrySerializeNonPrimitiveObject_m4829/* method */
	, &PocoJsonSerializerStrategy_t932_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t_ObjectU26_t1197/* invoker_method */
	, PocoJsonSerializerStrategy_t932_PocoJsonSerializerStrategy_TrySerializeNonPrimitiveObject_m4829_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1944/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Enum_t278_0_0_0;
static const ParameterInfo PocoJsonSerializerStrategy_t932_PocoJsonSerializerStrategy_SerializeEnum_m4830_ParameterInfos[] = 
{
	{"p", 0, 134220018, 0, &Enum_t278_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object SimpleJson.PocoJsonSerializerStrategy::SerializeEnum(System.Enum)
extern const MethodInfo PocoJsonSerializerStrategy_SerializeEnum_m4830_MethodInfo = 
{
	"SerializeEnum"/* name */
	, (methodPointerType)&PocoJsonSerializerStrategy_SerializeEnum_m4830/* method */
	, &PocoJsonSerializerStrategy_t932_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, PocoJsonSerializerStrategy_t932_PocoJsonSerializerStrategy_SerializeEnum_m4830_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1945/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_1_0_2;
static const ParameterInfo PocoJsonSerializerStrategy_t932_PocoJsonSerializerStrategy_TrySerializeKnownTypes_m4831_ParameterInfos[] = 
{
	{"input", 0, 134220019, 0, &Object_t_0_0_0},
	{"output", 1, 134220020, 0, &Object_t_1_0_2},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t_ObjectU26_t1197 (const MethodInfo* method, void* obj, void** args);
// System.Boolean SimpleJson.PocoJsonSerializerStrategy::TrySerializeKnownTypes(System.Object,System.Object&)
extern const MethodInfo PocoJsonSerializerStrategy_TrySerializeKnownTypes_m4831_MethodInfo = 
{
	"TrySerializeKnownTypes"/* name */
	, (methodPointerType)&PocoJsonSerializerStrategy_TrySerializeKnownTypes_m4831/* method */
	, &PocoJsonSerializerStrategy_t932_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t_ObjectU26_t1197/* invoker_method */
	, PocoJsonSerializerStrategy_t932_PocoJsonSerializerStrategy_TrySerializeKnownTypes_m4831_ParameterInfos/* parameters */
	, 985/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1946/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_1_0_2;
static const ParameterInfo PocoJsonSerializerStrategy_t932_PocoJsonSerializerStrategy_TrySerializeUnknownTypes_m4832_ParameterInfos[] = 
{
	{"input", 0, 134220021, 0, &Object_t_0_0_0},
	{"output", 1, 134220022, 0, &Object_t_1_0_2},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t_ObjectU26_t1197 (const MethodInfo* method, void* obj, void** args);
// System.Boolean SimpleJson.PocoJsonSerializerStrategy::TrySerializeUnknownTypes(System.Object,System.Object&)
extern const MethodInfo PocoJsonSerializerStrategy_TrySerializeUnknownTypes_m4832_MethodInfo = 
{
	"TrySerializeUnknownTypes"/* name */
	, (methodPointerType)&PocoJsonSerializerStrategy_TrySerializeUnknownTypes_m4832/* method */
	, &PocoJsonSerializerStrategy_t932_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t_ObjectU26_t1197/* invoker_method */
	, PocoJsonSerializerStrategy_t932_PocoJsonSerializerStrategy_TrySerializeUnknownTypes_m4832_ParameterInfos/* parameters */
	, 986/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1947/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* PocoJsonSerializerStrategy_t932_MethodInfos[] =
{
	&PocoJsonSerializerStrategy__ctor_m4823_MethodInfo,
	&PocoJsonSerializerStrategy__cctor_m4824_MethodInfo,
	&PocoJsonSerializerStrategy_MapClrMemberNameToJsonFieldName_m4825_MethodInfo,
	&PocoJsonSerializerStrategy_ContructorDelegateFactory_m4826_MethodInfo,
	&PocoJsonSerializerStrategy_GetterValueFactory_m4827_MethodInfo,
	&PocoJsonSerializerStrategy_SetterValueFactory_m4828_MethodInfo,
	&PocoJsonSerializerStrategy_TrySerializeNonPrimitiveObject_m4829_MethodInfo,
	&PocoJsonSerializerStrategy_SerializeEnum_m4830_MethodInfo,
	&PocoJsonSerializerStrategy_TrySerializeKnownTypes_m4831_MethodInfo,
	&PocoJsonSerializerStrategy_TrySerializeUnknownTypes_m4832_MethodInfo,
	NULL
};
extern const MethodInfo PocoJsonSerializerStrategy_TrySerializeNonPrimitiveObject_m4829_MethodInfo;
extern const MethodInfo PocoJsonSerializerStrategy_MapClrMemberNameToJsonFieldName_m4825_MethodInfo;
extern const MethodInfo PocoJsonSerializerStrategy_ContructorDelegateFactory_m4826_MethodInfo;
extern const MethodInfo PocoJsonSerializerStrategy_GetterValueFactory_m4827_MethodInfo;
extern const MethodInfo PocoJsonSerializerStrategy_SetterValueFactory_m4828_MethodInfo;
extern const MethodInfo PocoJsonSerializerStrategy_SerializeEnum_m4830_MethodInfo;
extern const MethodInfo PocoJsonSerializerStrategy_TrySerializeKnownTypes_m4831_MethodInfo;
extern const MethodInfo PocoJsonSerializerStrategy_TrySerializeUnknownTypes_m4832_MethodInfo;
static const Il2CppMethodReference PocoJsonSerializerStrategy_t932_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&PocoJsonSerializerStrategy_TrySerializeNonPrimitiveObject_m4829_MethodInfo,
	&PocoJsonSerializerStrategy_MapClrMemberNameToJsonFieldName_m4825_MethodInfo,
	&PocoJsonSerializerStrategy_ContructorDelegateFactory_m4826_MethodInfo,
	&PocoJsonSerializerStrategy_GetterValueFactory_m4827_MethodInfo,
	&PocoJsonSerializerStrategy_SetterValueFactory_m4828_MethodInfo,
	&PocoJsonSerializerStrategy_TrySerializeNonPrimitiveObject_m4829_MethodInfo,
	&PocoJsonSerializerStrategy_SerializeEnum_m4830_MethodInfo,
	&PocoJsonSerializerStrategy_TrySerializeKnownTypes_m4831_MethodInfo,
	&PocoJsonSerializerStrategy_TrySerializeUnknownTypes_m4832_MethodInfo,
};
static bool PocoJsonSerializerStrategy_t932_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* PocoJsonSerializerStrategy_t932_InterfacesTypeInfos[] = 
{
	&IJsonSerializerStrategy_t931_0_0_0,
};
static Il2CppInterfaceOffsetPair PocoJsonSerializerStrategy_t932_InterfacesOffsets[] = 
{
	{ &IJsonSerializerStrategy_t931_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType PocoJsonSerializerStrategy_t932_1_0_0;
struct PocoJsonSerializerStrategy_t932;
const Il2CppTypeDefinitionMetadata PocoJsonSerializerStrategy_t932_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, PocoJsonSerializerStrategy_t932_InterfacesTypeInfos/* implementedInterfaces */
	, PocoJsonSerializerStrategy_t932_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, PocoJsonSerializerStrategy_t932_VTable/* vtableMethods */
	, PocoJsonSerializerStrategy_t932_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1101/* fieldStart */

};
TypeInfo PocoJsonSerializerStrategy_t932_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "PocoJsonSerializerStrategy"/* name */
	, "SimpleJson"/* namespaze */
	, PocoJsonSerializerStrategy_t932_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &PocoJsonSerializerStrategy_t932_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 984/* custom_attributes_cache */
	, &PocoJsonSerializerStrategy_t932_0_0_0/* byval_arg */
	, &PocoJsonSerializerStrategy_t932_1_0_0/* this_arg */
	, &PocoJsonSerializerStrategy_t932_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PocoJsonSerializerStrategy_t932)/* instance_size */
	, sizeof (PocoJsonSerializerStrategy_t932)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(PocoJsonSerializerStrategy_t932_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2
extern TypeInfo ThreadSafeDictionary_2_t1121_il2cpp_TypeInfo;
extern const Il2CppGenericContainer ThreadSafeDictionary_2_t1121_Il2CppGenericContainer;
extern TypeInfo ThreadSafeDictionary_2_t1121_gp_TKey_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter ThreadSafeDictionary_2_t1121_gp_TKey_0_il2cpp_TypeInfo_GenericParamFull = { &ThreadSafeDictionary_2_t1121_Il2CppGenericContainer, NULL, "TKey", 0, 0 };
extern TypeInfo ThreadSafeDictionary_2_t1121_gp_TValue_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter ThreadSafeDictionary_2_t1121_gp_TValue_1_il2cpp_TypeInfo_GenericParamFull = { &ThreadSafeDictionary_2_t1121_Il2CppGenericContainer, NULL, "TValue", 1, 0 };
static const Il2CppGenericParameter* ThreadSafeDictionary_2_t1121_Il2CppGenericParametersArray[2] = 
{
	&ThreadSafeDictionary_2_t1121_gp_TKey_0_il2cpp_TypeInfo_GenericParamFull,
	&ThreadSafeDictionary_2_t1121_gp_TValue_1_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer ThreadSafeDictionary_2_t1121_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&ThreadSafeDictionary_2_t1121_il2cpp_TypeInfo, 2, 0, ThreadSafeDictionary_2_t1121_Il2CppGenericParametersArray };
extern const Il2CppType ThreadSafeDictionaryValueFactory_2_t1202_0_0_0;
extern const Il2CppType ThreadSafeDictionaryValueFactory_2_t1202_0_0_0;
static const ParameterInfo ThreadSafeDictionary_2_t1121_ThreadSafeDictionary_2__ctor_m5299_ParameterInfos[] = 
{
	{"valueFactory", 0, 134220043, 0, &ThreadSafeDictionaryValueFactory_2_t1202_0_0_0},
};
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2::.ctor(SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<TKey,TValue>)
extern const MethodInfo ThreadSafeDictionary_2__ctor_m5299_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &ThreadSafeDictionary_2_t1121_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ThreadSafeDictionary_2_t1121_ThreadSafeDictionary_2__ctor_m5299_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1966/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Collections.IEnumerator SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2::System.Collections.IEnumerable.GetEnumerator()
extern const MethodInfo ThreadSafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m5300_MethodInfo = 
{
	"System.Collections.IEnumerable.GetEnumerator"/* name */
	, NULL/* method */
	, &ThreadSafeDictionary_2_t1121_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t217_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1967/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ThreadSafeDictionary_2_t1121_gp_0_0_0_0;
extern const Il2CppType ThreadSafeDictionary_2_t1121_gp_0_0_0_0;
static const ParameterInfo ThreadSafeDictionary_2_t1121_ThreadSafeDictionary_2_Get_m5301_ParameterInfos[] = 
{
	{"key", 0, 134220044, 0, &ThreadSafeDictionary_2_t1121_gp_0_0_0_0},
};
extern const Il2CppType ThreadSafeDictionary_2_t1121_gp_1_0_0_0;
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2::Get(TKey)
extern const MethodInfo ThreadSafeDictionary_2_Get_m5301_MethodInfo = 
{
	"Get"/* name */
	, NULL/* method */
	, &ThreadSafeDictionary_2_t1121_il2cpp_TypeInfo/* declaring_type */
	, &ThreadSafeDictionary_2_t1121_gp_1_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ThreadSafeDictionary_2_t1121_ThreadSafeDictionary_2_Get_m5301_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1968/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ThreadSafeDictionary_2_t1121_gp_0_0_0_0;
static const ParameterInfo ThreadSafeDictionary_2_t1121_ThreadSafeDictionary_2_AddValue_m5302_ParameterInfos[] = 
{
	{"key", 0, 134220045, 0, &ThreadSafeDictionary_2_t1121_gp_0_0_0_0},
};
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2::AddValue(TKey)
extern const MethodInfo ThreadSafeDictionary_2_AddValue_m5302_MethodInfo = 
{
	"AddValue"/* name */
	, NULL/* method */
	, &ThreadSafeDictionary_2_t1121_il2cpp_TypeInfo/* declaring_type */
	, &ThreadSafeDictionary_2_t1121_gp_1_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ThreadSafeDictionary_2_t1121_ThreadSafeDictionary_2_AddValue_m5302_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1969/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ThreadSafeDictionary_2_t1121_gp_0_0_0_0;
extern const Il2CppType ThreadSafeDictionary_2_t1121_gp_1_0_0_0;
static const ParameterInfo ThreadSafeDictionary_2_t1121_ThreadSafeDictionary_2_Add_m5303_ParameterInfos[] = 
{
	{"key", 0, 134220046, 0, &ThreadSafeDictionary_2_t1121_gp_0_0_0_0},
	{"value", 1, 134220047, 0, &ThreadSafeDictionary_2_t1121_gp_1_0_0_0},
};
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2::Add(TKey,TValue)
extern const MethodInfo ThreadSafeDictionary_2_Add_m5303_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ThreadSafeDictionary_2_t1121_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ThreadSafeDictionary_2_t1121_ThreadSafeDictionary_2_Add_m5303_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1970/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICollection_1_t1203_0_0_0;
// System.Collections.Generic.ICollection`1<TKey> SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2::get_Keys()
extern const MethodInfo ThreadSafeDictionary_2_get_Keys_m5304_MethodInfo = 
{
	"get_Keys"/* name */
	, NULL/* method */
	, &ThreadSafeDictionary_2_t1121_il2cpp_TypeInfo/* declaring_type */
	, &ICollection_1_t1203_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1971/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ThreadSafeDictionary_2_t1121_gp_0_0_0_0;
static const ParameterInfo ThreadSafeDictionary_2_t1121_ThreadSafeDictionary_2_Remove_m5305_ParameterInfos[] = 
{
	{"key", 0, 134220048, 0, &ThreadSafeDictionary_2_t1121_gp_0_0_0_0},
};
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2::Remove(TKey)
extern const MethodInfo ThreadSafeDictionary_2_Remove_m5305_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ThreadSafeDictionary_2_t1121_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ThreadSafeDictionary_2_t1121_ThreadSafeDictionary_2_Remove_m5305_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1972/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ThreadSafeDictionary_2_t1121_gp_0_0_0_0;
extern const Il2CppType ThreadSafeDictionary_2_t1121_gp_1_1_0_2;
extern const Il2CppType ThreadSafeDictionary_2_t1121_gp_1_1_0_0;
static const ParameterInfo ThreadSafeDictionary_2_t1121_ThreadSafeDictionary_2_TryGetValue_m5306_ParameterInfos[] = 
{
	{"key", 0, 134220049, 0, &ThreadSafeDictionary_2_t1121_gp_0_0_0_0},
	{"value", 1, 134220050, 0, &ThreadSafeDictionary_2_t1121_gp_1_1_0_2},
};
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2::TryGetValue(TKey,TValue&)
extern const MethodInfo ThreadSafeDictionary_2_TryGetValue_m5306_MethodInfo = 
{
	"TryGetValue"/* name */
	, NULL/* method */
	, &ThreadSafeDictionary_2_t1121_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ThreadSafeDictionary_2_t1121_ThreadSafeDictionary_2_TryGetValue_m5306_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1973/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICollection_1_t1204_0_0_0;
// System.Collections.Generic.ICollection`1<TValue> SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2::get_Values()
extern const MethodInfo ThreadSafeDictionary_2_get_Values_m5307_MethodInfo = 
{
	"get_Values"/* name */
	, NULL/* method */
	, &ThreadSafeDictionary_2_t1121_il2cpp_TypeInfo/* declaring_type */
	, &ICollection_1_t1204_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1974/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ThreadSafeDictionary_2_t1121_gp_0_0_0_0;
static const ParameterInfo ThreadSafeDictionary_2_t1121_ThreadSafeDictionary_2_get_Item_m5308_ParameterInfos[] = 
{
	{"key", 0, 134220051, 0, &ThreadSafeDictionary_2_t1121_gp_0_0_0_0},
};
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2::get_Item(TKey)
extern const MethodInfo ThreadSafeDictionary_2_get_Item_m5308_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &ThreadSafeDictionary_2_t1121_il2cpp_TypeInfo/* declaring_type */
	, &ThreadSafeDictionary_2_t1121_gp_1_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ThreadSafeDictionary_2_t1121_ThreadSafeDictionary_2_get_Item_m5308_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1975/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ThreadSafeDictionary_2_t1121_gp_0_0_0_0;
extern const Il2CppType ThreadSafeDictionary_2_t1121_gp_1_0_0_0;
static const ParameterInfo ThreadSafeDictionary_2_t1121_ThreadSafeDictionary_2_set_Item_m5309_ParameterInfos[] = 
{
	{"key", 0, 134220052, 0, &ThreadSafeDictionary_2_t1121_gp_0_0_0_0},
	{"value", 1, 134220053, 0, &ThreadSafeDictionary_2_t1121_gp_1_0_0_0},
};
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2::set_Item(TKey,TValue)
extern const MethodInfo ThreadSafeDictionary_2_set_Item_m5309_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &ThreadSafeDictionary_2_t1121_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ThreadSafeDictionary_2_t1121_ThreadSafeDictionary_2_set_Item_m5309_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1976/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType KeyValuePair_2_t1205_0_0_0;
extern const Il2CppType KeyValuePair_2_t1205_0_0_0;
static const ParameterInfo ThreadSafeDictionary_2_t1121_ThreadSafeDictionary_2_Add_m5310_ParameterInfos[] = 
{
	{"item", 0, 134220054, 0, &KeyValuePair_2_t1205_0_0_0},
};
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2::Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern const MethodInfo ThreadSafeDictionary_2_Add_m5310_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ThreadSafeDictionary_2_t1121_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ThreadSafeDictionary_2_t1121_ThreadSafeDictionary_2_Add_m5310_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1977/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2::Clear()
extern const MethodInfo ThreadSafeDictionary_2_Clear_m5311_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ThreadSafeDictionary_2_t1121_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1978/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType KeyValuePair_2_t1205_0_0_0;
static const ParameterInfo ThreadSafeDictionary_2_t1121_ThreadSafeDictionary_2_Contains_m5312_ParameterInfos[] = 
{
	{"item", 0, 134220055, 0, &KeyValuePair_2_t1205_0_0_0},
};
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2::Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern const MethodInfo ThreadSafeDictionary_2_Contains_m5312_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ThreadSafeDictionary_2_t1121_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ThreadSafeDictionary_2_t1121_ThreadSafeDictionary_2_Contains_m5312_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1979/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType KeyValuePair_2U5BU5D_t1206_0_0_0;
extern const Il2CppType KeyValuePair_2U5BU5D_t1206_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo ThreadSafeDictionary_2_t1121_ThreadSafeDictionary_2_CopyTo_m5313_ParameterInfos[] = 
{
	{"array", 0, 134220056, 0, &KeyValuePair_2U5BU5D_t1206_0_0_0},
	{"arrayIndex", 1, 134220057, 0, &Int32_t253_0_0_0},
};
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern const MethodInfo ThreadSafeDictionary_2_CopyTo_m5313_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ThreadSafeDictionary_2_t1121_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ThreadSafeDictionary_2_t1121_ThreadSafeDictionary_2_CopyTo_m5313_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1980/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Int32 SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2::get_Count()
extern const MethodInfo ThreadSafeDictionary_2_get_Count_m5314_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ThreadSafeDictionary_2_t1121_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1981/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2::get_IsReadOnly()
extern const MethodInfo ThreadSafeDictionary_2_get_IsReadOnly_m5315_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ThreadSafeDictionary_2_t1121_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1982/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType KeyValuePair_2_t1205_0_0_0;
static const ParameterInfo ThreadSafeDictionary_2_t1121_ThreadSafeDictionary_2_Remove_m5316_ParameterInfos[] = 
{
	{"item", 0, 134220058, 0, &KeyValuePair_2_t1205_0_0_0},
};
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2::Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern const MethodInfo ThreadSafeDictionary_2_Remove_m5316_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ThreadSafeDictionary_2_t1121_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ThreadSafeDictionary_2_t1121_ThreadSafeDictionary_2_Remove_m5316_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 18/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1983/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IEnumerator_1_t1207_0_0_0;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2::GetEnumerator()
extern const MethodInfo ThreadSafeDictionary_2_GetEnumerator_m5317_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &ThreadSafeDictionary_2_t1121_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t1207_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 19/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1984/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ThreadSafeDictionary_2_t1121_MethodInfos[] =
{
	&ThreadSafeDictionary_2__ctor_m5299_MethodInfo,
	&ThreadSafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m5300_MethodInfo,
	&ThreadSafeDictionary_2_Get_m5301_MethodInfo,
	&ThreadSafeDictionary_2_AddValue_m5302_MethodInfo,
	&ThreadSafeDictionary_2_Add_m5303_MethodInfo,
	&ThreadSafeDictionary_2_get_Keys_m5304_MethodInfo,
	&ThreadSafeDictionary_2_Remove_m5305_MethodInfo,
	&ThreadSafeDictionary_2_TryGetValue_m5306_MethodInfo,
	&ThreadSafeDictionary_2_get_Values_m5307_MethodInfo,
	&ThreadSafeDictionary_2_get_Item_m5308_MethodInfo,
	&ThreadSafeDictionary_2_set_Item_m5309_MethodInfo,
	&ThreadSafeDictionary_2_Add_m5310_MethodInfo,
	&ThreadSafeDictionary_2_Clear_m5311_MethodInfo,
	&ThreadSafeDictionary_2_Contains_m5312_MethodInfo,
	&ThreadSafeDictionary_2_CopyTo_m5313_MethodInfo,
	&ThreadSafeDictionary_2_get_Count_m5314_MethodInfo,
	&ThreadSafeDictionary_2_get_IsReadOnly_m5315_MethodInfo,
	&ThreadSafeDictionary_2_Remove_m5316_MethodInfo,
	&ThreadSafeDictionary_2_GetEnumerator_m5317_MethodInfo,
	NULL
};
extern const MethodInfo ThreadSafeDictionary_2_get_Keys_m5304_MethodInfo;
static const PropertyInfo ThreadSafeDictionary_2_t1121____Keys_PropertyInfo = 
{
	&ThreadSafeDictionary_2_t1121_il2cpp_TypeInfo/* parent */
	, "Keys"/* name */
	, &ThreadSafeDictionary_2_get_Keys_m5304_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ThreadSafeDictionary_2_get_Values_m5307_MethodInfo;
static const PropertyInfo ThreadSafeDictionary_2_t1121____Values_PropertyInfo = 
{
	&ThreadSafeDictionary_2_t1121_il2cpp_TypeInfo/* parent */
	, "Values"/* name */
	, &ThreadSafeDictionary_2_get_Values_m5307_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ThreadSafeDictionary_2_get_Item_m5308_MethodInfo;
extern const MethodInfo ThreadSafeDictionary_2_set_Item_m5309_MethodInfo;
static const PropertyInfo ThreadSafeDictionary_2_t1121____Item_PropertyInfo = 
{
	&ThreadSafeDictionary_2_t1121_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &ThreadSafeDictionary_2_get_Item_m5308_MethodInfo/* get */
	, &ThreadSafeDictionary_2_set_Item_m5309_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ThreadSafeDictionary_2_get_Count_m5314_MethodInfo;
static const PropertyInfo ThreadSafeDictionary_2_t1121____Count_PropertyInfo = 
{
	&ThreadSafeDictionary_2_t1121_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ThreadSafeDictionary_2_get_Count_m5314_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ThreadSafeDictionary_2_get_IsReadOnly_m5315_MethodInfo;
static const PropertyInfo ThreadSafeDictionary_2_t1121____IsReadOnly_PropertyInfo = 
{
	&ThreadSafeDictionary_2_t1121_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ThreadSafeDictionary_2_get_IsReadOnly_m5315_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ThreadSafeDictionary_2_t1121_PropertyInfos[] =
{
	&ThreadSafeDictionary_2_t1121____Keys_PropertyInfo,
	&ThreadSafeDictionary_2_t1121____Values_PropertyInfo,
	&ThreadSafeDictionary_2_t1121____Item_PropertyInfo,
	&ThreadSafeDictionary_2_t1121____Count_PropertyInfo,
	&ThreadSafeDictionary_2_t1121____IsReadOnly_PropertyInfo,
	NULL
};
extern const MethodInfo ThreadSafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m5300_MethodInfo;
extern const MethodInfo ThreadSafeDictionary_2_Add_m5303_MethodInfo;
extern const MethodInfo ThreadSafeDictionary_2_Remove_m5305_MethodInfo;
extern const MethodInfo ThreadSafeDictionary_2_TryGetValue_m5306_MethodInfo;
extern const MethodInfo ThreadSafeDictionary_2_Add_m5310_MethodInfo;
extern const MethodInfo ThreadSafeDictionary_2_Clear_m5311_MethodInfo;
extern const MethodInfo ThreadSafeDictionary_2_Contains_m5312_MethodInfo;
extern const MethodInfo ThreadSafeDictionary_2_CopyTo_m5313_MethodInfo;
extern const MethodInfo ThreadSafeDictionary_2_Remove_m5316_MethodInfo;
extern const MethodInfo ThreadSafeDictionary_2_GetEnumerator_m5317_MethodInfo;
static const Il2CppMethodReference ThreadSafeDictionary_2_t1121_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&ThreadSafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m5300_MethodInfo,
	&ThreadSafeDictionary_2_Add_m5303_MethodInfo,
	&ThreadSafeDictionary_2_Remove_m5305_MethodInfo,
	&ThreadSafeDictionary_2_TryGetValue_m5306_MethodInfo,
	&ThreadSafeDictionary_2_get_Item_m5308_MethodInfo,
	&ThreadSafeDictionary_2_set_Item_m5309_MethodInfo,
	&ThreadSafeDictionary_2_get_Keys_m5304_MethodInfo,
	&ThreadSafeDictionary_2_get_Values_m5307_MethodInfo,
	&ThreadSafeDictionary_2_get_Count_m5314_MethodInfo,
	&ThreadSafeDictionary_2_get_IsReadOnly_m5315_MethodInfo,
	&ThreadSafeDictionary_2_Add_m5310_MethodInfo,
	&ThreadSafeDictionary_2_Clear_m5311_MethodInfo,
	&ThreadSafeDictionary_2_Contains_m5312_MethodInfo,
	&ThreadSafeDictionary_2_CopyTo_m5313_MethodInfo,
	&ThreadSafeDictionary_2_Remove_m5316_MethodInfo,
	&ThreadSafeDictionary_2_GetEnumerator_m5317_MethodInfo,
};
static bool ThreadSafeDictionary_2_t1121_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IDictionary_2_t1208_0_0_0;
extern const Il2CppType ICollection_1_t1209_0_0_0;
extern const Il2CppType IEnumerable_1_t1210_0_0_0;
static const Il2CppType* ThreadSafeDictionary_2_t1121_InterfacesTypeInfos[] = 
{
	&IEnumerable_t438_0_0_0,
	&IDictionary_2_t1208_0_0_0,
	&ICollection_1_t1209_0_0_0,
	&IEnumerable_1_t1210_0_0_0,
};
static Il2CppInterfaceOffsetPair ThreadSafeDictionary_2_t1121_InterfacesOffsets[] = 
{
	{ &IEnumerable_t438_0_0_0, 4},
	{ &IDictionary_2_t1208_0_0_0, 5},
	{ &ICollection_1_t1209_0_0_0, 12},
	{ &IEnumerable_1_t1210_0_0_0, 19},
};
extern const Il2CppGenericMethod Dictionary_2_GetEnumerator_m5430_GenericMethod;
extern const Il2CppType Enumerator_t1211_0_0_0;
extern const Il2CppGenericMethod ThreadSafeDictionary_2_AddValue_m5431_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2_TryGetValue_m5432_GenericMethod;
extern const Il2CppGenericMethod ThreadSafeDictionaryValueFactory_2_Invoke_m5433_GenericMethod;
extern const Il2CppType Dictionary_2_t1212_0_0_0;
extern const Il2CppGenericMethod Dictionary_2__ctor_m5434_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2_set_Item_m5435_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2__ctor_m5436_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2_get_Keys_m5437_GenericMethod;
extern const Il2CppGenericMethod ThreadSafeDictionary_2_get_Item_m5438_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2_get_Values_m5439_GenericMethod;
extern const Il2CppGenericMethod ThreadSafeDictionary_2_Get_m5440_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2_get_Count_m5441_GenericMethod;
static Il2CppRGCTXDefinition ThreadSafeDictionary_2_t1121_RGCTXData[15] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, &Dictionary_2_GetEnumerator_m5430_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&Enumerator_t1211_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &ThreadSafeDictionary_2_AddValue_m5431_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Dictionary_2_TryGetValue_m5432_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &ThreadSafeDictionaryValueFactory_2_Invoke_m5433_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&Dictionary_2_t1212_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Dictionary_2__ctor_m5434_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Dictionary_2_set_Item_m5435_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Dictionary_2__ctor_m5436_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Dictionary_2_get_Keys_m5437_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &ThreadSafeDictionary_2_get_Item_m5438_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Dictionary_2_get_Values_m5439_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &ThreadSafeDictionary_2_Get_m5440_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Dictionary_2_get_Count_m5441_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ThreadSafeDictionary_2_t1121_0_0_0;
extern const Il2CppType ThreadSafeDictionary_2_t1121_1_0_0;
extern TypeInfo ReflectionUtils_t946_il2cpp_TypeInfo;
extern const Il2CppType ReflectionUtils_t946_0_0_0;
struct ThreadSafeDictionary_2_t1121;
const Il2CppTypeDefinitionMetadata ThreadSafeDictionary_2_t1121_DefinitionMetadata = 
{
	&ReflectionUtils_t946_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, ThreadSafeDictionary_2_t1121_InterfacesTypeInfos/* implementedInterfaces */
	, ThreadSafeDictionary_2_t1121_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ThreadSafeDictionary_2_t1121_VTable/* vtableMethods */
	, ThreadSafeDictionary_2_t1121_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, ThreadSafeDictionary_2_t1121_RGCTXData/* rgctxDefinition */
	, 1107/* fieldStart */

};
TypeInfo ThreadSafeDictionary_2_t1121_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ThreadSafeDictionary`2"/* name */
	, ""/* namespaze */
	, ThreadSafeDictionary_2_t1121_MethodInfos/* methods */
	, ThreadSafeDictionary_2_t1121_PropertyInfos/* properties */
	, NULL/* events */
	, &ThreadSafeDictionary_2_t1121_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 991/* custom_attributes_cache */
	, &ThreadSafeDictionary_2_t1121_0_0_0/* byval_arg */
	, &ThreadSafeDictionary_2_t1121_1_0_0/* this_arg */
	, &ThreadSafeDictionary_2_t1121_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &ThreadSafeDictionary_2_t1121_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048834/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 19/* method_count */
	, 5/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 20/* vtable_count */
	, 4/* interfaces_count */
	, 4/* interface_offsets_count */

};
// SimpleJson.Reflection.ReflectionUtils/GetDelegate
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_GetDelegat.h"
// Metadata Definition SimpleJson.Reflection.ReflectionUtils/GetDelegate
extern TypeInfo GetDelegate_t937_il2cpp_TypeInfo;
// SimpleJson.Reflection.ReflectionUtils/GetDelegate
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_GetDelegatMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo GetDelegate_t937_GetDelegate__ctor_m4833_ParameterInfos[] = 
{
	{"object", 0, 134220059, 0, &Object_t_0_0_0},
	{"method", 1, 134220060, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void SimpleJson.Reflection.ReflectionUtils/GetDelegate::.ctor(System.Object,System.IntPtr)
extern const MethodInfo GetDelegate__ctor_m4833_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&GetDelegate__ctor_m4833/* method */
	, &GetDelegate_t937_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_IntPtr_t/* invoker_method */
	, GetDelegate_t937_GetDelegate__ctor_m4833_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1985/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo GetDelegate_t937_GetDelegate_Invoke_m4834_ParameterInfos[] = 
{
	{"source", 0, 134220061, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object SimpleJson.Reflection.ReflectionUtils/GetDelegate::Invoke(System.Object)
extern const MethodInfo GetDelegate_Invoke_m4834_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&GetDelegate_Invoke_m4834/* method */
	, &GetDelegate_t937_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, GetDelegate_t937_GetDelegate_Invoke_m4834_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1986/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType AsyncCallback_t547_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo GetDelegate_t937_GetDelegate_BeginInvoke_m4835_ParameterInfos[] = 
{
	{"source", 0, 134220062, 0, &Object_t_0_0_0},
	{"callback", 1, 134220063, 0, &AsyncCallback_t547_0_0_0},
	{"object", 2, 134220064, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult SimpleJson.Reflection.ReflectionUtils/GetDelegate::BeginInvoke(System.Object,System.AsyncCallback,System.Object)
extern const MethodInfo GetDelegate_BeginInvoke_m4835_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&GetDelegate_BeginInvoke_m4835/* method */
	, &GetDelegate_t937_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t546_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, GetDelegate_t937_GetDelegate_BeginInvoke_m4835_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1987/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t546_0_0_0;
static const ParameterInfo GetDelegate_t937_GetDelegate_EndInvoke_m4836_ParameterInfos[] = 
{
	{"result", 0, 134220065, 0, &IAsyncResult_t546_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object SimpleJson.Reflection.ReflectionUtils/GetDelegate::EndInvoke(System.IAsyncResult)
extern const MethodInfo GetDelegate_EndInvoke_m4836_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&GetDelegate_EndInvoke_m4836/* method */
	, &GetDelegate_t937_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, GetDelegate_t937_GetDelegate_EndInvoke_m4836_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1988/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* GetDelegate_t937_MethodInfos[] =
{
	&GetDelegate__ctor_m4833_MethodInfo,
	&GetDelegate_Invoke_m4834_MethodInfo,
	&GetDelegate_BeginInvoke_m4835_MethodInfo,
	&GetDelegate_EndInvoke_m4836_MethodInfo,
	NULL
};
extern const MethodInfo GetDelegate_Invoke_m4834_MethodInfo;
extern const MethodInfo GetDelegate_BeginInvoke_m4835_MethodInfo;
extern const MethodInfo GetDelegate_EndInvoke_m4836_MethodInfo;
static const Il2CppMethodReference GetDelegate_t937_VTable[] =
{
	&MulticastDelegate_Equals_m3586_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&MulticastDelegate_GetHashCode_m3587_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&Delegate_Clone_m3589_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&MulticastDelegate_GetInvocationList_m3590_MethodInfo,
	&MulticastDelegate_CombineImpl_m3591_MethodInfo,
	&MulticastDelegate_RemoveImpl_m3592_MethodInfo,
	&GetDelegate_Invoke_m4834_MethodInfo,
	&GetDelegate_BeginInvoke_m4835_MethodInfo,
	&GetDelegate_EndInvoke_m4836_MethodInfo,
};
static bool GetDelegate_t937_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair GetDelegate_t937_InterfacesOffsets[] = 
{
	{ &ICloneable_t733_0_0_0, 4},
	{ &ISerializable_t439_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GetDelegate_t937_0_0_0;
extern const Il2CppType GetDelegate_t937_1_0_0;
struct GetDelegate_t937;
const Il2CppTypeDefinitionMetadata GetDelegate_t937_DefinitionMetadata = 
{
	&ReflectionUtils_t946_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, GetDelegate_t937_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t549_0_0_0/* parent */
	, GetDelegate_t937_VTable/* vtableMethods */
	, GetDelegate_t937_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo GetDelegate_t937_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GetDelegate"/* name */
	, ""/* namespaze */
	, GetDelegate_t937_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &GetDelegate_t937_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GetDelegate_t937_0_0_0/* byval_arg */
	, &GetDelegate_t937_1_0_0/* this_arg */
	, &GetDelegate_t937_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_GetDelegate_t937/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GetDelegate_t937)/* instance_size */
	, sizeof (GetDelegate_t937)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// SimpleJson.Reflection.ReflectionUtils/SetDelegate
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_SetDelegat.h"
// Metadata Definition SimpleJson.Reflection.ReflectionUtils/SetDelegate
extern TypeInfo SetDelegate_t938_il2cpp_TypeInfo;
// SimpleJson.Reflection.ReflectionUtils/SetDelegate
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_SetDelegatMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo SetDelegate_t938_SetDelegate__ctor_m4837_ParameterInfos[] = 
{
	{"object", 0, 134220066, 0, &Object_t_0_0_0},
	{"method", 1, 134220067, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void SimpleJson.Reflection.ReflectionUtils/SetDelegate::.ctor(System.Object,System.IntPtr)
extern const MethodInfo SetDelegate__ctor_m4837_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SetDelegate__ctor_m4837/* method */
	, &SetDelegate_t938_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_IntPtr_t/* invoker_method */
	, SetDelegate_t938_SetDelegate__ctor_m4837_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1989/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo SetDelegate_t938_SetDelegate_Invoke_m4838_ParameterInfos[] = 
{
	{"source", 0, 134220068, 0, &Object_t_0_0_0},
	{"value", 1, 134220069, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void SimpleJson.Reflection.ReflectionUtils/SetDelegate::Invoke(System.Object,System.Object)
extern const MethodInfo SetDelegate_Invoke_m4838_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&SetDelegate_Invoke_m4838/* method */
	, &SetDelegate_t938_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, SetDelegate_t938_SetDelegate_Invoke_m4838_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1990/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType AsyncCallback_t547_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo SetDelegate_t938_SetDelegate_BeginInvoke_m4839_ParameterInfos[] = 
{
	{"source", 0, 134220070, 0, &Object_t_0_0_0},
	{"value", 1, 134220071, 0, &Object_t_0_0_0},
	{"callback", 2, 134220072, 0, &AsyncCallback_t547_0_0_0},
	{"object", 3, 134220073, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult SimpleJson.Reflection.ReflectionUtils/SetDelegate::BeginInvoke(System.Object,System.Object,System.AsyncCallback,System.Object)
extern const MethodInfo SetDelegate_BeginInvoke_m4839_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&SetDelegate_BeginInvoke_m4839/* method */
	, &SetDelegate_t938_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t546_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, SetDelegate_t938_SetDelegate_BeginInvoke_m4839_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1991/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t546_0_0_0;
static const ParameterInfo SetDelegate_t938_SetDelegate_EndInvoke_m4840_ParameterInfos[] = 
{
	{"result", 0, 134220074, 0, &IAsyncResult_t546_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void SimpleJson.Reflection.ReflectionUtils/SetDelegate::EndInvoke(System.IAsyncResult)
extern const MethodInfo SetDelegate_EndInvoke_m4840_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&SetDelegate_EndInvoke_m4840/* method */
	, &SetDelegate_t938_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, SetDelegate_t938_SetDelegate_EndInvoke_m4840_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1992/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SetDelegate_t938_MethodInfos[] =
{
	&SetDelegate__ctor_m4837_MethodInfo,
	&SetDelegate_Invoke_m4838_MethodInfo,
	&SetDelegate_BeginInvoke_m4839_MethodInfo,
	&SetDelegate_EndInvoke_m4840_MethodInfo,
	NULL
};
extern const MethodInfo SetDelegate_Invoke_m4838_MethodInfo;
extern const MethodInfo SetDelegate_BeginInvoke_m4839_MethodInfo;
extern const MethodInfo SetDelegate_EndInvoke_m4840_MethodInfo;
static const Il2CppMethodReference SetDelegate_t938_VTable[] =
{
	&MulticastDelegate_Equals_m3586_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&MulticastDelegate_GetHashCode_m3587_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&Delegate_Clone_m3589_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&MulticastDelegate_GetInvocationList_m3590_MethodInfo,
	&MulticastDelegate_CombineImpl_m3591_MethodInfo,
	&MulticastDelegate_RemoveImpl_m3592_MethodInfo,
	&SetDelegate_Invoke_m4838_MethodInfo,
	&SetDelegate_BeginInvoke_m4839_MethodInfo,
	&SetDelegate_EndInvoke_m4840_MethodInfo,
};
static bool SetDelegate_t938_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair SetDelegate_t938_InterfacesOffsets[] = 
{
	{ &ICloneable_t733_0_0_0, 4},
	{ &ISerializable_t439_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType SetDelegate_t938_0_0_0;
extern const Il2CppType SetDelegate_t938_1_0_0;
struct SetDelegate_t938;
const Il2CppTypeDefinitionMetadata SetDelegate_t938_DefinitionMetadata = 
{
	&ReflectionUtils_t946_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SetDelegate_t938_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t549_0_0_0/* parent */
	, SetDelegate_t938_VTable/* vtableMethods */
	, SetDelegate_t938_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo SetDelegate_t938_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SetDelegate"/* name */
	, ""/* namespaze */
	, SetDelegate_t938_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SetDelegate_t938_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SetDelegate_t938_0_0_0/* byval_arg */
	, &SetDelegate_t938_1_0_0/* this_arg */
	, &SetDelegate_t938_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_SetDelegate_t938/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SetDelegate_t938)/* instance_size */
	, sizeof (SetDelegate_t938)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_Constructo.h"
// Metadata Definition SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate
extern TypeInfo ConstructorDelegate_t939_il2cpp_TypeInfo;
// SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_ConstructoMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo ConstructorDelegate_t939_ConstructorDelegate__ctor_m4841_ParameterInfos[] = 
{
	{"object", 0, 134220075, 0, &Object_t_0_0_0},
	{"method", 1, 134220076, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate::.ctor(System.Object,System.IntPtr)
extern const MethodInfo ConstructorDelegate__ctor_m4841_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ConstructorDelegate__ctor_m4841/* method */
	, &ConstructorDelegate_t939_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_IntPtr_t/* invoker_method */
	, ConstructorDelegate_t939_ConstructorDelegate__ctor_m4841_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1993/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectU5BU5D_t224_0_0_0;
extern const Il2CppType ObjectU5BU5D_t224_0_0_0;
static const ParameterInfo ConstructorDelegate_t939_ConstructorDelegate_Invoke_m4842_ParameterInfos[] = 
{
	{"args", 0, 134220077, 992, &ObjectU5BU5D_t224_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate::Invoke(System.Object[])
extern const MethodInfo ConstructorDelegate_Invoke_m4842_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&ConstructorDelegate_Invoke_m4842/* method */
	, &ConstructorDelegate_t939_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ConstructorDelegate_t939_ConstructorDelegate_Invoke_m4842_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1994/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectU5BU5D_t224_0_0_0;
extern const Il2CppType AsyncCallback_t547_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo ConstructorDelegate_t939_ConstructorDelegate_BeginInvoke_m4843_ParameterInfos[] = 
{
	{"args", 0, 134220078, 993, &ObjectU5BU5D_t224_0_0_0},
	{"callback", 1, 134220079, 0, &AsyncCallback_t547_0_0_0},
	{"object", 2, 134220080, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate::BeginInvoke(System.Object[],System.AsyncCallback,System.Object)
extern const MethodInfo ConstructorDelegate_BeginInvoke_m4843_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&ConstructorDelegate_BeginInvoke_m4843/* method */
	, &ConstructorDelegate_t939_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t546_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, ConstructorDelegate_t939_ConstructorDelegate_BeginInvoke_m4843_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1995/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t546_0_0_0;
static const ParameterInfo ConstructorDelegate_t939_ConstructorDelegate_EndInvoke_m4844_ParameterInfos[] = 
{
	{"result", 0, 134220081, 0, &IAsyncResult_t546_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate::EndInvoke(System.IAsyncResult)
extern const MethodInfo ConstructorDelegate_EndInvoke_m4844_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&ConstructorDelegate_EndInvoke_m4844/* method */
	, &ConstructorDelegate_t939_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ConstructorDelegate_t939_ConstructorDelegate_EndInvoke_m4844_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1996/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ConstructorDelegate_t939_MethodInfos[] =
{
	&ConstructorDelegate__ctor_m4841_MethodInfo,
	&ConstructorDelegate_Invoke_m4842_MethodInfo,
	&ConstructorDelegate_BeginInvoke_m4843_MethodInfo,
	&ConstructorDelegate_EndInvoke_m4844_MethodInfo,
	NULL
};
extern const MethodInfo ConstructorDelegate_Invoke_m4842_MethodInfo;
extern const MethodInfo ConstructorDelegate_BeginInvoke_m4843_MethodInfo;
extern const MethodInfo ConstructorDelegate_EndInvoke_m4844_MethodInfo;
static const Il2CppMethodReference ConstructorDelegate_t939_VTable[] =
{
	&MulticastDelegate_Equals_m3586_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&MulticastDelegate_GetHashCode_m3587_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&Delegate_Clone_m3589_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&MulticastDelegate_GetInvocationList_m3590_MethodInfo,
	&MulticastDelegate_CombineImpl_m3591_MethodInfo,
	&MulticastDelegate_RemoveImpl_m3592_MethodInfo,
	&ConstructorDelegate_Invoke_m4842_MethodInfo,
	&ConstructorDelegate_BeginInvoke_m4843_MethodInfo,
	&ConstructorDelegate_EndInvoke_m4844_MethodInfo,
};
static bool ConstructorDelegate_t939_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ConstructorDelegate_t939_InterfacesOffsets[] = 
{
	{ &ICloneable_t733_0_0_0, 4},
	{ &ISerializable_t439_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ConstructorDelegate_t939_1_0_0;
struct ConstructorDelegate_t939;
const Il2CppTypeDefinitionMetadata ConstructorDelegate_t939_DefinitionMetadata = 
{
	&ReflectionUtils_t946_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ConstructorDelegate_t939_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t549_0_0_0/* parent */
	, ConstructorDelegate_t939_VTable/* vtableMethods */
	, ConstructorDelegate_t939_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ConstructorDelegate_t939_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ConstructorDelegate"/* name */
	, ""/* namespaze */
	, ConstructorDelegate_t939_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ConstructorDelegate_t939_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ConstructorDelegate_t939_0_0_0/* byval_arg */
	, &ConstructorDelegate_t939_1_0_0/* this_arg */
	, &ConstructorDelegate_t939_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_ConstructorDelegate_t939/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ConstructorDelegate_t939)/* instance_size */
	, sizeof (ConstructorDelegate_t939)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionaryValueFactory`2
extern TypeInfo ThreadSafeDictionaryValueFactory_2_t1122_il2cpp_TypeInfo;
extern const Il2CppGenericContainer ThreadSafeDictionaryValueFactory_2_t1122_Il2CppGenericContainer;
extern TypeInfo ThreadSafeDictionaryValueFactory_2_t1122_gp_TKey_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter ThreadSafeDictionaryValueFactory_2_t1122_gp_TKey_0_il2cpp_TypeInfo_GenericParamFull = { &ThreadSafeDictionaryValueFactory_2_t1122_Il2CppGenericContainer, NULL, "TKey", 0, 0 };
extern TypeInfo ThreadSafeDictionaryValueFactory_2_t1122_gp_TValue_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter ThreadSafeDictionaryValueFactory_2_t1122_gp_TValue_1_il2cpp_TypeInfo_GenericParamFull = { &ThreadSafeDictionaryValueFactory_2_t1122_Il2CppGenericContainer, NULL, "TValue", 1, 0 };
static const Il2CppGenericParameter* ThreadSafeDictionaryValueFactory_2_t1122_Il2CppGenericParametersArray[2] = 
{
	&ThreadSafeDictionaryValueFactory_2_t1122_gp_TKey_0_il2cpp_TypeInfo_GenericParamFull,
	&ThreadSafeDictionaryValueFactory_2_t1122_gp_TValue_1_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer ThreadSafeDictionaryValueFactory_2_t1122_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&ThreadSafeDictionaryValueFactory_2_t1122_il2cpp_TypeInfo, 2, 0, ThreadSafeDictionaryValueFactory_2_t1122_Il2CppGenericParametersArray };
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo ThreadSafeDictionaryValueFactory_2_t1122_ThreadSafeDictionaryValueFactory_2__ctor_m5318_ParameterInfos[] = 
{
	{"object", 0, 134220082, 0, &Object_t_0_0_0},
	{"method", 1, 134220083, 0, &IntPtr_t_0_0_0},
};
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionaryValueFactory`2::.ctor(System.Object,System.IntPtr)
extern const MethodInfo ThreadSafeDictionaryValueFactory_2__ctor_m5318_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &ThreadSafeDictionaryValueFactory_2_t1122_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ThreadSafeDictionaryValueFactory_2_t1122_ThreadSafeDictionaryValueFactory_2__ctor_m5318_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1997/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ThreadSafeDictionaryValueFactory_2_t1122_gp_0_0_0_0;
extern const Il2CppType ThreadSafeDictionaryValueFactory_2_t1122_gp_0_0_0_0;
static const ParameterInfo ThreadSafeDictionaryValueFactory_2_t1122_ThreadSafeDictionaryValueFactory_2_Invoke_m5319_ParameterInfos[] = 
{
	{"key", 0, 134220084, 0, &ThreadSafeDictionaryValueFactory_2_t1122_gp_0_0_0_0},
};
extern const Il2CppType ThreadSafeDictionaryValueFactory_2_t1122_gp_1_0_0_0;
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionaryValueFactory`2::Invoke(TKey)
extern const MethodInfo ThreadSafeDictionaryValueFactory_2_Invoke_m5319_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &ThreadSafeDictionaryValueFactory_2_t1122_il2cpp_TypeInfo/* declaring_type */
	, &ThreadSafeDictionaryValueFactory_2_t1122_gp_1_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ThreadSafeDictionaryValueFactory_2_t1122_ThreadSafeDictionaryValueFactory_2_Invoke_m5319_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1998/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ThreadSafeDictionaryValueFactory_2_t1122_gp_0_0_0_0;
extern const Il2CppType AsyncCallback_t547_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo ThreadSafeDictionaryValueFactory_2_t1122_ThreadSafeDictionaryValueFactory_2_BeginInvoke_m5320_ParameterInfos[] = 
{
	{"key", 0, 134220085, 0, &ThreadSafeDictionaryValueFactory_2_t1122_gp_0_0_0_0},
	{"callback", 1, 134220086, 0, &AsyncCallback_t547_0_0_0},
	{"object", 2, 134220087, 0, &Object_t_0_0_0},
};
// System.IAsyncResult SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionaryValueFactory`2::BeginInvoke(TKey,System.AsyncCallback,System.Object)
extern const MethodInfo ThreadSafeDictionaryValueFactory_2_BeginInvoke_m5320_MethodInfo = 
{
	"BeginInvoke"/* name */
	, NULL/* method */
	, &ThreadSafeDictionaryValueFactory_2_t1122_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t546_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ThreadSafeDictionaryValueFactory_2_t1122_ThreadSafeDictionaryValueFactory_2_BeginInvoke_m5320_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1999/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t546_0_0_0;
static const ParameterInfo ThreadSafeDictionaryValueFactory_2_t1122_ThreadSafeDictionaryValueFactory_2_EndInvoke_m5321_ParameterInfos[] = 
{
	{"result", 0, 134220088, 0, &IAsyncResult_t546_0_0_0},
};
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionaryValueFactory`2::EndInvoke(System.IAsyncResult)
extern const MethodInfo ThreadSafeDictionaryValueFactory_2_EndInvoke_m5321_MethodInfo = 
{
	"EndInvoke"/* name */
	, NULL/* method */
	, &ThreadSafeDictionaryValueFactory_2_t1122_il2cpp_TypeInfo/* declaring_type */
	, &ThreadSafeDictionaryValueFactory_2_t1122_gp_1_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ThreadSafeDictionaryValueFactory_2_t1122_ThreadSafeDictionaryValueFactory_2_EndInvoke_m5321_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2000/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ThreadSafeDictionaryValueFactory_2_t1122_MethodInfos[] =
{
	&ThreadSafeDictionaryValueFactory_2__ctor_m5318_MethodInfo,
	&ThreadSafeDictionaryValueFactory_2_Invoke_m5319_MethodInfo,
	&ThreadSafeDictionaryValueFactory_2_BeginInvoke_m5320_MethodInfo,
	&ThreadSafeDictionaryValueFactory_2_EndInvoke_m5321_MethodInfo,
	NULL
};
extern const MethodInfo ThreadSafeDictionaryValueFactory_2_Invoke_m5319_MethodInfo;
extern const MethodInfo ThreadSafeDictionaryValueFactory_2_BeginInvoke_m5320_MethodInfo;
extern const MethodInfo ThreadSafeDictionaryValueFactory_2_EndInvoke_m5321_MethodInfo;
static const Il2CppMethodReference ThreadSafeDictionaryValueFactory_2_t1122_VTable[] =
{
	&MulticastDelegate_Equals_m3586_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&MulticastDelegate_GetHashCode_m3587_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&Delegate_Clone_m3589_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&MulticastDelegate_GetInvocationList_m3590_MethodInfo,
	&MulticastDelegate_CombineImpl_m3591_MethodInfo,
	&MulticastDelegate_RemoveImpl_m3592_MethodInfo,
	&ThreadSafeDictionaryValueFactory_2_Invoke_m5319_MethodInfo,
	&ThreadSafeDictionaryValueFactory_2_BeginInvoke_m5320_MethodInfo,
	&ThreadSafeDictionaryValueFactory_2_EndInvoke_m5321_MethodInfo,
};
static bool ThreadSafeDictionaryValueFactory_2_t1122_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ThreadSafeDictionaryValueFactory_2_t1122_InterfacesOffsets[] = 
{
	{ &ICloneable_t733_0_0_0, 4},
	{ &ISerializable_t439_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ThreadSafeDictionaryValueFactory_2_t1122_0_0_0;
extern const Il2CppType ThreadSafeDictionaryValueFactory_2_t1122_1_0_0;
struct ThreadSafeDictionaryValueFactory_2_t1122;
const Il2CppTypeDefinitionMetadata ThreadSafeDictionaryValueFactory_2_t1122_DefinitionMetadata = 
{
	&ReflectionUtils_t946_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ThreadSafeDictionaryValueFactory_2_t1122_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t549_0_0_0/* parent */
	, ThreadSafeDictionaryValueFactory_2_t1122_VTable/* vtableMethods */
	, ThreadSafeDictionaryValueFactory_2_t1122_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ThreadSafeDictionaryValueFactory_2_t1122_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ThreadSafeDictionaryValueFactory`2"/* name */
	, ""/* namespaze */
	, ThreadSafeDictionaryValueFactory_2_t1122_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ThreadSafeDictionaryValueFactory_2_t1122_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ThreadSafeDictionaryValueFactory_2_t1122_0_0_0/* byval_arg */
	, &ThreadSafeDictionaryValueFactory_2_t1122_1_0_0/* this_arg */
	, &ThreadSafeDictionaryValueFactory_2_t1122_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &ThreadSafeDictionaryValueFactory_2_t1122_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// SimpleJson.Reflection.ReflectionUtils/<GetConstructorByReflection>c__AnonStorey1
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_U3CGetCons.h"
// Metadata Definition SimpleJson.Reflection.ReflectionUtils/<GetConstructorByReflection>c__AnonStorey1
extern TypeInfo U3CGetConstructorByReflectionU3Ec__AnonStorey1_t941_il2cpp_TypeInfo;
// SimpleJson.Reflection.ReflectionUtils/<GetConstructorByReflection>c__AnonStorey1
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_U3CGetConsMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SimpleJson.Reflection.ReflectionUtils/<GetConstructorByReflection>c__AnonStorey1::.ctor()
extern const MethodInfo U3CGetConstructorByReflectionU3Ec__AnonStorey1__ctor_m4845_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CGetConstructorByReflectionU3Ec__AnonStorey1__ctor_m4845/* method */
	, &U3CGetConstructorByReflectionU3Ec__AnonStorey1_t941_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2001/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectU5BU5D_t224_0_0_0;
static const ParameterInfo U3CGetConstructorByReflectionU3Ec__AnonStorey1_t941_U3CGetConstructorByReflectionU3Ec__AnonStorey1_U3CU3Em__0_m4846_ParameterInfos[] = 
{
	{"args", 0, 134220089, 0, &ObjectU5BU5D_t224_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object SimpleJson.Reflection.ReflectionUtils/<GetConstructorByReflection>c__AnonStorey1::<>m__0(System.Object[])
extern const MethodInfo U3CGetConstructorByReflectionU3Ec__AnonStorey1_U3CU3Em__0_m4846_MethodInfo = 
{
	"<>m__0"/* name */
	, (methodPointerType)&U3CGetConstructorByReflectionU3Ec__AnonStorey1_U3CU3Em__0_m4846/* method */
	, &U3CGetConstructorByReflectionU3Ec__AnonStorey1_t941_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, U3CGetConstructorByReflectionU3Ec__AnonStorey1_t941_U3CGetConstructorByReflectionU3Ec__AnonStorey1_U3CU3Em__0_m4846_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2002/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CGetConstructorByReflectionU3Ec__AnonStorey1_t941_MethodInfos[] =
{
	&U3CGetConstructorByReflectionU3Ec__AnonStorey1__ctor_m4845_MethodInfo,
	&U3CGetConstructorByReflectionU3Ec__AnonStorey1_U3CU3Em__0_m4846_MethodInfo,
	NULL
};
static const Il2CppMethodReference U3CGetConstructorByReflectionU3Ec__AnonStorey1_t941_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool U3CGetConstructorByReflectionU3Ec__AnonStorey1_t941_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType U3CGetConstructorByReflectionU3Ec__AnonStorey1_t941_0_0_0;
extern const Il2CppType U3CGetConstructorByReflectionU3Ec__AnonStorey1_t941_1_0_0;
struct U3CGetConstructorByReflectionU3Ec__AnonStorey1_t941;
const Il2CppTypeDefinitionMetadata U3CGetConstructorByReflectionU3Ec__AnonStorey1_t941_DefinitionMetadata = 
{
	&ReflectionUtils_t946_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CGetConstructorByReflectionU3Ec__AnonStorey1_t941_VTable/* vtableMethods */
	, U3CGetConstructorByReflectionU3Ec__AnonStorey1_t941_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1110/* fieldStart */

};
TypeInfo U3CGetConstructorByReflectionU3Ec__AnonStorey1_t941_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "<GetConstructorByReflection>c__AnonStorey1"/* name */
	, ""/* namespaze */
	, U3CGetConstructorByReflectionU3Ec__AnonStorey1_t941_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U3CGetConstructorByReflectionU3Ec__AnonStorey1_t941_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 994/* custom_attributes_cache */
	, &U3CGetConstructorByReflectionU3Ec__AnonStorey1_t941_0_0_0/* byval_arg */
	, &U3CGetConstructorByReflectionU3Ec__AnonStorey1_t941_1_0_0/* this_arg */
	, &U3CGetConstructorByReflectionU3Ec__AnonStorey1_t941_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CGetConstructorByReflectionU3Ec__AnonStorey1_t941)/* instance_size */
	, sizeof (U3CGetConstructorByReflectionU3Ec__AnonStorey1_t941)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SimpleJson.Reflection.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey2
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_U3CGetGetM.h"
// Metadata Definition SimpleJson.Reflection.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey2
extern TypeInfo U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t942_il2cpp_TypeInfo;
// SimpleJson.Reflection.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey2
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_U3CGetGetMMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SimpleJson.Reflection.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey2::.ctor()
extern const MethodInfo U3CGetGetMethodByReflectionU3Ec__AnonStorey2__ctor_m4847_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CGetGetMethodByReflectionU3Ec__AnonStorey2__ctor_m4847/* method */
	, &U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t942_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2003/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t942_U3CGetGetMethodByReflectionU3Ec__AnonStorey2_U3CU3Em__1_m4848_ParameterInfos[] = 
{
	{"source", 0, 134220090, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object SimpleJson.Reflection.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey2::<>m__1(System.Object)
extern const MethodInfo U3CGetGetMethodByReflectionU3Ec__AnonStorey2_U3CU3Em__1_m4848_MethodInfo = 
{
	"<>m__1"/* name */
	, (methodPointerType)&U3CGetGetMethodByReflectionU3Ec__AnonStorey2_U3CU3Em__1_m4848/* method */
	, &U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t942_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t942_U3CGetGetMethodByReflectionU3Ec__AnonStorey2_U3CU3Em__1_m4848_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2004/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t942_MethodInfos[] =
{
	&U3CGetGetMethodByReflectionU3Ec__AnonStorey2__ctor_m4847_MethodInfo,
	&U3CGetGetMethodByReflectionU3Ec__AnonStorey2_U3CU3Em__1_m4848_MethodInfo,
	NULL
};
static const Il2CppMethodReference U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t942_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t942_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t942_0_0_0;
extern const Il2CppType U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t942_1_0_0;
struct U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t942;
const Il2CppTypeDefinitionMetadata U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t942_DefinitionMetadata = 
{
	&ReflectionUtils_t946_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t942_VTable/* vtableMethods */
	, U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t942_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1111/* fieldStart */

};
TypeInfo U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t942_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "<GetGetMethodByReflection>c__AnonStorey2"/* name */
	, ""/* namespaze */
	, U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t942_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t942_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 995/* custom_attributes_cache */
	, &U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t942_0_0_0/* byval_arg */
	, &U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t942_1_0_0/* this_arg */
	, &U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t942_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t942)/* instance_size */
	, sizeof (U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t942)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SimpleJson.Reflection.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey3
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_U3CGetGetM_0.h"
// Metadata Definition SimpleJson.Reflection.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey3
extern TypeInfo U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t943_il2cpp_TypeInfo;
// SimpleJson.Reflection.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey3
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_U3CGetGetM_0MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SimpleJson.Reflection.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey3::.ctor()
extern const MethodInfo U3CGetGetMethodByReflectionU3Ec__AnonStorey3__ctor_m4849_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CGetGetMethodByReflectionU3Ec__AnonStorey3__ctor_m4849/* method */
	, &U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t943_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2005/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t943_U3CGetGetMethodByReflectionU3Ec__AnonStorey3_U3CU3Em__2_m4850_ParameterInfos[] = 
{
	{"source", 0, 134220091, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object SimpleJson.Reflection.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey3::<>m__2(System.Object)
extern const MethodInfo U3CGetGetMethodByReflectionU3Ec__AnonStorey3_U3CU3Em__2_m4850_MethodInfo = 
{
	"<>m__2"/* name */
	, (methodPointerType)&U3CGetGetMethodByReflectionU3Ec__AnonStorey3_U3CU3Em__2_m4850/* method */
	, &U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t943_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t943_U3CGetGetMethodByReflectionU3Ec__AnonStorey3_U3CU3Em__2_m4850_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2006/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t943_MethodInfos[] =
{
	&U3CGetGetMethodByReflectionU3Ec__AnonStorey3__ctor_m4849_MethodInfo,
	&U3CGetGetMethodByReflectionU3Ec__AnonStorey3_U3CU3Em__2_m4850_MethodInfo,
	NULL
};
static const Il2CppMethodReference U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t943_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t943_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t943_0_0_0;
extern const Il2CppType U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t943_1_0_0;
struct U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t943;
const Il2CppTypeDefinitionMetadata U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t943_DefinitionMetadata = 
{
	&ReflectionUtils_t946_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t943_VTable/* vtableMethods */
	, U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t943_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1112/* fieldStart */

};
TypeInfo U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t943_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "<GetGetMethodByReflection>c__AnonStorey3"/* name */
	, ""/* namespaze */
	, U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t943_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t943_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 996/* custom_attributes_cache */
	, &U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t943_0_0_0/* byval_arg */
	, &U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t943_1_0_0/* this_arg */
	, &U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t943_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t943)/* instance_size */
	, sizeof (U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t943)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SimpleJson.Reflection.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey4
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_U3CGetSetM.h"
// Metadata Definition SimpleJson.Reflection.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey4
extern TypeInfo U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t944_il2cpp_TypeInfo;
// SimpleJson.Reflection.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey4
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_U3CGetSetMMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SimpleJson.Reflection.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey4::.ctor()
extern const MethodInfo U3CGetSetMethodByReflectionU3Ec__AnonStorey4__ctor_m4851_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CGetSetMethodByReflectionU3Ec__AnonStorey4__ctor_m4851/* method */
	, &U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t944_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2007/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t944_U3CGetSetMethodByReflectionU3Ec__AnonStorey4_U3CU3Em__3_m4852_ParameterInfos[] = 
{
	{"source", 0, 134220092, 0, &Object_t_0_0_0},
	{"value", 1, 134220093, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void SimpleJson.Reflection.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey4::<>m__3(System.Object,System.Object)
extern const MethodInfo U3CGetSetMethodByReflectionU3Ec__AnonStorey4_U3CU3Em__3_m4852_MethodInfo = 
{
	"<>m__3"/* name */
	, (methodPointerType)&U3CGetSetMethodByReflectionU3Ec__AnonStorey4_U3CU3Em__3_m4852/* method */
	, &U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t944_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t944_U3CGetSetMethodByReflectionU3Ec__AnonStorey4_U3CU3Em__3_m4852_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2008/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t944_MethodInfos[] =
{
	&U3CGetSetMethodByReflectionU3Ec__AnonStorey4__ctor_m4851_MethodInfo,
	&U3CGetSetMethodByReflectionU3Ec__AnonStorey4_U3CU3Em__3_m4852_MethodInfo,
	NULL
};
static const Il2CppMethodReference U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t944_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t944_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t944_0_0_0;
extern const Il2CppType U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t944_1_0_0;
struct U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t944;
const Il2CppTypeDefinitionMetadata U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t944_DefinitionMetadata = 
{
	&ReflectionUtils_t946_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t944_VTable/* vtableMethods */
	, U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t944_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1113/* fieldStart */

};
TypeInfo U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t944_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "<GetSetMethodByReflection>c__AnonStorey4"/* name */
	, ""/* namespaze */
	, U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t944_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t944_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 997/* custom_attributes_cache */
	, &U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t944_0_0_0/* byval_arg */
	, &U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t944_1_0_0/* this_arg */
	, &U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t944_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t944)/* instance_size */
	, sizeof (U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t944)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SimpleJson.Reflection.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey5
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_U3CGetSetM_0.h"
// Metadata Definition SimpleJson.Reflection.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey5
extern TypeInfo U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t945_il2cpp_TypeInfo;
// SimpleJson.Reflection.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey5
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_U3CGetSetM_0MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SimpleJson.Reflection.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey5::.ctor()
extern const MethodInfo U3CGetSetMethodByReflectionU3Ec__AnonStorey5__ctor_m4853_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CGetSetMethodByReflectionU3Ec__AnonStorey5__ctor_m4853/* method */
	, &U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t945_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2009/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t945_U3CGetSetMethodByReflectionU3Ec__AnonStorey5_U3CU3Em__4_m4854_ParameterInfos[] = 
{
	{"source", 0, 134220094, 0, &Object_t_0_0_0},
	{"value", 1, 134220095, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void SimpleJson.Reflection.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey5::<>m__4(System.Object,System.Object)
extern const MethodInfo U3CGetSetMethodByReflectionU3Ec__AnonStorey5_U3CU3Em__4_m4854_MethodInfo = 
{
	"<>m__4"/* name */
	, (methodPointerType)&U3CGetSetMethodByReflectionU3Ec__AnonStorey5_U3CU3Em__4_m4854/* method */
	, &U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t945_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t945_U3CGetSetMethodByReflectionU3Ec__AnonStorey5_U3CU3Em__4_m4854_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2010/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t945_MethodInfos[] =
{
	&U3CGetSetMethodByReflectionU3Ec__AnonStorey5__ctor_m4853_MethodInfo,
	&U3CGetSetMethodByReflectionU3Ec__AnonStorey5_U3CU3Em__4_m4854_MethodInfo,
	NULL
};
static const Il2CppMethodReference U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t945_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t945_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t945_0_0_0;
extern const Il2CppType U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t945_1_0_0;
struct U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t945;
const Il2CppTypeDefinitionMetadata U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t945_DefinitionMetadata = 
{
	&ReflectionUtils_t946_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t945_VTable/* vtableMethods */
	, U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t945_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1114/* fieldStart */

};
TypeInfo U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t945_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "<GetSetMethodByReflection>c__AnonStorey5"/* name */
	, ""/* namespaze */
	, U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t945_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t945_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 998/* custom_attributes_cache */
	, &U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t945_0_0_0/* byval_arg */
	, &U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t945_1_0_0/* this_arg */
	, &U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t945_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t945)/* instance_size */
	, sizeof (U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t945)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SimpleJson.Reflection.ReflectionUtils
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils.h"
// Metadata Definition SimpleJson.Reflection.ReflectionUtils
// SimpleJson.Reflection.ReflectionUtils
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtilsMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void SimpleJson.Reflection.ReflectionUtils::.cctor()
extern const MethodInfo ReflectionUtils__cctor_m4855_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&ReflectionUtils__cctor_m4855/* method */
	, &ReflectionUtils_t946_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1948/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo ReflectionUtils_t946_ReflectionUtils_GetConstructors_m4856_ParameterInfos[] = 
{
	{"type", 0, 134220023, 0, &Type_t_0_0_0},
};
extern const Il2CppType IEnumerable_1_t1039_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.Generic.IEnumerable`1<System.Reflection.ConstructorInfo> SimpleJson.Reflection.ReflectionUtils::GetConstructors(System.Type)
extern const MethodInfo ReflectionUtils_GetConstructors_m4856_MethodInfo = 
{
	"GetConstructors"/* name */
	, (methodPointerType)&ReflectionUtils_GetConstructors_m4856/* method */
	, &ReflectionUtils_t946_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerable_1_t1039_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ReflectionUtils_t946_ReflectionUtils_GetConstructors_m4856_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1949/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType TypeU5BU5D_t238_0_0_0;
extern const Il2CppType TypeU5BU5D_t238_0_0_0;
static const ParameterInfo ReflectionUtils_t946_ReflectionUtils_GetConstructorInfo_m4857_ParameterInfos[] = 
{
	{"type", 0, 134220024, 0, &Type_t_0_0_0},
	{"argsType", 1, 134220025, 988, &TypeU5BU5D_t238_0_0_0},
};
extern const Il2CppType ConstructorInfo_t940_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.ConstructorInfo SimpleJson.Reflection.ReflectionUtils::GetConstructorInfo(System.Type,System.Type[])
extern const MethodInfo ReflectionUtils_GetConstructorInfo_m4857_MethodInfo = 
{
	"GetConstructorInfo"/* name */
	, (methodPointerType)&ReflectionUtils_GetConstructorInfo_m4857/* method */
	, &ReflectionUtils_t946_il2cpp_TypeInfo/* declaring_type */
	, &ConstructorInfo_t940_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, ReflectionUtils_t946_ReflectionUtils_GetConstructorInfo_m4857_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1950/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo ReflectionUtils_t946_ReflectionUtils_GetProperties_m4858_ParameterInfos[] = 
{
	{"type", 0, 134220026, 0, &Type_t_0_0_0},
};
extern const Il2CppType IEnumerable_1_t1040_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.Generic.IEnumerable`1<System.Reflection.PropertyInfo> SimpleJson.Reflection.ReflectionUtils::GetProperties(System.Type)
extern const MethodInfo ReflectionUtils_GetProperties_m4858_MethodInfo = 
{
	"GetProperties"/* name */
	, (methodPointerType)&ReflectionUtils_GetProperties_m4858/* method */
	, &ReflectionUtils_t946_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerable_1_t1040_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ReflectionUtils_t946_ReflectionUtils_GetProperties_m4858_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1951/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo ReflectionUtils_t946_ReflectionUtils_GetFields_m4859_ParameterInfos[] = 
{
	{"type", 0, 134220027, 0, &Type_t_0_0_0},
};
extern const Il2CppType IEnumerable_1_t1041_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.Generic.IEnumerable`1<System.Reflection.FieldInfo> SimpleJson.Reflection.ReflectionUtils::GetFields(System.Type)
extern const MethodInfo ReflectionUtils_GetFields_m4859_MethodInfo = 
{
	"GetFields"/* name */
	, (methodPointerType)&ReflectionUtils_GetFields_m4859/* method */
	, &ReflectionUtils_t946_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerable_1_t1041_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ReflectionUtils_t946_ReflectionUtils_GetFields_m4859_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1952/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PropertyInfo_t_0_0_0;
extern const Il2CppType PropertyInfo_t_0_0_0;
static const ParameterInfo ReflectionUtils_t946_ReflectionUtils_GetGetterMethodInfo_m4860_ParameterInfos[] = 
{
	{"propertyInfo", 0, 134220028, 0, &PropertyInfo_t_0_0_0},
};
extern const Il2CppType MethodInfo_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo SimpleJson.Reflection.ReflectionUtils::GetGetterMethodInfo(System.Reflection.PropertyInfo)
extern const MethodInfo ReflectionUtils_GetGetterMethodInfo_m4860_MethodInfo = 
{
	"GetGetterMethodInfo"/* name */
	, (methodPointerType)&ReflectionUtils_GetGetterMethodInfo_m4860/* method */
	, &ReflectionUtils_t946_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ReflectionUtils_t946_ReflectionUtils_GetGetterMethodInfo_m4860_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1953/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PropertyInfo_t_0_0_0;
static const ParameterInfo ReflectionUtils_t946_ReflectionUtils_GetSetterMethodInfo_m4861_ParameterInfos[] = 
{
	{"propertyInfo", 0, 134220029, 0, &PropertyInfo_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo SimpleJson.Reflection.ReflectionUtils::GetSetterMethodInfo(System.Reflection.PropertyInfo)
extern const MethodInfo ReflectionUtils_GetSetterMethodInfo_m4861_MethodInfo = 
{
	"GetSetterMethodInfo"/* name */
	, (methodPointerType)&ReflectionUtils_GetSetterMethodInfo_m4861/* method */
	, &ReflectionUtils_t946_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ReflectionUtils_t946_ReflectionUtils_GetSetterMethodInfo_m4861_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1954/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType TypeU5BU5D_t238_0_0_0;
static const ParameterInfo ReflectionUtils_t946_ReflectionUtils_GetContructor_m4862_ParameterInfos[] = 
{
	{"type", 0, 134220030, 0, &Type_t_0_0_0},
	{"argsType", 1, 134220031, 989, &TypeU5BU5D_t238_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate SimpleJson.Reflection.ReflectionUtils::GetContructor(System.Type,System.Type[])
extern const MethodInfo ReflectionUtils_GetContructor_m4862_MethodInfo = 
{
	"GetContructor"/* name */
	, (methodPointerType)&ReflectionUtils_GetContructor_m4862/* method */
	, &ReflectionUtils_t946_il2cpp_TypeInfo/* declaring_type */
	, &ConstructorDelegate_t939_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, ReflectionUtils_t946_ReflectionUtils_GetContructor_m4862_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1955/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ConstructorInfo_t940_0_0_0;
static const ParameterInfo ReflectionUtils_t946_ReflectionUtils_GetConstructorByReflection_m4863_ParameterInfos[] = 
{
	{"constructorInfo", 0, 134220032, 0, &ConstructorInfo_t940_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate SimpleJson.Reflection.ReflectionUtils::GetConstructorByReflection(System.Reflection.ConstructorInfo)
extern const MethodInfo ReflectionUtils_GetConstructorByReflection_m4863_MethodInfo = 
{
	"GetConstructorByReflection"/* name */
	, (methodPointerType)&ReflectionUtils_GetConstructorByReflection_m4863/* method */
	, &ReflectionUtils_t946_il2cpp_TypeInfo/* declaring_type */
	, &ConstructorDelegate_t939_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ReflectionUtils_t946_ReflectionUtils_GetConstructorByReflection_m4863_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1956/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType TypeU5BU5D_t238_0_0_0;
static const ParameterInfo ReflectionUtils_t946_ReflectionUtils_GetConstructorByReflection_m4864_ParameterInfos[] = 
{
	{"type", 0, 134220033, 0, &Type_t_0_0_0},
	{"argsType", 1, 134220034, 990, &TypeU5BU5D_t238_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate SimpleJson.Reflection.ReflectionUtils::GetConstructorByReflection(System.Type,System.Type[])
extern const MethodInfo ReflectionUtils_GetConstructorByReflection_m4864_MethodInfo = 
{
	"GetConstructorByReflection"/* name */
	, (methodPointerType)&ReflectionUtils_GetConstructorByReflection_m4864/* method */
	, &ReflectionUtils_t946_il2cpp_TypeInfo/* declaring_type */
	, &ConstructorDelegate_t939_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, ReflectionUtils_t946_ReflectionUtils_GetConstructorByReflection_m4864_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1957/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PropertyInfo_t_0_0_0;
static const ParameterInfo ReflectionUtils_t946_ReflectionUtils_GetGetMethod_m4865_ParameterInfos[] = 
{
	{"propertyInfo", 0, 134220035, 0, &PropertyInfo_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// SimpleJson.Reflection.ReflectionUtils/GetDelegate SimpleJson.Reflection.ReflectionUtils::GetGetMethod(System.Reflection.PropertyInfo)
extern const MethodInfo ReflectionUtils_GetGetMethod_m4865_MethodInfo = 
{
	"GetGetMethod"/* name */
	, (methodPointerType)&ReflectionUtils_GetGetMethod_m4865/* method */
	, &ReflectionUtils_t946_il2cpp_TypeInfo/* declaring_type */
	, &GetDelegate_t937_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ReflectionUtils_t946_ReflectionUtils_GetGetMethod_m4865_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1958/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType FieldInfo_t_0_0_0;
extern const Il2CppType FieldInfo_t_0_0_0;
static const ParameterInfo ReflectionUtils_t946_ReflectionUtils_GetGetMethod_m4866_ParameterInfos[] = 
{
	{"fieldInfo", 0, 134220036, 0, &FieldInfo_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// SimpleJson.Reflection.ReflectionUtils/GetDelegate SimpleJson.Reflection.ReflectionUtils::GetGetMethod(System.Reflection.FieldInfo)
extern const MethodInfo ReflectionUtils_GetGetMethod_m4866_MethodInfo = 
{
	"GetGetMethod"/* name */
	, (methodPointerType)&ReflectionUtils_GetGetMethod_m4866/* method */
	, &ReflectionUtils_t946_il2cpp_TypeInfo/* declaring_type */
	, &GetDelegate_t937_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ReflectionUtils_t946_ReflectionUtils_GetGetMethod_m4866_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1959/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PropertyInfo_t_0_0_0;
static const ParameterInfo ReflectionUtils_t946_ReflectionUtils_GetGetMethodByReflection_m4867_ParameterInfos[] = 
{
	{"propertyInfo", 0, 134220037, 0, &PropertyInfo_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// SimpleJson.Reflection.ReflectionUtils/GetDelegate SimpleJson.Reflection.ReflectionUtils::GetGetMethodByReflection(System.Reflection.PropertyInfo)
extern const MethodInfo ReflectionUtils_GetGetMethodByReflection_m4867_MethodInfo = 
{
	"GetGetMethodByReflection"/* name */
	, (methodPointerType)&ReflectionUtils_GetGetMethodByReflection_m4867/* method */
	, &ReflectionUtils_t946_il2cpp_TypeInfo/* declaring_type */
	, &GetDelegate_t937_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ReflectionUtils_t946_ReflectionUtils_GetGetMethodByReflection_m4867_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1960/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType FieldInfo_t_0_0_0;
static const ParameterInfo ReflectionUtils_t946_ReflectionUtils_GetGetMethodByReflection_m4868_ParameterInfos[] = 
{
	{"fieldInfo", 0, 134220038, 0, &FieldInfo_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// SimpleJson.Reflection.ReflectionUtils/GetDelegate SimpleJson.Reflection.ReflectionUtils::GetGetMethodByReflection(System.Reflection.FieldInfo)
extern const MethodInfo ReflectionUtils_GetGetMethodByReflection_m4868_MethodInfo = 
{
	"GetGetMethodByReflection"/* name */
	, (methodPointerType)&ReflectionUtils_GetGetMethodByReflection_m4868/* method */
	, &ReflectionUtils_t946_il2cpp_TypeInfo/* declaring_type */
	, &GetDelegate_t937_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ReflectionUtils_t946_ReflectionUtils_GetGetMethodByReflection_m4868_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1961/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PropertyInfo_t_0_0_0;
static const ParameterInfo ReflectionUtils_t946_ReflectionUtils_GetSetMethod_m4869_ParameterInfos[] = 
{
	{"propertyInfo", 0, 134220039, 0, &PropertyInfo_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// SimpleJson.Reflection.ReflectionUtils/SetDelegate SimpleJson.Reflection.ReflectionUtils::GetSetMethod(System.Reflection.PropertyInfo)
extern const MethodInfo ReflectionUtils_GetSetMethod_m4869_MethodInfo = 
{
	"GetSetMethod"/* name */
	, (methodPointerType)&ReflectionUtils_GetSetMethod_m4869/* method */
	, &ReflectionUtils_t946_il2cpp_TypeInfo/* declaring_type */
	, &SetDelegate_t938_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ReflectionUtils_t946_ReflectionUtils_GetSetMethod_m4869_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1962/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType FieldInfo_t_0_0_0;
static const ParameterInfo ReflectionUtils_t946_ReflectionUtils_GetSetMethod_m4870_ParameterInfos[] = 
{
	{"fieldInfo", 0, 134220040, 0, &FieldInfo_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// SimpleJson.Reflection.ReflectionUtils/SetDelegate SimpleJson.Reflection.ReflectionUtils::GetSetMethod(System.Reflection.FieldInfo)
extern const MethodInfo ReflectionUtils_GetSetMethod_m4870_MethodInfo = 
{
	"GetSetMethod"/* name */
	, (methodPointerType)&ReflectionUtils_GetSetMethod_m4870/* method */
	, &ReflectionUtils_t946_il2cpp_TypeInfo/* declaring_type */
	, &SetDelegate_t938_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ReflectionUtils_t946_ReflectionUtils_GetSetMethod_m4870_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1963/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PropertyInfo_t_0_0_0;
static const ParameterInfo ReflectionUtils_t946_ReflectionUtils_GetSetMethodByReflection_m4871_ParameterInfos[] = 
{
	{"propertyInfo", 0, 134220041, 0, &PropertyInfo_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// SimpleJson.Reflection.ReflectionUtils/SetDelegate SimpleJson.Reflection.ReflectionUtils::GetSetMethodByReflection(System.Reflection.PropertyInfo)
extern const MethodInfo ReflectionUtils_GetSetMethodByReflection_m4871_MethodInfo = 
{
	"GetSetMethodByReflection"/* name */
	, (methodPointerType)&ReflectionUtils_GetSetMethodByReflection_m4871/* method */
	, &ReflectionUtils_t946_il2cpp_TypeInfo/* declaring_type */
	, &SetDelegate_t938_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ReflectionUtils_t946_ReflectionUtils_GetSetMethodByReflection_m4871_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1964/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType FieldInfo_t_0_0_0;
static const ParameterInfo ReflectionUtils_t946_ReflectionUtils_GetSetMethodByReflection_m4872_ParameterInfos[] = 
{
	{"fieldInfo", 0, 134220042, 0, &FieldInfo_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// SimpleJson.Reflection.ReflectionUtils/SetDelegate SimpleJson.Reflection.ReflectionUtils::GetSetMethodByReflection(System.Reflection.FieldInfo)
extern const MethodInfo ReflectionUtils_GetSetMethodByReflection_m4872_MethodInfo = 
{
	"GetSetMethodByReflection"/* name */
	, (methodPointerType)&ReflectionUtils_GetSetMethodByReflection_m4872/* method */
	, &ReflectionUtils_t946_il2cpp_TypeInfo/* declaring_type */
	, &SetDelegate_t938_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ReflectionUtils_t946_ReflectionUtils_GetSetMethodByReflection_m4872_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1965/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ReflectionUtils_t946_MethodInfos[] =
{
	&ReflectionUtils__cctor_m4855_MethodInfo,
	&ReflectionUtils_GetConstructors_m4856_MethodInfo,
	&ReflectionUtils_GetConstructorInfo_m4857_MethodInfo,
	&ReflectionUtils_GetProperties_m4858_MethodInfo,
	&ReflectionUtils_GetFields_m4859_MethodInfo,
	&ReflectionUtils_GetGetterMethodInfo_m4860_MethodInfo,
	&ReflectionUtils_GetSetterMethodInfo_m4861_MethodInfo,
	&ReflectionUtils_GetContructor_m4862_MethodInfo,
	&ReflectionUtils_GetConstructorByReflection_m4863_MethodInfo,
	&ReflectionUtils_GetConstructorByReflection_m4864_MethodInfo,
	&ReflectionUtils_GetGetMethod_m4865_MethodInfo,
	&ReflectionUtils_GetGetMethod_m4866_MethodInfo,
	&ReflectionUtils_GetGetMethodByReflection_m4867_MethodInfo,
	&ReflectionUtils_GetGetMethodByReflection_m4868_MethodInfo,
	&ReflectionUtils_GetSetMethod_m4869_MethodInfo,
	&ReflectionUtils_GetSetMethod_m4870_MethodInfo,
	&ReflectionUtils_GetSetMethodByReflection_m4871_MethodInfo,
	&ReflectionUtils_GetSetMethodByReflection_m4872_MethodInfo,
	NULL
};
static const Il2CppType* ReflectionUtils_t946_il2cpp_TypeInfo__nestedTypes[10] =
{
	&ThreadSafeDictionary_2_t1121_0_0_0,
	&GetDelegate_t937_0_0_0,
	&SetDelegate_t938_0_0_0,
	&ConstructorDelegate_t939_0_0_0,
	&ThreadSafeDictionaryValueFactory_2_t1122_0_0_0,
	&U3CGetConstructorByReflectionU3Ec__AnonStorey1_t941_0_0_0,
	&U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t942_0_0_0,
	&U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t943_0_0_0,
	&U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t944_0_0_0,
	&U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t945_0_0_0,
};
static const Il2CppMethodReference ReflectionUtils_t946_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool ReflectionUtils_t946_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ReflectionUtils_t946_1_0_0;
struct ReflectionUtils_t946;
const Il2CppTypeDefinitionMetadata ReflectionUtils_t946_DefinitionMetadata = 
{
	NULL/* declaringType */
	, ReflectionUtils_t946_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ReflectionUtils_t946_VTable/* vtableMethods */
	, ReflectionUtils_t946_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1115/* fieldStart */

};
TypeInfo ReflectionUtils_t946_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ReflectionUtils"/* name */
	, "SimpleJson.Reflection"/* namespaze */
	, ReflectionUtils_t946_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ReflectionUtils_t946_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 987/* custom_attributes_cache */
	, &ReflectionUtils_t946_0_0_0/* byval_arg */
	, &ReflectionUtils_t946_1_0_0/* this_arg */
	, &ReflectionUtils_t946_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ReflectionUtils_t946)/* instance_size */
	, sizeof (ReflectionUtils_t946)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ReflectionUtils_t946_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 18/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 10/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.WrapperlessIcall
#include "UnityEngine_UnityEngine_WrapperlessIcall.h"
// Metadata Definition UnityEngine.WrapperlessIcall
extern TypeInfo WrapperlessIcall_t947_il2cpp_TypeInfo;
// UnityEngine.WrapperlessIcall
#include "UnityEngine_UnityEngine_WrapperlessIcallMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.WrapperlessIcall::.ctor()
extern const MethodInfo WrapperlessIcall__ctor_m4873_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&WrapperlessIcall__ctor_m4873/* method */
	, &WrapperlessIcall_t947_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2011/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* WrapperlessIcall_t947_MethodInfos[] =
{
	&WrapperlessIcall__ctor_m4873_MethodInfo,
	NULL
};
extern const MethodInfo Attribute_Equals_m5384_MethodInfo;
extern const MethodInfo Attribute_GetHashCode_m5253_MethodInfo;
static const Il2CppMethodReference WrapperlessIcall_t947_VTable[] =
{
	&Attribute_Equals_m5384_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Attribute_GetHashCode_m5253_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool WrapperlessIcall_t947_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern const Il2CppType _Attribute_t1145_0_0_0;
static Il2CppInterfaceOffsetPair WrapperlessIcall_t947_InterfacesOffsets[] = 
{
	{ &_Attribute_t1145_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType WrapperlessIcall_t947_0_0_0;
extern const Il2CppType WrapperlessIcall_t947_1_0_0;
extern const Il2CppType Attribute_t805_0_0_0;
struct WrapperlessIcall_t947;
const Il2CppTypeDefinitionMetadata WrapperlessIcall_t947_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, WrapperlessIcall_t947_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t805_0_0_0/* parent */
	, WrapperlessIcall_t947_VTable/* vtableMethods */
	, WrapperlessIcall_t947_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo WrapperlessIcall_t947_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "WrapperlessIcall"/* name */
	, "UnityEngine"/* namespaze */
	, WrapperlessIcall_t947_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &WrapperlessIcall_t947_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &WrapperlessIcall_t947_0_0_0/* byval_arg */
	, &WrapperlessIcall_t947_1_0_0/* this_arg */
	, &WrapperlessIcall_t947_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WrapperlessIcall_t947)/* instance_size */
	, sizeof (WrapperlessIcall_t947)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.IL2CPPStructAlignmentAttribute
#include "UnityEngine_UnityEngine_IL2CPPStructAlignmentAttribute.h"
// Metadata Definition UnityEngine.IL2CPPStructAlignmentAttribute
extern TypeInfo IL2CPPStructAlignmentAttribute_t948_il2cpp_TypeInfo;
// UnityEngine.IL2CPPStructAlignmentAttribute
#include "UnityEngine_UnityEngine_IL2CPPStructAlignmentAttributeMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.IL2CPPStructAlignmentAttribute::.ctor()
extern const MethodInfo IL2CPPStructAlignmentAttribute__ctor_m4874_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&IL2CPPStructAlignmentAttribute__ctor_m4874/* method */
	, &IL2CPPStructAlignmentAttribute_t948_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2012/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IL2CPPStructAlignmentAttribute_t948_MethodInfos[] =
{
	&IL2CPPStructAlignmentAttribute__ctor_m4874_MethodInfo,
	NULL
};
static const Il2CppMethodReference IL2CPPStructAlignmentAttribute_t948_VTable[] =
{
	&Attribute_Equals_m5384_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Attribute_GetHashCode_m5253_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool IL2CPPStructAlignmentAttribute_t948_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair IL2CPPStructAlignmentAttribute_t948_InterfacesOffsets[] = 
{
	{ &_Attribute_t1145_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType IL2CPPStructAlignmentAttribute_t948_0_0_0;
extern const Il2CppType IL2CPPStructAlignmentAttribute_t948_1_0_0;
struct IL2CPPStructAlignmentAttribute_t948;
const Il2CppTypeDefinitionMetadata IL2CPPStructAlignmentAttribute_t948_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, IL2CPPStructAlignmentAttribute_t948_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t805_0_0_0/* parent */
	, IL2CPPStructAlignmentAttribute_t948_VTable/* vtableMethods */
	, IL2CPPStructAlignmentAttribute_t948_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1116/* fieldStart */

};
TypeInfo IL2CPPStructAlignmentAttribute_t948_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "IL2CPPStructAlignmentAttribute"/* name */
	, "UnityEngine"/* namespaze */
	, IL2CPPStructAlignmentAttribute_t948_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IL2CPPStructAlignmentAttribute_t948_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 999/* custom_attributes_cache */
	, &IL2CPPStructAlignmentAttribute_t948_0_0_0/* byval_arg */
	, &IL2CPPStructAlignmentAttribute_t948_1_0_0/* this_arg */
	, &IL2CPPStructAlignmentAttribute_t948_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (IL2CPPStructAlignmentAttribute_t948)/* instance_size */
	, sizeof (IL2CPPStructAlignmentAttribute_t948)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.AttributeHelperEngine
#include "UnityEngine_UnityEngine_AttributeHelperEngine.h"
// Metadata Definition UnityEngine.AttributeHelperEngine
extern TypeInfo AttributeHelperEngine_t952_il2cpp_TypeInfo;
// UnityEngine.AttributeHelperEngine
#include "UnityEngine_UnityEngine_AttributeHelperEngineMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.AttributeHelperEngine::.cctor()
extern const MethodInfo AttributeHelperEngine__cctor_m4875_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&AttributeHelperEngine__cctor_m4875/* method */
	, &AttributeHelperEngine_t952_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2013/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo AttributeHelperEngine_t952_AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m4876_ParameterInfos[] = 
{
	{"type", 0, 134220096, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type UnityEngine.AttributeHelperEngine::GetParentTypeDisallowingMultipleInclusion(System.Type)
extern const MethodInfo AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m4876_MethodInfo = 
{
	"GetParentTypeDisallowingMultipleInclusion"/* name */
	, (methodPointerType)&AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m4876/* method */
	, &AttributeHelperEngine_t952_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, AttributeHelperEngine_t952_AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m4876_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2014/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo AttributeHelperEngine_t952_AttributeHelperEngine_GetRequiredComponents_m4877_ParameterInfos[] = 
{
	{"klass", 0, 134220097, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type[] UnityEngine.AttributeHelperEngine::GetRequiredComponents(System.Type)
extern const MethodInfo AttributeHelperEngine_GetRequiredComponents_m4877_MethodInfo = 
{
	"GetRequiredComponents"/* name */
	, (methodPointerType)&AttributeHelperEngine_GetRequiredComponents_m4877/* method */
	, &AttributeHelperEngine_t952_il2cpp_TypeInfo/* declaring_type */
	, &TypeU5BU5D_t238_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, AttributeHelperEngine_t952_AttributeHelperEngine_GetRequiredComponents_m4877_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2015/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo AttributeHelperEngine_t952_AttributeHelperEngine_CheckIsEditorScript_m4878_ParameterInfos[] = 
{
	{"klass", 0, 134220098, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.AttributeHelperEngine::CheckIsEditorScript(System.Type)
extern const MethodInfo AttributeHelperEngine_CheckIsEditorScript_m4878_MethodInfo = 
{
	"CheckIsEditorScript"/* name */
	, (methodPointerType)&AttributeHelperEngine_CheckIsEditorScript_m4878/* method */
	, &AttributeHelperEngine_t952_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, AttributeHelperEngine_t952_AttributeHelperEngine_CheckIsEditorScript_m4878_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2016/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AttributeHelperEngine_t952_MethodInfos[] =
{
	&AttributeHelperEngine__cctor_m4875_MethodInfo,
	&AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m4876_MethodInfo,
	&AttributeHelperEngine_GetRequiredComponents_m4877_MethodInfo,
	&AttributeHelperEngine_CheckIsEditorScript_m4878_MethodInfo,
	NULL
};
static const Il2CppMethodReference AttributeHelperEngine_t952_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool AttributeHelperEngine_t952_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType AttributeHelperEngine_t952_0_0_0;
extern const Il2CppType AttributeHelperEngine_t952_1_0_0;
struct AttributeHelperEngine_t952;
const Il2CppTypeDefinitionMetadata AttributeHelperEngine_t952_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, AttributeHelperEngine_t952_VTable/* vtableMethods */
	, AttributeHelperEngine_t952_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1117/* fieldStart */

};
TypeInfo AttributeHelperEngine_t952_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "AttributeHelperEngine"/* name */
	, "UnityEngine"/* namespaze */
	, AttributeHelperEngine_t952_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &AttributeHelperEngine_t952_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AttributeHelperEngine_t952_0_0_0/* byval_arg */
	, &AttributeHelperEngine_t952_1_0_0/* this_arg */
	, &AttributeHelperEngine_t952_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AttributeHelperEngine_t952)/* instance_size */
	, sizeof (AttributeHelperEngine_t952)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(AttributeHelperEngine_t952_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.DisallowMultipleComponent
#include "UnityEngine_UnityEngine_DisallowMultipleComponent.h"
// Metadata Definition UnityEngine.DisallowMultipleComponent
extern TypeInfo DisallowMultipleComponent_t724_il2cpp_TypeInfo;
// UnityEngine.DisallowMultipleComponent
#include "UnityEngine_UnityEngine_DisallowMultipleComponentMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.DisallowMultipleComponent::.ctor()
extern const MethodInfo DisallowMultipleComponent__ctor_m3528_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DisallowMultipleComponent__ctor_m3528/* method */
	, &DisallowMultipleComponent_t724_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2017/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* DisallowMultipleComponent_t724_MethodInfos[] =
{
	&DisallowMultipleComponent__ctor_m3528_MethodInfo,
	NULL
};
static const Il2CppMethodReference DisallowMultipleComponent_t724_VTable[] =
{
	&Attribute_Equals_m5384_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Attribute_GetHashCode_m5253_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool DisallowMultipleComponent_t724_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair DisallowMultipleComponent_t724_InterfacesOffsets[] = 
{
	{ &_Attribute_t1145_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType DisallowMultipleComponent_t724_0_0_0;
extern const Il2CppType DisallowMultipleComponent_t724_1_0_0;
struct DisallowMultipleComponent_t724;
const Il2CppTypeDefinitionMetadata DisallowMultipleComponent_t724_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DisallowMultipleComponent_t724_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t805_0_0_0/* parent */
	, DisallowMultipleComponent_t724_VTable/* vtableMethods */
	, DisallowMultipleComponent_t724_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo DisallowMultipleComponent_t724_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "DisallowMultipleComponent"/* name */
	, "UnityEngine"/* namespaze */
	, DisallowMultipleComponent_t724_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &DisallowMultipleComponent_t724_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1000/* custom_attributes_cache */
	, &DisallowMultipleComponent_t724_0_0_0/* byval_arg */
	, &DisallowMultipleComponent_t724_1_0_0/* this_arg */
	, &DisallowMultipleComponent_t724_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DisallowMultipleComponent_t724)/* instance_size */
	, sizeof (DisallowMultipleComponent_t724)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.RequireComponent
#include "UnityEngine_UnityEngine_RequireComponent.h"
// Metadata Definition UnityEngine.RequireComponent
extern TypeInfo RequireComponent_t259_il2cpp_TypeInfo;
// UnityEngine.RequireComponent
#include "UnityEngine_UnityEngine_RequireComponentMethodDeclarations.h"
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo RequireComponent_t259_RequireComponent__ctor_m1023_ParameterInfos[] = 
{
	{"requiredComponent", 0, 134220099, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.RequireComponent::.ctor(System.Type)
extern const MethodInfo RequireComponent__ctor_m1023_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RequireComponent__ctor_m1023/* method */
	, &RequireComponent_t259_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, RequireComponent_t259_RequireComponent__ctor_m1023_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2018/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* RequireComponent_t259_MethodInfos[] =
{
	&RequireComponent__ctor_m1023_MethodInfo,
	NULL
};
static const Il2CppMethodReference RequireComponent_t259_VTable[] =
{
	&Attribute_Equals_m5384_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Attribute_GetHashCode_m5253_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool RequireComponent_t259_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair RequireComponent_t259_InterfacesOffsets[] = 
{
	{ &_Attribute_t1145_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType RequireComponent_t259_0_0_0;
extern const Il2CppType RequireComponent_t259_1_0_0;
struct RequireComponent_t259;
const Il2CppTypeDefinitionMetadata RequireComponent_t259_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RequireComponent_t259_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t805_0_0_0/* parent */
	, RequireComponent_t259_VTable/* vtableMethods */
	, RequireComponent_t259_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1120/* fieldStart */

};
TypeInfo RequireComponent_t259_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "RequireComponent"/* name */
	, "UnityEngine"/* namespaze */
	, RequireComponent_t259_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &RequireComponent_t259_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1001/* custom_attributes_cache */
	, &RequireComponent_t259_0_0_0/* byval_arg */
	, &RequireComponent_t259_1_0_0/* this_arg */
	, &RequireComponent_t259_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RequireComponent_t259)/* instance_size */
	, sizeof (RequireComponent_t259)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.AddComponentMenu
#include "UnityEngine_UnityEngine_AddComponentMenu.h"
// Metadata Definition UnityEngine.AddComponentMenu
extern TypeInfo AddComponentMenu_t264_il2cpp_TypeInfo;
// UnityEngine.AddComponentMenu
#include "UnityEngine_UnityEngine_AddComponentMenuMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo AddComponentMenu_t264_AddComponentMenu__ctor_m1040_ParameterInfos[] = 
{
	{"menuName", 0, 134220100, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.AddComponentMenu::.ctor(System.String)
extern const MethodInfo AddComponentMenu__ctor_m1040_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AddComponentMenu__ctor_m1040/* method */
	, &AddComponentMenu_t264_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, AddComponentMenu_t264_AddComponentMenu__ctor_m1040_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2019/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo AddComponentMenu_t264_AddComponentMenu__ctor_m3524_ParameterInfos[] = 
{
	{"menuName", 0, 134220101, 0, &String_t_0_0_0},
	{"order", 1, 134220102, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.AddComponentMenu::.ctor(System.String,System.Int32)
extern const MethodInfo AddComponentMenu__ctor_m3524_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AddComponentMenu__ctor_m3524/* method */
	, &AddComponentMenu_t264_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Int32_t253/* invoker_method */
	, AddComponentMenu_t264_AddComponentMenu__ctor_m3524_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2020/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AddComponentMenu_t264_MethodInfos[] =
{
	&AddComponentMenu__ctor_m1040_MethodInfo,
	&AddComponentMenu__ctor_m3524_MethodInfo,
	NULL
};
static const Il2CppMethodReference AddComponentMenu_t264_VTable[] =
{
	&Attribute_Equals_m5384_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Attribute_GetHashCode_m5253_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool AddComponentMenu_t264_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair AddComponentMenu_t264_InterfacesOffsets[] = 
{
	{ &_Attribute_t1145_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType AddComponentMenu_t264_0_0_0;
extern const Il2CppType AddComponentMenu_t264_1_0_0;
struct AddComponentMenu_t264;
const Il2CppTypeDefinitionMetadata AddComponentMenu_t264_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AddComponentMenu_t264_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t805_0_0_0/* parent */
	, AddComponentMenu_t264_VTable/* vtableMethods */
	, AddComponentMenu_t264_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1123/* fieldStart */

};
TypeInfo AddComponentMenu_t264_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "AddComponentMenu"/* name */
	, "UnityEngine"/* namespaze */
	, AddComponentMenu_t264_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &AddComponentMenu_t264_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AddComponentMenu_t264_0_0_0/* byval_arg */
	, &AddComponentMenu_t264_1_0_0/* this_arg */
	, &AddComponentMenu_t264_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AddComponentMenu_t264)/* instance_size */
	, sizeof (AddComponentMenu_t264)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.ExecuteInEditMode
#include "UnityEngine_UnityEngine_ExecuteInEditMode.h"
// Metadata Definition UnityEngine.ExecuteInEditMode
extern TypeInfo ExecuteInEditMode_t262_il2cpp_TypeInfo;
// UnityEngine.ExecuteInEditMode
#include "UnityEngine_UnityEngine_ExecuteInEditModeMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.ExecuteInEditMode::.ctor()
extern const MethodInfo ExecuteInEditMode__ctor_m1027_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ExecuteInEditMode__ctor_m1027/* method */
	, &ExecuteInEditMode_t262_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2021/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ExecuteInEditMode_t262_MethodInfos[] =
{
	&ExecuteInEditMode__ctor_m1027_MethodInfo,
	NULL
};
static const Il2CppMethodReference ExecuteInEditMode_t262_VTable[] =
{
	&Attribute_Equals_m5384_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Attribute_GetHashCode_m5253_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool ExecuteInEditMode_t262_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ExecuteInEditMode_t262_InterfacesOffsets[] = 
{
	{ &_Attribute_t1145_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ExecuteInEditMode_t262_0_0_0;
extern const Il2CppType ExecuteInEditMode_t262_1_0_0;
struct ExecuteInEditMode_t262;
const Il2CppTypeDefinitionMetadata ExecuteInEditMode_t262_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ExecuteInEditMode_t262_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t805_0_0_0/* parent */
	, ExecuteInEditMode_t262_VTable/* vtableMethods */
	, ExecuteInEditMode_t262_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ExecuteInEditMode_t262_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ExecuteInEditMode"/* name */
	, "UnityEngine"/* namespaze */
	, ExecuteInEditMode_t262_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ExecuteInEditMode_t262_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ExecuteInEditMode_t262_0_0_0/* byval_arg */
	, &ExecuteInEditMode_t262_1_0_0/* this_arg */
	, &ExecuteInEditMode_t262_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ExecuteInEditMode_t262)/* instance_size */
	, sizeof (ExecuteInEditMode_t262)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.HideInInspector
#include "UnityEngine_UnityEngine_HideInInspector.h"
// Metadata Definition UnityEngine.HideInInspector
extern TypeInfo HideInInspector_t270_il2cpp_TypeInfo;
// UnityEngine.HideInInspector
#include "UnityEngine_UnityEngine_HideInInspectorMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.HideInInspector::.ctor()
extern const MethodInfo HideInInspector__ctor_m1046_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&HideInInspector__ctor_m1046/* method */
	, &HideInInspector_t270_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2022/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* HideInInspector_t270_MethodInfos[] =
{
	&HideInInspector__ctor_m1046_MethodInfo,
	NULL
};
static const Il2CppMethodReference HideInInspector_t270_VTable[] =
{
	&Attribute_Equals_m5384_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Attribute_GetHashCode_m5253_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool HideInInspector_t270_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair HideInInspector_t270_InterfacesOffsets[] = 
{
	{ &_Attribute_t1145_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType HideInInspector_t270_0_0_0;
extern const Il2CppType HideInInspector_t270_1_0_0;
struct HideInInspector_t270;
const Il2CppTypeDefinitionMetadata HideInInspector_t270_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, HideInInspector_t270_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t805_0_0_0/* parent */
	, HideInInspector_t270_VTable/* vtableMethods */
	, HideInInspector_t270_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo HideInInspector_t270_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "HideInInspector"/* name */
	, "UnityEngine"/* namespaze */
	, HideInInspector_t270_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &HideInInspector_t270_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &HideInInspector_t270_0_0_0/* byval_arg */
	, &HideInInspector_t270_1_0_0/* this_arg */
	, &HideInInspector_t270_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HideInInspector_t270)/* instance_size */
	, sizeof (HideInInspector_t270)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition UnityEngine.CastHelper`1
extern TypeInfo CastHelper_1_t1124_il2cpp_TypeInfo;
extern const Il2CppGenericContainer CastHelper_1_t1124_Il2CppGenericContainer;
extern TypeInfo CastHelper_1_t1124_gp_T_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter CastHelper_1_t1124_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &CastHelper_1_t1124_Il2CppGenericContainer, NULL, "T", 0, 0 };
static const Il2CppGenericParameter* CastHelper_1_t1124_Il2CppGenericParametersArray[1] = 
{
	&CastHelper_1_t1124_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer CastHelper_1_t1124_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&CastHelper_1_t1124_il2cpp_TypeInfo, 1, 0, CastHelper_1_t1124_Il2CppGenericParametersArray };
static const MethodInfo* CastHelper_1_t1124_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference CastHelper_1_t1124_VTable[] =
{
	&ValueType_Equals_m1076_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&ValueType_GetHashCode_m1077_MethodInfo,
	&ValueType_ToString_m1078_MethodInfo,
};
static bool CastHelper_1_t1124_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType CastHelper_1_t1124_0_0_0;
extern const Il2CppType CastHelper_1_t1124_1_0_0;
const Il2CppTypeDefinitionMetadata CastHelper_1_t1124_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t285_0_0_0/* parent */
	, CastHelper_1_t1124_VTable/* vtableMethods */
	, CastHelper_1_t1124_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1125/* fieldStart */

};
TypeInfo CastHelper_1_t1124_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CastHelper`1"/* name */
	, "UnityEngine"/* namespaze */
	, CastHelper_1_t1124_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CastHelper_1_t1124_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CastHelper_1_t1124_0_0_0/* byval_arg */
	, &CastHelper_1_t1124_1_0_0/* this_arg */
	, &CastHelper_1_t1124_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &CastHelper_1_t1124_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.SetupCoroutine
#include "UnityEngine_UnityEngine_SetupCoroutine.h"
// Metadata Definition UnityEngine.SetupCoroutine
extern TypeInfo SetupCoroutine_t953_il2cpp_TypeInfo;
// UnityEngine.SetupCoroutine
#include "UnityEngine_UnityEngine_SetupCoroutineMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SetupCoroutine::.ctor()
extern const MethodInfo SetupCoroutine__ctor_m4879_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SetupCoroutine__ctor_m4879/* method */
	, &SetupCoroutine_t953_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2023/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo SetupCoroutine_t953_SetupCoroutine_InvokeMember_m4880_ParameterInfos[] = 
{
	{"behaviour", 0, 134220103, 0, &Object_t_0_0_0},
	{"name", 1, 134220104, 0, &String_t_0_0_0},
	{"variable", 2, 134220105, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object UnityEngine.SetupCoroutine::InvokeMember(System.Object,System.String,System.Object)
extern const MethodInfo SetupCoroutine_InvokeMember_m4880_MethodInfo = 
{
	"InvokeMember"/* name */
	, (methodPointerType)&SetupCoroutine_InvokeMember_m4880/* method */
	, &SetupCoroutine_t953_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, SetupCoroutine_t953_SetupCoroutine_InvokeMember_m4880_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2024/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo SetupCoroutine_t953_SetupCoroutine_InvokeStatic_m4881_ParameterInfos[] = 
{
	{"klass", 0, 134220106, 0, &Type_t_0_0_0},
	{"name", 1, 134220107, 0, &String_t_0_0_0},
	{"variable", 2, 134220108, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object UnityEngine.SetupCoroutine::InvokeStatic(System.Type,System.String,System.Object)
extern const MethodInfo SetupCoroutine_InvokeStatic_m4881_MethodInfo = 
{
	"InvokeStatic"/* name */
	, (methodPointerType)&SetupCoroutine_InvokeStatic_m4881/* method */
	, &SetupCoroutine_t953_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, SetupCoroutine_t953_SetupCoroutine_InvokeStatic_m4881_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2025/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SetupCoroutine_t953_MethodInfos[] =
{
	&SetupCoroutine__ctor_m4879_MethodInfo,
	&SetupCoroutine_InvokeMember_m4880_MethodInfo,
	&SetupCoroutine_InvokeStatic_m4881_MethodInfo,
	NULL
};
static const Il2CppMethodReference SetupCoroutine_t953_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool SetupCoroutine_t953_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType SetupCoroutine_t953_0_0_0;
extern const Il2CppType SetupCoroutine_t953_1_0_0;
struct SetupCoroutine_t953;
const Il2CppTypeDefinitionMetadata SetupCoroutine_t953_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SetupCoroutine_t953_VTable/* vtableMethods */
	, SetupCoroutine_t953_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo SetupCoroutine_t953_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SetupCoroutine"/* name */
	, "UnityEngine"/* namespaze */
	, SetupCoroutine_t953_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SetupCoroutine_t953_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SetupCoroutine_t953_0_0_0/* byval_arg */
	, &SetupCoroutine_t953_1_0_0/* this_arg */
	, &SetupCoroutine_t953_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SetupCoroutine_t953)/* instance_size */
	, sizeof (SetupCoroutine_t953)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.WritableAttribute
#include "UnityEngine_UnityEngine_WritableAttribute.h"
// Metadata Definition UnityEngine.WritableAttribute
extern TypeInfo WritableAttribute_t954_il2cpp_TypeInfo;
// UnityEngine.WritableAttribute
#include "UnityEngine_UnityEngine_WritableAttributeMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.WritableAttribute::.ctor()
extern const MethodInfo WritableAttribute__ctor_m4882_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&WritableAttribute__ctor_m4882/* method */
	, &WritableAttribute_t954_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2026/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* WritableAttribute_t954_MethodInfos[] =
{
	&WritableAttribute__ctor_m4882_MethodInfo,
	NULL
};
static const Il2CppMethodReference WritableAttribute_t954_VTable[] =
{
	&Attribute_Equals_m5384_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Attribute_GetHashCode_m5253_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool WritableAttribute_t954_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair WritableAttribute_t954_InterfacesOffsets[] = 
{
	{ &_Attribute_t1145_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType WritableAttribute_t954_0_0_0;
extern const Il2CppType WritableAttribute_t954_1_0_0;
struct WritableAttribute_t954;
const Il2CppTypeDefinitionMetadata WritableAttribute_t954_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, WritableAttribute_t954_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t805_0_0_0/* parent */
	, WritableAttribute_t954_VTable/* vtableMethods */
	, WritableAttribute_t954_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo WritableAttribute_t954_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "WritableAttribute"/* name */
	, "UnityEngine"/* namespaze */
	, WritableAttribute_t954_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &WritableAttribute_t954_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1002/* custom_attributes_cache */
	, &WritableAttribute_t954_0_0_0/* byval_arg */
	, &WritableAttribute_t954_1_0_0/* this_arg */
	, &WritableAttribute_t954_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WritableAttribute_t954)/* instance_size */
	, sizeof (WritableAttribute_t954)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.AssemblyIsEditorAssembly
#include "UnityEngine_UnityEngine_AssemblyIsEditorAssembly.h"
// Metadata Definition UnityEngine.AssemblyIsEditorAssembly
extern TypeInfo AssemblyIsEditorAssembly_t955_il2cpp_TypeInfo;
// UnityEngine.AssemblyIsEditorAssembly
#include "UnityEngine_UnityEngine_AssemblyIsEditorAssemblyMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.AssemblyIsEditorAssembly::.ctor()
extern const MethodInfo AssemblyIsEditorAssembly__ctor_m4883_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AssemblyIsEditorAssembly__ctor_m4883/* method */
	, &AssemblyIsEditorAssembly_t955_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2027/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AssemblyIsEditorAssembly_t955_MethodInfos[] =
{
	&AssemblyIsEditorAssembly__ctor_m4883_MethodInfo,
	NULL
};
static const Il2CppMethodReference AssemblyIsEditorAssembly_t955_VTable[] =
{
	&Attribute_Equals_m5384_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Attribute_GetHashCode_m5253_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool AssemblyIsEditorAssembly_t955_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair AssemblyIsEditorAssembly_t955_InterfacesOffsets[] = 
{
	{ &_Attribute_t1145_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType AssemblyIsEditorAssembly_t955_0_0_0;
extern const Il2CppType AssemblyIsEditorAssembly_t955_1_0_0;
struct AssemblyIsEditorAssembly_t955;
const Il2CppTypeDefinitionMetadata AssemblyIsEditorAssembly_t955_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AssemblyIsEditorAssembly_t955_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t805_0_0_0/* parent */
	, AssemblyIsEditorAssembly_t955_VTable/* vtableMethods */
	, AssemblyIsEditorAssembly_t955_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo AssemblyIsEditorAssembly_t955_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "AssemblyIsEditorAssembly"/* name */
	, "UnityEngine"/* namespaze */
	, AssemblyIsEditorAssembly_t955_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &AssemblyIsEditorAssembly_t955_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1003/* custom_attributes_cache */
	, &AssemblyIsEditorAssembly_t955_0_0_0/* byval_arg */
	, &AssemblyIsEditorAssembly_t955_1_0_0/* this_arg */
	, &AssemblyIsEditorAssembly_t955_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AssemblyIsEditorAssembly_t955)/* instance_size */
	, sizeof (AssemblyIsEditorAssembly_t955)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcUserPro.h"
// Metadata Definition UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData
extern TypeInfo GcUserProfileData_t956_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcUserProMethodDeclarations.h"
extern const Il2CppType UserProfile_t976_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.SocialPlatforms.Impl.UserProfile UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData::ToUserProfile()
extern const MethodInfo GcUserProfileData_ToUserProfile_m4884_MethodInfo = 
{
	"ToUserProfile"/* name */
	, (methodPointerType)&GcUserProfileData_ToUserProfile_m4884/* method */
	, &GcUserProfileData_t956_il2cpp_TypeInfo/* declaring_type */
	, &UserProfile_t976_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2028/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UserProfileU5BU5D_t793_1_0_0;
extern const Il2CppType UserProfileU5BU5D_t793_1_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo GcUserProfileData_t956_GcUserProfileData_AddToArray_m4885_ParameterInfos[] = 
{
	{"array", 0, 134220109, 0, &UserProfileU5BU5D_t793_1_0_0},
	{"number", 1, 134220110, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_UserProfileU5BU5DU26_t1141_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData::AddToArray(UnityEngine.SocialPlatforms.Impl.UserProfile[]&,System.Int32)
extern const MethodInfo GcUserProfileData_AddToArray_m4885_MethodInfo = 
{
	"AddToArray"/* name */
	, (methodPointerType)&GcUserProfileData_AddToArray_m4885/* method */
	, &GcUserProfileData_t956_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_UserProfileU5BU5DU26_t1141_Int32_t253/* invoker_method */
	, GcUserProfileData_t956_GcUserProfileData_AddToArray_m4885_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2029/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* GcUserProfileData_t956_MethodInfos[] =
{
	&GcUserProfileData_ToUserProfile_m4884_MethodInfo,
	&GcUserProfileData_AddToArray_m4885_MethodInfo,
	NULL
};
static const Il2CppMethodReference GcUserProfileData_t956_VTable[] =
{
	&ValueType_Equals_m1076_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&ValueType_GetHashCode_m1077_MethodInfo,
	&ValueType_ToString_m1078_MethodInfo,
};
static bool GcUserProfileData_t956_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GcUserProfileData_t956_0_0_0;
extern const Il2CppType GcUserProfileData_t956_1_0_0;
const Il2CppTypeDefinitionMetadata GcUserProfileData_t956_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t285_0_0_0/* parent */
	, GcUserProfileData_t956_VTable/* vtableMethods */
	, GcUserProfileData_t956_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1127/* fieldStart */

};
TypeInfo GcUserProfileData_t956_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GcUserProfileData"/* name */
	, "UnityEngine.SocialPlatforms.GameCenter"/* namespaze */
	, GcUserProfileData_t956_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &GcUserProfileData_t956_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GcUserProfileData_t956_0_0_0/* byval_arg */
	, &GcUserProfileData_t956_1_0_0/* this_arg */
	, &GcUserProfileData_t956_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GcUserProfileData_t956)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (GcUserProfileData_t956)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve.h"
// Metadata Definition UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData
extern TypeInfo GcAchievementDescriptionData_t957_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieveMethodDeclarations.h"
extern const Il2CppType AchievementDescription_t978_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.SocialPlatforms.Impl.AchievementDescription UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData::ToAchievementDescription()
extern const MethodInfo GcAchievementDescriptionData_ToAchievementDescription_m4886_MethodInfo = 
{
	"ToAchievementDescription"/* name */
	, (methodPointerType)&GcAchievementDescriptionData_ToAchievementDescription_m4886/* method */
	, &GcAchievementDescriptionData_t957_il2cpp_TypeInfo/* declaring_type */
	, &AchievementDescription_t978_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2030/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* GcAchievementDescriptionData_t957_MethodInfos[] =
{
	&GcAchievementDescriptionData_ToAchievementDescription_m4886_MethodInfo,
	NULL
};
static const Il2CppMethodReference GcAchievementDescriptionData_t957_VTable[] =
{
	&ValueType_Equals_m1076_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&ValueType_GetHashCode_m1077_MethodInfo,
	&ValueType_ToString_m1078_MethodInfo,
};
static bool GcAchievementDescriptionData_t957_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GcAchievementDescriptionData_t957_0_0_0;
extern const Il2CppType GcAchievementDescriptionData_t957_1_0_0;
const Il2CppTypeDefinitionMetadata GcAchievementDescriptionData_t957_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t285_0_0_0/* parent */
	, GcAchievementDescriptionData_t957_VTable/* vtableMethods */
	, GcAchievementDescriptionData_t957_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1131/* fieldStart */

};
TypeInfo GcAchievementDescriptionData_t957_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GcAchievementDescriptionData"/* name */
	, "UnityEngine.SocialPlatforms.GameCenter"/* namespaze */
	, GcAchievementDescriptionData_t957_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &GcAchievementDescriptionData_t957_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GcAchievementDescriptionData_t957_0_0_0/* byval_arg */
	, &GcAchievementDescriptionData_t957_1_0_0/* this_arg */
	, &GcAchievementDescriptionData_t957_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GcAchievementDescriptionData_t957)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (GcAchievementDescriptionData_t957)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve_0.h"
// Metadata Definition UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
extern TypeInfo GcAchievementData_t958_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve_0MethodDeclarations.h"
extern const Il2CppType Achievement_t977_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.SocialPlatforms.Impl.Achievement UnityEngine.SocialPlatforms.GameCenter.GcAchievementData::ToAchievement()
extern const MethodInfo GcAchievementData_ToAchievement_m4887_MethodInfo = 
{
	"ToAchievement"/* name */
	, (methodPointerType)&GcAchievementData_ToAchievement_m4887/* method */
	, &GcAchievementData_t958_il2cpp_TypeInfo/* declaring_type */
	, &Achievement_t977_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2031/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* GcAchievementData_t958_MethodInfos[] =
{
	&GcAchievementData_ToAchievement_m4887_MethodInfo,
	NULL
};
static const Il2CppMethodReference GcAchievementData_t958_VTable[] =
{
	&ValueType_Equals_m1076_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&ValueType_GetHashCode_m1077_MethodInfo,
	&ValueType_ToString_m1078_MethodInfo,
};
static bool GcAchievementData_t958_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GcAchievementData_t958_0_0_0;
extern const Il2CppType GcAchievementData_t958_1_0_0;
const Il2CppTypeDefinitionMetadata GcAchievementData_t958_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t285_0_0_0/* parent */
	, GcAchievementData_t958_VTable/* vtableMethods */
	, GcAchievementData_t958_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1138/* fieldStart */

};
TypeInfo GcAchievementData_t958_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GcAchievementData"/* name */
	, "UnityEngine.SocialPlatforms.GameCenter"/* namespaze */
	, GcAchievementData_t958_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &GcAchievementData_t958_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GcAchievementData_t958_0_0_0/* byval_arg */
	, &GcAchievementData_t958_1_0_0/* this_arg */
	, &GcAchievementData_t958_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)GcAchievementData_t958_marshal/* marshal_to_native_func */
	, (methodPointerType)GcAchievementData_t958_marshal_back/* marshal_from_native_func */
	, (methodPointerType)GcAchievementData_t958_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (GcAchievementData_t958)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (GcAchievementData_t958)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(GcAchievementData_t958_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.GameCenter.GcScoreData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcScoreDa.h"
// Metadata Definition UnityEngine.SocialPlatforms.GameCenter.GcScoreData
extern TypeInfo GcScoreData_t959_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.GameCenter.GcScoreData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcScoreDaMethodDeclarations.h"
extern const Il2CppType Score_t979_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.SocialPlatforms.Impl.Score UnityEngine.SocialPlatforms.GameCenter.GcScoreData::ToScore()
extern const MethodInfo GcScoreData_ToScore_m4888_MethodInfo = 
{
	"ToScore"/* name */
	, (methodPointerType)&GcScoreData_ToScore_m4888/* method */
	, &GcScoreData_t959_il2cpp_TypeInfo/* declaring_type */
	, &Score_t979_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2032/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* GcScoreData_t959_MethodInfos[] =
{
	&GcScoreData_ToScore_m4888_MethodInfo,
	NULL
};
static const Il2CppMethodReference GcScoreData_t959_VTable[] =
{
	&ValueType_Equals_m1076_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&ValueType_GetHashCode_m1077_MethodInfo,
	&ValueType_ToString_m1078_MethodInfo,
};
static bool GcScoreData_t959_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GcScoreData_t959_0_0_0;
extern const Il2CppType GcScoreData_t959_1_0_0;
const Il2CppTypeDefinitionMetadata GcScoreData_t959_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t285_0_0_0/* parent */
	, GcScoreData_t959_VTable/* vtableMethods */
	, GcScoreData_t959_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1143/* fieldStart */

};
TypeInfo GcScoreData_t959_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GcScoreData"/* name */
	, "UnityEngine.SocialPlatforms.GameCenter"/* namespaze */
	, GcScoreData_t959_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &GcScoreData_t959_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GcScoreData_t959_0_0_0/* byval_arg */
	, &GcScoreData_t959_1_0_0/* this_arg */
	, &GcScoreData_t959_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)GcScoreData_t959_marshal/* marshal_to_native_func */
	, (methodPointerType)GcScoreData_t959_marshal_back/* marshal_from_native_func */
	, (methodPointerType)GcScoreData_t959_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (GcScoreData_t959)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (GcScoreData_t959)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(GcScoreData_t959_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Resolution
#include "UnityEngine_UnityEngine_Resolution.h"
// Metadata Definition UnityEngine.Resolution
extern TypeInfo Resolution_t960_il2cpp_TypeInfo;
// UnityEngine.Resolution
#include "UnityEngine_UnityEngine_ResolutionMethodDeclarations.h"
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.Resolution::get_width()
extern const MethodInfo Resolution_get_width_m4889_MethodInfo = 
{
	"get_width"/* name */
	, (methodPointerType)&Resolution_get_width_m4889/* method */
	, &Resolution_t960_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2033/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo Resolution_t960_Resolution_set_width_m4890_ParameterInfos[] = 
{
	{"value", 0, 134220111, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Resolution::set_width(System.Int32)
extern const MethodInfo Resolution_set_width_m4890_MethodInfo = 
{
	"set_width"/* name */
	, (methodPointerType)&Resolution_set_width_m4890/* method */
	, &Resolution_t960_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, Resolution_t960_Resolution_set_width_m4890_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2034/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.Resolution::get_height()
extern const MethodInfo Resolution_get_height_m4891_MethodInfo = 
{
	"get_height"/* name */
	, (methodPointerType)&Resolution_get_height_m4891/* method */
	, &Resolution_t960_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2035/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo Resolution_t960_Resolution_set_height_m4892_ParameterInfos[] = 
{
	{"value", 0, 134220112, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Resolution::set_height(System.Int32)
extern const MethodInfo Resolution_set_height_m4892_MethodInfo = 
{
	"set_height"/* name */
	, (methodPointerType)&Resolution_set_height_m4892/* method */
	, &Resolution_t960_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, Resolution_t960_Resolution_set_height_m4892_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2036/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.Resolution::get_refreshRate()
extern const MethodInfo Resolution_get_refreshRate_m4893_MethodInfo = 
{
	"get_refreshRate"/* name */
	, (methodPointerType)&Resolution_get_refreshRate_m4893/* method */
	, &Resolution_t960_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2037/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo Resolution_t960_Resolution_set_refreshRate_m4894_ParameterInfos[] = 
{
	{"value", 0, 134220113, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Resolution::set_refreshRate(System.Int32)
extern const MethodInfo Resolution_set_refreshRate_m4894_MethodInfo = 
{
	"set_refreshRate"/* name */
	, (methodPointerType)&Resolution_set_refreshRate_m4894/* method */
	, &Resolution_t960_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, Resolution_t960_Resolution_set_refreshRate_m4894_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2038/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Resolution::ToString()
extern const MethodInfo Resolution_ToString_m4895_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&Resolution_ToString_m4895/* method */
	, &Resolution_t960_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2039/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Resolution_t960_MethodInfos[] =
{
	&Resolution_get_width_m4889_MethodInfo,
	&Resolution_set_width_m4890_MethodInfo,
	&Resolution_get_height_m4891_MethodInfo,
	&Resolution_set_height_m4892_MethodInfo,
	&Resolution_get_refreshRate_m4893_MethodInfo,
	&Resolution_set_refreshRate_m4894_MethodInfo,
	&Resolution_ToString_m4895_MethodInfo,
	NULL
};
extern const MethodInfo Resolution_get_width_m4889_MethodInfo;
extern const MethodInfo Resolution_set_width_m4890_MethodInfo;
static const PropertyInfo Resolution_t960____width_PropertyInfo = 
{
	&Resolution_t960_il2cpp_TypeInfo/* parent */
	, "width"/* name */
	, &Resolution_get_width_m4889_MethodInfo/* get */
	, &Resolution_set_width_m4890_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Resolution_get_height_m4891_MethodInfo;
extern const MethodInfo Resolution_set_height_m4892_MethodInfo;
static const PropertyInfo Resolution_t960____height_PropertyInfo = 
{
	&Resolution_t960_il2cpp_TypeInfo/* parent */
	, "height"/* name */
	, &Resolution_get_height_m4891_MethodInfo/* get */
	, &Resolution_set_height_m4892_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Resolution_get_refreshRate_m4893_MethodInfo;
extern const MethodInfo Resolution_set_refreshRate_m4894_MethodInfo;
static const PropertyInfo Resolution_t960____refreshRate_PropertyInfo = 
{
	&Resolution_t960_il2cpp_TypeInfo/* parent */
	, "refreshRate"/* name */
	, &Resolution_get_refreshRate_m4893_MethodInfo/* get */
	, &Resolution_set_refreshRate_m4894_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Resolution_t960_PropertyInfos[] =
{
	&Resolution_t960____width_PropertyInfo,
	&Resolution_t960____height_PropertyInfo,
	&Resolution_t960____refreshRate_PropertyInfo,
	NULL
};
extern const MethodInfo Resolution_ToString_m4895_MethodInfo;
static const Il2CppMethodReference Resolution_t960_VTable[] =
{
	&ValueType_Equals_m1076_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&ValueType_GetHashCode_m1077_MethodInfo,
	&Resolution_ToString_m4895_MethodInfo,
};
static bool Resolution_t960_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Resolution_t960_0_0_0;
extern const Il2CppType Resolution_t960_1_0_0;
const Il2CppTypeDefinitionMetadata Resolution_t960_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t285_0_0_0/* parent */
	, Resolution_t960_VTable/* vtableMethods */
	, Resolution_t960_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1150/* fieldStart */

};
TypeInfo Resolution_t960_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Resolution"/* name */
	, "UnityEngine"/* namespaze */
	, Resolution_t960_MethodInfos/* methods */
	, Resolution_t960_PropertyInfos/* properties */
	, NULL/* events */
	, &Resolution_t960_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Resolution_t960_0_0_0/* byval_arg */
	, &Resolution_t960_1_0_0/* this_arg */
	, &Resolution_t960_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Resolution_t960)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Resolution_t960)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Resolution_t960 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 7/* method_count */
	, 3/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.RenderBuffer
#include "UnityEngine_UnityEngine_RenderBuffer.h"
// Metadata Definition UnityEngine.RenderBuffer
extern TypeInfo RenderBuffer_t961_il2cpp_TypeInfo;
// UnityEngine.RenderBuffer
#include "UnityEngine_UnityEngine_RenderBufferMethodDeclarations.h"
static const MethodInfo* RenderBuffer_t961_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference RenderBuffer_t961_VTable[] =
{
	&ValueType_Equals_m1076_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&ValueType_GetHashCode_m1077_MethodInfo,
	&ValueType_ToString_m1078_MethodInfo,
};
static bool RenderBuffer_t961_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType RenderBuffer_t961_0_0_0;
extern const Il2CppType RenderBuffer_t961_1_0_0;
const Il2CppTypeDefinitionMetadata RenderBuffer_t961_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t285_0_0_0/* parent */
	, RenderBuffer_t961_VTable/* vtableMethods */
	, RenderBuffer_t961_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1153/* fieldStart */

};
TypeInfo RenderBuffer_t961_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "RenderBuffer"/* name */
	, "UnityEngine"/* namespaze */
	, RenderBuffer_t961_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &RenderBuffer_t961_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RenderBuffer_t961_0_0_0/* byval_arg */
	, &RenderBuffer_t961_1_0_0/* this_arg */
	, &RenderBuffer_t961_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RenderBuffer_t961)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (RenderBuffer_t961)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(RenderBuffer_t961 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.FogMode
#include "UnityEngine_UnityEngine_FogMode.h"
// Metadata Definition UnityEngine.FogMode
extern TypeInfo FogMode_t962_il2cpp_TypeInfo;
// UnityEngine.FogMode
#include "UnityEngine_UnityEngine_FogModeMethodDeclarations.h"
static const MethodInfo* FogMode_t962_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference FogMode_t962_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool FogMode_t962_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair FogMode_t962_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType FogMode_t962_0_0_0;
extern const Il2CppType FogMode_t962_1_0_0;
const Il2CppTypeDefinitionMetadata FogMode_t962_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FogMode_t962_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, FogMode_t962_VTable/* vtableMethods */
	, FogMode_t962_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1155/* fieldStart */

};
TypeInfo FogMode_t962_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "FogMode"/* name */
	, "UnityEngine"/* namespaze */
	, FogMode_t962_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &FogMode_t962_0_0_0/* byval_arg */
	, &FogMode_t962_1_0_0/* this_arg */
	, &FogMode_t962_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FogMode_t962)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (FogMode_t962)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.CameraClearFlags
#include "UnityEngine_UnityEngine_CameraClearFlags.h"
// Metadata Definition UnityEngine.CameraClearFlags
extern TypeInfo CameraClearFlags_t963_il2cpp_TypeInfo;
// UnityEngine.CameraClearFlags
#include "UnityEngine_UnityEngine_CameraClearFlagsMethodDeclarations.h"
static const MethodInfo* CameraClearFlags_t963_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference CameraClearFlags_t963_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool CameraClearFlags_t963_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair CameraClearFlags_t963_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType CameraClearFlags_t963_0_0_0;
extern const Il2CppType CameraClearFlags_t963_1_0_0;
const Il2CppTypeDefinitionMetadata CameraClearFlags_t963_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CameraClearFlags_t963_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, CameraClearFlags_t963_VTable/* vtableMethods */
	, CameraClearFlags_t963_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1159/* fieldStart */

};
TypeInfo CameraClearFlags_t963_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraClearFlags"/* name */
	, "UnityEngine"/* namespaze */
	, CameraClearFlags_t963_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CameraClearFlags_t963_0_0_0/* byval_arg */
	, &CameraClearFlags_t963_1_0_0/* this_arg */
	, &CameraClearFlags_t963_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraClearFlags_t963)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (CameraClearFlags_t963)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.DepthTextureMode
#include "UnityEngine_UnityEngine_DepthTextureMode.h"
// Metadata Definition UnityEngine.DepthTextureMode
extern TypeInfo DepthTextureMode_t964_il2cpp_TypeInfo;
// UnityEngine.DepthTextureMode
#include "UnityEngine_UnityEngine_DepthTextureModeMethodDeclarations.h"
static const MethodInfo* DepthTextureMode_t964_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference DepthTextureMode_t964_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool DepthTextureMode_t964_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair DepthTextureMode_t964_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType DepthTextureMode_t964_0_0_0;
extern const Il2CppType DepthTextureMode_t964_1_0_0;
const Il2CppTypeDefinitionMetadata DepthTextureMode_t964_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DepthTextureMode_t964_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, DepthTextureMode_t964_VTable/* vtableMethods */
	, DepthTextureMode_t964_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1165/* fieldStart */

};
TypeInfo DepthTextureMode_t964_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "DepthTextureMode"/* name */
	, "UnityEngine"/* namespaze */
	, DepthTextureMode_t964_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1004/* custom_attributes_cache */
	, &DepthTextureMode_t964_0_0_0/* byval_arg */
	, &DepthTextureMode_t964_1_0_0/* this_arg */
	, &DepthTextureMode_t964_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DepthTextureMode_t964)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (DepthTextureMode_t964)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.MeshTopology
#include "UnityEngine_UnityEngine_MeshTopology.h"
// Metadata Definition UnityEngine.MeshTopology
extern TypeInfo MeshTopology_t965_il2cpp_TypeInfo;
// UnityEngine.MeshTopology
#include "UnityEngine_UnityEngine_MeshTopologyMethodDeclarations.h"
static const MethodInfo* MeshTopology_t965_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference MeshTopology_t965_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool MeshTopology_t965_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair MeshTopology_t965_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType MeshTopology_t965_0_0_0;
extern const Il2CppType MeshTopology_t965_1_0_0;
const Il2CppTypeDefinitionMetadata MeshTopology_t965_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MeshTopology_t965_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, MeshTopology_t965_VTable/* vtableMethods */
	, MeshTopology_t965_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1169/* fieldStart */

};
TypeInfo MeshTopology_t965_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "MeshTopology"/* name */
	, "UnityEngine"/* namespaze */
	, MeshTopology_t965_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MeshTopology_t965_0_0_0/* byval_arg */
	, &MeshTopology_t965_1_0_0/* this_arg */
	, &MeshTopology_t965_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MeshTopology_t965)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (MeshTopology_t965)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.ColorSpace
#include "UnityEngine_UnityEngine_ColorSpace.h"
// Metadata Definition UnityEngine.ColorSpace
extern TypeInfo ColorSpace_t966_il2cpp_TypeInfo;
// UnityEngine.ColorSpace
#include "UnityEngine_UnityEngine_ColorSpaceMethodDeclarations.h"
static const MethodInfo* ColorSpace_t966_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference ColorSpace_t966_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool ColorSpace_t966_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ColorSpace_t966_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ColorSpace_t966_0_0_0;
extern const Il2CppType ColorSpace_t966_1_0_0;
const Il2CppTypeDefinitionMetadata ColorSpace_t966_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ColorSpace_t966_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, ColorSpace_t966_VTable/* vtableMethods */
	, ColorSpace_t966_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1175/* fieldStart */

};
TypeInfo ColorSpace_t966_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ColorSpace"/* name */
	, "UnityEngine"/* namespaze */
	, ColorSpace_t966_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ColorSpace_t966_0_0_0/* byval_arg */
	, &ColorSpace_t966_1_0_0/* this_arg */
	, &ColorSpace_t966_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ColorSpace_t966)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ColorSpace_t966)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.FilterMode
#include "UnityEngine_UnityEngine_FilterMode.h"
// Metadata Definition UnityEngine.FilterMode
extern TypeInfo FilterMode_t967_il2cpp_TypeInfo;
// UnityEngine.FilterMode
#include "UnityEngine_UnityEngine_FilterModeMethodDeclarations.h"
static const MethodInfo* FilterMode_t967_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference FilterMode_t967_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool FilterMode_t967_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair FilterMode_t967_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType FilterMode_t967_0_0_0;
extern const Il2CppType FilterMode_t967_1_0_0;
const Il2CppTypeDefinitionMetadata FilterMode_t967_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FilterMode_t967_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, FilterMode_t967_VTable/* vtableMethods */
	, FilterMode_t967_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1179/* fieldStart */

};
TypeInfo FilterMode_t967_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "FilterMode"/* name */
	, "UnityEngine"/* namespaze */
	, FilterMode_t967_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &FilterMode_t967_0_0_0/* byval_arg */
	, &FilterMode_t967_1_0_0/* this_arg */
	, &FilterMode_t967_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FilterMode_t967)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (FilterMode_t967)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.TextureWrapMode
#include "UnityEngine_UnityEngine_TextureWrapMode.h"
// Metadata Definition UnityEngine.TextureWrapMode
extern TypeInfo TextureWrapMode_t968_il2cpp_TypeInfo;
// UnityEngine.TextureWrapMode
#include "UnityEngine_UnityEngine_TextureWrapModeMethodDeclarations.h"
static const MethodInfo* TextureWrapMode_t968_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference TextureWrapMode_t968_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool TextureWrapMode_t968_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TextureWrapMode_t968_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TextureWrapMode_t968_0_0_0;
extern const Il2CppType TextureWrapMode_t968_1_0_0;
const Il2CppTypeDefinitionMetadata TextureWrapMode_t968_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TextureWrapMode_t968_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, TextureWrapMode_t968_VTable/* vtableMethods */
	, TextureWrapMode_t968_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1183/* fieldStart */

};
TypeInfo TextureWrapMode_t968_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TextureWrapMode"/* name */
	, "UnityEngine"/* namespaze */
	, TextureWrapMode_t968_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TextureWrapMode_t968_0_0_0/* byval_arg */
	, &TextureWrapMode_t968_1_0_0/* this_arg */
	, &TextureWrapMode_t968_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TextureWrapMode_t968)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TextureWrapMode_t968)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.TextureFormat
#include "UnityEngine_UnityEngine_TextureFormat.h"
// Metadata Definition UnityEngine.TextureFormat
extern TypeInfo TextureFormat_t969_il2cpp_TypeInfo;
// UnityEngine.TextureFormat
#include "UnityEngine_UnityEngine_TextureFormatMethodDeclarations.h"
static const MethodInfo* TextureFormat_t969_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference TextureFormat_t969_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool TextureFormat_t969_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TextureFormat_t969_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TextureFormat_t969_0_0_0;
extern const Il2CppType TextureFormat_t969_1_0_0;
const Il2CppTypeDefinitionMetadata TextureFormat_t969_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TextureFormat_t969_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, TextureFormat_t969_VTable/* vtableMethods */
	, TextureFormat_t969_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1186/* fieldStart */

};
TypeInfo TextureFormat_t969_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TextureFormat"/* name */
	, "UnityEngine"/* namespaze */
	, TextureFormat_t969_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TextureFormat_t969_0_0_0/* byval_arg */
	, &TextureFormat_t969_1_0_0/* this_arg */
	, &TextureFormat_t969_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TextureFormat_t969)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TextureFormat_t969)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 45/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.RenderTextureFormat
#include "UnityEngine_UnityEngine_RenderTextureFormat.h"
// Metadata Definition UnityEngine.RenderTextureFormat
extern TypeInfo RenderTextureFormat_t970_il2cpp_TypeInfo;
// UnityEngine.RenderTextureFormat
#include "UnityEngine_UnityEngine_RenderTextureFormatMethodDeclarations.h"
static const MethodInfo* RenderTextureFormat_t970_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference RenderTextureFormat_t970_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool RenderTextureFormat_t970_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair RenderTextureFormat_t970_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType RenderTextureFormat_t970_0_0_0;
extern const Il2CppType RenderTextureFormat_t970_1_0_0;
const Il2CppTypeDefinitionMetadata RenderTextureFormat_t970_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RenderTextureFormat_t970_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, RenderTextureFormat_t970_VTable/* vtableMethods */
	, RenderTextureFormat_t970_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1231/* fieldStart */

};
TypeInfo RenderTextureFormat_t970_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "RenderTextureFormat"/* name */
	, "UnityEngine"/* namespaze */
	, RenderTextureFormat_t970_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RenderTextureFormat_t970_0_0_0/* byval_arg */
	, &RenderTextureFormat_t970_1_0_0/* this_arg */
	, &RenderTextureFormat_t970_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RenderTextureFormat_t970)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (RenderTextureFormat_t970)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 20/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.RenderTextureReadWrite
#include "UnityEngine_UnityEngine_RenderTextureReadWrite.h"
// Metadata Definition UnityEngine.RenderTextureReadWrite
extern TypeInfo RenderTextureReadWrite_t971_il2cpp_TypeInfo;
// UnityEngine.RenderTextureReadWrite
#include "UnityEngine_UnityEngine_RenderTextureReadWriteMethodDeclarations.h"
static const MethodInfo* RenderTextureReadWrite_t971_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference RenderTextureReadWrite_t971_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool RenderTextureReadWrite_t971_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair RenderTextureReadWrite_t971_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType RenderTextureReadWrite_t971_0_0_0;
extern const Il2CppType RenderTextureReadWrite_t971_1_0_0;
const Il2CppTypeDefinitionMetadata RenderTextureReadWrite_t971_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RenderTextureReadWrite_t971_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, RenderTextureReadWrite_t971_VTable/* vtableMethods */
	, RenderTextureReadWrite_t971_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1251/* fieldStart */

};
TypeInfo RenderTextureReadWrite_t971_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "RenderTextureReadWrite"/* name */
	, "UnityEngine"/* namespaze */
	, RenderTextureReadWrite_t971_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RenderTextureReadWrite_t971_0_0_0/* byval_arg */
	, &RenderTextureReadWrite_t971_1_0_0/* this_arg */
	, &RenderTextureReadWrite_t971_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RenderTextureReadWrite_t971)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (RenderTextureReadWrite_t971)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.Rendering.ReflectionProbeBlendInfo
#include "UnityEngine_UnityEngine_Rendering_ReflectionProbeBlendInfo.h"
// Metadata Definition UnityEngine.Rendering.ReflectionProbeBlendInfo
extern TypeInfo ReflectionProbeBlendInfo_t972_il2cpp_TypeInfo;
// UnityEngine.Rendering.ReflectionProbeBlendInfo
#include "UnityEngine_UnityEngine_Rendering_ReflectionProbeBlendInfoMethodDeclarations.h"
static const MethodInfo* ReflectionProbeBlendInfo_t972_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference ReflectionProbeBlendInfo_t972_VTable[] =
{
	&ValueType_Equals_m1076_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&ValueType_GetHashCode_m1077_MethodInfo,
	&ValueType_ToString_m1078_MethodInfo,
};
static bool ReflectionProbeBlendInfo_t972_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ReflectionProbeBlendInfo_t972_0_0_0;
extern const Il2CppType ReflectionProbeBlendInfo_t972_1_0_0;
const Il2CppTypeDefinitionMetadata ReflectionProbeBlendInfo_t972_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t285_0_0_0/* parent */
	, ReflectionProbeBlendInfo_t972_VTable/* vtableMethods */
	, ReflectionProbeBlendInfo_t972_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1255/* fieldStart */

};
TypeInfo ReflectionProbeBlendInfo_t972_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ReflectionProbeBlendInfo"/* name */
	, "UnityEngine.Rendering"/* namespaze */
	, ReflectionProbeBlendInfo_t972_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ReflectionProbeBlendInfo_t972_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ReflectionProbeBlendInfo_t972_0_0_0/* byval_arg */
	, &ReflectionProbeBlendInfo_t972_1_0_0/* this_arg */
	, &ReflectionProbeBlendInfo_t972_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ReflectionProbeBlendInfo_t972)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ReflectionProbeBlendInfo_t972)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.GUIStateObjects
#include "UnityEngine_UnityEngine_GUIStateObjects.h"
// Metadata Definition UnityEngine.GUIStateObjects
extern TypeInfo GUIStateObjects_t974_il2cpp_TypeInfo;
// UnityEngine.GUIStateObjects
#include "UnityEngine_UnityEngine_GUIStateObjectsMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.GUIStateObjects::.cctor()
extern const MethodInfo GUIStateObjects__cctor_m4896_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&GUIStateObjects__cctor_m4896/* method */
	, &GUIStateObjects_t974_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2040/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo GUIStateObjects_t974_GUIStateObjects_GetStateObject_m4897_ParameterInfos[] = 
{
	{"t", 0, 134220114, 0, &Type_t_0_0_0},
	{"controlID", 1, 134220115, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Object UnityEngine.GUIStateObjects::GetStateObject(System.Type,System.Int32)
extern const MethodInfo GUIStateObjects_GetStateObject_m4897_MethodInfo = 
{
	"GetStateObject"/* name */
	, (methodPointerType)&GUIStateObjects_GetStateObject_m4897/* method */
	, &GUIStateObjects_t974_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t253/* invoker_method */
	, GUIStateObjects_t974_GUIStateObjects_GetStateObject_m4897_ParameterInfos/* parameters */
	, 1005/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2041/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* GUIStateObjects_t974_MethodInfos[] =
{
	&GUIStateObjects__cctor_m4896_MethodInfo,
	&GUIStateObjects_GetStateObject_m4897_MethodInfo,
	NULL
};
static const Il2CppMethodReference GUIStateObjects_t974_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool GUIStateObjects_t974_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GUIStateObjects_t974_0_0_0;
extern const Il2CppType GUIStateObjects_t974_1_0_0;
struct GUIStateObjects_t974;
const Il2CppTypeDefinitionMetadata GUIStateObjects_t974_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, GUIStateObjects_t974_VTable/* vtableMethods */
	, GUIStateObjects_t974_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1257/* fieldStart */

};
TypeInfo GUIStateObjects_t974_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GUIStateObjects"/* name */
	, "UnityEngine"/* namespaze */
	, GUIStateObjects_t974_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &GUIStateObjects_t974_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GUIStateObjects_t974_0_0_0/* byval_arg */
	, &GUIStateObjects_t974_1_0_0/* this_arg */
	, &GUIStateObjects_t974_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GUIStateObjects_t974)/* instance_size */
	, sizeof (GUIStateObjects_t974)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(GUIStateObjects_t974_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.Impl.LocalUser
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_LocalUser.h"
// Metadata Definition UnityEngine.SocialPlatforms.Impl.LocalUser
extern TypeInfo LocalUser_t794_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.Impl.LocalUser
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_LocalUserMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.LocalUser::.ctor()
extern const MethodInfo LocalUser__ctor_m4898_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&LocalUser__ctor_m4898/* method */
	, &LocalUser_t794_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2042/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IUserProfileU5BU5D_t975_0_0_0;
extern const Il2CppType IUserProfileU5BU5D_t975_0_0_0;
static const ParameterInfo LocalUser_t794_LocalUser_SetFriends_m4899_ParameterInfos[] = 
{
	{"friends", 0, 134220116, 0, &IUserProfileU5BU5D_t975_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.LocalUser::SetFriends(UnityEngine.SocialPlatforms.IUserProfile[])
extern const MethodInfo LocalUser_SetFriends_m4899_MethodInfo = 
{
	"SetFriends"/* name */
	, (methodPointerType)&LocalUser_SetFriends_m4899/* method */
	, &LocalUser_t794_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, LocalUser_t794_LocalUser_SetFriends_m4899_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2043/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo LocalUser_t794_LocalUser_SetAuthenticated_m4900_ParameterInfos[] = 
{
	{"value", 0, 134220117, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.LocalUser::SetAuthenticated(System.Boolean)
extern const MethodInfo LocalUser_SetAuthenticated_m4900_MethodInfo = 
{
	"SetAuthenticated"/* name */
	, (methodPointerType)&LocalUser_SetAuthenticated_m4900/* method */
	, &LocalUser_t794_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274/* invoker_method */
	, LocalUser_t794_LocalUser_SetAuthenticated_m4900_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2044/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo LocalUser_t794_LocalUser_SetUnderage_m4901_ParameterInfos[] = 
{
	{"value", 0, 134220118, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.LocalUser::SetUnderage(System.Boolean)
extern const MethodInfo LocalUser_SetUnderage_m4901_MethodInfo = 
{
	"SetUnderage"/* name */
	, (methodPointerType)&LocalUser_SetUnderage_m4901/* method */
	, &LocalUser_t794_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274/* invoker_method */
	, LocalUser_t794_LocalUser_SetUnderage_m4901_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2045/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.SocialPlatforms.Impl.LocalUser::get_authenticated()
extern const MethodInfo LocalUser_get_authenticated_m4902_MethodInfo = 
{
	"get_authenticated"/* name */
	, (methodPointerType)&LocalUser_get_authenticated_m4902/* method */
	, &LocalUser_t794_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2046/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* LocalUser_t794_MethodInfos[] =
{
	&LocalUser__ctor_m4898_MethodInfo,
	&LocalUser_SetFriends_m4899_MethodInfo,
	&LocalUser_SetAuthenticated_m4900_MethodInfo,
	&LocalUser_SetUnderage_m4901_MethodInfo,
	&LocalUser_get_authenticated_m4902_MethodInfo,
	NULL
};
extern const MethodInfo LocalUser_get_authenticated_m4902_MethodInfo;
static const PropertyInfo LocalUser_t794____authenticated_PropertyInfo = 
{
	&LocalUser_t794_il2cpp_TypeInfo/* parent */
	, "authenticated"/* name */
	, &LocalUser_get_authenticated_m4902_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* LocalUser_t794_PropertyInfos[] =
{
	&LocalUser_t794____authenticated_PropertyInfo,
	NULL
};
extern const MethodInfo UserProfile_ToString_m4905_MethodInfo;
extern const MethodInfo UserProfile_get_userName_m4909_MethodInfo;
extern const MethodInfo UserProfile_get_id_m4910_MethodInfo;
extern const MethodInfo UserProfile_get_isFriend_m4911_MethodInfo;
extern const MethodInfo UserProfile_get_state_m4912_MethodInfo;
static const Il2CppMethodReference LocalUser_t794_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&UserProfile_ToString_m4905_MethodInfo,
	&UserProfile_get_userName_m4909_MethodInfo,
	&UserProfile_get_id_m4910_MethodInfo,
	&UserProfile_get_isFriend_m4911_MethodInfo,
	&UserProfile_get_state_m4912_MethodInfo,
	&LocalUser_get_authenticated_m4902_MethodInfo,
};
static bool LocalUser_t794_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ILocalUser_t1017_0_0_0;
extern const Il2CppType IUserProfile_t1126_0_0_0;
static const Il2CppType* LocalUser_t794_InterfacesTypeInfos[] = 
{
	&ILocalUser_t1017_0_0_0,
	&IUserProfile_t1126_0_0_0,
};
static Il2CppInterfaceOffsetPair LocalUser_t794_InterfacesOffsets[] = 
{
	{ &IUserProfile_t1126_0_0_0, 4},
	{ &ILocalUser_t1017_0_0_0, 8},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType LocalUser_t794_0_0_0;
extern const Il2CppType LocalUser_t794_1_0_0;
struct LocalUser_t794;
const Il2CppTypeDefinitionMetadata LocalUser_t794_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, LocalUser_t794_InterfacesTypeInfos/* implementedInterfaces */
	, LocalUser_t794_InterfacesOffsets/* interfaceOffsets */
	, &UserProfile_t976_0_0_0/* parent */
	, LocalUser_t794_VTable/* vtableMethods */
	, LocalUser_t794_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1258/* fieldStart */

};
TypeInfo LocalUser_t794_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "LocalUser"/* name */
	, "UnityEngine.SocialPlatforms.Impl"/* namespaze */
	, LocalUser_t794_MethodInfos/* methods */
	, LocalUser_t794_PropertyInfos/* properties */
	, NULL/* events */
	, &LocalUser_t794_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &LocalUser_t794_0_0_0/* byval_arg */
	, &LocalUser_t794_1_0_0/* this_arg */
	, &LocalUser_t794_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LocalUser_t794)/* instance_size */
	, sizeof (LocalUser_t794)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.Impl.UserProfile
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_UserProfile.h"
// Metadata Definition UnityEngine.SocialPlatforms.Impl.UserProfile
extern TypeInfo UserProfile_t976_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.Impl.UserProfile
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_UserProfileMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::.ctor()
extern const MethodInfo UserProfile__ctor_m4903_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UserProfile__ctor_m4903/* method */
	, &UserProfile_t976_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2047/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
extern const Il2CppType UserState_t987_0_0_0;
extern const Il2CppType UserState_t987_0_0_0;
extern const Il2CppType Texture2D_t63_0_0_0;
extern const Il2CppType Texture2D_t63_0_0_0;
static const ParameterInfo UserProfile_t976_UserProfile__ctor_m4904_ParameterInfos[] = 
{
	{"name", 0, 134220119, 0, &String_t_0_0_0},
	{"id", 1, 134220120, 0, &String_t_0_0_0},
	{"friend", 2, 134220121, 0, &Boolean_t273_0_0_0},
	{"state", 3, 134220122, 0, &UserState_t987_0_0_0},
	{"image", 4, 134220123, 0, &Texture2D_t63_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t_SByte_t274_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::.ctor(System.String,System.String,System.Boolean,UnityEngine.SocialPlatforms.UserState,UnityEngine.Texture2D)
extern const MethodInfo UserProfile__ctor_m4904_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UserProfile__ctor_m4904/* method */
	, &UserProfile_t976_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t_SByte_t274_Int32_t253_Object_t/* invoker_method */
	, UserProfile_t976_UserProfile__ctor_m4904_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2048/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.UserProfile::ToString()
extern const MethodInfo UserProfile_ToString_m4905_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&UserProfile_ToString_m4905/* method */
	, &UserProfile_t976_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2049/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo UserProfile_t976_UserProfile_SetUserName_m4906_ParameterInfos[] = 
{
	{"name", 0, 134220124, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::SetUserName(System.String)
extern const MethodInfo UserProfile_SetUserName_m4906_MethodInfo = 
{
	"SetUserName"/* name */
	, (methodPointerType)&UserProfile_SetUserName_m4906/* method */
	, &UserProfile_t976_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, UserProfile_t976_UserProfile_SetUserName_m4906_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2050/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo UserProfile_t976_UserProfile_SetUserID_m4907_ParameterInfos[] = 
{
	{"id", 0, 134220125, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::SetUserID(System.String)
extern const MethodInfo UserProfile_SetUserID_m4907_MethodInfo = 
{
	"SetUserID"/* name */
	, (methodPointerType)&UserProfile_SetUserID_m4907/* method */
	, &UserProfile_t976_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, UserProfile_t976_UserProfile_SetUserID_m4907_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2051/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Texture2D_t63_0_0_0;
static const ParameterInfo UserProfile_t976_UserProfile_SetImage_m4908_ParameterInfos[] = 
{
	{"image", 0, 134220126, 0, &Texture2D_t63_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::SetImage(UnityEngine.Texture2D)
extern const MethodInfo UserProfile_SetImage_m4908_MethodInfo = 
{
	"SetImage"/* name */
	, (methodPointerType)&UserProfile_SetImage_m4908/* method */
	, &UserProfile_t976_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, UserProfile_t976_UserProfile_SetImage_m4908_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2052/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.UserProfile::get_userName()
extern const MethodInfo UserProfile_get_userName_m4909_MethodInfo = 
{
	"get_userName"/* name */
	, (methodPointerType)&UserProfile_get_userName_m4909/* method */
	, &UserProfile_t976_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2053/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.UserProfile::get_id()
extern const MethodInfo UserProfile_get_id_m4910_MethodInfo = 
{
	"get_id"/* name */
	, (methodPointerType)&UserProfile_get_id_m4910/* method */
	, &UserProfile_t976_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2054/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.SocialPlatforms.Impl.UserProfile::get_isFriend()
extern const MethodInfo UserProfile_get_isFriend_m4911_MethodInfo = 
{
	"get_isFriend"/* name */
	, (methodPointerType)&UserProfile_get_isFriend_m4911/* method */
	, &UserProfile_t976_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2055/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_UserState_t987 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.SocialPlatforms.UserState UnityEngine.SocialPlatforms.Impl.UserProfile::get_state()
extern const MethodInfo UserProfile_get_state_m4912_MethodInfo = 
{
	"get_state"/* name */
	, (methodPointerType)&UserProfile_get_state_m4912/* method */
	, &UserProfile_t976_il2cpp_TypeInfo/* declaring_type */
	, &UserState_t987_0_0_0/* return_type */
	, RuntimeInvoker_UserState_t987/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2056/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UserProfile_t976_MethodInfos[] =
{
	&UserProfile__ctor_m4903_MethodInfo,
	&UserProfile__ctor_m4904_MethodInfo,
	&UserProfile_ToString_m4905_MethodInfo,
	&UserProfile_SetUserName_m4906_MethodInfo,
	&UserProfile_SetUserID_m4907_MethodInfo,
	&UserProfile_SetImage_m4908_MethodInfo,
	&UserProfile_get_userName_m4909_MethodInfo,
	&UserProfile_get_id_m4910_MethodInfo,
	&UserProfile_get_isFriend_m4911_MethodInfo,
	&UserProfile_get_state_m4912_MethodInfo,
	NULL
};
static const PropertyInfo UserProfile_t976____userName_PropertyInfo = 
{
	&UserProfile_t976_il2cpp_TypeInfo/* parent */
	, "userName"/* name */
	, &UserProfile_get_userName_m4909_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo UserProfile_t976____id_PropertyInfo = 
{
	&UserProfile_t976_il2cpp_TypeInfo/* parent */
	, "id"/* name */
	, &UserProfile_get_id_m4910_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo UserProfile_t976____isFriend_PropertyInfo = 
{
	&UserProfile_t976_il2cpp_TypeInfo/* parent */
	, "isFriend"/* name */
	, &UserProfile_get_isFriend_m4911_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo UserProfile_t976____state_PropertyInfo = 
{
	&UserProfile_t976_il2cpp_TypeInfo/* parent */
	, "state"/* name */
	, &UserProfile_get_state_m4912_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* UserProfile_t976_PropertyInfos[] =
{
	&UserProfile_t976____userName_PropertyInfo,
	&UserProfile_t976____id_PropertyInfo,
	&UserProfile_t976____isFriend_PropertyInfo,
	&UserProfile_t976____state_PropertyInfo,
	NULL
};
static const Il2CppMethodReference UserProfile_t976_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&UserProfile_ToString_m4905_MethodInfo,
	&UserProfile_get_userName_m4909_MethodInfo,
	&UserProfile_get_id_m4910_MethodInfo,
	&UserProfile_get_isFriend_m4911_MethodInfo,
	&UserProfile_get_state_m4912_MethodInfo,
};
static bool UserProfile_t976_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* UserProfile_t976_InterfacesTypeInfos[] = 
{
	&IUserProfile_t1126_0_0_0,
};
static Il2CppInterfaceOffsetPair UserProfile_t976_InterfacesOffsets[] = 
{
	{ &IUserProfile_t1126_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UserProfile_t976_1_0_0;
struct UserProfile_t976;
const Il2CppTypeDefinitionMetadata UserProfile_t976_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, UserProfile_t976_InterfacesTypeInfos/* implementedInterfaces */
	, UserProfile_t976_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, UserProfile_t976_VTable/* vtableMethods */
	, UserProfile_t976_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1261/* fieldStart */

};
TypeInfo UserProfile_t976_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UserProfile"/* name */
	, "UnityEngine.SocialPlatforms.Impl"/* namespaze */
	, UserProfile_t976_MethodInfos/* methods */
	, UserProfile_t976_PropertyInfos/* properties */
	, NULL/* events */
	, &UserProfile_t976_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UserProfile_t976_0_0_0/* byval_arg */
	, &UserProfile_t976_1_0_0/* this_arg */
	, &UserProfile_t976_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UserProfile_t976)/* instance_size */
	, sizeof (UserProfile_t976)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 4/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.Impl.Achievement
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Achievement.h"
// Metadata Definition UnityEngine.SocialPlatforms.Impl.Achievement
extern TypeInfo Achievement_t977_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.Impl.Achievement
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_AchievementMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Double_t1070_0_0_0;
extern const Il2CppType Double_t1070_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
extern const Il2CppType DateTime_t406_0_0_0;
extern const Il2CppType DateTime_t406_0_0_0;
static const ParameterInfo Achievement_t977_Achievement__ctor_m4913_ParameterInfos[] = 
{
	{"id", 0, 134220127, 0, &String_t_0_0_0},
	{"percentCompleted", 1, 134220128, 0, &Double_t1070_0_0_0},
	{"completed", 2, 134220129, 0, &Boolean_t273_0_0_0},
	{"hidden", 3, 134220130, 0, &Boolean_t273_0_0_0},
	{"lastReportedDate", 4, 134220131, 0, &DateTime_t406_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Double_t1070_SByte_t274_SByte_t274_DateTime_t406 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::.ctor(System.String,System.Double,System.Boolean,System.Boolean,System.DateTime)
extern const MethodInfo Achievement__ctor_m4913_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Achievement__ctor_m4913/* method */
	, &Achievement_t977_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Double_t1070_SByte_t274_SByte_t274_DateTime_t406/* invoker_method */
	, Achievement_t977_Achievement__ctor_m4913_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2057/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Double_t1070_0_0_0;
static const ParameterInfo Achievement_t977_Achievement__ctor_m4914_ParameterInfos[] = 
{
	{"id", 0, 134220132, 0, &String_t_0_0_0},
	{"percent", 1, 134220133, 0, &Double_t1070_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Double_t1070 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::.ctor(System.String,System.Double)
extern const MethodInfo Achievement__ctor_m4914_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Achievement__ctor_m4914/* method */
	, &Achievement_t977_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Double_t1070/* invoker_method */
	, Achievement_t977_Achievement__ctor_m4914_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2058/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::.ctor()
extern const MethodInfo Achievement__ctor_m4915_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Achievement__ctor_m4915/* method */
	, &Achievement_t977_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2059/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.Achievement::ToString()
extern const MethodInfo Achievement_ToString_m4916_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&Achievement_ToString_m4916/* method */
	, &Achievement_t977_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2060/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.Achievement::get_id()
extern const MethodInfo Achievement_get_id_m4917_MethodInfo = 
{
	"get_id"/* name */
	, (methodPointerType)&Achievement_get_id_m4917/* method */
	, &Achievement_t977_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 1008/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2061/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Achievement_t977_Achievement_set_id_m4918_ParameterInfos[] = 
{
	{"value", 0, 134220134, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::set_id(System.String)
extern const MethodInfo Achievement_set_id_m4918_MethodInfo = 
{
	"set_id"/* name */
	, (methodPointerType)&Achievement_set_id_m4918/* method */
	, &Achievement_t977_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, Achievement_t977_Achievement_set_id_m4918_ParameterInfos/* parameters */
	, 1009/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2062/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Double_t1070 (const MethodInfo* method, void* obj, void** args);
// System.Double UnityEngine.SocialPlatforms.Impl.Achievement::get_percentCompleted()
extern const MethodInfo Achievement_get_percentCompleted_m4919_MethodInfo = 
{
	"get_percentCompleted"/* name */
	, (methodPointerType)&Achievement_get_percentCompleted_m4919/* method */
	, &Achievement_t977_il2cpp_TypeInfo/* declaring_type */
	, &Double_t1070_0_0_0/* return_type */
	, RuntimeInvoker_Double_t1070/* invoker_method */
	, NULL/* parameters */
	, 1010/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2063/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Double_t1070_0_0_0;
static const ParameterInfo Achievement_t977_Achievement_set_percentCompleted_m4920_ParameterInfos[] = 
{
	{"value", 0, 134220135, 0, &Double_t1070_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Double_t1070 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::set_percentCompleted(System.Double)
extern const MethodInfo Achievement_set_percentCompleted_m4920_MethodInfo = 
{
	"set_percentCompleted"/* name */
	, (methodPointerType)&Achievement_set_percentCompleted_m4920/* method */
	, &Achievement_t977_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Double_t1070/* invoker_method */
	, Achievement_t977_Achievement_set_percentCompleted_m4920_ParameterInfos/* parameters */
	, 1011/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2064/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.SocialPlatforms.Impl.Achievement::get_completed()
extern const MethodInfo Achievement_get_completed_m4921_MethodInfo = 
{
	"get_completed"/* name */
	, (methodPointerType)&Achievement_get_completed_m4921/* method */
	, &Achievement_t977_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2065/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.SocialPlatforms.Impl.Achievement::get_hidden()
extern const MethodInfo Achievement_get_hidden_m4922_MethodInfo = 
{
	"get_hidden"/* name */
	, (methodPointerType)&Achievement_get_hidden_m4922/* method */
	, &Achievement_t977_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2066/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_DateTime_t406 (const MethodInfo* method, void* obj, void** args);
// System.DateTime UnityEngine.SocialPlatforms.Impl.Achievement::get_lastReportedDate()
extern const MethodInfo Achievement_get_lastReportedDate_m4923_MethodInfo = 
{
	"get_lastReportedDate"/* name */
	, (methodPointerType)&Achievement_get_lastReportedDate_m4923/* method */
	, &Achievement_t977_il2cpp_TypeInfo/* declaring_type */
	, &DateTime_t406_0_0_0/* return_type */
	, RuntimeInvoker_DateTime_t406/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2067/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Achievement_t977_MethodInfos[] =
{
	&Achievement__ctor_m4913_MethodInfo,
	&Achievement__ctor_m4914_MethodInfo,
	&Achievement__ctor_m4915_MethodInfo,
	&Achievement_ToString_m4916_MethodInfo,
	&Achievement_get_id_m4917_MethodInfo,
	&Achievement_set_id_m4918_MethodInfo,
	&Achievement_get_percentCompleted_m4919_MethodInfo,
	&Achievement_set_percentCompleted_m4920_MethodInfo,
	&Achievement_get_completed_m4921_MethodInfo,
	&Achievement_get_hidden_m4922_MethodInfo,
	&Achievement_get_lastReportedDate_m4923_MethodInfo,
	NULL
};
extern const MethodInfo Achievement_get_id_m4917_MethodInfo;
extern const MethodInfo Achievement_set_id_m4918_MethodInfo;
static const PropertyInfo Achievement_t977____id_PropertyInfo = 
{
	&Achievement_t977_il2cpp_TypeInfo/* parent */
	, "id"/* name */
	, &Achievement_get_id_m4917_MethodInfo/* get */
	, &Achievement_set_id_m4918_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Achievement_get_percentCompleted_m4919_MethodInfo;
extern const MethodInfo Achievement_set_percentCompleted_m4920_MethodInfo;
static const PropertyInfo Achievement_t977____percentCompleted_PropertyInfo = 
{
	&Achievement_t977_il2cpp_TypeInfo/* parent */
	, "percentCompleted"/* name */
	, &Achievement_get_percentCompleted_m4919_MethodInfo/* get */
	, &Achievement_set_percentCompleted_m4920_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Achievement_get_completed_m4921_MethodInfo;
static const PropertyInfo Achievement_t977____completed_PropertyInfo = 
{
	&Achievement_t977_il2cpp_TypeInfo/* parent */
	, "completed"/* name */
	, &Achievement_get_completed_m4921_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Achievement_get_hidden_m4922_MethodInfo;
static const PropertyInfo Achievement_t977____hidden_PropertyInfo = 
{
	&Achievement_t977_il2cpp_TypeInfo/* parent */
	, "hidden"/* name */
	, &Achievement_get_hidden_m4922_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Achievement_get_lastReportedDate_m4923_MethodInfo;
static const PropertyInfo Achievement_t977____lastReportedDate_PropertyInfo = 
{
	&Achievement_t977_il2cpp_TypeInfo/* parent */
	, "lastReportedDate"/* name */
	, &Achievement_get_lastReportedDate_m4923_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Achievement_t977_PropertyInfos[] =
{
	&Achievement_t977____id_PropertyInfo,
	&Achievement_t977____percentCompleted_PropertyInfo,
	&Achievement_t977____completed_PropertyInfo,
	&Achievement_t977____hidden_PropertyInfo,
	&Achievement_t977____lastReportedDate_PropertyInfo,
	NULL
};
extern const MethodInfo Achievement_ToString_m4916_MethodInfo;
static const Il2CppMethodReference Achievement_t977_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Achievement_ToString_m4916_MethodInfo,
	&Achievement_get_id_m4917_MethodInfo,
	&Achievement_set_id_m4918_MethodInfo,
	&Achievement_get_percentCompleted_m4919_MethodInfo,
	&Achievement_set_percentCompleted_m4920_MethodInfo,
	&Achievement_get_completed_m4921_MethodInfo,
	&Achievement_get_hidden_m4922_MethodInfo,
	&Achievement_get_lastReportedDate_m4923_MethodInfo,
};
static bool Achievement_t977_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IAchievement_t1021_0_0_0;
static const Il2CppType* Achievement_t977_InterfacesTypeInfos[] = 
{
	&IAchievement_t1021_0_0_0,
};
static Il2CppInterfaceOffsetPair Achievement_t977_InterfacesOffsets[] = 
{
	{ &IAchievement_t1021_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Achievement_t977_1_0_0;
struct Achievement_t977;
const Il2CppTypeDefinitionMetadata Achievement_t977_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Achievement_t977_InterfacesTypeInfos/* implementedInterfaces */
	, Achievement_t977_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Achievement_t977_VTable/* vtableMethods */
	, Achievement_t977_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1266/* fieldStart */

};
TypeInfo Achievement_t977_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Achievement"/* name */
	, "UnityEngine.SocialPlatforms.Impl"/* namespaze */
	, Achievement_t977_MethodInfos/* methods */
	, Achievement_t977_PropertyInfos/* properties */
	, NULL/* events */
	, &Achievement_t977_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Achievement_t977_0_0_0/* byval_arg */
	, &Achievement_t977_1_0_0/* this_arg */
	, &Achievement_t977_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Achievement_t977)/* instance_size */
	, sizeof (Achievement_t977)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 5/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.Impl.AchievementDescription
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_AchievementDesc.h"
// Metadata Definition UnityEngine.SocialPlatforms.Impl.AchievementDescription
extern TypeInfo AchievementDescription_t978_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.Impl.AchievementDescription
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_AchievementDescMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Texture2D_t63_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo AchievementDescription_t978_AchievementDescription__ctor_m4924_ParameterInfos[] = 
{
	{"id", 0, 134220136, 0, &String_t_0_0_0},
	{"title", 1, 134220137, 0, &String_t_0_0_0},
	{"image", 2, 134220138, 0, &Texture2D_t63_0_0_0},
	{"achievedDescription", 3, 134220139, 0, &String_t_0_0_0},
	{"unachievedDescription", 4, 134220140, 0, &String_t_0_0_0},
	{"hidden", 5, 134220141, 0, &Boolean_t273_0_0_0},
	{"points", 6, 134220142, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t_Object_t_Object_t_SByte_t274_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.AchievementDescription::.ctor(System.String,System.String,UnityEngine.Texture2D,System.String,System.String,System.Boolean,System.Int32)
extern const MethodInfo AchievementDescription__ctor_m4924_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AchievementDescription__ctor_m4924/* method */
	, &AchievementDescription_t978_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t_Object_t_Object_t_SByte_t274_Int32_t253/* invoker_method */
	, AchievementDescription_t978_AchievementDescription__ctor_m4924_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 7/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2068/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::ToString()
extern const MethodInfo AchievementDescription_ToString_m4925_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&AchievementDescription_ToString_m4925/* method */
	, &AchievementDescription_t978_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2069/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Texture2D_t63_0_0_0;
static const ParameterInfo AchievementDescription_t978_AchievementDescription_SetImage_m4926_ParameterInfos[] = 
{
	{"image", 0, 134220143, 0, &Texture2D_t63_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.AchievementDescription::SetImage(UnityEngine.Texture2D)
extern const MethodInfo AchievementDescription_SetImage_m4926_MethodInfo = 
{
	"SetImage"/* name */
	, (methodPointerType)&AchievementDescription_SetImage_m4926/* method */
	, &AchievementDescription_t978_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, AchievementDescription_t978_AchievementDescription_SetImage_m4926_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2070/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_id()
extern const MethodInfo AchievementDescription_get_id_m4927_MethodInfo = 
{
	"get_id"/* name */
	, (methodPointerType)&AchievementDescription_get_id_m4927/* method */
	, &AchievementDescription_t978_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 1013/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2071/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo AchievementDescription_t978_AchievementDescription_set_id_m4928_ParameterInfos[] = 
{
	{"value", 0, 134220144, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.AchievementDescription::set_id(System.String)
extern const MethodInfo AchievementDescription_set_id_m4928_MethodInfo = 
{
	"set_id"/* name */
	, (methodPointerType)&AchievementDescription_set_id_m4928/* method */
	, &AchievementDescription_t978_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, AchievementDescription_t978_AchievementDescription_set_id_m4928_ParameterInfos/* parameters */
	, 1014/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2072/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_title()
extern const MethodInfo AchievementDescription_get_title_m4929_MethodInfo = 
{
	"get_title"/* name */
	, (methodPointerType)&AchievementDescription_get_title_m4929/* method */
	, &AchievementDescription_t978_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2073/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_achievedDescription()
extern const MethodInfo AchievementDescription_get_achievedDescription_m4930_MethodInfo = 
{
	"get_achievedDescription"/* name */
	, (methodPointerType)&AchievementDescription_get_achievedDescription_m4930/* method */
	, &AchievementDescription_t978_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2074/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_unachievedDescription()
extern const MethodInfo AchievementDescription_get_unachievedDescription_m4931_MethodInfo = 
{
	"get_unachievedDescription"/* name */
	, (methodPointerType)&AchievementDescription_get_unachievedDescription_m4931/* method */
	, &AchievementDescription_t978_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2075/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_hidden()
extern const MethodInfo AchievementDescription_get_hidden_m4932_MethodInfo = 
{
	"get_hidden"/* name */
	, (methodPointerType)&AchievementDescription_get_hidden_m4932/* method */
	, &AchievementDescription_t978_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2076/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_points()
extern const MethodInfo AchievementDescription_get_points_m4933_MethodInfo = 
{
	"get_points"/* name */
	, (methodPointerType)&AchievementDescription_get_points_m4933/* method */
	, &AchievementDescription_t978_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2077/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AchievementDescription_t978_MethodInfos[] =
{
	&AchievementDescription__ctor_m4924_MethodInfo,
	&AchievementDescription_ToString_m4925_MethodInfo,
	&AchievementDescription_SetImage_m4926_MethodInfo,
	&AchievementDescription_get_id_m4927_MethodInfo,
	&AchievementDescription_set_id_m4928_MethodInfo,
	&AchievementDescription_get_title_m4929_MethodInfo,
	&AchievementDescription_get_achievedDescription_m4930_MethodInfo,
	&AchievementDescription_get_unachievedDescription_m4931_MethodInfo,
	&AchievementDescription_get_hidden_m4932_MethodInfo,
	&AchievementDescription_get_points_m4933_MethodInfo,
	NULL
};
extern const MethodInfo AchievementDescription_get_id_m4927_MethodInfo;
extern const MethodInfo AchievementDescription_set_id_m4928_MethodInfo;
static const PropertyInfo AchievementDescription_t978____id_PropertyInfo = 
{
	&AchievementDescription_t978_il2cpp_TypeInfo/* parent */
	, "id"/* name */
	, &AchievementDescription_get_id_m4927_MethodInfo/* get */
	, &AchievementDescription_set_id_m4928_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AchievementDescription_get_title_m4929_MethodInfo;
static const PropertyInfo AchievementDescription_t978____title_PropertyInfo = 
{
	&AchievementDescription_t978_il2cpp_TypeInfo/* parent */
	, "title"/* name */
	, &AchievementDescription_get_title_m4929_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AchievementDescription_get_achievedDescription_m4930_MethodInfo;
static const PropertyInfo AchievementDescription_t978____achievedDescription_PropertyInfo = 
{
	&AchievementDescription_t978_il2cpp_TypeInfo/* parent */
	, "achievedDescription"/* name */
	, &AchievementDescription_get_achievedDescription_m4930_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AchievementDescription_get_unachievedDescription_m4931_MethodInfo;
static const PropertyInfo AchievementDescription_t978____unachievedDescription_PropertyInfo = 
{
	&AchievementDescription_t978_il2cpp_TypeInfo/* parent */
	, "unachievedDescription"/* name */
	, &AchievementDescription_get_unachievedDescription_m4931_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AchievementDescription_get_hidden_m4932_MethodInfo;
static const PropertyInfo AchievementDescription_t978____hidden_PropertyInfo = 
{
	&AchievementDescription_t978_il2cpp_TypeInfo/* parent */
	, "hidden"/* name */
	, &AchievementDescription_get_hidden_m4932_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AchievementDescription_get_points_m4933_MethodInfo;
static const PropertyInfo AchievementDescription_t978____points_PropertyInfo = 
{
	&AchievementDescription_t978_il2cpp_TypeInfo/* parent */
	, "points"/* name */
	, &AchievementDescription_get_points_m4933_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* AchievementDescription_t978_PropertyInfos[] =
{
	&AchievementDescription_t978____id_PropertyInfo,
	&AchievementDescription_t978____title_PropertyInfo,
	&AchievementDescription_t978____achievedDescription_PropertyInfo,
	&AchievementDescription_t978____unachievedDescription_PropertyInfo,
	&AchievementDescription_t978____hidden_PropertyInfo,
	&AchievementDescription_t978____points_PropertyInfo,
	NULL
};
extern const MethodInfo AchievementDescription_ToString_m4925_MethodInfo;
static const Il2CppMethodReference AchievementDescription_t978_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&AchievementDescription_ToString_m4925_MethodInfo,
	&AchievementDescription_get_id_m4927_MethodInfo,
	&AchievementDescription_set_id_m4928_MethodInfo,
	&AchievementDescription_get_title_m4929_MethodInfo,
	&AchievementDescription_get_achievedDescription_m4930_MethodInfo,
	&AchievementDescription_get_unachievedDescription_m4931_MethodInfo,
	&AchievementDescription_get_hidden_m4932_MethodInfo,
	&AchievementDescription_get_points_m4933_MethodInfo,
};
static bool AchievementDescription_t978_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IAchievementDescription_t1127_0_0_0;
static const Il2CppType* AchievementDescription_t978_InterfacesTypeInfos[] = 
{
	&IAchievementDescription_t1127_0_0_0,
};
static Il2CppInterfaceOffsetPair AchievementDescription_t978_InterfacesOffsets[] = 
{
	{ &IAchievementDescription_t1127_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType AchievementDescription_t978_1_0_0;
struct AchievementDescription_t978;
const Il2CppTypeDefinitionMetadata AchievementDescription_t978_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, AchievementDescription_t978_InterfacesTypeInfos/* implementedInterfaces */
	, AchievementDescription_t978_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, AchievementDescription_t978_VTable/* vtableMethods */
	, AchievementDescription_t978_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1271/* fieldStart */

};
TypeInfo AchievementDescription_t978_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "AchievementDescription"/* name */
	, "UnityEngine.SocialPlatforms.Impl"/* namespaze */
	, AchievementDescription_t978_MethodInfos/* methods */
	, AchievementDescription_t978_PropertyInfos/* properties */
	, NULL/* events */
	, &AchievementDescription_t978_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AchievementDescription_t978_0_0_0/* byval_arg */
	, &AchievementDescription_t978_1_0_0/* this_arg */
	, &AchievementDescription_t978_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AchievementDescription_t978)/* instance_size */
	, sizeof (AchievementDescription_t978)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 6/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.Impl.Score
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Score.h"
// Metadata Definition UnityEngine.SocialPlatforms.Impl.Score
extern TypeInfo Score_t979_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.Impl.Score
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_ScoreMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int64_t1071_0_0_0;
extern const Il2CppType Int64_t1071_0_0_0;
static const ParameterInfo Score_t979_Score__ctor_m4934_ParameterInfos[] = 
{
	{"leaderboardID", 0, 134220145, 0, &String_t_0_0_0},
	{"value", 1, 134220146, 0, &Int64_t1071_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Int64_t1071 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Score::.ctor(System.String,System.Int64)
extern const MethodInfo Score__ctor_m4934_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Score__ctor_m4934/* method */
	, &Score_t979_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Int64_t1071/* invoker_method */
	, Score_t979_Score__ctor_m4934_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2078/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int64_t1071_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType DateTime_t406_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo Score_t979_Score__ctor_m4935_ParameterInfos[] = 
{
	{"leaderboardID", 0, 134220147, 0, &String_t_0_0_0},
	{"value", 1, 134220148, 0, &Int64_t1071_0_0_0},
	{"userID", 2, 134220149, 0, &String_t_0_0_0},
	{"date", 3, 134220150, 0, &DateTime_t406_0_0_0},
	{"formattedValue", 4, 134220151, 0, &String_t_0_0_0},
	{"rank", 5, 134220152, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Int64_t1071_Object_t_DateTime_t406_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Score::.ctor(System.String,System.Int64,System.String,System.DateTime,System.String,System.Int32)
extern const MethodInfo Score__ctor_m4935_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Score__ctor_m4935/* method */
	, &Score_t979_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Int64_t1071_Object_t_DateTime_t406_Object_t_Int32_t253/* invoker_method */
	, Score_t979_Score__ctor_m4935_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2079/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.Score::ToString()
extern const MethodInfo Score_ToString_m4936_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&Score_ToString_m4936/* method */
	, &Score_t979_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2080/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.Score::get_leaderboardID()
extern const MethodInfo Score_get_leaderboardID_m4937_MethodInfo = 
{
	"get_leaderboardID"/* name */
	, (methodPointerType)&Score_get_leaderboardID_m4937/* method */
	, &Score_t979_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 1017/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2081/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Score_t979_Score_set_leaderboardID_m4938_ParameterInfos[] = 
{
	{"value", 0, 134220153, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Score::set_leaderboardID(System.String)
extern const MethodInfo Score_set_leaderboardID_m4938_MethodInfo = 
{
	"set_leaderboardID"/* name */
	, (methodPointerType)&Score_set_leaderboardID_m4938/* method */
	, &Score_t979_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, Score_t979_Score_set_leaderboardID_m4938_ParameterInfos/* parameters */
	, 1018/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2082/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int64_t1071 (const MethodInfo* method, void* obj, void** args);
// System.Int64 UnityEngine.SocialPlatforms.Impl.Score::get_value()
extern const MethodInfo Score_get_value_m4939_MethodInfo = 
{
	"get_value"/* name */
	, (methodPointerType)&Score_get_value_m4939/* method */
	, &Score_t979_il2cpp_TypeInfo/* declaring_type */
	, &Int64_t1071_0_0_0/* return_type */
	, RuntimeInvoker_Int64_t1071/* invoker_method */
	, NULL/* parameters */
	, 1019/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2083/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int64_t1071_0_0_0;
static const ParameterInfo Score_t979_Score_set_value_m4940_ParameterInfos[] = 
{
	{"value", 0, 134220154, 0, &Int64_t1071_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int64_t1071 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Score::set_value(System.Int64)
extern const MethodInfo Score_set_value_m4940_MethodInfo = 
{
	"set_value"/* name */
	, (methodPointerType)&Score_set_value_m4940/* method */
	, &Score_t979_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int64_t1071/* invoker_method */
	, Score_t979_Score_set_value_m4940_ParameterInfos/* parameters */
	, 1020/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2084/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Score_t979_MethodInfos[] =
{
	&Score__ctor_m4934_MethodInfo,
	&Score__ctor_m4935_MethodInfo,
	&Score_ToString_m4936_MethodInfo,
	&Score_get_leaderboardID_m4937_MethodInfo,
	&Score_set_leaderboardID_m4938_MethodInfo,
	&Score_get_value_m4939_MethodInfo,
	&Score_set_value_m4940_MethodInfo,
	NULL
};
extern const MethodInfo Score_get_leaderboardID_m4937_MethodInfo;
extern const MethodInfo Score_set_leaderboardID_m4938_MethodInfo;
static const PropertyInfo Score_t979____leaderboardID_PropertyInfo = 
{
	&Score_t979_il2cpp_TypeInfo/* parent */
	, "leaderboardID"/* name */
	, &Score_get_leaderboardID_m4937_MethodInfo/* get */
	, &Score_set_leaderboardID_m4938_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Score_get_value_m4939_MethodInfo;
extern const MethodInfo Score_set_value_m4940_MethodInfo;
static const PropertyInfo Score_t979____value_PropertyInfo = 
{
	&Score_t979_il2cpp_TypeInfo/* parent */
	, "value"/* name */
	, &Score_get_value_m4939_MethodInfo/* get */
	, &Score_set_value_m4940_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Score_t979_PropertyInfos[] =
{
	&Score_t979____leaderboardID_PropertyInfo,
	&Score_t979____value_PropertyInfo,
	NULL
};
extern const MethodInfo Score_ToString_m4936_MethodInfo;
static const Il2CppMethodReference Score_t979_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Score_ToString_m4936_MethodInfo,
	&Score_get_leaderboardID_m4937_MethodInfo,
	&Score_set_leaderboardID_m4938_MethodInfo,
	&Score_get_value_m4939_MethodInfo,
	&Score_set_value_m4940_MethodInfo,
};
static bool Score_t979_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IScore_t980_0_0_0;
static const Il2CppType* Score_t979_InterfacesTypeInfos[] = 
{
	&IScore_t980_0_0_0,
};
static Il2CppInterfaceOffsetPair Score_t979_InterfacesOffsets[] = 
{
	{ &IScore_t980_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Score_t979_1_0_0;
struct Score_t979;
const Il2CppTypeDefinitionMetadata Score_t979_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Score_t979_InterfacesTypeInfos/* implementedInterfaces */
	, Score_t979_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Score_t979_VTable/* vtableMethods */
	, Score_t979_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1278/* fieldStart */

};
TypeInfo Score_t979_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Score"/* name */
	, "UnityEngine.SocialPlatforms.Impl"/* namespaze */
	, Score_t979_MethodInfos/* methods */
	, Score_t979_PropertyInfos/* properties */
	, NULL/* events */
	, &Score_t979_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Score_t979_0_0_0/* byval_arg */
	, &Score_t979_1_0_0/* this_arg */
	, &Score_t979_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Score_t979)/* instance_size */
	, sizeof (Score_t979)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 2/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.Impl.Leaderboard
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Leaderboard.h"
// Metadata Definition UnityEngine.SocialPlatforms.Impl.Leaderboard
extern TypeInfo Leaderboard_t797_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.Impl.Leaderboard
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_LeaderboardMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::.ctor()
extern const MethodInfo Leaderboard__ctor_m4941_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Leaderboard__ctor_m4941/* method */
	, &Leaderboard_t797_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2085/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.Leaderboard::ToString()
extern const MethodInfo Leaderboard_ToString_m4942_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&Leaderboard_ToString_m4942/* method */
	, &Leaderboard_t797_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2086/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IScore_t980_0_0_0;
static const ParameterInfo Leaderboard_t797_Leaderboard_SetLocalUserScore_m4943_ParameterInfos[] = 
{
	{"score", 0, 134220155, 0, &IScore_t980_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::SetLocalUserScore(UnityEngine.SocialPlatforms.IScore)
extern const MethodInfo Leaderboard_SetLocalUserScore_m4943_MethodInfo = 
{
	"SetLocalUserScore"/* name */
	, (methodPointerType)&Leaderboard_SetLocalUserScore_m4943/* method */
	, &Leaderboard_t797_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, Leaderboard_t797_Leaderboard_SetLocalUserScore_m4943_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2087/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UInt32_t1063_0_0_0;
static const ParameterInfo Leaderboard_t797_Leaderboard_SetMaxRange_m4944_ParameterInfos[] = 
{
	{"maxRange", 0, 134220156, 0, &UInt32_t1063_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::SetMaxRange(System.UInt32)
extern const MethodInfo Leaderboard_SetMaxRange_m4944_MethodInfo = 
{
	"SetMaxRange"/* name */
	, (methodPointerType)&Leaderboard_SetMaxRange_m4944/* method */
	, &Leaderboard_t797_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, Leaderboard_t797_Leaderboard_SetMaxRange_m4944_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2088/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IScoreU5BU5D_t981_0_0_0;
extern const Il2CppType IScoreU5BU5D_t981_0_0_0;
static const ParameterInfo Leaderboard_t797_Leaderboard_SetScores_m4945_ParameterInfos[] = 
{
	{"scores", 0, 134220157, 0, &IScoreU5BU5D_t981_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::SetScores(UnityEngine.SocialPlatforms.IScore[])
extern const MethodInfo Leaderboard_SetScores_m4945_MethodInfo = 
{
	"SetScores"/* name */
	, (methodPointerType)&Leaderboard_SetScores_m4945/* method */
	, &Leaderboard_t797_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, Leaderboard_t797_Leaderboard_SetScores_m4945_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2089/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Leaderboard_t797_Leaderboard_SetTitle_m4946_ParameterInfos[] = 
{
	{"title", 0, 134220158, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::SetTitle(System.String)
extern const MethodInfo Leaderboard_SetTitle_m4946_MethodInfo = 
{
	"SetTitle"/* name */
	, (methodPointerType)&Leaderboard_SetTitle_m4946/* method */
	, &Leaderboard_t797_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, Leaderboard_t797_Leaderboard_SetTitle_m4946_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2090/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StringU5BU5D_t243_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String[] UnityEngine.SocialPlatforms.Impl.Leaderboard::GetUserFilter()
extern const MethodInfo Leaderboard_GetUserFilter_m4947_MethodInfo = 
{
	"GetUserFilter"/* name */
	, (methodPointerType)&Leaderboard_GetUserFilter_m4947/* method */
	, &Leaderboard_t797_il2cpp_TypeInfo/* declaring_type */
	, &StringU5BU5D_t243_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2091/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.Leaderboard::get_id()
extern const MethodInfo Leaderboard_get_id_m4948_MethodInfo = 
{
	"get_id"/* name */
	, (methodPointerType)&Leaderboard_get_id_m4948/* method */
	, &Leaderboard_t797_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 1025/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2092/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Leaderboard_t797_Leaderboard_set_id_m4949_ParameterInfos[] = 
{
	{"value", 0, 134220159, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_id(System.String)
extern const MethodInfo Leaderboard_set_id_m4949_MethodInfo = 
{
	"set_id"/* name */
	, (methodPointerType)&Leaderboard_set_id_m4949/* method */
	, &Leaderboard_t797_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, Leaderboard_t797_Leaderboard_set_id_m4949_ParameterInfos/* parameters */
	, 1026/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2093/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UserScope_t988_0_0_0;
extern void* RuntimeInvoker_UserScope_t988 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.SocialPlatforms.UserScope UnityEngine.SocialPlatforms.Impl.Leaderboard::get_userScope()
extern const MethodInfo Leaderboard_get_userScope_m4950_MethodInfo = 
{
	"get_userScope"/* name */
	, (methodPointerType)&Leaderboard_get_userScope_m4950/* method */
	, &Leaderboard_t797_il2cpp_TypeInfo/* declaring_type */
	, &UserScope_t988_0_0_0/* return_type */
	, RuntimeInvoker_UserScope_t988/* invoker_method */
	, NULL/* parameters */
	, 1027/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2094/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UserScope_t988_0_0_0;
static const ParameterInfo Leaderboard_t797_Leaderboard_set_userScope_m4951_ParameterInfos[] = 
{
	{"value", 0, 134220160, 0, &UserScope_t988_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_userScope(UnityEngine.SocialPlatforms.UserScope)
extern const MethodInfo Leaderboard_set_userScope_m4951_MethodInfo = 
{
	"set_userScope"/* name */
	, (methodPointerType)&Leaderboard_set_userScope_m4951/* method */
	, &Leaderboard_t797_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, Leaderboard_t797_Leaderboard_set_userScope_m4951_ParameterInfos/* parameters */
	, 1028/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2095/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Range_t982_0_0_0;
extern void* RuntimeInvoker_Range_t982 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.SocialPlatforms.Range UnityEngine.SocialPlatforms.Impl.Leaderboard::get_range()
extern const MethodInfo Leaderboard_get_range_m4952_MethodInfo = 
{
	"get_range"/* name */
	, (methodPointerType)&Leaderboard_get_range_m4952/* method */
	, &Leaderboard_t797_il2cpp_TypeInfo/* declaring_type */
	, &Range_t982_0_0_0/* return_type */
	, RuntimeInvoker_Range_t982/* invoker_method */
	, NULL/* parameters */
	, 1029/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2096/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Range_t982_0_0_0;
static const ParameterInfo Leaderboard_t797_Leaderboard_set_range_m4953_ParameterInfos[] = 
{
	{"value", 0, 134220161, 0, &Range_t982_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Range_t982 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_range(UnityEngine.SocialPlatforms.Range)
extern const MethodInfo Leaderboard_set_range_m4953_MethodInfo = 
{
	"set_range"/* name */
	, (methodPointerType)&Leaderboard_set_range_m4953/* method */
	, &Leaderboard_t797_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Range_t982/* invoker_method */
	, Leaderboard_t797_Leaderboard_set_range_m4953_ParameterInfos/* parameters */
	, 1030/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2097/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeScope_t989_0_0_0;
extern void* RuntimeInvoker_TimeScope_t989 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.SocialPlatforms.TimeScope UnityEngine.SocialPlatforms.Impl.Leaderboard::get_timeScope()
extern const MethodInfo Leaderboard_get_timeScope_m4954_MethodInfo = 
{
	"get_timeScope"/* name */
	, (methodPointerType)&Leaderboard_get_timeScope_m4954/* method */
	, &Leaderboard_t797_il2cpp_TypeInfo/* declaring_type */
	, &TimeScope_t989_0_0_0/* return_type */
	, RuntimeInvoker_TimeScope_t989/* invoker_method */
	, NULL/* parameters */
	, 1031/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2098/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeScope_t989_0_0_0;
static const ParameterInfo Leaderboard_t797_Leaderboard_set_timeScope_m4955_ParameterInfos[] = 
{
	{"value", 0, 134220162, 0, &TimeScope_t989_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_timeScope(UnityEngine.SocialPlatforms.TimeScope)
extern const MethodInfo Leaderboard_set_timeScope_m4955_MethodInfo = 
{
	"set_timeScope"/* name */
	, (methodPointerType)&Leaderboard_set_timeScope_m4955/* method */
	, &Leaderboard_t797_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, Leaderboard_t797_Leaderboard_set_timeScope_m4955_ParameterInfos/* parameters */
	, 1032/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2099/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Leaderboard_t797_MethodInfos[] =
{
	&Leaderboard__ctor_m4941_MethodInfo,
	&Leaderboard_ToString_m4942_MethodInfo,
	&Leaderboard_SetLocalUserScore_m4943_MethodInfo,
	&Leaderboard_SetMaxRange_m4944_MethodInfo,
	&Leaderboard_SetScores_m4945_MethodInfo,
	&Leaderboard_SetTitle_m4946_MethodInfo,
	&Leaderboard_GetUserFilter_m4947_MethodInfo,
	&Leaderboard_get_id_m4948_MethodInfo,
	&Leaderboard_set_id_m4949_MethodInfo,
	&Leaderboard_get_userScope_m4950_MethodInfo,
	&Leaderboard_set_userScope_m4951_MethodInfo,
	&Leaderboard_get_range_m4952_MethodInfo,
	&Leaderboard_set_range_m4953_MethodInfo,
	&Leaderboard_get_timeScope_m4954_MethodInfo,
	&Leaderboard_set_timeScope_m4955_MethodInfo,
	NULL
};
extern const MethodInfo Leaderboard_get_id_m4948_MethodInfo;
extern const MethodInfo Leaderboard_set_id_m4949_MethodInfo;
static const PropertyInfo Leaderboard_t797____id_PropertyInfo = 
{
	&Leaderboard_t797_il2cpp_TypeInfo/* parent */
	, "id"/* name */
	, &Leaderboard_get_id_m4948_MethodInfo/* get */
	, &Leaderboard_set_id_m4949_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Leaderboard_get_userScope_m4950_MethodInfo;
extern const MethodInfo Leaderboard_set_userScope_m4951_MethodInfo;
static const PropertyInfo Leaderboard_t797____userScope_PropertyInfo = 
{
	&Leaderboard_t797_il2cpp_TypeInfo/* parent */
	, "userScope"/* name */
	, &Leaderboard_get_userScope_m4950_MethodInfo/* get */
	, &Leaderboard_set_userScope_m4951_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Leaderboard_get_range_m4952_MethodInfo;
extern const MethodInfo Leaderboard_set_range_m4953_MethodInfo;
static const PropertyInfo Leaderboard_t797____range_PropertyInfo = 
{
	&Leaderboard_t797_il2cpp_TypeInfo/* parent */
	, "range"/* name */
	, &Leaderboard_get_range_m4952_MethodInfo/* get */
	, &Leaderboard_set_range_m4953_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Leaderboard_get_timeScope_m4954_MethodInfo;
extern const MethodInfo Leaderboard_set_timeScope_m4955_MethodInfo;
static const PropertyInfo Leaderboard_t797____timeScope_PropertyInfo = 
{
	&Leaderboard_t797_il2cpp_TypeInfo/* parent */
	, "timeScope"/* name */
	, &Leaderboard_get_timeScope_m4954_MethodInfo/* get */
	, &Leaderboard_set_timeScope_m4955_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Leaderboard_t797_PropertyInfos[] =
{
	&Leaderboard_t797____id_PropertyInfo,
	&Leaderboard_t797____userScope_PropertyInfo,
	&Leaderboard_t797____range_PropertyInfo,
	&Leaderboard_t797____timeScope_PropertyInfo,
	NULL
};
extern const MethodInfo Leaderboard_ToString_m4942_MethodInfo;
static const Il2CppMethodReference Leaderboard_t797_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Leaderboard_ToString_m4942_MethodInfo,
	&Leaderboard_get_id_m4948_MethodInfo,
	&Leaderboard_get_userScope_m4950_MethodInfo,
	&Leaderboard_get_range_m4952_MethodInfo,
	&Leaderboard_get_timeScope_m4954_MethodInfo,
	&Leaderboard_set_id_m4949_MethodInfo,
	&Leaderboard_set_userScope_m4951_MethodInfo,
	&Leaderboard_set_range_m4953_MethodInfo,
	&Leaderboard_set_timeScope_m4955_MethodInfo,
};
static bool Leaderboard_t797_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ILeaderboard_t1020_0_0_0;
static const Il2CppType* Leaderboard_t797_InterfacesTypeInfos[] = 
{
	&ILeaderboard_t1020_0_0_0,
};
static Il2CppInterfaceOffsetPair Leaderboard_t797_InterfacesOffsets[] = 
{
	{ &ILeaderboard_t1020_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Leaderboard_t797_0_0_0;
extern const Il2CppType Leaderboard_t797_1_0_0;
struct Leaderboard_t797;
const Il2CppTypeDefinitionMetadata Leaderboard_t797_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Leaderboard_t797_InterfacesTypeInfos/* implementedInterfaces */
	, Leaderboard_t797_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Leaderboard_t797_VTable/* vtableMethods */
	, Leaderboard_t797_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1284/* fieldStart */

};
TypeInfo Leaderboard_t797_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Leaderboard"/* name */
	, "UnityEngine.SocialPlatforms.Impl"/* namespaze */
	, Leaderboard_t797_MethodInfos/* methods */
	, Leaderboard_t797_PropertyInfos/* properties */
	, NULL/* events */
	, &Leaderboard_t797_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Leaderboard_t797_0_0_0/* byval_arg */
	, &Leaderboard_t797_1_0_0/* this_arg */
	, &Leaderboard_t797_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Leaderboard_t797)/* instance_size */
	, sizeof (Leaderboard_t797)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 15/* method_count */
	, 4/* property_count */
	, 10/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 12/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SendMouseEvents/HitInfo
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfo.h"
// Metadata Definition UnityEngine.SendMouseEvents/HitInfo
extern TypeInfo HitInfo_t983_il2cpp_TypeInfo;
// UnityEngine.SendMouseEvents/HitInfo
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfoMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo HitInfo_t983_HitInfo_SendMessage_m4956_ParameterInfos[] = 
{
	{"name", 0, 134220167, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SendMouseEvents/HitInfo::SendMessage(System.String)
extern const MethodInfo HitInfo_SendMessage_m4956_MethodInfo = 
{
	"SendMessage"/* name */
	, (methodPointerType)&HitInfo_SendMessage_m4956/* method */
	, &HitInfo_t983_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, HitInfo_t983_HitInfo_SendMessage_m4956_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2103/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType HitInfo_t983_0_0_0;
extern const Il2CppType HitInfo_t983_0_0_0;
extern const Il2CppType HitInfo_t983_0_0_0;
static const ParameterInfo HitInfo_t983_HitInfo_Compare_m4957_ParameterInfos[] = 
{
	{"lhs", 0, 134220168, 0, &HitInfo_t983_0_0_0},
	{"rhs", 1, 134220169, 0, &HitInfo_t983_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_HitInfo_t983_HitInfo_t983 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.SendMouseEvents/HitInfo::Compare(UnityEngine.SendMouseEvents/HitInfo,UnityEngine.SendMouseEvents/HitInfo)
extern const MethodInfo HitInfo_Compare_m4957_MethodInfo = 
{
	"Compare"/* name */
	, (methodPointerType)&HitInfo_Compare_m4957/* method */
	, &HitInfo_t983_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_HitInfo_t983_HitInfo_t983/* invoker_method */
	, HitInfo_t983_HitInfo_Compare_m4957_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2104/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType HitInfo_t983_0_0_0;
static const ParameterInfo HitInfo_t983_HitInfo_op_Implicit_m4958_ParameterInfos[] = 
{
	{"exists", 0, 134220170, 0, &HitInfo_t983_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_HitInfo_t983 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.SendMouseEvents/HitInfo::op_Implicit(UnityEngine.SendMouseEvents/HitInfo)
extern const MethodInfo HitInfo_op_Implicit_m4958_MethodInfo = 
{
	"op_Implicit"/* name */
	, (methodPointerType)&HitInfo_op_Implicit_m4958/* method */
	, &HitInfo_t983_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_HitInfo_t983/* invoker_method */
	, HitInfo_t983_HitInfo_op_Implicit_m4958_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2105/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* HitInfo_t983_MethodInfos[] =
{
	&HitInfo_SendMessage_m4956_MethodInfo,
	&HitInfo_Compare_m4957_MethodInfo,
	&HitInfo_op_Implicit_m4958_MethodInfo,
	NULL
};
static const Il2CppMethodReference HitInfo_t983_VTable[] =
{
	&ValueType_Equals_m1076_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&ValueType_GetHashCode_m1077_MethodInfo,
	&ValueType_ToString_m1078_MethodInfo,
};
static bool HitInfo_t983_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType HitInfo_t983_1_0_0;
extern TypeInfo SendMouseEvents_t986_il2cpp_TypeInfo;
extern const Il2CppType SendMouseEvents_t986_0_0_0;
const Il2CppTypeDefinitionMetadata HitInfo_t983_DefinitionMetadata = 
{
	&SendMouseEvents_t986_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t285_0_0_0/* parent */
	, HitInfo_t983_VTable/* vtableMethods */
	, HitInfo_t983_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1294/* fieldStart */

};
TypeInfo HitInfo_t983_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "HitInfo"/* name */
	, ""/* namespaze */
	, HitInfo_t983_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &HitInfo_t983_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &HitInfo_t983_0_0_0/* byval_arg */
	, &HitInfo_t983_1_0_0/* this_arg */
	, &HitInfo_t983_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HitInfo_t983)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (HitInfo_t983)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048843/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.SendMouseEvents
#include "UnityEngine_UnityEngine_SendMouseEvents.h"
// Metadata Definition UnityEngine.SendMouseEvents
// UnityEngine.SendMouseEvents
#include "UnityEngine_UnityEngine_SendMouseEventsMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SendMouseEvents::.cctor()
extern const MethodInfo SendMouseEvents__cctor_m4959_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&SendMouseEvents__cctor_m4959/* method */
	, &SendMouseEvents_t986_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2100/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo SendMouseEvents_t986_SendMouseEvents_DoSendMouseEvents_m4960_ParameterInfos[] = 
{
	{"mouseUsed", 0, 134220163, 0, &Int32_t253_0_0_0},
	{"skipRTCameras", 1, 134220164, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SendMouseEvents::DoSendMouseEvents(System.Int32,System.Int32)
extern const MethodInfo SendMouseEvents_DoSendMouseEvents_m4960_MethodInfo = 
{
	"DoSendMouseEvents"/* name */
	, (methodPointerType)&SendMouseEvents_DoSendMouseEvents_m4960/* method */
	, &SendMouseEvents_t986_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253_Int32_t253/* invoker_method */
	, SendMouseEvents_t986_SendMouseEvents_DoSendMouseEvents_m4960_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2101/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType HitInfo_t983_0_0_0;
static const ParameterInfo SendMouseEvents_t986_SendMouseEvents_SendEvents_m4961_ParameterInfos[] = 
{
	{"i", 0, 134220165, 0, &Int32_t253_0_0_0},
	{"hit", 1, 134220166, 0, &HitInfo_t983_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253_HitInfo_t983 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SendMouseEvents::SendEvents(System.Int32,UnityEngine.SendMouseEvents/HitInfo)
extern const MethodInfo SendMouseEvents_SendEvents_m4961_MethodInfo = 
{
	"SendEvents"/* name */
	, (methodPointerType)&SendMouseEvents_SendEvents_m4961/* method */
	, &SendMouseEvents_t986_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253_HitInfo_t983/* invoker_method */
	, SendMouseEvents_t986_SendMouseEvents_SendEvents_m4961_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2102/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SendMouseEvents_t986_MethodInfos[] =
{
	&SendMouseEvents__cctor_m4959_MethodInfo,
	&SendMouseEvents_DoSendMouseEvents_m4960_MethodInfo,
	&SendMouseEvents_SendEvents_m4961_MethodInfo,
	NULL
};
static const Il2CppType* SendMouseEvents_t986_il2cpp_TypeInfo__nestedTypes[1] =
{
	&HitInfo_t983_0_0_0,
};
static const Il2CppMethodReference SendMouseEvents_t986_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool SendMouseEvents_t986_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType SendMouseEvents_t986_1_0_0;
struct SendMouseEvents_t986;
const Il2CppTypeDefinitionMetadata SendMouseEvents_t986_DefinitionMetadata = 
{
	NULL/* declaringType */
	, SendMouseEvents_t986_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SendMouseEvents_t986_VTable/* vtableMethods */
	, SendMouseEvents_t986_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1296/* fieldStart */

};
TypeInfo SendMouseEvents_t986_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SendMouseEvents"/* name */
	, "UnityEngine"/* namespaze */
	, SendMouseEvents_t986_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SendMouseEvents_t986_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SendMouseEvents_t986_0_0_0/* byval_arg */
	, &SendMouseEvents_t986_1_0_0/* this_arg */
	, &SendMouseEvents_t986_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SendMouseEvents_t986)/* instance_size */
	, sizeof (SendMouseEvents_t986)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(SendMouseEvents_t986_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.SocialPlatforms.ISocialPlatform
extern TypeInfo ISocialPlatform_t1125_il2cpp_TypeInfo;
extern const Il2CppType ILocalUser_t1017_0_0_0;
extern const Il2CppType Action_1_t787_0_0_0;
extern const Il2CppType Action_1_t787_0_0_0;
static const ParameterInfo ISocialPlatform_t1125_ISocialPlatform_Authenticate_m5325_ParameterInfos[] = 
{
	{"user", 0, 134220171, 0, &ILocalUser_t1017_0_0_0},
	{"callback", 1, 134220172, 0, &Action_1_t787_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.ISocialPlatform::Authenticate(UnityEngine.SocialPlatforms.ILocalUser,System.Action`1<System.Boolean>)
extern const MethodInfo ISocialPlatform_Authenticate_m5325_MethodInfo = 
{
	"Authenticate"/* name */
	, NULL/* method */
	, &ISocialPlatform_t1125_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, ISocialPlatform_t1125_ISocialPlatform_Authenticate_m5325_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2106/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ILocalUser_t1017_0_0_0;
extern const Il2CppType Action_1_t787_0_0_0;
static const ParameterInfo ISocialPlatform_t1125_ISocialPlatform_LoadFriends_m5326_ParameterInfos[] = 
{
	{"user", 0, 134220173, 0, &ILocalUser_t1017_0_0_0},
	{"callback", 1, 134220174, 0, &Action_1_t787_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.ISocialPlatform::LoadFriends(UnityEngine.SocialPlatforms.ILocalUser,System.Action`1<System.Boolean>)
extern const MethodInfo ISocialPlatform_LoadFriends_m5326_MethodInfo = 
{
	"LoadFriends"/* name */
	, NULL/* method */
	, &ISocialPlatform_t1125_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, ISocialPlatform_t1125_ISocialPlatform_LoadFriends_m5326_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2107/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ISocialPlatform_t1125_MethodInfos[] =
{
	&ISocialPlatform_Authenticate_m5325_MethodInfo,
	&ISocialPlatform_LoadFriends_m5326_MethodInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ISocialPlatform_t1125_0_0_0;
extern const Il2CppType ISocialPlatform_t1125_1_0_0;
struct ISocialPlatform_t1125;
const Il2CppTypeDefinitionMetadata ISocialPlatform_t1125_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ISocialPlatform_t1125_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ISocialPlatform"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, ISocialPlatform_t1125_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ISocialPlatform_t1125_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ISocialPlatform_t1125_0_0_0/* byval_arg */
	, &ISocialPlatform_t1125_1_0_0/* this_arg */
	, &ISocialPlatform_t1125_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.SocialPlatforms.ILocalUser
extern TypeInfo ILocalUser_t1017_il2cpp_TypeInfo;
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.SocialPlatforms.ILocalUser::get_authenticated()
extern const MethodInfo ILocalUser_get_authenticated_m5327_MethodInfo = 
{
	"get_authenticated"/* name */
	, NULL/* method */
	, &ILocalUser_t1017_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2108/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ILocalUser_t1017_MethodInfos[] =
{
	&ILocalUser_get_authenticated_m5327_MethodInfo,
	NULL
};
extern const MethodInfo ILocalUser_get_authenticated_m5327_MethodInfo;
static const PropertyInfo ILocalUser_t1017____authenticated_PropertyInfo = 
{
	&ILocalUser_t1017_il2cpp_TypeInfo/* parent */
	, "authenticated"/* name */
	, &ILocalUser_get_authenticated_m5327_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ILocalUser_t1017_PropertyInfos[] =
{
	&ILocalUser_t1017____authenticated_PropertyInfo,
	NULL
};
static const Il2CppType* ILocalUser_t1017_InterfacesTypeInfos[] = 
{
	&IUserProfile_t1126_0_0_0,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ILocalUser_t1017_1_0_0;
struct ILocalUser_t1017;
const Il2CppTypeDefinitionMetadata ILocalUser_t1017_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ILocalUser_t1017_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ILocalUser_t1017_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ILocalUser"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, ILocalUser_t1017_MethodInfos/* methods */
	, ILocalUser_t1017_PropertyInfos/* properties */
	, NULL/* events */
	, &ILocalUser_t1017_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ILocalUser_t1017_0_0_0/* byval_arg */
	, &ILocalUser_t1017_1_0_0/* this_arg */
	, &ILocalUser_t1017_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.UserState
#include "UnityEngine_UnityEngine_SocialPlatforms_UserState.h"
// Metadata Definition UnityEngine.SocialPlatforms.UserState
extern TypeInfo UserState_t987_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.UserState
#include "UnityEngine_UnityEngine_SocialPlatforms_UserStateMethodDeclarations.h"
static const MethodInfo* UserState_t987_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference UserState_t987_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool UserState_t987_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UserState_t987_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UserState_t987_1_0_0;
const Il2CppTypeDefinitionMetadata UserState_t987_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UserState_t987_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, UserState_t987_VTable/* vtableMethods */
	, UserState_t987_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1303/* fieldStart */

};
TypeInfo UserState_t987_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UserState"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, UserState_t987_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UserState_t987_0_0_0/* byval_arg */
	, &UserState_t987_1_0_0/* this_arg */
	, &UserState_t987_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UserState_t987)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UserState_t987)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Metadata Definition UnityEngine.SocialPlatforms.IUserProfile
extern TypeInfo IUserProfile_t1126_il2cpp_TypeInfo;
static const MethodInfo* IUserProfile_t1126_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType IUserProfile_t1126_1_0_0;
struct IUserProfile_t1126;
const Il2CppTypeDefinitionMetadata IUserProfile_t1126_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IUserProfile_t1126_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "IUserProfile"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, IUserProfile_t1126_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IUserProfile_t1126_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IUserProfile_t1126_0_0_0/* byval_arg */
	, &IUserProfile_t1126_1_0_0/* this_arg */
	, &IUserProfile_t1126_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.SocialPlatforms.IAchievement
extern TypeInfo IAchievement_t1021_il2cpp_TypeInfo;
static const MethodInfo* IAchievement_t1021_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType IAchievement_t1021_1_0_0;
struct IAchievement_t1021;
const Il2CppTypeDefinitionMetadata IAchievement_t1021_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IAchievement_t1021_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "IAchievement"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, IAchievement_t1021_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IAchievement_t1021_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IAchievement_t1021_0_0_0/* byval_arg */
	, &IAchievement_t1021_1_0_0/* this_arg */
	, &IAchievement_t1021_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.SocialPlatforms.IAchievementDescription
extern TypeInfo IAchievementDescription_t1127_il2cpp_TypeInfo;
static const MethodInfo* IAchievementDescription_t1127_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType IAchievementDescription_t1127_1_0_0;
struct IAchievementDescription_t1127;
const Il2CppTypeDefinitionMetadata IAchievementDescription_t1127_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IAchievementDescription_t1127_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "IAchievementDescription"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, IAchievementDescription_t1127_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IAchievementDescription_t1127_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IAchievementDescription_t1127_0_0_0/* byval_arg */
	, &IAchievementDescription_t1127_1_0_0/* this_arg */
	, &IAchievementDescription_t1127_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.SocialPlatforms.IScore
extern TypeInfo IScore_t980_il2cpp_TypeInfo;
static const MethodInfo* IScore_t980_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType IScore_t980_1_0_0;
struct IScore_t980;
const Il2CppTypeDefinitionMetadata IScore_t980_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IScore_t980_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "IScore"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, IScore_t980_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IScore_t980_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IScore_t980_0_0_0/* byval_arg */
	, &IScore_t980_1_0_0/* this_arg */
	, &IScore_t980_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.UserScope
#include "UnityEngine_UnityEngine_SocialPlatforms_UserScope.h"
// Metadata Definition UnityEngine.SocialPlatforms.UserScope
extern TypeInfo UserScope_t988_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.UserScope
#include "UnityEngine_UnityEngine_SocialPlatforms_UserScopeMethodDeclarations.h"
static const MethodInfo* UserScope_t988_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference UserScope_t988_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool UserScope_t988_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UserScope_t988_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UserScope_t988_1_0_0;
const Il2CppTypeDefinitionMetadata UserScope_t988_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UserScope_t988_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, UserScope_t988_VTable/* vtableMethods */
	, UserScope_t988_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1309/* fieldStart */

};
TypeInfo UserScope_t988_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UserScope"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, UserScope_t988_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UserScope_t988_0_0_0/* byval_arg */
	, &UserScope_t988_1_0_0/* this_arg */
	, &UserScope_t988_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UserScope_t988)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UserScope_t988)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.TimeScope
#include "UnityEngine_UnityEngine_SocialPlatforms_TimeScope.h"
// Metadata Definition UnityEngine.SocialPlatforms.TimeScope
extern TypeInfo TimeScope_t989_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.TimeScope
#include "UnityEngine_UnityEngine_SocialPlatforms_TimeScopeMethodDeclarations.h"
static const MethodInfo* TimeScope_t989_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference TimeScope_t989_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool TimeScope_t989_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TimeScope_t989_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TimeScope_t989_1_0_0;
const Il2CppTypeDefinitionMetadata TimeScope_t989_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TimeScope_t989_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, TimeScope_t989_VTable/* vtableMethods */
	, TimeScope_t989_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1312/* fieldStart */

};
TypeInfo TimeScope_t989_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TimeScope"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, TimeScope_t989_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TimeScope_t989_0_0_0/* byval_arg */
	, &TimeScope_t989_1_0_0/* this_arg */
	, &TimeScope_t989_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TimeScope_t989)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TimeScope_t989)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.Range
#include "UnityEngine_UnityEngine_SocialPlatforms_Range.h"
// Metadata Definition UnityEngine.SocialPlatforms.Range
extern TypeInfo Range_t982_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.Range
#include "UnityEngine_UnityEngine_SocialPlatforms_RangeMethodDeclarations.h"
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo Range_t982_Range__ctor_m4962_ParameterInfos[] = 
{
	{"fromValue", 0, 134220175, 0, &Int32_t253_0_0_0},
	{"valueCount", 1, 134220176, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Range::.ctor(System.Int32,System.Int32)
extern const MethodInfo Range__ctor_m4962_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Range__ctor_m4962/* method */
	, &Range_t982_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253_Int32_t253/* invoker_method */
	, Range_t982_Range__ctor_m4962_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2109/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Range_t982_MethodInfos[] =
{
	&Range__ctor_m4962_MethodInfo,
	NULL
};
static const Il2CppMethodReference Range_t982_VTable[] =
{
	&ValueType_Equals_m1076_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&ValueType_GetHashCode_m1077_MethodInfo,
	&ValueType_ToString_m1078_MethodInfo,
};
static bool Range_t982_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Range_t982_1_0_0;
const Il2CppTypeDefinitionMetadata Range_t982_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t285_0_0_0/* parent */
	, Range_t982_VTable/* vtableMethods */
	, Range_t982_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1316/* fieldStart */

};
TypeInfo Range_t982_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Range"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, Range_t982_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Range_t982_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Range_t982_0_0_0/* byval_arg */
	, &Range_t982_1_0_0/* this_arg */
	, &Range_t982_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Range_t982)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Range_t982)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Range_t982 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.SocialPlatforms.ILeaderboard
extern TypeInfo ILeaderboard_t1020_il2cpp_TypeInfo;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.ILeaderboard::get_id()
extern const MethodInfo ILeaderboard_get_id_m5328_MethodInfo = 
{
	"get_id"/* name */
	, NULL/* method */
	, &ILeaderboard_t1020_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2110/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_UserScope_t988 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.SocialPlatforms.UserScope UnityEngine.SocialPlatforms.ILeaderboard::get_userScope()
extern const MethodInfo ILeaderboard_get_userScope_m5329_MethodInfo = 
{
	"get_userScope"/* name */
	, NULL/* method */
	, &ILeaderboard_t1020_il2cpp_TypeInfo/* declaring_type */
	, &UserScope_t988_0_0_0/* return_type */
	, RuntimeInvoker_UserScope_t988/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2111/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Range_t982 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.SocialPlatforms.Range UnityEngine.SocialPlatforms.ILeaderboard::get_range()
extern const MethodInfo ILeaderboard_get_range_m5330_MethodInfo = 
{
	"get_range"/* name */
	, NULL/* method */
	, &ILeaderboard_t1020_il2cpp_TypeInfo/* declaring_type */
	, &Range_t982_0_0_0/* return_type */
	, RuntimeInvoker_Range_t982/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2112/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_TimeScope_t989 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.SocialPlatforms.TimeScope UnityEngine.SocialPlatforms.ILeaderboard::get_timeScope()
extern const MethodInfo ILeaderboard_get_timeScope_m5331_MethodInfo = 
{
	"get_timeScope"/* name */
	, NULL/* method */
	, &ILeaderboard_t1020_il2cpp_TypeInfo/* declaring_type */
	, &TimeScope_t989_0_0_0/* return_type */
	, RuntimeInvoker_TimeScope_t989/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2113/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ILeaderboard_t1020_MethodInfos[] =
{
	&ILeaderboard_get_id_m5328_MethodInfo,
	&ILeaderboard_get_userScope_m5329_MethodInfo,
	&ILeaderboard_get_range_m5330_MethodInfo,
	&ILeaderboard_get_timeScope_m5331_MethodInfo,
	NULL
};
extern const MethodInfo ILeaderboard_get_id_m5328_MethodInfo;
static const PropertyInfo ILeaderboard_t1020____id_PropertyInfo = 
{
	&ILeaderboard_t1020_il2cpp_TypeInfo/* parent */
	, "id"/* name */
	, &ILeaderboard_get_id_m5328_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ILeaderboard_get_userScope_m5329_MethodInfo;
static const PropertyInfo ILeaderboard_t1020____userScope_PropertyInfo = 
{
	&ILeaderboard_t1020_il2cpp_TypeInfo/* parent */
	, "userScope"/* name */
	, &ILeaderboard_get_userScope_m5329_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ILeaderboard_get_range_m5330_MethodInfo;
static const PropertyInfo ILeaderboard_t1020____range_PropertyInfo = 
{
	&ILeaderboard_t1020_il2cpp_TypeInfo/* parent */
	, "range"/* name */
	, &ILeaderboard_get_range_m5330_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ILeaderboard_get_timeScope_m5331_MethodInfo;
static const PropertyInfo ILeaderboard_t1020____timeScope_PropertyInfo = 
{
	&ILeaderboard_t1020_il2cpp_TypeInfo/* parent */
	, "timeScope"/* name */
	, &ILeaderboard_get_timeScope_m5331_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ILeaderboard_t1020_PropertyInfos[] =
{
	&ILeaderboard_t1020____id_PropertyInfo,
	&ILeaderboard_t1020____userScope_PropertyInfo,
	&ILeaderboard_t1020____range_PropertyInfo,
	&ILeaderboard_t1020____timeScope_PropertyInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ILeaderboard_t1020_1_0_0;
struct ILeaderboard_t1020;
const Il2CppTypeDefinitionMetadata ILeaderboard_t1020_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ILeaderboard_t1020_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ILeaderboard"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, ILeaderboard_t1020_MethodInfos/* methods */
	, ILeaderboard_t1020_PropertyInfos/* properties */
	, NULL/* events */
	, &ILeaderboard_t1020_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ILeaderboard_t1020_0_0_0/* byval_arg */
	, &ILeaderboard_t1020_1_0_0/* this_arg */
	, &ILeaderboard_t1020_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 4/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.PropertyAttribute
#include "UnityEngine_UnityEngine_PropertyAttribute.h"
// Metadata Definition UnityEngine.PropertyAttribute
extern TypeInfo PropertyAttribute_t990_il2cpp_TypeInfo;
// UnityEngine.PropertyAttribute
#include "UnityEngine_UnityEngine_PropertyAttributeMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.PropertyAttribute::.ctor()
extern const MethodInfo PropertyAttribute__ctor_m4963_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PropertyAttribute__ctor_m4963/* method */
	, &PropertyAttribute_t990_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2114/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* PropertyAttribute_t990_MethodInfos[] =
{
	&PropertyAttribute__ctor_m4963_MethodInfo,
	NULL
};
static const Il2CppMethodReference PropertyAttribute_t990_VTable[] =
{
	&Attribute_Equals_m5384_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Attribute_GetHashCode_m5253_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool PropertyAttribute_t990_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair PropertyAttribute_t990_InterfacesOffsets[] = 
{
	{ &_Attribute_t1145_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType PropertyAttribute_t990_0_0_0;
extern const Il2CppType PropertyAttribute_t990_1_0_0;
struct PropertyAttribute_t990;
const Il2CppTypeDefinitionMetadata PropertyAttribute_t990_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, PropertyAttribute_t990_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t805_0_0_0/* parent */
	, PropertyAttribute_t990_VTable/* vtableMethods */
	, PropertyAttribute_t990_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo PropertyAttribute_t990_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "PropertyAttribute"/* name */
	, "UnityEngine"/* namespaze */
	, PropertyAttribute_t990_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &PropertyAttribute_t990_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1033/* custom_attributes_cache */
	, &PropertyAttribute_t990_0_0_0/* byval_arg */
	, &PropertyAttribute_t990_1_0_0/* this_arg */
	, &PropertyAttribute_t990_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PropertyAttribute_t990)/* instance_size */
	, sizeof (PropertyAttribute_t990)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.TooltipAttribute
#include "UnityEngine_UnityEngine_TooltipAttribute.h"
// Metadata Definition UnityEngine.TooltipAttribute
extern TypeInfo TooltipAttribute_t266_il2cpp_TypeInfo;
// UnityEngine.TooltipAttribute
#include "UnityEngine_UnityEngine_TooltipAttributeMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo TooltipAttribute_t266_TooltipAttribute__ctor_m1042_ParameterInfos[] = 
{
	{"tooltip", 0, 134220177, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.TooltipAttribute::.ctor(System.String)
extern const MethodInfo TooltipAttribute__ctor_m1042_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TooltipAttribute__ctor_m1042/* method */
	, &TooltipAttribute_t266_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, TooltipAttribute_t266_TooltipAttribute__ctor_m1042_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2115/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TooltipAttribute_t266_MethodInfos[] =
{
	&TooltipAttribute__ctor_m1042_MethodInfo,
	NULL
};
static const Il2CppMethodReference TooltipAttribute_t266_VTable[] =
{
	&Attribute_Equals_m5384_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Attribute_GetHashCode_m5253_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool TooltipAttribute_t266_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TooltipAttribute_t266_InterfacesOffsets[] = 
{
	{ &_Attribute_t1145_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TooltipAttribute_t266_0_0_0;
extern const Il2CppType TooltipAttribute_t266_1_0_0;
struct TooltipAttribute_t266;
const Il2CppTypeDefinitionMetadata TooltipAttribute_t266_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TooltipAttribute_t266_InterfacesOffsets/* interfaceOffsets */
	, &PropertyAttribute_t990_0_0_0/* parent */
	, TooltipAttribute_t266_VTable/* vtableMethods */
	, TooltipAttribute_t266_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1318/* fieldStart */

};
TypeInfo TooltipAttribute_t266_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TooltipAttribute"/* name */
	, "UnityEngine"/* namespaze */
	, TooltipAttribute_t266_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TooltipAttribute_t266_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1034/* custom_attributes_cache */
	, &TooltipAttribute_t266_0_0_0/* byval_arg */
	, &TooltipAttribute_t266_1_0_0/* this_arg */
	, &TooltipAttribute_t266_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TooltipAttribute_t266)/* instance_size */
	, sizeof (TooltipAttribute_t266)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SpaceAttribute
#include "UnityEngine_UnityEngine_SpaceAttribute.h"
// Metadata Definition UnityEngine.SpaceAttribute
extern TypeInfo SpaceAttribute_t726_il2cpp_TypeInfo;
// UnityEngine.SpaceAttribute
#include "UnityEngine_UnityEngine_SpaceAttributeMethodDeclarations.h"
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo SpaceAttribute_t726_SpaceAttribute__ctor_m3533_ParameterInfos[] = 
{
	{"height", 0, 134220178, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SpaceAttribute::.ctor(System.Single)
extern const MethodInfo SpaceAttribute__ctor_m3533_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SpaceAttribute__ctor_m3533/* method */
	, &SpaceAttribute_t726_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Single_t254/* invoker_method */
	, SpaceAttribute_t726_SpaceAttribute__ctor_m3533_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2116/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SpaceAttribute_t726_MethodInfos[] =
{
	&SpaceAttribute__ctor_m3533_MethodInfo,
	NULL
};
static const Il2CppMethodReference SpaceAttribute_t726_VTable[] =
{
	&Attribute_Equals_m5384_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Attribute_GetHashCode_m5253_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool SpaceAttribute_t726_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair SpaceAttribute_t726_InterfacesOffsets[] = 
{
	{ &_Attribute_t1145_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType SpaceAttribute_t726_0_0_0;
extern const Il2CppType SpaceAttribute_t726_1_0_0;
struct SpaceAttribute_t726;
const Il2CppTypeDefinitionMetadata SpaceAttribute_t726_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SpaceAttribute_t726_InterfacesOffsets/* interfaceOffsets */
	, &PropertyAttribute_t990_0_0_0/* parent */
	, SpaceAttribute_t726_VTable/* vtableMethods */
	, SpaceAttribute_t726_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1319/* fieldStart */

};
TypeInfo SpaceAttribute_t726_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SpaceAttribute"/* name */
	, "UnityEngine"/* namespaze */
	, SpaceAttribute_t726_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SpaceAttribute_t726_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1035/* custom_attributes_cache */
	, &SpaceAttribute_t726_0_0_0/* byval_arg */
	, &SpaceAttribute_t726_1_0_0/* this_arg */
	, &SpaceAttribute_t726_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SpaceAttribute_t726)/* instance_size */
	, sizeof (SpaceAttribute_t726)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
