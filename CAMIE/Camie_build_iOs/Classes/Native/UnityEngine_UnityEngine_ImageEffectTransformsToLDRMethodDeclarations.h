﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.ImageEffectTransformsToLDR
struct ImageEffectTransformsToLDR_t268;

// System.Void UnityEngine.ImageEffectTransformsToLDR::.ctor()
extern "C" void ImageEffectTransformsToLDR__ctor_m1044 (ImageEffectTransformsToLDR_t268 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
