﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets.Effects.AfterburnerPhysicsForce
struct AfterburnerPhysicsForce_t137;

// System.Void UnityStandardAssets.Effects.AfterburnerPhysicsForce::.ctor()
extern "C" void AfterburnerPhysicsForce__ctor_m394 (AfterburnerPhysicsForce_t137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Effects.AfterburnerPhysicsForce::OnEnable()
extern "C" void AfterburnerPhysicsForce_OnEnable_m395 (AfterburnerPhysicsForce_t137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Effects.AfterburnerPhysicsForce::FixedUpdate()
extern "C" void AfterburnerPhysicsForce_FixedUpdate_m396 (AfterburnerPhysicsForce_t137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Effects.AfterburnerPhysicsForce::OnDrawGizmosSelected()
extern "C" void AfterburnerPhysicsForce_OnDrawGizmosSelected_m397 (AfterburnerPhysicsForce_t137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
