﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.PlayerPrefsException
struct PlayerPrefsException_t868;
// System.String
struct String_t;

// System.Void UnityEngine.PlayerPrefsException::.ctor(System.String)
extern "C" void PlayerPrefsException__ctor_m4433 (PlayerPrefsException_t868 * __this, String_t* ___error, const MethodInfo* method) IL2CPP_METHOD_ATTR;
