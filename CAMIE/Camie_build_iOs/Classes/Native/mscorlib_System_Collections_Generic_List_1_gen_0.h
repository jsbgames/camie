﻿#pragma once
#include <stdint.h>
// UnityEngine.Rigidbody[]
struct RigidbodyU5BU5D_t2865;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<UnityEngine.Rigidbody>
struct  List_1_t142  : public Object_t
{
	// T[] System.Collections.Generic.List`1<UnityEngine.Rigidbody>::_items
	RigidbodyU5BU5D_t2865* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.Rigidbody>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.Rigidbody>::_version
	int32_t ____version_3;
};
struct List_1_t142_StaticFields{
	// T[] System.Collections.Generic.List`1<UnityEngine.Rigidbody>::EmptyArray
	RigidbodyU5BU5D_t2865* ___EmptyArray_4;
};
