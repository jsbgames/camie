﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t78;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GameObject>
struct  KeyValuePair_2_t419 
{
	// TKey System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GameObject>::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GameObject>::value
	GameObject_t78 * ___value_1;
};
