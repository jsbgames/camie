﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>>
struct KeyValuePair_2_t3330;
// System.Type
struct Type_t;
// System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>
struct IDictionary_2_t1038;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_5MethodDeclarations.h"
#define KeyValuePair_2__ctor_m20923(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3330 *, Type_t *, Object_t*, const MethodInfo*))KeyValuePair_2__ctor_m13651_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>>::get_Key()
#define KeyValuePair_2_get_Key_m20924(__this, method) (( Type_t * (*) (KeyValuePair_2_t3330 *, const MethodInfo*))KeyValuePair_2_get_Key_m13652_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m20925(__this, ___value, method) (( void (*) (KeyValuePair_2_t3330 *, Type_t *, const MethodInfo*))KeyValuePair_2_set_Key_m13653_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>>::get_Value()
#define KeyValuePair_2_get_Value_m20926(__this, method) (( Object_t* (*) (KeyValuePair_2_t3330 *, const MethodInfo*))KeyValuePair_2_get_Value_m13654_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m20927(__this, ___value, method) (( void (*) (KeyValuePair_2_t3330 *, Object_t*, const MethodInfo*))KeyValuePair_2_set_Value_m13655_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>>::ToString()
#define KeyValuePair_2_ToString_m20928(__this, method) (( String_t* (*) (KeyValuePair_2_t3330 *, const MethodInfo*))KeyValuePair_2_ToString_m13656_gshared)(__this, method)
