﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t1007;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t1009;
// UnityEngine.Events.UnityEventBase
struct UnityEventBase_t1010;

// System.Void UnityEngine.Events.PersistentCallGroup::.ctor()
extern "C" void PersistentCallGroup__ctor_m5057 (PersistentCallGroup_t1007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.PersistentCallGroup::Initialize(UnityEngine.Events.InvokableCallList,UnityEngine.Events.UnityEventBase)
extern "C" void PersistentCallGroup_Initialize_m5058 (PersistentCallGroup_t1007 * __this, InvokableCallList_t1009 * ___invokableList, UnityEventBase_t1010 * ___unityEventBase, const MethodInfo* method) IL2CPP_METHOD_ATTR;
