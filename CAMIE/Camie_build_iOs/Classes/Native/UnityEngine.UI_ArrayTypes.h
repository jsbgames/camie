﻿#pragma once
// System.Array
#include "mscorlib_System_Array.h"
// UnityEngine.EventSystems.IPointerDownHandler[]
// UnityEngine.EventSystems.IPointerDownHandler[]
struct  IPointerDownHandlerU5BU5D_t3906  : public Array_t
{
};
// UnityEngine.EventSystems.IEventSystemHandler[]
// UnityEngine.EventSystems.IEventSystemHandler[]
struct  IEventSystemHandlerU5BU5D_t3020  : public Array_t
{
};
// UnityEngine.EventSystems.IPointerUpHandler[]
// UnityEngine.EventSystems.IPointerUpHandler[]
struct  IPointerUpHandlerU5BU5D_t3907  : public Array_t
{
};
// UnityEngine.EventSystems.RaycastResult[]
// UnityEngine.EventSystems.RaycastResult[]
struct  RaycastResultU5BU5D_t2933  : public Array_t
{
};
// UnityEngine.UI.Text[]
// UnityEngine.UI.Text[]
struct  TextU5BU5D_t369  : public Array_t
{
};
struct TextU5BU5D_t369_StaticFields{
};
// UnityEngine.UI.ILayoutElement[]
// UnityEngine.UI.ILayoutElement[]
struct  ILayoutElementU5BU5D_t3908  : public Array_t
{
};
// UnityEngine.UI.MaskableGraphic[]
// UnityEngine.UI.MaskableGraphic[]
struct  MaskableGraphicU5BU5D_t3909  : public Array_t
{
};
// UnityEngine.UI.IMaskable[]
// UnityEngine.UI.IMaskable[]
struct  IMaskableU5BU5D_t3910  : public Array_t
{
};
// UnityEngine.UI.Graphic[]
// UnityEngine.UI.Graphic[]
struct  GraphicU5BU5D_t3115  : public Array_t
{
};
struct GraphicU5BU5D_t3115_StaticFields{
};
// UnityEngine.UI.ICanvasElement[]
// UnityEngine.UI.ICanvasElement[]
struct  ICanvasElementU5BU5D_t3081  : public Array_t
{
};
// UnityEngine.EventSystems.UIBehaviour[]
// UnityEngine.EventSystems.UIBehaviour[]
struct  UIBehaviourU5BU5D_t3911  : public Array_t
{
};
// UnityEngine.EventSystems.BaseInputModule[]
// UnityEngine.EventSystems.BaseInputModule[]
struct  BaseInputModuleU5BU5D_t3014  : public Array_t
{
};
// UnityEngine.EventSystems.BaseRaycaster[]
// UnityEngine.EventSystems.BaseRaycaster[]
struct  BaseRaycasterU5BU5D_t3036  : public Array_t
{
};
// UnityEngine.EventSystems.EventTrigger/Entry[]
// UnityEngine.EventSystems.EventTrigger/Entry[]
struct  EntryU5BU5D_t3042  : public Array_t
{
};
// UnityEngine.EventSystems.PointerEventData[]
// UnityEngine.EventSystems.PointerEventData[]
struct  PointerEventDataU5BU5D_t3056  : public Array_t
{
};
// UnityEngine.EventSystems.PointerInputModule/ButtonState[]
// UnityEngine.EventSystems.PointerInputModule/ButtonState[]
struct  ButtonStateU5BU5D_t3071  : public Array_t
{
};
// UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>[]
// UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>[]
struct  IndexedSet_1U5BU5D_t3119  : public Array_t
{
};
// UnityEngine.UI.InputField/ContentType[]
// UnityEngine.UI.InputField/ContentType[]
struct  ContentTypeU5BU5D_t648  : public Array_t
{
};
// UnityEngine.UI.Selectable[]
// UnityEngine.UI.Selectable[]
struct  SelectableU5BU5D_t3143  : public Array_t
{
};
struct SelectableU5BU5D_t3143_StaticFields{
};
// UnityEngine.UI.StencilMaterial/MatEntry[]
// UnityEngine.UI.StencilMaterial/MatEntry[]
struct  MatEntryU5BU5D_t3155  : public Array_t
{
};
// UnityEngine.UI.Toggle[]
// UnityEngine.UI.Toggle[]
struct  ToggleU5BU5D_t3165  : public Array_t
{
};
