﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>
struct Collection_1_t3236;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t217;
// UnityEngine.UICharInfo[]
struct UICharInfoU5BU5D_t1025;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UICharInfo>
struct IEnumerator_1_t3735;
// System.Collections.Generic.IList`1<UnityEngine.UICharInfo>
struct IList_1_t690;
// UnityEngine.UICharInfo
#include "UnityEngine_UnityEngine_UICharInfo.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::.ctor()
extern "C" void Collection_1__ctor_m19654_gshared (Collection_1_t3236 * __this, const MethodInfo* method);
#define Collection_1__ctor_m19654(__this, method) (( void (*) (Collection_1_t3236 *, const MethodInfo*))Collection_1__ctor_m19654_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19655_gshared (Collection_1_t3236 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19655(__this, method) (( bool (*) (Collection_1_t3236 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19655_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m19656_gshared (Collection_1_t3236 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m19656(__this, ___array, ___index, method) (( void (*) (Collection_1_t3236 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m19656_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m19657_gshared (Collection_1_t3236 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m19657(__this, method) (( Object_t * (*) (Collection_1_t3236 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m19657_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m19658_gshared (Collection_1_t3236 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m19658(__this, ___value, method) (( int32_t (*) (Collection_1_t3236 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m19658_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m19659_gshared (Collection_1_t3236 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m19659(__this, ___value, method) (( bool (*) (Collection_1_t3236 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m19659_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m19660_gshared (Collection_1_t3236 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m19660(__this, ___value, method) (( int32_t (*) (Collection_1_t3236 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m19660_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m19661_gshared (Collection_1_t3236 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m19661(__this, ___index, ___value, method) (( void (*) (Collection_1_t3236 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m19661_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m19662_gshared (Collection_1_t3236 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m19662(__this, ___value, method) (( void (*) (Collection_1_t3236 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m19662_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m19663_gshared (Collection_1_t3236 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m19663(__this, method) (( bool (*) (Collection_1_t3236 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m19663_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m19664_gshared (Collection_1_t3236 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m19664(__this, method) (( Object_t * (*) (Collection_1_t3236 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m19664_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m19665_gshared (Collection_1_t3236 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m19665(__this, method) (( bool (*) (Collection_1_t3236 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m19665_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m19666_gshared (Collection_1_t3236 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m19666(__this, method) (( bool (*) (Collection_1_t3236 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m19666_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m19667_gshared (Collection_1_t3236 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m19667(__this, ___index, method) (( Object_t * (*) (Collection_1_t3236 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m19667_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m19668_gshared (Collection_1_t3236 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m19668(__this, ___index, ___value, method) (( void (*) (Collection_1_t3236 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m19668_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::Add(T)
extern "C" void Collection_1_Add_m19669_gshared (Collection_1_t3236 * __this, UICharInfo_t689  ___item, const MethodInfo* method);
#define Collection_1_Add_m19669(__this, ___item, method) (( void (*) (Collection_1_t3236 *, UICharInfo_t689 , const MethodInfo*))Collection_1_Add_m19669_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::Clear()
extern "C" void Collection_1_Clear_m19670_gshared (Collection_1_t3236 * __this, const MethodInfo* method);
#define Collection_1_Clear_m19670(__this, method) (( void (*) (Collection_1_t3236 *, const MethodInfo*))Collection_1_Clear_m19670_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::ClearItems()
extern "C" void Collection_1_ClearItems_m19671_gshared (Collection_1_t3236 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m19671(__this, method) (( void (*) (Collection_1_t3236 *, const MethodInfo*))Collection_1_ClearItems_m19671_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::Contains(T)
extern "C" bool Collection_1_Contains_m19672_gshared (Collection_1_t3236 * __this, UICharInfo_t689  ___item, const MethodInfo* method);
#define Collection_1_Contains_m19672(__this, ___item, method) (( bool (*) (Collection_1_t3236 *, UICharInfo_t689 , const MethodInfo*))Collection_1_Contains_m19672_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m19673_gshared (Collection_1_t3236 * __this, UICharInfoU5BU5D_t1025* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m19673(__this, ___array, ___index, method) (( void (*) (Collection_1_t3236 *, UICharInfoU5BU5D_t1025*, int32_t, const MethodInfo*))Collection_1_CopyTo_m19673_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m19674_gshared (Collection_1_t3236 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m19674(__this, method) (( Object_t* (*) (Collection_1_t3236 *, const MethodInfo*))Collection_1_GetEnumerator_m19674_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m19675_gshared (Collection_1_t3236 * __this, UICharInfo_t689  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m19675(__this, ___item, method) (( int32_t (*) (Collection_1_t3236 *, UICharInfo_t689 , const MethodInfo*))Collection_1_IndexOf_m19675_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m19676_gshared (Collection_1_t3236 * __this, int32_t ___index, UICharInfo_t689  ___item, const MethodInfo* method);
#define Collection_1_Insert_m19676(__this, ___index, ___item, method) (( void (*) (Collection_1_t3236 *, int32_t, UICharInfo_t689 , const MethodInfo*))Collection_1_Insert_m19676_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m19677_gshared (Collection_1_t3236 * __this, int32_t ___index, UICharInfo_t689  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m19677(__this, ___index, ___item, method) (( void (*) (Collection_1_t3236 *, int32_t, UICharInfo_t689 , const MethodInfo*))Collection_1_InsertItem_m19677_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::Remove(T)
extern "C" bool Collection_1_Remove_m19678_gshared (Collection_1_t3236 * __this, UICharInfo_t689  ___item, const MethodInfo* method);
#define Collection_1_Remove_m19678(__this, ___item, method) (( bool (*) (Collection_1_t3236 *, UICharInfo_t689 , const MethodInfo*))Collection_1_Remove_m19678_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m19679_gshared (Collection_1_t3236 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m19679(__this, ___index, method) (( void (*) (Collection_1_t3236 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m19679_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m19680_gshared (Collection_1_t3236 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m19680(__this, ___index, method) (( void (*) (Collection_1_t3236 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m19680_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::get_Count()
extern "C" int32_t Collection_1_get_Count_m19681_gshared (Collection_1_t3236 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m19681(__this, method) (( int32_t (*) (Collection_1_t3236 *, const MethodInfo*))Collection_1_get_Count_m19681_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::get_Item(System.Int32)
extern "C" UICharInfo_t689  Collection_1_get_Item_m19682_gshared (Collection_1_t3236 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m19682(__this, ___index, method) (( UICharInfo_t689  (*) (Collection_1_t3236 *, int32_t, const MethodInfo*))Collection_1_get_Item_m19682_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m19683_gshared (Collection_1_t3236 * __this, int32_t ___index, UICharInfo_t689  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m19683(__this, ___index, ___value, method) (( void (*) (Collection_1_t3236 *, int32_t, UICharInfo_t689 , const MethodInfo*))Collection_1_set_Item_m19683_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m19684_gshared (Collection_1_t3236 * __this, int32_t ___index, UICharInfo_t689  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m19684(__this, ___index, ___item, method) (( void (*) (Collection_1_t3236 *, int32_t, UICharInfo_t689 , const MethodInfo*))Collection_1_SetItem_m19684_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m19685_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m19685(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m19685_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::ConvertItem(System.Object)
extern "C" UICharInfo_t689  Collection_1_ConvertItem_m19686_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m19686(__this /* static, unused */, ___item, method) (( UICharInfo_t689  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m19686_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m19687_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m19687(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m19687_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m19688_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m19688(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m19688_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m19689_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m19689(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m19689_gshared)(__this /* static, unused */, ___list, method)
