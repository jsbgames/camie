﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.ObjectPool`1<System.Object>
struct ObjectPool_1_t3028;
// UnityEngine.Events.UnityAction`1<System.Object>
struct UnityAction_1_t3027;
// System.Object
struct Object_t;

// System.Void UnityEngine.UI.ObjectPool`1<System.Object>::.ctor(UnityEngine.Events.UnityAction`1<T>,UnityEngine.Events.UnityAction`1<T>)
extern "C" void ObjectPool_1__ctor_m16456_gshared (ObjectPool_1_t3028 * __this, UnityAction_1_t3027 * ___actionOnGet, UnityAction_1_t3027 * ___actionOnRelease, const MethodInfo* method);
#define ObjectPool_1__ctor_m16456(__this, ___actionOnGet, ___actionOnRelease, method) (( void (*) (ObjectPool_1_t3028 *, UnityAction_1_t3027 *, UnityAction_1_t3027 *, const MethodInfo*))ObjectPool_1__ctor_m16456_gshared)(__this, ___actionOnGet, ___actionOnRelease, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Object>::get_countAll()
extern "C" int32_t ObjectPool_1_get_countAll_m16458_gshared (ObjectPool_1_t3028 * __this, const MethodInfo* method);
#define ObjectPool_1_get_countAll_m16458(__this, method) (( int32_t (*) (ObjectPool_1_t3028 *, const MethodInfo*))ObjectPool_1_get_countAll_m16458_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Object>::set_countAll(System.Int32)
extern "C" void ObjectPool_1_set_countAll_m16460_gshared (ObjectPool_1_t3028 * __this, int32_t ___value, const MethodInfo* method);
#define ObjectPool_1_set_countAll_m16460(__this, ___value, method) (( void (*) (ObjectPool_1_t3028 *, int32_t, const MethodInfo*))ObjectPool_1_set_countAll_m16460_gshared)(__this, ___value, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Object>::get_countActive()
extern "C" int32_t ObjectPool_1_get_countActive_m16462_gshared (ObjectPool_1_t3028 * __this, const MethodInfo* method);
#define ObjectPool_1_get_countActive_m16462(__this, method) (( int32_t (*) (ObjectPool_1_t3028 *, const MethodInfo*))ObjectPool_1_get_countActive_m16462_gshared)(__this, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Object>::get_countInactive()
extern "C" int32_t ObjectPool_1_get_countInactive_m16464_gshared (ObjectPool_1_t3028 * __this, const MethodInfo* method);
#define ObjectPool_1_get_countInactive_m16464(__this, method) (( int32_t (*) (ObjectPool_1_t3028 *, const MethodInfo*))ObjectPool_1_get_countInactive_m16464_gshared)(__this, method)
// T UnityEngine.UI.ObjectPool`1<System.Object>::Get()
extern "C" Object_t * ObjectPool_1_Get_m16466_gshared (ObjectPool_1_t3028 * __this, const MethodInfo* method);
#define ObjectPool_1_Get_m16466(__this, method) (( Object_t * (*) (ObjectPool_1_t3028 *, const MethodInfo*))ObjectPool_1_Get_m16466_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Object>::Release(T)
extern "C" void ObjectPool_1_Release_m16468_gshared (ObjectPool_1_t3028 * __this, Object_t * ___element, const MethodInfo* method);
#define ObjectPool_1_Release_m16468(__this, ___element, method) (( void (*) (ObjectPool_1_t3028 *, Object_t *, const MethodInfo*))ObjectPool_1_Release_m16468_gshared)(__this, ___element, method)
