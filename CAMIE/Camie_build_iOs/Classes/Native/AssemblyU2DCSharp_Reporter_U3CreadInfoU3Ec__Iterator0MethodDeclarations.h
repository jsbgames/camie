﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Reporter/<readInfo>c__Iterator0
struct U3CreadInfoU3Ec__Iterator0_t296;
// System.Object
struct Object_t;

// System.Void Reporter/<readInfo>c__Iterator0::.ctor()
extern "C" void U3CreadInfoU3Ec__Iterator0__ctor_m1085 (U3CreadInfoU3Ec__Iterator0_t296 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Reporter/<readInfo>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CreadInfoU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1086 (U3CreadInfoU3Ec__Iterator0_t296 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Reporter/<readInfo>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CreadInfoU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1087 (U3CreadInfoU3Ec__Iterator0_t296 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Reporter/<readInfo>c__Iterator0::MoveNext()
extern "C" bool U3CreadInfoU3Ec__Iterator0_MoveNext_m1088 (U3CreadInfoU3Ec__Iterator0_t296 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Reporter/<readInfo>c__Iterator0::Dispose()
extern "C" void U3CreadInfoU3Ec__Iterator0_Dispose_m1089 (U3CreadInfoU3Ec__Iterator0_t296 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Reporter/<readInfo>c__Iterator0::Reset()
extern "C" void U3CreadInfoU3Ec__Iterator0_Reset_m1090 (U3CreadInfoU3Ec__Iterator0_t296 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
