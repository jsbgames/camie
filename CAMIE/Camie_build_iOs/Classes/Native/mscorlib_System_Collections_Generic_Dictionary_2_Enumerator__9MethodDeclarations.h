﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>
struct Enumerator_t2957;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Int32>
struct Dictionary_2_t2952;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_11.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m15593_gshared (Enumerator_t2957 * __this, Dictionary_2_t2952 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m15593(__this, ___dictionary, method) (( void (*) (Enumerator_t2957 *, Dictionary_2_t2952 *, const MethodInfo*))Enumerator__ctor_m15593_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m15594_gshared (Enumerator_t2957 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m15594(__this, method) (( Object_t * (*) (Enumerator_t2957 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15594_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t1430  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15595_gshared (Enumerator_t2957 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15595(__this, method) (( DictionaryEntry_t1430  (*) (Enumerator_t2957 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15595_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15596_gshared (Enumerator_t2957 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15596(__this, method) (( Object_t * (*) (Enumerator_t2957 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15596_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15597_gshared (Enumerator_t2957 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15597(__this, method) (( Object_t * (*) (Enumerator_t2957 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15597_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::MoveNext()
extern "C" bool Enumerator_MoveNext_m15598_gshared (Enumerator_t2957 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m15598(__this, method) (( bool (*) (Enumerator_t2957 *, const MethodInfo*))Enumerator_MoveNext_m15598_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::get_Current()
extern "C" KeyValuePair_2_t2953  Enumerator_get_Current_m15599_gshared (Enumerator_t2957 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m15599(__this, method) (( KeyValuePair_2_t2953  (*) (Enumerator_t2957 *, const MethodInfo*))Enumerator_get_Current_m15599_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::get_CurrentKey()
extern "C" Object_t * Enumerator_get_CurrentKey_m15600_gshared (Enumerator_t2957 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m15600(__this, method) (( Object_t * (*) (Enumerator_t2957 *, const MethodInfo*))Enumerator_get_CurrentKey_m15600_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::get_CurrentValue()
extern "C" int32_t Enumerator_get_CurrentValue_m15601_gshared (Enumerator_t2957 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m15601(__this, method) (( int32_t (*) (Enumerator_t2957 *, const MethodInfo*))Enumerator_get_CurrentValue_m15601_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::VerifyState()
extern "C" void Enumerator_VerifyState_m15602_gshared (Enumerator_t2957 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m15602(__this, method) (( void (*) (Enumerator_t2957 *, const MethodInfo*))Enumerator_VerifyState_m15602_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m15603_gshared (Enumerator_t2957 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m15603(__this, method) (( void (*) (Enumerator_t2957 *, const MethodInfo*))Enumerator_VerifyCurrent_m15603_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::Dispose()
extern "C" void Enumerator_Dispose_m15604_gshared (Enumerator_t2957 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m15604(__this, method) (( void (*) (Enumerator_t2957 *, const MethodInfo*))Enumerator_Dispose_m15604_gshared)(__this, method)
