﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.AudioSource>
struct KeyValuePair_2_t3010;
// System.String
struct String_t;
// UnityEngine.AudioSource
struct AudioSource_t247;

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.AudioSource>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_5MethodDeclarations.h"
#define KeyValuePair_2__ctor_m16209(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3010 *, String_t*, AudioSource_t247 *, const MethodInfo*))KeyValuePair_2__ctor_m13651_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.AudioSource>::get_Key()
#define KeyValuePair_2_get_Key_m16210(__this, method) (( String_t* (*) (KeyValuePair_2_t3010 *, const MethodInfo*))KeyValuePair_2_get_Key_m13652_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.AudioSource>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m16211(__this, ___value, method) (( void (*) (KeyValuePair_2_t3010 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m13653_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.AudioSource>::get_Value()
#define KeyValuePair_2_get_Value_m16212(__this, method) (( AudioSource_t247 * (*) (KeyValuePair_2_t3010 *, const MethodInfo*))KeyValuePair_2_get_Value_m13654_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.AudioSource>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m16213(__this, ___value, method) (( void (*) (KeyValuePair_2_t3010 *, AudioSource_t247 *, const MethodInfo*))KeyValuePair_2_set_Value_m13655_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.AudioSource>::ToString()
#define KeyValuePair_2_ToString_m16214(__this, method) (( String_t* (*) (KeyValuePair_2_t3010 *, const MethodInfo*))KeyValuePair_2_ToString_m13656_gshared)(__this, method)
