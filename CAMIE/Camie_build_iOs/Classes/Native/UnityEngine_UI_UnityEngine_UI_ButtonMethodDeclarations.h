﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.Button
struct Button_t324;
// UnityEngine.UI.Button/ButtonClickedEvent
struct ButtonClickedEvent_t415;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t215;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t453;
// System.Collections.IEnumerator
struct IEnumerator_t217;

// System.Void UnityEngine.UI.Button::.ctor()
extern "C" void Button__ctor_m2175 (Button_t324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Button/ButtonClickedEvent UnityEngine.UI.Button::get_onClick()
extern "C" ButtonClickedEvent_t415 * Button_get_onClick_m1724 (Button_t324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Button::set_onClick(UnityEngine.UI.Button/ButtonClickedEvent)
extern "C" void Button_set_onClick_m2176 (Button_t324 * __this, ButtonClickedEvent_t415 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Button::Press()
extern "C" void Button_Press_m2177 (Button_t324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Button::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern "C" void Button_OnPointerClick_m2178 (Button_t324 * __this, PointerEventData_t215 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Button::OnSubmit(UnityEngine.EventSystems.BaseEventData)
extern "C" void Button_OnSubmit_m2179 (Button_t324 * __this, BaseEventData_t453 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UnityEngine.UI.Button::OnFinishSubmit()
extern "C" Object_t * Button_OnFinishSubmit_m2180 (Button_t324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
