﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<Reporter/Sample>
struct IList_1_t2895;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.ReadOnlyCollection`1<Reporter/Sample>
struct  ReadOnlyCollection_1_t2896  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Reporter/Sample>::list
	Object_t* ___list_0;
};
