﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<System.String>
struct List_1_t52;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t3530;
// System.Collections.Generic.IEnumerator`1<System.String>
struct IEnumerator_1_t3519;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t217;
// System.Collections.Generic.ICollection`1<System.String>
struct ICollection_1_t1032;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.String>
struct ReadOnlyCollection_1_t2854;
// System.String[]
struct StringU5BU5D_t243;
// System.Predicate`1<System.String>
struct Predicate_1_t2855;
// System.Comparison`1<System.String>
struct Comparison_1_t2857;
// System.Collections.Generic.List`1/Enumerator<System.String>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_5.h"

// System.Void System.Collections.Generic.List`1<System.String>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_25MethodDeclarations.h"
#define List_1__ctor_m733(__this, method) (( void (*) (List_1_t52 *, const MethodInfo*))List_1__ctor_m5170_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.String>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m13912(__this, ___collection, method) (( void (*) (List_1_t52 *, Object_t*, const MethodInfo*))List_1__ctor_m13913_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.String>::.ctor(System.Int32)
#define List_1__ctor_m13914(__this, ___capacity, method) (( void (*) (List_1_t52 *, int32_t, const MethodInfo*))List_1__ctor_m13915_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<System.String>::.cctor()
#define List_1__cctor_m13916(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m13917_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.String>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m13918(__this, method) (( Object_t* (*) (List_1_t52 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m5425_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.String>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m13919(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t52 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m5408_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.String>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m13920(__this, method) (( Object_t * (*) (List_1_t52 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m5404_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.String>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m13921(__this, ___item, method) (( int32_t (*) (List_1_t52 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m5413_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.String>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m13922(__this, ___item, method) (( bool (*) (List_1_t52 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m5415_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.String>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m13923(__this, ___item, method) (( int32_t (*) (List_1_t52 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m5416_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.String>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m13924(__this, ___index, ___item, method) (( void (*) (List_1_t52 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m5417_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.String>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m13925(__this, ___item, method) (( void (*) (List_1_t52 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m5418_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.String>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m13926(__this, method) (( bool (*) (List_1_t52 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m5420_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.String>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m13927(__this, method) (( bool (*) (List_1_t52 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m5406_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.String>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m13928(__this, method) (( Object_t * (*) (List_1_t52 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m5407_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.String>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m13929(__this, method) (( bool (*) (List_1_t52 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m5409_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.String>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m13930(__this, method) (( bool (*) (List_1_t52 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m5410_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.String>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m13931(__this, ___index, method) (( Object_t * (*) (List_1_t52 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m5411_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.String>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m13932(__this, ___index, ___value, method) (( void (*) (List_1_t52 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m5412_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<System.String>::Add(T)
#define List_1_Add_m13933(__this, ___item, method) (( void (*) (List_1_t52 *, String_t*, const MethodInfo*))List_1_Add_m5421_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.String>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m13934(__this, ___newCount, method) (( void (*) (List_1_t52 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m13935_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<System.String>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m13936(__this, ___collection, method) (( void (*) (List_1_t52 *, Object_t*, const MethodInfo*))List_1_AddCollection_m13937_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.String>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m13938(__this, ___enumerable, method) (( void (*) (List_1_t52 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m13939_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<System.String>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m13940(__this, ___collection, method) (( void (*) (List_1_t52 *, Object_t*, const MethodInfo*))List_1_AddRange_m13941_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.String>::AsReadOnly()
#define List_1_AsReadOnly_m13942(__this, method) (( ReadOnlyCollection_1_t2854 * (*) (List_1_t52 *, const MethodInfo*))List_1_AsReadOnly_m13943_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.String>::Clear()
#define List_1_Clear_m13944(__this, method) (( void (*) (List_1_t52 *, const MethodInfo*))List_1_Clear_m5414_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.String>::Contains(T)
#define List_1_Contains_m13945(__this, ___item, method) (( bool (*) (List_1_t52 *, String_t*, const MethodInfo*))List_1_Contains_m5422_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.String>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m13946(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t52 *, StringU5BU5D_t243*, int32_t, const MethodInfo*))List_1_CopyTo_m5423_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<System.String>::Find(System.Predicate`1<T>)
#define List_1_Find_m13947(__this, ___match, method) (( String_t* (*) (List_1_t52 *, Predicate_1_t2855 *, const MethodInfo*))List_1_Find_m13948_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.String>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m13949(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2855 *, const MethodInfo*))List_1_CheckMatch_m13950_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.String>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m13951(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t52 *, int32_t, int32_t, Predicate_1_t2855 *, const MethodInfo*))List_1_GetIndex_m13952_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.String>::GetEnumerator()
#define List_1_GetEnumerator_m13953(__this, method) (( Enumerator_t2856  (*) (List_1_t52 *, const MethodInfo*))List_1_GetEnumerator_m13954_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.String>::IndexOf(T)
#define List_1_IndexOf_m13955(__this, ___item, method) (( int32_t (*) (List_1_t52 *, String_t*, const MethodInfo*))List_1_IndexOf_m5426_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.String>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m13956(__this, ___start, ___delta, method) (( void (*) (List_1_t52 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m13957_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<System.String>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m13958(__this, ___index, method) (( void (*) (List_1_t52 *, int32_t, const MethodInfo*))List_1_CheckIndex_m13959_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.String>::Insert(System.Int32,T)
#define List_1_Insert_m13960(__this, ___index, ___item, method) (( void (*) (List_1_t52 *, int32_t, String_t*, const MethodInfo*))List_1_Insert_m5427_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.String>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m13961(__this, ___collection, method) (( void (*) (List_1_t52 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m13962_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<System.String>::Remove(T)
#define List_1_Remove_m13963(__this, ___item, method) (( bool (*) (List_1_t52 *, String_t*, const MethodInfo*))List_1_Remove_m5424_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.String>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m13964(__this, ___match, method) (( int32_t (*) (List_1_t52 *, Predicate_1_t2855 *, const MethodInfo*))List_1_RemoveAll_m13965_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.String>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m13966(__this, ___index, method) (( void (*) (List_1_t52 *, int32_t, const MethodInfo*))List_1_RemoveAt_m5419_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.String>::Reverse()
#define List_1_Reverse_m13967(__this, method) (( void (*) (List_1_t52 *, const MethodInfo*))List_1_Reverse_m13968_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.String>::Sort()
#define List_1_Sort_m13969(__this, method) (( void (*) (List_1_t52 *, const MethodInfo*))List_1_Sort_m13970_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.String>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m13971(__this, ___comparison, method) (( void (*) (List_1_t52 *, Comparison_1_t2857 *, const MethodInfo*))List_1_Sort_m13972_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<System.String>::ToArray()
#define List_1_ToArray_m12632(__this, method) (( StringU5BU5D_t243* (*) (List_1_t52 *, const MethodInfo*))List_1_ToArray_m13973_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.String>::TrimExcess()
#define List_1_TrimExcess_m13974(__this, method) (( void (*) (List_1_t52 *, const MethodInfo*))List_1_TrimExcess_m13975_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.String>::get_Capacity()
#define List_1_get_Capacity_m13976(__this, method) (( int32_t (*) (List_1_t52 *, const MethodInfo*))List_1_get_Capacity_m13977_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.String>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m13978(__this, ___value, method) (( void (*) (List_1_t52 *, int32_t, const MethodInfo*))List_1_set_Capacity_m13979_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<System.String>::get_Count()
#define List_1_get_Count_m13980(__this, method) (( int32_t (*) (List_1_t52 *, const MethodInfo*))List_1_get_Count_m5405_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.String>::get_Item(System.Int32)
#define List_1_get_Item_m13981(__this, ___index, method) (( String_t* (*) (List_1_t52 *, int32_t, const MethodInfo*))List_1_get_Item_m5428_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.String>::set_Item(System.Int32,T)
#define List_1_set_Item_m13982(__this, ___index, ___value, method) (( void (*) (List_1_t52 *, int32_t, String_t*, const MethodInfo*))List_1_set_Item_m5429_gshared)(__this, ___index, ___value, method)
