﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystem>
struct InternalEnumerator_1_t2932;
// System.Object
struct Object_t;
// UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystem
struct DemoParticleSystem_t321;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystem>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m15203(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2932 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m13513_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystem>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15204(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2932 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13515_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystem>::Dispose()
#define InternalEnumerator_1_Dispose_m15205(__this, method) (( void (*) (InternalEnumerator_1_t2932 *, const MethodInfo*))InternalEnumerator_1_Dispose_m13517_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystem>::MoveNext()
#define InternalEnumerator_1_MoveNext_m15206(__this, method) (( bool (*) (InternalEnumerator_1_t2932 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m13519_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystem>::get_Current()
#define InternalEnumerator_1_get_Current_m15207(__this, method) (( DemoParticleSystem_t321 * (*) (InternalEnumerator_1_t2932 *, const MethodInfo*))InternalEnumerator_1_get_Current_m13521_gshared)(__this, method)
