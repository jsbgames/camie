﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets.Utility.ObjectResetter
struct ObjectResetter_t149;
// System.Collections.IEnumerator
struct IEnumerator_t217;

// System.Void UnityStandardAssets.Utility.ObjectResetter::.ctor()
extern "C" void ObjectResetter__ctor_m513 (ObjectResetter_t149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Utility.ObjectResetter::Start()
extern "C" void ObjectResetter_Start_m514 (ObjectResetter_t149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Utility.ObjectResetter::DelayedReset(System.Single)
extern "C" void ObjectResetter_DelayedReset_m515 (ObjectResetter_t149 * __this, float ___delay, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UnityStandardAssets.Utility.ObjectResetter::ResetCoroutine(System.Single)
extern "C" Object_t * ObjectResetter_ResetCoroutine_m516 (ObjectResetter_t149 * __this, float ___delay, const MethodInfo* method) IL2CPP_METHOD_ATTR;
