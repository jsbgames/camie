﻿using UnityEngine;
using System.Collections;

public class SCR_CamieDirections : MonoBehaviour {

	SCR_Camie avatar;
	Quaternion[] angles;
	Vector3[] normals;

	void Start(){
		avatar = SCR_ManagerComponents.ManagerInstance.AvatarMove;
		avatar.Director = this;

		Transform[] children = ReorderByName(GetComponentsInChildren<Transform>());

		angles = new Quaternion[24];
		normals = new Vector3[24];
		int index = 0;

		for (int i = 0; i < children.Length-1; i++){
			if (children[i] != this.transform){
				angles[index] = children[i].rotation;
				normals[index] = (children[i].position - transform.position).normalized;
				index ++;
			}
		}
	}

	void Update () {
		this.transform.position = avatar.ColliderCenter;
	}

	Transform[] ReorderByName (Transform[] array){
		Transform[] reordered = new Transform[array.Length];

		for (int i = 0; i < array.Length; i ++){
			int index = array.Length-1;
			if (int.TryParse (array[i].name, out index)){
				reordered[index] = array[i];
			}
			else{
				reordered[index] = array[i];
			}
		}
		return reordered;
	}

	public Quaternion[] Angles{
		get {return angles;}
	}
	public Vector3[] Normals{
		get {return normals;}
	}
}
