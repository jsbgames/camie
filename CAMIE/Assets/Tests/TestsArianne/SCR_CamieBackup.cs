﻿//using UnityEngine;
//using System.Collections;
//
//public class SCR_Camie : MonoBehaviour { 
//	
//	public float moveSpeed = 5f;
//	public float lerpSpeed = 10f;
//	public float gravity = 10f;
//	
//	bool isGrounded = false;
//	public float deltaGround = 0.2f;
//	public float jumpSpeed = 10f;
//	
//	Vector3 surfaceNormal;
//	Vector3 myNormal;
//	Vector3 myForward;
//	bool onCorner;
//	
//	bool goingRight = true;
//	float speedModifier = 1f;
//	
//	public float maxFlyTime;
//	float currFlyTime;
//	bool isFlyTimeCounting;
//	bool canFly;
//	bool isJumping = false;
//	IEnumerator jumpRoutine;
//	SCR_Wings[] wings;
//	
//	public LayerMask walkable;
//	Rigidbody rb;
//	Collider myColl;
//	Animator anim;
//	SCR_Respawn respawner;
//	
//	bool isKnockbacked;
//	
//	Transform trans;
//	
//	public SphereCollider frontCorner;
//	public SphereCollider bottomCorner;
//	public SphereCollider ground;
//	float groundRadius;
//	float cornerRadius;
//	Transform groundPosition;
//	Transform frontPosiiton;
//	Transform bottomPosition;
//	
//	void Start(){
//		rb = this.GetComponent<Rigidbody>();
//		myColl = this.GetComponent<Collider>();
//		anim = this.GetComponentInChildren<Animator>();
//		respawner = this.GetComponent<SCR_Respawn>();
//		
//		trans = transform;
//		wings = trans.GetComponentsInChildren<SCR_Wings>();
//		ToggleWings (false);
//		jumpRoutine = Jump (myNormal);
//		myNormal = trans.up;
//		rb.freezeRotation = true;
//		
//		groundPosition = ground.transform;
//		frontPosiiton = frontCorner.transform;
//		bottomPosition = bottomCorner.transform;
//		groundRadius = ground.radius;
//		cornerRadius = frontCorner.radius;
//		ground.enabled = false;
//		frontCorner.enabled = false;
//		bottomCorner.enabled = false;
//	}
//	void FixedUpdate(){}
//	void Update(){
//		if (respawner.IsRespawning){
//			rb.AddForce (Vector3.down);
//			return;
//		}
//		else if (trans.position.y < SCR_ManagerComponents.ManagerInstance.BottomFrame.position.y){
//			ToggleWings (false);
//			Respawn ();
//			return;
//		}
//		
//		Ray ray;
//		RaycastHit hit;
//		
//		if (((SCR_ManagerComponents.ManagerInstance.FlyButton != null && SCR_ManagerComponents.ManagerInstance.FlyButton.IsPressed) 
//		     || Input.GetKeyDown (KeyCode.F)) //test in-editor 
//		    && !isJumping && canFly && isGrounded){
//			
//			anim.SetBool ("isWalking", false);
//			anim.SetBool ("isIdle", false);
//			anim.SetBool ("isFlying", true);
//			CancelInvoke ("BreakIdle");
//			
//			ToggleWings (true);
//			
//			isGrounded = false;
//			isJumping = true;
//			jumpRoutine = Jump (myNormal);
//			StartCoroutine (jumpRoutine);
//			return;
//		}
//		else if (isJumping && ((Application.isEditor && Input.GetKeyUp(KeyCode.F)) || (!Application.isEditor && !SCR_ManagerComponents.ManagerInstance.FlyButton.IsPressed))){ //lache le bouton
//			StopCoroutine (jumpRoutine); 
//			ToggleWings (false);
//			anim.SetBool ("isFlying", false);
//			isJumping = false;
//			isFlyTimeCounting = false;
//		}
//		else if (isJumping && isFlyTimeCounting){ //vol normal
//			if (currFlyTime < maxFlyTime){ //peut voler
//				if (SCR_ManagerComponents.ManagerInstance.Joystick.IsDragging){
//					
//					Vector3 input = new Vector3 (SCR_ManagerComponents.ManagerInstance.Joystick.HorizontalAxis, SCR_ManagerComponents.ManagerInstance.Joystick.VerticalAxis, 0f);
//					//ray = new Ray (myColl.bounds.center, myColl.bounds.center + input);
//					
//					ray = new Ray (myColl.bounds.center, myColl.bounds.center + input.normalized * 5f);
//					if (Physics.Raycast (ray, out hit, 3f)){
//						Debug.Log (trans.name);
//						anim.SetBool ("isFlying", false);
//						surfaceNormal = hit.normal;
//					}
//					else{
//						trans.Translate (input * Time.deltaTime * speedModifier * moveSpeed);
//					}
//				}
//				currFlyTime += Time.deltaTime;
//				return;
//			}
//			else{
//				anim.SetBool ("isFlying", false);
//				currFlyTime = 0f;
//				isJumping = false;
//				canFly = false;
//			}
//		}
//		else if (isJumping) return; //pendant l'atterrissage.
//		else {
//			rb.AddForce (-gravity * rb.mass * myNormal * speedModifier * Time.deltaTime);
//		}
//		
//		Ray fwdRay = new Ray (myColl.bounds.center, myColl.bounds.center + trans.right);
//		ray = new Ray (trans.position, -myNormal);
//		
//		//onCorner = Physics.Raycast (ray, 1f) && Physics.Raycast (fwdRay, 1f);
//		//Debug.Log (onCorner);
//		
//		
//		isGrounded = Physics.CheckSphere (groundPosition.position, groundRadius);
//		
//		if (!isGrounded){
//			return;
//		}
//		else{
//			RaycastHit[] hits = Physics.SphereCastAll (myColl.bounds.center, cornerRadius, frontPosiiton.position);
//			
//			if (hits.Length > 0){
//				Debug.Log (hits[0].transform.parent.name);
//			}
//			else{
//				hits = Physics.SphereCastAll (myColl.bounds.center, cornerRadius, bottomPosition.position);
//				Debug.Log (hits.Length);
//				if (hits.Length > 0){
//					Debug.Log ("bot");
//				}
//			}
//		}
//		
//		//		if (Physics.Raycast(fwdRay, out hit, 1f) && hit.transform.gameObject.tag == "Navigable"){
//		//			//isGrounded = hit.distance <= Mathf.Sign(distGround) * distGround * deltaGround;
//		//			surfaceNormal = hit.normal;
//		//			Debug.Log (hit.transform.parent.name);
//		//		}
//		//		else if (!onCorner && Physics.Raycast (ray, out hit)){
//		//			//isGrounded = hit.distance <=  deltaGround;
//		//			surfaceNormal = hit.normal;
//		//		}
//		//		else if (!onCorner){ 
//		//			isGrounded = false;
//		//			surfaceNormal = Vector3.up;
//		//			myNormal = Vector3.up;
//		//		}
//		//
//		//		if (isGrounded){ 
//		//			canFly = true;
//		//			isJumping = false;
//		//		}
//		//
//		//		Vector3 right = trans.right;
//		//		if (goingRight){
//		//			if (right.z > 0){
//		//				right = -right;
//		//			}
//		//		}
//		//		else if (right.z < 0){
//		//			right = - right;
//		//		}
//		//
//		//		myNormal = Vector3.Lerp (myNormal, surfaceNormal, lerpSpeed * 4 * Time.deltaTime);
//		//		myForward = Vector3.Cross (right, myNormal); 
//		//
//		//		Quaternion targetRot = Quaternion.LookRotation (myForward, new Vector3 (myNormal.x, myNormal.y, 0f));
//		//		trans.rotation = targetRot;
//		
//		if (SCR_ManagerComponents.ManagerInstance.Joystick != null && SCR_ManagerComponents.ManagerInstance.Joystick.IsDragging){
//			trans.Translate (SCR_ManagerComponents.ManagerInstance.Joystick.HorizontalAxis * Mathf.Sign (SCR_ManagerComponents.ManagerInstance.Joystick.HorizontalAxis) * moveSpeed * speedModifier * Time.deltaTime, 0f, 0f);
//			anim.SetBool ("isWalking", true);
//			anim.SetBool ("isIdle", false);
//			CancelInvoke ("BreakIdle");
//		}
//		else{
//			anim.SetBool ("isWalking", false);
//			anim.SetBool ("isIdle", true);
//		}
//	}
//	
//	IEnumerator Jump (Vector3 normal){
//		float t = 0f;
//		rb.velocity += myNormal * jumpSpeed;
//		while (t < 0.5){
//			t += Time.deltaTime * speedModifier;
//			yield return new WaitForEndOfFrame();
//		}
//		rb.velocity -= myNormal * jumpSpeed;
//		currFlyTime = 0f;
//		isFlyTimeCounting = true;
//	}
//	
//	public IEnumerator Knockback (Vector3 target, Quaternion rotation){
//		isKnockbacked = true;
//		float time = 0f;
//		
//		float halfDistance = Vector3.Distance (trans.position, target)/2; 
//		float height = target.y + halfDistance; 
//		
//		while (isKnockbacked){
//			
//			if (Vector3.Distance (trans.position, target) > halfDistance){
//				Vector3 heightVector = new Vector3 (target.x, height, 0f);
//				trans.position = Vector3.Slerp (trans.position, heightVector, time/moveSpeed * speedModifier);
//			}
//			else{
//				trans.position = Vector3.Slerp (trans.position, target, time/moveSpeed * speedModifier);
//			}
//			
//			trans.rotation = Quaternion.Lerp (trans.rotation, rotation, time/lerpSpeed); 
//			
//			time += Time.deltaTime;
//			
//			if (Vector3.Distance (trans.position, target) < 0.05f){
//				trans.position = target; 
//				trans.rotation = rotation; 
//				
//				isKnockbacked = false;
//			}
//			yield return new WaitForEndOfFrame();
//		}
//	}
//	
//	public void ChangeDirection(bool goRight){
//		goingRight = goRight;
//	}
//	
//	public void DecreaseSpeed(float toPercent){
//		speedModifier = speedModifier/toPercent;
//	}
//	public void NormalSpeed(){
//		speedModifier = 1f;
//	}
//	
//	public void Respawn(){
//		anim.SetBool ("isFlying", false);
//		anim.SetBool ("isWalking", false);
//		respawner.StartRespawn();
//		canFly = false;
//	}
//	
//	void BreakIdle(){
//		if (anim.GetBool ("breakIdle1") || anim.GetBool ("breakIdle2")){
//			anim.SetBool ("breakIdle1", false);
//			anim.SetBool ("breakIdle2", false);
//		}
//		else{
//			int random = Random.Range (0,3);
//			
//			switch (random){
//			case 0 : 
//				anim.SetBool ("breakIdle1", true);
//				break;
//			case 1: 
//				anim.SetBool ("breakIdle2", true);
//				break;
//			case 2:
//				anim.SetBool ("breakIdle3", false);
//				break;
//			}
//		}
//	}
//	
//	public void ResetIdle(){
//		anim.SetBool ("breakIdle1", false);
//		anim.SetBool ("breakIdle2", false);
//		anim.SetBool ("breakIdle3", false);
//		
//		if (!IsInvoking("BreakIdle")){
//			Invoke ("BreakIdle", 1.5f);
//		}
//	}
//	
//	public void ToggleWings(bool show){
//		foreach (SCR_Wings wing in wings){
//			wing.SetVisible (show);
//		}
//	}
//	
//	public bool IsGrounded{
//		get {return isGrounded;}
//	}
//	public bool CanFly{
//		get {return canFly;}
//		set {canFly = value;}
//	}
//	public Animator MyAnimations{
//		get {return anim;}
//	}
//}
