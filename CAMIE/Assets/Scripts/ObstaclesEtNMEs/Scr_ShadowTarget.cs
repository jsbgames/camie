﻿using UnityEngine;
using System.Collections;

public class Scr_ShadowTarget : MonoBehaviour {

    public Vector3 camieOffset = new Vector3(0f, 0f, -4.0f);

    public bool canMove;
    public bool canGrow;

    Transform camieTransform;

    public SCR_Bird attackCount;

    public Projector projector;
    public float growSpeed = 0.5f;
    public float maxSize;
    // Use this for initialization
    void Start ()
    {
		transform.SetParent (null);
        projector.fieldOfView = 0f;
        camieTransform = SCR_ManagerComponents.ManagerInstance.AvatarMove.transform;
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (canMove)
        {
            this.transform.position = camieTransform.position + camieOffset;
        }

        if (canGrow && attackCount.attacknb != 3)
        {
            if (growSpeed < maxSize)
            {
                projector.fieldOfView += growSpeed * Time.deltaTime;
            }
        }
        else
        {
            projector.fieldOfView = 0f;
        }
    }
}
