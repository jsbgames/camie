﻿using UnityEngine;
using System.Collections;

public class SCR_FallOutOfScreen : MonoBehaviour {

	void OnBecameVisible(){
		this.GetComponent<Renderer>().enabled = false;
	}

	void OnTriggerEnter (Collider other){
		if (other.tag == "Player"){
			Invoke ("CallAvatarRespawn", 0.5f);
		}
	}

	void CallAvatarRespawn(){
		SCR_ManagerComponents.ManagerInstance.AvatarMove.Respawn();
	}
}
