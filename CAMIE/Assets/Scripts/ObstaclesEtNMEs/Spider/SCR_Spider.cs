﻿using UnityEngine;
using System.Collections;

public class SCR_Spider : MonoBehaviour {

	Vector3 startPosition; 
	LineRenderer line;

	bool isActive;

	ParticleSystem leaves;
	float totalLeafPlayTime = 2f;
	float currentLeafPlayTime = 0f;
	bool dropLeaves;

	void Start(){
		SCR_ManagerComponents.ManagerInstance.Spider = this;
		startPosition = this.transform.position;

		leaves = this.GetComponentInChildren<ParticleSystem>();
		leaves.Stop();
		leaves.transform.parent = null;
		dropLeaves = false;

		line = this.GetComponent<LineRenderer>();
		line.SetVertexCount (2);
		line.SetWidth (0.1f, 0.1f);

		Vector3 lineStart = startPosition;
		lineStart.z += 1f;
		line.SetPosition (0, lineStart);

		line.enabled = false;
	}

	void FixedUpdate(){
		if (dropLeaves){
			DropLeaves();
		}
	}

	void OnTriggerEnter (Collider other){
		if (other.tag == "Frame" && !IsInvoking("Reset")){
			line.enabled = false;
			Invoke ("Reset", 2f);
		}
	}

	public void Activate(){
		if (!isActive){
			isActive = true;
			dropLeaves = true;
			line.enabled = true;

			SCR_ManagerComponents.SoundManager.PlaySound ("SND_SpiderComing");
		}
	}

	void Reset(){
		isActive = false;
		this.transform.position = startPosition;
		dropLeaves = false;
		currentLeafPlayTime = 0f;
	}

	void DropLeaves(){
		if (leaves.isStopped){
			leaves.Play();

			Vector3 setPosition = new Vector3 (SCR_ManagerComponents.ManagerInstance.AvatarMove.transform.position.x,
			                                   Camera.main.ScreenToWorldPoint(Vector3.zero).y + 10f,
			                                   10f);
			leaves.transform.position = setPosition;
		}
		else{
			currentLeafPlayTime += Time.deltaTime;

			if (currentLeafPlayTime < totalLeafPlayTime){
				currentLeafPlayTime += Time.deltaTime;
			}
			else{
				dropLeaves = false;
				leaves.Stop ();
				StartCoroutine ("ChaseAvatar");
			}
		}
	}

	void DrawWebString(){
		Vector3 lineEnd = this.transform.position;
		lineEnd.z += 1f;
		line.SetPosition (1, lineEnd);
	}

	IEnumerator ChaseAvatar(){
		float speed = 4f;

		while (isActive){
			this.transform.parent.position = Vector3.Lerp (this.transform.parent.position, 
			                                               this.transform.parent.position + Vector3.down, speed * Time.deltaTime);

			if (this.transform.position.y > SCR_ManagerComponents.ManagerInstance.AvatarMove.transform.position.y){
				Vector3 targetX = new Vector3 (SCR_ManagerComponents.ManagerInstance.AvatarMove.transform.position.x,
				                               this.transform.position.y, this.transform.position.z);

				this.transform.position = Vector3.Slerp (this.transform.position, targetX, speed/8 * Time.deltaTime);
			}

			DrawWebString();

			yield return new WaitForEndOfFrame();
		}
	}
}
