﻿using UnityEngine;
using System.Collections;

public class SCR_Web : SCR_SlowAvatar {

	Animator anim;
	Transform trans;
	public Vector3 reScale = new Vector3(0.01f,0.01f,0);


    public float maxD;
   // public float size = 0.25f;

	void Start(){
		anim = this.GetComponent<Animator>();
		//trans = this.GetComponent<Transform>();
		anim.enabled = false;
	}

	protected override void TriggerEnterEffect(){
		Debug.Log ("Trigger");
		SCR_ManagerComponents.ManagerInstance.Spider.Activate();
		anim.enabled = true;
		StartCoroutine("DecreaseSize");

	}
	protected override void TriggerExitEffet(){
		anim.enabled = false;
	}


    IEnumerator DecreaseSize()
    {
		for (int i = 0; i < maxD; i++) 
		{
			if(this.transform.localScale.x > 0)
			{
				this.transform.localScale -= reScale * Time.deltaTime; 
			}
			else
			{
				Destroy(this.gameObject);
			}
			yield return new WaitForEndOfFrame();
		}
    }
}
