﻿using UnityEngine;
using System.Collections;

public class SCR_Bird : MonoBehaviour {


    public Scr_ShadowTarget shadow;

    public GameObject bird;

	SCR_Hurt beak;
	Animator anim;
	//bool isIdle;

	Vector3 targetPosition = Vector3.zero;
	Vector3 startPosition = Vector3.zero;
	Transform camieTransform;
	public Vector3 camieOffset = new Vector3 (0f,0f,1.3f);

	bool attackIsInvoked;
    public bool attackStarted = false;
    public int attacknb;
	int maxAttacks = 3;
	[Range (0,100)]
	public float attaqueApresXSecondes;

    public float flySpeed;


    void Start()
    {
		beak = this.transform.GetComponentInChildren<SCR_Hurt>();
		anim = this.GetComponent<Animator>();

		camieTransform = SCR_ManagerComponents.ManagerInstance.AvatarMove.transform;
		Pool (); 


	}

	void Update()
    {
        Fly();
        if (!attackIsInvoked && SCR_ManagerComponents.TimeManager.IsCounting){
			Invoke ("SetToIdlePosition", attaqueApresXSecondes);
            attackIsInvoked = true;
		}
	}

	void SetToIdlePosition()
    {

		if (Vector3.Distance (camieTransform.position, SCR_ManagerComponents.ManagerInstance.LeftFrame.position)
		    > Vector3.Distance (camieTransform.position, SCR_ManagerComponents.ManagerInstance.RightFrame.position))
        {

			//this.transform.rotation = Quaternion.AngleAxis (150f, this.transform.up); //il ferait face de l'autre cote.
		}
		this.transform.position = camieTransform.position + camieOffset;

		SCR_ManagerComponents.SoundManager.PlaySound ("SND_BirdComing");
		StartCoroutine ("Landing"); 
	}

    void Fly()
    {
        if(shadow.canMove)
        { 
            //Debug.Log("Hi");
            this.transform.position = Vector3.Lerp(this.transform.position, targetPosition + camieOffset, flySpeed * Time.deltaTime);
        }
    }


	IEnumerator Landing()
    {
		AttackStart();
		yield return null;
	}

	void AttackStart()
    {
        GetTargetPoint();
		Invoke ("Attack", 3f);
    }


    void Attack()//Coup de bec
    {
		attackStarted = true;
        anim.SetBool("canIdle", true);
        if (attacknb == maxAttacks)
        {
            EndAttack();
        }
        else
        {
            AttackStart();
        }
    }
    void GetTargetPoint()
    {
		targetPosition = SCR_ManagerComponents.ManagerInstance.AvatarMove.transform.position;
		//afficher un blot shadow sur ce point qui s'agrandit

	}

	void GoIdle()
    {
        if(attackStarted)
        {
            anim.SetTrigger("isIdles");
        }
    }

    void CanMove()
    {
        if(attackStarted)
        {
            shadow.canMove = !shadow.canMove;
        }
    }

    void BlobShadow()
    {
     if(attackStarted)
        {
            shadow.canGrow = !shadow.canGrow;
        }
    }

	void EndAttack(){

		beak.enabled = false;
		//part
		Invoke ("Pool", anim.GetCurrentAnimatorClipInfo(0).Length);
        bird.SetActive(false);
    }
	

	void Pool()
    {
		this.transform.position = new Vector3 (-100,-100, -100);
	}

    void AddAttack()
    {
        attacknb++;
    }
}
