﻿using UnityEngine;
using System.Collections;

public class SCR_Hurt : MonoBehaviour {
	
	
	void OnTriggerEnter (Collider other){
		if (other.tag == "Player" && SCR_ManagerComponents.TimeManager.IsCounting){
			AvatarWasHurt ();
		}
	}
	
	public void AvatarWasHurt (){
		SCR_ManagerComponents.PointManager.LosePucerons();
		//SCR_ManagerComponents.ManagerInstance.AvatarMove.LosePucerons();
	}
}
