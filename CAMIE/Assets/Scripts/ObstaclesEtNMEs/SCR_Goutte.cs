﻿using UnityEngine;
using System.Collections;

public class SCR_Goutte : SCR_SlowAvatar {

	protected override void TriggerEnterEffect(){
		SCR_ManagerComponents.SoundManager.PlaySound ("SND_Bubble");
		SCR_ManagerComponents.ManagerInstance.AvatarMove.InBubble(this);
	}
	protected override void TriggerExitEffet(){

	}
}
