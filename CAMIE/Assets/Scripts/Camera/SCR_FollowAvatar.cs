﻿using UnityEngine;
using System.Collections;

public class SCR_FollowAvatar : MonoBehaviour {

	public float followSpeed = 4.0f;
	public float zoomInSpeed = 5.0f;

	[Range(0,10)]
	public float distanceDuFrame;
	[Range (0,3)]
	public float plusHautQueCamie;

	private float minX;
	private float maxX;
	private float minY;
	private float maxY;

	#region init
	void Start(){
		DeleteExtraCameras();
		SetBounds ();
		StartCoroutine (ZoomInOn (SCR_ManagerComponents.ManagerInstance.AvatarMove.transform));
	}

	void DeleteExtraCameras(){
		GameObject[] sceneCameras = GameObject.FindGameObjectsWithTag ("MainCamera");
		foreach (GameObject camera in sceneCameras){
			if (camera != this.gameObject){
				Destroy (camera);
			}
		}
	}

	void SetBounds(){

		GameObject[] frame = GameObject.FindGameObjectsWithTag("Frame");

		foreach (GameObject border in frame){
			switch (border.name){
			case "left":
				minX = border.transform.position.x + distanceDuFrame;
				SCR_ManagerComponents.ManagerInstance.LeftFrame = border.transform;
				break;
			case "right":
				maxX = border.transform.position.x - distanceDuFrame;
				SCR_ManagerComponents.ManagerInstance.RightFrame = border.transform;
				break;
			case "top":
				maxY = border.transform.position.y - distanceDuFrame;
				break;
			case "bottom":
				minY = border.transform.position.y + distanceDuFrame;
				SCR_ManagerComponents.ManagerInstance.BottomFrame = border.transform;
				break;
			}
		}
	}

	IEnumerator ZoomInOn (Transform zoomTo){
		float time = 0f;
		Vector3 toVector = new Vector3();
		bool zoomIn = true;

		while (zoomIn){
			toVector = new Vector3 (zoomTo.position.x, zoomTo.position.y + plusHautQueCamie, -10f);
			this.transform.position = Vector3.Lerp (this.transform.position, toVector, time/zoomInSpeed);
			this.transform.position = BindPosition (this.transform.position);

			zoomIn = this.transform.position.z < -10.5f;
			time += Time.deltaTime;
			yield return new WaitForEndOfFrame();
		}


		SCR_ManagerComponents.PointManager.StartScoring();
		StartCoroutine ("FollowAvatar");
		StopCoroutine ("ZoomInOn");
		yield return null;
	}
	#endregion

	#region Positionning
	IEnumerator FollowAvatar(){
		while (true){

			if (!SCR_ManagerComponents.TimeManager.IsPaused){

				Vector3 focus = SCR_ManagerComponents.ManagerInstance.AvatarMove.transform.position;
				focus.y += plusHautQueCamie;
				focus.z = -10f;
				Vector3 newPosition = Vector3.Lerp (this.transform.position, focus, followSpeed * Time.deltaTime);
				this.transform.position = new Vector3 (newPosition.x, newPosition.y, -10f);

				this.transform.position = BindPosition (newPosition);
			}
			yield return null;
		}
	}

	Vector3 BindPosition (Vector3 newPosition){
		if (this.transform.position.x > maxX || this.transform.position.x < minX){
			float newX = this.transform.position.x > maxX ? maxX : minX;
			newPosition = new Vector3 (newX, this.transform.position.y, this.transform.position.z);
		}
		
		if (this.transform.position.y > maxY || this.transform.position.y < minY){
			float newY = this.transform.position.y > maxY ? maxY : minY;
			newPosition = new Vector3 (this.transform.position.x, newY, this.transform.position.z);
		}
		return newPosition;
	}
	#endregion
}
