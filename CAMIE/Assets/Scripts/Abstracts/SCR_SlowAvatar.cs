﻿using UnityEngine;
using System.Collections;

public abstract class SCR_SlowAvatar : MonoBehaviour {

	[Range (0,100)]
	public float speedDecreaseToPercent;

	protected abstract void TriggerEnterEffect();
	protected abstract void TriggerExitEffet();

	public void OnTriggerEnter(Collider other){
		if (other.tag == "Player" && !SCR_ManagerComponents.ManagerInstance.AvatarMove.IsSlowed){
			SCR_ManagerComponents.ManagerInstance.AvatarMove.DecreaseSpeed (speedDecreaseToPercent);
			TriggerEnterEffect ();
		}
	}

	public void OnTriggerExit (){
		SCR_ManagerComponents.ManagerInstance.AvatarMove.NormalSpeed ();
		TriggerExitEffet ();
	}
}
