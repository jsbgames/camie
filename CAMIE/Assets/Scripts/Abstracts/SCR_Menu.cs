﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public abstract class SCR_Menu : MonoBehaviour {

	#region init et vars
	private Transform[] children;
	private Dictionary<string, GameObject> menuItems = new Dictionary<string, GameObject>();
    private SCR_SoundManager sound;

	public void Awake(){
		children = this.transform.GetComponentsInChildren<Transform>();
		FillDictionary (children);
	}

	public void OnEnable(){
		if (SCR_ManagerComponents.GUIManager != null){
			ResetMenu();
			SetMenuLanguage ();
		}
		else return;
	}

	void FillDictionary (Transform[] children){
		foreach (Transform child in children){
			menuItems.Add (child.name, child.gameObject);
		}
	}
	#endregion

	#region Modif GUI
	public void ShowMenuItem (string itemName, bool show){
		GameObject item;

		if (menuItems.TryGetValue (itemName, out item)){
			item.SetActive (show);
		}
	}

	public void ResetMenu (){
		foreach (KeyValuePair<string, GameObject> pair in menuItems){
			pair.Value.SetActive (true);
		}
	}

	public GameObject GetMenuItem (string name){
		GameObject item;

		if (menuItems.TryGetValue (name, out item)){
			return item;
		}
		else return null;
	}

	public void ModifyText(string textGuiName, string textToWrite){
		GameObject item;

		if (menuItems.TryGetValue (textGuiName, out item)){
			Text text = item.GetComponent<Text>();

			if (text != null){
					text.text = textToWrite;
			}
		}
	}

	public IEnumerator AlphaLerp(Text text, float speed, bool isRepeating){
		float alphaValue = 0f;
		float modifier = 1f;

		while (text.isActiveAndEnabled){
			float dTime = SCR_ManagerComponents.TimeManager.IsPaused ? Time.fixedDeltaTime : Time.deltaTime;
			alphaValue += (dTime * speed * modifier);

			if (isRepeating && alphaValue >= 1f){
				modifier = -1f;
			}
			else if (alphaValue <= 0f){
				modifier = 1f;
			}
			
			Color newColor = text.color;
			newColor.a = alphaValue;
			text.color = newColor;
			
			yield return new WaitForEndOfFrame();
		}
	}

	public IEnumerator AlphaLerp(Image image, float speed, bool isRepeating){
		float alphaValue = 0f;
		float modifier = 1f;

		while (image.isActiveAndEnabled){
			
			alphaValue += (Time.fixedDeltaTime * speed * modifier);
			if (isRepeating && alphaValue >= 1f){
				modifier = -1f;
			}
			else if (alphaValue <= 0f){
				modifier = 1f;
			}
			
			Color newColor = image.color;
			newColor.a = alphaValue;
			image.color = newColor;
			
			yield return new WaitForEndOfFrame();
		}
	}
	#endregion

	#region Options
	public void Options (){
		ShowMenuItem ("Son", true);
		ShowMenuItem ("Langue", true);
		ShowMenuItem ("Options", false);
		
		ShowMenuItem ("On", !SCR_ManagerComponents.SoundManager.SoundOn);
		ShowMenuItem ("Off", SCR_ManagerComponents.SoundManager.SoundOn);
	}

	public void SwitchLanguage(bool isFrench){ 
		SCR_ManagerComponents.GUIManager.ToggleLanguage(isFrench);
	}

	public void ToggleSound(bool soundOn){
		SCR_ManagerComponents.SoundManager.SoundOn = soundOn;
		ShowMenuItem ("On", !soundOn);
        ShowMenuItem("Off", soundOn);


        if (!soundOn)
        {
            SCR_ManagerComponents.SoundManager.StopAllSounds();
        }
         else
        {
            SCR_ManagerComponents.SoundManager.PlaySound("SND_LevelsMusic");
        }
    }

    public void SetMenuLanguage()
    {
        if (SCR_ManagerComponents.GUIManager == null) return;
        if (children == null) children = this.transform.GetComponentsInChildren<Transform>();

		for (int i = 0; i < children.Length; i++){
			if (children[i].tag == "FrenchGUI"){
				ShowMenuItem (children[i].name, SCR_ManagerComponents.GUIManager.IsFrench);
			}
			else if (children[i].tag == "EnglishGUI"){
				ShowMenuItem (children[i].name, !SCR_ManagerComponents.GUIManager.IsFrench);
			}
		}


        /* foreach (Transform child in children)
         {
             if (SCR_ManagerComponents.GUIManager.IsFrench)
             {
				child.gameObject.SetActive(child.gameObject.tag == "FrenchGUI");

			}
         else if (!SCR_ManagerComponents.GUIManager.IsFrench)
             {
				child.gameObject.SetActive(child.gameObject.tag == "EnglishGUI");

			}
         }*/

        /*foreach (Transform child in children)
        {
            if (SCR_ManagerComponents.GUIManager.IsFrench)
            {

                if (child.gameObject.tag == "FrenchGUI")
                {
                    child.gameObject.SetActive(true);

                    if (child.tag == "EnglishGUI")
                    {
                        //child.gameObject.SetActive(false);
                    }
                }
            }
            else if (!SCR_ManagerComponents.GUIManager.IsFrench)
            {
                if (child.gameObject.tag == "EnglishGUI")
                {
                    child.gameObject.SetActive(true);

                    if (child.gameObject.tag == "FrenchGUI")
                    {
                       // child.gameObject.SetActive(false);
                    }
                }
            }*/

            /*  foreach (Transform child in children)
                {
                    if ((SCR_ManagerComponents.GUIManager.IsFrench && child.tag == "FrenchGUI")
                            || (!SCR_ManagerComponents.GUIManager.IsFrench && child.tag == "FrenchGUI"))
                    {
                        child.gameObject.SetActive(false);
                    }
                    else if ((SCR_ManagerComponents.GUIManager.IsFrench && child.tag == "FrenchGUI")
                            || (!SCR_ManagerComponents.GUIManager.IsFrench && child.tag == "EnglishGUI"))
                    {
                        child.gameObject.SetActive(true);
                    }
                }*/

        //}
    }


    public void ReturnToMainMenu(){
		SCR_ManagerComponents.LevelManager.LoadLevel (SCR_LevelManager.MAINMENU);
	}

	public void LoadLevel (int level){
		SCR_ManagerComponents.LevelManager.LoadLevel (level);
	}
	#endregion

	public void ButtonSound(){
		SCR_ManagerComponents.SoundManager.PlaySound ("SND_Button");
	}
}
