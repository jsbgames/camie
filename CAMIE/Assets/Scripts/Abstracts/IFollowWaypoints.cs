﻿using UnityEngine;
using System.Collections;

interface IFollowWaypoints{
	SCR_Waypoint[] GetWaypoints();
	SCR_Waypoint[] SortWaypoints(SCR_Waypoint[] unorderedArray, bool isInit);
	void UnChildWaypoints();
	SCR_Waypoint GetNextWaypoint();
	IEnumerator MoveToWaypoint (SCR_Waypoint wp);
	//IEnumerator SmoothTurning (SCR_Waypoint from, SCR_Waypoint to, float speed);
	void StartMove();
	void StopMove();
}
