﻿using UnityEngine;
using System.Collections;

public abstract class SCR_Collectible : MonoBehaviour {

	protected int pointValue;
	protected bool isTaken;

	protected abstract void Collect();

	public int PointValue{
		get {return pointValue;}
	}
	public bool IsTaken{
		get {return isTaken;}
		set {isTaken = value;}
	}
}
