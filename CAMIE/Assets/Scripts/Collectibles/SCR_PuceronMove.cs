﻿using UnityEngine;
using System.Collections;

public class SCR_PuceronMove : SCR_Puceron, IFollowWaypoints {

	public enum TrajectoryType{
		allerRetour,
		enBoucle
	};
	public TrajectoryType trajectoire;

	private float speed = 1f;
	private SCR_Waypoint[] myWaypoints;
	private int goingToIndex = 1;
	private bool doMove;
	private bool isRotating;
	private bool isStarted; 

	new void Start(){
		base.Start();
		myWaypoints = GetWaypoints();

		SCR_ManagerComponents.PointManager.IncrementMovingPucerons();

		UnChildWaypoints();

	}

	void Update(){
		if (!isStarted && SCR_ManagerComponents.TimeManager.IsCounting){
			StartMove();
			isStarted = true;
		}
	}

	protected override void Collect (){
		base.Collect ();
		StopMove();
	}

	new public void ReturnToGame(){
		base.ReturnToGame();
		goingToIndex = 1;
		StartMove();
	}

	public SCR_Waypoint[] GetWaypoints(){
		return SortWaypoints (this.GetComponentsInChildren<SCR_Waypoint>(), true);
	}

	public SCR_Waypoint[] SortWaypoints(SCR_Waypoint[] unorderedArray, bool isInit){

		SCR_Waypoint[] newArray = new SCR_Waypoint[unorderedArray.Length];

		if (isInit){
			for (int i = 0; i < unorderedArray.Length; i++){
				newArray[unorderedArray[i].id] = unorderedArray[i];
			}

		}
		else{
			switch (trajectoire){
			case TrajectoryType.allerRetour:

				goingToIndex = 0;
				for (int i = 0; i < unorderedArray.Length; i++){
					newArray[i] = unorderedArray[unorderedArray.Length - 1 - i];
				}
				break;
			case TrajectoryType.enBoucle: 
				int index = goingToIndex-1;
				for (int i = 0; i < unorderedArray.Length; i++){

					newArray[i] = unorderedArray[index];
					index = (++index) % (unorderedArray.Length - 1);
				}
				goingToIndex = 0;
				break;
			}
		}

		return newArray;
	}

	public void UnChildWaypoints(){
		foreach (SCR_Waypoint wpt in myWaypoints){
			wpt.transform.parent = null;
		}
	}

	public SCR_Waypoint GetNextWaypoint(){
		goingToIndex ++;
		if (goingToIndex == myWaypoints.Length){
			myWaypoints = SortWaypoints (myWaypoints, false);
			goingToIndex = 0;
		}
		return myWaypoints[goingToIndex];
	}

	public IEnumerator MoveToWaypoint (SCR_Waypoint wp){

		doMove = true;

		while (doMove){

			if (Vector3.Distance (this.transform.position, wp.transform.position) < 0.5f){
				//StartCoroutine (SmoothTurning (wp, GetNextWaypoint(), 2f));
				doMove = false;
			}
			Vector3 target = -(this.transform.position - wp.transform.position);
			//if (target.y < 0) target.y = -target.y;
			this.transform.right = target; //Smooth Turning: trouver moyen de prendre cette valeur progressivement
			this.transform.position = Vector3.Lerp (this.transform.position, wp.transform.position, speed * Time.deltaTime);

			yield return new WaitForEndOfFrame();
		}
		StartCoroutine (MoveToWaypoint (GetNextWaypoint()));

		yield return null;
	}
//
//	public IEnumerator SmoothTurning (SCR_Waypoint from, SCR_Waypoint to, float speed){
//		isRotating = true;
//		float time = 0f;
//
//		Quaternion originalRotation = this.transform.rotation;
//		this.transform.right = (this.transform.position - to.transform.position);
//
//		Quaternion target = this.transform.rotation;
//		this.transform.rotation = originalRotation;
//		
//		while (time < speed){
//			
//			transform.rotation = Quaternion.Slerp (transform.rotation, target, (time/speed));
//			time += Time.deltaTime; 
//			
//			yield return new WaitForEndOfFrame();
//		}
//		this.transform.right = (this.transform.position - to.transform.position);
//		isRotating = false;
//		StartCoroutine (MoveToWaypoint (to));
//		yield return null;
//	}

	public void StartMove(){
		doMove = true;
		StartCoroutine (MoveToWaypoint (myWaypoints[goingToIndex]));
	}

	public void StopMove(){
		doMove = false;
		StopAllCoroutines();
	}
}

