﻿using UnityEngine;
using System.Collections;

public class SCR_Star : SCR_Collectible {

	Animator starAnim;

	void Start(){
		starAnim = this.GetComponent<Animator>();

		GameObject child = this.transform.GetChild(0).gameObject;
		child.transform.parent = null;
		this.transform.position = child.transform.position;
		child.SetActive(false);
	}

	void OnTriggerEnter(Collider other){
		if (other.tag == "Player"){
			Collect ();
		}
	}

	void Kill (){
		Destroy (this.gameObject);
	}
	
	protected override void Collect ()
	{
		isTaken = true; 
		starAnim.SetBool ("isTaken", isTaken);
		SCR_ManagerComponents.PointManager.AcquireCollectible (SCR_PointManager.CollectibleType.ladybug);
		this.GetComponent<Collider>().enabled = false;
		Invoke ("Kill", 1f);
	}
}
