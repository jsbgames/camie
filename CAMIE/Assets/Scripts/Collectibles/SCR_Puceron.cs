﻿using UnityEngine;
using System.Collections;

public class SCR_Puceron : SCR_Collectible {

	protected Vector3 originalPosition;
	protected Quaternion originalRotation;
	protected BoxCollider trigger;

	public void Start(){
		originalPosition = this.transform.position;
		originalRotation = this.transform.rotation;
		Invoke ("UnRotate", 0.1f);
	}

	void UnRotate(){
		Transform[] wps = GetComponentsInChildren<Transform>();

		if (wps.Length < 1){
			this.transform.eulerAngles = new Vector3(0,0,0);
		}
		else{
			Invoke ("UnRotate", 0.1f);
		}
	}

	void OnTriggerEnter(Collider other){
		if (other.tag == "Player"){
			Collect();
		}
	}

	void Update (){
		if (isTaken && Input.GetKeyDown (KeyCode.Backspace)){
			ReturnToGame ();
		}
	}

	protected override void Collect(){
		isTaken = true;
		SCR_ManagerComponents.PointManager.AcquireCollectible (SCR_PointManager.CollectibleType.puceron); 
		SCR_ManagerComponents.PointManager.PuceronsCollected.Add (this);
		Pool ();
	}

	void Pool(){
		Vector3 newPosition = new Vector3 (this.transform.position.x, this.transform.position.y, -100);
		this.transform.position = newPosition;

		if (trigger == null){
			trigger = this.GetComponent<BoxCollider>();
		}
		trigger.enabled = false;
	}

	public void ReturnToGame(){
		isTaken = false;
		this.transform.position = originalPosition;
		this.transform.rotation = originalRotation;
		trigger.enabled = true;
	}

}
