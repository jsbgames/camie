﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SCR_LevelEndMenu : SCR_Menu {

	public static SCR_LevelEndMenu instance;

	public Sprite[] lockedLevels;
	public Sprite[] unlockedLevels;

	private Text[] newHighScoreText = new Text[2];
	private bool canGoToNext;

	new void Awake(){
		//Debug.Log (instance == null);
		if (SCR_ManagerComponents.GUIManager.LevelEndMenu == this){
			instance = this;
			DontDestroyOnLoad (this.gameObject);
		}
		else{
			Destroy (this.gameObject);
			return;
		}
		base.Awake();
	}



	new void OnEnable(){
		if (SCR_ManagerComponents.GUIManager == null) return;
		base.OnEnable();
		newHighScoreText[0] = GetMenuItem ("TXT_NewHighscore").GetComponent<Text>();
		newHighScoreText[1] = GetMenuItem ("TXT_NvMeilleurScore").GetComponent<Text>();

		canGoToNext = SCR_ManagerComponents.LevelManager.GameplayLevel < lockedLevels.Length - 1 && 
			SCR_ManagerComponents.PointManager.ScoreCumul > SCR_ManagerComponents.PointManager.PointsForLevelAccess[SCR_ManagerComponents.LevelManager.GameplayLevel+1];

		AssignButtonImages();

		if (SCR_ManagerComponents.PointManager.TotalScore == SCR_ManagerComponents.PointManager.HighScores[SCR_ManagerComponents.LevelManager.GameplayLevel]){
			NewHighScore();
		}
	}

	void AssignButtonImages(){
		Image replayBtnImg = GetMenuItem ("ReplayLevelButton").GetComponent<Image>();
		Image nextLvlBtnImg;

		if (canGoToNext){
			nextLvlBtnImg = GetMenuItem ("NextLevelButton").GetComponent<Image>();
			ShowMenuItem ("LevelLockedButton", false);
		}
		else{
			nextLvlBtnImg = GetMenuItem ("LevelLockedButton").GetComponent<Image>();
			ShowMenuItem ("NextLevelButton", false);
		}

		if (SCR_ManagerComponents.LevelManager.GameplayLevel < lockedLevels.Length - 1){
			replayBtnImg.sprite = unlockedLevels[SCR_ManagerComponents.LevelManager.GameplayLevel - 1];
			nextLvlBtnImg.sprite = canGoToNext ? unlockedLevels[SCR_ManagerComponents.LevelManager.GameplayLevel] : lockedLevels[SCR_ManagerComponents.LevelManager.GameplayLevel];
		}
		else{
			ShowMenuItem (nextLvlBtnImg.name, false);
		}
	}

	public void Reload(){
		LoadLevel (Application.loadedLevel);
	}

	public void LoadNext(){
		LoadLevel (Application.loadedLevel + 1);
	}

	#region High Score
	public void NewHighScore(){
		SetMenuLanguage();
		foreach (Text text in newHighScoreText){
			if (text.isActiveAndEnabled){
				StartCoroutine (AlphaLerp (text, 1f, true));
				break;
			}
		}
	}


	#endregion
}
