﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SCR_LevelStartCountdown : SCR_Menu {

	public static SCR_LevelStartCountdown instance;

	GameObject[] countdownTexts = new GameObject[4];
	int activeText = 0;
	bool getBigger = true;

	 void Awake(){
		if (instance == null){
			instance = this;
			DontDestroyOnLoad (this.gameObject);
		}
		else{
			Destroy (this.gameObject);
			return;
		}
		base.Awake();
		FillCountdownArray();
	}

	new void OnEnable(){

		base.OnEnable();
		if (SCR_ManagerComponents.GUIManager == null) return;
		Reinitialize();
        if (Application.loadedLevel != SCR_LevelManager.MAINMENU)
        {
            Countdown();
        }
	}

    void OnDisable()
    {

		StopAllCoroutines();
    }

	public void Countdown(){
		if (getBigger){
			StartCoroutine (Rescale(true, countdownTexts[activeText].transform));
		}
		else{
			StartCoroutine (Rescale(false, countdownTexts[activeText].transform));
		}
	}

	IEnumerator Rescale (bool up, Transform objToScale){
		float time = 0f;

		if (up){
			ShowMenuItem (objToScale.name, true);
			objToScale.localScale = new Vector3 (0.1f, 0.1f, 0.1f);

			while (time < 0.5f){
				objToScale.localScale = Vector3.Lerp (objToScale.localScale, Vector3.one, time/60);
				time += Time.deltaTime;

				yield return new WaitForEndOfFrame();
			}
			getBigger = false;
			Countdown();
		}
		else{
			objToScale.localScale = Vector3.one;

			while (objToScale.localScale.x > 0.5f){
				objToScale.localScale = Vector3.Lerp (objToScale.localScale, Vector3.zero, time/60);
				time += Time.deltaTime;

				yield return new WaitForEndOfFrame();
			}
			getBigger = true;
			ShowMenuItem (objToScale.name, false);

			if (activeText != 3){
				activeText ++;
				Countdown();
			}
			else{
				SCR_ManagerComponents.GUIManager.OpenLevelMenus();
			}
		}
	}

	void Reinitialize(){
		HideAll();
		activeText = 0;
		getBigger = true;
	}

	void HideAll(){
		for (int i = 0; i < countdownTexts.Length; i++){
			ShowMenuItem (countdownTexts[i].name, false);
		}
	}

	void FillCountdownArray(){
		countdownTexts[0] = GetMenuItem ("3sec");
		countdownTexts[1] = GetMenuItem ("2sec");
		countdownTexts[2] = GetMenuItem ("1sec");
		countdownTexts[3] = GetMenuItem ("Start");
	}

}
