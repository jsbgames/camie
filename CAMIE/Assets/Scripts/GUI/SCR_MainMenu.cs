﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Text.RegularExpressions;
using System.Collections.Generic;

public class SCR_MainMenu : SCR_Menu {
	#region variables

	public static SCR_MainMenu instance;

	static bool isFromSplashScreen = false;
	bool timerOn;
	float time = 0f;
	public float timeBeforePressToContinue; 
	Text pressToContinue; 
	bool selectedIsLocked;
	Image selectedLevelImg;
	int selectedLevel; 
	bool hasSelectedALevel;

	GameObject[] levelSets = new GameObject[5];
	int currentLevelSet = 0;
	Dictionary<int, bool> levelsLocked = new Dictionary<int, bool>();
	#endregion

	#region monobevahior

	new void Awake(){
		if (instance == null){
			instance = this;
			DontDestroyOnLoad (this.gameObject);
		}
		else{
			Destroy (this.gameObject);
			return;
		}
		base.Awake();
	}
	new void OnEnable(){
		if (SCR_ManagerComponents.GUIManager == null) return;

		base.OnEnable();

		LevelSetsInit();

      //  if (isFromSplashScreen){
			ShowMenuItem ("MainMenu", false);
            ShowMenuItem("Tutorial", false);
			timerOn = true;

			time = 0f;
		//}
		/*else{
			OpenMenu();
		}*/
    }

	void Update(){
		if (timerOn){
			time += Time.deltaTime;

			if (time >= timeBeforePressToContinue){
				Text[] bothLanguages = GetMenuItem ("PressToContinue").GetComponentsInChildren<Text>();
				foreach (Text text in bothLanguages){
					if (text.isActiveAndEnabled){
						StartCoroutine (AlphaLerp (text, 0.3f, false));
						break;
					}
				}
				timerOn = false;
			}
		}
	}
	#endregion

	#region menu init
	public void OpenMenu(){
        ShowMenuItem("Tutorial", false);
		ShowMenuItem ("MainMenu", true);

		hasSelectedALevel = false;
		
		selectedLevelImg = GetMenuItem ("SelectedLevel").GetComponent<Image>();

		ShowMenuItem ("SelectedLevel", false);
		ShowMenuItem ("TXT_LevelInfo", false);
		ShowMenuItem ("PlayButtons", false);
	}
	
	void LevelSetsInit(){
		currentLevelSet = 0;
		levelsLocked.Clear();
        if (SCR_ManagerComponents.PointManager == null)
        {
            Invoke("LevelSetsInit", 0.01f);
            return;
        }
        for (int i = 0; i < levelSets.Length; i++){
			levelSets[i] = GetMenuItem ("LevelsSet" + (i+1).ToString());
			
			foreach (Transform child in levelSets[i].transform){
				int level = int.Parse (Regex.Match(child.name, @"\d+").Value)-1;
				bool isLocked = SCR_ManagerComponents.PointManager.ScoreCumul > SCR_ManagerComponents.PointManager.PointsForLevelAccess[level];
				
				bool foo;
				if (!levelsLocked.TryGetValue(level, out foo)){
					levelsLocked.Add (level, isLocked);
				}
				
				ShowMenuItem (level.ToString(), !isLocked);
				ShowMenuItem (level.ToString() + "Locked", isLocked);
			}
			
			if (i > currentLevelSet){
				ShowMenuItem (levelSets[i].name, false);
				ShowMenuItem ("Previous", false);
			}
		}
	}

    public void ShowTutorials()
    {
        SetMenuLanguage();

        ShowMenuItem("Tutorial", true);
    }

	#endregion

	#region levels
	public void ShowLevelInfo (int level){
		if (levelsLocked.TryGetValue(level, out selectedIsLocked)){
				
			if (!hasSelectedALevel){
				ShowMenuItem ("SelectedLevel", true);
				ShowMenuItem ("TXT_LevelInfo", true);
				ShowMenuItem ("PlayButtons", true);
				
				hasSelectedALevel = true;
			}

			Image pressedButtonImg;

			if (selectedIsLocked){
				pressedButtonImg = GetMenuItem (level.ToString() + "Locked").GetComponent<Image>();
				selectedLevelImg.sprite = pressedButtonImg.sprite;

				float pointsForSelectedLvl = 0f; // = total correspondant dans le tableau point manager.
				ModifyText ("TXT_LevelInfo", (pointsForSelectedLvl /* - points accumules*/).ToString() + (SCR_ManagerComponents.GUIManager.IsFrench ? " points manquants." : " points missing."));
				ShowMenuItem ("PlayButtons", false);
			}
			else{
				pressedButtonImg = GetMenuItem (level.ToString()).GetComponent<Image>();
				selectedLevel = level; 
				selectedLevelImg.sprite = pressedButtonImg.sprite;

				ModifyText ("TXT_LevelInfo", (SCR_ManagerComponents.GUIManager.IsFrench ? "Meilleur score : " : "Highscore: ") + SCR_ManagerComponents.PointManager.HighScores[selectedLevel-1]);
				ShowMenuItem ("PlayButtons", true);
			}
		}
	}
	
	public void StartLevel (){
		SCR_ManagerComponents.LevelManager.LoadLevel (selectedLevel);
	}

	public void ScrollLevelSets(bool goRight){
		ShowMenuItem (levelSets[currentLevelSet].name, false);

		if (goRight){
			currentLevelSet ++;
		}
		else{
			currentLevelSet --;
		}

		ShowMenuItem ("Next", (currentLevelSet != levelSets.Length - 1));
		ShowMenuItem ("Previous", (currentLevelSet != 0));
		ShowMenuItem (levelSets[currentLevelSet].name, true);
	}
	#endregion

	public static bool IsFromSplashScreen{
		get {return isFromSplashScreen;}
		set {isFromSplashScreen = value;}
	}
}
