﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class SCR_Joystick : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {

	#region variables
	private bool isDragging;
	private Touch currTouch;
	int touchId;
	private Vector2 currTouchPosition;
	public float radius;

	private RectTransform rectTransform;
	private Transform center;
	private Vector3 axis;

	private SCR_Camie avatar;
	private Image img;

	public enum Direction{
		left,
		right
	};
	private SCR_JoystickDirection[] directionPoints;
	private Direction pointingTo;
	private Direction activeDirection;
	#endregion

	#region Monobehaviors
	void Start(){

		if (SCR_ManagerComponents.ManagerInstance.Joystick == null){
			SCR_ManagerComponents.ManagerInstance.Joystick = this;
			DontDestroyOnLoad (this.gameObject);
		}
		else{
			Destroy (this.gameObject);
			return;
		}

		img = this.GetComponent<Image>();
		avatar = SCR_ManagerComponents.ManagerInstance.AvatarMove;
		rectTransform = this.GetComponent<RectTransform>();

		foreach (Transform child in transform.parent){
			if (child.name == "Center"){
				center = child;
				break;
			}
		}

		directionPoints = this.transform.parent.GetComponentsInChildren<SCR_JoystickDirection>();
		activeDirection = Direction.right;
		pointingTo = Direction.right;
	}

	void OnLevelWasLoaded(){
		avatar = SCR_ManagerComponents.ManagerInstance.AvatarMove;
		activeDirection = Direction.right;
		pointingTo = Direction.right;
		isDragging = false;
	}

	void OnDisable (){
		isDragging = false;
	}

	void Update (){
		if (avatar.Respawner.IsRespawning){
			isDragging = false;
		}

		if (isDragging){
			if (Application.isEditor) currTouchPosition = new Vector2 (Input.mousePosition.x, Input.mousePosition.y);
			else{
				foreach (Touch touch in Input.touches){
					//if (Vector3.Distance (this.transform.position, touch.rawPosition) < radius){
					if (touch.fingerId == touchId){
						currTouchPosition = touch.position;
						currTouch = touch;
					}
				}
			}
			FollowTouch();
			AdjustPosition();

			pointingTo = GetClosestDirection();
			if (pointingTo != activeDirection){
				activeDirection = SwitchDirections(pointingTo);
			}

			if (!Application.isEditor && (currTouch.phase == TouchPhase.Canceled || currTouch.phase == TouchPhase.Ended)){
				isDragging = false;
			}

			float axisX = -(center.position.x - this.transform.position.x)/radius;
			float axisY = -(center.position.y - this.transform.position.y)/radius;
			axis = new Vector3 (axisX, axisY, 0f);

			if (SCR_ManagerComponents.ManagerInstance.AvatarMove.IsGrounded && !SCR_ManagerComponents.ManagerInstance.FlyButton.IsPressed){
				ToAvatarAngle (axis);
			}
		}
		else if (rectTransform.position != center.position){
			rectTransform.position = center.position;
			pointingTo = GetClosestDirection();
		}
	}
	#endregion

	#region Dragging State & Pointers
	public void OnPointerDown (PointerEventData eventData){
		isDragging = true;
		currTouchPosition = eventData.position;
		touchId = eventData.pointerId;

//		pointingTo = GetClosestDirection();
//		if (pointingTo != activeDirection){
//			activeDirection = SwitchDirections(pointingTo);
//		}
	}
	
	public void OnPointerUp (PointerEventData eventData){
		isDragging = false;
	}
	#endregion

	#region Position Adjustments
	public void FollowTouch(){
		Vector3 newJoystickPosition = new Vector3 (currTouchPosition.x, currTouchPosition.y, 0f);

		rectTransform.position = newJoystickPosition;
	}

	void AdjustPosition (){
		Vector3 adjustedPosition = rectTransform.position;

		if (/*adjustedPosition.magnitude*/ Vector3.Distance (adjustedPosition, center.position) >= radius){
			Vector3 direction = adjustedPosition - center.position;
			adjustedPosition = center.position + (direction.normalized * radius);
			rectTransform.position = adjustedPosition;
		}
	}

	Vector3 ToAvatarAngle (Vector3 vector){
		return SCR_ManagerComponents.ManagerInstance.AvatarMove.transform.rotation * vector;
	}
	#endregion

	#region Directions
	Direction GetClosestDirection(){
		float smallestDistance = Mathf.Infinity;
		SCR_JoystickDirection closest = directionPoints[0];
		for (int i = 0; i < directionPoints.Length; i++){
			float distFromI = Vector3.Distance (this.transform.position, directionPoints[i].transform.position);
			if (distFromI < smallestDistance){
				smallestDistance = distFromI;
				closest = directionPoints[i];
			}
		}
		return closest.myDirection;
	}

	Direction SwitchDirections(Direction newDir){
		Direction toReturn = Direction.right;
		switch (newDir){
		case Direction.right:
//			if (activeDirection == Direction.left){
//				avatar.ChangeDirection(false);
//			}
			break;
		case Direction.left:
//			if (activeDirection == Direction.right){
//				avatar.ChangeDirection(true);
//			}
			toReturn = Direction.left;
			break;
		}
		return toReturn;
	}
	#endregion

	public bool IsDragging{
		get {return isDragging;}
	}
	public float HorizontalAxis{
		get {return axis.x;}
	}
	public float VerticalAxis{
		get {return axis.y;}
	}
	public Direction PointingTo{
		get {return pointingTo;}
	}
}
