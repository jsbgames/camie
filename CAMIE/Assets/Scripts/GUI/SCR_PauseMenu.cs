﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SCR_PauseMenu : SCR_Menu {

	public SCR_PauseMenu instance;

	public GameObject[] ladybugGUI = new GameObject[3]; 

	new void Awake(){
		if (instance == null){
			instance = this;
			DontDestroyOnLoad (this.gameObject);
		}
		else{
			Destroy (this.gameObject);
			return;
		}
		base.Awake ();
	}

	new void OnEnable(){

		base.OnEnable();
		if (SCR_ManagerComponents.GUIManager == null) return;
		ShowMenuItem ("PauseMenu", false);
		ShowLadybugCollectibles (0);
		SCR_ManagerComponents.GUIManager.RelayTextInfo (SCR_GUIManager.TextType.puceron);
        SetMenuLanguage();
        //Debug.Log("PauseMenu");
	}

	public void PauseGame (bool pause){

		ShowMenuItem ("PauseButton", !pause);
		ShowMenuItem ("PauseMenu", pause);

		if (pause){
			ShowMenuItem ("Son", false);
			ShowMenuItem ("Langue", false);
            ShowMenuItem("Options", true);
		}

		SCR_ManagerComponents.TimeManager.Pause ();
	}

	public void ShowLadybugCollectibles (int collected){

		for (int i = 0; i < ladybugGUI.Length; i++){
			int valeur = i + 1;
			if (valeur == collected){
				ShowMenuItem (ladybugGUI[i].name, true);
			}
			else {
				ShowMenuItem (ladybugGUI[i].name, false);
			}
		}
	}
}