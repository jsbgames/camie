﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class SCR_FlyButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler{

	private bool isPressed;
	private bool shown;
	private Button button;
	private Image image;

	void Start(){
		if (SCR_ManagerComponents.ManagerInstance.FlyButton == null){
			SCR_ManagerComponents.ManagerInstance.FlyButton = this;
			DontDestroyOnLoad (this.gameObject);
		}
		else{
			Destroy (this.gameObject);
			return;
		}
		button = this.GetComponent<Button>();
		image = this.GetComponent<Image>();
		shown = true;
	}

	void Update(){
		if (SCR_ManagerComponents.ManagerInstance.AvatarMove == null) return;

		if (shown != SCR_ManagerComponents.ManagerInstance.AvatarMove.CanFly){
			shown = SCR_ManagerComponents.ManagerInstance.AvatarMove.CanFly;
			ShowButton (shown);
		}
	}

	void OnDisable(){
		isPressed = false;
	}

	public void OnPointerDown (PointerEventData eventData){
		isPressed = true;
	}
	
	public void OnPointerUp (PointerEventData eventData){
		isPressed = false;
	}

	void ShowButton (bool show){
		image.enabled = show;
		button.enabled = show;
	}

	public bool IsPressed{
		get {return isPressed;}
		set {isPressed = value;}
	}
}
