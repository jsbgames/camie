﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class SCR_GUIManager : MonoBehaviour {

	#region Variables
	public Canvas[] gameCanvases;
	public List<SCR_Menu> menus = new List<SCR_Menu>();

	private SCR_PauseMenu pauseMenu;
	private SCR_MainMenu mainMenu;
	private SCR_LevelEndMenu lvlEndMenu;

	private bool isFrench;

	public enum TextType{
		ladybug,
		puceron,
		time,
		highscore
	};
	#endregion

	#region Monobehaviors
	void Awake(){

		if (SCR_ManagerComponents.GUIManager == null){
			SCR_ManagerComponents.GUIManager = this;
		}
		else return;

		gameCanvases = GameObject.FindObjectsOfType<Canvas>();
		bool isEmpty = gameCanvases[0] == null;

		for (int i = 0; i < gameCanvases.Length; i++){
			if (isEmpty){
				string name = gameCanvases[i].name;
				gameCanvases[i] = Instantiate (gameCanvases[i]) as Canvas;

				if (gameCanvases[i].name.Contains ("(Clone)")){
					gameCanvases[i].name = name;
				}
			}

			DontDestroyOnLoad (gameCanvases[i].gameObject);
			SCR_Menu menuComponent = gameCanvases[i].GetComponent<SCR_Menu>();
			if (menuComponent != null) menus.Add (menuComponent);
		}

		switch (Application.systemLanguage){
		case SystemLanguage.French:
			isFrench = false;
			break;
		default: 
			isFrench = true;
			break;
		}

		foreach (SCR_Menu menu in menus){
			pauseMenu = menu.GetComponent<SCR_PauseMenu>();

			if (pauseMenu != null){
				break;
			}
		}
		foreach (SCR_Menu menu in menus){
			if (menu.gameObject != pauseMenu.gameObject){
				mainMenu = menu.GetComponent<SCR_MainMenu>();
                
				if (mainMenu != null){
					break;
				}
			}
		}
		foreach (SCR_Menu menu in menus){
			if (menu.gameObject != pauseMenu.gameObject && menu.gameObject != mainMenu.gameObject){
				lvlEndMenu = menu.GetComponent<SCR_LevelEndMenu>();

				if (lvlEndMenu != null){
					break;
				}
			}
		}
		//ToggleLanguage (isFrench);
		LevelInit ();

	}

	void OnLevelWasLoaded(){
		LevelInit();
	}
	#endregion

	#region init
	void LevelInit(){
		/*if (Application.loadedLevel == SCR_LevelManager.SPLASHSCREEN){
			CloseAllMenus();
			OpenMenu ("Canvas_SplashScreen");
		}
		else*/ if (Application.loadedLevel == SCR_LevelManager.MAINMENU){
			CloseAllMenus();
			OpenMenu ("Canvas_MainMenu");
		}
		else{
			CloseAllMenus();
			OpenMenu ("Canvas_LevelStart");
		}
	}
	#endregion

	#region Open and Close Menus
	public void CloseAllMenus (){
		foreach (Canvas canvas in gameCanvases){
			if (canvas.isActiveAndEnabled){
				canvas.gameObject.SetActive (false);
			}
		}
	}

	public void OpenMenu (string menuName){
		foreach (Canvas canvas in gameCanvases){
			if (canvas.name == menuName){
				canvas.gameObject.SetActive (true);
				break;
			}
		}
	}
	
	public void OpenLevelMenus(){
		CloseAllMenus();
		
		OpenMenu ("Canvas_In-GameButtons");
		OpenMenu ("Canvas_PauseMenu");
		
		SCR_ManagerComponents.TimeManager.SetTimer (true);
	}
	#endregion

	#region GUI modification
	public void RelayTextInfo(TextType collectibleType){

		switch (collectibleType){
		case TextType.ladybug:
			if (SCR_ManagerComponents.TimeManager.IsCounting){

				pauseMenu.ShowLadybugCollectibles (SCR_ManagerComponents.PointManager.LadybugsCurrentLevel);
			}
			else if (pauseMenu != null){
				pauseMenu.ShowLadybugCollectibles (0);
			}
			break;

		case TextType.puceron:
			if (SCR_ManagerComponents.TimeManager.IsCounting){
				pauseMenu.ModifyText ("TXT_Pucerons", SCR_ManagerComponents.PointManager.PuceronsCurrentLevel.ToString());
			}
			else pauseMenu.ModifyText ("TXT_Temps", "0");
			break;

		case TextType.time:
			if (SCR_ManagerComponents.TimeManager.IsCounting){
				pauseMenu.ModifyText ("TXT_Temps",  Mathf.Round (SCR_ManagerComponents.TimeManager.TimeSinceLevelStart).ToString());
			}
			else if (pauseMenu != null){
				pauseMenu.ModifyText ("TXT_Temps", "0");
			}
			break;

		case TextType.highscore:
			lvlEndMenu.ModifyText ("TXT_SNumber", SCR_ManagerComponents.PointManager.TotalScore.ToString());
			lvlEndMenu.ModifyText ("TXT_HSNumber", SCR_ManagerComponents.PointManager.HighScores[SCR_ManagerComponents.LevelManager.GameplayLevel].ToString());
			break;
		}
	}

	public void ToggleLanguage (bool french){
        isFrench = !isFrench;
        
		foreach (SCR_Menu menu in menus){
            menu.SetMenuLanguage();
		}
	}
	#endregion

	#region Accessors
	public bool IsFrench{
		get {return isFrench;}
	}
	public SCR_LevelEndMenu LevelEndMenu{
		get {return lvlEndMenu;}
	}
	public SCR_MainMenu MainMenu{
		get {return mainMenu;}
	}
	#endregion
}
