﻿using UnityEngine;
using System.Collections;

public class SCR_ManagerComponents : MonoBehaviour {

	private static SCR_ManagerComponents instance;
	private static SCR_PointManager pointManager;
	private static SCR_LevelManager levelManager;
	private static SCR_SaveData saveData;
	private static SCR_TimeManager timeManager;
	private static SCR_GUIManager guiManager;
	private static SCR_SoundManager soundManager;

	private SCR_Camie avMove;
	private SCR_Joystick joystick;
	private SCR_FlyButton flyButton;
	private SCR_Spider sceneSpider;
	private Transform leftFrame;
	private Transform rightFrame;
	private Transform bottomFrame;

	void Awake(){

		if (instance != null && instance != this){
			Destroy (this.gameObject);
			return;
		}

		instance = this;
		DontDestroyOnLoad (this.gameObject);

		if (Application.loadedLevelName.Contains("NIV") || Application.loadedLevelName.Contains("GYM")){
			avMove = GameObject.FindGameObjectWithTag("Player").GetComponent<SCR_Camie>();
		}
	}

	void OnLevelWasLoaded(){
		if (instance == this && (Application.loadedLevelName.Contains("NIV") || Application.loadedLevelName.Contains("GYM"))){
			avMove = GameObject.FindGameObjectWithTag("Player").GetComponent<SCR_Camie>();
		}
	}

	#region Accessors
	public static SCR_ManagerComponents ManagerInstance{
		get {return instance;}
		set {instance = value;}
	}
	public static SCR_PointManager PointManager{
		get {return pointManager;}
		set {pointManager = value;}
	}
	public static SCR_LevelManager LevelManager{
		get {return levelManager;}
		set {levelManager = value;}
	}
	public static SCR_SaveData SaveData{
		get {return saveData;}
		set {saveData = value;}
	}
	public static SCR_TimeManager TimeManager{
		get {return timeManager;}
		set {timeManager = value;}
	}
	public static SCR_GUIManager GUIManager{
		get {return guiManager;}
		set {guiManager = value;}
	}
	public static SCR_SoundManager SoundManager{
		get {return soundManager;}
		set {soundManager = value;}
	}
	public SCR_Camie AvatarMove{
		get {return avMove;}
		set {avMove = value;}
	}
	public SCR_Spider Spider{
		get {return sceneSpider;}
		set {sceneSpider = value;}
	}
	public SCR_Joystick Joystick{
		get {return joystick;}
		set {joystick = value;}
	}
	public SCR_FlyButton FlyButton{
		get {return flyButton;}
		set {flyButton = value;}
	}
	public Transform LeftFrame{
		get {return leftFrame;}
		set {leftFrame = value;}
	}
	public Transform RightFrame{
		get {return rightFrame;}
		set {rightFrame = value;}
	}
	public Transform BottomFrame{
		get {return bottomFrame;}
		set {bottomFrame = value;}
	}
	#endregion
}
