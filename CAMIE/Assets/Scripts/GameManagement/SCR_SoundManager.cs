﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SCR_SoundManager : MonoBehaviour {

	#region monobehaviors
	bool soundOn = true;
	Dictionary<string, AudioSource> sources = new Dictionary<string, AudioSource>();

	void Awake(){
		if (SCR_ManagerComponents.SoundManager == null){
			SCR_ManagerComponents.SoundManager = this;
		}
		else return;

		FillSoundDictionary();
	}

	void Start(){
		LevelInit();
	}

	void OnLevelWasLoaded(){
		LevelInit();
	}
	#endregion

	#region init
	void FillSoundDictionary(){
		AudioSource[] temp = transform.GetComponentsInChildren<AudioSource>();

		foreach (AudioSource source in temp){
			sources.Add (source.name, source);
		}
	}

	void LevelInit(){
		StopAllSounds();
		if (SCR_ManagerComponents.LevelManager.CurrentLevel == SCR_LevelManager.MAINMENU){
			PlaySound ("SND_MainMenu");
		}
		else {
			PlaySound ("SND_LevelsMusic");
		}
	}
	#endregion

	#region Play and Stop sounds
	public void PlaySound (string name){
		if (!soundOn) return;

		AudioSource play;

		if (sources.TryGetValue (name, out play)){
			if (!play.isPlaying){
				play.Play ();
			}
		}
	}

	public void StopSound (string name){
		AudioSource stop;

		if (sources.TryGetValue (name, out stop)){
			if (stop.isPlaying){
				stop.Stop();
			}
		}
	}

	public void StopAllSounds(){
		foreach (AudioSource source in sources.Values){
			if (source.isPlaying){
				source.Stop ();
			}
		}
	}

	public bool SoundOn{
		get {return soundOn;}
		set {soundOn = value;}
	}
	#endregion
}
