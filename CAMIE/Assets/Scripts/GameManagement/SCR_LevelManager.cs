﻿using UnityEngine;
using System.Collections;

public class SCR_LevelManager : MonoBehaviour {

	public const int MAINMENU = 0;

	int currentLevel;
	int gameplayLevel; //exclue menus
	bool isMenu; // 0 (splash) et 1 (mainmenu)

	#region Mono
	void Awake(){
		if (SCR_ManagerComponents.LevelManager == null){
			SCR_ManagerComponents.LevelManager = this;
		}
		else return;
	}

#if UNITY_EDITOR
	void Update (){
		if (Input.GetKeyDown (KeyCode.Space)){
			Application.LoadLevel ("GYM_Arianne");
		}
	}
#endif
	void OnLevelWasLoaded(){
		currentLevel = Application.loadedLevel;
		isMenu = currentLevel < 1;
		if (isMenu){
			gameplayLevel = 0;
		}
		else{
			gameplayLevel = currentLevel;
		}
	}
	#endregion

	#region Level start/end
	public void EndLevel(){
		SCR_ManagerComponents.PointManager.ValidateScores ();
		SCR_ManagerComponents.TimeManager.Pause();
		SCR_ManagerComponents.TimeManager.SetTimer (false);
		SCR_ManagerComponents.GUIManager.CloseAllMenus();
		SCR_ManagerComponents.GUIManager.OpenMenu ("Canvas_LevelEnd");
		SCR_ManagerComponents.GUIManager.RelayTextInfo(SCR_GUIManager.TextType.highscore);
		SCR_ManagerComponents.SoundManager.PlaySound ("SND_HighScore");
	}

	public void LoadLevel (int level){
		if (SCR_ManagerComponents.TimeManager.IsPaused){
			SCR_ManagerComponents.TimeManager.Pause ();
		}
		if (!Application.isLoadingLevel){
			Application.LoadLevel (level);
		}
	}
	#endregion

	public int CurrentLevel{
		get {return currentLevel;}
	}
	public int GameplayLevel{
		get {return gameplayLevel;}
	}
	public bool IsMenu{
		get {return isMenu;}
	}
}
