﻿using UnityEngine;
using System.Collections;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class SCR_SaveData : MonoBehaviour {

	public string saveFile = "/Camie.dat";
	Data data;
	int[] highscores; 

	void Awake(){
		if (SCR_ManagerComponents.SaveData == null){
			SCR_ManagerComponents.SaveData = this;
		}
		else return;
	}

	void Start(){
		if (SCR_ManagerComponents.SaveData != this) return;

		Load ();
	}

	#region value assignation
	private void GetDataValues(int level){
		highscores[level] = SCR_ManagerComponents.PointManager.HighScores[level];
	}

	private void SetDataValues(){
		SCR_ManagerComponents.PointManager.HighScores = highscores;
	}
	#endregion

	#region Serialization
	public void Save(int level){
		GetDataValues(level);

		BinaryFormatter bf = new BinaryFormatter();
		FileStream file = File.Create (Application.persistentDataPath + saveFile);
		
		if(data == null){ 
			data = new Data();
		}

		data.highscores = highscores;

		bf.Serialize (file, data);
		file.Close();
	}

	public void Load (){
		if(File.Exists (Application.persistentDataPath + saveFile)){
			BinaryFormatter bf = new BinaryFormatter();
			FileStream file = File.Open(Application.persistentDataPath + saveFile, FileMode.Open); 
			
			data = (Data) bf.Deserialize(file);
			file.Close (); 

			highscores = data.highscores;
		}
		else{
			highscores = new int[30];
			for (int i = 0; i < highscores.Length; i++){
				highscores[i] = 0;
			}
		}

		SetDataValues ();
	}

	public void ResetSave(){
		for (int i = 0; i < highscores.Length; i++){
			highscores[i] = 0;
		}
		SetDataValues ();

		BinaryFormatter bf = new BinaryFormatter();
		FileStream file = File.Create (Application.persistentDataPath + saveFile);
		
		if(data == null){ 
			data = new Data();
		}
		
		data.highscores = highscores;
		
		bf.Serialize (file, data);
		file.Close();
	}
	#endregion
}

#region serializable class
[Serializable]
class Data{
	public int[] highscores;
}
#endregion

