﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SCR_PointManager : MonoBehaviour {

	#region variables and constants
	int[] pointsForLevelAccess = new int[30];

	int ladybugsCurrLevel;
	int puceronsCurrLevel;

	int puceronsMovingInLevel = 0;
	int puceronsStaticInLevel = 10;
	List<SCR_Puceron> puceronsCollected = new List<SCR_Puceron>();

	int[] highscorePerLevel;
	int scoreCumul;
	int totalScoreCurr;

	bool isScoreCounting;

	private const int TOTALLEVELS = 30;
	private const int STARVALUE = 200;
	private const int COLLSTATICVALUE = 7;
	private const int COLLMOVINGVALUE = 10;
	private const int PUCERONSPERLEVEL = 10;

	public enum CollectibleType{
		ladybug,
		puceron
	};
	#endregion

	#region mono
	void Awake(){

		if (SCR_ManagerComponents.PointManager == null){
			SCR_ManagerComponents.PointManager = this;
		}
		else return;

		highscorePerLevel = new int[TOTALLEVELS];

		int levelUnlockScore = 0;
		for (int i = 0; i < TOTALLEVELS; i++){

			pointsForLevelAccess[i] = levelUnlockScore;
			levelUnlockScore += 600;
		}

		isScoreCounting = false;
	}

	void Start(){
		LevelInit();
	}

	void OnLevelWasLoaded(){
		LevelInit();
	}

	void Update(){
		if (puceronsCurrLevel == PUCERONSPERLEVEL && SCR_ManagerComponents.TimeManager.IsCounting){
			SCR_ManagerComponents.LevelManager.EndLevel();
		}
	}
	#endregion

	#region init
	void LevelInit(){
		puceronsMovingInLevel = 0;
		puceronsStaticInLevel = 10;
		totalScoreCurr = 0;
		puceronsCurrLevel = 0;
		ladybugsCurrLevel = 0;

		puceronsCollected.Clear();

		SCR_ManagerComponents.GUIManager.RelayTextInfo (SCR_GUIManager.TextType.ladybug);
		SCR_ManagerComponents.GUIManager.RelayTextInfo (SCR_GUIManager.TextType.puceron);
	}

	public void StartScoring(){
		isScoreCounting = true;
	}
	
	public void IncrementMovingPucerons(){ 
		puceronsMovingInLevel ++; 
		puceronsStaticInLevel --;
	} 
	#endregion

	#region Scoring
	public void ValidateScores(){
		isScoreCounting = false;

		HighScores[SCR_ManagerComponents.LevelManager.GameplayLevel] = Mathf.CeilToInt ((STARVALUE * ladybugsCurrLevel) + 
		                                            (COLLSTATICVALUE * puceronsStaticInLevel) / (SCR_ManagerComponents.TimeManager.TimeSinceLevelStart/75) + 
		                                            (COLLMOVINGVALUE * puceronsMovingInLevel) / (SCR_ManagerComponents.TimeManager.TimeSinceLevelStart/75));

		if (CompareHighscore(SCR_ManagerComponents.LevelManager.GameplayLevel)){
			SetNewHighScore(SCR_ManagerComponents.LevelManager.GameplayLevel);
		}
	}

	public void AcquireCollectible (CollectibleType type){
		switch (type){
		case CollectibleType.ladybug:
			ladybugsCurrLevel ++;

			if (ladybugsCurrLevel == 3){
				SCR_ManagerComponents.SoundManager.PlaySound ("SND_AllStars");
			}
			else{
				SCR_ManagerComponents.SoundManager.PlaySound ("SND_CollectLadybug");
			}

			SCR_ManagerComponents.GUIManager.RelayTextInfo (SCR_GUIManager.TextType.ladybug);
			break;
		case CollectibleType.puceron:
			puceronsCurrLevel ++;
			SCR_ManagerComponents.SoundManager.PlaySound ("SND_CollectPuceron");
			SCR_ManagerComponents.GUIManager.RelayTextInfo (SCR_GUIManager.TextType.puceron);
			break;
		}
	}

	public void LosePucerons (){
		if (puceronsCollected.Count > 0){

			SCR_ManagerComponents.SoundManager.PlaySound ("SND_LoseCollectible");
			SCR_ManagerComponents.ManagerInstance.AvatarMove.LosePucerons();

			int removeAmount = 1;
			if (puceronsCollected.Count > 1){
				removeAmount = Mathf.FloorToInt ((float) puceronsCurrLevel/2);
			}

			while (removeAmount > 0){
				SCR_Puceron toRemove = puceronsCollected[Random.Range (0, removeAmount)];
				toRemove.ReturnToGame();
				puceronsCollected.Remove (toRemove);
				puceronsCollected.TrimExcess();

				puceronsCurrLevel --;
				removeAmount --;
			}
		}
		else{
			SCR_ManagerComponents.LevelManager.EndLevel();
		}

		SCR_ManagerComponents.GUIManager.RelayTextInfo (SCR_GUIManager.TextType.puceron);
	}

	public bool CompareHighscore (int level){
		return totalScoreCurr > highscorePerLevel[level];
	}

	public void SetNewHighScore (int level){
		highscorePerLevel[level] = totalScoreCurr;
		SCR_ManagerComponents.GUIManager.LevelEndMenu.NewHighScore();
		SCR_ManagerComponents.SaveData.Save(SCR_ManagerComponents.LevelManager.GameplayLevel);

		scoreCumul = 0;
		foreach (int score in highscorePerLevel){
			scoreCumul += score;
		}
	}
	#endregion 

	#region Accessors
	public int LadybugsCurrentLevel{
		get {return ladybugsCurrLevel;}
	}
	public int PuceronsCurrentLevel{
		get {return puceronsCurrLevel;}
	}
	public int TotalScore{
		get {return totalScoreCurr;}
	}
	public int[] HighScores{
		get {return highscorePerLevel;}
		set {highscorePerLevel = value;}
	}
	public int ScoreCumul{
		get {return scoreCumul;}
	}
	public int[] PointsForLevelAccess{
		get {return pointsForLevelAccess;}
	}
	public bool IsScoreCounting{
		get {return isScoreCounting;}
	}
	public List<SCR_Puceron> PuceronsCollected{
		get {return puceronsCollected;}
		set {puceronsCollected = value;}
	}
	#endregion
}
