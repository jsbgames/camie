﻿using UnityEngine;
using System.Collections;

public class SCR_TimeManager : MonoBehaviour {

	private bool isPaused = false;
	private bool isCounting = false;
	private float timeSinceLevelStart = 0f;

	void Awake(){
		if (SCR_ManagerComponents.TimeManager == null){
			SCR_ManagerComponents.TimeManager = this;
		}
		else return;
	}

	void Start(){
		SCR_ManagerComponents.GUIManager.RelayTextInfo (SCR_GUIManager.TextType.time);
	}

	void OnLevelWasLoaded(){
		isPaused = false;
		isCounting = false;
		timeSinceLevelStart = 0f;
		SCR_ManagerComponents.GUIManager.RelayTextInfo (SCR_GUIManager.TextType.time);
	}

	void Update(){
		if (isCounting && !isPaused){
			timeSinceLevelStart += Time.deltaTime;
			SCR_ManagerComponents.GUIManager.RelayTextInfo (SCR_GUIManager.TextType.time);
		}
	}

	public void SetTimer (bool turnOn){
		isCounting = turnOn;
	}

	public void Pause(){
		isPaused = !isPaused;

		if (isPaused){
			Time.timeScale = 0;
		}
		else{
			Time.timeScale = 1;
		}
	}

	public bool IsPaused{
		get {return isPaused;}
	}
	public bool IsCounting{
		get {return isCounting;}
	}
	public float TimeSinceLevelStart{
		get {return timeSinceLevelStart;}
	}
}
