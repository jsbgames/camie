﻿using UnityEngine;
using System.Collections;

public class SCR_Wings : MonoBehaviour {

	private Renderer rend;

	void Awake(){
		rend = GetComponent<Renderer>();
	}

	public void SetVisible (bool isVisible){
		rend.enabled = isVisible;
	}
}
