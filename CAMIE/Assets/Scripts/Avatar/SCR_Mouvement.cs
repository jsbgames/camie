﻿//using UnityEngine;
//using System.Collections;
//
//public class SCR_Mouvement : MonoBehaviour {
//
//	#region Variables
//	public float fallGravity = 8f;
//
//	private Vector3 startPosition;
//	private Vector3 inputValue;
//	private Vector3 inputDirection;
//
//	private Vector3 currentDirection; 
//	private GameObject previousHit;
//
//	[Range(0f,10f)]
//	public float speed;
//	public float flightSpeed;
//	private float speedModifier = 1f;
//
//	private bool isGrounded;
//	private bool isFlying;
//	private bool isFalling;
//	private bool canFly;
//	private bool isTakingOff;
//	private bool inBubble;
//
//	private bool branchTipLerping;
//	//private SCR_BranchTip branchTipTrigger;
//
//	private bool hasInput;
//	private bool blockInput;
//
//	public LayerMask walkable;
//
//	public float flightTime;
//	private float currentFlightTime;
//
//	private Animator animator;
//	private CharacterController controller;
//	private SCR_Wings[] wings;
//	#endregion
//
//	#region Monobehaviors
//	void Awake(){
//		startPosition = this.transform.position;
//		isFalling = true;
//		speedModifier = 1f;
//
//		animator = this.GetComponentInChildren<Animator>();
//		controller = this.GetComponent<CharacterController>();
//		wings = this.GetComponentsInChildren<SCR_Wings>();
//	}
//
//	void Update(){
//		if (isGrounded){
//			if (hasInput){
//				OnBranchInput();
//			}
//		}
//		else if (isFlying && !isTakingOff){
//			canFly = FlyTimer();
//
//			if (canFly && hasInput){
//				FlyingInput();
//			}
//			else if (!canFly){
//				FlyEnd ();
//			}
//		}
//		else if (IsFalling){
//			Fall ();
//		}
//	}
//
//	void LateUpdate(){
//		UpdateAnimations();
//	}
//	#endregion
//
//	#region Animations
//	void UpdateAnimations(){
//		animator.SetBool ("isIdle", (isGrounded && !hasInput));
//		animator.SetBool ("isWalking", (isGrounded && hasInput));
//		animator.SetBool ("isFlying", isFlying);
//
//		if (IsInvoking ("BreakIdle") && (hasInput || !isGrounded)){
//			CancelInvoke ("BreakIdle");
//		}
//		else if (isGrounded && !hasInput && !IsInvoking("BreakIdle")){
//			Invoke ("BreakIdle", 2.5f);
//		}
//	}
//
//	void BreakIdle(){
//		if (animator.GetBool ("breakIdle1") || animator.GetBool ("breakIdle2")){
//			animator.SetBool ("breakIdle1", false);
//			animator.SetBool ("breakIdle2", false);
//		}
//		else{
//			int random = Random.Range (0,2);
//
//			switch (random){
//			case 0 : 
//				animator.SetBool ("breakIdle1", true);
//				break;
//			case 1: 
//				animator.SetBool ("breakIdle2", true);
//				break;
//			}
//			Invoke ("BreakIdle", 1.1f);
//		}
//	}
//	#endregion
//
//	#region Inputs
//	public void RecieveInputValues (Vector3 input){
//		inputValue = input;
//		hasInput = true;
//	}
//
//	public void ResetInputValues (){
//		hasInput = false;
//	}
//
//	void OnBranchInput(){
//		RaycastHit hit;
//		Vector3 inputPosition = this.transform.position + inputValue;
//		
////		if (!blockInput && branchTipLerping && branchTipTrigger.HasEntered){
////			Debug.Log (branchTipLerping && !branchTipTrigger.HasEntered);
////			int closestTipWP = branchTipTrigger.GetClosestWaypoint (inputPosition);
////			
////			if (closestTipWP == (branchTipTrigger.CurrentlyOnWaypoint +1) || closestTipWP == (branchTipTrigger.CurrentlyOnWaypoint -1)){
////				branchTipTrigger.GoToWaypoint (closestTipWP, this.transform, false);
////				animator.SetBool ("isWalking", true);
////			}
////			else{
////				closestTipWP = branchTipTrigger.GetClosestExitWaypoint (inputPosition, 0.7f);
////				
////				if (closestTipWP >= 0){
////					branchTipTrigger.GoToWaypoint (closestTipWP, this.transform, true);
////					animator.SetBool ("isWalking", true);
////				}
////				else{
////					animator.SetBool ("isWalking", false);
////				}
////			}
////		}
//		
//		/*else*/ if (Physics.Raycast (this.transform.position + this.transform.up/2f, inputPosition + this.transform.up/2f, out hit, 1.5f, walkable) ||
//		         Physics.Raycast (inputPosition, inputPosition + this.transform.up, out hit, 1.5f, walkable) ||
//		         Physics.Raycast (inputPosition - this.transform.up/4f, this.transform.position - this.transform.up/4f, out hit, 1.5f, walkable)
//		         /* Physics.Raycast (inputPosition, inputPosition - currentDirection, out hit, 1.5f, walkable)*/){
//			
//			float angle = inputValue.x < 0 ? 90 : -90;
//			currentDirection = Quaternion.AngleAxis(angle, Vector3.forward) * hit.normal;
//			controller.Move (currentDirection * speed * speedModifier * 2 * Time.deltaTime);
//		}
//		
//		//TODO Fix le mouvement en dessous de la branche.
//	}
//
//	void FlyingInput(){
//		//cast raycast pour input fly
//		
//		//si hit
//		//part rotation de up vers normale
//		//mouvement et anim d'atterrissage
//	}
//	#endregion
//
//	#region State and Position changes
//	
//	public void OnExitBranchTip(){
//		branchTipLerping = false;
//		//branchTipTrigger = null;
//		Fall (-this.transform.up);
//	}
//
//	public void FlyTakeOff(){
//		StopAllCoroutines();
//		isFlying = true;
//		isFalling = false;
//		isGrounded = false;
//		branchTipLerping = false;
//		ToggleWings (true);
//		SCR_ManagerComponents.SoundManager.PlaySound ("SND_Fly");
//	}
//
//	public IEnumerator TakeOff(){
//		while (isTakingOff){
//
//			this.transform.position = Vector3.Lerp (this.transform.position, this.transform.position + this.transform.up * 3, 0.7f * Time.deltaTime);
//			yield return new WaitForEndOfFrame();
//		}
//	}
//
//	public void FlyEnd(){
//		isFlying = false;
//		isFalling = true;
//
//		SCR_ManagerComponents.SoundManager.StopSound ("SND_Fly");
//		SCR_ManagerComponents.SoundManager.PlaySound ("SND_Fall");
//	}
//
//	IEnumerator Land (Vector3 where, Vector3 surfaceNormal){
//		
//		bool landing = true;
//		Quaternion rotationTarget = Quaternion.FromToRotation (this.transform.up, surfaceNormal);
//		currentDirection = Quaternion.AngleAxis(90, Vector3.forward) * surfaceNormal;
//		
//		while (landing){
//			if (Vector3.Distance(this.transform.position, where) < 0.1f){
//				this.transform.position = where;
//				this.transform.rotation = rotationTarget;
//				Landed ();
//				landing = false;
//			}
//			else{
//				ToggleWings (true);
//				this.transform.position = Vector3.Lerp (this.transform.position, where, fallGravity * 15f * Time.deltaTime);
//				this.transform.rotation = Quaternion.Lerp (this.transform.rotation, rotationTarget, fallGravity * 25f * Time.deltaTime);
//			}
//			yield return null; 
//		}
//	}
//
//	void Landed(){
//		ToggleWings (false);
//		canFly = true;
//		isFalling = false;
//		isGrounded = true;
//
//		SCR_ManagerComponents.SoundManager.PlaySound ("SND_Land");
//	} 
//
//	private void ToggleWings (bool visible){
//		foreach (SCR_Wings wing in wings){
//			wing.SetVisible (visible);
//		}
//	}
//
//	public void Respawn (){
//		this.transform.position = startPosition;
//		isFalling = true;
//	}
//	#endregion
//
//	#region Fall
//	public void Fall(){
//		isFlying = false;
//		isFalling = true;
//		isGrounded = false;
//		ToggleWings(false);
//
//		RaycastHit hit;
//
//		if (Physics.Raycast(this.transform.position, Vector3.down, out hit, 1.5f, walkable.value)){
//
//			isFalling = false;
//			StartCoroutine (Land (hit.point, hit.normal));
//		}
//		else{
//			controller.Move (Vector3.down * 20f * fallGravity * Time.deltaTime);
//		}
//	}
//
//	public void Fall(Vector3 direction){
//		isFlying = false;
//		ToggleWings (false);
//
//		RaycastHit hit;
//
//		if (Physics.Raycast(this.transform.position, this.transform.position + direction, out hit, 0.5f, walkable.value)){
//			StartCoroutine (Land (hit.point, hit.normal));
//		}
//		else{
//			controller.Move (direction * 20f * fallGravity * Time.deltaTime);
//		}
//	}
//	#endregion
//
//	#region Time and Speed changes
//	bool FlyTimer (){
//		if (currentFlightTime < flightTime){
//			currentFlightTime += Time.deltaTime;
//			return true;
//		}
//		else {
//			return false;
//		}
//	}
//
//	public IEnumerator Propulsion (float duration){
//		float time = 0f;	
//		blockInput = true;
//		while (time < duration){
//			this.transform.position = Vector3.Lerp (this.transform.position, this.transform.position += currentDirection, (speed * speedModifier * Time.deltaTime));
//			time += Time.deltaTime;
//			yield return new WaitForEndOfFrame();
//		}
//		blockInput = false;
//	}
//
//	public void DecreaseSpeed (float byPercent){
//		speedModifier = byPercent/100f;
//	}
//
//	public void NormalSpeed (){
//		speedModifier = 1f;
//	}
//	#endregion
//
//	#region Getters & Setters
//	public bool IsFalling{
//		get {return isFalling;}
//		set {isFalling = value;}
//	}
//	public bool CanFly{
//		get {return canFly;}
//	}
//	public bool IsFlying{
//		get {return isFlying;}
//	}
//	public bool IsTakingOff{
//		get {return isTakingOff;}
//		set {isTakingOff = value;}
//	}
//	public bool IsGrounded{
//		get {return isGrounded;}
//	}
//	public bool BranchTipLerping{
//		get {return branchTipLerping;}
//		set {branchTipLerping = value;}
//	}
////	public SCR_BranchTip BranchTipTrigger{
////		get {return branchTipTrigger;}
////		set {branchTipTrigger = value;}
////	}
//	public float ControllerRadius{
//		get {return controller.radius;}
//	}
//	public Animator MyAnimations{
//		get {return animator;}
//		set {animator = value;}
//	}
//	#endregion
//}
