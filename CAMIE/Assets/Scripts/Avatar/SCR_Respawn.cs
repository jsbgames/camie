﻿using UnityEngine;
using System.Collections;

public class SCR_Respawn : MonoBehaviour {

	private Transform avatarTransform;
	private Vector3 avatarStartPosition;
	private Quaternion avatarStartRotation;
	private bool isRespawning;

	void Start () {
		isRespawning = false;
		avatarTransform = SCR_ManagerComponents.ManagerInstance.AvatarMove.transform;
		avatarStartPosition = avatarTransform.position;
		avatarStartRotation = avatarTransform.rotation;
	}

	public void StartRespawn (){
		isRespawning = true; 
		Invoke ("EndRespawn", 1f);
	}

	void EndRespawn (){
		avatarTransform.position = avatarStartPosition;
		avatarTransform.rotation = avatarStartRotation;
		isRespawning = false;
		SCR_ManagerComponents.ManagerInstance.AvatarMove.CanFly = true;
	}

	public bool IsRespawning{
		get {return isRespawning;}
		set {isRespawning = value;}
	}
}
