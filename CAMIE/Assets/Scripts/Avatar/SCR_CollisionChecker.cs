﻿using UnityEngine;
using System.Collections;

public class SCR_CollisionChecker : MonoBehaviour {

	SCR_Camie avatar;
	SCR_Joystick joystick;
	Transform avatarTransform;
	Transform myTransform;
	
	void Start(){
		avatar = SCR_ManagerComponents.ManagerInstance.AvatarMove;
		joystick = SCR_ManagerComponents.ManagerInstance.Joystick;

		avatarTransform = avatar.transform;
		myTransform = this.transform;
	}

	void Update(){
//		if (avatar.IsFlying){
//			if (joystick == null){
//				joystick = SCR_ManagerComponents.ManagerInstance.Joystick;
//			}
//			myTransform.position = avatar.ColliderCenter + (new Vector3 (joystick.HorizontalAxis, joystick.VerticalAxis, 0f).normalized * 0.75f);
//		}
//		else{
			myTransform.position = avatarTransform.position + avatarTransform.right * 0.75f + avatarTransform.up/2;
		myTransform.rotation = avatarTransform.rotation;
		//}
	}

	void OnCollisionEnter(Collision info){
//		if (avatar.IsFlying){
//			avatar.StartCoroutine (avatar.LandOnCollisionPoint (info.contacts[0].point, info.contacts[0].normal));
//		}
//		else{
			avatar.StartCoroutine(avatar.GoToFrontCollisionPoint (info.contacts[0].point, info.contacts[0].normal));
		//}
	}
}
