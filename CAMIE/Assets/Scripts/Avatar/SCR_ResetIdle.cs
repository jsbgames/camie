﻿using UnityEngine;
using System.Collections;

public class SCR_ResetIdle : MonoBehaviour {

	SCR_Camie camie;

	void Start(){
		camie = SCR_ManagerComponents.ManagerInstance.AvatarMove;
	}

	public void ResetIdle(){
		camie.ResetIdle();
	}

	public void Land(){
		camie.ToggleWings (false);
	}

	public void OutOfBubble(){
		camie.EndBubble();
	}
}
