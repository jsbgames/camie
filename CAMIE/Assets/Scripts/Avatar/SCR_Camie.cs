﻿using UnityEngine;
using System.Collections;

public class SCR_Camie : MonoBehaviour {

	public float moveSpeed = 5f;
	public float lerpSpeed = 10f;
	public float gravity = 10f;

	private bool isGrounded = false;
	public float jumpSpeed = 10f;

	private Vector3 surfaceNormal;
	private Vector3 myNormal;
	//private Vector3 myForward;
	private float distGround;
	private bool onCorner;

	private bool goingRight = true;
	private float speedModifier = 1f;

	public float maxFlyTime;
	private float currFlyTime;
	private bool isFlyTimeCounting;
	private bool canFly;
	private bool isJumping = false;
	private IEnumerator jumpRoutine;
	private SCR_Wings[] wings;

	public LayerMask walkable;
	private Rigidbody rb;
	private BoxCollider myColl;
	private Animator anim;
	private SCR_Respawn respawner;

	private SCR_FlyButton flyButton;
	private SCR_Joystick joystick;

	private bool isKnockbacked;
	private bool isLerping;
	private bool isFlyLerping;

	public Animator puceronsPerdus;
	private SCR_Goutte currentGoutte;
	private bool isSlowed;

	private Transform trans;
	private SCR_CamieDirections directionChecker;

	public Transform frontRayTarget;
	public Transform backRayTarget;
	public Collider frontColliderChecker;
	private SphereCollider flyCollider;

	void Start(){
		rb = this.GetComponent<Rigidbody>();
		myColl = this.GetComponent<BoxCollider>();
		anim = this.GetComponentInChildren<Animator>();
		wings = transform.GetComponentsInChildren<SCR_Wings>();
		respawner = this.GetComponent<SCR_Respawn>();

		flyButton = SCR_ManagerComponents.ManagerInstance.FlyButton;
		joystick = SCR_ManagerComponents.ManagerInstance.Joystick;

		ToggleWings (false);

		trans = transform; //transform tout court cache un getcomp donc pas 100% optimise.

		flyCollider = this.GetComponent<SphereCollider>();
		flyCollider.enabled = false;

		jumpRoutine = Jump (myNormal);
		myNormal = trans.up;
		rb.freezeRotation = true;
	}

	void Update(){

		if (joystick == null){
			joystick = SCR_ManagerComponents.ManagerInstance.Joystick;
			if (joystick == null) return;
		}

		#region Respawn, z fix, joystick null check
		if (respawner.IsRespawning){
			rb.AddForce (Vector3.down);
			return;
		}
		else if (trans.position.y < SCR_ManagerComponents.ManagerInstance.BottomFrame.position.y){
			ToggleWings (false);
			Respawn ();
			return;
		}

		if (trans.position.z < -0.5 || trans.position.z > 0.5){
			trans.position = new Vector3 (trans.position.x, trans.position.y, 0f);
		}

		if (joystick == null){
			joystick = SCR_ManagerComponents.ManagerInstance.Joystick;
		}
		#endregion 

		if (isFlyLerping || isLerping) return;

		Ray ray;
		RaycastHit hit;

		#region Flight
		if (canFly && isGrounded && !isJumping && ((flyButton != null && flyButton.IsPressed) || Input.GetKeyDown (KeyCode.F))){
			anim.SetBool ("isWalking", false);
			anim.SetBool ("isIdle", false);
			anim.SetBool ("isFlying", true);
			CancelInvoke ("BreakIdle");

			ToggleWings (true);

			frontColliderChecker.enabled = false;

			isGrounded = false;
			isJumping = true;

			jumpRoutine = Jump (myNormal);
			StartCoroutine (jumpRoutine);
			return;
		}
		else if (isJumping && isFlyTimeCounting){ 
			if (currFlyTime < maxFlyTime && (flyButton.IsPressed || (Application.isEditor && Input.GetKey (KeyCode.F)))){ 
				if (joystick.IsDragging){
					Vector3 input = new Vector3 (joystick.HorizontalAxis, joystick.VerticalAxis, 0f);
					trans.Translate (input * Time.deltaTime * speedModifier * moveSpeed);

				}
				currFlyTime += Time.deltaTime;
			}
			else{ 
				anim.SetBool ("isFlying", false);
				SCR_ManagerComponents.SoundManager.PlaySound ("SND_Fall");
				rb.AddForce (myNormal * -gravity * rb.mass * Time.deltaTime);
				currFlyTime = 0f;
				isJumping = false;
				canFly = false;
				//surfaceNormal = Vector3.up;
				//myNormal = Vector3.up;
				//frontColliderChecker.enabled = true;
				//flyCollider.enabled = false;
				//myColl.enabled = true;
			}
		}
		else if (isJumping) return; //pendant l'atterrissage et le decollage.
		else if (flyButton == null){
			flyButton = SCR_ManagerComponents.ManagerInstance.FlyButton;
		}
		else{
			rb.AddForce (-gravity * rb.mass * myNormal* Time.deltaTime);
		}
		#endregion

		if (isJumping) return;

		if (joystick.IsDragging) goingRight = joystick.PointingTo == SCR_Joystick.Direction.right;

		ray = new Ray (trans.position, -myNormal);

		if (Physics.Raycast (ray, out hit) && hit.distance < 1.6){
			isGrounded = true;

			Ray ray1 = new Ray (trans.position + trans.right/2, -frontRayTarget.position);
			Ray ray2 = new Ray (trans.position - trans.right/2, -backRayTarget.position);

			RaycastHit hit1;
			RaycastHit hit2;

			if (Physics.Raycast (ray1, out hit1) && Physics.Raycast (ray2, out hit2) 
			    				&& hit1.normal != hit2.normal && hit1.distance < 2f && hit2.distance < 2f){

				surfaceNormal = (hit1.normal + hit2.normal) / 2f;
			}
			else{
				surfaceNormal = hit.normal;
			}
		}
		else {
			isGrounded = false;
			surfaceNormal = Vector3.up;
		}

		canFly = true;
		isJumping = false;

		GetDirection ();
		myNormal = surfaceNormal;

		//Effectue le mouvement
		if (joystick != null && joystick.IsDragging){

			transform.Translate (joystick.HorizontalAxis * Mathf.Sign (joystick.HorizontalAxis) * moveSpeed * speedModifier * Time.deltaTime, 0f, 0f);

			anim.SetBool ("isWalking", true);
			anim.SetBool ("isIdle", false);
			CancelInvoke ("BreakIdle");
		}
		else{
			anim.SetBool ("isWalking", false);
			anim.SetBool ("isIdle", true);
		}

		rb.AddForce (-gravity * rb.mass * myNormal * speedModifier * Time.deltaTime);
	}

	void OnCollisionEnter (Collision info){
		if (IsFlying && !isFlyLerping && info.transform.tag == "Navigable"){
			StartCoroutine (LandOnCollisionPoint (info.contacts[0].point, info.contacts[0].normal)); 
		}
	}

	public IEnumerator GoToFrontCollisionPoint (Vector3 point, Vector3 normal){
		if (normal != Vector3.zero){
			isLerping = true;
			frontColliderChecker.enabled = false;

			while (Vector3.Distance (point, trans.position) > 0.2){
				trans.position = point;
				surfaceNormal = normal;
				GetDirection ();
				myNormal = surfaceNormal;
				rb.AddForce (-gravity * rb.mass * myNormal * speedModifier * Time.deltaTime);

				yield return new WaitForEndOfFrame();
			}
			isLerping = false;
			Invoke ("ResetActiveCollisionDetector", 0.1f);
		}
	}

	void ResetActiveCollisionDetector(){
		frontColliderChecker.enabled = true;
	}

	public IEnumerator LandOnCollisionPoint (Vector3 point, Vector3 normal){
		SCR_ManagerComponents.SoundManager.StopSound ("SND_Fly");
		isFlyLerping = true;
		frontColliderChecker.enabled = false;
		isJumping = false;
		isFlyTimeCounting = false;
		canFly = false;
		anim.SetBool ("isFlying", false);
		flyCollider.enabled = false;

		while (Vector3.Distance (trans.position, point) > 0.3f){
			trans.position = Vector3.Lerp (trans.position, point, jumpSpeed * 1.75f * speedModifier * Time.deltaTime);
			surfaceNormal = normal;
			GetDirection();

			yield return new WaitForEndOfFrame();
		}
		trans.position = point;
		surfaceNormal = normal;
		myNormal = normal;
		GetDirection();
		isGrounded = true;
		frontColliderChecker.enabled = true;
		myColl.enabled = true;
		SCR_ManagerComponents.SoundManager.PlaySound ("SND_Land");
		isFlyLerping = false;
		canFly = true;
		//ResetActiveCollisionDetector();
		Invoke ("ResetActiveCollisionDetector", 0.1f);
	}

	IEnumerator Jump (Vector3 normal){
		SCR_ManagerComponents.SoundManager.PlaySound ("SND_Fly");
		float t = 0f;
		rb.velocity += myNormal * jumpSpeed;
		myColl.enabled = false;

		while (t < 0.5){
			t += Time.deltaTime * speedModifier;
			yield return new WaitForEndOfFrame();
		}

		surfaceNormal = Vector3.up;
		goingRight = true;
		GetDirection();
		flyCollider.enabled = true;
		rb.velocity = Vector3.zero;
		currFlyTime = 0f;
		isFlyTimeCounting = true;
		flyCollider.enabled = true;
	}

	void GetDirection (){
		int quaternionIndex = 0;
		Vector3 closestNormal = directionChecker.Normals[0];

		for (int i = 1; i < directionChecker.Normals.Length; i++){
			if (Vector3.Distance (directionChecker.Normals[i], surfaceNormal) < Vector3.Distance (closestNormal, surfaceNormal)){
				closestNormal = directionChecker.Normals[i];
				quaternionIndex = i;
			}
		}
		surfaceNormal = closestNormal;
		Quaternion targetRotation = goingRight ? directionChecker.Angles[quaternionIndex] : directionChecker.Angles[quaternionIndex] * Quaternion.AngleAxis (180f, Vector3.up);
		trans.rotation = Quaternion.Lerp (trans.rotation, targetRotation, lerpSpeed * 5 * Time.deltaTime);
	}
	
	public void DecreaseSpeed(float toPercent){
		speedModifier = speedModifier/toPercent;
		isSlowed = true;
	}
	public void NormalSpeed(){
		speedModifier = 1f;
		isSlowed = false;
	}

	public void Respawn(){
		anim.SetBool ("isFlying", false);
		anim.SetBool ("isWalking", false);
		isJumping = false;
		currFlyTime = 0;
		isFlyLerping = false;
		isLerping = false;
		surfaceNormal = Vector3.up;
		GetDirection ();
		flyButton.IsPressed = false;
		respawner.StartRespawn();
		canFly = false;
		flyCollider.enabled = false;
		myColl.enabled = true;
		frontColliderChecker.enabled = true;
	}

	void BreakIdle(){
		if (anim.GetBool ("breakIdle1") || anim.GetBool ("breakIdle2")){
			anim.SetBool ("breakIdle1", false);
			anim.SetBool ("breakIdle2", false);
		}
		else{
			int random = Random.Range (0,3);
			
			switch (random){
			case 0 : 
				anim.SetBool ("breakIdle1", true);
				break;
			case 1: 
				anim.SetBool ("breakIdle2", true);
				break;
			case 2:
				anim.SetBool ("breakIdle3", false);
				break;
			}
		}
	}

	public void ResetIdle(){
		anim.SetBool ("breakIdle1", false);
		anim.SetBool ("breakIdle2", false);
		anim.SetBool ("breakIdle3", false);

		if (!IsInvoking("BreakIdle")){
			Invoke ("BreakIdle", 1.5f);
		}
	}

	public void ToggleWings(bool show){
		foreach (SCR_Wings wing in wings){
			wing.SetVisible (show);
		}
	}

	public void InBubble(SCR_Goutte goutte){
		isSlowed = true;
		anim.SetBool ("isInBubble", true);
		currentGoutte = goutte;
	}

	public void EndBubble(){
		NormalSpeed();
		anim.SetBool ("isInBubble", false);
		Destroy (currentGoutte.gameObject);
	}

	public void LosePucerons (){
		puceronsPerdus.SetTrigger ("Play");
	}

	public bool IsGrounded{
		get {return isGrounded;}
	}
	public bool IsFlying{
		get {return isJumping && isFlyTimeCounting;}
	}
	public bool CanFly{
		get {return canFly;}
		set {canFly = value;}
	}
	public bool IsSlowed{
		get {return isSlowed;}
	}
	public Animator MyAnimations{
		get {return anim;}
	}
	public Vector3 ColliderCenter{
		get {return myColl.bounds.center;}
	}
	public SCR_CamieDirections Director{
		get {return directionChecker;}
		set {directionChecker = value;}
	}
	public SCR_Respawn Respawner{
		get {return respawner;}
	}
}
